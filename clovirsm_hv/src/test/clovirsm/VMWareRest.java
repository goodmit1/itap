package test.clovirsm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.VCRestConnection;
import com.clovirsm.hv.vmware.rest.GetVMTags;
import com.clovirsm.hv.vmware.rest.PutVMTags;

public class VMWareRest {

	
@org.junit.Test
public void putTag() throws Exception {
	VCRestConnection conn = login();
	PutVMTags puttags= new PutVMTags(conn);
	Map param = new HashMap();
	Map tags = new HashMap();
	tags.put("test1", "test");
	param.put("VM_HV_ID", "vm-1020");
	param.put("TAGS", tags);
	puttags.run(param, param);
	
}
	
	@org.junit.Test
	
	public void tagList() throws Exception {
		VCRestConnection conn = login();
		GetVMTags tags = new GetVMTags(conn);
		Map param = new HashMap();
		tags.run(param, param);
		System.out.println(param);
		
	}
	
	public VCRestConnection login() throws Exception {
		VCRestConnection conn = new VCRestConnection();
		
		conn.connect( "https://10.150.0.10/sdk", "administrator@vsphere.local","Goodmit1!", null);
		 
		return conn;
	}
}

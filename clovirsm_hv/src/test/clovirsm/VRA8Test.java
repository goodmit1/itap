package test.clovirsm;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.Before;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.vra.v8.ParseParam;
import com.clovirsm.hv.vmware.vra.v8.VListCatalog8;
import com.clovirsm.hv.vmware.vra.v8.VRA8Connection;
import com.clovirsm.hv.vmware.vra.v8.VRA8Util;
import com.clovirsm.hv.vmware.vra.v8.VRAAPI8;
import com.clovirsm.hv.vmware.vra.v8.YamlCalc; 
public class VRA8Test {
	
	VRAAPI8 api = null;
	Map  param = null;
	@Before
	public void setUp() {
		param = new HashMap();
		param.put("CONN_URL", "https://vm1vra.goodmit.vm1.kr");
		 
		param.put("CONN_USERID", "webadmin");
		param.put("CONN_PWD", "VMware1!");
		api = new VRAAPI8();
		 
	}
	
	@Test
	public void listCluster() throws Exception {
			VRA8Connection conn;
		 
			conn = (VRA8Connection) api.connect(param);
			JSONObject json = conn.getRestClient().getJSON("/provisioning/uerp/provisioning/mgmt/compute?expand&$filter=((type%20eq%20%27ZONE%27)%20or%20(type%20eq%20%27VM_HOST%27))");
			System.out.println(json.toString());
			JSONObject documents = json.getJSONObject("documents");
			Iterator it = documents.keys();
			List result = new ArrayList();
			while(it.hasNext()) {
				String key = (String) it.next();
				JSONObject document = documents.getJSONObject(key);
				JSONArray tags = document.getJSONArray("tags");
				//if(tags.length()==0) continue;
				String name = document.getString("name");
				
				String hostName = document.getJSONArray("endpoints").getJSONObject(0).getJSONObject("endpointProperties").getString("hostName");
				
				
				Map m = new HashMap();
				String datacenter = document.getString("regionId");
				int pos = datacenter.indexOf(":");
				m.put("DC_HV_ID", datacenter.substring(pos+1));
				m.put("NAME", name);
				if(tags.length()>0) {
					String tagKey = tags.getJSONObject(0).getString("key");
					String tagValue = tags.getJSONObject(0).getString("value");
					m.put("TAG", tagKey + ("".equals(tagValue)?"":":" + tagValue));
				}
				m.put("TYPE", document.getString("type"));
				m.put("DC_IP", hostName);
				result.add(m);
			}
			System.out.println(result);
	}
			
	private void put(JSONObject result, String name, JSONObject value) {
		String type = (String) value.keys().next();
		if(type.equals("string")) {
			result.put(name, value.getJSONObject(type).getString("value"));
			
		}
		else if(type.equals("number")) {
			result.put(name, value.getJSONObject(type).getInt("value"));
		}
		else if(type.equals("composite")) {
			 JSONArray property = value.getJSONObject(type).getJSONArray("property");
			 for(int j=0; j < property.length(); j++ ) {
				 
			 }
		}
	}
	@Test
	public void requestCatalog() throws Exception {
		VRA8Connection conn;
		 
			conn = (VRA8Connection) api.connect(param);
			JSONObject json = new JSONObject();
			json.put("deploymentName","test234");
			json.put("reason","test234");
			json.put("projectId","6386337f-cd32-4800-a34a-7ebead30106a");
			JSONObject inputs = new JSONObject();
			inputs.put("vm_nm","dcvm1");
			json.put("inputs", inputs);
			//inputs.put("dc","dc");
			System.out.println(conn.getRestClient().post("/catalog/api/items/b880d62d-2282-3645-9fb3-137123742496/request", json.toString()));
	}
			
	@Test
	public void executeWorkflow() throws Exception {
		VRA8Connection conn;
		 
			conn = (VRA8Connection) api.connect(param);
			JSONObject json = new JSONObject();
			/*
			 * deploymentName: "test3"
projectId: "6386337f-cd32-4800-a34a-7ebead30106a"
inputs: {vm_nm: "test"}
			 */
			json.put("deploymentName", "test1");
			json.put("projectId","6386337f-cd32-4800-a34a-7ebead30106a");
			JSONObject inputs = new JSONObject();
			inputs.put("vm_nm", "test");
			json.put("inputs", inputs);
			System.out.println(conn.getRestClient().post("/catalog/api/items/b880d62d-2282-3645-9fb3-137123742496/request", json.toString()));
	}
	@Test
	public void getWorkflowList() throws Exception {
		VRA8Connection conn;
		 
			conn = (VRA8Connection) api.connect(param);
			//byte[] buf =  (byte[])conn.getRestClient().get("/icon/api/icons/fbca4820-d22c-36a4-908e-ee7ed2644cad")  ;
			System.out.println(conn.getRestClient().getJSON("/deployment/api/deployments/90aed056-24f4-4f99-a1dc-1bbeaedc24c1/resources/baec8ac5-149d-45a9-9673-be5a4733bc9c"));
	}
	@Test
	public void getCatalogInfo() throws Exception {
		VRA8Connection conn;
		 
			conn = (VRA8Connection) api.connect(param);
			String catalogId = "3280b61d-e04f-3131-8a2e-3047153fe9d8";
			JSONObject json = (JSONObject) conn.getRestClient().get("/catalog/api/items/" + catalogId);
			System.out.println(json);
			String projectId = json.getJSONArray("projectIds").getString(0);
			JSONObject modelJson = new JSONObject();
			
			//{"type":"object","properties":{"deploymentName":{"type":"string","description":"Deployment Name","maxLength":"80"},"description":{"type":"string","description":"Description","maxLength":"256"},"project":{"type":"string","description":"Project","enum":[],"default":"6386337f-cd32-4800-a34a-7ebead30106a"},"dc":{"type":"string","title":"dc"},"vm_nm":{"type":"string","title":"vm_nm"}},"required":["deploymentName","project"]}
			JSONObject model = (JSONObject)conn.getRestClient().post("/form-service/api/forms/renderer/model?sourceType=com.vmw.vro.workflow&sourceId=" + catalogId + "&formType=requestForm", json.getJSONObject("schema").toString());
			System.out.println(model);
			JSONArray pages = model.getJSONObject("model").getJSONObject("layout").getJSONArray("pages");
			Map step = new LinkedHashMap();
			String[] skipId = new String[] {"deploymentName", "project", "description"};
			Map defaultMap = new HashMap();
			//[{"type":"string","oneOf":[{"title":"VMKLOUD","const":"vmkloud"}],"default":"vmkloud","title":"Region","name":"env","fieldName":"constraints"},{"type":"string","encrypted":true,"default":"password","title":"Login Password","name":"password","fieldName":"cloudConfig"},{"type":"string","oneOf":[{"title":"Public","const":"public"},{"title":"Static","const":"static"},{"title":"Stage","const":"stage"},{"title":"Department","const":"routed"},{"title":"Development","const":"outbound"}],"default":"stage","title":"Purpose","name":"net","fieldName":"networkType"}]
			for(int i=0; i < pages.length(); i++) {
				JSONObject page = pages.getJSONObject(i);
				String title = page.getString("title");
				JSONArray fieldList = new JSONArray();
				JSONArray sections = page.getJSONArray("sections");
				for(int j=0; j < sections.length(); j++) {
					JSONArray  fields = sections.getJSONObject(j).getJSONArray("fields");
					for(int k=0; k < fields.length(); k++) {
						JSONObject field = fields.getJSONObject(k);
						String id = field.getString("id");
						if( CommonUtil.indexOf( skipId, field.getString("id"))>=0 ) {
							continue;
						}
						if(!field.getJSONObject("state").getBoolean("visible")) {
							defaultMap.put(id, model.getJSONObject("model").getJSONObject("schema").getJSONObject(id).getString("default"));
							continue;
						}
						String inputType = field.getString("display"); // dropDown
					
						fieldList.put( getFieldInfo(id, projectId, model.getJSONObject("model").getJSONObject("schema").getJSONObject(id), defaultMap));
					}
				}
				step.put(title, fieldList);
			}
			 
			System.out.println(step);
			
	}
	private JSONObject getFieldInfo(String name, String prjId, JSONObject schema, Map defaultMap) {
		JSONObject result = new JSONObject();
		result.put("title", schema.getString("label"));
		result.put("name", name);
		
		if(schema.getJSONObject("type").has("isMultiple") && schema.getJSONObject("type").getBoolean("isMultiple")) {
			result.put("type", "array");
		}
		else {
			result.put("type", schema.getJSONObject("type").getString("dataType"));
		}
		if(schema.getJSONObject("type").getString("dataType").equals("reference")) {
			result.put("fieldName", schema.getJSONObject("type").getString("referenceType"));
		}
		if(schema.has("default")) {
			result.put("default", schema.getString("default"));
		}
		if(schema.has("valueList")) {
			JSONObject valueList = schema.getJSONObject("valueList");
			if("scriptAction".equals(valueList.getString("type"))){
				JSONObject api = new JSONObject();
				api.put("url","/form-service/api/forms/renderer/external-value?projectId=" + prjId);
				JSONArray params1 = new JSONArray();
				JSONArray dependOn = new JSONArray();
				JSONArray parameters = valueList.getJSONArray("parameters");
				for(int i=0; i  < parameters.length(); i++) {
					JSONObject param = parameters.getJSONObject(i);
					Iterator it = param.keys();
					while(it.hasNext()) {
						String key = (String)it.next();
						if(!key.equals("$type")) {
							JSONObject param1 = new JSONObject();
							param1.put("name", key);
							Object d = defaultMap.get(param.get(key));
							if(d == null) {
								param1.put("value", "${" + param.get(key) + "}");
								dependOn.put(param.get(key));
							}
							else {
								param1.put("value", d);
							}
							param1.put("useResultFromRequestId",-1);
							params1.put(param1);
						
							
						}
					}
				}
				api.put("param", "{\"uri\":\"" + valueList.getString("id") + "\",\"dataSource\":\"scriptAction\",\"parameters\": " + params1.toString() +",\"requestId\":1}");
				api.put("dependOn",dependOn);
				result.put("api", api);
			}
		}
		return result;
	}
	@Test
	public void listOWorkflow() {
		VRA8Connection conn;
		try {
			conn = (VRA8Connection) api.connect(param);
			JSONObject json = conn.getRestClient().getJSON("/vco-controlcenter/client/api/platform/workflows/71abd7be-0cd9-49b2-bc4a-96f96ad5707f/executions/a430bbec-77a3-459f-8103-0824ef423ae1/?showDetails=true");
			JSONArray outputs = json.getJSONArray("output-parameters");
			JSONObject result = new JSONObject();
			for(int i=0; i< outputs.length(); i++) {
				JSONObject output = outputs.getJSONObject(i);
				String name = output.getString("name");
				JSONObject value = output.getJSONObject("value");
				String type = (String) value.keys().next();
				if(type.equals("string")) {
					result.put(name, value.getJSONObject(type).getString("value"));
				}
				else if(type.equals("composite")) {
					JSONObject item = new JSONObject();
					 JSONArray property = value.getJSONObject(type).getJSONArray("property");
					 for(int j=0; j < property.length(); j++ ) {
						 item.put(property.getJSONObject(j).getString("id"), property.getJSONObject(j).getJSONObject("value").getJSONObject("string").getString("value"));
					 }
					 result.put(name, item);
				}
				else if(type.equals("array")) {
					JSONObject item = new JSONObject();
					 JSONArray property = value.getJSONObject(type).getJSONArray("elements");
					 for(int j=0; j < property.length(); j++ ) {
						 item.put(property.getJSONObject(j).getString("id"), property.getJSONObject(j).getJSONObject("value").getJSONObject("string").getString("value"));
					 }
					 result.put(name, item);
				}
			}
			System.out.println(result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//https://api.mgmt.cloud.vmware.com/provisioning/access-token
		@org.junit.Test
		public void vra8Login() throws Exception {
			RestClient client = new RestClient("https://console.cloud.vmware.com");
			String param = "refresh_token=nnBVnTHzWZjKugopToKvENKOWdY3PCEG26nThMTWmVN45raJEPmSJFq8iKu2jgSn";
			client.setMimeType("application/x-www-form-urlencoded");
			System.out.println(client.post("/csp/gateway/am/api/auth/api-tokens/authorize", param));
			
		}
		@org.junit.Test
		public void request() throws Exception {
			param.put("PURPOSE","syk-test");
			param.put("CMT","syk-test");
			param.put("CATALOG_ID","41f63f33-e369-43e2-b69b-8a023f541057");
			
			param.put("DATA_JSON","{\"diskSize\":\"1\",\"_p_kubun\":\"qa\",\"_fab\":\"m14\",\"_purpose\":\"db\",\"_vm\":[\"m14AICdb003qa\",\"m14AICdb004qa\"],\"_svc\":\"AIC\"}");
				
			System.out.println(api.request(param,null));
		}
		@org.junit.Test
		public void reconfig() throws Exception{
			param.put("VRA_REQUEST_ID", "90aed056-24f4-4f99-a1dc-1bbeaedc24c1");
			param.put("CPU_CNT",2);
			param.put("RAM_SIZE",2);
			param.put("VM_NM", "m16nulldb001dev");
			List diskList = new ArrayList();
			Map disk = new HashMap();
			disk.put("DISK_NM","boot-disk");
			disk.put("DISK_SIZE",42);
			diskList.add(disk);
			param.put("DATA_DISK_LIST", diskList);
			api.getVMReconfig(param);
		}
		@org.junit.Test
		public void getStatus() throws Exception {
			param.put("VRA_REQUEST_ID","90aed056-24f4-4f99-a1dc-1bbeaedc24c1");
			 
			System.out.println(api.getRequestState(param));
		}
		@org.junit.Test
		public void requestModify() throws Exception {
			
			
			param.put("PURPOSE","syk-test");
			param.put("CMT","syk-test");
			param.put("CATALOG_ID","41f63f33-e369-43e2-b69b-8a023f541057");
			param.put("VRA_REQUEST_ID","49f38b3a-2cc6-458d-9e7a-949e29d39f81");
			param.put("DATA_JSON","{'diskSize':3}");
			JSONObject o = new JSONObject((String)param.get("DATA_JSON"));
			System.out.println(o.toString());
			//if(true) return ;
			System.out.println(api.request(param,null));
		}
		@org.junit.Test
		public void requestDelete() throws Exception {
			
			
			param.put("PURPOSE","syk-test");
			param.put("CMT","syk-test");
			param.put("CATALOG_ID","41f63f33-e369-43e2-b69b-8a023f541057");
			param.put("VRA_REQUEST_ID","49f38b3a-2cc6-458d-9e7a-949e29d39f81");
			param.put("DATA_JSON","{'diskSize':3}");
			 
			System.out.println(api.requestDelete(param ));
		}
		@org.junit.Test
		public void listCatalog() throws Exception {
			System.out.println(api.listCatalog(param));
		}
		@org.junit.Test
		public void getFavors() throws Exception {
			System.out.println(api.getFavors(param));
		}
		@org.junit.Test
		public void getBaseImages() throws Exception {
			System.out.println(api.getImageNames(param));
		}
		@org.junit.Test
		public void getDeployments() throws Exception {
			//param.put("VRA_REQUEST_ID", "6db56100-3d7f-4052-8d4c-56303d6606fa");
			System.out.println(api.getRequestState(param));
		}
		@org.junit.Test
		public void ipdomain() throws Exception{
			String ip = "10.10.10.206";
			InetAddress ipAddress = java.net.InetAddress.getByName(ip);
			String domain = ipAddress.getHostName();
			System.out.println(domain);
			InetAddress domainAddress = java.net.InetAddress.getByName(domain);
			System.out.println(domainAddress.getHostAddress());
		}
		@Test
		public void getApp() {
			 
			
			try {
				  Yaml yaml= new Yaml();
				  Map map = (Map) yaml.load( new FileInputStream("C:\\workspace_clovir\\clovirsm_hv\\src\\test\\clovirsm\\ansible.yaml"));
				  JSONObject json = new JSONObject(map);
				  System.out.println(VRA8Util.getSWInfo( json, null));
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		@Test
		public void test() {
			String pattern = ".*-\\s('|\")?\\$\\{input.ip";
			String test = "zzzzzzzzzzzzzzzz\n  - ${input.ip}'";
			Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

			Matcher m = p.matcher(test);
			 
			 
			
			System.out.println(m.find());
		}
		@Test
		public void externParam() throws Exception   {
			VRA8Connection conn;
			 
				conn = (VRA8Connection) api.connect(param);
				String str  ="{\"param\":{\"requestId\":1,\"uri\":\"syk/getVM\",\"dataSource\":\"scriptAction\",\"parameters\":[{\"name\":\"dcName\",\"useResultFromRequestId\":-1,\"value\":\"dc\"}]},\"dependOn\":[],\"url\":\"/form-service/api/forms/renderer/external-value?projectId=6386337f-cd32-4800-a34a-7ebead30106a\"}";
				JSONObject json = new JSONObject(str);
				 
				System.out.println(conn.getRestClient().post(json.getString("url"), json.getJSONObject("param").toString()));
		}
		@Test
		public void test1() throws JSONException, Exception {
			YamlCalc cal = new YamlCalc(new JSONObject("{multi_disk:['40=/data1']}"));
			System.out.println(cal.cal("${length(input.multi_disk)>=2  ? split(input.multi_disk[1],\"=\")[0] : 0}"));
			
		}
		@Test
		public void parseParam() {
			 
			
			try {
				  Yaml yaml= new Yaml();
				  Map map = (Map) yaml.load( new FileInputStream("C:\\workspace_clovir\\clovirsm_hv\\src\\test\\clovirsm\\blueprint.yaml"));
				 
				ParseParam list = new ParseParam();
				Map steps = list.run(map);
				System.out.println(list.getJSON() + ":" + steps);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
}

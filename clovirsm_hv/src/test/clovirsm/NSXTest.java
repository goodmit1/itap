package test.clovirsm;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.vmware.log.LogInsightAPI;
import com.clovirsm.hv.vmware.nsx.NSXAPI;
import com.clovirsm.hv.vmware.nsx.NSXConnection;

public class NSXTest {
	com.clovirsm.hv.vmware.nsx.NSXAPI api;
	NSXConnection conn;
	Map  param ;
	@Before
	public void setUp() throws Exception {
		param = new HashMap();
		param.put("CONN_URL", "https://nsx.goodcs.local");
		 
		param.put("CONN_USERID", "admin");
		param.put("CONN_PWD", "VMware1!VMware1!");
		//param.put("provider", "Local");
		api = new NSXAPI();
		conn = (NSXConnection) api.connect(param); 
	}
	
	@Test
	public void listDCList() throws Exception {
		JSONObject list = conn.getRestClient().getJSON("/api/v1/fabric/compute-managers");
		JSONArray results = list.getJSONArray("results");
		for(int i=0;  i < results.length(); i++) {
			System.out.println(results.getJSONObject(i).getString("server"));
		}
	}
	
	@Test
	public void listTransportNodeList() throws Exception {
		JSONObject list = conn.getRestClient().getJSON("/api/v1/transport-nodes");
		JSONArray results = list.getJSONArray("results");
		for(int i=0;  i < results.length(); i++) {
			JSONObject node_deployment_info = results.getJSONObject(i).getJSONObject("node_deployment_info");
			System.out.println(node_deployment_info);
			if(node_deployment_info.has("fqdn")) {
				System.out.println(node_deployment_info.getString("resource_type") + ":" + node_deployment_info.getString("fqdn"));
			}
		}
	}
	
}

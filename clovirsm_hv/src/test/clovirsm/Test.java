package test.clovirsm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.json.JSONTokener;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.SessionRestClient;
import com.clovirsm.hv.vmware.vra.FormGen;
import com.clovirsm.hv.vmware.vra.MyPredicate;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.vmware.security.credstore.Base64;

public class Test {
	@org.junit.Test
	public void reg3() {
		String pattern = "/api/ddic/(save|delete).*";
		System.out.println("/api/ddic/saveMulti".matches(pattern));
		System.out.println("/api/ddic/save".matches(pattern));
		System.out.println("/api/ddic/delete".matches(pattern));
		System.out.println("/api/ddic/list".matches(pattern));

	}
	
	
	@org.junit.Test
	public void rest() throws Exception {
		SessionRestClient client = new SessionRestClient("http://10.150.0.4");
		System.out.println(client.get("/api/guest/ping"));
	}
	@org.junit.Test
	public void dateTest() {
		Date date1 = new Date(1570248000000l);
		Date today = new Date();
		 
		System.out.println();
		System.out.println(new Date(15634602828281266l));
		System.out.println(new Date(15634602830267154l));
		
	}
	@org.junit.Test
	public void linked() {
		Map m = new LinkedHashMap();
		for(int i=0; i < 10; i++) {
			m.put(i,i);
		}
		for(Object o:m.keySet()) {
			System.out.println(o);
		}
	 
	}
	
	public static List getApp(String str) {
		String startStr = "?tasks";
		int pos = str.lastIndexOf(startStr);
		 
		if(pos<0) return null;
		str = str.substring(pos);
		String[] arr = str.split("/");
		List result = new ArrayList();
		for(String arr1 : arr) {
			int pos1 = arr1.indexOf(".yaml");
			if(pos1>0) {
				result.add(arr1.substring(0, pos1));
			}
		}
		return result;
		
		 
	}
	@org.junit.Test
	public void test() {
		String s = "${resource[\"Disk\"].id}";
		System.out.println(CommonUtil.getInnerStr(s, "resource[\"", "\"]"));
	}
	@org.junit.Test
	public void getSW() {
		String test="/test/tomcat_8.5.yaml";
		String test1="/test/tasks.yaml?tasks=/WAS/tomcat_8.5.yaml&tasks=/DB/mysql_8.yaml";
		
		System.out.println(getApp(test));
		System.out.println(getApp(test1));
		
	}
	@org.junit.Test
	public void sessionRestClient() {
		RestClient client = new RestClient("http://localhost");
		Map header = new HashMap();
		header.put("Cookie","JSESSIONID=EF6370E28A26BC1CA843B6B537D6BCAF");
		client.setHeader(header);
		try {
			System.out.println(client.get("/api/vra_catalog/list/list_NC_VRA_CATALOG/"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@org.junit.Test
	public void move() {
		File file = new File("/temp/1.bmp");
		String name = file.getName();
		System.out.println(name);
		file.renameTo(new File("/temp/deploy/" + name));
	}
	@org.junit.Test
	public void split() {
	
		String str = "${map_to_object(resource.Disk_1[*].id+resource.Disk_2[*].id + resource.Disk_3[*].id+resource.Disk_4[*].id, \"source\")}";
		String arr[] = str.split("(\\.|\\[|\\+|\\(| )");
		System.out.println(arr);
	}
	@org.junit.Test
	public void split2() {
	
		String str = "102.1.2.1     	localhost";
		String arr[] = str.split("( |\t)+");
		System.out.println(arr.length + ":" + arr[1]);
	}
	@org.junit.Test
	public void reg() {
		String str = "'${input.ip}\",\"\\\"\\/test\\/tasks.yaml?tasks=${input.app}\\\"'";
		
		System.out.println(str.matches(".*tasks\\.yaml\\?tasks=.*input\\.app.*"));
		 
	}
	@org.junit.Test
	public void split3() {
		String test="1,b,3";
		String[] arr = test.split(",");
		System.out.println(arr.length + "=" + arr[1]);
				
				
	}
	@org.junit.Test
	public void reg2() {
		String str = "${map_to_object(resource['DISK1'][*].id)}";
		//String pattern = ".*\\$\\{.*resource(\\[\"|\\.)DISK1(\"\\]|\\.).*\\}.*";
		String pattern = ".*\\$\\{.*resource(\\[(\"|')|\\.)DISK1(('|\")\\]|\\.|\\[).*";
		System.out.println(str.matches(pattern));
		 
	}
	@org.junit.Test
	public void base64()
	{
		String s="ewogICAgImFwaVZlcnNpb24iOiAxLAogICAgImRhdGFzb3VyY2VzIjogWwogICAgICAgIHsKICAgICAgICAgICAgImFjY2VzcyI6ICJwcm94eSIsCiAgICAgICAgICAgICJiYXNpY0F1dGgiOiB0cnVlLAogICAgICAgICAgICAiYmFzaWNBdXRoUGFzc3dvcmQiOiAiREJzSmVKeEFKU1NuSzJlL3BjaU5GblRPckFFeGdKVlJLZzEwbEdncWE4bnl5bytDVm54MFZWcVJ0TitHY3piRldoNjJHeTBicktoUXNhbHlCS01rUXZ3WW9NUzlmWWpqbmdaajBmbjFBcWczd0ZIZ2JFU3dqeG1JdWtwVDBvVVEvMzBkK1JLQkJnM3dIaC9mRVFhOXlUTkVtcmNGY09McWxjdDVieUE4aEcwdVUxblYxMWZnaVM2VG9TVm55NDVHaFRiSnNDNllEODlkWFFoaFlHVHpGanFvQkdiNE1EblRkZjFOZEZ3UkhjRlJ1VVpmbTFMQi82QlJDNVVIa1RldVR0c0MwMHYrZlJWeVlnREEvblQwYmNUR2JlQ3NzUDY0QmxVSXZCTVNSVzNVZGt4UDJaUWg5QWNsMVFnOUFpbit5eWxpRUtXcDVJRnVsYmJ1ZCtvcSIsCiAgICAgICAgICAgICJiYXNpY0F1dGhVc2VyIjogImludGVybmFsIiwKICAgICAgICAgICAgImVkaXRhYmxlIjogZmFsc2UsCiAgICAgICAgICAgICJqc29uRGF0YSI6IHsKICAgICAgICAgICAgICAgICJ0bHNTa2lwVmVyaWZ5IjogdHJ1ZQogICAgICAgICAgICB9LAogICAgICAgICAgICAibmFtZSI6ICJwcm9tZXRoZXVzIiwKICAgICAgICAgICAgIm9yZ0lkIjogMSwKICAgICAgICAgICAgInR5cGUiOiAicHJvbWV0aGV1cyIsCiAgICAgICAgICAgICJ1cmwiOiAiaHR0cHM6Ly9wcm9tZXRoZXVzLWs4cy5vcGVuc2hpZnQtbW9uaXRvcmluZy5zdmM6OTA5MSIsCiAgICAgICAgICAgICJ2ZXJzaW9uIjogMQogICAgICAgIH0KICAgIF0KfQ==";
		System.out.println(new String(java.util.Base64.getDecoder().decode(s)));
	}
	@org.junit.Test
	public void base64encode()
	{
		String s;
		try {
			s = fromFile("C:\\workspace_clovir\\clovircm_sys\\src\\test\\grafana_datasource.json").toString(4);
			System.out.println(s);
			System.out.println(new String(java.util.Base64.getEncoder().encode(s.getBytes())));
		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void maxName()
	{
		String t = "volume-951si";
		
				
	}
	
	private static JSONObject fromFile(String filePath) throws FileNotFoundException
	{
		JSONTokener tokener = new JSONTokener( new FileReader(filePath));
		return (new JSONObject(tokener)) ;
	}
	private static JSONObject fromStr(String str) throws FileNotFoundException
	{
		JSONTokener tokener = new JSONTokener(str);
		return (new JSONObject(tokener)) ;
	}
	public static void main(String[] args)
	{
		try {
			System.out.println(System.currentTimeMillis());
			System.out.println(new Date(1566265519000l));
			if(true) return;
			java.util.List<String> fields = new ArrayList();
			JSONTokener tokener = new JSONTokener( new FileReader("c:/workspace_git/clovirsm_hv/src/vra_request.json"));
			JSONObject myForm = (new JSONObject(tokener)) ;
			Iterator<String> it = myForm.keys();
			while(it.hasNext())
			{
				String myKey = it.next();
				JSONObject myItem = myForm.getJSONObject(myKey);
				if(!myItem.has("visible") || myItem.getBoolean("visible"))
				{
					int order = myItem.getInt("order");
					int len = fields.size();
					for(int i=0; i < order-len+1; i++)
					{
						fields.add("");
					}
					fields.set(order, myKey);
					
				}
			}
			System.out.println(fields);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static String getId(String path)
	{
		if( path.endsWith("']"))
		{
			int pos = path.lastIndexOf("['");
			return path.substring(pos+2,  path.length()-2);
		}
		else
		{
			int pos = path.lastIndexOf(".");
			return path.substring(pos+1);
		}
		
	}
	public static void main3(String[] args)
	{
		try {
			DocumentContext obj =JsonPath.parse(new File("c:/workspace_git/clovirsm_hv/src/test/clovirsm/state.json"));
			boolean result = (boolean) ((net.minidev.json.JSONArray)obj.read("$.facets[?(@.type=='editable')].value.value.value")).get(0);
			System.out.println(result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void main4(String[] args)
	{
		 
		float t=1.0f;
		int a = 1;
		System.out.println(t==a);
		if(true) return;
		String test="1G1s/MTAzNGhzaC00LjIkIA==";
		System.out.println( new String(Base64.decode(test)));
		if(true) return;
		try {
			 
			
			FormGen gen = new FormGen(fromFile("c:/workspace_git/clovirsm_hv/src/vra_request.json"));
			System.out.println(gen.formGen(fromFile("c:/workspace_git/clovirsm_hv/src/test/clovirsm/test.json"), fromFile("c:/workspace_git/clovirsm_hv/src/test/clovirsm/schema.json")));
			System.out.println(gen.getJspTemplateArr());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public static void main_put(String[] args)
	{
		 
		try {
			 
			
			FormGen gen = new FormGen(fromFile("c:/workspace_git/clovirsm_hv/src/vra_request.json"));
			
			System.out.println(gen.putVal(fromFile("c:/workspace_git/clovirsm_hv/src/test/clovirsm/test.json"), fromFile("c:/workspace_git/clovirsm_hv/src/test/clovirsm/userdata.json")));
			
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public static void main5(String[] args)
	{
		String str = "2018-11-06T09:17:00.000Z";
		try {
			MyPredicate booksWithISBN = new MyPredicate();
			//DocumentContext obj = JsonPath.parse(new File("c:/workspace_git/clovirsm_hv/src/test/clovirsm/test.json"));
			JSONTokener tokener = new JSONTokener( new FileReader("c:/workspace_git/clovirsm_hv/src/test/clovirsm/test.json"));
			JSONObject json = (new JSONObject(tokener)) ;
			DocumentContext obj = JsonPath.parse(json.toString());
			String path = "$.data.*.data.memory";
			System.out.println(obj.read(path));
			String[] valList = new   String[]{"1","2"};
			path = path.replaceAll("\\*", "*[?]");
			System.out.println(path);
			for(int i=0; i < valList.length; i++)
			{
				booksWithISBN.setidx(i);
				obj.set(path, valList[i], booksWithISBN);
				
			}
			 
			System.out.println(obj.jsonString());
			System.out.println(obj.read( "$.data.*.data.disks[0].data.capacity"));
		} catch ( Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
 
}

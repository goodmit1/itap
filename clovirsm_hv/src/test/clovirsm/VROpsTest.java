package test.clovirsm;

import java.io.FileWriter;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.vrops.VROpsAPI;
import com.clovirsm.hv.vmware.vrops.VROpsConnection; 

public class VROpsTest {

	public Map prepare() throws Exception {
		Map param = new HashMap();
		 
 
		param.put("SOURCE", "Local");
		param.put("CONN_URL", "https://vrops.goodmit.tech.kr/suite-api/api");
		param.put("CONN_USERID", "admin");
		param.put("CONN_PWD", "VMware1!");
		
		return param;
	}
	@org.junit.Test
	public void listDCHealth() throws Exception{
		VROpsAPI api = new VROpsAPI();
		Map param = prepare();
		System.out.println(api.getAllDCHeathInfo(param));
	}
	@org.junit.Test
	public void saveAllMetrics() throws Exception{
		
		VROpsAPI api = new VROpsAPI();
		Map param = prepare();
		VROpsConnection conn = (VROpsConnection) api.connect(param);
		JSONArray adapters = ((JSONObject) conn.getRestClient().get("/adapterkinds")).getJSONObject("ops:adapter-kinds").getJSONArray("ops:adapter-kind");
		System.out.println(adapters);
		JSONArray result = new JSONArray();
		for(int i=0; i < adapters.length(); i++) {
			JSONObject adapter = adapters.getJSONObject(i);
			String key = adapter.getString("key");
			System.out.println(key + "===============================");
			Object rk = adapter.get("ops:resourceKinds");
			JSONArray resources = null;
			if(rk instanceof JSONArray) {
				resources = adapter.getJSONArray("ops:resourceKinds");
			}
			else {
				resources = new JSONArray();
				resources.put((String)rk);
			}
			for(int j=0; j < resources.length(); j++) {
				System.out.println(URLEncoder.encode(resources.getString(j)).replace("+", "%20"));
				JSONArray statkeys = ((JSONObject)conn.getRestClient().get("/adapterkinds/" +  URLEncoder.encode(key).replace("+", "%20") + "/resourcekinds/"+ URLEncoder.encode(resources.getString(j)).replace("+", "%20") + "/statkeys")).getJSONObject("ops:resource-kind-attributes").getJSONArray("ops:resource-kind-attribute");
				for(int k=0; k < statkeys.length(); k++) {
					result.put(statkeys.getJSONObject(k));
				}
			}
		}
		FileWriter writer = new FileWriter("C:\\workspace_clovir\\clovirsm_hv\\src\\test\\clovirsm\\vrops_metrics.json");
		writer.write(result.toString(4));
		writer.close();
		
	}
	
	
	@org.junit.Test
	public void uicapacity() throws Exception {
		RestClient client = new RestClient("https://172.16.33.63/");
		String param = "mainAction=getDataCenters&currentComponentInfo=TODO&secureToken=8bc076b7-8b9f-4a08-80f8-ba9de0864f5d";
		System.out.println(client.post("/ui/capacityNew.action", param));
	}
	
	@org.junit.Test
	public void vropsLogin2() throws Exception {
		VROpsAPI api = new VROpsAPI();
		Map param = prepare();
		
		
		VROpsConnection conn = (VROpsConnection) api.connect(param);

		try {
			
		 System.out.println(conn.getRestClient().get("/resources?resourceKind=datacenter"));
			//System.out.println(api.getClient().get("/adapterkinds"));
			//System.out.println(api.getClient().get("/adapterkinds/APPOSUCP/resourcekinds"));
			//System.out.println(((JSONObject)api.getClient().get("/adapterkinds/VMWARE/resourcekinds/DataCenter/statkeys")).toString());
			//System.out.println(api.getClient().get("/adapterkinds/LogInsightAdapter/resourcekinds"));
			//System.out.println(api.getClient().get("/adapterkinds/LogInsightAdapter/resourcekinds/LoginSight%20Adapter%20Instance/statkeys"));
		 //System.out.println(api.getClient().get("/resources/e0636d4c-cd54-49e1-94ed-b8f55480c78a/statkeys"));
			//System.out.println(api.getClient().get("/resources/stats/latest?resourceId=3849244a-ec00-46b6-ac8e-9a5eb9ce6f15"));
			//System.out.println(api.getClient().get("/resources/3849244a-ec00-46b6-ac8e-9a5eb9ce6f15"));
			//System.out.println(api.getClient().get("/resources?name=Clovir-vROPs"));
					
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@org.junit.Test
	public void getUnderOverSize() throws Exception {
		VROpsAPI api = new VROpsAPI();
		Map param = prepare();
		System.out.println(api.getUnderOverSize(param,10));
	}
	@org.junit.Test
	public void metricData() throws Exception {
		VROpsAPI api = new VROpsAPI();
		Map param = prepare();
		
		
		 

		try {
			//System.out.println(api.getUnderOverSize(30));
			Calendar begin = Calendar.getInstance();
			begin.add(Calendar.DATE, -30);
			 
			System.out.println(api.getMetricData(param, "9ea985c4-7c00-45b4-bc20-21b71131d675", "net|usage_average", "DAYS", begin.getTimeInMillis(), System.currentTimeMillis()));
					
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

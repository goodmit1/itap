


package test.clovirsm;
 
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.Test;

import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.vra.VRAAPI;
import com.clovirsm.hv.vmware.vra.v8.VListCatalog8;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

public class VRATest {

	Map prepare()
	{
		Map  param = new HashMap();
		param.put("CONN_URL", "https://172.16.33.210");
		param.put("CONN_PWD", "VMware1!");
		param.put("CONN_USERID", "configurationadmin");
		param.put("tenant","vsphere.local");
		return param;
		
	}
	@Test
	public void testDelete()
	{
		VRAAPI api = new VRAAPI();
		Map param = prepare();
		param.put("VRA_REQUEST_ID","a1fc33cb-9892-49ba-aae0-4486a2817bf9");
		try {
			api.requestDelete(param);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	 
	//https://api.mgmt.cloud.vmware.com/provisioning/access-token
	@org.junit.Test
	public void vra8Login() throws Exception {
		RestClient client = new RestClient("https://console.cloud.vmware.com/");
		String param = "refresh_token=nnBVnTHzWZjKugopToKvENKOWdY3PCEG26nThMTWmVN45raJEPmSJFq8iKu2jgSn";
		client.setMimeType("application/x-www-form-urlencoded");
		System.out.println(client.post("/csp/gateway/am/api/auth/api-tokens/authorize", param));
		
	}
	
	@org.junit.Test
	public void vraProject() throws Exception {
		RestClient client = new RestClient("https://api.mgmt.cloud.vmware.com");
		client.setMimeType("application/json");
		client.setToken("eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InNpZ25pbmdfMiJ9.eyJzdWIiOiJ2bXdhcmVpZDpiOTYwNzZiOC01NWMyLTQ2ZWMtYmFiYy04NmU5ZDdlNmNmOGEiLCJhenAiOiJjc3BfcHJkX2dhel9pbnRlcm5hbF9jbGllbnRfaWQiLCJkb21haW4iOiJ2bXdhcmVpZCIsImNvbnRleHQiOiI1NTE4MTE1MC0zZWYyLTQxNzQtOTdkMi0xYTRmMDEyNDkzOGYiLCJpc3MiOiJodHRwczovL2dhei5jc3AtdmlkbS1wcm9kLmNvbSIsInBlcm1zIjpbImNzcDpvcmdfb3duZXIiLCJleHRlcm5hbC9aeTkyNG1FM2R3bjJBU3lWWlIwTm43bHVwZUFfL2F1dG9tYXRpb25zZXJ2aWNlOnVzZXIiLCJleHRlcm5hbC9aeTkyNG1FM2R3bjJBU3lWWlIwTm43bHVwZUFfL2F1dG9tYXRpb25zZXJ2aWNlOmNsb3VkX2FkbWluIl0sImNvbnRleHRfbmFtZSI6ImEyZDgxMTcxLWJkZGEtNDQ0ZS04MjY5LWRjZjZmMGQ1YzNmOSIsImV4cCI6MTU2NzczMTIzNiwiaWF0IjoxNTY3NzI5NDM2LCJqdGkiOiIyOTBjZjE0Yy00NWIwLTRjMmMtYWRlYi1iN2Q1ZmU4ZmZkYjAiLCJhY2N0Ijoic3lrJTQwZ29vZG1pdC5jby5rckB2bXdhcmVpZCIsInVzZXJuYW1lIjoic3lrQGdvb2RtaXQuY28ua3IifQ.cOsnK4-ed7Yj5KSG8KeBb01t2MOtGUEjOKmmDuPuzE9X5KOCHrX6CTNzrrMWX75Z6a2T9D5Vy-l7j-gmFib6BAN4nO1yDfEGj2wjiJCNVfhhYlwwXsZrlQCps6nk3gm0bVkPOue27UGoSfPwYJSJFNAAoAStkenwL2SzPuoPTQlk3uXa3xQnW2VL9SodPC-egRDV6XQB2RJQOksiPiBu7Np4iNIFWg1TkgyMuA_IhCM-36mKUqy_-XgGPPZaLWQCOMaBAVkA_qIWHgWSnnKrY0BergumsyNU-JYiOzbefb4g2PiHO4yBLifdxfE9umlDAL8ssbT6D17Fl2SjbpZP1Q");
		//System.out.println(client.get("/project-service/api/projects"));
		System.out.println(client.get("/blueprint/api/blueprints"));
	}
	
	
	@Test
	public void testDate() throws ParseException {
		String date = "2019-08-22T09:27:36.330Z";
	    DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	    Date d = utcFormat.parse(date);
		 
		System.out.println(d);
	}
	
	@Test
	public void parseParam() {
		 
		
		try {
			JSONObject json = new JSONObject(new JSONTokener( new FileInputStream("C:\\workspace_clovir\\clovirsm_hv\\src\\test\\clovirsm\\vra8_1.yaml")));
			VListCatalog8 list = new VListCatalog8(null);
			//Map steps = list.parseParam(json);
			//System.out.println(steps.keySet() + ":" + steps);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

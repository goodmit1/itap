package test.clovirsm;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.clovirsm.hv.vmware.impl.VAddPermission;
import com.clovirsm.hv.vmware.impl.VCreateFolder;
import com.clovirsm.hv.vmware.impl.VCreateResourcePool;
import com.clovirsm.hv.vmware.impl.VGuestRun;
import com.clovirsm.hv.vmware.impl.VInfoVM;
import com.clovirsm.hv.vmware.impl.VListPerfHistory;
import com.clovirsm.hv.vmware.impl.VOpenConsole;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfAuthorizationRole;
import com.vmware.vim25.ArrayOfTag;
import com.vmware.vim25.AuthorizationRole;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.PerfMetricId;
import com.vmware.vim25.PerfQuerySpec;
import com.vmware.vim25.Tag;
import com.vmware.vim25.TaskInfo;
import com.vmware.vim25.TaskInfoState;

public class VMWare {

	protected Map prepare()
	{
		Map map = new HashMap();
		 
		map.put(HypervisorAPI.PARAM_URL, "https://172.16.33.31/sdk");
		//map.put(HypervisorAPI.PARAM_URL, "https://10.1.20.11/sdk");
		map.put(HypervisorAPI.PARAM_USERID, "root");
		map.put(HypervisorAPI.PARAM_PWD, ")*)@GITtjqltm09");
		//map.put(HypervisorAPI.PARAM_USERID, "parkwy");
		//map.put(HypervisorAPI.PARAM_PWD, "qazxsw1234!@");
		map.put(VMWareAPI.PARAM_REAL_DC_NM, "DAOU_IDC");
		map.put(VMWareAPI.PARAM_FOLDER_NM, "");
		map.put(VMWareAPI.PARAM_CLUSTER, "Cluster");
		map.put(VMWareAPI.PARAM_RESOURCEPOOL_NM, "rp2");
		System.setProperty("java.util.logging.config.file","logging.properties");
		return map;
	}
	protected Map prepare1()
	{
		Map map = new HashMap();
		 
		map.put(HypervisorAPI.PARAM_URL, "https://10.150.0.10/sdk");
		//map.put(HypervisorAPI.PARAM_URL, "https://10.1.20.11/sdk");
		map.put(HypervisorAPI.PARAM_USERID, "administrator@vsphere.local");
		map.put(HypervisorAPI.PARAM_PWD, "Goodmit1!");
		//map.put(HypervisorAPI.PARAM_USERID, "parkwy");
		//map.put(HypervisorAPI.PARAM_PWD, "qazxsw1234!@");
		map.put(VMWareAPI.PARAM_REAL_DC_NM, "goodcs");
		map.put(VMWareAPI.PARAM_FOLDER_NM, "");
		map.put(VMWareAPI.PARAM_CLUSTER, "Cluster");
		map.put(VMWareAPI.PARAM_RESOURCEPOOL_NM, "rp2");
		System.setProperty("java.util.logging.config.file","logging.properties");
		return map;
	}
	@Test
	public void getNWInfo() throws Exception
	{
		Map map =  prepare();
		
			VMWareAPI api = new VMWareAPI();
		 
			ConnectionMng conn = api.connect(map);
			CommonAction action = new CommonAction(conn) {
				
				@Override
				protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result) throws Exception {
					ManagedObjectReference obj = super.makeManagedObjectReference("Network", "network-27");
					System.out.println(super.getProp(obj, "type"));
					return null;
				}
			};
			Map param = new HashMap();
			action.run(param, param);
			
	}
	@Test 
	public void listObj() throws Exception{
		Map map =  prepare1();
		
		VMWareAPI api = new VMWareAPI();
	 
		ConnectionMng conn = api.connect(map);
		CommonAction action = new CommonAction(conn) {
			
			@Override
			protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result) throws Exception {
				
				ManagedObjectReference dcmor = super.getDataCenter(dc);
				Map<String, ManagedObjectReference> list = super.findObjectList(dcmor, "Datastore");
				for(String k: list.keySet()) {
					 
					Map m = super.getProps(list.get(k), "name","parent" );
					System.out.println(m + ":" + ((ManagedObjectReference)m.get("parent")).getType());
				}
				return null;
			}
		};
		 
		action.run(map, map);
	}
	@Test 
	public void listTag() throws Exception{
		Map map =  prepare1();
		
		VMWareAPI api = new VMWareAPI();
	 
		ConnectionMng conn = api.connect(map);
		CommonAction action = new CommonAction(conn) {
			
			@Override
			protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result) throws Exception {
				
				ManagedObjectReference dcmor = super.getDataCenter(dc);
				ManagedObjectReference vm = super.findObject(dcmor , "VirtualMachine", "Centos-MJ");
				 
				List<Tag> tags  = ((ArrayOfTag) super.getProp(vm, "tag")).getTag();
				System.out.println(tags);
				return null;
			}
		};
		 
		action.run(map, map);
	}
	@Test
	public void getTaskInfo()
	{
		VMWareAPI api = new VMWareAPI();
		Map map =  prepare();
		try {
			ConnectionMng conn = api.connect(map);
			CommonAction action = new CommonAction(conn) {

				@Override
				protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
						throws Exception {
					ManagedObjectReference task = super.makeManagedObjectReference("Task", "task-42135");
					TaskInfo taskInfo = (TaskInfo)super.getProp(task, "info");
					System.out.println("success" + taskInfo.getState().equals(TaskInfoState.SUCCESS));
					System.out.println("err" + taskInfo.getError().toString());
					return null;
				}
				
			};
			action.run(map, map);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*@Test
	public void onFirstVM() {
		VMWareAPI api = new VMWareAPI();
		Map map = new HashMap();
		map.put(HypervisorAPI.PARAM_ORG_ID, 11);
		map.put(HypervisorAPI.PARAM_URL, "https://demo-vcenter.ad.1aopen.com/sdk");
		map.put(HypervisorAPI.PARAM_USERID, "portal");
		map.put(VMWareAPI.PARAM_PWD, "P@ssw0rd");
		map.put(VMWareAPI.PARAM_DC, "DC");
		map.put(VMWareAPI.PARAM_CLUSTER, "Cluster");
		map.put(VMWareAPI.PARAM_VIRTUALSWITCH_ID, "dvSwitch");
		 
		try {
			api.onFirstVM(map);
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}*/
	 
	@Test
	public void onDeleteOrg() {
		HypervisorAPI api = new VMWareAPI();
		Map map = prepare();
 
		map.put(VMWareAPI.PARAM_PORT_GROUP_NM, "dvpg11");
		map.put(VMWareAPI.PARAM_RESOURCEPOOL_NM, "rp11");
		try {
			api.deleteOrg(map);
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}
	@Test
	public void onMoveVMFolder() throws Exception {
		HypervisorAPI api = new VMWareAPI();
		Map map =  prepare1();
		map.put(VMWareAPI.PARAM_FOLDER_NM, "Deleted");
		
		map.put(HypervisorAPI.PARAM_VM_NM, "m16nulldb001dev");
		VMInfo vmInfo = new VMInfo();
		vmInfo.hostName="m16nulldb001dev";
		api.moveVMinfoFolder(map, vmInfo);
	}
	
	@Test
	public void onCreateVM() {
		HypervisorAPI api = new VMWareAPI();
		Map map =  prepare();
		map.put(VMWareAPI.PARAM_PORT_GROUP_NM, "dvpg2");
		map.put(VMWareAPI.PARAM_RESOURCEPOOL_NM, "rp2");
		map.put(VMWareAPI.PARAM_FOLDER_NM, "org2");
		map.put(HypervisorAPI.PARAM_VM_NM, "syk11");
		map.put(VMWareAPI.PARAM_DATASTORE_NM_LIST, new String[]{"1aopen-iscsi2","vsanDatastore"}); //GB
		map.put(HypervisorAPI.PARAM_CPU,"1");
		map.put(HypervisorAPI.PARAM_MEM,"1"); //GB
		map.put(HypervisorAPI.PARAM_DISK,"2"); //GB
		map.put(HypervisorAPI.PARAM_FROM,"base_template/centos_temp"); //GB
		map.put("PRIVATE_IP", "10.1.1.34");
		map.put("GW_IP", "10.1.1.33");
		map.put("SUBNET_MASK", "255.255.255.224");
		
		try {
			api.createVM(null, false, map);
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}
	 
	@Test
	public void onPowerOnVM() {
		HypervisorAPI api = new VMWareAPI();
		Map map =  prepare();
	 
		 
		 
		try {
			VMInfo vmInfo = new VMInfo();
			vmInfo.hostName="syk11";
			api.powerOpVM(map,vmInfo , HypervisorAPI.POWER_ON);
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}
	@Test
	public void onReconfigVM() {
		HypervisorAPI api = new VMWareAPI();
		Map map =  prepare();
	 
		map.put(HypervisorAPI.PARAM_VM_NM, "temp-vm");
		map.put(VMWareAPI.PARAM_DATASTORE_NM,"1aopen-iscsi1"); //GB
		map.put(HypervisorAPI.PARAM_CPU,"2");
		map.put(HypervisorAPI.PARAM_MEM,"8"); //GB
		map.put(HypervisorAPI.PARAM_DISK,"45"); //GB 
		try {
			VMInfo oldInfo = new VMInfo();
			oldInfo.hostName = "temp-vm";
			api.reconfigVM(map, oldInfo );
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}
	/*@Test
	public void roleList()
	{
		VMWareAPI api = new VMWareAPI();
		Map map = prepare();
		ConnectionMng connectionMgr  = null;
		map.put(VMWareAPI.PARAM_VM_NM, "fsec-scenario-rudy_analyzer_windows7_x86");
		map.put(VMWareAPI.PARAM_OBJECT_TYPE,  VMWareCommon.OBJ_TYPE_VM);
		try {
			connectionMgr = api.connect(map);
			ManagedObjectReference account = connectionMgr.getServiceContent().getAccountManager();
			if(account==null)
			{
				throw new Exception("account is null");
			}
			ManagedObjectReference mng = connectionMgr.getServiceContent().getAuthorizationManager();
			ArrayOfAuthorizationRole list1 = (ArrayOfAuthorizationRole) connectionMgr.getMOREF().entityProps(mng,new String[]{"roleList"}).get("roleList");
			List<AuthorizationRole> list = list1.getAuthorizationRole();
			for(AuthorizationRole r : list)
			{
				System.out.println(r.getRoleId() + ":" + r.getName());
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			connectionMgr.disconnect();
		}
			
	}*/
	
	@Test
	public void guestRun() throws Exception
	{
		Map map = prepare();
		VMWareAPI api = new VMWareAPI();
		ConnectionMng connectionMgr  = api.connect(map);
		map.put("GUEST_ID","sysportal");
		map.put("GUEST_PWD","vmware1!");
		//map.put("GUEST_ID","Administrator");

		map.put("VM_NM","Clovir-SM_Template");
		Map result = new HashMap();
		//map.put(VMWareAPI.PARAM_VM_HV_ID,"vm-115");
		VGuestRun run = new VGuestRun(connectionMgr);
		map.put("PROGRAM_PATH","/home/sysportal/portal_cmd.sh");
		map.put("PROGRAM_ARG", "adduser test1");
		//map.put("PROGRAM_PATH","c:\\PerfLogs\\portal_cmd.bat");
		//map.put("PROGRAM_PATH","/usr/bin/id");
		//map.put("PROGRAM_ARG", "test1");
		//run.run(map, result);
		run.run(map, result);
		System.out.println(result);
	}
	@Test
	public void guestwinRun() throws Exception
	{
		Map map = prepare1();
		VMWareAPI api = new VMWareAPI();
		ConnectionMng connectionMgr  = api.connect(map);
		map.put("GUEST_ID","sysportal");
		map.put("GUEST_PWD","vmware1!");
		//map.put("GUEST_ID","Administrator");

		map.put("VM_NM","GiTech-ClovirSM");
		Map result = new HashMap();
		//map.put(VMWareAPI.PARAM_VM_HV_ID,"vm-115");
		VGuestRun run = new VGuestRun(connectionMgr);
		//map.put("PROGRAM_PATH","/home/sysportal/portal_cmd.sh");
		//map.put("PROGRAM_ARG", "adduser test1");
		map.put("PROGRAM_PATH","c:\\PerfLogs\\portal_cmd.bat");
		map.put("PROGRAM_ARG", "chkuser sysportal");
		//map.put("PROGRAM_PATH","‪C:\\Windows\\SysWOW64\\wbem\\WMIC.exe");
		//map.put("PROGRAM_ARG", "useraccount where name='sysportal' get sid");
		//run.run(map, result);
		run.run(map, result);
		System.out.println(result);
	}
	@Test
	public void vmInfo()
	{
		VMWareAPI api = new VMWareAPI();
		Map map = prepare1();
		ConnectionMng connectionMgr  = null;
		map.put(VMWareAPI.PARAM_VM_NM, "goodcs-gitlab");
		map.put(VMWareAPI.PARAM_START_DT, "20201011");
		map.put(VMWareAPI.PARAM_FINISH_DT, "20201017");
		map.put(VMWareAPI.PARAM_OBJECT_TYPE,  VMWareCommon.OBJ_TYPE_VM);
		try {
			connectionMgr = api.connect(map);
			map.put("ALL_YN", "Y");
			VInfoVM vmaction = new VInfoVM(connectionMgr);
			Map result = new HashMap();
			vmaction.run(map, result);
			System.out.println(result);
			/*map.put(VMWareAPI.PARAM_OBJECT_TYPE,VMWareAPI.OBJ_TYPE_HOST);
			map.put(VMWareAPI.PARAM_OBJECT_NM, "10.1.20.13");
			System.out.println("Host " + map.get(VMWareAPI.PARAM_OBJECT_NM) + "="+ api.listPerf(map));
			map.put(VMWareAPI.PARAM_OBJECT_NM, "10.1.20.11");
			System.out.println("Host " + map.get(VMWareAPI.PARAM_OBJECT_NM) + "="+ api.listPerf(map));
			map.put(VMWareAPI.PARAM_OBJECT_NM, "10.1.20.12");
			System.out.println("Host " + map.get(VMWareAPI.PARAM_OBJECT_NM) + "="+ api.listPerf(map));
			*/
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			connectionMgr.disconnect();
		}
	}
	@Test
	public void listPerf()
	{
		VMWareAPI api = new VMWareAPI();
		Map map = prepare1();
		map.put(VMWareAPI.PARAM_PERIOD, VMWareAPI.PERF_PERIOD_1W);
		map.put(VMWareAPI.PARAM_VM_NM, "goodcs4-master-0");
		map.put(VMWareCommon.PARAM_INTERVAL,1800);
		Date now = new Date();
		now.setDate(now.getDate()-200);
		map.put(VMWareAPI.PARAM_START_DT, now);
		Date now1 = new Date();
		now1.setDate(now1.getDate()-1);
		 
		map.put(VMWareAPI.PARAM_FINISH_DT, now1);
		map.put(VMWareAPI.PARAM_OBJECT_TYPE,  VMWareCommon.OBJ_TYPE_VM);
		ConnectionMng connectionMgr  = null;
		
		
		try {
			connectionMgr = api.connect(map);
			VListPerfHistory perf = new VListPerfHistory(connectionMgr) {
				Map<Integer, String> getPerfIds(PerfQuerySpec qSpec,String objType, String[] perfIds) throws Exception
				{
					
					String[] ids = perfIds;
					 
					List<PerfMetricId> list = qSpec.getMetricId();
					
					int idx=0;
					Map<Integer, String> result = new HashMap();
					 
					PerfMetricId m = new PerfMetricId();
					m.setCounterId(4);
					result.put(m.getCounterId(), "cpu_usage_max");
					list.add(m);
					 
					
					PerfMetricId m2 = new PerfMetricId();
					m2.setCounterId(2);
					result.put(m2.getCounterId(), "cpu_usage");
					list.add(m2);
					return result;
				}
			} ;
			perf.run(map, map);
			System.out.println(map.get("LIST"));
			if(((List)map.get("LIST")).size()>0) {
				Map result = (Map)((List)map.get("LIST")).get(0);
				List<Map> cpu = (List)result.get("cpu_usage");
				List<Map> cpu_usage_max = (List)result.get("cpu_usage_max");
			 
				LinkedHashMap<Long, Object > dates = new LinkedHashMap();
				for(Map m : cpu) {
					//dates.put( (Long)m.get("CRE_TIME"), m.get("VAL"));
					System.out.println(new Date((Long)m.get("CRE_TIME")) + "= avg:" + m.get("VAL"));
					
				}
				if(cpu_usage_max != null) {
					for(Map m : cpu_usage_max) {
						Object u = dates.get((Long)m.get("CRE_TIME"));
						if(u != null) {
							System.out.println(new Date((Long)m.get("CRE_TIME")) + "= avg:" + u  + ", max:" + m.get("VAL"));
						}
					}
				}
			}
			/*map.put(VMWareAPI.PARAM_OBJECT_TYPE,VMWareAPI.OBJ_TYPE_HOST);
			map.put(VMWareAPI.PARAM_OBJECT_NM, "10.1.20.13");
			System.out.println("Host " + map.get(VMWareAPI.PARAM_OBJECT_NM) + "="+ api.listPerf(map));
			map.put(VMWareAPI.PARAM_OBJECT_NM, "10.1.20.11");
			System.out.println("Host " + map.get(VMWareAPI.PARAM_OBJECT_NM) + "="+ api.listPerf(map));
			map.put(VMWareAPI.PARAM_OBJECT_NM, "10.1.20.12");
			System.out.println("Host " + map.get(VMWareAPI.PARAM_OBJECT_NM) + "="+ api.listPerf(map));
			*/
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			connectionMgr.disconnect();
		}
	}
	@Test
	public void listLicense()
	{
		VMWareAPI api = new VMWareAPI();
		Map map = prepare();
		 
		try {
			System.out.println(api.listLicense(map));
			/*map.put(VMWareAPI.PARAM_OBJECT_TYPE,VMWareAPI.OBJ_TYPE_HOST);
			map.put(VMWareAPI.PARAM_OBJECT_NM, "10.1.20.13");
			System.out.println("Host " + map.get(VMWareAPI.PARAM_OBJECT_NM) + "="+ api.listPerf(map));
			map.put(VMWareAPI.PARAM_OBJECT_NM, "10.1.20.11");
			System.out.println("Host " + map.get(VMWareAPI.PARAM_OBJECT_NM) + "="+ api.listPerf(map));
			map.put(VMWareAPI.PARAM_OBJECT_NM, "10.1.20.12");
			System.out.println("Host " + map.get(VMWareAPI.PARAM_OBJECT_NM) + "="+ api.listPerf(map));
			*/
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void listHost()
	{
		VMWareAPI api = new VMWareAPI();
		Map map = prepare();
		 
		try {
			System.out.println(api.getHostAttribute(map));
			/*map.put(VMWareAPI.PARAM_OBJECT_TYPE,VMWareAPI.OBJ_TYPE_HOST);
			map.put(VMWareAPI.PARAM_OBJECT_NM, "10.1.20.13");
			System.out.println("Host " + map.get(VMWareAPI.PARAM_OBJECT_NM) + "="+ api.listPerf(map));
			map.put(VMWareAPI.PARAM_OBJECT_NM, "10.1.20.11");
			System.out.println("Host " + map.get(VMWareAPI.PARAM_OBJECT_NM) + "="+ api.listPerf(map));
			map.put(VMWareAPI.PARAM_OBJECT_NM, "10.1.20.12");
			System.out.println("Host " + map.get(VMWareAPI.PARAM_OBJECT_NM) + "="+ api.listPerf(map));
			*/
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void onDeleteVM() {
		HypervisorAPI api = new VMWareAPI();
		Map map = prepare();
	 
		map.put(HypervisorAPI.PARAM_VM_NM, "syk11");
	 
		try {
			api.deleteVM(map , null);
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}
}


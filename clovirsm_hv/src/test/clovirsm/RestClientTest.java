package test.clovirsm;

import java.io.File;
import java.io.FileWriter;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.vra.VRAAPI;
import com.clovirsm.hv.vmware.vrops.VROpsAPI;

public class RestClientTest {

	@org.junit.Test
	public void grafana()
	{
		
		try {
			RestClient client = new RestClient("https://grafana-openshift-monitoring.apps.kbos.local" );
			client.setMimeType("application/json");
			//client.setToken("eyJrIjoiVmhsS09VY3ZWOTZ3aktaRGVtUnNMYzFVZjhHVGNxTWoiLCJuIjoiYWRtaW4iLCJpZCI6MX0=");
			client.setToken("3ZPvKhbFtOsFm9Kde-IALhHcIebc3602bLViGwvYs0E");
			//Map  header = new HashMap();
			//header.put("X-Forwarded-User", "admin");
			//client.setHeader(header); 
			//System.out.println(client.get("/api/dashboards/home"));
			System.out.println(client.get("/api/datasources/proxy/1/api/v1/query_range?query=sum(namespace_pod_name_container_name%3Acontainer_cpu_usage_seconds_total%3Asum_rate%7Bnamespace%3D%22tomcat8%22%2C%20pod_name%3D%22tomcat8-12-jjjnq%22%2C%20container_name!%3D%22POD%22%7D)%20by%20(container_name)&start=1562038140&end=1562041770&step=30"));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@org.junit.Test 
	public void replace() {
		System.out.println("a|b".replace("|", "_"));
		
	}
	@org.junit.Test 
	public void dateTest() throws Exception{
		System.out.println(URLEncoder.encode("a b","utf-8"));
		Date date = new Date(1566431999999l);
		System.out.println(date);
	}
	
	@org.junit.Test 
	public void dateTest2() throws Exception{
		Date date = new Date("Friday, August 23, 2019 12:45:09 PM UTC");
		System.out.println(date);
	}
	
	@org.junit.Test 
	public void search() throws Exception{
		RestClient client = new RestClient("http://115.71.42.53:9200");
		client.setMimeType("application/json");
		JSONObject timestamp = (new JSONObject()).put("gte", "2019-08-29T05:01:02.453Z");
		JSONObject json = new JSONObject();
		json.put("query", ( new JSONObject()).put("range", ( new JSONObject()).put("@timestamp", timestamp)));
		json.put("sort" , (new JSONArray()).put("@timestamp"));
		System.out.println(json.toString());
		 
		System.out.println(client.get("/logstash-2019.08.29-000001/_search?pretty", json.toString()));
	}
	
	@org.junit.Test
	public void match() throws Exception{
		String str = "10.2.1.10";
		System.out.println(str.matches("10.2.1"));
		System.out.println(str.matches("10.2.1.10"));
	}
}

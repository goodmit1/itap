package com.clovirsm.hv.vmware.sddc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.CommonAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.nsx.NSXConnection;

public class SDDCAPI extends CommonAPI {
	public static String getHVType()
	{
		return "V.SDDC";
	}
	protected  String getConnectionName() {
		return SDDCConnection.class.getName();
	}
	public Map listHostNode(Map param) throws Exception
	{
		SDDCConnection conn = (SDDCConnection) connect(param);
		try {
			JSONObject list = conn.getRestClient().getJSON("/v1/hosts?status=ASSIGNED");
			JSONArray results = list.getJSONArray("elements");
			List serverlist = new ArrayList();
			for(int i=0;  i < results.length(); i++) {
			 
				 
				if(results.getJSONObject(i).has("fqdn")) {
					serverlist.add( results.getJSONObject(i).getString("fqdn"));
				}
			}
			Map result = new HashMap();
			result.put(VMWareAPI.PARAM_LIST, serverlist);
			return result;
		}
		finally {
			this.disconnect(conn);
		}
		
	}
}

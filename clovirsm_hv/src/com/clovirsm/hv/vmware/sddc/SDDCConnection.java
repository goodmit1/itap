package com.clovirsm.hv.vmware.sddc;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.RestClient;

public class SDDCConnection implements IConnection{

	String url;
	Date expire;
	 
	RestClient client;
	@Override
	public String getURL() {
	 
		return url;
	}
	public RestClient getRestClient()
	{
		return client;
	}
	@Override
	public void connect(String url, String userId, String pwd, Map prop) throws Exception {
		this.url = url;
		
		client = new RestClient(url);
		client.setMimeType("application/json");
		JSONObject json = new JSONObject();
		json.put("username", userId);
		json.put("password", pwd);
		JSONObject result = (JSONObject)client.post("/v1/tokens", json.toString());
		if(!result.has("accessToken")) {
			throw new Exception("Login Fail");
		}
		client.setToken(result.getString("accessToken"));
		
		
		
		Calendar now = Calendar.getInstance();
		now.add(Calendar.MINUTE, 10);
		expire = now.getTime();
	}

	@Override
	public void disconnect() {
		client = null;
		
	}

	@Override
	public boolean isConnected() {
		 
		return client == null || expire.compareTo(new Date())>0;
	}

}

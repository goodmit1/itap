package com.clovirsm.hv.vmware.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.vmware.VCRestCommon;
import com.clovirsm.hv.vmware.VCRestConnection;

public class PutVMTags extends VCRestCommon {

	public PutVMTags(VCRestConnection conn) {
		super(conn);
		 
	}

	@Override
	public void run(Map param, Map result) throws Exception {
		JSONObject categoryList = client.getJSON("/com/vmware/cis/tagging/category");
		JSONArray list = categoryList.getJSONArray("value");
		Map categoryMap = new HashMap();
		 
		Map categoryNameMap = new HashMap();
		for(int i=0; i < list.length(); i++) {
			
			JSONObject category =  client.getJSON("/com/vmware/cis/tagging/category/id:" + list.getString(i));
			categoryMap.put(  list.getString(i),category.getJSONObject("value").getString("name"));
			categoryNameMap.put( category.getJSONObject("value").getString("name"), list.getString(i) );
		}
		JSONObject tagList = client.getJSON("/com/vmware/cis/tagging/tag");
		list = tagList.getJSONArray("value");
		Map<String , String> tagMap = new HashMap();
		for(int i=0; i < list.length(); i++) {
				
				JSONObject category =  client.getJSON("/com/vmware/cis/tagging/tag/id:" + list.getString(i));
			 
				tagMap.put( (String)categoryMap.get(category.getJSONObject("value").getString("category_id")) + "=" + category.getJSONObject("value").getString("name")  ,  list.getString(i));
		}
		Map<String, String> tags = (Map) param.get("TAGS");
		String vmId = (String)param.get("VM_HV_ID");
		for(String key  : tags.keySet()) {
			String tagId = tagMap.get( key + "=" +  tags.get(key) );
			 
			 
			if(tagId != null) {
				attach(vmId, tagId);
			}
			else {
				if(categoryNameMap.get(key) == null) {
					String categoryId = createCategory(key);
					 
					categoryNameMap.put(key, categoryId );
					tagId = createTag(categoryId, tags.get(key));
					 
				}
				else {
					tagId = createTag(categoryNameMap.get(key), tags.get(key));
				}
				attach(vmId, tagId);	
			}
		}
		
		
	}

	private void attach(String vmId, String tagId) throws Exception {
		// 
		JSONObject json = new JSONObject();
		json.put("object_id", (new JSONObject()).put("id", vmId).put("type", "VirtualMachine")).put("tag_id", tagId);
		client.post("/com/vmware/cis/tagging/tag-association/id:" + tagId  + "?~action=attach", json.toString());
		
	}
	private String createTag(Object categoryId, String value) throws Exception {
		/*
		 {
    "create_spec": {
        "category_id": "",
        "description": "",
        "name": "",
        "tag_id": ""
    }
}
		 */
		JSONObject param = new JSONObject();
		param.put("create_spec", (new JSONObject()).put("category_id", categoryId).put("tag_id", "").put("description", "").put("name", value));
		
		JSONObject res = (JSONObject)client.post("/com/vmware/cis/tagging/tag", param.toString());
		return res.getString("value");
	}

	private String createCategory(String key) throws Exception {
		/**
		 * {
    "create_spec": {
        "associable_types": [   ],
        "cardinality": "SINGLE",
        "category_id": "",
        "description": "",
        "name": "test"
    }
}
		 */
		JSONObject param = new JSONObject();
		param.put("create_spec", (new JSONObject()).put("associable_types", new JSONArray()).put("cardinality", "SINGLE").put("category_id", "").put("description", "").put("name", key));
		JSONObject res = (JSONObject)client.post("/com/vmware/cis/tagging/category", param.toString());
		return res.getString("value");
		
	}

}

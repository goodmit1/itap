package com.clovirsm.hv.vmware.impl;

import java.util.Arrays;
import java.util.Map;

import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

public class VMoveVMIntoFolder extends com.clovirsm.hv.vmware.CommonAction {

	public VMoveVMIntoFolder(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result) throws Exception {
		ManagedObjectReference vm = super.getVM(param);
		ManagedObjectReference folder = null;
		try {
			folder = super.findObjectFromRoot(VMWareAPI.OBJ_TYPE_FOLDER, (String)param.get(VMWareAPI.PARAM_FOLDER_NM));
		}
		catch(NotFoundException e) {
			VCreateFolder createFolder = new VCreateFolder(this.connectionMng);
			folder = createFolder.makeFolder(super.getDataCenter(dc),  (String)param.get(VMWareAPI.PARAM_FOLDER_NM));
		}
		if(folder != null && vm != null) {
			  ManagedObjectReference taskmor =
		               vimPort.moveIntoFolderTask(folder, Arrays.asList(vm));
		}
		return null;
	}

}

package com.clovirsm.hv.vmware.impl;

import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.FileFaultFaultMsg;
import com.vmware.vim25.InsufficientResourcesFaultFaultMsg;
import com.vmware.vim25.InvalidStateFaultMsg;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.RuntimeFaultFaultMsg;
import com.vmware.vim25.TaskInProgressFaultMsg;
import com.vmware.vim25.VmConfigFaultFaultMsg;

/**
 * VM 시작
 * @author 윤경
 *
 */
public class VPowerOnVM extends com.clovirsm.hv.vmware.CommonAction {

	public VPowerOnVM(ConnectionMng m) {
		super(m);

	}

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		ManagedObjectReference vmMor = powerop(super.getVM(param), param);
		onAfter(vmMor, result); 
		return null;

	}
	protected void onAfter(ManagedObjectReference vmMor,Map result) throws Exception
	{ 
	}
	protected ManagedObjectReference powerop(ManagedObjectReference vmMor, Map param) throws Exception {
	 
		if (vmMor != null) {
			param.put("VM_HV_ID", vmMor.getValue());
			 
			ManagedObjectReference taskmor =  process(vmMor);
			if (waiting && taskmor != null  ) {

				try
				{
					getTaskResultAfterDone(taskmor, "power operation : " + vmMor.getValue());
				}
				catch(Exception ignore)
				{
					//
				}
				 
			}
			
		}
		return vmMor;
	}

	 

	protected ManagedObjectReference process(ManagedObjectReference vmMor)
			throws Exception {
		return vimPort.powerOnVMTask(vmMor, null);
	}
	boolean waiting = true;
	public void setWaiting(boolean b) {
		this.waiting = b;
		
	}
}

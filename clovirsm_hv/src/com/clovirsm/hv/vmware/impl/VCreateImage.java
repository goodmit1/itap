package com.clovirsm.hv.vmware.impl;

import java.util.Map;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;
/**
 * 이미지 추가
 * @author 윤경
 *
 */
public class VCreateImage extends VCreateVM{

	public VCreateImage(ConnectionMng m) {
		super(m);

	}
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		this.param = param;
		int disk = CommonUtil.getInt(param.get(HypervisorAPI.PARAM_DISK));
		return this.createVM(true, result, dc, (String)param.get(VMWareAPI.PARAM_FOLDER_NM), null,
				(String)param.get(HypervisorAPI.PARAM_IMAGE_NM), -1, -1, disk,
				(String[])param.get(VMWareAPI.PARAM_IMG_DATASTORE_NM),
				null,
				(String)param.get(HypervisorAPI.PARAM_FROM), -1, null , null, param );

	}
}

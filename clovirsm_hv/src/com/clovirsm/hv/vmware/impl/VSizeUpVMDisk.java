package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.VirtualDevice;
import com.vmware.vim25.VirtualDeviceConfigSpec;
import com.vmware.vim25.VirtualDeviceConfigSpecOperation;
import com.vmware.vim25.VirtualDisk; 
import com.vmware.vim25.VirtualMachineConfigSpec;
import com.vmware.vim25.VirtualMachineRelocateSpec;

/**
 * VM 기본 디스크 사이즈 up
 * @author 윤경
 *
 */
public class VSizeUpVMDisk  extends VReconfigVM
{

	public VSizeUpVMDisk(ConnectionMng m) {
		super(m);
		 
	}
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		int cpu = CommonUtil.getInt(param.get(HypervisorAPI.PARAM_CPU));
		int mem = CommonUtil.getInt(param.get(HypervisorAPI.PARAM_MEM));
		int diskSize = CommonUtil.getInt(param.get(HypervisorAPI.PARAM_DISK));
		reconfig(true, super.getVM(param), cpu, mem, diskSize, param );
		return null;
	}

	 

}

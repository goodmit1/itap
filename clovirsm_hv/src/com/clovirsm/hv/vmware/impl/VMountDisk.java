package com.clovirsm.hv.vmware.impl;

import java.util.Map;

import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.VirtualDeviceConfigSpec;
/**
 * 디스크 마운트
 * @author 윤경
 *
 */
public class VMountDisk extends VCreateDisk{

	protected boolean isWaiting()
	{
		return true;
	}
	protected String getVMDiskFileName(Map param)
	{
		return (String)param.get(VMWareAPI.PARAM_DISK_PATH);
	}
	public VMountDisk(ConnectionMng m) {
		super(m);
		 
	}
	protected VirtualDeviceConfigSpec getDiskDeviceConfigSpec(  ManagedObjectReference virtualMachine,  String diskName, int diskSizeByte, String diskmode) throws Exception {
		return getDiskDeviceConfigSpec(false,  virtualMachine,  diskName,diskSizeByte,diskmode );
	}
}

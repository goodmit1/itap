package com.clovirsm.hv.vmware.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Map;

import com.clovirsm.hv.RestClient;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.FileTransferInformation;
import com.vmware.vim25.GuestFileAttributes;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.NamePasswordAuthentication;

public class VGuestFileDownload extends com.clovirsm.hv.vmware.CommonAction{
	protected NamePasswordAuthentication auth = new NamePasswordAuthentication();
	public VGuestFileDownload(ConnectionMng m) {
		super(m);

	}

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result) throws Exception {
		ManagedObjectReference vm = super.getVM(param);


		auth.setUsername((String)param.get("GUEST_ID"));
		auth.setPassword((String)param.get("GUEST_PWD"));
		String fromPath = ((String)param.get("FROM_PATH"));
		
		downloadFile(   vm, fromPath, param.get("TO_PATH"));
		return null; 
	}
	public void downloadFile(ManagedObjectReference vmRef, String filePathInGuest, Object filePathLocal)
			throws Exception
	{
		OutputStream out ;
		File file = null;
		if(filePathLocal instanceof String)
		{
			out = new FileOutputStream((String)filePathLocal);
			file = new File((String)filePathLocal);
		}
		else
		{
			out = (OutputStream)filePathLocal;
		}
		GuestFileAttributes fileAttr = downloadToStream(vmRef, filePathInGuest, out);
		out.close();
		if(file != null)
		{
			file.setLastModified(fileAttr.getModificationTime().toGregorianCalendar().getTimeInMillis());
		}
	}
	public GuestFileAttributes downloadToStream(ManagedObjectReference vmRef, String filePathInGuest, OutputStream out) throws Exception
	{
		if(filePathInGuest.indexOf("\\")<0 && isWindow(vmRef))
		{
			if(filePathInGuest.indexOf(":")<0) filePathInGuest = "c:" + filePathInGuest;
			filePathInGuest = filePathInGuest.replaceAll("/", "\\\\");
			
		}
		ManagedObjectReference mor = (ManagedObjectReference)super.getProp(super.connectionMng.getServiceContent().getGuestOperationsManager(), "fileManager");
		
		FileTransferInformation fileTranInfo = super.connectionMng.getVimPort().initiateFileTransferFromGuest(mor, vmRef, this.auth, filePathInGuest);
		RestClient client = new RestClient(fileTranInfo.getUrl());
		byte[] buf = (byte[])client.get("");
		out.write(buf);
		

		return fileTranInfo.getAttributes();
	}
	
	/*
	private static void readStream2Stream(InputStream from, OutputStream to) throws IOException
	{
		byte[] buf = new byte[4096];
		int len = 0;
		while ((len = from.read(buf)) > 0)
		{
			to.write(buf, 0, len);
		}
	}*/
}

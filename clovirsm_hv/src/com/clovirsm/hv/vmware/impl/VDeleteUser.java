package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.security.credstore.CredentialStore;
import com.vmware.security.credstore.CredentialStoreFactory;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.Permission;

public class VDeleteUser  extends CommonAction{

	public VDeleteUser(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result) throws Exception {
		ManagedObjectReference hostLocalAccountManager = null;
		String hostName = (String)param.get(VMWareAPI.PARAM_HOST_NM);
		ManagedObjectReference host = (ManagedObjectReference)super.findObjectFromRoot("HostSystem", hostName);
		hostLocalAccountManager = (ManagedObjectReference)super.getProp(host, "configManager.accountManager");
		
		String userName = VMWareAPI.GUEST_USER_ID_PREFIX +  param.get(VMWareAPI.PARAM_GUEST_USERID);
		vimPort.removeUser(hostLocalAccountManager, userName);
		CredentialStore csObj = CredentialStoreFactory.getCredentialStore();
		csObj.removePassword(hostName, userName);
		return null;
	}

}

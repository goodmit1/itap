package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorException;
import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfVirtualDevice;
import com.vmware.vim25.DVPortgroupConfigInfo;
import com.vmware.vim25.DistributedVirtualSwitchPortConnection;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.VMwareDVSConfigInfo;
import com.vmware.vim25.VirtualDevice;
import com.vmware.vim25.VirtualDeviceConfigSpec;
import com.vmware.vim25.VirtualDeviceConfigSpecOperation;
import com.vmware.vim25.VirtualEthernetCard;
import com.vmware.vim25.VirtualEthernetCardDistributedVirtualPortBackingInfo;
import com.vmware.vim25.VirtualEthernetCardNetworkBackingInfo;
import com.vmware.vim25.VirtualMachineConfigSpec;
import com.vmware.vim25.VirtualPCNet32;
import com.vmware.vim25.VirtualVmxnet3;

/**
 * 랜카드 삭제
 * @author 윤경
 *
 */
public class VDeleteNic extends CommonAction{

	public VDeleteNic(ConnectionMng m) {
		super(m);

	}
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param,
			Map result) throws Exception {
		deleteNic(super.getVM(param)
				  , CommonUtil.getInt(param.get(VMWareAPI.PARAM_NIC_ID)));
		return null;
	}
	private void deleteNic(ManagedObjectReference vmRef, int confKey) throws Exception
	{
		 
		VirtualDeviceConfigSpec nicSpec = new VirtualDeviceConfigSpec();
		VirtualEthernetCard nic = null;
		nicSpec.setOperation(VirtualDeviceConfigSpecOperation.REMOVE);
		List<VirtualDevice> hardwares = super.getHardwareDevice(vmRef);
		for (VirtualDevice device : hardwares) {
			if (device instanceof VirtualEthernetCard) {
				if (confKey == device.getKey()) {
					nic = (VirtualEthernetCard) device;
					break;
				}
			}
		}
		if (nic != null) {
			nicSpec.setDevice(nic);
			VirtualMachineConfigSpec vmConfigSpec = new VirtualMachineConfigSpec();

			List<VirtualDeviceConfigSpec> nicSpecArray =
					new ArrayList<VirtualDeviceConfigSpec>();
			nicSpecArray.add(nicSpec);
			vmConfigSpec.getDeviceChange().addAll(nicSpecArray);
			ManagedObjectReference task =
					vimPort.reconfigVMTask(vmRef, vmConfigSpec);
			getTaskResultAfterDone(task, "Delete NIC:" + vmRef.getValue() + "'" + confKey);
		} else {
			 

		}



		



	}
	

}

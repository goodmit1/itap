package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.security.credstore.CredentialStore;
import com.vmware.security.credstore.CredentialStoreFactory;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.Permission;
import com.vmware.vim25.TaskInfo;
import com.vmware.vim25.TaskInfoState;

public class VChkTask  extends CommonAction{

	public VChkTask(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result) throws Exception {
		String taskId = (String)param.get("TASK_ID");
		ManagedObjectReference task  = this.makeManagedObjectReference("Task", taskId);
		TaskInfo taskInfo = null;
		try
		{
			taskInfo = (TaskInfo)getProp(task, "info");
		}
		catch(Exception e)
		{
			System.out.println("task " + taskId + " is deleted");
			result.put("RESULT", true);
			return null;
		}
		
		if(taskInfo == null)
		{
			throw new Exception(taskId + " not found");
		}
		
		if(taskInfo.getState().equals(TaskInfoState.SUCCESS))
		{
			result.put("RESULT", true);
		}
		else if(taskInfo.getState().equals(TaskInfoState.ERROR))
		{
			throw new Exception(taskInfo.getError().getLocalizedMessage());
		}
		else
		{
			result.put("RESULT", false);
		}
		return null;
	}

}

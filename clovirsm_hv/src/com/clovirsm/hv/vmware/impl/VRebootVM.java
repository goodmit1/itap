package com.clovirsm.hv.vmware.impl;

import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

/**
 * VM OS reboot
 * @author 윤경
 *
 */
public class VRebootVM extends VPowerOffVM{

	public VRebootVM(ConnectionMng m) {
		super(m);
		 
	}
	protected ManagedObjectReference process(ManagedObjectReference vmMor)
			throws Exception {
		vimPort.rebootGuest(vmMor );
		return null;
	}
}

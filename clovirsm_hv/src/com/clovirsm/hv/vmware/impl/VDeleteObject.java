package com.clovirsm.hv.vmware.impl;

import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.HypervisorException;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

/**
 * 폴더 등 기타 오브젝트 삭제
 * @author 윤경
 *
 */
public class VDeleteObject  extends com.clovirsm.hv.vmware.CommonAction{

	public VDeleteObject(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		try
		{
			ManagedObjectReference obj = getObject(dc, param);
			deleteObject(obj);
		}
		catch(HypervisorException ignore)
		{
			System.out.println("NOT FOUND : " + (String)param.get(VMWareAPI.PARAM_OBJECT_TYPE) + "," + (String)param.get(VMWareAPI.PARAM_OBJECT_NM)+ param);
		}
		return null;
	}
	
	protected void deleteObject(ManagedObjectReference obj) throws Exception
	{
	 
		ManagedObjectReference taskmor = vimPort.destroyTask(obj);
		getTaskResultAfterDone(taskmor, "Delete " + obj.getType() + ":" + obj.getValue());
	}

}

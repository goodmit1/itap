package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.List;

import com.vmware.vim25.VirtualDevice;
import com.vmware.vim25.VirtualDeviceConfigSpec;
import com.vmware.vim25.VirtualDeviceConfigSpecOperation;
import com.vmware.vim25.VirtualDevicePciBusSlotInfo;
import com.vmware.vim25.VirtualPCIPassthrough;
import com.vmware.vim25.VirtualPCIPassthroughVmiopBackingInfo;

public class GPUConfig {
	int totalSize = 0;
	List<VirtualPCIPassthrough> gpuList = null;
	String model = null;
	int maxQ = 24;
	public GPUConfig(List<VirtualDevice> deviceList) {
		gpuList = new ArrayList();
		 
		for(int i=deviceList.size()-1; i>=0; i--){
			VirtualDevice d = deviceList.get(i);
			if(d instanceof VirtualPCIPassthrough ) {
				VirtualPCIPassthrough gpu = (VirtualPCIPassthrough)d;
				gpuList.add(gpu);
				totalSize += getSize(gpu);
				if(model == null) {
					model = getModel(gpu);
				}
			}
		}
		
	}
	public boolean addGPUChange(List deviceConfigSpec, int wantedSize) throws Exception{
		if(wantedSize == totalSize) return false;
		if(wantedSize <= maxQ) {
			int i=0;
			if(gpuList.size()>1) {
				for( i=0 ; i <  gpuList.size()-1; i++) {
					deviceConfigSpec.add(getDelConfig(gpuList.get(i)));
				}
			}
			deviceConfigSpec.add(getChgConfig(gpuList.get(i), wantedSize));
		}
		else {
			if(wantedSize % maxQ != 0) {
				throw new Exception("Invaild Multi GPU Size");
			}
			int cnt = wantedSize / maxQ;
			
			if(gpuList.size() < cnt) {
				if(totalSize<maxQ) {
					deviceConfigSpec.add(getChgConfig(gpuList.get(0), maxQ));
				}
				for(int i=gpuList.size(); i < cnt; i++) {
					deviceConfigSpec.add(getAddConfig(gpuList.get(0), maxQ));
				}
			}
			else if(gpuList.size() > cnt) {
				for(int i=0 ; i>  gpuList.size()-cnt; i++) {
					deviceConfigSpec.add(getDelConfig(gpuList.get(i)));
				}
			}
		}
		return true;
	}
	private VirtualDeviceConfigSpec getDelConfig(VirtualPCIPassthrough pci) {
		VirtualDeviceConfigSpec spec = new VirtualDeviceConfigSpec();
		spec.setDevice(pci);
		spec.setOperation(VirtualDeviceConfigSpecOperation.REMOVE);
		return spec;
	}
	private VirtualDeviceConfigSpec getChgConfig(VirtualPCIPassthrough pci, int size) {
		VirtualDeviceConfigSpec spec = new VirtualDeviceConfigSpec();
		VirtualPCIPassthroughVmiopBackingInfo backing = (VirtualPCIPassthroughVmiopBackingInfo)pci.getBacking();
		String gpuStr = backing.getVgpu();
		int pos = gpuStr.lastIndexOf("-");
		backing.setVgpu(gpuStr.substring(0, pos+1) + size + "q");
		spec.setDevice(pci);
		spec.setOperation(VirtualDeviceConfigSpecOperation.EDIT);
		return spec;
	}
	private VirtualDeviceConfigSpec getAddConfig(VirtualPCIPassthrough pci, int size) {
		VirtualDeviceConfigSpec spec = new VirtualDeviceConfigSpec();
		VirtualPCIPassthrough new_pci = new VirtualPCIPassthrough();
		VirtualPCIPassthroughVmiopBackingInfo new_backing = new VirtualPCIPassthroughVmiopBackingInfo();
		VirtualDevicePciBusSlotInfo new_slot = new VirtualDevicePciBusSlotInfo();
		new_pci.setBacking(new_backing);
		new_pci.setSlotInfo(new_slot);
		new_pci.setUnitNumber(pci.getUnitNumber()+1);
		new_pci.setKey(pci.getKey()+1);
		new_slot.setPciSlotNumber( ((VirtualDevicePciBusSlotInfo)pci.getSlotInfo() ).getPciSlotNumber()+1 );
		
		VirtualPCIPassthroughVmiopBackingInfo backing = (VirtualPCIPassthroughVmiopBackingInfo)pci.getBacking();
		String gpuStr = backing.getVgpu();
		int pos = gpuStr.lastIndexOf("-");
		new_backing.setVgpu(gpuStr.substring(0, pos+1) + size + "q");
		
		spec.setDevice(new_pci);
		spec.setOperation(VirtualDeviceConfigSpecOperation.ADD);
		return spec;
	}
	private String getModel(VirtualPCIPassthrough gpu) {
		VirtualPCIPassthroughVmiopBackingInfo backing = (VirtualPCIPassthroughVmiopBackingInfo)gpu.getBacking();
		String gpuStr = backing.getVgpu();
		int pos = gpuStr.lastIndexOf("-");
		int pos1 = gpuStr.lastIndexOf("_");
		return gpuStr.substring(pos1, pos);
	}
	private int getSize(VirtualPCIPassthrough gpu) {
		VirtualPCIPassthroughVmiopBackingInfo backing = (VirtualPCIPassthroughVmiopBackingInfo)gpu.getBacking();
		String gpuStr = backing.getVgpu();
		int pos = gpuStr.lastIndexOf("-");
		return Integer.parseInt(gpuStr.substring(pos+1, gpuStr.length()-1));
	}
}

package com.clovirsm.hv.vmware.impl;

import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

/**
 * 콘솔 열기
 * @author 윤경
 *
 */
public class VOpenConsole extends com.clovirsm.hv.vmware.CommonAction
{

	public VOpenConsole(ConnectionMng m) {
		super(m);
		 
	}
	public static void main(String[] args)
	{
		System.out.println(VOpenConsole.getServer("https://demo-vcenter.ad.1aopen.com"));
	}
	static String getServer(String url)
	{
		int pos = url.indexOf("//");
		int pos1 = url.indexOf("/", pos+3);
		if(pos1>0)
		{
			return url.substring(pos+2, pos1);
		}
		else
		{
			return url.substring(pos+2);
		}
	}
	protected String getConsoleUrl(ManagedObjectReference vmRef , String server )
			throws Exception {
		String ticket = vimPort.acquireCloneTicket(connectionMng.getServiceContent().getSessionManager());
		 
		String url = "vmrc://clone:" + ticket + "@" + getServer(server) + ":443/?moid=" + vmRef.getValue();
		return url;
	}


	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		String url = this.getConsoleUrl(super.getVM(param),(String) param.get(HypervisorAPI.PARAM_URL));
		result.put(HypervisorAPI.PARAM_CONSOLE_URL, url);
		return null;
		
	}

}

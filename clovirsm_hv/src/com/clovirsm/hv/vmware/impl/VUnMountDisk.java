package com.clovirsm.hv.vmware.impl;

import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.VirtualDeviceConfigSpec;

/**
 * 디스크 언마운트
 * @author 윤경
 *
 */
public class VUnMountDisk extends VDeleteDiskFromVM{

	public VUnMountDisk(ConnectionMng m) {
		super(m);
		 
	}
	protected VirtualDeviceConfigSpec getDiskDeviceConfigSpec(  ManagedObjectReference virtualMachine,   String diskName, int diskSizeByte, String diskmode) throws Exception {
		return getDiskDeviceConfigSpec(false, virtualMachine,   diskName,diskSizeByte,diskmode );
	}
}

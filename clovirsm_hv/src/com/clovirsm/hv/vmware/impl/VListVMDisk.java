package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfVirtualDevice;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.VirtualDevice;
import com.vmware.vim25.VirtualDeviceFileBackingInfo;
import com.vmware.vim25.VirtualDisk;

/**
 * VM 추가 디스크 목록
 * @author 윤경
 *
 */
public class VListVMDisk extends CommonAction{

	public VListVMDisk(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param,
			Map result) throws Exception {
		String folderNm = 	 (String)param.get(VMWareAPI.PARAM_FOLDER_NM); 
		String vmName = (String)param.get(VMWareAPI.PARAM_VM_NM); 
		List diskList =  getDisk(super.getVM(param));
		result.put(VMWareAPI.PARAM_LIST, diskList);
		return null;
	}

	public List getDisk(ManagedObjectReference virtualMachine) throws Exception
	{
		 List<VirtualDevice> deviceList =  ((ArrayOfVirtualDevice) getProp(virtualMachine,"config.hardware.device" )).getVirtualDevice();
		 List result = new ArrayList();
         for (VirtualDevice device : deviceList) {
         
         	//System.out.println(device.getDeviceInfo().getLabel() + ":" + device.getDeviceInfo().getSummary());
             if (device instanceof VirtualDisk) {
            	 VirtualDeviceFileBackingInfo backingInfo = (VirtualDeviceFileBackingInfo)device.getBacking();
             	Map m = new HashMap();
             	m.put(HypervisorAPI.PARAM_DISK_PATH, backingInfo.getFileName());
             	m.put(HypervisorAPI.PARAM_DISK,  ((VirtualDisk) device).getCapacityInKB()/1024/1024);
             	result.add(m);
             }
         }
         return result;
	}
}

package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfGuestNicInfo;
import com.vmware.vim25.ArrayOfTag;
import com.vmware.vim25.GuestNicInfo;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.Tag;
import com.vmware.vim25.VirtualMachinePowerState;

public class VListVMIpInDC extends VListVMInDC{

	public VListVMIpInDC(ConnectionMng m) {
		super(m);
		 
	}
	protected void addPerfInfo(Map param, ManagedObjectReference vm, Map newAttr) 
	{
		 
		VirtualMachinePowerState powerState = (VirtualMachinePowerState)newAttr.remove("POWERSTATE");
		if(powerState != null)
		{
			newAttr.put(VMWareAPI.PARAM_RUN_CD,powerState.value().equals("poweredOn") ? VMWareAPI.RUN_CD_RUN : VMWareAPI.RUN_CD_STOP);
		}
		List<GuestNicInfo> nicInfoArr = ((ArrayOfGuestNicInfo) newAttr.remove("NET")).getGuestNicInfo();
		List<Map> ipAddress = new ArrayList();
		for(GuestNicInfo nic:nicInfoArr)
		{
			List<String> ip = nic.getIpAddress();
			for(String ip1:ip)
			{
				if(ip1.indexOf(":")>0) continue;
				Map m  = new HashMap();
				m.put("IP", ip1);
				m.put("NIC_ID", nic.getDeviceConfigId());
				m.put("MAC_ADDR", nic.getMacAddress() ) ;
				ipAddress.add(m);
			}
		}
		newAttr.put("IP", ipAddress);
		/*ManagedObjectReference host = (ManagedObjectReference)newAttr.remove("HOST");
		if(host != null) {
			newAttr.put("HOST_HV_ID", host.getValue());
		}*/
		
		
	}
	 
	/*protected Map putCategory(ManagedObjectReference vm) throws Exception {
		String svcTagName = HVProperty.getInstance().getProperty("svc_tagName");
		
		if(svcTagName != null) {
			Map tagMap = new HashMap();
			List<Tag> tags  = ((ArrayOfTag) super.getProp(vm, "tag")).getTag();
			for(Tag tag : tags) {
				 tagMap.put(tag.g, value)
			}
		}
	}*/
	protected String[] attributes()
	{
		return new String[] {
				"summary.config.name" ,  "guest.net", "runtime.powerState" , "config.ftInfo.role"//, "runtime.host"
		};
		
		 
	}
}

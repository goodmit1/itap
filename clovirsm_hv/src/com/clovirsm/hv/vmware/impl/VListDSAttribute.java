package com.clovirsm.hv.vmware.impl;

import java.util.Map;

import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;


/**
 * 클러스터내 데이터 스토어 속성 정보 목록
 * @author 윤경
 *
 */
public class VListDSAttribute extends VListHostAttribute {

	 
	public VListDSAttribute(ConnectionMng m) {
		super(m);
		 
	}
	@Override
	protected void addPerfInfo(Map param, ManagedObjectReference host, Map newAttr) 
	{
		super.addPerfInfo(param,  host, newAttr);
		
		ManagedObjectReference parent = (ManagedObjectReference)newAttr.remove("PARENT");
		if(parent.getType().equals("StoragePod")) {
			try {
				String parentName = (String)super.getProp(parent, "name");
				if(parent != null) {
					newAttr.put("TYPE",newAttr.get("TYPE") + ":" + parentName);
				}
			} catch (Exception e) {
				 
				e.printStackTrace();
			}
		}
	}
	protected String getObjType()
	{
		return VMWareCommon.OBJ_TYPE_DS;
	}
	protected String[] attributes()
	{
		return new String[] {
				"summary.capacity","summary.type", "summary.freeSpace","summary.name","summary.uncommitted",
				"overallStatus", "vm", "parent"
		};
	}
}

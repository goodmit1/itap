package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfVirtualDevice;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.VirtualDevice;
import com.vmware.vim25.VirtualDeviceConfigSpec;
import com.vmware.vim25.VirtualDeviceConfigSpecFileOperation;
import com.vmware.vim25.VirtualDeviceConfigSpecOperation;
import com.vmware.vim25.VirtualDisk; 
import com.vmware.vim25.VirtualMachineConfigSpec;
import com.vmware.vim25.VirtualMachinePowerState;

/**
 * VM사양 변경
 * @author 윤경
 *
 */
public class VReconfigVM  extends com.clovirsm.hv.vmware.CommonAction
{

	Map param;
	String dc;
	public VReconfigVM(ConnectionMng m) {
		super(m);
		 
	}
	boolean isWindow = false;
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		this.dc  = dc;
		int cpu = CommonUtil.getInt(param.get(HypervisorAPI.PARAM_CPU));
		int mem = CommonUtil.getInt(param.get(HypervisorAPI.PARAM_MEM));
		int diskSize = CommonUtil.getInt(param.get(HypervisorAPI.PARAM_DISK));
		this.param = param;
		reconfig(super.getVM(param), cpu, mem, diskSize , param);
		if(diskShellErr.size()>0) {
			throw new Exception("Disk shell call error");
		}
		return null;
	}
	/*List<String> unMountDisks ; 
	protected void diskUnMountAll(ManagedObjectReference vmRef , List<VirtualDisk> disks)  throws Exception {
		unMountDisks = new ArrayList();
		for(int i=1;i < disks.size(); i++)
		{
			VirtualDisk d = disks.get(i);
			VUnMountDisk unmount  = new  VUnMountDisk(this.connectionMng);
			String diskName = ((VirtualDiskFlatVer2BackingInfo)d.getBacking()).getFileName();
			unMountDisks.add(diskName);
			unmount.doAction(vmRef, diskName, -1 );
		}
		System.out.println(unMountDisks);
	}
	protected void diskMountAll(ManagedObjectReference vmRef  )  throws Exception {
		 
		for(String diskName : unMountDisks)
		{
			 
			VMountDisk  mount  = new  VMountDisk(this.connectionMng);
		 
			mount.doAction(vmRef, diskName, -1 );
		}
	}*/
    Map diskShellErr = new HashMap();
	protected VirtualDeviceConfigSpec createVirtualDisk(ManagedObjectReference vmRef, Set dsInHost, Map m, String[] dsNames,
            int diskSizeGB, int createIdx) throws Exception {

		 
			 
			
			 String diskName = createDisk.getDiskName(dsInHost, dsNames, (String)param.get("VM_NM"),  diskSizeGB);
			 String diskmode = "persistent";
			 VirtualDeviceConfigSpec vdcs = createDisk.getDiskDeviceConfigSpec(vmRef, diskName, diskSizeGB, diskmode);
			 m.put("DISK_PATH", diskName);
			 /*String shellKey =  "DISK_SHELL_LINUX";
			 if(isWindow) {
				 shellKey = "DISK_SHELL_WIN";
			 }
			 Map shellConnInfo = (Map)param.get(shellKey);
			 if(shellConnInfo != null) {
				 Integer exitCode = this.runDiskCmd(vmRef, shellConnInfo, createIdx + " " + (String) m.get("DISK_NM"));
				 if(exitCode != 0) {
					 diskShellErr.put(m.get("DISK_ID"), m.get("DISK_NM"));
				 }
					 
			 }*/
		 	 return vdcs;
		 
	 
	}
	VCreateDisk createDisk;
	protected ArrayList<VirtualDeviceConfigSpec> createVirtualDisk(ManagedObjectReference vmRef , List<VirtualDisk> disks,      int diskSizeGB, Map param) throws Exception {
		ArrayList deviceConfigSpec = new ArrayList<VirtualDeviceConfigSpec>();
		List<Map> dataDisk = (List<Map>) param.get(HypervisorAPI.PARAM_DATA_DISK_LIST);
		createDisk = new VCreateDisk(this.connectionMng);
		ManagedObjectReference host = (ManagedObjectReference)getProp(vmRef,"runtime.host");
		Set dsInHost = super.getDsInHost(host);
		int createIdx  = disks.size();
		for(Map m : dataDisk)
		{
			if(NumberUtil.getInt(m.get("DISK_SIZE"))==0) continue;
			VirtualDeviceConfigSpec diskSpec = null;
			String path = (String)m.get("DISK_PATH");
			if(path == null || "".equals(path)) // 추가
			{	
				diskSpec = createVirtualDisk(vmRef, dsInHost, m, (String[])m.get(VMWareAPI.PARAM_DATASTORE_NM_LIST),  NumberUtil.getInt(m.get("DISK_SIZE")), ++createIdx);
			}
			else
			{
				VirtualDisk d = createDisk.getDiskByPath(disks, path);
				if(d != null)
				{
					if("Y".equals(m.get("DEL_YN")))
					{
						//diskSpec = new VirtualDeviceConfigSpec();
						//diskSpec.setDevice(d);
						//diskSpec.setFileOperation(VirtualDeviceConfigSpecFileOperation.DESTROY);
						//diskSpec.setOperation(VirtualDeviceConfigSpecOperation.REMOVE);
					}
					else
					{
						long diskSize = NumberUtil.getInt(m.get("DISK_SIZE"))*1024*1024;
						if(d.getCapacityInKB() < diskSize)
						{
							diskSpec = new VirtualDeviceConfigSpec();
							d.setCapacityInKB(diskSize);
							diskSpec.setDevice(d);
							diskSpec.setOperation(VirtualDeviceConfigSpecOperation.EDIT);
						}
					}
					
				}
				 
			}
			 
			if(diskSpec != null) deviceConfigSpec.add(diskSpec);
		}
		return  deviceConfigSpec;
		 
	}
	protected void reconfig(ManagedObjectReference vmRef,int cpu, int mem, int diskSize, Map param ) throws Exception
	{
		isWindow = this.isWindow(vmRef);
		reconfig(false, vmRef, cpu, mem, diskSize, param);
	}
	 
	protected Integer runDiskCmd(ManagedObjectReference vmRef, Map param , String args) throws Exception {
		VGuestRun run = new VGuestRun(this.connectionMng);
		return run.runCmd(vmRef, (String)param.get("CONN_USERID"), (String)param.get("CONN_PWD"), (String)param.get("CONN_URL"), args, null);
		
	}
	protected void reconfig(boolean onlyDisk, ManagedObjectReference vmRef,int cpu, int mem, int diskSize, Map param ) throws Exception
	{
		 
	
		List<VirtualDevice> hardwares = super.getHardwareDevice(vmRef);
		List<VirtualDisk> disks = getDisks(hardwares);
		VirtualMachineConfigSpec spec = new VirtualMachineConfigSpec();
		Map<String, Object> propValues = super.getProps(vmRef,  "summary.config.memorySizeMB",				"summary.config.numCpu",  "config.hardware.device" );
		if(propValues.size()==0) return;
		if(cpu == NumberUtil.getInt(propValues.get("summary.config.numCpu")) && mem  == NumberUtil.getLong(propValues.get("summary.config.memorySizeMB"))/1024) {
			onlyDisk = true;
		};
		
		spec.setNumCPUs(cpu);
		spec.setMemoryMB(new Long(mem * 1024));
			
		 
		boolean isChangeGpu = false;
		ArrayList deviceConfigSpec = createVirtualDisk(vmRef, disks, diskSize, param);
		if(param.get("GPU_SIZE") != null) {
			List<VirtualDevice> deviceList = ( ((ArrayOfVirtualDevice)propValues.get( "config.hardware.device")).getVirtualDevice());
			GPUConfig gpuConfig = new GPUConfig(deviceList );
			isChangeGpu = gpuConfig.addGPUChange(deviceConfigSpec, NumberUtil.getInt(param.get("GPU_SIZE")));
		}
		if(deviceConfigSpec != null && deviceConfigSpec.size()>0)
		{
			spec.getDeviceChange().addAll(deviceConfigSpec);
		}
		else if(onlyDisk)
		{
			return ; // disk만 변경하려고 하는데, 변경할 것이 없으면..
		}
		
		boolean isHotDeploy = HVProperty.getInstance().getProperty("reconfig.poweron_yn", "N").equals("Y");
		boolean isRestart = true;
		if(!isHotDeploy && onlyDisk) {
			isRestart = false; // hot deploy가 아니고 디스크만 변경된 경우 
		}
		else if(isHotDeploy) { //hot deploy면서 cpu가 커지거나, memory가 커지거나 gpu  변경이 없다면
			if(cpu >= NumberUtil.getInt(propValues.get("summary.config.numCpu")) && mem  >= NumberUtil.getLong(propValues.get("summary.config.memorySizeMB"))/1024 && !isChangeGpu) {
				isRestart = false;
			};
		}
		doProcess(isRestart, vmRef, spec );
	}
	protected void powerOn(ManagedObjectReference vmRef ) throws Exception
	{
		 
		 
		VPowerOnVM powerOn = new  VPowerOnVM(super.connectionMng);
		powerOn.process(vmRef);
		
	}
	private void doProcess(boolean isRestart, ManagedObjectReference vmRef, VirtualMachineConfigSpec spec ) throws Exception
	{
		boolean needStart = false; 
		if(isRestart)
		{
			VirtualMachinePowerState  powerState = (VirtualMachinePowerState) super.getProp(vmRef, "runtime.powerState" );
			
		 
			if(!powerState.value().equals("poweredOff"))
			{
				
				VPowerOffVM powerOff = new  VPowerOffVM(super.connectionMng);
				powerOff.process(vmRef);
				needStart = true;
				Thread.sleep(3000);
				
			}
		}
		ManagedObjectReference task =
				vimPort.reconfigVMTask(vmRef, spec);
				
		try
		{
			getTaskResultAfterDone(task, "ReconfigVM:" + vmRef.getValue());
		}
		finally
		{
			if(needStart) {
				powerOn(vmRef );
			}
		}
	}

}

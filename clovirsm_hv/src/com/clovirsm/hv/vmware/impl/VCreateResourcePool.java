package com.clovirsm.hv.vmware.impl;

import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.HypervisorException;
import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.DuplicateNameFaultMsg;
import com.vmware.vim25.InsufficientResourcesFaultFaultMsg;
import com.vmware.vim25.InvalidNameFaultMsg;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ResourceAllocationInfo;
import com.vmware.vim25.ResourceConfigSpec;
import com.vmware.vim25.RuntimeFaultFaultMsg;
import com.vmware.vim25.SharesInfo;
import com.vmware.vim25.SharesLevel;
/**
 * 리소스 풀 추가
 * @author 윤경
 *
 */
public class VCreateResourcePool extends com.clovirsm.hv.vmware.CommonAction{

	public VCreateResourcePool(ConnectionMng m) {
		super(m);

	}

	
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result) throws Exception {
		 
		addResourcePool(dc, cluster,  (String)param.get(VMWareAPI.PARAM_RESOURCEPOOL_NM));
		return null;
		
	}
	protected void setCPUConfig(ResourceAllocationInfo cpu )
	{
		cpu.setLimit(0l);
		cpu.setExpandableReservation(false);
		cpu.setReservation(0l);
	}

	public void addResourcePool(ManagedObjectReference resourcepoolmor, String poolName) throws Exception
	{
		SharesInfo sharesInfo = new SharesInfo();
		sharesInfo.setLevel(SharesLevel.NORMAL);
		sharesInfo.setShares(4000);

		ResourceConfigSpec spec = new ResourceConfigSpec();
		ResourceAllocationInfo cpu = new ResourceAllocationInfo();
		setCPUConfig(cpu);
		
		cpu.setShares(sharesInfo);

		//my $cpuAllocation = ResourceAllocationInfo->new(expandableReservation => 'true', limit => -1, reservation => 0, shares => $cpuShares);
		//my $memoryAllocation = ResourceAllocationInfo->new(expandableReservation => 'true', limit => -1, reservation => 0, shares => $memShares);
		spec.setCpuAllocation(cpu  );
		SharesInfo sharesInfo1 = new SharesInfo();
		sharesInfo1.setLevel(SharesLevel.NORMAL);
		sharesInfo1.setShares(163840);
		ResourceAllocationInfo mem  = new ResourceAllocationInfo();
		mem.setLimit(-1l);
		mem.setExpandableReservation(true);
		mem.setReservation(0l);
		mem.setShares(sharesInfo1);


		spec.setCpuAllocation(cpu  );
		spec.setMemoryAllocation(mem);

		resourcePool = vimPort.createResourcePool(resourcepoolmor, poolName, spec);

		if (!resourcePool.getType().equals("ResourcePool")) {
			throw new HypervisorException(HypervisorException.PROCESS_FAIL, "CreateResourcePool:" + poolName);
		}
	}
	protected void addResourcePool(String dc, String cluster, String poolName) throws Exception
	{
		ManagedObjectReference clusterMor = super.getCluster(dc, cluster);
		try
		{
			ManagedObjectReference t = super.findObject(clusterMor, "ResourcePool", poolName);	
		}
		catch(NotFoundException e)
		{
			ManagedObjectReference resourcepoolmor = 
					(ManagedObjectReference) super.getProp(clusterMor ,"resourcePool");
	
			addResourcePool(resourcepoolmor, poolName);
		}

	}
	ManagedObjectReference resourcePool;
	public ManagedObjectReference getResourcePool()
	{
		return resourcePool;
	}

}

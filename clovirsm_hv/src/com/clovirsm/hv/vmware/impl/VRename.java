package com.clovirsm.hv.vmware.impl;

import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

/**
 * 이름 변경
 * @author 윤경
 *
 */
public class VRename  extends com.clovirsm.hv.vmware.CommonAction{

	public VRename(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		ManagedObjectReference obj = getObject(dc, param);
		rename(obj,(String)param.get(HypervisorAPI.PARAM_OBJECT_NEW_NM));
		return null;
	}
	
	protected void rename(ManagedObjectReference obj, String newName) throws Exception
	{
		 
		ManagedObjectReference taskmor = vimPort.renameTask(obj, newName);
		getTaskResultAfterDone(taskmor,"Rename " + obj.getType()  +":" + newName);
	}


}

package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.vmware.CommonAction;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfTaskInfo;
import com.vmware.vim25.InvalidStateFaultMsg;
import com.vmware.vim25.LocalizedMethodFault;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.RuntimeFaultFaultMsg;
import com.vmware.vim25.TaskFilterSpec;
import com.vmware.vim25.TaskFilterSpecByEntity;
import com.vmware.vim25.TaskFilterSpecByUsername;
import com.vmware.vim25.TaskInfo;
import com.vmware.vim25.TaskInfoState;

/**
 * WAS시작 시 이전에 중단 된 Task정보 상태 알아내서 사후 작업 실행
 * @author 윤경
 *
 */
public class VOnStartUpProcess extends CommonAction{

	boolean waiting = true;
	public VOnStartUpProcess(ConnectionMng m) {
		super(m);
		// TODO Auto-generated constructor stub
	}
	public void setWaiting(boolean waiting)
	{
		this.waiting = waiting;
	}
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param,
			Map result) throws Exception {
		getTaskList(param, (Map<String, IAfterProcess>)param.get(HypervisorAPI.PARAM_LIST));
		return null;
	}
	private String getStatus(TaskInfoState taskInfoState)
	{
		if(taskInfoState.equals(TaskInfoState.ERROR))
		{
			return "F";
		}
		else if(taskInfoState.equals(TaskInfoState.SUCCESS))
		{
			return "S";
		}
		else
		{
			return "W";
		}
	}
	protected String getFailMsg(LocalizedMethodFault localizedMethodFault)
	{
		return localizedMethodFault.getLocalizedMessage();
		
	}
	protected void getTaskList(Map param, Map<String, IAfterProcess> dbTaskList) throws Exception
	{
		ManagedObjectReference taskManagerRef = super.connectionMng.getServiceContent().getTaskManager();
		
    	TaskFilterSpec filter = new TaskFilterSpec();
    	TaskFilterSpecByUsername usernameFilter = new TaskFilterSpecByUsername();
    	usernameFilter.getUserList().add((String)param.get(HypervisorAPI.PARAM_USERID));
		 
		//filter.setUserName(usernameFilter );
    	//Set taskIds = dbTaskList.keySet();
    	filter.getState().add(TaskInfoState.RUNNING);
		filter.getState().add(TaskInfoState.ERROR);
		filter.getState().add(TaskInfoState.QUEUED);
    	ManagedObjectReference result = vimPort.createCollectorForTasks(taskManagerRef, filter );
    	vimPort.setCollectorPageSize(result, 500);
    	ArrayOfTaskInfo t = (ArrayOfTaskInfo) super.getProp(result, "latestPage");
    	//List<TaskInfo> nextList = vimPort.readNextTasks(result, 200);
    	List<TaskInfo> list = t.getTaskInfo();
    	//list.addAll(nextList);
    	Set<String>  dbTaskNames= dbTaskList.keySet();
    	Set<String> finishTasks = new HashSet();
    	finishTasks.addAll(dbTaskNames);
    	System.out.println("task size = " + list.size());
    	for(TaskInfo i : list)
    	{
    		if(dbTaskNames.contains(i.getKey()))
    		{
    			 
    			finishTasks.remove(i.getKey());
    			String status = this.getStatus(i.getState());
    			if(status.equals("F"))
    			{
    				dbTaskList.get(i.getKey()).onAfterFail(i.getKey(), new Exception(getFailMsg(i.getError())));
    			}
    			else if(status.equals("S"))
    			{
    				dbTaskList.get(i.getKey()).onAfterSuccess(i.getKey());
    			}
    			else if(this.waiting)
    			{
    				super.getTaskResultAfterDone(true, i.getTask(), "", dbTaskList.get(i.getKey()));
    			}
    			 
    		}
    		 
    	}
    	System.out.println("finish task size = " + finishTasks.size());
    	for(String task:finishTasks)
    	{
    		dbTaskList.get(task).onAfterSuccess(task);
    	}
	}
}

package com.clovirsm.hv.vmware.impl;

import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;
/**
 * 폴더 추가
 * @author 윤경
 *
 */
public class VCreateFolder extends com.clovirsm.hv.vmware.CommonAction{

	public VCreateFolder(ConnectionMng m) {
		super(m);

	}

	
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result) throws Exception {
		
		addFolder(dc, cluster, (String) param.get(VMWareAPI.PARAM_FOLDER_NM));
		return null;
		
	}
	
	
	public ManagedObjectReference makeFolder(ManagedObjectReference dcM, String folderName) throws Exception
	{
		String[] arr = folderName.split("/");
		ManagedObjectReference parent = null;
		for(int i=0; i <arr.length; i++)
		{
			ManagedObjectReference me=null;
			try
			{
				me = super.findObject(dcM, "Folder", arr[i]);
			}
			catch(NotFoundException e)
			{
				
			}
			
			if(me==null)
			{
				if(parent == null)
				{
					parent = (ManagedObjectReference)getProp(dcM, "vmFolder");
				}
				parent = vimPort.createFolder(parent, arr[i]);
			}
			else
			{
				parent = me;
			}
		}
		return parent;
	}
	private void addFolder(String dc, String cluster, String folderName) throws Exception {
		ManagedObjectReference dcM = super.getDataCenter(dc);
		makeFolder(dcM, folderName);
		
	}

}

package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.NumberUtil;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfManagedObjectReference;
import com.vmware.vim25.ArrayOfVirtualDevice;
import com.vmware.vim25.ManagedObjectReference;

/**
 * 템플릿 명 존재 여부 체크
 * @author 윤경
 *
 */
public class VCheckTemplatePath extends com.clovirsm.hv.vmware.CommonAction{
	//Set<String> excludePath ;
	public VCheckTemplatePath(ConnectionMng m) {
		super(m);
		/*try {
			String[] path= HVProperty.getInstance().getStrings("disk_exclude_path");
			if(path != null)
			{
				excludePath = new HashSet();
				for(String p:path)
				{
					excludePath.add(p);
				}
			}
		} catch (Exception e) {
		 
			e.printStackTrace();
		}*/
	}

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param,
			Map result) throws Exception {
		this.checkTemplateName(dc, (String[])param.get(HypervisorAPI.PARAM_TMPL_NM_LIST), result);
		return null;
	}
	
	private void checkTemplateName(String dc, String[] path, Map result) throws  Exception
	{
		for(String path1 : path)
		{
			ManagedObjectReference obj = super.getVM(path1);
			if(obj == null)
			{
				throw new NotFoundException( path1);
			}
			Map propValues = super.getProps(obj, "summary.config.guestFullName","summary.config.numCpu","summary.config.memorySizeMB", "config.hardware.device","environmentBrowser", "runtime.host","network");
			Map result1 = new HashMap();
			result1.put("GUEST_NM", propValues.get("summary.config.guestFullName"));
			result1.put("CPU_CNT", propValues.get("summary.config.numCpu"));
			ManagedObjectReference host = (ManagedObjectReference)propValues.get("runtime.host");
			if(host != null)
			{
				result1.put("HOST_HV_ID", host.getValue());
			}
			result1.put("RAM_SIZE", NumberUtil.getLong(propValues.get("summary.config.memorySizeMB"))/1024);
			result1.put("DISK_SIZE", VInfoVM.getDiskSize((ArrayOfVirtualDevice) propValues.get("config.hardware.device"))/1024/1024);
			List<ManagedObjectReference> vmNetwork = ((ArrayOfManagedObjectReference) propValues.get("network")).getManagedObjectReference();
			 
			List network = new ArrayList(); 
			for (ManagedObjectReference n : vmNetwork) {
				
				network.add(n.getValue());
			}
			result1.put("NETWORK", network);
			result.put(path1,result1);
		}
	}

}

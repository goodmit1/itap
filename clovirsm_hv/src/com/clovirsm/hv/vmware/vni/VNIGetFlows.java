package com.clovirsm.hv.vmware.vni;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.NumberUtil;

public class VNIGetFlows extends VNICommon{

	public VNIGetFlows(VNIConnection conn) {
		super(conn);
		 
	}

	@Override
	public void run(Map param, Map result) throws Exception {
		getData((String)param.get("VM_NM"), NumberUtil.getInt(param.get("HOURS"), 24), result);
		
	}
	/*protected Map getEntities() throws Exception {
		String path="/api/search/completions?partialQuery=vm+where+name+like+&fullQuery=vm+where+name+like+&maxCompletions=70&includeKeysForValues=true";
		JSONObject json = super.client.getJSON(path);
		JSONArray values = json.getJSONObject("data").getJSONObject("partialQueryCompletions").getJSONObject("completionsByType").getJSONArray("VALUE");
		Map result = new HashMap();
		for(int i=0; i < values.length(); i++) {
			JSONObject value = values.getJSONObject(i);
			String name = value.getString("completion");
			JSONArray modelKeys = value.getJSONArray("modelKeys");
			for(int j=0; j < modelKeys.length();j++) {
				result.put(modelKeys.getString(j), name);
			}
		}
		return result;
	}*/
	
	private String getParent(JSONObject json) throws Exception{
		JSONArray properties = json.getJSONObject("propertyBag").getJSONArray("properties");
		for(int i=0; i < properties.length(); i++) {
			if("PARENT_GROUP_ID".equals(properties.getJSONObject(i).getString("key"))) {
				return properties.getJSONObject(i).getString("value");
			}
		}
		return null;
	}
	private boolean isShared(JSONObject json) throws Exception{
		JSONArray properties = json.getJSONObject("propertyBag").getJSONArray("properties");
		for(int i=0; i < properties.length(); i++) {
			if("Shared".equals(properties.getJSONObject(i).getString("key"))) {
				return properties.getJSONObject(i).getBoolean("value");
			}
		}
		return false;
	}
	protected Map getGraph(String[] id,List<Map> byteMap ,  String vmName, long now, long lowerBound, long upperBound) throws Exception{
		String param = "graphLevel=DEFAULT&graphType=SEGMENTATION_TOPOLOGY&includeFlowType=ACTION_ALLOW&listOfObjects=" + URLEncoder.encode(id[0]) + "&listOfObjects=" + URLEncoder.encode(id[1]) + "&listOfObjects=10000%3A1%3A0&time=" + now + "&upperBound=" + upperBound + "&lowerBound=" + lowerBound;
		JSONObject json = client.getJSON("/api/config/graph?" + param);
		JSONArray  nodes = json.getJSONObject("graph").getJSONArray("nodes");
		Map<Object, String> nodeMap = new HashMap();
		Set<String> parents = new HashSet();
		Object vmId = null;
		for(int i=0; i < nodes.length(); i++) {
			JSONObject node = nodes.getJSONObject(i);
			if(isShared(node)) {
				continue;
			}
			String name=node.getJSONObject("data").getString("name");
			if(name.equals(vmName)) {
				vmId = node.get("id");
			}
			String parent = getParent(node);
			if("Others".equals(name) && parent != null) {
				continue;
			}
			if(parent != null) {
				parents.add(parent);
			}
			nodeMap.put(node.get("id"),	name);
		}
		
	 
		JSONArray  edges = json.getJSONObject("graph").getJSONArray("edges");
		Map flowMap = new HashMap();
		
		for(int i=0; i < edges.length(); i++) {
			JSONObject edge = edges.getJSONObject(i);
			String from =(String) nodeMap.get(edge.get("fromNodeId"));
			String to = (String)nodeMap.get(edge.get("toNodeId"));
			if(from==null || to==null) {
				continue;
			}
			if(parents.contains(from) || parents.contains(to)) {
				continue;
			}
			long total = getTotal(byteMap, from, to, edge.getJSONObject("propertyBag").getJSONArray("properties").getJSONObject(0).getString("value"), now);
			flowMap.put(from+"."+to, new Object[] {from, to, total/1024});
			//long rtotal = getReverseTotal(edge.getJSONObject("propertyBag").getJSONArray("properties").getJSONObject(0).getString("value"), now);
			//if(rtotal >0) {
			//	flowMap.put( to+"."+from, new Object[] { to, from, rtotal/1024});
			//}
			
		}	
		return flowMap;
	}
	protected long getTotal(List byteArr, String fromName, String toName, String queryStr, long now) throws Exception{
		//System.out.println("========="  ); 
		String param = "searchString=" + URLEncoder.encode(queryStr) + "&timestamp=" + now + "&includeObjects=false&includeFacets=false&includeMetrics=false&includeEvents=false&startIndex=0&maxItemCount=0&dateTimeZone=%2B09%3A00&sourceString=APPLET&includeModelKeyOnly=true";
		JSONObject json = client.getJSON("/api/search/query?"+param);
		JSONArray entries = json.getJSONObject("groupBy").getJSONArray("entries");
		long total = 0;
		for(int i=0; i < entries.length(); i++) {
			long sum = entries.getJSONObject(i).getJSONArray("aggregates2").getJSONObject(0).getJSONObject("aggs").getLong("SUM");
			System.out.println(entries.getJSONObject(i).getJSONObject("value").getString("name") + "=" + sum/1024);
			Map m = new HashMap();
			
			m.put("FROM", fromName);
			m.put("TO_CATEGORY", toName);
			m.put("TO", entries.getJSONObject(i).getJSONObject("value").getString("name")  );
			m.put("TOTAL_BYTE", sum);
			byteArr.add(m);
			total += sum;
		}
		//System.out.println("=========total" + (total/1024)  ); 
		return total;
	}
	/*protected long getReverseTotal(String queryStr, long now) throws Exception{
		String srcKey = CommonUtil.getInnerStr(queryStr, "srcMicroSegGroupMembers.modelKey = '","'");
		String dstKey = CommonUtil.getInnerStr(queryStr, "dstMicroSegGroupMembers.modelKey = '","'");
		if(srcKey != null && dstKey != null) {
			String newQuery = CommonUtil.getReplaceInnerStr(queryStr, "srcMicroSegGroupMembers.modelKey = '","'", dstKey);
			if(newQuery != null) {
				newQuery = CommonUtil.getReplaceInnerStr(newQuery, "dstMicroSegGroupMembers.modelKey = '","'", srcKey);
			}
			if(newQuery != null) {
				return getTotal(newQuery, now);
			}
		}
		return 0;
	}*/
	protected void getData(String vmName, int hour, Map result) throws Exception {
		long current = System.currentTimeMillis();
		String queryString = "searchString=plan+vm+where+name+like+%27" + vmName + "%27+in+last+" + hour + "+hours&timestamp=" + current + "&timeRangeString=+at+Now+&includeObjects=false&includeFacets=true&includeMetrics=false&includeEvents=false&startIndex=0&maxItemCount=10&dateTimeZone=%2B09%3A00&sourceString=USER&includeModelKeyOnly=false";
		String path = "/api/search/query?"+ queryString;
		JSONObject json = super.client.getJSON(path);
		JSONObject applets = json.getJSONArray("applets").getJSONObject(0);
		JSONArray payload = applets.getJSONArray("applets").getJSONObject(2).getJSONArray("payload");
		if(payload.length()==0) return;
		List portTrafficList = new ArrayList();
		for(int i=0; i < payload.length(); i++) {
			JSONArray fields = payload.getJSONObject(i).getJSONArray("fields");
			Map m = new HashMap();
			for(int j=0; j < fields.length(); j++) {
				JSONObject field = fields.getJSONObject(j);
				m.put(field.getString("name") , field.get("value"));
			}
			m.put("VM_NM", vmName);
			portTrafficList.add(m);
		}
		result.put("PORT_TRAFFIC", portTrafficList);
		
		JSONArray entities = applets.getJSONArray("applets").getJSONObject(0).getJSONArray("entities");
		String[] ids = new String[2];
		ids[0] = entities.getJSONObject(0).getString("modelKey");
		ids[1] = entities.getJSONObject(1).getString("modelKey");
		long lowerBound = json.getJSONObject("queryResultSemanticsContext").getJSONObject("timeRange").getLong("lowerBound");
		long upperBound = json.getJSONObject("queryResultSemanticsContext").getJSONObject("timeRange").getLong("upperBound");
		List byteArr = new ArrayList();
		Map flowMap = this.getGraph(ids, byteArr, vmName, current, lowerBound, upperBound);
		result.put("FLOW", flowMap);
		result.put("FLOW_BYTES", byteArr);
	}

}

package com.clovirsm.hv.vmware.vni;

import java.util.Map;

import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.SessionRestClient;

public abstract  class VNICommon {
	protected  RestClient client;
	
	public VNICommon(VNIConnection conn)
	{
		if(conn != null) {
			this.client = conn.getRestClient();
		}
	}
	public abstract void run(Map param, Map result) throws Exception ;
	public void run(Map param, Map result, IAfterProcess after) throws Exception 
	{
		this.run(param, result);
		 
	}
	 
}

package com.clovirsm.hv.vmware.vni;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class GetVNIHostList extends VNICommon{

	public GetVNIHostList(VNIConnection conn) {
		super(conn);
		 
	}

	private void getHosts(List serverlist, String cursor , long start) throws Exception {
		String param = "?size=100";
		if(cursor != null) {
			param += "&cursor=" + cursor + "&start_time=" + start;
		}
		JSONObject hosts = client.getJSON("/api/ni/entities/hosts" + param);
		JSONArray results = hosts.getJSONArray("results");
		 
		for(int i=0; i < results.length(); i++) {
			String entity_id = results.getJSONObject(i).getString("entity_id");
			System.out.println(entity_id);
			JSONObject host = client.getJSON("/api/ni/entities/hosts/" + entity_id);
			serverlist.add(host.getString("name"));
		}
		if(results.length()>0 && hosts.has("cursor")) {
			 
			getHosts(serverlist, hosts.getString("cursor"), hosts.getLong("start_time") + 1);
		}
	}
	@Override
	public void run(Map param, Map result) throws Exception {
		 
		List<String> list = new ArrayList();
		getHosts(list, null, 0);
		result.put("LIST", list);
	}

}

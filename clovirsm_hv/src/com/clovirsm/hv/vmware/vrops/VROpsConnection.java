package com.clovirsm.hv.vmware.vrops;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.RestClient;

public class VROpsConnection implements IConnection{
	String url;
	String userId;
	String pwd;
	 
	RestClient client;
	Date expire;
	String token;
	boolean connect = false;
	public RestClient getRestClient()
	{
		return client;
	}
 

	@Override
	public String getURL() {
		return url;
	}

 
	@Override
	public void connect(String url, String userId, String pwd, Map prop) throws Exception {
		this.url = url;
		 
		client = new RestClient(url);
		client.setMimeType("application/json");
		client.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		JSONObject json = new JSONObject();
		json.put("username",userId);
		json.put("password", pwd);
		json.put("authSource", prop.get("SOURCE"));  
		JSONObject result = (JSONObject)client.post("/auth/token/acquire", json.toString());
		JSONObject authToken = result.getJSONObject("ops:auth-token");
		try {
			expire = new Date(authToken.getString("ops:expiresAt"));
		}
		catch(Exception e) {
			expire = null;
		}
		if(expire == null || expire.compareTo(new Date())<=0)
		{
			expire = new Date();
			expire.setMinutes(expire.getMinutes()+30);
		}
		Map<String, String> header = new HashMap();
		header.put("Authorization", "vRealizeOpsToken " + authToken.getString("ops:token"));
		client.setHeader(header);
		connect = true;
	}

	 

	@Override
	public boolean isConnected() {
		return client == null || expire.compareTo(new Date())>0;
		
	}

	 
	@Override
	public void disconnect() {
		client = null;
		
	}
}

package com.clovirsm.hv.vmware.vrops;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.vmware.VMWareAPI;

public class GetVROpsHostList extends VROpsCommon{

	public GetVROpsHostList(VROpsConnection conn) {
		super(conn);
		// TODO Auto-generated constructor stub
	}

	 

	@Override
	public Map run(Map param ) throws Exception {
		JSONObject resources =  (JSONObject) client.get("/resources?resourceKind=hostsystem");
		JSONArray resourceArr =CommonUtil.getJSONArray(resources.getJSONObject("ops:resources"), "ops:resource");
		List serverlist = new ArrayList();
		for(int i=0; i < resourceArr.length(); i++) {
			JSONObject resource = resourceArr.getJSONObject(i);
			String name = resource.getJSONObject("ops:resourceKey").getString("ops:name") ;
			 
			serverlist.add(name);
			 
		 
			
		}
		Map result = new HashMap();
		result.put(VMWareAPI.PARAM_LIST, serverlist);
		return result;
		
	}

}

package com.clovirsm.hv.vmware.log;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.CommonAPI;
import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.IAPI;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.nsx.NSXConnection;
 

public class LogInsightAPI extends CommonAPI {
	public static String getHVType() {
		 
		return "V.VRLI";
	}
	protected  String getConnectionName() {
		return VLogConnection.class.getName();
	}
	public List<Object[]> getLog(Map param,String field, String value, long start_timestamp, long end_timestamp) throws Exception{
		VLogConnection conn = (VLogConnection) connect(param);
		String path = "/api/v1/events/" + field + "/CONTAINS%20" + value +  (start_timestamp >0 ? "/timestamp/%3E%3D" + start_timestamp : "" ) + ( end_timestamp >0 ? "/timestamp/%3C%3D" + end_timestamp : "") +"?order-by-direction=ASC&view=SIMPLE";
		JSONObject res = (JSONObject)conn.getRestClient().get(path);
		List result = new ArrayList();
		org.json.JSONArray list =  res.getJSONArray("results");
		for(int i=0 ; i < list.length(); i++) {
			JSONObject result1 = list.getJSONObject(i);
			result.add(new Object[] { new Date(result1.getLong("timestamp")) ,result1.getString("text")});
		}
		disconnect(conn);
		return result;
	}
	
	public Map listHostNode(Map param) throws Exception
	{
		VLogConnection conn = (VLogConnection) connect(param);
		try {
			JSONObject list = conn.getRestClient().getJSON("/api/v1/vsphere");
			JSONArray results = list.getJSONArray("vCenterServers");
			List serverlist = new ArrayList();
			for(int i=0;  i < results.length(); i++) {
				String hostname = results.getJSONObject(i).getString("hostname");
				JSONObject json = conn.getRestClient().getJSON("/api/v1/vsphere/" + hostname + "/hosts");
				JSONArray hosts = json.getJSONArray("hosts");
				for(int j=0; j < hosts.length(); j++) {
					if(hosts.getJSONObject(j).getBoolean( "configured")) {
						serverlist.add(hosts.getJSONObject(j).getString("hostname"));
					}
				}
			}
			Map result = new HashMap();
			result.put(VMWareAPI.PARAM_LIST, serverlist);
			return result;
		}
		finally {
			this.disconnect(conn);
		}
		
	}
	
	 
}

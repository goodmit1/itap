package com.clovirsm.hv.vmware.log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.RestClient;

public class VLogConnection implements IConnection{
	String url;
	String userId;
	String pwd;
	 
	RestClient client;
	Date expire;
	String token;
	boolean connect = false;
	public RestClient getRestClient()
	{
		return client;
	}
	 

	@Override
	public String getURL() {
		return url;
	}

 
	@Override
	public void connect(String url, String userId, String pwd, Map prop) throws Exception {
		this.url = url;
		 
		client = new RestClient(url);
		client.setMimeType("application/json");
		client.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		JSONObject json = new JSONObject();
		json.put("username",userId);
		json.put("password", pwd);
		json.put("provider", prop.get("provider"));  
		JSONObject result = (JSONObject)client.post("/api/v1/sessions", json.toString());
		expire = new Date();
		expire.setMinutes(expire.getMinutes() + result.getInt("ttl")/60);
		
		token = result.getString("sessionId");
		client.setToken(token);
		connect = true;
	}

	 

	@Override
	public boolean isConnected() {
		return client == null || expire.compareTo(new Date())>0;
		
	}

	public static void main(String[] args)
	{
		VLogConnection conn = new VLogConnection();
		Map prop = new HashMap();
		prop.put("tenant", "vsphere.local");
		try {
			conn.connect("https://172.16.33.210", "configurationadmin", "VMware1!", prop);
			Map param = new HashMap();
			  
			//System.out.println(conn.getRestClient().getJSON(  "/forms-service/api/forms/aa9ecece-1e7e-4c81-92d9-a9565cdc0046" ));
			System.out.println(param);
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}
	@Override
	public void disconnect() {
		client = null;
		
	}
}

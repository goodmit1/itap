package com.clovirsm.hv.vmware.nsx;

import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.ParamObj;
import com.clovirsm.hv.RestClient;

public abstract class NSXCommon {

	protected RestClient client;
	public static String XML_PREFIX="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	public NSXCommon(RestClient client)
	{
		this.client = client;
	}
	 
	protected String pathPrefix()
	{
		return "/api";
	}
	protected String getPath(String path)
	{
		return pathPrefix() + path;
	}
	protected Object post(String path, ParamObj obj) throws Exception
	{
		return client.post(getPath(path),  XML_PREFIX + obj.toXML());
	}
	protected Object put(String path, ParamObj obj) throws Exception
	{
		return client.put(getPath(path),  XML_PREFIX + obj.toXML());
	}
	protected Object delete(String path ) throws Exception
	{
		return client.delete(getPath(path));
	}
	protected JSONObject get(String path ) throws Exception
	{
		return client.getJSON( getPath(path) );
	}
	
	public RestClient getRestClient()
	{
		return this.client;
	}
	public abstract void run(Map param, Map result, IAfterProcess after) throws Exception ;
}

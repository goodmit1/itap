package com.clovirsm.hv.vmware.nsx;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.RestClient;

public class NSXConnection implements IConnection{

	String url;
	Date expire;
	 
	RestClient client;
	@Override
	public String getURL() {
	 
		return url;
	}
	public RestClient getRestClient()
	{
		return client;
	}
	@Override
	public void connect(String url, String userId, String pwd, Map prop) throws Exception {
		this.url = url;
		client = new RestClient(url,userId,pwd );
		Calendar now = Calendar.getInstance();
		now.add(Calendar.MINUTE, 10);
		expire = now.getTime();
	}

	@Override
	public void disconnect() {
		client = null;
		
	}

	@Override
	public boolean isConnected() {
		 
		return client == null || expire.compareTo(new Date())>0;
	}

}

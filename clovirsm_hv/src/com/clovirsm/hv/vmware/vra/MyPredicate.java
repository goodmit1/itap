package com.clovirsm.hv.vmware.vra;

import com.jayway.jsonpath.Predicate;
import com.jayway.jsonpath.Predicate.PredicateContext;

public class MyPredicate implements Predicate
{
	int idx ; 
	int current = 0;
    
    public void setidx(int idx)
    {
    	this.idx = idx;
    	current = 0;
    }

	@Override
	public boolean apply(PredicateContext ctx) {
		return idx==current++;
	}

}

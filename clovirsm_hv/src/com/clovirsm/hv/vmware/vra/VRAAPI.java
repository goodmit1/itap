package com.clovirsm.hv.vmware.vra;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.CommonAPI;
import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.IAPI;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.vmware.VMWareConnection;
import com.vmware.connection.ConnectionMng;

public class VRAAPI extends CommonAPI  {
	public Object getExternParam(Map param,  JSONObject json) throws Exception{
		return null;
	}
	
	 

	public static FormGen getFormGet() throws Exception
	{
		return new FormGen();
	}
	public static String getHVType() {
	 
		return "V.VRA";
	}
	public Map listCatalog(Map param) throws Exception
	{
		return this.doProcess(null, param,  VListCatalog.class);
	}
	protected Map doProcess(IAfterProcess after, Map param,  Class... classList) throws Exception
	{	
		VRAConnection connectionMgr = null;
		try
		{
			connectionMgr = (VRAConnection) connect(param);
			 
			Map result = new HashMap();
			for(Class cls : classList)
			{
				VRACommon action = (VRACommon)cls.getConstructor(VRAConnection.class).newInstance(connectionMgr);
			 	action.run(param, result, after);
			 	
			}
			return result;
		}
		finally
		{
			disconnect(connectionMgr);
		}
	}
	 

	public Map request(Map param, IAfterProcess iafter) throws Exception {
		return this.doProcess(iafter, param, VCatalogRequest.class);
		
	}
	public Map requestDelete(Map param ) throws Exception {
		return this.doProcess(null, param, VCatalogRequestDelete.class);
		
	}
	public Map getRequestState(Map param) throws Exception {
		return this.doProcess(null, param, VGetRequestState.class);
		
	}
	public Map getVMReconfig(Map param) throws Exception {
		return null;
		
	}
	public Map getImageNames(Map param) throws Exception {
		 
		return null;
	}

	public Map getFavors(Map param) throws Exception {
		 
		return null;
	}

	public Map deleteVM(Map param) throws Exception {
	 
		return null;
	}

	public Map getListClusterTag(Map param) throws Exception {
	 
		return null;
	}

	public Map listIpRange(Map param) throws Exception {
		 
		return null;
	}
}

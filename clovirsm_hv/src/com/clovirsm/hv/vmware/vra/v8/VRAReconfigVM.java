package com.clovirsm.hv.vmware.vra.v8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.vra.VRACommon;
import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VRAReconfigVM extends VRACommon{
	JSONObject json;
	String requestId;
	boolean isFinished = false;
	String vmId;
	public VRAReconfigVM(VRAConnection conn) {
		super(conn);
		 
	}
	
	public boolean isFinished() {
		return isFinished;
	}
	protected JSONObject getResource ( String name ) {
		JSONArray resources = json.getJSONArray("resources");
		for(int i=0; i< resources.length(); i++) {
			JSONObject property = resources.getJSONObject(i).getJSONObject("properties");
		 
				if(name.equals(property.getString("resourceName"))){
					return  resources.getJSONObject(i);
					
				}
			 
		}
		return null;
	}
	protected String getResourceId( String name, boolean isId) {
		JSONArray resources = json.getJSONArray("resources");
		for(int i=0; i< resources.length(); i++) {
			if( resources.getJSONObject(i).has("properties")) {
				JSONObject property = resources.getJSONObject(i).getJSONObject("properties");
				 
					if(name.equals(property.getString("resourceName"))){
						if(isId) {
							return resources.getJSONObject(i).getString("id");
						}
						else {
							return property.getString("resourceId");
						}
						
					}
				 
			}
		}
		return null;
	}
	protected void removeDisk( String name ) throws Exception{
		//{"actionId":"Cloud.vSphere.Machine.Remove.Disk","targetId":"542f5fe2-2638-4622-a2bf-4804aac3732f","inputs":{"diskId":"/resources/disks/671bb1417f99447559e81693ce2e8"}}
				JSONObject param = new JSONObject();
				param.put("actionId", "Cloud.vSphere.Machine.Remove.Disk");
				param.put("targetId", vmId);
				JSONObject inputs = new JSONObject();
				inputs.put("name", "HDD");
				String diskId = getResourceId(name, false);
				inputs.put("diskId", diskId);
			 
				param.put("inputs", inputs);
				JSONObject result = (JSONObject)super.client.post("/deployment/api/deployments/" + requestId + "/resources/" + vmId + "/requests", param.toString());
				this.chkFinish(); 
	}
	protected void sizeUpBootDisk(   int sizeGB) throws Exception{
		//{"actionId":"Cloud.vSphere.Machine.Compute.Disk.Resize","targetId":"542f5fe2-2638-4622-a2bf-4804aac3732f","inputs":{"capacityGb":41}}
			 
				JSONObject param = new JSONObject();
				param.put("actionId", "Cloud.vSphere.Machine.Compute.Disk.Resize");
				param.put("targetId", vmId);
				JSONObject inputs = new JSONObject();
				inputs.put("capacityGb", sizeGB);
				
				 
			 
				param.put("inputs", inputs);
				JSONObject result = (JSONObject)super.client.post("/deployment/api/deployments/" + requestId + "/resources/" + vmId + "/requests", param.toString());
				this.chkFinish(); 
	}
	protected void sizeUpDisk(String name,  int sizeGB) throws Exception{
		//{"actionId":"Cloud.vSphere.Disk.Disk.Resize","targetId":"baec8ac5-149d-45a9-9673-be5a4733bc9c","inputs":{"capacityGb":2}}
		String diskId = getResourceId(name, true);
		JSONObject param = new JSONObject();
		param.put("actionId", "Cloud.vSphere.Disk.Disk.Resize");
		param.put("targetId", diskId);
		JSONObject inputs = new JSONObject();
		inputs.put("capacityGb", sizeGB);
		
		 
	 
		param.put("inputs", inputs);
		JSONObject result = (JSONObject)super.client.post("/deployment/api/deployments/" + requestId + "/resources/" + diskId + "/requests", param.toString());
		this.chkFinish(); 

	}
	protected void chkFinish() throws Exception{
		json = super.client.getJSON("/deployment/api/deployments/" + requestId + "?expandProject=true&expandResources=true&expandLastRequest=true");
		String status = json.getJSONObject("lastRequest").getString("status");
		if(status.indexOf("PENDING")<0 && status.indexOf("INPROGRESS")<0)	{
			if(status.indexOf("FAILED")>=0) {
				throw new Exception (json.getJSONObject("lastRequest").getString("statusDetails"));
			  
			}
			else {
				return  ;
			}
		}
		Thread.sleep(1000);
		chkFinish();
	}
	protected void addDisk(String name,  int sizeGB) throws Exception{
		if(name==null) {
			name = "HDD1";
		}
		//{"actionId":"Cloud.vSphere.Machine.Add.Disk","targetId":"542f5fe2-2638-4622-a2bf-4804aac3732f","inputs":{"name":"HDD2","capacityGb":1,"type":"Cloud.Volume"}}
		JSONObject param = new JSONObject();
		param.put("actionId", "Cloud.vSphere.Machine.Add.Disk");
		param.put("targetId", vmId);
		JSONObject inputs = new JSONObject();
		inputs.put("name", name);
		inputs.put("capacityGb", sizeGB);
		inputs.put("type", "Cloud.Volume");
		param.put("inputs", inputs);
		JSONObject result = (JSONObject)super.client.post("/deployment/api/deployments/" + requestId + "/resources/" + vmId + "/requests", param.toString());
		this.chkFinish(); 
	}
	protected void resize(  int cpu, int memGB) throws Exception{
		
		//{"actionId":"Cloud.vSphere.Machine.Resize","targetId":"542f5fe2-2638-4622-a2bf-4804aac3732f","inputs":{"cpuCount":2,"totalMemoryMB":1024}}
		JSONObject param = new JSONObject();
		param.put("actionId", "Cloud.vSphere.Machine.Resize");
		param.put("targetId", vmId);
		JSONObject inputs = new JSONObject();
		inputs.put("cpuCount", cpu);
		inputs.put("totalMemoryMB", memGB*1024);
		param.put("inputs", inputs);
		JSONObject result = (JSONObject)super.client.post("/deployment/api/deployments/" + requestId + "/resources/" + vmId + "/requests", param.toString());
		this.chkFinish(); 
	}
	protected JSONObject getInfoByResouceLink(JSONArray resources, String link) {
		for(int i=0; i < resources.length(); i++) {
			JSONObject resource = resources.getJSONObject(i);
			JSONObject properties = resource.getJSONObject("properties");
			if(link.endsWith(properties.getString("resourceId"))) {
				return properties;
			}
		}
		return null;
	}
	protected int getCurrentDisk(Map diskInfo, Map param) throws Exception{
		JSONObject resource = getResource((String)param.get("VM_NM")).getJSONObject("properties");
		JSONArray disks = resource.getJSONObject("storage").getJSONArray("disks");
		 
		int total = 0;
		for(int j=0; j < disks.length(); j++) {
			if("HDD".equals( disks.getJSONObject(j).getString("type"))){
				total+=disks.getJSONObject(j).getInt("capacityGb");
				diskInfo.put( disks.getJSONObject(j).getString("name"), new Object[] {"Y", disks.getJSONObject(j).getInt("capacityGb")});
			}
		}
		if(resource.has("attachedDisks")) {
			JSONArray attachedDisks = resource.getJSONArray("attachedDisks");
			for(int j=0; j < attachedDisks.length(); j++) {
				JSONObject disk = getInfoByResouceLink(json.getJSONArray("resources"), attachedDisks.getJSONObject(j).getString("source"));
				total+=disk.getInt("capacityGb");
				diskInfo.put(disk.getString("name"),new Object[] {"N", disk.getInt("capacityGb")}) ;
			}
		}
		return total;
	}
	protected void processDisk(  Map param) throws Exception {
		List<Map> dataDisk = (List<Map>) param.get(HypervisorAPI.PARAM_DATA_DISK_LIST);
		
		int reqTotal = 0;
		Map<String, Object[]> currentDiskInfo = new HashMap();
		int currentTotal = getCurrentDisk(currentDiskInfo, param);
		List<Map> addDisk = new ArrayList();
		for(Map m : dataDisk) {
			int reqSize = NumberUtil.getInt(m.get("DISK_SIZE"));
			reqTotal += reqSize;
			String diskNm = (String) m.get("DISK_NM");
			Object[] o = currentDiskInfo.remove(diskNm);
			Integer currDiskSize = (Integer)o[1];
			if(currDiskSize != null) {
				if("Y".equals(m.get("DEL_YN"))){
					 this.removeDisk(diskNm);
					 
				}
				else {
					if(currDiskSize==reqSize) {
						continue;
					}
					else {
						if(o[0].equals("Y")) {
							sizeUpBootDisk( reqSize);
						}
						else {
							sizeUpDisk(diskNm, reqSize);
						}
						
						  
					}
				}
			}
			else {
				addDisk.add(m);
			}
		}
		
		
		
		if(reqTotal == currentTotal) return;
		
		for(Map m : addDisk) {	
			 
			int reqSize = NumberUtil.getInt(m.get("DISK_SIZE"));
			boolean isNew = true;
			for(String nm : currentDiskInfo.keySet()) {
				Object[] o = currentDiskInfo.get(nm);
				if( (Integer)o[1] ==reqSize) {
					currentDiskInfo.remove(nm);
					isNew = false;
					break;
				}
			}
			if(!isNew) {
				continue;
			}
			String diskNm = (String)m.get("DISK_NM");
			 this.addDisk( (String)m.get("DISK_NM"),NumberUtil.getInt(m.get("DISK_SIZE"))) ;
			 
			
		}
	}
	@Override
	public void run(Map param, Map result) throws Exception {
		 requestId = (String)param.get("VRA_REQUEST_ID");
		 
		 json = super.client.getJSON("/deployment/api/deployments/" + requestId + "?expandProject=true&expandResources=true&expandLastRequest=true");
		 vmId = this.getResourceId((String)param.get("VM_NM"), true);
		 int cpu = CommonUtil.getInt(param.get(HypervisorAPI.PARAM_CPU));
		 int mem = CommonUtil.getInt(param.get(HypervisorAPI.PARAM_MEM));
		 JSONObject resource = getResource((String)param.get("VM_NM"));
		 
		 if(resource.getJSONObject("properties").getInt("cpuCount") != cpu || resource.getJSONObject("properties").getInt("totalMemoryMB")/1024 != mem) {
			  this.resize(cpu, mem) ;
			  
		 }
		 processDisk( param);
		 isFinished=true;
	}
}

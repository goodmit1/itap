package com.clovirsm.hv.vmware.vra.v8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.vmware.log.VLogConnection;
import com.clovirsm.hv.vmware.vra.VCatalogRequest;
import com.clovirsm.hv.vmware.vra.VCatalogRequestDelete;
import com.clovirsm.hv.vmware.vra.VGetRequestState;
import com.clovirsm.hv.vmware.vra.VRAAPI;
import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VRAAPI8 extends VRAAPI {
	
	 
	protected  String getConnectionName() {
		return VRA8Connection.class.getName();
	}
	public Object runWorkflow(Map param, String id, JSONObject json) throws Exception {
		VRA8Connection conn = (VRA8Connection) connect(param);
		try {
			JSONObject result = (JSONObject)conn.getRestClient().post("/vco-controlcenter/client/api/platform/workflows/" + id + "/executions", json.toString());
			return result;
		}
		finally {
			disconnect(conn);
		}
	}
	@Override
	public Object getExternParam(Map param, JSONObject json) throws Exception {
		VRA8Connection conn = (VRA8Connection) connect(param);
		try {
			JSONObject result = (JSONObject)conn.getRestClient().post(json.getString("url"), json.getJSONObject("param").toString());
			if(result.has("data")) {
				return result.get("data");
			}
			else if(result.has("error")){
				throw new Exception(result.getJSONObject("error").getString("summaryMessage"));
			}
			else {
				return "";
			}
		}
		finally {
			disconnect(conn);
		}
	}
	@Override
	public Map getListClusterTag(Map param) throws Exception {
		return this.doProcess(null, param, VListClusterTag.class);
	}
	@Override
	public Map getVMReconfig(Map param) throws Exception {
		return this.doProcess(null, param, VRAReconfigVM.class);
		
	}
	@Override
	public Map deleteVM(Map param) throws Exception {
		return this.doProcess(null, param, VRADeleteVM.class);
		
	}
	@Override
	public Map request(Map param, IAfterProcess iafter) throws Exception {
		if("O".equals(param.get("KUBUN"))) {
			return this.doProcess(iafter, param, VCatalogRequest8.class);
		}
		else {
			return this.doProcess(iafter, param, VBluePrintRequest8.class);
		}
		
		
	}
	@Override
	public Map requestDelete(Map param ) throws Exception {
		return this.doProcess(null, param, VCatalogRequestDelete8.class);
		
	}
	@Override
	public Map listCatalog(Map param) throws Exception
	{
		return this.doProcess(null, param,  VListCatalog8.class);
	}
	@Override
	public Map listIpRange(Map param) throws Exception
	{
		return this.doProcess(null, param,  VListIPRange.class);
	}
	@Override
	public Map getImageNames(Map param) throws Exception{
		return this.doProcess(null, param,  VListImage.class);
	}
	@Override
	public Map getFavors(Map param) throws Exception{
		return this.doProcess(null, param,  VListFlavor.class);
	}
	
	public Map getRequestState(Map param) throws Exception {
		return this.doProcess(null, param, VGetRequestState8.class);
		
	}
}

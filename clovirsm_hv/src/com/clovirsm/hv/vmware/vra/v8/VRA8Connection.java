package com.clovirsm.hv.vmware.vra.v8;

import java.util.Date;
import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VRA8Connection extends VRAConnection {
	@Override
	/*public void connect(String url, String userId, String pwd, Map prop) throws Exception {
		this.url = url;
		 
		client = new RestClient(url);
		client.setMimeType("application/x-www-form-urlencoded");
		client.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		String param = "refresh_token=" + userId;
		client.setMimeType("application/x-www-form-urlencoded");
		JSONObject result = (JSONObject)client.post("/csp/gateway/am/api/auth/api-tokens/authorize", param);
		expire = new Date();
		expire.setSeconds(expire.getSeconds()+ result.getInt("expires_in"));
		
		token = result.getString("access_token");
		client.setToken(token);
		client.setMimeType("application/json");
		if(prop.get("api_url") != null) {
			client.setBaseUrl( (String)prop.get("api_url"));
		}
		connect = true;
	}*/
	public void connect(String url, String userId, String pwd, Map prop) throws Exception {
		this.url = url;
		 
		client = new RestClient(url);
		
		client.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		
		client.setMimeType("application/json");
		JSONObject json = new JSONObject();
		json.put("username", userId);
		json.put("password", pwd);
		JSONObject result = (JSONObject)client.post("/csp/gateway/am/api/login", json.toString());
		 
		
		token = result.getString("cspAuthToken");
		client.setToken(token);
		expire = new Date();
		expire.setMinutes(expire.getMinutes()+30);
		if(prop.get("api_url") != null) {
			client.setBaseUrl( (String)prop.get("api_url"));
		}
		connect = true;
	}
}

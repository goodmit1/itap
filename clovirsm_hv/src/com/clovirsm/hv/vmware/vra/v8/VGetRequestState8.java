package com.clovirsm.hv.vmware.vra.v8;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.client.utils.DateUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VGetRequestState8 extends VRACommon8{

	public VGetRequestState8(VRAConnection conn) {
		super(conn);
		 
	}
/*
 * https://api.mgmt.cloud.vmware.com/deployment/api/deployments/28e0e409-75f3-4222-a18e-5634e256ccf4?expandProject=true&expandResources=true&expandLastRequest=true
 * GET
 * {"id":"28e0e409-75f3-4222-a18e-5634e256ccf4","name":"test2","description":"test","orgId":"a2d81171-bdda-444e-8269-dcf6f0d5c3f9","blueprintId":"dd7907dd-a0ca-4631-a3ec-34789fcdd566","createdAt":"2019-08-23T08:05:05.860Z","createdBy":"syk@goodmit.co.kr","lastUpdatedAt":"2019-08-23T08:05:05.863Z","lastUpdatedBy":"syk@goodmit.co.kr","inputs":{"cloud":"vmware","count":1,"image":"centos7new","flavor":"medium","ssh_key":"11111111111111111","password":"((secret:v1:AAFKJ+Kj26AnZ93tdhXwEiZYQAXUHS4ySnmQYRNomiMvThRD))","bitnami_bundle_url":"https://s3.amazonaws.com/bitnami-autobuild-ondemand/temporary/s3temp-bitnami-20190814095748-86377/provisioner-jenkins-2.164.2-5-bundle-vmware.tar.gz?AWSAccessKeyId=AKIAIZ44VOFCHNG7C2NQ&Expires=1566374268&Signature=TyK44%2BTdnkYXfcxDKC4kf7S1%2BnI="},"simulated":false,"resources":[],"project":{"id":"7709713a-9d5c-48ef-9599-33d471205c15","name":"clovircm"},"lastRequest":{"id":"d54abf9b-776e-4106-8b26-5c50a7cf577d","name":"Create","deploymentId":"28e0e409-75f3-4222-a18e-5634e256ccf4","createdAt":"2019-08-23T08:05:06.006Z","updatedAt":"2019-08-23T08:05:06.006Z","requestedBy":"syk@goodmit.co.kr","requester":"syk@goodmit.co.kr","blueprintId":"dd7907dd-a0ca-4631-a3ec-34789fcdd566","completedTasks":0,"totalTasks":8,"status":"INPROGRESS","inputs":{"cloud":"vmware","count":1,"image":"centos7new","flavor":"medium","ssh_key":"11111111111111111","password":"((secret:v1:AAFKJ+Kj26AnZ93tdhXwEiZYQAXUHS4ySnmQYRNomiMvThRD))","bitnami_bundle_url":"https://s3.amazonaws.com/bitnami-autobuild-ondemand/temporary/s3temp-bitnami-20190814095748-86377/provisioner-jenkins-2.164.2-5-bundle-vmware.tar.gz?AWSAccessKeyId=AKIAIZ44VOFCHNG7C2NQ&Expires=1566374268&Signature=TyK44%2BTdnkYXfcxDKC4kf7S1%2BnI="}},"expense":{"lastUpdatedTime":"2019-08-23T08:05:05.860Z"},"status":"CREATE_INPROGRESS"}
 */

	@Override
	public void run(Map param, Map result) throws Exception {
		String requestId = (String)param.get("VRA_REQUEST_ID");
		JSONObject requestState = null;
		try{
			requestState = super.client.getJSON("/deployment/api/deployments/" + requestId + "?expandProject=true&expandResources=true&expandLastRequest=true");
			System.out.println("=========================================== deploy result ========================================= ");
			System.out.println(requestState);
			System.out.println("=========================================== deploy result ========================================= ");
		}
		catch(Exception e) {
			Date date = (Date)param.get("INS_TMS");
			long diff = (new Date()).getTime() - date.getTime();
			e.printStackTrace();
			if(diff/1000/60 > 100) {
				result.put("FAIL_MSG", "Not found");
				result.put("TASK_STATUS_CD" , "F");
			}
			 
			return;
			/*String msg = e.getMessage();
			if(msg.startsWith("{")) {
				JSONObject msgJson = new JSONObject(msg);
				if(msgJson.getInt("status") == 404) {
					return;
				}
			}
			else {
				return;
			}*/
		}
		String status = requestState.getString("status");
		if(status.indexOf("INPROGRESS")<0)
		{
			if(status.indexOf("FAILED")<0)
			{
				
				boolean hasIP = getVMInfo(  result,  requestId,requestState);
				if(hasIP) { 
					result.put("TASK_STATUS_CD" , "S");
				}
			}
			else
			{
				result.put("FAIL_MSG", requestState.getJSONObject("lastRequest").getString("statusDetails"));
				result.put("TASK_STATUS_CD" , "F");
			}
		}
		
		
	}
	
	private Map getDCIps() throws Exception {
		JSONObject json = super.client.getJSON("/provisioning/uerp/provisioning/mgmt/endpoints?expand&$filter=((((customProperties.__facade_endpoint_type%20ne%20%27vmc%27)%20and%20(endpointType%20ne%20%27cmx.openshift-endpoint%27))%20and%20(endpointType%20ne%20%27abx.endpoint%27))%20and%20(customProperties.isExternal%20ne%20%27true%27))&$orderby=name%20asc&$top=100&$skip=0");
		JSONObject documents = json.getJSONObject("documents");
		Iterator<String> it = documents.keys();
		Map result  = new HashMap();
		while(it.hasNext()) {
			String k = it.next();
			JSONObject document = documents.getJSONObject(k);
			result.put(document.getString("name"), document.getJSONObject("endpointProperties").getString("hostName"));
		}
		return result;
	}
	private String getOBJId(String id) {
		int pos = id.indexOf(":");
		return id.substring(pos+1);
	}
	private Map getTag(JSONObject properties) {
		Map result = new HashMap();
		if(properties.has("tags")) {
			JSONArray tags = properties.getJSONArray("tags");
			for(int i=0; i < tags.length(); i++) {
				result.put(tags.getJSONObject(i).getString("key"),tags.getJSONObject(i).getString("value"));
			}
		}
		return result;
	}
	private void getSWInfo() throws Exception{
		
	}
	private Map getDiskInfo(String name, int gb, String ds) {
		Map m = new HashMap();
		m.put("DISK_NM", name);
		m.put("DISK_SIZE", gb);
		//m.put("DISK_PATH", ds);
		return m;
	}
	protected JSONObject getInfoByResouceLink(JSONArray resources, String link) {
		for(int i=0; i < resources.length(); i++) {
			JSONObject resource = resources.getJSONObject(i);
			JSONObject properties = resource.getJSONObject("properties");
			if(link.endsWith(properties.getString("resourceId"))) {
				return properties;
			}
		}
		return null;
	}
	protected List getDiskInfo(JSONArray resources, JSONObject resource) {
		JSONArray disks = resource.getJSONObject("storage").getJSONArray("disks");
		List vmDisk = new ArrayList();
		for(int j=0; j < disks.length(); j++) {
			if("HDD".equals( disks.getJSONObject(j).getString("type"))){
				vmDisk.add(getDiskInfo(disks.getJSONObject(j).getString("name"), disks.getJSONObject(j).getInt("capacityGb"), disks.getJSONObject(j).getString("vcUuid")));
			}
		}
		if(resource.has("attachedDisks")) {
			JSONArray attachedDisks = resource.getJSONArray("attachedDisks");
			for(int j=0; j < attachedDisks.length(); j++) {
				JSONObject disk = getInfoByResouceLink(resources, attachedDisks.getJSONObject(j).getString("source"));
				vmDisk.add(getDiskInfo(disk.getString("name"), disk.getInt("capacityGb"), disk.getString("vcUuid")));
			}
		}
		return vmDisk;
		
	}
	private boolean getVMInfo(Map result, String requestId,JSONObject requestState) throws Exception {
		JSONArray resources = requestState.getJSONArray("resources");
		Map swMap = VRA8Util.getSWInfo4Deploy(requestState, null);
		List list = new ArrayList();
		Map dcIps = getDCIps();
		for(int i=0; i < resources.length(); i++) {
			JSONObject resource = resources.getJSONObject(i);
			if(resource.get("type").equals("Cloud.vSphere.Machine") || resource.get("type").equals("Cloud.Machine")  ) {
				Map m = new HashMap();
				JSONObject properties = resource.getJSONObject("properties");
				m.put("HV_CD", "V");
				m.put("VM_NM", properties.getString("resourceName"));
				m.put("CPU_CNT", properties.getInt("cpuCount"));
				m.put("OS_ID", properties.getString("image"));
				if(properties.has("flavor")) {
					m.put("SPEC_ID", properties.getString("flavor"));
				}
				else {
					m.put("SPEC_ID", "0");
				}
				m.put("DS_NM", properties.getString("datastoreName"));
				m.put("GUEST_NM", properties.getString("softwareName"));
				if(properties.has("vm")) {
					String vm = properties.getString("vm");
					m.put("VM_HV_ID", getOBJId(vm));
				}
				m.put("tags", getTag(properties));
				m.put("DC_HV_ID", getOBJId(properties.getString("region")));
				m.put("DC_IP", dcIps.get(properties.getString("account")));
				if(!"".equals(properties.getString("address"))) {
					m.put("PRIVATE_IP", properties.getString("address"));
				}
				else {
					try {
						m.put("PRIVATE_IP", properties.getJSONArray("networks").getJSONObject(0).getString("address"));
					}
					catch(Exception e) {
						
					}
				}
				m.put("RESOURCE_ID", properties.getString("resourceId"));
				m.put("sws", swMap.get(properties.getString("resourceName")));
				m.put("disks", getDiskInfo(resources, properties));
				if(m.get("PRIVATE_IP")==null || "".equals(m.get("PRIVATE_IP"))) {
					return false;
				}
				list.add(m);
				
			}
			/*
			
				m.put("VM_HV_ID", data.getString("EXTERNAL_REFERENCE_ID"));
				m.put("CPU_CNT", data.getInt("MachineCPU"));
				m.put("RAM_SIZE", data.getInt("MachineMemory"));
				m.put("VM_NM", data.getString("MachineName"));
				m.put("DISK_SIZE", data.getInt("MachineStorage"));
				m.put("PRIVATE_IP", data.getString("ip_address"));
				m.put("RESOURCE_ID", resource1.getString("resourceId"));
			 */
			result.put("VM_LIST", list);
		}
		return true;
	}
	
}

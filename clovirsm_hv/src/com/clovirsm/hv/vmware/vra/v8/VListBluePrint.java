package com.clovirsm.hv.vmware.vra.v8;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jettison.json.JSONException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.YamlUtil;
import com.clovirsm.hv.vmware.vra.VRACommon;
import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VListBluePrint  extends VRACommon8{

	public VListBluePrint(VRAConnection conn) {
		super(conn);
		 
	}

	protected void getIcon(org.codehaus.jettison.json.JSONObject json, Map m) throws JSONException {
		if(json.has("icon")) {
			m.put("ICON",json.getString("icon"));
		}
		else {
			//m.put("ICON","/res/img/deployment-default.svg");
		}
	}
	@Override
	public void run(Map param, Map result) throws Exception {
		JSONObject json = (JSONObject) super.client.get("/blueprint/api/blueprints?page=0&size=10000");
		
		JSONArray arr = json.getJSONArray("content");
		List list = new ArrayList();
		ParseParam parseParam = new ParseParam();
		boolean isRelease = param.get("DRAFT") == null;
		for(int i=0; i < arr.length(); i++)
		{
			JSONObject json1 = arr.getJSONObject(i);
			Map m = new HashMap();
			 
			System.out.println(json1.getString("name") + "======================");
			m.put("CATALOG_NM", json1.getString("name"));
			m.put("CATALOG_ID", json1.getString("id"));
			
			m.put("CATALOG_CMT", json1.getString("description"));
			m.put("INS_TMS", super.client.parseDate(json1.getString("createdAt")));
			m.put("KUBUN","A");
			
			JSONObject blueprint = getBluePrint( json1.getString("id"), isRelease);
			if(blueprint == null) {
				continue;
			}
			if(blueprint.has("version")) {
					
				 m.put("VER", blueprint.getString("version"));
			}
			m.put("STEP_INFO", parseParam.run(blueprint.getString("content")).values());
			
			org.codehaus.jettison.json.JSONObject yaml = parseParam.getJSON();
			getIcon(yaml,m);		
			m.put("FORM_INFO", yaml.toString());
			list.add(m);
		}
		result.put("LIST", list);
	}
	protected JSONObject getBluePrint(String id, boolean isRelease) throws Exception{
		if(isRelease) {
			JSONObject res = client.getJSON("/blueprint/api/blueprints/" + id + "/versions");
			JSONArray contents = res.getJSONArray("content");
			for(int i=0; i<contents.length();i++) {
				if("RELEASED".equals( contents.getJSONObject(i).getString("status"))){
					return client.getJSON("/blueprint/api/blueprints/" + id + "/versions/" + contents.getJSONObject(i).getString("version"));
				}
			}
			return null;
		}
		else {
			return client.getJSON("/blueprint/api/blueprints/" + id);
		}
	}
	
	
}

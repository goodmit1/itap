package com.clovirsm.hv.vmware.vra.v8;

 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VListImage  extends VRACommon8{

	public VListImage(VRAConnection conn) {
		super(conn);
		 
	}

	private long getDiskSize(JSONArray diskConfigs) {
		long total = 0;
		for(int i=0; i < diskConfigs.length(); i++) {
			total += diskConfigs.getJSONObject(i).getInt("capacityMBytes");
		}
		return total / 1024;
	}
	@Override
	public void run(Map param, Map result) throws Exception {
		JSONArray list = (JSONArray)super.client.get("/provisioning/uerp/provisioning/mgmt/image-names?view=list");
		List nameList = new ArrayList();
		for(int i=0; i < list.length(); i++) {
			Map m = new HashMap();
			m.put("OS_NM", list.getJSONObject(i).getString("name"));
			
			JSONObject imageMapping = list.getJSONObject(i).getJSONObject("imageMapping");
			try {
				Map template = templateDetail(imageMapping);
				m.put("LINUX_YN", template.remove("LINUX_YN"));
				m.put("template", template);
				nameList.add( m);
			}
			catch(Exception e) {
				System.out.println(list.getJSONObject(i).getString("name") + ":" + e.getMessage());
			}
		}
		
		result.put("LIST", nameList);
	}
	
	private Map templateDetail(JSONObject imageMapping) throws Exception {
		Map result = new HashMap();
		Iterator  keys = imageMapping.keys();
		while(keys.hasNext()) {
			String imageLink = imageMapping.getJSONObject((String)keys.next()).getString("imageLink");
			JSONObject imageInfo = super.client.getJSON("/provisioning/uerp" + imageLink);
			result.put("LINUX_YN", "LINUX".equals(imageInfo.get("osFamily")) ? "Y":"N");
			result.put(imageInfo.get("name"), getDiskSize(imageInfo.getJSONArray("diskConfigs")));
			 
		}
		return result;
	}

}

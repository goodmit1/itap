package com.clovirsm.hv.vmware.vra;

import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.IAfterProcess;
import com.jayway.jsonpath.JsonPath;
  
public class VCatalogRequest extends VRACommon{

	public VCatalogRequest(VRAConnection conn) {
		super(conn);
		 
	}

	@Override
	public void run(Map param, Map result ) throws Exception {
		JSONObject json = client.getJSON(  "/catalog-service/api/consumer/entitledCatalogItems/" + param.get("CATALOG_ID") + "/requests/template" );
		
		FormGen gen = VRAAPI.getFormGet();
		Object data = param.get("DATA_JSON");
		JSONObject userdata = null;
		if(data instanceof String){
			userdata = new JSONObject((String)param.get("DATA_JSON"));
		}
		else
		{
			userdata = (JSONObject)data;
		}
		
		if(param.get("PURPOSE") != null)
		{
			userdata.put("PURPOSE", param.get("PURPOSE"));
		}
		if(param.get("CMT") != null)
		{
			userdata.put("CMT", param.get("CMT"));
		}
		String requestData = gen.putVal(json, userdata );
		try
		{
			JSONObject response = (JSONObject)client.post("/catalog-service/api/consumer/entitledCatalogItems/" + param.get("CATALOG_ID") + "/requests", requestData);
			//{"@type":"CatalogItemRequest","id":"6c4ad903-4f5a-45fa-ad56-fcd3d129a7a5","iconId":"composition.blueprint.png","version":0,"requestNumber":null,"state":"SUBMITTED","description":null,"reasons":null,"requestedFor":"configurationadmin@vsphere.local","requestedBy":"configurationadmin@vsphere.local","organization":{"tenantRef":"vsphere.local","tenantLabel":null,"subtenantRef":"550fc363-0259-4a2a-9cff-516e517da9f5","subtenantLabel":null},"requestorEntitlementId":"6a6d74b3-600c-46da-8b9e-d95ba34df8bc","preApprovalId":null,"postApprovalId":null,"dateCreated":"2018-11-07T05:12:18.543Z","lastUpdated":"2018-11-07T05:12:18.543Z","dateSubmitted":"2018-11-07T05:12:18.543Z","dateApproved":null,"dateCompleted":null,"quote":{"leasePeriod":null,"leaseRate":null,"totalLeaseCost":null},"requestCompletion":null,"requestData":{"entries":[{"key":"subtenantId","value":{"type":"string","value":"550fc363-0259-4a2a-9cff-516e517da9f5"}},{"key":"_leaseDays","value":null},{"key":"providerBindingId","value":{"type":"string","value":"test"}},{"key":"providerId","value":{"type":"string","value":"380b2652-fb08-4610-9d66-6cfd19ac0a75"}},{"key":"Win2012R2NoTools_1","value":{"type":"complex","componentTypeId":"com.vmware.csp.component.cafe.composition","componentId":null,"classId":"Blueprint.Component.Declaration","typeFilter":"test*Win2012R2NoTools_1","values":{"entries":[{"key":"Win2012R2-NoTools_v2","value":{"type":"complex","componentTypeId":"com.vmware.csp.component.cafe.composition","componentId":null,"classId":"Blueprint.Component.Declaration","typeFilter":"Win2012R2NoTools*Win2012R2-NoTools_v2","values":{"entries":[{"key":"snapshot_name","value":null},{"key":"memory","value":{"type":"integer","value":4096}},{"key":"disks","value":{"type":"multiple","elementTypeId":"COMPLEX","items":[{"type":"complex","componentTypeId":"com.vmware.csp.iaas.blueprint.service","componentId":null,"classId":"Infrastructure.Compute.Machine.MachineDisk","typeFilter":null,"values":{"entries":[{"key":"is_clone","value":{"type":"boolean","value":true}},{"key":"initial_location","value":{"type":"string","value":""}},{"key":"volumeId","value":{"type":"string","value":"0"}},{"key":"custom_properties","value":null},{"key":"id","value":{"type":"integer","value":1541075844637}},{"key":"label","value":{"type":"string","value":"Hard disk 1"}},{"key":"userCreated","value":{"type":"boolean","value":false}},{"key":"storage_reservation_policy","value":{"type":"string","value":""}},{"key":"capacity","value":{"type":"integer","value":40}}]}}]}},{"key":"ovf_use_proxy","value":{"type":"boolean","value":false}},{"key":"description","value":null},{"key":"storage","value":{"type":"integer","value":40}},{"key":"guest_customization_specification","value":null},{"key":"_hasChildren","value":{"type":"boolean","value":false}},{"key":"os_distribution","value":null},{"key":"reservation_policy","value":null},{"key":"max_network_adapters","value":{"type":"integer","value":-1}},{"key":"max_per_user","value":{"type":"integer","value":0}},{"key":"nics","value":null},{"key":"ovfAuthNeeded","value":{"type":"boolean","value":false}},{"key":"source_machine_vmsnapshot","value":null},{"key":"display_location","value":{"type":"boolean","value":false}},{"key":"os_version","value":null},{"key":"os_arch","value":{"type":"string","value":"x86_64"}},{"key":"cpu","value":{"type":"integer","value":1}},{"key":"datacenter_location","value":null},{"key":"property_groups","value":null},{"key":"ovf_proxy_endpoint","value":null},{"key":"_cluster","value":{"type":"integer","value":1}},{"key":"security_groups","value":{"type":"multiple","elementTypeId":"ENTITY_REFERENCE","items":[]}},{"key":"ovf_url_username","value":null},{"key":"ovf_url","value":null},{"key":"os_type","value":{"type":"string","value":"Linux"}},{"key":"ovf_url_pwd","value":null},{"key":"source_machine_external_snapshot","value":null},{"key":"security_tags","value":{"type":"multiple","elementTypeId":"ENTITY_REFERENCE","items":[]}}]}}},{"key":"_hasChildren","value":{"type":"boolean","value":false}},{"key":"_leaseDays","value":null},{"key":"_number_of_instances","value":{"type":"integer","value":1}}]}}},{"key":"_number_of_instances","value":{"type":"integer","value":1}}]},"retriesRemaining":3,"requestedItemName":"test","requestedItemDescription":"","components":null,"successful":false,"final":false,"stateName":null,"catalogItemRef":{"id":"599e9cf3-cf31-4e2f-aa71-51b4fee26490","label":"test"},"catalogItemProviderBinding":{"bindingId":"vsphere.local!::!test","providerRef":{"id":"380b2652-fb08-4610-9d66-6cfd19ac0a75","label":"Blueprint Service"}} 
			

			param.put("VRA_REQUEST_ID", response.getString("id"));
			
		}
		catch(Exception e)
		{
			try
			{
				System.out.println(e.getMessage());
				String[] msgs = JsonPath.parse( e.getMessage()).read("$..message");
				StringBuffer sb = new StringBuffer();
				for(String m : msgs)
				{
					if(sb.length()>0) sb.append(",");
					sb.append(m);
				}
				throw new Exception( sb.toString() );
			}
			catch(Exception ignore)
			{
				throw e;
			}
			
			
			
		}
		
	}

}

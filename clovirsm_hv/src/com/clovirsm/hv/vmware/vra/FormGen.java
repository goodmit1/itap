package com.clovirsm.hv.vmware.vra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.HVProperty;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;

public class FormGen {
	
	 
 
 
	JSONObject myForm;
	int ipCnt ;
	List dcIds ;
	JSONArray jspTemplateArr;
	public int getIpCnt()
	{
		return ipCnt;
	}
	public FormGen() throws Exception
	{
		myForm = HVProperty.getInstance().getJsonObject("vra_request");
		ipCnt = 0;
	}
	public FormGen(JSONObject myForm)  
	{
		this.myForm = myForm;
		ipCnt = 0;
	}
	public JSONArray getJspTemplateArr()
	{
		return jspTemplateArr;
	}
	protected int getMin(DocumentContext context, String id)
	{
		List val = context.read("$.[?(@.id=='" + id + "')].state.facets[?(@.type=='minValue')].value.value.value");
		if(val != null)
		{
			return (int)val.get(0);
		}
		return -1;
	}
	protected int getMax(DocumentContext context, String id)
	{
		List val = context.read("$.[?(@.id=='" + id + "')].state.facets[?(@.type=='maxValue')].value.value.value");
		if(val != null)
		{
			return (int)val.get(0);
		}
		return -1;
	}
	private String getId(String path)
	{
		if( path.endsWith("']"))
		{
			int pos = path.lastIndexOf("['");
			return path.substring(pos+2,  path.length()-2);
		}
		else
		{
			int pos = path.lastIndexOf(".");
			return path.substring(pos+1);
		}
		
	}
	 
	public String formGen(JSONObject template, JSONObject schema  )
	{
		dcIds = new ArrayList();
		Iterator<String> it = myForm.keys();
		List<String[]> fields = new ArrayList();
		DocumentContext context = JsonPath.parse(template.toString());
		while(it.hasNext())
		{
			String myKey = it.next();
			JSONObject myItem = myForm.getJSONObject(myKey);
			if(!myItem.has("visible") || myItem.getBoolean("visible"))
			{
				int order = myItem.getInt("order");
				int len = fields.size();
				for(int i=0; i < order-len+1; i++)
				{
					fields.add(new String[]{});
				}
				String path =  myItem.getJSONArray("field").getString(0);
				String id = getId(path);
				fields.set(order, new String[]{myKey,id});
				if(myKey.equals("PRIVATE_IP"))
				{
					 
					try
					{
						List  defaultVals = context.read( path);
						ipCnt = defaultVals.size();
					}
					catch(PathNotFoundException ignore)
					{
						
					}
				}
			}
		}
		 
		
		StringBuffer sb = new StringBuffer();
		JSONArray schemaFields = schema.getJSONArray("fields");
		jspTemplateArr = new JSONArray();
		for(int i=0; i < schemaFields.length(); i++)
		{
			JSONObject schemaField = schemaFields.getJSONObject(i);
			if(!"complex".equals(schemaField.getJSONObject("dataType").getString("type"))) continue;
			
			String sectionId = schemaField.getString("id");
			String sectionTitle = schemaField.getString("label");
			sb.append("<div class='section' id='" + sectionId + "' title='" + sectionTitle + "'>");
			DocumentContext schemacontext = JsonPath.parse(schemaFields.getJSONObject(i).toString());
			for(String arr[] :fields)
			{
				String myKey = arr[0];
				String svrKey = arr[1];
				if(myKey.equals("CPU_CNT"))
				{
					net.minidev.json.JSONArray result = schemacontext.read("$..schema.fields[?(@.id=='" + svrKey + "')]");
					if(result != null && result.size()>0)
					{
						sb.append(this.getSpecSelHtml(sectionId));
						net.minidev.json.JSONArray memory = schemacontext.read("$..schema.fields[?(@.id=='memory')]");
						net.minidev.json.JSONArray storage = schemacontext.read("$..schema.fields[?(@.id=='storage')]");
						jspTemplateArr.put(this.getJspTemplateArr("spec",sectionId,  new JSONObject((Map)result.get(0)),  new JSONObject((Map)memory.get(0)),    new JSONObject((Map)storage.get(0))));
					}
				}
				else
				{
					net.minidev.json.JSONArray result = schemacontext.read("$..schema.fields[?(@.id=='" + svrKey + "')]");
					if(result != null && result.size()>0)
					{
						JSONObject field = new JSONObject((Map)result.get(0));
						sb.append(this.getHtml(field, myKey));
					}
					else
					{
						System.out.println(svrKey + " not found"); 
					}
				}
				 
			}
			dcIds.add( ((net.minidev.json.JSONArray)schemacontext.read("$..schema.fields[?(@.id=='source_machine')].state.facets[*].value.value.id")).get(0));
			sb.append("</div>");
		}
		
	 
		return sb.toString();
		
	}
	public List getDCIds()
	{
		return dcIds;
	}
	
	private void putMinMaxDefault(JSONObject t, JSONObject cpuInfo, String kubun)
	{
		JSONObject kubunObj = new JSONObject();
		JSONArray facets = cpuInfo.getJSONObject("state").getJSONArray("facets");
		for(int i=0; i < facets.length(); i++)
		{
			JSONObject json =  facets.getJSONObject(i);
			String type = json.getString("type");
			try
			{
				kubunObj.put(type, json.getJSONObject("value").getJSONObject("value").getInt("value"));
			}
			catch(Exception ignore)
			{
				
			}
		}
		t.put(kubun, kubunObj);
	}
	private JSONObject getJspTemplateArr(String kubun, String sectionId, JSONObject cpuInfo,   JSONObject memoryInfo, JSONObject storage) {
		 
		JSONObject t = new JSONObject();
		t.put("id", sectionId);
		t.put("type",kubun);
		t.put("url", "/clovirsm/resources/vra/template/spec_select.jsp");
		putMinMaxDefault(t , cpuInfo,"cpu");
		putMinMaxDefault(t , memoryInfo,"memory");
		putMinMaxDefault(t , storage,"disk");
		return t;
	}
	private Object getSpecSelHtml(String id) {
		 
		return "@template:" + id + "@";
	}
	protected void setVal(DocumentContext context, String path, Object newVal)
	{
		
		if(newVal instanceof String || newVal instanceof Integer)
		{
			context.set(  path, newVal);
		}
		else if(newVal instanceof JSONArray)
		{
			
			MyPredicate predicate = new MyPredicate();
			
			JSONArray arr = (JSONArray)newVal;
			for(int i=0; i < arr.length(); i++)
			{
				path = path.replaceAll("\\*", "*[?]");
				predicate.setidx(i);
				context.set(path, arr.getString(i), predicate);
			}
		}
	}
	protected void setVal(DocumentContext context, String path, Integer newVal)
	{
		context.set(  path, newVal);
	}
	protected void setVal(DocumentContext context, String path, List list)
	{
		Object newVal = list.remove(0);
		context.set(  path, newVal);
	}
	
	/**
	 *
	 * @param template
	 * @param userdata {"RequestNetwork-Win2012R2":{"SPEC_NM":"저사양","SPEC_ID":"277196042338240","CPU_CNT":"2","RAM_SIZE":"8","DISK_SIZE":"100","DISK_UNIT":"G"},"db":{"SPEC_NM":"M0404","SPEC_ID":"2081127227641642","CPU_CNT":"4","RAM_SIZE":"4","DISK_SIZE":"80","DISK_UNIT":"G"}}
	 * @return
	 */
	
	public String putVal(JSONObject template, JSONObject userdata)
	{
		Iterator<String> it = userdata.keys();
		DocumentContext context = JsonPath.parse(template.toString());
		
		while(it.hasNext()) // user data
		{
			String key = it.next();
			Object val = userdata.get(key);
			if(key.equals("fields"))
			{
				JSONObject valJson = (JSONObject)val;
				Set<String> valKeys = valJson.keySet();
				for(String valKey:valKeys)
				{
					context.set("$.data." + valKey, valJson.get(valKey));
					 
				}
			}
			else
			{
				
				if(val instanceof JSONObject)
				{
					
					Iterator userdatakeys = ((JSONObject) val).keys();
					while(userdatakeys.hasNext())
					{
						String key1 = (String)userdatakeys.next();
						Object val1 =  ((JSONObject) val).getString(key1);
						if(!myForm.has(key1)) continue;
						JSONObject myItem = myForm.getJSONObject(key1);
						
						JSONArray myFieldList = myItem.getJSONArray("field");
						for(int i=0; i < myFieldList.length(); i++)
						{
							String path = myFieldList.getString(i);
							path = "$.data." + key +  path.substring(1);
									
							setVal(context, path, val1); 
						}
					}
				}
				else
				{
				
					JSONObject item = myForm.getJSONObject(key);
					JSONArray fieldList = item.getJSONArray("field");
					for(int i=0; i < fieldList.length(); i++)
					{
						setVal(context, fieldList.getString(i), val); 
					}
				}
			}
		}
		return context.jsonString();
		
	}
	private Object getFacets(DocumentContext context, String id, Object defaultVal)
	{
		try
		{
			 return ((net.minidev.json.JSONArray)context.read("$.facets[?(@.type=='" + id + "')].value.value.value")).get(0);
		}
		catch(Exception e)
		{
			return defaultVal;
		}
	}
	protected String getHtml(JSONObject field, String id )
	{
		if(id == null) id = field.getString("id");
		String label = field.getString("label");
		//String description = field.getString("description");
		String classId = null; 
		try
		{
			if(field.getJSONObject("dataType").has("classId") )
			{
				classId = field.getJSONObject("dataType").getString("classId");
			}
			else
			{
				classId = field.getJSONObject("dataType").getString("typeId");
			}
		}
		catch(Exception ignore){}
		JSONObject state = field.getJSONObject("state");
		DocumentContext context = JsonPath.parse(state.toString());
		boolean editable = (boolean)getFacets(context, "editable",  true);
		if(!editable) return "";
		boolean required = (boolean)getFacets(context, "mandatory",  false);
		
		Object max = getFacets(context, "maxValue",  null);
		Object min = getFacets(context, "minValue",  null);
		Object defaultVal = getFacets(context, "defaultValue",  null);
			
		boolean isMultiValued =  field.getBoolean("isMultiValued");
		
		
		StringBuffer sb = new StringBuffer();
		sb.append("<div>");
		sb.append("<label >" + label + "</label>");
		sb.append("<input title='" + label + "' name='" + id + "' classId='" + classId + "' isMultiValued='" + isMultiValued + "'");
		if(required)
		{
			sb.append(" required='required'" );
		}
		if(defaultVal != null)
		{
			sb.append(" value='" + defaultVal + "'");
		}
		if(min != null)
		{
			sb.append(" minValue='" + min + "'");
		}
		if(max != null)
		{
			sb.append(" maxValue='" + max + "'");
		}
		sb.append( "></input>");
		sb.append("</div>");
		return sb.toString();
	}
	public String formGenCustom(JSONArray fields)
	{
		StringBuffer sb = new StringBuffer();
		for(int i=0; i < fields.length(); i++)
		{
			
			JSONObject field = fields.getJSONObject(i);
			sb.append(getHtml(field, null));
			
		}
		return sb.toString();
	
	}
	 
}


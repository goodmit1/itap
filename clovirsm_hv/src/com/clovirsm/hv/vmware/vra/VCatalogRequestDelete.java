package com.clovirsm.hv.vmware.vra;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.IAfterProcess;
  
public class VCatalogRequestDelete extends VRACommon{

	public VCatalogRequestDelete(VRAConnection conn) {
		super(conn);
		 
	}

	@Override
	public void run(Map param, Map result ) throws Exception {
		JSONObject json = null;
		try
		{
			json = client.getJSON(  "/catalog-service/api/consumer/requests/" + param.get("VRA_REQUEST_ID")  +"/resourceViews" );
		}
		catch(Exception e)
		{
			 
			return;
		}
		JSONArray content = json.getJSONArray("content");
		for(int i=0; i < content.length(); i++)
		{
			JSONArray links = content.getJSONObject(i).getJSONArray("links");
			for(int j=0;j < links.length(); j++)
			{
				JSONObject link = links.getJSONObject(j);
				String rel = link.getString("rel");
				if(rel.toLowerCase().indexOf("destroy")>0 && rel.indexOf("GET")>=0)
				{
					String href = link.getString("href");
					int pos = href.indexOf("/", 10);
					int pos1 = href.lastIndexOf("/");
					JSONObject template = client.getJSON(href.substring(pos));
					Object result1 = client.post(href.substring(pos,pos1), template.toString());
					break;
				}
			}
		}
		System.out.println(json);
	}

}

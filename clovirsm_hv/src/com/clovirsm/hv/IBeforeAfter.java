package com.clovirsm.hv;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.ISite;
 
/**
 * Hypervisor VM생성전, 오브젝트 생성 후 작업 정의
 * @author 윤경
 *
 */
public interface IBeforeAfter extends ISite{




	 




	List<IPInfo> getNextIp( int firstNicId,  Map param) throws Exception;


	/**
	 * VM 생성 전
	 * @param isNew DC에 최초 생성 여부
	 * @param param
	 * @return 
	 * @throws Exception
	 */
	boolean onBeforeCreateVM(boolean isNew, Map param) throws Exception;






	 

	void onChangeDCInfo(Map param) throws Exception;






	Runnable getAfterCreateThread(String kubun, Map param) throws Exception;




	String getVMName(Map vm) throws Exception;

	String[] getVMNames(Map vm,int count) throws Exception;


 




	String getImgName(Map param) throws Exception;




	 



	boolean onAfterGetVM(Map result) throws Exception;




	int chgNaming(Object categoryId, String naming) throws Exception;




	void onAfterFWDeploy(Map info) throws Exception;




	void onAfterDeleteVM(Map param);




	boolean reconfigVM(String pkVal, String instDt, Map param) throws Exception;




	boolean deleteVM(String pkVal, String instDt, Map param) throws Exception;




	String getFolderName(Map param) throws Exception;


	 


	void putVraDataJson(JSONObject dataJSON, org.codehaus.jettison.json.JSONObject formJSON, Map param) throws Exception;


	








	 


 

}

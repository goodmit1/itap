package com.clovirsm.hv;

import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.json.JSONObject;
import org.json.JSONTokener;

public class HVProperty {


		private static HVProperty ms_codeHelper = null;
		private Properties m_codeTable = null;
		private Map<String, JSONObject> jsons;
		private HVProperty() throws Exception
		{
			load();
		}
		public JSONObject getJsonObject(String name) throws Exception
		{
			if(jsons == null)
			{
				jsons = new HashMap();
				
			}
			JSONObject json = jsons.get(name);
			if(json==null)
			{
				Reader reader =  (new InputStreamReader(this.getClass().getResourceAsStream("/" + name + ".json"), "UTF-8"));
				JSONTokener tokener = new JSONTokener(reader);
				json = new JSONObject(tokener);
				jsons.put(name, json);
			}
			return json;
		}
		public String getProperty(String key, String def)
		{
			String v =  m_codeTable.getProperty(key);
			if(v == null)
			{
				return def;
			}
			else
			{
				return v;
			}
		}
		public String getProperty(String key)
		{
			return m_codeTable.getProperty(key);
		}
		public int getInt(String key, int defaultVal)
		{
			String t= m_codeTable.getProperty(key);
			if(t==null) return defaultVal;
			return Integer.parseInt(t);
		}
		public String[] getStrings(String key)
		{
			String t= m_codeTable.getProperty(key);
			if(t==null) return null;
			return t.split(",");
		}
		public HypervisorAPI getAPI(String hvType) throws Exception
		{
			String className = m_codeTable.getProperty("hv.class." + hvType);
			if(className == null) {
				return new DummyAPI();
			}
			return (HypervisorAPI)Class.forName(className).newInstance();
		}
		public Object getOtherAPI(String hvType, Class defaultClass) throws Exception
		{
			String className = m_codeTable.getProperty("hv.class." + hvType);
			if(className == null) {
				return defaultClass.newInstance();
			}
			return  Class.forName(className).newInstance();
		}
		public void load() throws Exception
		{
			m_codeTable = new Properties();
			m_codeTable.load(new InputStreamReader(this.getClass().getResourceAsStream("/hv.properties"), "UTF-8"));
			try
			{
				Properties tmp = new Properties();
				tmp.load(new InputStreamReader(this.getClass().getResourceAsStream("/hv_site.properties"), "UTF-8"));
				m_codeTable.putAll(tmp);
			}
			catch(Exception ignore)
			{
				
			}
			//m_codeTable.load(this.getClass().getResourceAsStream("/hv.properties"));
			//System.out.println("properties : "+m_codeTable);
		}
		public static HVProperty getInstance() throws Exception
		{
			synchronized(HVProperty.class)
			{
				if(ms_codeHelper == null)
				{
					ms_codeHelper = new HVProperty();
				}
			}

			return ms_codeHelper;
		}
}

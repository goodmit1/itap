import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openstack4j.api.Builders;
import org.openstack4j.api.OSClient;
import org.openstack4j.model.compute.ActionResponse;
import org.openstack4j.model.compute.Flavor;
import org.openstack4j.model.compute.Image;
import org.openstack4j.model.compute.Server;
import org.openstack4j.model.compute.ServerCreate;
import org.openstack4j.model.network.Port;
import org.openstack4j.model.network.Subnet;
import org.openstack4j.openstack.OSFactory;

public class Delete extends Common {
	public void run()
	{
		connect();
		ActionResponse response = os.compute().servers().delete(super.getSvrId("syk"));
		if(!response.isSuccess())
		{
			System.out.println(response.getFault());
		}
	}
	 
	public static void main(String[] args)
	{
		Delete c = new Delete();
		c.run();
		 
	}
}

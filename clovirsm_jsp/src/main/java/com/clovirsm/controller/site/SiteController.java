package com.clovirsm.controller.site;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.VraCatalogMngService;
import com.clovirsm.service.resource.VraCatalogService;
import com.clovirsm.service.site.SiteService;
import com.clovirsm.site.HynixBeforeAfterVMWare;
import com.clovirsm.site.HynixNextApprover;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

import freemarker.template.utility.StringUtil;

@RestController
@RequestMapping(value =  "/site" )
public class SiteController  extends DefaultController {

	@Autowired SiteService service;
	@Override
	protected SiteService getService() {

		return service;
	}
	@RequestMapping({"/specList"})
	public Object getSpecList(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			Map param  = ControllerUtil.getParam(request);
			param.put("ID", param.get("P_KUBUN"));
			Map info = service.selectOneByQueryKey("com.clovirsm.common.Component","selectSpecSetting", param);
			String val1 = (String)info.get("VAL1");
			String arr[] = val1.split(",");
			List result = new ArrayList();
		
				 
			for(String a:arr) {
				 
				String[] t = StringUtil.split(a, '|');
				result.add(  getListItem( t[0] , t.length>1?t[1] : t[0],request.getParameter("FIELD")));
			}
			return result;
			 
		} catch (Exception e) {
			return ControllerUtil.getErrMap(e);
		}
		
	}
	private Map getListItem(Object id, String title, String keyField) {
		Map map = new HashMap();
		map.put(keyField, id);
		map.put(keyField + "_NM", title);
		return map;
	}
	@RequestMapping({"/itemListByCatalogInput"})
	public Object itemListByCatalogInput(HttpServletRequest request, HttpServletResponse response) {
		VraCatalogService  service = (VraCatalogService)SpringBeanUtil.getBean("vraCatalogService");
		try {
			Map param  = ControllerUtil.getParam(request);
			Map info = service.selectOneByQueryKey("getCatalogForBot", param);
			JSONArray inputs = new JSONArray((String)info.get("INPUTS"));
			JSONObject input =  null;
			for(int i=0; i < inputs.length(); i++) {
				if(inputs.getJSONObject(i).getString("name").equals(request.getParameter("FIELD"))) {
					input = inputs.getJSONObject(i);
					break;
				}
			}
			 
			List result = new ArrayList();
			if(input.has("enum")) {
				JSONArray enumArr = input.getJSONArray("enum");
				for(int i=0; i< enumArr.length(); i++) {
					result.add(getListItem( "" + enumArr.get(i), "" + enumArr.get(i),request.getParameter("FIELD")));
				}
				return input.getJSONArray("enum").toString();
			}
			else if(input.has("oneOf")) {
				JSONArray oneOf = input.getJSONArray("oneOf");
				for(int i=0; i< oneOf.length(); i++) {
					result.add(getListItem( oneOf.getJSONObject(i).getString("const"), oneOf.getJSONObject(i).getString("title"),request.getParameter("FIELD")));
				}
			}
			else if(input.has("maximum")) {
				int minimum = 1;
				int maximum = input.getInt("maximum");
				if(input.has("minimum")){
					minimum = input.getInt("minimum");
				}
				for(int i=minimum; i <= maximum ; i++) {
					result.add(getListItem( "" +   i, "" + i,request.getParameter("FIELD")));
				}
			}
			return result;
		} catch (Exception e) {
			return ControllerUtil.getErrMap(e);
		}
	}
	@RequestMapping({"/vra/mail_resend"})
	public Object vraMailReSend(HttpServletRequest request, HttpServletResponse response) {
		try {
			Map param  = ControllerUtil.getParam(request);
			HynixBeforeAfterVMWare beforeAfter = new HynixBeforeAfterVMWare();
			beforeAfter.sendVraSuccessMail(param, false);
			return ControllerUtil.getSuccessMap(param);
		} catch (Exception e) {
			return ControllerUtil.getErrMap(e);
		}
	}
	@Autowired VraCatalogService catalogService;
	@RequestMapping({"/catalogList"})
	public Object getCatalogList(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			Map param  = ControllerUtil.getParam(request);
			List<Map> result = new ArrayList();
			
			List<Map> list = service.selectListByQueryKey("com.clovirsm.resources.vra.VraCatalog","getCatalogForBot", param);
			for(Map m : list) {
				try	{
					String fixed = (String)m.get("FIXED_JSON");
					if(fixed == null) {
						JSONObject formJson = new JSONObject((String)m.get("FORM_INFO"));
						m.put("S_PURPOSE", formJson.getJSONObject("inputs").getJSONObject("_purpose").getString("default"));
					}
					else {
						JSONObject formJson = new JSONObject(fixed);
						m.put("S_PURPOSE", formJson.getString("_purpose"));
					}
					String dataJson = "{'_count_vm':1}";
					List<Map> summary = catalogService.getSummary((String)m.get("FORM_INFO"), dataJson, fixed,  0);
					if(summary.size()>0) {
						Map t = summary.get(0);
						MapUtil.copy(t, m, new String[] {"IMAGE",  "DISK_SIZE","CPU", "MEMORY", "FLAVOR","CATEGORY_NM"});
						List sws = (List)t.get("SW");
						if(sws.size()==0) {
							m.put("SW", "");
						}
						else {
							m.put("SW", sws.toString());
						}
					}
				}
				catch(Exception ignore) {
					
				}
				result.add(m);
			}
			return result;
		} catch (Exception e) {
			return ControllerUtil.getErrMap(e);
		}
		
	}
	 
	@RequestMapping({"/querykey/{queryKey}/"})
	public List querykey(HttpServletRequest request, HttpServletResponse response, @PathVariable("queryKey")final String queryKey) {
		return ((List) new ActionRunner() {
			protected Object run(Map param) throws Exception {
				return service.selectListByQueryKey(queryKey, param);
			}
		}.run(request));
	}

}

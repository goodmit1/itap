package com.clovirsm.controller.thinClnt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.site.thinClnt.ThinClntService;
import com.fliconz.fm.mvc.DefaultController;

@RestController
@RequestMapping(value =  "/thin_client" )
public class ThinClntController extends DefaultController {

	@Autowired ThinClntService service;
	@Override
	protected ThinClntService getService() {
		return service;
	}

}
package com.clovirsm.site;

import java.util.Map;

import com.clovirsm.service.site.SiteService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class GMobisBeforeAfterVMWare extends MobisBeforeAfterVMWare{

	public static void main(String[] args)
	{
		try {
			GMobisBeforeAfterVMWare w = new GMobisBeforeAfterVMWare( );
			System.out.println(w.getCnt0("ACBD00"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public GMobisBeforeAfterVMWare( ) throws Exception {
		super();
		 
	}
	@Override
	public String getFolderName(  Map param) throws Exception
	{
		String nm = super.getIdByTeam(param, "FOLDER");
		if(nm == null) {
			String code = super.getFolderName(param);
			return  getVMName(code, getCnt0(code), -1) ;
		}
		else {
			return nm;
		}
	}
		
	 
	 
	@Override
	protected int getCnt0(String code)
	{
		int cnt0 = 0;
		for(cnt0=0; cnt0 < code.length(); cnt0++)
		{
			if(code.charAt(code.length()-1-cnt0) != '0')
			{
				break;
			}
		}
		return cnt0;
	}
	
	@Override
	protected String getVMName(String code, int cnt0, int idx)
	{
		return  code.substring(0, code.length()-cnt0) + (idx>0 ?( TextHelper.lpad(idx, cnt0,'0') + "R"):"");
		 
	}
}

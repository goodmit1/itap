package com.clovirsm.site;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCReqService;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.vmware.vra.v8.VRAAPI8;
import com.clovirsm.service.AlarmService;
import com.clovirsm.service.ComponentService;
import com.clovirsm.service.admin.DcKubunConfService;
import com.clovirsm.service.resource.IPService;
import com.clovirsm.service.resource.VMService;
import com.clovirsm.service.resource.VraCatalogService;
import com.clovirsm.service.site.SiteService;
import com.clovirsm.service.workflow.DefaultNextApprover;
import com.clovirsm.service.workflow.DeployFailService;
import com.clovirsm.service.workflow.ExpireService;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.executor.GetVMInfos;
import com.clovirsm.sys.hv.executor.InsertMMStat;
import com.clovirsm.sys.hv.executor.ReconfigVM;
import com.clovirsm.sys.hv.vra.VRACommonAction;
import com.clovirsm.sys.hv.vra.VRADeleteVMReq;
import com.clovirsm.sys.hv.vra.VRAOnAfterRequest;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fm.util.IMailSender;
import com.fliconz.fw.runtime.util.DateUtil;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

 

public class HynixBeforeAfterVMWare extends SiteBeforeAfterVMWare{
	public org.apache.log4j.Logger varResultlog = LogManager.getLogger("vra_result");
	
	@Override
	public void onAfterCollect(String table, Map info) throws Exception {
		if(table.equals("NC_IP")) {
			insertIpFromFile() ;
		}
		else if(table.equals("NC_VRA_CATALOG")) {
			List list = (List)info.get(HypervisorAPI.PARAM_LIST);
			onAfterCollectCatalog(list);
		}
		else if(table.equals("NC_VM_TAG")) {
			notiVMDup() ;
		}
		else if(table.equals("NC_ETC_FEE")) {
			InsertMMStat insertMMStat = (InsertMMStat)SpringBeanUtil.getBean("insertMMStat");
			String yyyyMM = DateUtil.format(new Date(), "yyyyMM");
			insertMMStat.runHasDetail( yyyyMM);
			 
		}
	}
	private void notiVMDup() throws Exception {
		SiteService service = (SiteService)SpringBeanUtil.getBean("siteService");
		AlarmService alarmService = (AlarmService)SpringBeanUtil.getBean("alarmService");
		Map param = new HashMap();
		List<Map> list = service.selectListByQueryKey("selectDupVM4Noti", param);
		int day = Integer.parseInt((String) ComponentService.getEnv("send_control_term_day", "7"));
		org.apache.log4j.Logger log = LogManager.getLogger("controlcenter");
		for(Map m:list) {
			boolean isOk = alarmService.insertAlarmChkTerm("중복 배포 업무: " + m.get("SVC_NM") +  " 용도: " + m.get("PURPOSE_NM") , "DUP", (String)m.get("SVC") + m.get("PURPOSE"), day, null);
			if(isOk) {
				String message="업무: " + m.get("SVC_NM") + " 용도: " +  m.get("PURPOSE_NM") + "의 VM이 " + m.get("MAX_VM_CNT") + " 개 중복 배포 되었습니다.(DC_ID:" +  m.get("DC_ID") + " DC_NM:"  +  m.get("DC_NM") + ")";
				log.info(message);
			}
		}
	}
	public Map getCustomInfo(String dataJsonStr, String formJsonStr)  throws Exception{
		VraCatalogService vraservice = (VraCatalogService)SpringBeanUtil.getBean("vraCatalogService");
		Map custInfo  = vraservice.getCustomInfo(dataJsonStr, formJsonStr);
	 
		JSONObject formJson = new JSONObject(formJsonStr);
		try {
			if(formJson.getJSONObject("inputs").has("_svc") && "array".equals(formJson.getJSONObject("inputs").getJSONObject("_svc").getString("type"))) {
				JSONObject dataJson = new JSONObject(dataJsonStr);
				JSONArray svc = dataJson.getJSONArray("_svc") ;
				if(svc.length()>1) {
					 custInfo.put("업무 코드", svc);
				}
			}
		}
		catch(Exception ignore) {
			
		}
		 
		org.codehaus.jettison.json.JSONArray os_param = null;
		try {
			JSONObject os_param_json = formJson.getJSONObject("inputs").getJSONObject("os_param");
			os_param = (org.codehaus.jettison.json.JSONArray)custInfo.remove(os_param_json.get("title"));
		}
		catch(Exception ignore) {
			
		}
		 
		 
		if(os_param != null) {
			 for(int i=0; i < os_param.length(); i++) {
				 String[] arr1 = os_param.getString(i).split("=");
				 if(arr1.length==2) {
					 if(arr1[0].equals("os_ad_user")) {
						 if(custInfo.get("AD User") != null) {
							 custInfo.put("AD User",custInfo.get("AD User") + "," + arr1[1]);
						 }
						 else {
							 custInfo.put("AD User", arr1[1]);
						 }
						 
					 }
					 else if(arr1[0].equals("os_local_user")) {
						 if(custInfo.get("User ID") != null) {
							 custInfo.put("User ID", custInfo.get("User ID") + "," +arr1[1]);
						 }
						 else {
							 custInfo.put("User ID", arr1[1]);
						 }
						 
					 }
				 }
			 }
		}
		return custInfo;
	}
	public void insertIpFromFile() {
		File dir = new File(PropertyManager.getString("host_dir","/temp/hosts/"));
		File[] files = dir.listFiles();
		if(files==null) return;
		String moveDir =  dir.getPath() + "/backup/" ;
		for(File f:files) {
			try {
				if(f.isFile()) {
					if(insertIpFromFile(f) ) {
						f.renameTo(new File( moveDir +  f.getName()));
					}
				}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	public  boolean insertIpFromFile(File file) throws Exception{
		 
		Map<String, String> unique = new HashMap();
		BufferedReader reader = null;
		int count = 0;
		try {
			reader = new BufferedReader(  new FileReader(file));
			String line = null;
			while((line=reader.readLine()) != null) {
				String[] arr = line.split( "( |\t)+" );
				if(arr.length == 2 && !arr[1].trim().endsWith("-scan")) {
					unique.put(arr[1], arr[0]);
				}
			}
			reader.close();
			reader = null;
			if(unique.size()>0)
			{			// select DC_ID, VM_HV_ID from NC_MONITOR_VM where VM_NM=#{VM_NM}
				IPService ipService = (IPService)SpringBeanUtil.getBean("IPService");
				for(String host: unique.keySet()) {
					Map param = new HashMap();
					int pos = host.lastIndexOf("-");
					String vm = host;
					String rsn = "rip";
					if(pos>0) {
						vm = host.substring(0, pos);
						rsn = host.substring(pos+1);
					}
					  
					
					
					param.put("VM_NM", vm);
					Map info = ipService.selectOneByQueryKey("com.clovirsm.monitor","get_NC_MONITOR_VM_byName", param);
					if(info != null && !info.isEmpty()) {
						info.put("IP", unique.get(host));
						info.put("RSN", rsn);
						info.put("STATUS", "R");
						ipService.insertVMIp(info);
						count++;
					}
					
				}
			}
			return count==unique.size();
		}  
		finally {
			if(reader != null) {
				try {
					reader.close();
				}catch(Exception ignore) {
					
				}
			}
		}
				
	}
	@Override
	public void putVraDataJson(JSONObject dataJSON, org.codehaus.jettison.json.JSONObject formJSON, Map param)
			throws Exception {
		
		String val1 = (String)this.service.selectOneObjectByQueryKey("com.clovirsm.site", "select_CL_GROUP", param) ;
		if(val1 != null) {
			dataJSON.put("_cl_kubun", val1);
		}
		
	}
	public HynixBeforeAfterVMWare( ) {
		super( );
		 
	}
	@Override
	public boolean expire(String svcCd, String pkVal, boolean bySchedule ) throws Exception{
		boolean isResult = super.expire(svcCd, pkVal, bySchedule);
		if(svcCd.equals("S")) sendHYSACNoti(false, svcCd, pkVal);
		return isResult;
	}
	 
	@Override
	public
	IAfterProcess onAfterProcess(String svcCd, String cudCd,  Map info) throws Exception{
		
		
		if(svcCd.equals("C") && cudCd.equals("C")) {
			return  new VRAOnAfterRequest( info ) {
				@Override
				protected void onAfterFail(VraCatalogService vraCatalogService, Map param) {
					varResultlog.error(param.get("CATALOG_ID") + "=>" + param.get("REQ_TITLE")  + " FAIL" );
					super.onAfterFail(vraCatalogService, param);
					try {
						HynixNextApprover approver = (HynixNextApprover)SpringBeanUtil.getBean("nextApprover");
						approver.sendMailToFABAdmin( "mail_title_deploy_fail", "vraCreate", param); //어드민에게 메일
					} catch (Exception e1) {
						 
						e1.printStackTrace();
					}
				}
				@Override
				protected void sendSuccessMail(VraCatalogService vraCatalogService, Map param) throws Exception {
					varResultlog.info(param.get("CATALOG_ID") + "=>" + param.get("REQ_TITLE")  + " SUCCESS" );
					Map info = (Map)param.get("INFO");
//					if(!"prd".equals(info.get("P_KUBUN"))) { // 운영일 때 성공메일 보내지 않음.
					vraCatalogService.sendMail(param.get("INS_ID"), "mail_title_deploy", "vraCreate", param);
//					}
					vraCatalogService.sendMailToAdmin( "mail_title_deploy", "vraCreate", param); //어드민에게 메일
				}
				
				@Override
				protected void onAfterCreateVM(VraCatalogService vraCatalogService, Map param) {
					
					 
					super.onAfterCreateVM(vraCatalogService, param);
					Map info = (Map)param.get("INFO");
					 
					try {
						sendVraSuccessMail(param, true);
					} catch (Exception e) {
						 
						e.printStackTrace();
					}
					 
					
					insertVMSW(vraCatalogService , (String)info.get("DATA_JSON"));
					GetVMInfos vmState = ( GetVMInfos) SpringBeanUtil.getBean("getVMInfos");
					List<Map> vmList = (List)param.get("VM_LIST");
					vmState.runInThread(600000, vmList);
					 
				}
			};
		}
		else {
			return super.onAfterProcess(svcCd, cudCd, info);
		}
	}
	
	
	public void sendVraSuccessMail(Map param, boolean isFirst) throws Exception {
		VraCatalogService vraCatalogService =(VraCatalogService) SpringBeanUtil.getBean("vraCatalogService");
		if(!isFirst) {
			
			param.putAll( vraCatalogService.getAllDetail((String)param.get("CATALOGREQ_ID")) );
			Map info = (Map)param.get("INFO");
			vraCatalogService.sendMail(info.get("INS_ID"), "mail_title_deploy", "vraCreate", param); //요청자에게 메일
			 
		}
		else {
			try {
				HynixNextApprover approver = (HynixNextApprover)SpringBeanUtil.getBean("nextApprover");
				approver.sendMailToFABAdmin( "mail_title_deploy", "vraCreate", param);
			} catch (Exception e1) {
				 
				e1.printStackTrace();
			}
			Map info = (Map)param.get("INFO");
			List<Map> vmList = (List)param.get("VM_LIST");
			sendHYSACNoti(true, vmList);
			try {
				if("db".equals(info.get("PURPOSE"))){
					String dbMailTo = (String) ComponentService.getEnv("db.mailTo", null);
					if(dbMailTo != null) {
						String[] arr = dbMailTo.split(",");
						 
						for(String arr1 : arr) {
							vraCatalogService.sendMail(arr1, "DB 배포 완료", "vraCreate", "ko", param);
						}
					}
						
				}
				
			} catch (Exception e) {
				 
				e.printStackTrace();
			}
		}
	}
	private void insertVMSW(VraCatalogService vraCatalogService,String dataJSON) {
		int pos = dataJSON.indexOf("input_vm_name");
		int pos1 = dataJSON.indexOf("input_application");
		if(pos>0 && pos1>0) {
			JSONObject json = new JSONObject(dataJSON);
			JSONArray vms = json.getJSONArray("input_vm_name");
			JSONArray apps = json.getJSONArray("input_application");
			Map param = new HashMap();
			for(int i=0; i < vms.length(); i++) {
				param.put("VM_NM", vms.getString(i));
				for(int j=0; j < apps.length(); j++) {
					param.put("SW_NM", apps.getString(j));
					try {
						vraCatalogService.insertByQueryKey("com.clovirsm.resources.vm.VM", "insert_NC_VM_SW_byName", param);
					}
					catch(Exception  e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	@Override
	public void reuse(String svcCd, String pkVal, int mm, boolean sendMail) throws Exception{
		super.reuse(svcCd, pkVal, mm, sendMail);
		if(svcCd.equals("S")) sendHYSACNoti(true, svcCd, pkVal);
	}
	
	
	protected void sendHYSACNoti(boolean isCreate, String svcCd, String pkVal) {
			DefaultNextApprover service = (DefaultNextApprover)SpringBeanUtil.getBean("nextApprover");
			Map data;
			try {
				data = service.getDetailList(svcCd, pkVal, null);
				sendHYSACNoti(isCreate, (List)data.get("S_list") );
				
			} catch (Exception e) {
				LoggerFactory.getLogger(this.getClass()).error("HYSAC"  , e);
				e.printStackTrace();
			}
			
	}
	@Override
	protected void doDeleteAfterExpire(String svcCd, String pkVal) throws Exception{
		if(svcCd.equals(NCConstant.SVC.S.toString())){
			VRADeleteVMReq deleteVM = (VRADeleteVMReq)SpringBeanUtil.getBean("VRADeleteVMReq");
			Map result = deleteVM.run(pkVal);
			if(result.get("NOT_FOUND") != null) {
				deleteVM.run(pkVal);
			}
		}
	}
	protected void sendHYSACNoti(boolean isCreate, List list) {
		if(list.size()==0) {
			return;
		}
		String mailTo =  (String) ComponentService.getEnv("hysac.mailTo","");
		if("".equals(mailTo) ) return;
		String title = "서버 " + (isCreate ? "생성":"삭제") +   " 요청(to HYSAC)";
		org.apache.log4j.Logger maillog = LogManager.getLogger("mail");
		 
			 
		 
		Map data = new HashMap();
		String[] arr = mailTo.split(",");
		data.put("TITLE", title);
		data.put("VM_LIST", list);
		System.out.println("%%%%%%%%%%%%%%%%%%%%%%%% HYSAC Send%%%%%%%%%%%%%%%%%%%%");
		System.out.println(data);
		System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		VraCatalogService vraCatalogService =(VraCatalogService) SpringBeanUtil.getBean("vraCatalogService");
		for(String a : arr) {
			 
				vraCatalogService.sendMail(a, title, "hysac", "ko", data);				
				 
		}
			
		 
		
	}
	
	@Override
	public String getFolderName( Map param) throws Exception
	{
		VraCatalogService service = (VraCatalogService)SpringBeanUtil.getBean("vraCatalogService");
		param.put("CATALOGREQ_ID", param.get("FROM_ID"));
		Map catalogInfo = service.selectOneByQueryKey("selectByPrimaryKey_NC_VRA_CATALOGREQ", param);
		if(catalogInfo == null || catalogInfo.isEmpty()) {
			return "";
		}
		else {
			Map dataJSON = service.json2Map( new JSONObject((String) catalogInfo.get("DATA_JSON")));
			String pattern= (String) ComponentService.getEnv("vmFolder","{_svc}");
			return setVarVal(pattern, dataJSON);
		}
		
	}
	@Override
	public boolean  onBeforeCreateVM(boolean isNew, Map param) throws Exception {
		SiteService service = (SiteService)SpringBeanUtil.getBean("siteService");
		String host = (String) service.selectOneObjectByQueryKey("selectHost", param);
		if(host == null)
		{
			throw new Exception("Host not exist");
		}
		param.put("HOST_HV_ID", host);
		return true;
	}
	/*@Override
	public int chgNaming(Object categoryId,String naming) throws Exception{
		int cnt0 = getCnt0(naming);
		SiteService service = (SiteService)SpringBeanUtil.getBean("siteService");
		Map param = new HashMap();
		param.put("CATEGORY", categoryId);
		param.put("LEN", naming.length());
		param.put("PREFIX", naming.substring(0,  naming.length()-cnt0));
		int row =  service.updateByQueryKey("update_NC_VM_category_bynaming", param);
		row +=  service.updateByQueryKey("update_NC_VM_REQ_category_bynaming", param);
		return row;
	}
	*/
	/*@Override
	public void onAfterGetVM(Map result) throws Exception{
		SiteService service = (SiteService)SpringBeanUtil.getBean("siteService");
		
		List<Map> categoryList = service.selectListByQueryKey("select_all_CATEGORY_CODE", result);
		for(Map m : categoryList)
		{
			String code = (String) m.get("CATEGORY_CODE");
			int cnt0 = getCnt0(code);
			String vmNm = (String)result.get("VM_NM");
			if(vmNm.startsWith(code.substring(0, code.length()-cnt0)) && (vmNm.length()==code.length() || vmNm.length()==code.length()+1 ))
			{
				result.put("CATEGORY", m.get("CATEGORY_ID"));
				if(vmNm.charAt(code.length()-cnt0)=='d')
				{
					result.put("P_KUBUN", "D");
				}
				else
				{
					result.put("P_KUBUN", "P");
				}
				break;
			}
					
		}
		
		result.put("SPEC_ID", "0"); 
		
	}*/
	@Override
	public boolean reconfigVM(String pkVal, String instDt, Map param) throws Exception {
		try {
			
			ReconfigVM reconfigVM = (ReconfigVM)SpringBeanUtil.getBean("reconfigVM");
			reconfigVM.runByVmInfo(param);
			
			List<Map> dataDisk = (List<Map>) param.get(HypervisorAPI.PARAM_DATA_DISK_LIST);
			String diskParam = "";
			for(Map m : dataDisk) {
				if(NumberUtil.getInt(m.get("DISK_SIZE"))==0) continue;
				 
				if("C".equals(m.get("CUD_CD"))) { // 추가
					diskParam += m.get("DISK_NM") + "=" +  m.get("DISK_SIZE") + ",";
				}
			}
			if(!diskParam.equals("")) {
				VraCatalogService catalogReqService = (VraCatalogService)SpringBeanUtil.getBean("vraCatalogService");
				Map info = catalogReqService.selectOneByQueryKey("selectCatalogReqIdByVM", param);
				if(info != null && !info.isEmpty()) {
					param.putAll(info);
				}
				//String id = PropertyManager.getString("reconfig.workflowId");
			 
				
				String paramStr="{\"parameters\":[{\"name\":\"input_vm_name\",\"type\":\"Object\",\"value\":{\"string\":{\"value\":\"{0}\"}}},{\"name\":\"input_disk_list\",\"type\":\"string\",\"value\":{\"string\":{\"value\":\"{1}\"}}}],\"profilerOptions\":{\"enabled\":true,\"debuggerEnabled\":false}}";
				paramStr = paramStr.replaceFirst("\\{0\\}", (String)param.get("VM_NM"));
				paramStr = paramStr.replaceFirst("\\{1\\}", diskParam);
				JSONObject json  = new JSONObject(paramStr);
				VRAAPI8 api = (VRAAPI8)VRACommonAction.getAPI(param);
				String id=(String)param.get("disk_wf_id");
				System.out.println(((VRAAPI8)VRACommonAction.getAPI(param)).runWorkflow(param, id, json));
			}	 
			return true;
		}
		catch(Exception e) {
			
			VMService vmService = (VMService) NCReqService.getService("S");
			 
			DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
			dcService.updateCreateFail(pkVal, "NC_VM", param, e.getMessage());
			 
			vmService.updateStatusReq(pkVal, instDt); // NC_VM_REQ APPR_STATUS_CD='R'
			DeployFailService failService = (DeployFailService)SpringBeanUtil.getBean("deployFailService");
			failService.insert(  "S",   "U",   pkVal,   instDt, e.getMessage());
			
			//GetVMInfos vmState = ( GetVMInfos) SpringBeanUtil.getBean("getVMInfos");
			//vmState.runInThread(600000, (String) param.get("VM_ID"));
			 
			return false;
		}
	}
	/*@Override
	protected String getEtcCd(Map param) throws Exception
	{
		SiteService service = (SiteService)SpringBeanUtil.getBean("siteService");
		String code = (String)service.selectOneObjectByQueryKey("select_CATEGORY_CODE", param);
		if(code == null || "".equals(code)) throw new Exception("VM Name Prefix Not Configured");
		int pos = code.indexOf("0");
		return code.substring(0,pos) + ("D".equals(param.get("P_KUBUN"))?"d":"") + code.substring(pos ) ;
	}*/
	
	
	@Override
	public boolean deleteVM(String pkVal, String insDt, Map param) throws Exception {
		
		ExpireService expireService = (ExpireService)SpringBeanUtil.getBean("expireService");
		expireService.expire("S", pkVal, false);	
		
		return true;
	}
	@Override
	protected int getStart0Pos(String code) {
		return  code.indexOf("0", 5);
	}
	 
	@Override
	protected String getEtcCd(Map paramOrg) throws Exception
	{
		String pattern= (String) ComponentService.getEnv("vmNaming","{_fab}{_svc}{_purpose}000{_p_kubun}");
		Map param = new HashMap();
		param.putAll(paramOrg);
		if("prd".equals(param.get("_p_kubun"))){
			param.put("_p_kubun", ""); // 운영은 p_kubun을 ''으로 ..
		}
		if(pattern.indexOf("{num_cnt}")>0) {
			int numCnt = 2;
			if(!"db".equals(param.get("_purpose"))) {
				SiteService service = (SiteService)SpringBeanUtil.getBean("siteService");
				Map param1 = new HashMap();
				param1.put("CATEGORY_CODE", param.get("_svc"));
				try {
					int num = (Integer)service.selectOneObjectByQueryKey("getVMNamingNum", param1);
					if(num>0) {
						numCnt = num;
					}
				}
				catch(Exception ignore) {
					 
				}
					
			}
			pattern = pattern.replaceFirst("\\{num_cnt\\}", TextHelper.lpad("", numCnt, '0'));
			System.out.println("pattern=" + pattern + "," + param);
		}
		return setVarVal(pattern, param);
		
	}
	 
	
	
	

	public static void main(String[] args)
	{
		Map param = new HashMap();
		param.put("_fab", "m16");
		param.put("_category", "mes");
		param.put("_purpose", "db");
		param.put("_p_kubun", "qa");
		System.out.println(HynixBeforeAfterVMWare.setVarVal("{_fab}{_category}{_purpose}000{_p_kubun}", param));
				
		 
	}
	private List getEnum(JSONObject json) throws Exception{
		List result = new ArrayList();
		if(json.has("enum")) {
			JSONArray fabEnum = json.getJSONArray("enum");
			for(int i=0; i < fabEnum.length(); i++) {
				result.add(fabEnum.getString(i));
			}
		}
		else if(json.has("oneOf")) {
			JSONArray fabEnum = json.getJSONArray("oneOf");
			for(int i=0; i < fabEnum.length(); i++) {
				result.add(fabEnum.getJSONObject(i).get("const"));
			}
		}
		return result;
	}
	public void onAfterCollectCatalog(List<Map> list) throws Exception {
		DcKubunConfService mapService = (DcKubunConfService)SpringBeanUtil.getBean("dcKubunConfService");
		for(Map m : list) {
			String formInfo = (String)m.get("FORM_INFO");
			if(!TextHelper.isEmpty(formInfo)) {
				JSONObject json = new JSONObject(formInfo);
				JSONObject inputs = json.getJSONObject("inputs");
				mapService.delete(null,(String)m.get("CATALOG_ID"),  "FAB_CATALOG", null);
				if(inputs.has("_fab") ) {
					List<String> fabEnum = getEnum(inputs.getJSONObject("_fab"));
					for(int i=0; i < fabEnum.size(); i++) {
						mapService.insert( fabEnum.get(i),(String)m.get("CATALOG_ID"), "FAB_CATALOG", null, null);
					}
				}
				
				mapService.delete(null,(String)m.get("CATALOG_ID"),  "P_KUBUN_CATALOG", null);
				if(inputs.has("_p_kubun") ) {
					List<String> fabEnum = getEnum(inputs.getJSONObject("_p_kubun"));
					for(int i=0; i < fabEnum.size(); i++) {
						mapService.insert( fabEnum.get(i),(String)m.get("CATALOG_ID"), "P_KUBUN_CATALOG", null, null);
					}
				}
			}
		}
		
	}
	 
}

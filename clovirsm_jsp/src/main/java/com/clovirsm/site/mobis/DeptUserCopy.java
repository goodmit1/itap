package com.clovirsm.site.mobis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;

import com.clovirsm.service.batch.ScheduleService;
import com.clovirsm.service.site.GroupMemberService;
import com.clovirsm.service.site.GroupService;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class DeptUserCopy {

	public void scheduleRun()  {
		ScheduleService service = (ScheduleService)SpringBeanUtil.getBean("clovirScheduleService");
		if(service.isRunServer()) {
			try {
				move();
			}
			catch(Exception e) {
				service.log.error("DeptUserCopy", e);
			}
		}
	}
	
	protected Connection getConnection(GroupService groupService, Map param) throws Exception {
		Map connInfo = groupService.selectOneByQueryKey("getUserDBInfo", param);
		return ConnectionFactory.connection(  (String)connInfo.get("CONN_URL"), (String)connInfo.get("CONN_USERID"), (String)connInfo.get("CONN_PWD"));
//
	}
	public void move() throws Exception
	{
		Connection conn = null;
		ResultSet rs = null;

		try {
			GroupService groupService = (GroupService)SpringBeanUtil.getBean("groupService");
			Map param = new HashMap();
			conn = getConnection(groupService, param);
//			conn =   connection((String)connInfo.get("CONN_URL"), (String)connInfo.get("CONN_USERID"), (String)connInfo.get("CONN_PWD"));
			String select_dept_query = PropertyManager.getString("query.dept");
			//String select_dept_query = "SELECT '102216547406718' as COMP_ID, OBJID TEAM_CD, PAR_OBJID PARENT_CD, STEXT TEAM_NM FROM ORG_MASTER WHERE ENDDT = '99991231' AND PAR_OBJID IS NOT NULL ";
			//String select_user_query = "SELECT CASE WHEN  a.DEPTID IS NULL THEN '1' WHEN lengthb(a.DEPTID)>10 THEN substrb(a.DEPTID,1,10) ELSE a.DEPTID end  TEAM_CD, USERID LOGIN_ID, concat(USERID,'@mobis.co.kr') EMAIL, USERNAME USER_NAME, LEVELNAME POSITION FROM USER_account a WHERE status='TRUE' AND levelname IS NOT NULL AND length(levelname) <= 100 AND  username not LIKE '&%'";
			
			String select_user_query= PropertyManager.getString("query.user");
			/** 부서 **/



			rs = conn.prepareStatement(select_dept_query).executeQuery();
			
			groupService.updateMoveFromLegacy(rs);
			rs.close();

			/**
			 * 사용자 추가
			 */
			GroupMemberService userService = (GroupMemberService)SpringBeanUtil.getBean("groupMemberService");
			rs = conn.prepareStatement(select_user_query).executeQuery();
			userService.updateMoveFromLegacy(rs);
			try {
			
				userService.onAfterUserBatch( );
			}
			catch(Exception e) {
				org.apache.log4j.Logger log = LogManager.getLogger(ScheduleService.class);
				log.error(e);
			}

		}
		finally
		{

			try {
				if(rs != null) rs.close();
				if(conn != null) conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

	}

}

package com.clovirsm.site;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.clovirsm.hv.VMInfo;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.executor.CreateImg;
import com.fliconz.fw.runtime.util.DateUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class CopyTemplate {

	String vmName;

	String dcId;
	String[] dsNames; 
	public String[] getDsNames() {
		return dsNames;
	}

	public void setDsNames(String[] dsNames) {
		this.dsNames = dsNames;
	}

	public String getDcId() {
		return dcId;
	}

	public void setDcId(String dcId) {
		this.dcId = dcId;
	}

	public String getVmName() {
		return vmName;
	}

	public void setVmName(String vmName) {
		this.vmName = vmName;
	}
	
	public void run() {
		try {
			 
			DCService dcService =(DCService)SpringBeanUtil.getBean("DCService");
			DC dcInfo = dcService.getDC(dcId);
			VMInfo info = new VMInfo();
			info.hostName=vmName;
			Map param = new HashMap();
			param.put("IMG_NM", vmName + "_" + DateUtil.format(new Date(), "yyyyMMddHHmmss"));
			System.out.println( new Date() + " copy template start " + param);
			param.putAll(dcInfo.getProp());
			if(dsNames != null) {
				param.put(VMWareAPI.PARAM_IMG_DATASTORE_NM, dsNames);
			}
			Map result = dcInfo.getAPI().createImage(null, param, info );
			
		} catch (Exception e) {
			 e.printStackTrace();
		}
		
	}
}

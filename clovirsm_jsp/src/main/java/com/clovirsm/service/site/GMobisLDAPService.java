package com.clovirsm.service.site;

import com.clovirsm.ldap.LDAPHandler;
import com.clovirsm.service.admin.DcMngService;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.naming.NamingEnumeration;
import javax.naming.directory.SearchResult;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static java.util.stream.Collectors.toMap;

@Service
public class GMobisLDAPService extends LDAPHandler {

    Map<String, Object> filterProperty = getFilterProperty();

    public GMobisLDAPService() {
    }

    public GMobisLDAPService(String id, String pwd) throws Exception {
        super.init(
                filterProperty.get("url").toString(),
                setPrincipal(id),
                pwd,
                (String[]) filterProperty.get("attrIds")
        );
    }

    public GMobisLDAPService(String url, String id, String pwd) throws Exception {
        super.init(
                url,
                setPrincipal(id),
                pwd,
                (String[]) filterProperty.get("attrIds")
        );
    }

    public String setPrincipal(String id) {
        return String.format(filterProperty.get("principalFormat").toString(), id);
    }

    public Map getLDAPResponse(Map requestParam) throws Exception {

        String userID = requestParam.get("userId").toString();
        String password = requestParam.get("password").toString();
        String principal = setPrincipal(userID);

        return init(
                filterProperty.get("url").toString(),
                principal,
                password,
                (String[]) filterProperty.get("attrIds")
        )
                .searchById(
                        filterProperty.get("filter").toString(),
                        userID);
    }

    public Map<String, String> getLDAPConnectInfo(String dcId) throws Exception {
        DcMngService service = (DcMngService) SpringBeanUtil.getBean("dcMngService");
        List<Map<String, String>> ldapInfo = getLDAPInfo(dcId, service);

        return getLDAPInfo(dcId, service)
                .stream()
                .flatMap(entries -> entries.entrySet().stream())
                .collect(
                        toMap(
                                Entry::getKey,
                                Entry::getValue
                        ));
    }

    private List<Map<String, String>> getLDAPInfo(String dcId, DcMngService service) throws Exception {
        Map<String, String> param = new HashMap();
        param.put("DC_ID", dcId);
        param.put("CONN_TYPE", "LDAP");
        List<Map<String, String>> ldapInfo = service.selectList("NC_DC_ETC_CONN", param);
        return ldapInfo;
    }

    public static Map<String, Object> getFilterProperty() {
        Map<String, Object> param = new HashMap<>();

        String[] attrIds = PropertyManager.getString("ldap.attrIds").split(",");
        String dn = PropertyManager.getString("ldap.baseDn");
        String filter = PropertyManager.getString("ldap.filter");
        String principalFormat = PropertyManager.getString("ldap.principalFormat");
        String url = PropertyManager.getString("ldap.url");

        param.put("baseDn", dn);
        param.put("attrIds", attrIds);
        param.put("filter", filter);
        param.put("principalFormat", principalFormat);
        param.put("url", url);

        return param;
    }

    public Map searchById(String val) throws Exception {
        NamingEnumeration<SearchResult> searchResult = searchResultNamingEnumeration(
                filterProperty.get("baseDn").toString(),
                filterProperty.get("filter").toString(),
                val
        );

        if (searchResult.hasMore()) {
            SearchResult o = (SearchResult) searchResult.next();
            Map<String, String> result = new HashMap();
            result.put("USER_NAME", getAttribute(o, "cn"));
            result.put("LOGIN_ID", getAttribute(o, "sAMAccountName"));
            result.put("TEAM_NM", getAttribute(o, "department"));
            result.put("EMAIL", getAttribute(o, "mail"));
            System.out.println("searchResult attrs : " + o.getAttributes());
            return result;
        } else {
            return null;
        }
    }

    private String getAttribute(SearchResult o, String cn) {
        if (o.getAttributes().get(cn) == null) return null;

        String attr = o.getAttributes().get(cn).toString();
        return attr.substring(attr.indexOf(":") + 1, attr.length()).trim();
    }
}

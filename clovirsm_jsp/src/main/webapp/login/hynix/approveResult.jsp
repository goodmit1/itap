<%@page import="org.springframework.security.authentication.AuthenticationProvider"%>
<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page import="com.fliconz.fm.security.SNSAuthenticationToken"%>
<%@page import="org.springframework.security.core.Authentication"%> 
<%@page import="org.springframework.security.core.context.SecurityContext"%>
<%@page import="org.springframework.security.authentication.AbstractAuthenticationToken"%>
<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page import="com.clovirsm.service.workflow.ApprovalService"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=utf-8"    pageEncoding="utf-8"%>
<%
String reqId = request.getParameter("reqId");
String status = request.getParameter("status");
String reason = request.getParameter("reason");
Map param = new HashMap();

param.put("APPR_COMMENT", reason);
param.put("REQ_ID", reqId);
AbstractAuthenticationToken token = new SNSAuthenticationToken("hysos", "hysos");
SecurityContext context = SecurityContextHolder.getContext();
AuthenticationProvider authenticationProvider = (AuthenticationProvider)SpringBeanUtil.getBean(PropertyManager.getString("login.authenticationProvider", "fmAuthenticationProvider"));
Authentication authentication = authenticationProvider.authenticate(token);
context.setAuthentication(authentication);
ApprovalService service = (ApprovalService)SpringBeanUtil.getBean("approvalService");
if(status.equals("A")){
	service.updateApprove(param);

}
else{
	service.updateDeny(param);
}
context.setAuthentication(null);
%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@page import="java.util.ResourceBundle"%>
<%@page import="com.fliconz.fm.common.util.TextHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
if(request.getParameter("field") != null)
{


	String[] fields = TextHelper.split(request.getParameter("field"),"</p:column>");
	StringBuffer sb = new StringBuffer();
	//ResourceBundle msg = ResourceBundle.getBundle("message_" + table);
	sb.append("var columnDefs = [");

	for(String t:fields)
	{
		if(t==null) continue;
		System.out.println(t);
		int pos = t.indexOf("headerText=");
		int pos1 = t.indexOf( "}", pos);
		int pos2 = t.indexOf( "msg.", pos);
		if(pos2>0 && pos2<=pos1)
		{

			String title = t.substring(pos+"headerText=\"#{msg.".length(), pos1);
			sb.append("{headerName : \"<spring:message code=\"" + title + "\" text=\""  + "\" />\", ");
		}
		else
		{
			pos1 = t.indexOf( "\"", pos+"headerText=\"".length()+1);
			String title = t.substring(pos+"headerText=\"".length(), pos1);
			sb.append("{headerName : \"" +  title +  "\",");
		}
		pos = t.indexOf("value=");
		pos1 = t.indexOf( "}", pos);
		sb.append("field : \"" + t.substring(pos+"value=\"#{item.".length(),pos1) + "\"},\n");
	}
	sb.append("]");
	out.println("<textarea cols=\"1000\" rows=\"5\">" + sb.toString() + "</textarea>");
}

%>
<form method="post">

	field : <textarea name="field" cols="1000" rows="5"></textarea>
	<input type="submit">
</form>

<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.sql.DataSource"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page import="com.fliconz.fm.common.util.TextHelper"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.FileReader"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%
FileReader reader = new FileReader("/data/synonym.txt");
BufferedReader reader1 = new BufferedReader(reader);
String line;
DataSource ds = (DataSource)SpringBeanUtil.getBean("dataSource");
Connection conn =  ds.getConnection();
PreparedStatement pstmt =  conn.prepareStatement("INSERT INTO TA_DDIC(ID, KEYWORD, SYN_KEY) values(?, ?, ?)");
int idx=1;
while((line=reader1.readLine()) != null)
{
	String[] arr  = line.split(" ");
	StringBuffer sb = new StringBuffer();
	for(int i=1; i < arr.length; i++)
	{
		if(i>1) sb.append(";");
		sb.append(arr[i]);
	}
	pstmt.setInt(1, idx++);
	pstmt.setString(2, arr[0]);
	pstmt.setString(3, sb.toString());
	pstmt.executeUpdate();
	
}
pstmt.close();
conn.close();
%>

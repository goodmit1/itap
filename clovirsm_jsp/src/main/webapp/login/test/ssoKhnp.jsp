<%@page import="com.fliconz.fm.security.SSOHelper"%>
<%@page import="com.clovirsm.service.site.Crypt"%>
<%@page import="com.clovirsm.service.ComponentService"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> 
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
 
<%
//request.setAttribute("isRedirect", false);
	String id = request.getParameter("USER_ID");
	Crypt c = new Crypt();
	String decode = c.encryptAndEncode(id);
	System.out.println("decode : "+decode);
	System.out.println(c.decodeAndDecrypt(decode));

	String loginId = c.decodeAndDecrypt(decode);
	System.out.println("loginId : "+ loginId);
 
	SSOHelper.ssoLogin(loginId, "", request, response);
//	response.sendRedirect("/home/home.jsp?lang=ko");
//request.removeAttribute("isRedirect");
%>

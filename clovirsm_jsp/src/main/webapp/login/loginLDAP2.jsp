<%@page import="com.clovirsm.ldap.LDAPHandler"%>
<%@ page import="com.clovirsm.service.site.GMobisLDAPService" %>
<%@ page import="com.fliconz.fm.security.SSOHelper" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
         pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
    <title>Insert title here</title>
</head>
<body>
<%
    String userId = request.getParameter("userId");
    String password = request.getParameter("password");

    GMobisLDAPService ldapService = null;
    Map map = null;

    try {
        ldapService = new GMobisLDAPService(userId,password);
        map = ldapService.searchById(userId);

    } catch (Exception e) {
        e.printStackTrace();
    } finally {
        Objects.requireNonNull(ldapService).close();
    }
    System.out.println("ldapResponse : "+map);

    if (map != null && !map.isEmpty()){
//        SSOHelper.ssoLogin(map.get("LOGIN_ID").toString(), "", request, response);
        SSOHelper.ssoLogin(map.get("USER_NAME").toString(), "", request, response);
    }
    else{
%>
<script>
    alert("LDAP Login fail .\n 관리자에게 문의바랍니다.");
</script>
<%
    }
%>
</body>
</html>
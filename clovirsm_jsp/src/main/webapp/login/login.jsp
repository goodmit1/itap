<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="com.fliconz.fm.mvc.util.ControllerUtil"%>
<%@page import="com.fliconz.fm.common.util.HttpHelper"%>
<%@page import="java.util.Enumeration"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%> <%@ taglib prefix= "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%session.invalidate();
if(HttpHelper.isAjax(request)){
	JSONObject json = new JSONObject(ControllerUtil.getErrMap("NEED_LOGIN"));
	out.println(json.toJSONString());
	return;
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<script>
var dateLabel = {};
dateLabel.label_term = "<spring:message code="label_term" text="" />";
dateLabel.year = "<spring:message code="label_year" text="" />";
dateLabel.month = "<spring:message code="label_month" text="" />";
dateLabel.week = "<spring:message code="label_week" text="" />";
dateLabel.date = "<spring:message code="label_date" text="" />";
dateLabel.hour = "<spring:message code="hour" text="" />";
dateLabel.minute = "<spring:message code="minute" text="" />";
dateLabel.second = "<spring:message code="second" text="" />";

dateLabel.day = "<spring:message code="label_day" text="" />";
dateLabel.today = "<spring:message code="dt_today" text="" />";
dateLabel.pre_month = "<spring:message code="label_pre_month" text="" />";
dateLabel.next_month = "<spring:message code="label_next_month" text="" />";

dateLabel.sunday_min = "<spring:message code="label_sunday" text="" />";
dateLabel.monday_min = "<spring:message code="label_monday" text="" />";
dateLabel.tuesday_min = "<spring:message code="label_tuesday" text="" />";
dateLabel.wednesday_min = "<spring:message code="label_wednesday" text="" />";
dateLabel.thursday_min = "<spring:message code="label_thursday" text="" />";
dateLabel.friday_min = "<spring:message code="label_friday" text="" />";
dateLabel.saturday_min = "<spring:message code="label_saturday" text="" />";

dateLabel.sunday    = dateLabel.sunday_min    + dateLabel.day;
dateLabel.monday    = dateLabel.monday_min    + dateLabel.day;
dateLabel.tuesday   = dateLabel.tuesday_min   + dateLabel.day;
dateLabel.wednesday = dateLabel.wednesday_min + dateLabel.day;
dateLabel.thursday  = dateLabel.thursday_min  + dateLabel.day;
dateLabel.friday    = dateLabel.friday_min    + dateLabel.day;
dateLabel.saturday  = dateLabel.saturday_min  + dateLabel.day;
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title><spring:message code="btn_login" /></title>
	<link rel="stylesheet"
			href="/res/css/bootstrap.min.css">
	<script src="/res/js/jquery.min.js"></script>
	<link rel="stylesheet" href="/res/css/menu.css">
	<link rel="stylesheet" href="/res/css/common.css">
	<script type="text/javascript" src="/res/js/rsa.js"></script>
	<script type="text/javascript" src="/res/js/jsbn.js"></script>
	<script type="text/javascript" src="/res/js/prng4.js"></script>
	<script type="text/javascript" src="/res/js/rng.js"></script>
	<script src="/res/js/common.js" ></script>

</head>
<body>
	<form action="/j_spring_security_check" id="login-form" method="POST" onsubmit="beforeSubmit(this)">
		<fmtags:include page="_login.jsp" />
		<input type="hidden" name="lang" value="ko">
        <input type="hidden" name="locale" value="ko">
	</form>
	<script>
		var RSAModulus = "${RSAModulus}";
		var RSAExponent= "${RSAExponent}";
		$(document).ready(function(){
			if('<c:out value="${param.fail}"/>'=="true")
			{
				alert("<spring:message code="msg_invalid_id_password"/>");
			}
			else if('<c:out value="${param.fail}"/>'=="other")
			{
				alert("<spring:message code="msg_login_other" />");
			}
			$("#userId").focus();
			saveID();
		})
		function beforeSubmit(form)
		{
			//$("#userId").val(encrypt($("#userId").val()));
			//$("#password").val(encrypt($("#password").val()));
			return true;
		}
		
		function saveID(){
			var key = getCookie("key");
		    $("#userId").val(key); 
		     
		    if($("#userId").val() != ""){ 
		        $("#idSaveCheck").attr("checked", true); 
		    }
		     
		    $("#idSaveCheck").change(function(){ 
		        if($("#idSaveCheck").is(":checked")){ 
		            setCookie("key", $("#userId").val(), 7);
		        }else{ 
		            deleteCookie("key");
		        }
		    });
		     
		    $("#userId").keyup(function(){ 
		        if($("#idSaveCheck").is(":checked")){ 
		            setCookie("key", $("#userId").val(), 7); 
		        }
		    });
		}

	</script>
</body>
</html>

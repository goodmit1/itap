 
<%@page import="com.nhncorp.lucy.security.xss.XssPreventer"%>
<%@page import="com.fliconz.fm.mvc.util.MsgUtil"%>
<%@page import="com.clovirsm.service.resource.VMService"%>
<%@page import="com.fliconz.fm.common.CodeHandler"%>
<%@page import="com.clovirsm.site.HynixBeforeAfterVMWare"%>
<%@page import="java.util.Set"%>
<%@page import="com.clovirsm.service.resource.DiskService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.fliconz.fw.runtime.util.NumberUtil"%>
<%@page import="com.clovirsm.service.workflow.DefaultNextApprover"%>
<%@page import="com.clovirsm.service.workflow.WorkService"%>
<%@page import="com.clovirsm.service.resource.VraCatalogService"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.clovirsm.hv.CommonUtil"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.clovirsm.service.workflow.ApprovalService"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
   <%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %> 
   <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
   <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%! void putCodeTitle(Map result, CodeHandler codeHdr, String grp, String field){
	  Map map = codeHdr.getCodeList(grp, "ko");
	  String title =  (String)map.get(result.get(field));
	  result.put(field + "_NM", title);
}%>  
<%
// 메일의 내용이 되고, 결재 의견에 들어가서 css를 hard coding함.
String checked = request.getParameter("DATA");
checked = XssPreventer.unescape(checked);
System.out.println(checked);
if(checked != null){
 	JSONObject data = new JSONObject(checked);
 	for(Object k:data.keySet()){
 		request.setAttribute((String)k, CommonUtil.convertList( (JSONArray)data.get((String)k)));
 	}
}
else{
	String reqId = request.getParameter("REQ_ID");
	DefaultNextApprover service = (DefaultNextApprover)SpringBeanUtil.getBean("nextApprover");
	if(reqId != null){
		Map param = new HashMap();
		param.put("REQ_ID", reqId);

		Map<String, List> list = service.getDetailList(reqId);
		for(String k : list.keySet()){
			request.setAttribute((String)k, list.get(k));
		}
	}
	else{
		String svcId =  request.getParameter("SVC_ID");
		if(svcId != null){
			String svcCd =  request.getParameter("SVC_CD");
			Map<String,List> m = service.getDetailList(svcCd, svcId, request.getParameter("INS_DT"));
			for(String k : m.keySet()){
				request.setAttribute((String)k, m.get(k));
			}
			
		}
		
	}
	 
	
}

List<Map> cList = (List)request.getAttribute("C_list"); 
if(cList != null){
	VraCatalogService vraservice = (VraCatalogService)SpringBeanUtil.getBean("vraCatalogService");
	 CodeHandler codeHdr  = (CodeHandler)SpringBeanUtil.getBean("codeHandler");
	HynixBeforeAfterVMWare hynix = new HynixBeforeAfterVMWare();
	for(int i=0; i < cList.size(); i++){
		putCodeTitle(cList.get(i),   codeHdr, "FAB", "FAB");
		putCodeTitle(cList.get(i),   codeHdr, "P_KUBUN", "P_KUBUN");
		Map custInfo =  hynix.getCustomInfo((String)cList.get(i).get("DATA_JSON"), (String)cList.get(i).get("FORM_INFO"));		 
		cList.get(i).put("CUST_DATA",custInfo);
		System.out.println(custInfo);
		cList.get(i).put("SUMMARY", vraservice.getSummary((String)cList.get(i).get("FORM_INFO"), (String)cList.get(i).get("DATA_JSON"), (String)cList.get(i).get("FIXED_JSON"), NumberUtil.getInt(cList.get(i).get("DEPLOY_TIME"),10)));
	}
}
/*List<Map> sList = (List)request.getAttribute("S_list"); 
if(sList != null){
	DiskService diskService = (DiskService)SpringBeanUtil.getBean("diskService");
	VMService vmService = (VMService)SpringBeanUtil.getBean("VMService");
	for(int i=0; i < sList.size(); i++){
		Map sInfo =  sList.get(i);
		sInfo.put("DISK_LIST", diskService.list("list_NC_DISK_byVM", sInfo));
		if(vmService.needRestart(sInfo)){
			sInfo.put("WARN_MSG", MsgUtil.getMsg("msg_server_restart", new String[]{}) );
		}
	}
}
*/
%>
 
 
 
<c:forEach items="${S_list}" var="list">
<table border=1 cellspacing=0 cellpadding=10  width=100% bgColor="white">

<c:if test="${list.CUD_CD_NM != null}">
<tr><th  bgColor="#edeef3">구분</th><td>${list.SVC_CD_NM } ${list.CUD_CD_NM }</td></tr>
</c:if>
<tr ><th bgColor="#edeef3">서버명</th><td>${list.VM_NM }</td></tr>
<c:if test="${list.PRIVATE_IP!= null}">
<tr ><th bgColor="#edeef3">IP</th><td>${list.PRIVATE_IP }</td></tr>
</c:if>
<c:choose>
<c:when test="${list.CUD_CD eq 'U'}">
	<tr ><th bgColor="#edeef3">변경 전 사양</th><td>${list.OLD_SPEC_INFO }</td></tr>
	<tr ><th bgColor="#edeef3">변경 후 사양</th><td>${list.SPEC_INFO }</td></tr>
	<tr ><th bgColor="#edeef3">디스크 추가 정보</th><td>
	<c:forEach items="${list.DISK_LIST}" var="disklist">
	
		<c:if test="${disklist.CUD_CD eq 'C' }">
			${disklist.DISK_NM } = ${disklist.DISK_SIZE }GB <br>
		</c:if>
	</c:forEach>
	</td></tr>
	<tr ><th bgColor="#edeef3">사유</th><td>${list.CHG_CMT }</td></tr>
</c:when>

<c:otherwise>
<tr ><th bgColor="#edeef3">사양</th><td>${list.SPEC_INFO }</td></tr>
</c:otherwise>
</c:choose>
<c:if test="${list.THIN_REQ_YN != null}">
	<tr ><th bgColor="#edeef3">접속 단말 신청</th><td><pre>${list.THIN_REQ_YN eq 'Y' ? '신청': '미신청'}</pre></td></tr>
</c:if> 
<c:if test="${list.CUD_CD eq 'C'}">
	<tr ><th bgColor="#edeef3">설명</th><td><pre>${list.CMT }</pre></td></tr>
</c:if>
<c:if test="${list.WARN_MSG!= null}">
<tr ><th colspan="2"><font color='red'>${list.WARN_MSG}</font></th></tr>
</c:if>
<c:if test="${list.EXPIRE_DT != null}">
<tr ><th bgColor="#edeef3">만료일</th><td>${list.EXPIRE_DT_FORMAT }</td></tr>
</c:if>
</table>
<p>
</c:forEach>
 
<c:forEach items="${E_list}" var="list">
<table border=1 cellspacing=0 cellpadding=10  width=100% bgColor="white">
 
<tr><th width="200"  bgColor="#edeef3">구분</th><td>기간 연장</td></tr>
 
<tr ><th bgColor="#edeef3">서버명</th><td>${list.TITLE }</td></tr>
 
<tr ><th bgColor="#edeef3">추가 연장 기간(월)</th><td>${list.ADD_USE_MM }</td></tr>
<tr ><th bgColor="#edeef3">사유</th><td>${list.REASON }</td></tr>
</table>
<div style="height:10px"></div>
</c:forEach> 


<c:forEach items="${V_list}" var="list">
<table border=1 cellspacing=0 cellpadding=10  width=100% bgColor="white">
 
<tr><th width="200"  bgColor="#edeef3">구분</th><td>서버 복원</td></tr>
 
<tr ><th bgColor="#edeef3">서버명</th><td>${list.TITLE }</td></tr>
 
<tr ><th bgColor="#edeef3">추가 연장 기간(월)</th><td>${list.ADD_USE_MM }</td></tr>
<tr ><th bgColor="#edeef3">사유</th><td>${list.REASON }</td></tr>
</table>
<div style="height:10px"></div>
</c:forEach> 
<p>
 

<c:forEach items="${C_list}" var="list">
<table border=1 cellspacing=0 cellpadding=10  width=100% bgColor="white">
<c:if test="${list.SVC_CD_NM != null}">
<tr><th  bgColor="#edeef3">구분</th><td>${list.SVC_CD_NM } ${list.CUD_CD_NM } / ${list.FAB_NM } ${list.P_KUBUN_NM } </td></tr>
</c:if>
<tr ><th bgColor="#edeef3">제목</th><td>${list.REQ_TITLE }</td></tr>
<tr ><th bgColor="#edeef3">템플릿</th><td>${list.CATALOG_NM }</td></tr>
<tr ><th bgColor="#edeef3">업무</th><td>${list.CATEGORY_NM }</td></tr>
<c:forEach items="${list.CUST_DATA}" var="cust">
<tr ><th bgColor="#edeef3">${cust.key }</th><td>${cust.value }</td></tr>
</c:forEach>
<tr ><th bgColor="#edeef3">설명</th><td><pre>${list.CMT }</pre></td></tr>
</table>
<c:if test="${list.SUMMARY.size() > 0}">
<table border=1 cellspacing=0 cellpadding=10  width=100% bgColor="white">
<tr bgColor="#edeef3"><td class="stepTable"> <spring:message code="warnings_list_kinds" text="종류"/></td>
		<td class="stepTable"><spring:message code="OS" text="OS"/></td>		
		<td class="stepTable"><spring:message code="SW" text="SW"/></td>		
		<td class="stepTable"><spring:message code="NC_VM_SPEC_SPEC_INFOM" text="사양"/></td>
		<td class="stepTable"><spring:message code="ADD_DISK" text="Disk"/>(GB)</td>
		<td class="stepTable"><spring:message code="cnt" text="개수"/></td>
<%-- 		<td class="stepTable"><spring:message code="CONST_VM_FEE" text="비용"/>(<spring:message code="label_date" text=""/>)</td></tr> --%>
		<c:set var="total" value="0" />
		<c:set var="count" value="0" />
		<c:set var="disk" value="0" />
		<c:set var="time" value="0" />
<c:forEach items="${list.SUMMARY}" var="summary">
		<tr><td class="stepTable">${summary.NAME}</td>
		<td class="stepTable">${summary.IMAGE}</td>		
		<td class="stepTable">${summary.SW }</td>		
		<td class="stepTable">${summary.FLAVOR}</td>
		<td class="stepTable" style="text-align:right">${summary.DISK_SIZE}</td>
		<td class="stepTable" style="text-align:right">${summary.COUNT}</td>
<%-- 		<td class="stepTable" style="text-align:right"><fmt:formatNumber type="number" maxFractionDigits="3" value="${summary.FEE}" /></td></tr> --%>
<%-- 		<c:set var="total" value="${total + summary.FEE}" /> --%>
		<c:set var="count" value="${count + summary.COUNT}" />
		<c:set var="disk" value="${disk + summary.DISK_SIZE}" />
		<c:set var="time" value="${time + summary.DEPLOY_TIME}" />
</c:forEach>	
<tr bgColor="#edeef3"><td colspan="4">합계</td><td style="text-align:right"><fmt:formatNumber type="number" maxFractionDigits="3" value="${disk}" /></td>
<td style="text-align:right">${count }</td></tr>	
</table>
</c:if>
<p>
</c:forEach>

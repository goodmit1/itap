<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<style>
    .loginForm {
        width: 1080px;
        height: 650px;
        position: relative;
        margin: auto;
        margin-top: calc(25% - 325px);
    }
    .loginInput {
        float: right;
        width: 50%;
        height: 100%;
        background: white;
    }
    .loginLeftArea {
        position: absolute;
        left: 74px;
        top: 105px;
    }
    .loginLeftArea .loginDesc {
        text-align: left;
        margin-top: 20px;
        color: white !important;
        font-size: 16px;
    }
    .loginDesc .desc_title {
        font-size: 51px;
        font-weight: bold;
        line-height: 0.65;
        letter-spacing: -1.5px;
        color: white;
    }
    .loginDesc .desc_area {
        top: 26px;
        position: relative;
        width: 252px;
        font-size: 16px;
        font-weight: 500;
        line-height: 1.5;
        letter-spacing: -1.1px;
        display: block;
        color: #d3e6ff;
    }
    .login_desc_highlight {
        color: white;
        font-weight: bold;
    }
    .login_input {
        float: right;
        width: 50%;
        height: 100%;
        background: white;
    }
    .login_input > div {
        margin-left: 70px;
        margin-right: 76px;
        margin-bottom: 88px;
        margin-top: 70px;
    }
    .login-btn {
        width: 100%;
        height: 80px;
        background: #1b2a54 !important;
        box-shadow: none !important;
        padding: 0;
        margin: 0;
        margin-top: 45px;
        border: none !important;
        vertical-align: middle;
        text-align: center;
        font-size: 20px;
        color: #ffffff !important;
        font-size: 26px;
        font-weight: bold;
        line-height: 1.27;
        letter-spacing: 0.8px;
    }
    .login_logo {
        width: 272px;
        margin: 30px 70px 50px 54px;
    }
    .login_input ul {
        list-style: none;
        float: left;
        padding: 0;
        margin: 0;
    }
    .login_input ul li {
        list-style: none;
        margin-bottom: 15px;
    }
    .login_input_text {
        height: 32px;
        background-color: white !important;
        border: none !important;
        box-shadow: none;
        padding-left: 40px;
        border-bottom: 1px solid #dddddd !important;
        background-repeat: no-repeat !important;
        background-position: left center !important;
    }
    .login_id_icon{
        position: relative;
        top: 28px;
    }
    .login_pw_icon{
        position: relative;
        top: 27px;
        left: 3px;
    }
</style>
<div class="panel panel-default loginForm" >
    <img src="/res/img/dashboard/mobis/img-left.png" style="width: 50%; height: 100%; float: left;">
    <div class="loginLeftArea">
        <div class="loginDesc">
            <span class="desc_title">M.clovir</span>
            <br/>
            <span class="desc_area">
				<!-- M.clovir는 클라우드 플랫폼 기반에서<br><span class="login_desc_highlight">신속하고 안정적인 고품질의 클라우드<br>인프라</span>를 제공합니다.-->
				<spring:message code="login_desc_m"/><br><label class="login_desc_highlight"><spring:message code="login_highlight_desc"/></label><spring:message code="login_desc2"/>
			</span>
        </div>
    </div>
    <div class="login_input">
        <div>
            <ul>
                <li>
                    <img src="/res/img/dashboard/mobis/login.png" class="login_logo">
                </li>
                <li>
                    <img src="/res/img/mobis/shape-4.png" class="login_id_icon">
                    <input type="text" name="userId" id="userId" placeholder='<spring:message code="msg_input_login_id"/>' class="login_input_text form-control input-lg" >
                </li>
                <li>
                    <img src="/res/img/mobis/shape-5.png" class="login_pw_icon">
                    <input type="password" name="password" id="password" placeholder='<spring:message code="msg_input_password"/>' class="login_input_text form-control input-lg" >
                </li>
                <li>
                    <input type="submit" class="btn btn-primary login-btn" value="<spring:message code="label_confirm" />" />
                </li>
            </ul>
            <div>
                <input type="checkbox" id="idSaveCheck" />
                <div style="display: inline-block; position: relative;"><spring:message code="label_id_save" text="아이디 저장" /></div>
            </div>
        </div>
    </div>
</div>
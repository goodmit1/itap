<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<style>
body
{
	background-color:f3f3f3;
}
.loginForm {
	width: 1080px;
	height: 650px;
	position: relative;
	margin: auto;
	margin-top: calc(25% - 325px);
}
.loginInput {
	float: right;
	width: 50%;
	height: 100%;
	background: white;
}
.loginLeftArea {
	position: absolute;
	left: 74px;
	top: 105px;
}
.loginLeftArea .loginDesc {
	text-align: left;
	margin-top: 20px;
	color: white !important;
	font-size: 16px;
}
.loginDesc .desc_title {
	font-size: 51px;
	font-weight: bold;
	line-height: 0.65;
	letter-spacing: -1.5px;
	color: white;
}
.loginDesc .desc_area {
	top: 26px;
	position: relative;
	width: 252px;
	font-size: 16px;
	font-weight: 500;
	line-height: 1.5;
	letter-spacing: -1.1px;
	display: block;
	color: #d3e6ff;
}
.login_desc_highlight {
	color: white;
	font-weight: bold;
}
.login_input {
	float: right;
	width: 50%;
	height: 100%;
	background: white;
}
.login_input > div {
	margin-left: 70px;
	margin-right: 76px;
	margin-bottom: 88px;
	margin-top: 70px;
}
.login-btn {
	width: 100%;
	height: 80px;
	background: #1b2a54 !important;
	box-shadow: none !important;
	padding: 0;
	margin: 0;
	margin-top: 45px;
	border: none !important;
	vertical-align: middle;
	text-align: center;
	font-size: 20px;
	color: #ffffff !important;
	font-size: 26px;
	font-weight: bold;
	line-height: 1.27;
	letter-spacing: 0.8px;
}
.login_logo {
	width: 272px;
	margin: 30px 70px 50px 54px;
}
.login_input ul {
	list-style: none;
	float: left;
	padding: 0;
	margin: 0;
}
.login_input ul li {
	list-style: none;
	margin-bottom: 15px;
}
.login_input_text {
	height: 32px;
	background-color: white !important;
	border: none !important;
	box-shadow: none;
	padding-left: 40px;
	border-bottom: 1px solid #dddddd !important;
	background-repeat: no-repeat !important;
	background-position: left center !important;
}
.modal-size-position{
	width: 400px;
    margin-top: 20%;
}
.font-size-weight{
	font-size: 20px;
	font-weight: 600;
}
</style>
<%-- <div class="panel-heading">
	<h4><spring:message code="msg_password_re_setting" /></h4>

</div> --%>




<div class="panel panel-default loginForm" >
	<img src="/res/img/dashboard/mobis/img-left.png" style="width: 50%; height: 100%; float: left;">
		<div class="loginLeftArea">
			<div class="loginDesc">
				<span class="desc_title">M.clovir</span>
				<br/>
				<span class="desc_area" id="pwd_change_date"></span>
			</div>
		</div>
		<div class="login_input">
			<div>
				<ul>
					<li>
						<img  src="/res/img/dashboard/mobis/login.png" class="login_logo">
					</li>
					<li> 
						<input type="password" name=password id="password" placeholder="<spring:message code="label_password"/>"
									class="login_input_text form-control input-lg" >
					</li>
					<li>
						<input type="password" name="password1" id="password1"  placeholder="<spring:message code="label_password_confirm"/>"
									class="login_input_text form-control input-lg" >
					</li>
					<li>
						<input type="button" onclick="request()" class="btn btn-primary login-btn" value="<spring:message code="label_confirm" />" />
					</li>
				</ul>
				
			</div>
		</div>
</div>

<div class="modal fade popup_dialog  in" id="myModal" role="dialog">
   <div class="modal-dialog modal-size-position">

     <!-- Modal content-->
     <div class="modal-content container-fluid">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title font-size-weight"><spring:message code="msg_jsf_notifications"/></h4>
       </div>
       <div class="modal-body">
         <p><spring:message code="msg_new_password_change_input"/></p>
       </div>
      <!--  <div class="modal-footer">
       </div> -->
     </div>
     
   </div>
 </div>
<script>
$(document).ready(function(){
	new_pwd_popup();
	get_msg_date();
})

function new_pwd_popup(){
	$('#myModal').modal();        
}

function get_msg_date(){
	var date = getMessage("<spring:message code="msg_pwd_change_guide"/>",60);
	$('#pwd_change_date').html(date)
	// 
}
</script>



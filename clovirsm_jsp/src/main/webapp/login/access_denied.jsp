<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page language="java" contentType="text/html; charset=utf-8"    pageEncoding="utf-8"%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head id="j_idt2">
<script>
var dateLabel = {};
dateLabel.label_term = "<spring:message code="label_term" text="" />";
dateLabel.year = "<spring:message code="label_year" text="" />";
dateLabel.month = "<spring:message code="label_month" text="" />";
dateLabel.week = "<spring:message code="label_week" text="" />";
dateLabel.date = "<spring:message code="label_date" text="" />";
dateLabel.hour = "<spring:message code="hour" text="" />";
dateLabel.minute = "<spring:message code="minute" text="" />";
dateLabel.second = "<spring:message code="second" text="" />";

dateLabel.day = "<spring:message code="label_day" text="" />";
dateLabel.today = "<spring:message code="dt_today" text="" />";
dateLabel.pre_month = "<spring:message code="label_pre_month" text="" />";
dateLabel.next_month = "<spring:message code="label_next_month" text="" />";

dateLabel.sunday_min = "<spring:message code="label_sunday" text="" />";
dateLabel.monday_min = "<spring:message code="label_monday" text="" />";
dateLabel.tuesday_min = "<spring:message code="label_tuesday" text="" />";
dateLabel.wednesday_min = "<spring:message code="label_wednesday" text="" />";
dateLabel.thursday_min = "<spring:message code="label_thursday" text="" />";
dateLabel.friday_min = "<spring:message code="label_friday" text="" />";
dateLabel.saturday_min = "<spring:message code="label_saturday" text="" />";

dateLabel.sunday    = dateLabel.sunday_min    + dateLabel.day;
dateLabel.monday    = dateLabel.monday_min    + dateLabel.day;
dateLabel.tuesday   = dateLabel.tuesday_min   + dateLabel.day;
dateLabel.wednesday = dateLabel.wednesday_min + dateLabel.day;
dateLabel.thursday  = dateLabel.thursday_min  + dateLabel.day;
dateLabel.friday    = dateLabel.friday_min    + dateLabel.day;
dateLabel.saturday  = dateLabel.saturday_min  + dateLabel.day;
</script>
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalabel=0" />
            <meta name="apple-mobile-web-app-capable" content="yes" />
<style>
.exception-code {
    background-color: #ffb300;
}
.exception-panel {
    width: 550px;
    height: 480px;
    background-color: #ffffff;
    position: absolute;
    left: 50%;
    top: 50%;
    margin-left: -275px;
    margin-top: -240px;
    padding: 0;
    text-align: center;
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -webkit-box-shadow: 0 3px 5px 0 rgba(0, 0, 0, 0.2), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 6px 10px 0 rgba(0, 0, 0, 0.14);
    -moz-box-shadow: 0 3px 5px 0 rgba(0, 0, 0, 0.2), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 6px 10px 0 rgba(0, 0, 0, 0.14);
    box-shadow: 0 3px 5px 0 rgba(0, 0, 0, 0.2), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 6px 10px 0 rgba(0, 0, 0, 0.14);
}
.exception-detail h1
{
margin-top:60px
}

</style>
        <title><spring:message code="msg_access_denied"/></title></head><body class="exception-body accessdenied">
        <div class="exception-panel">
            <div class="exception-code"><img id="j_idt7" src="/res/img/401.svg" alt="" />
            </div>

            <div class="exception-detail">
                <h1><spring:message code="msg_access_denied"/></h1>
                <p><spring:message code="msg_ask_admin"/></p><button type="button" onclick="window.open('/home/home.jsp','_self')"><span ><spring:message code="msg_go_home"/></span></button>
            </div>
        </div></body>

</html>

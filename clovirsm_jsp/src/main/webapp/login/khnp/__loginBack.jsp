<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<style>
    body {
        background-color: #f5f6fa;
    }

    input[id="idSaveCheck"] {
        display: none;
    }

    input[id="idSaveCheck"] + label {
        display: inline-block;
        cursor: pointer;
        position: relative;
        /*  padding-left: 25px;
         margin-right: 15px; */
        font-size: 13px;
        float: left;
    }

    input[id="idSaveCheck"]+ label:before {
        content: "";
        display: inline-block;
        margin-right: 10px;
        /* position: absolute; */
        left: 0;
        bottom: 1px;
        width: 18px;
        height: 18px;
        border-radius: 3px;
        border: solid 2px #929594;
        background-color: #ffffff;
    }

    input[id="idSaveCheck"]:checked + label:before {
        padding-top: 3px;
        content: url("/res/img/hynix/newHynix/shape-516.png"); /* 체크모양 */
        text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
        font-weight: 800;
        color: #ffffff;
        background: #929594;
        text-align: center;
        line-height: 18px;
    }

    .loginForm {
        width: 1080px;
        height: 590px;
        position: relative;
        margin: 0 auto;
        margin-top: calc(25% - 325px);
        background-color: rgba(255,255,255,0);
        border: none;
        box-shadow: none;
    }
    .loginInput {
        float: right;
        width: 50%;
        height: 100%;
        background: white;
    }

    .login_input {
        width: 400px;
        height: 297px;
        margin: 0 auto;
        border-radius: 6px;
        box-shadow: 0px 10px 10px 0 rgba(0, 0, 0, 0.25);
        background-color: #ffffff;
    }
    .login_input > ul {
        padding: 0;
    }
    .login_input > div {
        /* 	margin-left: 70px;
            margin-right: 76px; */
        /* 	margin-bottom: 88px; */
        /* 	margin-top: 70px; */
    }
    .login-btn:active:hover{
        background-color: #00AC4F !important;
        color:white;
        font-weight: bold;
    }
    .login-btn {
        width: 340px;
        height: 30px;
        border-radius: 30px;
        background-color: #00AC4F;
        box-shadow: none !important;
        padding: 0;
        margin: 0;
        border: none !important;
        text-align: center;
        font-size: 20px;
        color: #ffffff !important;
        font-size: 12px;
        font-weight: bold;
        line-height: 1.27;
        letter-spacing: 0.8px;
        margin-top: 30px;
    }
    .login_logo {
        width: 520px;
        height: 230px;
        text-align: center;
        margin: 0 auto;
        padding: 27px 0px 0px 0px;
    }
    .login_input ul {
        list-style: none;
        float: left;
        padding: 0;
        margin: 0;
    }
    .login_input ul li {
        list-style: none;
        /* margin-bottom: 15px; */
        float: right;
        clear: both;
    }
    #login-form > div > div.login_input > div > ul > li:nth-child(2) > p{
        width: 265px;
        height: 39px;
        margin: 48px 142px 35px 0px;
        font-size: 35px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: center;
        color: #42a7f2;

    }
    .login_input .checkbox{
        float: left;
        width: 18px;
        height: 18px;
        border-radius: 3px;
        border: solid 2px #929594;
        background-color: #929594;
        margin: 4px 10px 0px 0px;
    }
    .login_input .checkbox_text{
        float: left;
        width: 100px;
        height: 16px;
        font-size: 10px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 19px;
        letter-spacing: normal;
        text-align: left;
        color: #929594;
        margin: 0;
    }
    .service_text{
        width: 263px;
        height: 40px;
        font-size: 16px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: center;
        color: #373a3a;
        margin: 31px 130px 0px 0px;
    }
    .form_out_text{
        font-size: 14px;
        font-weight: bold;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: center;
        color: #3c404a;
        margin: 0 0 0 0;
        font-weight: 900;
    }
    .log_img{
        width: 137px;
        height: 23px;
        margin-top: 123px;
    }
    .login_input_text {
        width: 340px;
        height: 40px;
        margin: 0px 77px 10px 0px;
        border-radius: 0px;
        background-color: #f4f6f8 !important;
        padding-left: 10px;
        border: 1px solid #d2d4d9 !important;
    }
    .login_top {
        height: 50px;
        background-color: #3c404a;
        color: white;
        padding: 16px;
        font-size: 14px;
        padding-left: 30px;
        font-weight: 900;
    }
    .login_contents {
        padding: 30px;
    }
    .login_bottom {
        width: 400px;
        margin: 0 auto;
        margin-top: 30px;
        position: relative;
    }
    .login_bottom div{
        color: #a4a7a9;
        display: inline-block;
        font-size: 10px;
        position: absolute;
        top: 10px;
        left: 130px;

    }
    .login_info {
        font-size: 12px;
        color: #3c404a;
        margin-top: 20px;
    }
</style>
<div class="panel panel-default loginForm" >
    <div class="login_logo">
        <div>
            <img src="/res/img/clovirsm-v.png" style="margin-bottom: 25px;height: 100px;">
        </div>
        <p class="form_out_text">ClovirSM은 클라우드 플랫폼 기반에서 </p>
        <p class="form_out_text">신속하고 안정적인 고품질의 클라우드 인프라를 제공합니다. </p>
    </div>
    <div class="login_input">
        <div class="login_top">
            Login
        </div>
        <div class="login_contents">
            <input type="text" name="userId" id="userId" class="login_input_text form-control input-lg" placeholder="ID" >
            <input type="password" name="password" id="password" class="login_input_text form-control input-lg" placeholder="Password" style="clear: both;">
            <input type="checkbox" id="idSaveCheck" class="checkbox"/>
            <label for="idSaveCheck"></label>
            <div class="checkbox_text"><spring:message code="label_id_save" text="아이디 저장" /></div>
            <input type="submit" class=" login-btn" value="<spring:message code="label_confirm" />" />
            <div class="login_info"></div>
        </div>
    </div>
    <div class="login_bottom">
    </div>
</div>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<style>
body
{
	background-color:#a3a0af
}
.panel-heading h4
{
	color : #2d062d;
}
.loginForm
{
	 width:400px;
	 margin-left : auto;
	 margin-right : auto;
	 margin-top : 150px;
}
.row
{
	margin-top: 20px;
}
.panel-footer
{
	margin-top: 40px;
	text-align: right;
}
</style>
<div class="panel panel-default loginForm" >
	<div class="panel-heading">
		<h4><spring:message code="msg_password_re_setting" /></h4>

	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-12">
				<label class="control-label  grid-title-sm" for="password"><spring:message code="msg_new_password" /></label>
				<input type="password" name=password id="password" placeholder="<spring:message code="msg_password_role"/>"
										class="form-control input-lg" >

			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<label class="control-label  grid-title-sm" for="password1"><spring:message code="label_password"/> <spring:message code="label_confirm" /></label>
				<input type="password" name="password1" id="password1"  placeholder="<spring:message code="msg_password_role"/>"
										class="form-control input-lg" >

			</div>
		</div>

	</div>
	<div class="panel-footer" >
	<input type="submit" onclick="return request()" class="btn btn-primary " value="<spring:message code="label_confirm" />" />
	</div>
</div>
		<div style="text-align:center">${__MSG__}</div>

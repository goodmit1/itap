<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	if(popupid == null || "".equals(popupid)){
		popupid = "user_search_select";
	}
	request.setAttribute("popupid", popupid);
%>
<style>
	#newVm_iframe_user_search_select_popup_form > div.panel-body > div > div.col.col-sm-3{
		min-height: 1px;
	    padding-right: 15px;
	    padding-left: 15px;
	    border-style: none;
	}
	#newVm_iframe_user_search_select_popup_form > div.panel-body > div > div.col.col-sm-6{
		min-height: 1px;
	    padding-right: 15px;
	    padding-left: 15px;
	    border-style: none;
	}
	
}
</style>
<fm-modal id="${popupid}" title="<spring:message code="FM_USER_SEARCH_POPUP_TITLE"/>" cmd="header-title" >
	<form id="${popupid}_form" action="none" onsubmit="return false;">
		<template>
			<div class="panel-body">
				<div class="search_area">
					<div class="col col-sm-3 colmargin3">
						<fm-select id="${popupid}_user_keyword_field" name="keyword_field" :options="{USER_NAME:'<spring:message code="FM_USER_USER_NAME" text="사용자" />', 'LOGIN_ID':'<spring:message code="FM_USER_USER_ID" text="" />', 'EMAIL':'<spring:message code="FM_USER_EMAIL" text="" />', TEAM_NM:'<spring:message code="FM_TEAM_TEAM_NM" text="부서명" />'}" ></fm-select>
					</div>
					<div class="col col-sm-6 colmargin6">
						<fm-input id="${popupid}_user_keyword" name="keyword" placeholder="<spring:message code="label_keyword" text=""/>"  ></fm-input>
						<input type="text" name="DUMMY" style="display:none"/>
					</div>
					<div class="col btn_group nomargin">
						<input type="button" onclick="${popupid}_user_search_select()" class="searchBtn btn" value="<spring:message code="btn_search" text="검색"/>" />
					</div>
				</div>
			</div>
			<div id="${popupid}_userSearchSelectGrid" style="height:250px" class="ag-theme-fresh"></div>

			<div style="width: 100%; padding: 15px; border-bottom: 1px solid #ddd;">
				<input placeholder="이메일 직접 입력" name="email" id="${popupid}_user_search_select_popup_email" class="form-control input" style="width: 300px; margin-right:10px;" onkeypress="if(event.keyCode == 13) { ${popupid}_add_user(); return false; }" /><input id="${popupid}_add_email_button" type="button" onclick="${popupid}_add_user()" class="searchBtn btn" value="추가" />
			</div>			
			<div id="${popupid}_selectedUser" style="width: 100%; min-height: 28px; height: auto;padding: 5px 10px 15px 10px; text-align:left;">
				<div :id="'selected_' + selectedUser.LOGIN_ID" v-for="selectedUser in selectedUserList" style="height: 28px; display: inline-block; margin-right: 5px; border: 1px solid rgb(221, 221, 221); border-radius: 5px;padding:0 10px;margin-top:10px;">
					<div v-if="selectedUser.USER_NAME != null" style="display: inline-block; padding: 0px;line-height:26px;">{{selectedUser.USER_NAME}}({{selectedUser.EMAIL}})</div>
					<div v-if="selectedUser.USER_NAME == null" style="display: inline-block; padding: 0px;line-height:26px;">{{selectedUser.EMAIL}}</div>
					<button class="delete_selected_user" style="display:inline-block;border: none; border-radius:50%; background-color: transparent;padding:0;" v-on:click.stop="deleteSelected(selectedUser.LOGIN_ID);">
						<i class="fa fa-trash" style="font-size:16px; color: #333333;margin-top:2px;"></i>
					</button>
				</div>
			</div>
			
			<div id="${popupid}_actionArea" style="display:none;text-align:right;">
				<input id="${popupid}_action_button" type="button" onclick="${popupid}_action()" class="searchBtn btn" value="" />
			</div>
		</template>
	</form>

	<script>
		var ${popupid} = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {keyword:''};
		var ${popupid}_parent_vue = null;
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
				, selectedUserList: []
			}
		});
		function ${popupid}_click(vue, param){
			${popupid}_parent_vue = vue;
			if(param != null && param=='Y'  && ADMIN_YN != 'Y')
			{
				${popupid}_param.MY_TEAM = 'Y'
			}
			 
			${popupid}_vue.form_data = ${popupid}_param;
			return true;
		}
		
		function ${popupid}_user_search_select(param){
			
			${popupid}_userSearchSelectReq.searchSub('/api/popup/userSearch/list', ${popupid}_param, function(data) {
				${popupid}_userSearchSelectGrid.setData(data);
			});
		}
		
		function ${popupid}_action(){
			${popupid}.targetvue.action(${popupid}_vue.selectedUserList);
		}
		
		function ${popupid}_select_user(data){
			if(data.EMAIL == undefined || data.EMAIL == null || data.EMAIL == ''){
				alert('선택된 사용자는 이메일이 없습니다.');
				return;
			}
			for(var i = 0;  i < ${popupid}_vue.selectedUserList.length; i++){
				var selectedUser = ${popupid}_vue.selectedUserList[i];
				if((data.LOGIN_ID != null && selectedUser.LOGIN_ID == data.LOGIN_ID) || selectedUser.EMAIL == data.EMAIL) {
					alert('이미 추가되었습니다.');
					return;
				}
			}
			${popupid}_vue.selectedUserList.push(data);
		}
		
		function deleteSelected(LOGIN_ID){
			for(var i = 0;  i < ${popupid}_vue.selectedUserList.length; i++){
				var selectedUser = ${popupid}_vue.selectedUserList[i];
				if(selectedUser.LOGIN_ID == LOGIN_ID) {
					${popupid}_vue.selectedUserList.splice(i, 1);
					break;
				}
			}
		}
		
		function ${popupid}_isEmail(asValue) {
			var regExp = /^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/;
			return regExp.test(asValue); // 형식에 맞는 경우 true 리턴	
		}
		
		function ${popupid}_add_user(){
			var email = $('#${popupid}_user_search_select_popup_email').val();
			if(email == null || email == ''){
				alert('이메일을 입력해주세요.');
				return;
			}
			
			var isEmail = ${popupid}_isEmail(email);
			if(isEmail){
				${popupid}_select_user({
					USER_NAME: null
					, EMAIL: email
				});
			} else {
				alert('이메일 형식에 맞게 입력해주세요.');
				$('#${popupid}_user_search_select_popup_email').select();
			}
		}

		var ${popupid}_userSearchSelectReq = new Req();
		var ${popupid}_userSearchSelectGrid;
		$(document).ready(function(){
			var ${popupid}_userSearchSelectGridColumnDefs =[
				{headerName: "<spring:message code="FM_TEAM_TEAM_NM" text="부서" />", field: "TEAM_NM", width: 140},
				{headerName: "<spring:message code="FM_USER_USER_ID" text="" />", field: "LOGIN_ID", width: 200},
				{headerName: "<spring:message code="FM_USER_USER_NAME" text="사용자" />", field: "USER_NAME", width: 160},
				{headerName: "<spring:message code="FM_TEAM_EMAIL" text="이메일" />", field: "EMAIL", width: 140}
			];
			var ${popupid}_userSearchSelectGridOptions = {
				columnDefs: ${popupid}_userSearchSelectGridColumnDefs,
				rowData: [],
				sizeColumnsToFit:true,
				enableSorting: true,
				rowSelection:'single',
			    enableColResize: true,
			    onRowDoubleClicked: function(){
					var arr = ${popupid}_userSearchSelectGrid.getSelectedRows();
					var callback = ${popupid}_parent_vue ? ${popupid}_parent_vue.callback: null;
					if(callback != null) eval( callback + '(arr[0], function(){ $(\'#${popupid}\').modal(\'hide\'); });');
			    }
			};
			${popupid}_userSearchSelectGrid = newGrid("${popupid}_userSearchSelectGrid", ${popupid}_userSearchSelectGridOptions);
		});
	</script>
</fm-modal>
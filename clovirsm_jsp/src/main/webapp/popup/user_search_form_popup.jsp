<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	if(popupid == null || "".equals(popupid)){
		popupid = "user_search";
	}
	request.setAttribute("popupid", popupid);
%>
<style>
	#newVm_iframe_user_search_popup_form > div.panel-body > div > div.col.col-sm-3{
		min-height: 1px;
	    padding-right: 15px;
	    padding-left: 15px;
	    border-style: none;
	}
	#newVm_iframe_user_search_popup_form > div.panel-body > div > div.col.col-sm-6{
		min-height: 1px;
	    padding-right: 15px;
	    padding-left: 15px;
	    border-style: none;
	}
}
</style>
<fm-modal id="${popupid}" title="<spring:message code="FM_USER_SEARCH_POPUP_TITLE"/>" cmd="header-title" >
	<form id="${popupid}_form" action="none">
		<template>
			<div class="panel-body">
				<div class="search_area">
					<div class="col col-sm-3 colmargin3">
						<fm-select id="${popupid}_user_keyword_field" name="keyword_field" :options="{USER_NAME:'<spring:message code="FM_USER_USER_NAME" text="사용자" />', 'LOGIN_ID':'<spring:message code="FM_USER_USER_ID" text="" />', 'EMAIL':'<spring:message code="FM_USER_EMAIL" text="" />', TEAM_NM:'<spring:message code="FM_TEAM_TEAM_NM" text="부서명" />'}" ></fm-select>
					</div>
					<div class="col col-sm-6 colmargin6">
						<fm-input id="${popupid}_user_keyword" name="keyword" placeholder="<spring:message code="label_keyword" text=""/>"  ></fm-input>
						<input type="text" name="DUMMY" style="display:none"/>
					</div>
					<div class="col btn_group nomargin">
							<button type="button" onclick="${popupid}_user_search()" class="searchBtn btn" value="<spring:message code="btn_search" text="검색"/>" />
					</div>
				</div>
			</div>
			<div id="${popupid}_userSearchGrid" style="height:250px" class="ag-theme-fresh"></div>
		</template>
	</form>

	<script>
		var ${popupid} = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {keyword:''};
		var ${popupid}_parent_vue = null;
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		function ${popupid}_click(vue, param){
			${popupid}_parent_vue = vue;
			if(param != null && param=='Y'  && ADMIN_YN != 'Y')
			{
				${popupid}_param.MY_TEAM = 'Y'
			}
			 
			${popupid}_vue.form_data = ${popupid}_param;
			return true;
		}

		function ${popupid}_user_search(param){
			
			${popupid}_userSearchReq.searchSub('/api/popup/userSearch/list', ${popupid}_param, function(data) {
				${popupid}_userSearchGrid.setData(data);
			});
		}

		var ${popupid}_userSearchReq = new Req();
		var ${popupid}_userSearchGrid;
		$(document).ready(function(){
			var ${popupid}_userSearchGridColumnDefs =[
				{headerName: "<spring:message code="FM_USER_USER_ID" text="" />", field: "LOGIN_ID", width: 200},
				{headerName: "<spring:message code="FM_USER_USER_NAME" text="사용자" />", field: "USER_NAME", width: 160},
				{headerName: "<spring:message code="FM_TEAM_TEAM_NM" text="부서" />", field: "TEAM_NM", width: 140},
				{headerName: "<spring:message code="FM_TEAM_EMAIL" text="이메일" />", field: "EMAIL", width: 140}
			];
			var ${popupid}_userSearchGridOptions = {
				columnDefs: ${popupid}_userSearchGridColumnDefs,
				rowData: [],
				sizeColumnsToFit:true,
				enableSorting: true,
				rowSelection:'single',
			    enableColResize: true,
			    onRowDoubleClicked: function(){
					var arr = ${popupid}_userSearchGrid.getSelectedRows();
					var callback = ${popupid}_parent_vue ? ${popupid}_parent_vue.callback: null;
					if(callback != null) eval( callback + '(arr[0], function(){ $(\'#${popupid}\').modal(\'hide\'); });');
			    }
			};
			${popupid}_userSearchGrid = newGrid("${popupid}_userSearchGrid", ${popupid}_userSearchGridOptions);
		});
		
		$("#app_owner_change_popup_owner_change_user_search").children().css( "width", "860px" );

	</script>
</fm-modal>
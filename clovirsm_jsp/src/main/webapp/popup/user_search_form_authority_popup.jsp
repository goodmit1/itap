<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	String action = request.getParameter("action");
	if(popupid == null || "".equals(popupid)){
		popupid = "user_search_form_authority_popup";
	}
	request.setAttribute("popupid", popupid);
	request.setAttribute("action", action);
%>
<style>
	#newVm_iframe_user_search_popup_form > div.panel-body > div > div.col.col-sm-3{
		min-height: 1px;
	    padding-right: 15px;
	    padding-left: 15px;
	    border-style: none;
	}
	#newVm_iframe_user_search_popup_form > div.panel-body > div > div.col.col-sm-6{
		min-height: 1px;
	    padding-right: 15px;
	    padding-left: 15px;
	    border-style: none;
	}
	.modal-body {
	    height: 450px;
	}
	#DD_VALUE{
		width:150px;
	}

</style>
<fm-modal id="${popupid}" title="<spring:message code="FM_USER_SEARCH_POPUP_TITLE"/>" cmd="header-title" >
	<form id="${popupid}_form" action="none">
		<template>
			<div class="panel-body">
				<div class="search_area">
					<div class="col col-sm-3">
						<fm-select id="${popupid}_user_keyword_field" name="keyword_field" :options="{USER_NAME:'<spring:message code="FM_USER_USER_NAME" text="사용자" />', 'LOGIN_ID':'<spring:message code="FM_USER_USER_ID" text="" />', TEAM_NM:'<spring:message code="FM_TEAM_TEAM_NM" text="부서명" />'}" ></fm-select>
					</div>
					<div class="col col-sm-6">
						<fm-input id="${popupid}_user_keyword" name="keyword" v-if="'${action}' == 'update'"  disabled="Y" autocomplete="N" placeholder="<spring:message code="label_keyword" text=""/>" onkeyup="if(event.keyCode == 13) ${popupid}_user_search();"></fm-input>
						<fm-input id="${popupid}_user_keyword" name="keyword" v-if="'${action}' != 'update'" autocomplete="N" placeholder="<spring:message code="label_keyword" text=""/>" onkeyup="if(event.keyCode == 13) ${popupid}_user_search();"></fm-input>
						<input type="text" name="DUMMY" style="display:none"/>
					</div>
					<div class="col btn_group none" v-if="'${action}' == 'update'">
							<button type="button" onclick="${popupid}_user_search()" class="btn btn-primary searchBtn" style="float:right;" disabled value="<spring:message code="btn_search" text="검색"/>" />
					</div>
					<div class="col btn_group none" v-if="'${action}' != 'update'">
							<button type="button" onclick="${popupid}_user_search()" class="btn btn-primary searchBtn" style="float:right;" value="<spring:message code="btn_search" text="검색"/>" />
					</div>
				</div>
			</div>
			<div id="${popupid}_userSearchGrid" style="height:250px;   border-top: 1px solid #626262; border-bottom: 1px solid #626262;margin-bottom: 20px;" class="ag-theme-fresh"></div>
			
			<div class="col col-sm-6" style="width:100%;">
				<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_LEVEL_Remove_admin" select_style="width:160px;" label_style="text-align: center;" titlefield="KO_DD_DESC" id="${popupid}_level" emptystr=" " keyfield="DD_VALUE" name="level" title="<spring:message code="project_authority" text="프로젝트 권한" />"></fm-select>
			</div>
		</template>
	</form>
	
	<span slot="footer">
		<button type="button" onclick="insertUserPrj();" class="btn popupBtn finish"><spring:message code="label_add" text="추가"/></button>
		
	</span>
	
	<script>
		var ${popupid}_userArr;
		var ${popupid} = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_parent_vue = null;
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		function insertUserPrj(){
			 
			 
			if(${popupid}_userArr != null || $("#${popupid}_level").val() != ''){
				
				post('/api/cm/prjUser/save', {USER_ID: ${popupid}_userArr[0].USER_ID , LEVEL : $("#${popupid}_level").val() , PRJ_ID : ${popupid}_param.PRJ_ID, CL_ID : ${popupid}_param.CL_ID  }, function(data){
					userSearch();
					alert(msg_complete);
					$('#${popupid}').modal('hide');
				});
			} else{
				
			}
		}
		function ${popupid}_click(vue, param){
			${popupid}_parent_vue = vue;
			
			 
			
			if(param != null){
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
			}
			
			if('${action}' == 'update'){
				${popupid}_param.level = param.level;
				${popupid}_param.keyword = param.USER_NAME;
				${popupid}_user_search();
				
			} else{
				${popupid}_param.user_my_id = param.USER_ID;
				
			}
			 
			${popupid}_vue.form_data = ${popupid}_param;
			return true;
		}
		
		function ${popupid}_user_search(param){
			${popupid}_vue.form_data.keyword = $("#${popupid}_user_keyword").val();
			${popupid}_userSearchReq.searchSub('/api/popup/userSearch/list', ${popupid}_vue.form_data, function(data) {
				${popupid}_userSearchGrid.setData(data);
			});
		}

		var ${popupid}_userSearchReq = new Req();
		var ${popupid}_userSearchGrid;
		$(document).ready(function(){
			var ${popupid}_userSearchGridColumnDefs =[
				{headerName: "<spring:message code="FM_USER_USER_ID" text="" />", field: "LOGIN_ID", width: 200},
				{headerName: "<spring:message code="FM_TEAM_TEAM_NM" text="부서명" />", field: "TEAM_NM", width: 140},
				{headerName: "<spring:message code="FM_USER_USER_NAME" text="사용자" />", field: "USER_NAME", width: 160}
			];
			var ${popupid}_userSearchGridOptions = {
				columnDefs: ${popupid}_userSearchGridColumnDefs,
				rowData: [],
				sizeColumnsToFit:true,
				enableSorting: true,
				rowSelection:'single',
			    enableColResize: true,
			    onRowClicked: function(){
			    	${popupid}_userArr = ${popupid}_userSearchGrid.getSelectedRows();
			    }
			};
			${popupid}_userSearchGrid = newGrid("${popupid}_userSearchGrid", ${popupid}_userSearchGridOptions);
		});
	</script>
</fm-modal>
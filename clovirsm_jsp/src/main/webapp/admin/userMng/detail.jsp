<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!-- 입력 Form -->
<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body" id="contents">
		<input id="USER_ID" name="USER_ID" type="hidden" v-model="form_data.USER_ID" />
		<div class="col col-sm-6">
			<fm-input id="LOGIN_ID" name="LOGIN_ID" required="true" title="<spring:message code="FM_USER_LOGIN_ID" text="사용자ID" />" maxlength="20"></fm-input>
		</div>
		<div class="col col-sm-6">
			<fm-input id="USER_NAME" name="USER_NAME" required="true" title="<spring:message code="" text="사용자명" />" maxlength="20"></fm-input>
		</div>
		<div class="col col-sm-6">
			<fm-input id="EMAIL" name="EMAIL" required="true" title="<spring:message code="FM_USER_EMAIL" text="" />" maxlength="25"></fm-input>
		</div>
		<div class="col col-sm-6">
			<fm-select url="/api/code_list?grp=USER_TYPE" id="USER_TYPE" required="true"
					   name="USER_TYPE" title="<spring:message code="FM_USER_USER_TYPE" text="권한"/>">
			</fm-select>
		</div>
		<div class="col col-sm-6">
			<fm-popup id="TEAM_NM" name="TEAM_NM" required="true" title="<spring:message code="" text="부서" />" disabled="disabled"></fm-popup>
			<input type="hidden" name="TEAM_CD" id="TEAM_CD">
		</div>
		<div class="col col-sm-6">
			<fm-input id="POSITION" name="POSITION" title="<spring:message code="FM_USER_POSITION" text="직급" />" maxlength="20"></fm-input>
		</div>
		<div class="col col-sm-6">
			<fm-select-yn id="USE_YN" name="USE_YN" title="<spring:message code="USE_YN" text=""/>">
			</fm-select-yn>
		</div>
		<div class="col col-sm-6">
			<fm-output id="LAST_ACCESS_TMS" name="LAST_ACCESS_TMS"
					   title="<spring:message code="FM_USER_LAST_ACCESS_TMS" text="마지막 접속시간" />"></fm-output>
		</div>
	</div>
	<script>
	</script>
</div>

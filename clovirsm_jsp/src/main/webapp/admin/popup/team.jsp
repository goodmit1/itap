<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%
pageContext.setAttribute("popup_id", "team");
%>
<fm-modal id="${popup_id}_popup" title="<spring:message code="label_select_team"/>" cmd="header-title">

	<span slot="footer">
		<input type="button" class="btn popupBtn normal" value="<spring:message code="no_select" text="선택없음"/>" onclick="setPopupReturnVal({})" >
		<input type="button" class="btn popupBtn normal" value="<spring:message code="btn_reload" text="새로고침"/>" onclick='$("#popup_menu_tree").trigger("reload");' >
	</span>
	<form id="${popup_id}_popup_form" style="height:300px;overflow:auto" action="none">
			<fm-tree id="popup_team_tree"  url="/api/team/list?USE_YN=Y" :field_config="{onselect:menu_popup_click, root:0,id:'TEAM_CD',parent:'PARENT_CD',text:'TEAM_NM'}" ></fm-tree>
	</form>

	<script>
	function menu_popup_click(selectedArr)
	{

		setPopupReturnVal(selectedArr[0]);
	}
	</script>



</fm-modal>

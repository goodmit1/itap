<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%
pageContext.setAttribute("popup_id", "prgm");
%>
<fm-modal id="${popup_id}_popup" title="<spring:message code="label_select_prgm"/>" cmd="header-title">

	<span slot="footer">
		<input type="button" class="btn popupBtn normal" value="<spring:message code="no_select" text="선택없음"/>" onclick="setPopupReturnVal({})" >

	</span>
	<form id="${popup_id}_popup_form" >
	<input type="hidden" name="POPUP_ID" value="15" />
	<input type="hidden" name="CHK_TEAM" value="N" />
	<input type="hidden" name="ROLE_ID" id="PRGM_ROLE_ID"  v-model="form_data.PRGM_ROLE_ID" />
	<input type="hidden" name="default_search_field" value="PRGM_PATH,PRGM_NM" />
	<div id="${popup_id}_popupsearch_area" class="search-panel panel panel-default search_area">
		<div class="panel-body">

			<div class="row">
			<div class="col col-sm-4" style="width: 130px;">
				<fm-select id="keyword_field" emptystr="<spring:message code="label_field" text=""/>" :options="{'PRGM_NM':'<spring:message code="title_prgm"/>', PRGM_PATH:'<spring:message code="label_path"/>'}" ></fm-select>
			</div>
				<div class="col col-sm-4" style="width: 130px;">
				<fm-select id="keyword_operator" emptystr="<spring:message code="label_operator" text=""/>" url="/api/code_list?grp=sys.sqlCond" ></fm-select>
			</div>


			<div class="col col-sm-4" style="width: 130px;">
				<fm-input id="keyword" name="keyword" placeholder="<spring:message code="label_keyword" text=""/>"></fm-input>
			</div>
			<div class="col btn_group nomargin">
			<button type="button" onclick="cur_popup.doSearch()" class="searchBtn" />
			</div>
			</div>
		</div>


	</div>
	<div id="${popup_id}_grid1" style="height:300px" class="ag-theme-fresh"></div>
	</form>



	<script>
	var ${popup_id}_popup_grid1;
	$(document).ready(function(){
		var columnDefs1 =[
		                  {headerName: "<spring:message code="title_prgm"/>", field: "PRGM_NM", width:200},
		   			     {headerName: "<spring:message code="label_path"/>", field: "PRGM_PATH", width: 300}];
		var gridOptions1 = {
			    columnDefs: columnDefs1,
			    rowData: [],
			    sizeColumnsToFit:false,
			    enableSorting: true,
			    rowSelection:'single',
			    enableColResize: true,
			    onRowClicked: function()
			    {
			    	var arr = ${popup_id}_popup_grid1.getSelectedRows();

			    	setPopupReturnVal(arr[0] );
			    }
			};
		${popup_id}_popup_grid1 = newGrid("${popup_id}_grid1", gridOptions1);
		
		
		$("form").keydown(function (e){
			if(e.keyCode == 13)
			{
				//console.log(e);
				cur_popup.doSearch();
			}
		})

	})

	</script>


</fm-modal>

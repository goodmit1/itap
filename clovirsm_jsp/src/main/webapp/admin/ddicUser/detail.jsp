<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!-- 입력 Form -->
<div class="form-panel detail-panel detail-resize panel panel-default" id="contents">
	<div class="panel-body">
<!-- 		<div class="col col-sm-6"> -->
<%-- 			<fm-input id="I_DD_NM" name="DD_NM" required="true" title="<spring:message code="FM_DDIC_DD_NM" text="그룹명" />" div_style="display:none;"></fm-input> --%>
<!-- 			<fm-select url="/api/ddicUser/list/list_FM_DDIC_USER_SEARCH/" id="select_DD_NM" -->
<!-- 				titlefield="DD_DESC" keyfield="DD_DESC" emptystr="" onchange="selectDdicChange()" -->
<%-- 				name="DD_NM" title="<spring:message code="FM_DDIC_DD_NM" text="그룹명"/> " required="true"> --%>
<!-- 			</fm-select> -->
<!-- 		</div> -->
		<div class="col col-sm-6">
			<fm-input id="DD_VALUE" name="DD_VALUE"   required="true" title="<spring:message code="FM_DDIC_DD_VALUE" text="코드값" />" maxlength="15"></fm-input>
		</div>
		<div class="col col-sm-6">
			<fm-input id="DD_DESC" name="${_LANG_.toUpperCase()}_DD_DESC" required="true" title="<spring:message code="FM_DDIC_KO_DD_DESC" text="코드명" />" maxlength="50"></fm-input>
		</div>
		<div class="col col-sm-6">
			<fm-input id="SORT" name="SORT"  title="<spring:message code="FM_DDIC_SORT" text="정렬순서" />"></fm-input>
		</div>
		<div class="col col-sm-6">
			<fm-input id="EXT_DD_ID" name="EXT_DD_ID"  title="<spring:message code="FM_DDIC_EXT_DD_ID" text="기타값" />"></fm-input>
		</div>
	</div>
	<script>
	</script>
</div>

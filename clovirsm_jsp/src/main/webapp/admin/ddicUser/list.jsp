<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 코드 관리 -->
<style>
div.ag-header.ag-pivot-off > div.ag-header-viewport > div > div > div:nth-child(1) > div.ag-cell-label-container.ag-header-cell-sorted-none{
	width: auto;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
	
		<div class="col col-sm">
			<!-- <fm-select url="/api/code_list?grp=MNG_DDIC" id="S_DD_VALUE" -->
			<fm-select url="/api/ddicUser/list/list_FM_DDIC_USER_SEARCH/" id="S_DD_ID"
				titlefield="DD_DESC" keyfield="DD_VALUE" 
				name="DD_ID" title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>">
			</fm-select>
		</div>
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="ddicSearch()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title ">
		<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportspecSetting()" class="layout excelBtn top0" id="excelBtn"></button>
			<fm-sbutton cmd="update" class="newBtn contentsBtn tabBtnImg new top0" onclick="insertRow()"   ><spring:message code="btn_new" text="신규"/></fm-sbutton>
			<fm-sbutton cmd="delete" class="delBtn contentsBtn tabBtnImg del top0" onclick="del()"   ><spring:message code="btn_del" text="삭제"/></fm-sbutton>
			<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg save top0" class="btn btn-primary" onclick="save()"><spring:message code="btn_save" text="저장"/></fm-sbutton>
		</div>
	</div>
	<div id="mainTable" class="ag-theme-fresh" style="height:300px;"></div>
	<jsp:include page="detail.jsp"></jsp:include>
</div>
<script>

	var ddicUserReq = new Req();
	var mainTable;
	
	function exportspecSetting(){
		mainTable.exportCSV({fileName:'User Code List'})
	}
	/* function savespecSetting(){
		mainTable.gridOptions.api.stopEditing();
		var arr = mainTable.getData();
		for(var i in arr){
			arr[i].IU = "U";
		}
		var selected_json = JSON.stringify(arr);
		post("/api/ddicUser/save", {"selected_json" : selected_json}, function(){
			searchspecSetting();
		})
	} */
	function ddicSearch(){
		ddicUserReq.search('/api/ddicUser/list/list_FM_DDIC_USER/', function(data){
			mainTable.setData(data);
		});
	}
	
	function insertRow()
	{
		ddicUserReq.setData();
		$("#I_DD_NM").parent().css("display","none");
		$("#select_DD_NM").parent().css("display","inline");
		$("#DD_VALUE").attr("disabled",false);
		$("#I_DD_NM").attr("disabled",false);

	}
	
	function save() {
		if(!validate("contents")){
			return false;
		} 
		if(confirm(msg_confirm_save)){
			form_data.DD_ID = $("#S_DD_ID").val();
			form_data.DD_NM = $("#S_DD_ID option:selected").html();
			post('/api/ddicUser/save', form_data , function(){
				alert("<spring:message code="saved" text="저장되었습니다"/>");
				ddicSearch();
			})
		}
	}
	
	function del(){
		//form_data.ID = ddicUserReq.getData().DD_ID.concat(".",ddicUserReq.getData().DD_VALUE);
		ddicUserReq.del('/api/ddicUser/delete', function(){
			ddicSearch();
		});
	}
	
	function selectDdicChange(){
		ddicUserReq.putData({DD_NM : $("#select_DD_NM").html(), DD_ID : $("#select_DD_NM option:selected").prop("data").DD_VALUE});
		
	}
	$(document).ready(function(){
		var columnDefs =[
		    { 
				headerName: "<spring:message code="FM_DDIC_DD_ID" text="그룹ID" />", 
				field: "DD_ID", 
				hide: true
		    },
		   	{ 
				headerName: "<spring:message code="FM_DDIC_DD_NM" text="그룹명" />", 
				field: "DD_NM" 
		    },
			{
				headerName: "<spring:message code="FM_DDIC_DD_VALUE" text="코드값" />",
				field: "DD_VALUE",
				//hide: true
			},
		    {
				headerName: "<spring:message code="FM_DDIC_KO_DD_DESC" text="코드명" />",
				field: "DD_DESC",
			},
		    {
				headerName: "<spring:message code="FM_DDIC_SORT" text="정렬순서" />",
				field: "SORT",
			},
	     ];
		var gridOptions = {
			    columnDefs: columnDefs,
			    rowData: [],
				rowSelection : 'single',
			    enableSorting: true,
			    enableColResize: true,
			    onRowClicked : function() {
					var arr = mainTable.getSelectedRows();
					$("#DD_VALUE").attr("disabled",true);
					$("#I_DD_NM").attr("disabled",true);
					
					$("#I_DD_NM").parent().css("display","inline");
					$("#select_DD_NM").parent().css("display","none");
					ddicUserReq.setData(arr[0]);
				},
			};
		mainTable = newGrid("mainTable", gridOptions)
	// 	mainTable.columnApi.setColumnVisible('DD_VALUE',false)
		ddicSearch();
	});
</script>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<!-- 사용자 관리 -->

<!--  조회 조건 및 버튼 -->
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->


		<div class="col">
			<fm-input id="S_LOGIN_ID" name="LOGIN_ID" title="ID"></fm-input>
		</div>
		<div class="col col-sm-3">
			<fm-popup id="S_TEAM_NM" name="TEAM_NM" title="<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />"> </fm-input> <input
				type="hidden" id="S_TEAM_CD" name="TEAM_CD">
		</div>
		<div class="col ">
			<fm-input id="S_USER_NAME" name="USER_NAME" title="<spring:message code="FM_USER_USER_NAME" text="사용자" />"></fm-input>
		</div>
		<div class="col btn_group">
			<input type="button" class="searchBtn btn" onclick="search()" value="<spring:message code="btn_search" text="" />" />
			<fm-sbutton cmd="update" class="btn btn-primary" onclick="passwordReset()" ><spring:message code="btn_password_reset" text="" /></fm-sbutton>
			<fm-sbutton cmd="update" class="btn btn-primary" onclick="newUser()" ><spring:message code="btn_new" text="" /></fm-sbutton>
			<fm-sbutton cmd="delete" class="btn btn-primary" onclick="deleteUser()" ><spring:message code="btn_delete" text=""/></fm-sbutton>
			<fm-sbutton cmd="update" class="btn btn-primary" onclick="saveUser()" ><spring:message code="btn_save" text="" /></fm-sbutton>


		</div>
	</div>


</div>
<div class="fullGrid" id="input_area">
<!--  사용자 목록 -->
	<div id="mainTable" style="height: 400px" class="ag-theme-fresh"></div>

	<jsp:include page="detail.jsp"></jsp:include>
</div>
<script>
					var user = new Req();
					mainTable;
					$(function() {
						var
						columnDefs = [ {
							headerName : "",
							hide : true,
							field : "USER_ID"
						}, {
							headerName : "ID",
							field : "LOGIN_ID"
						}, {
							headerName : "<spring:message code="FM_USER_USER_NAME" text="사용자" />",
							field : "USER_NAME"
						}, {
							headerName : "<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />",
							field : "TEAM_NM"
						}, {
							headerName : "<spring:message code="OFFICE_NO" text="내선번호"/>",
							field : "OFFICE_NO"
						}, {
							headerName : "<spring:message code="FM_USER_LAST_ACCESS_TMS"/>",
							field : "LAST_ACCESS_TMS"
						}, {
							headerName : "<spring:message code="USE_YN" text=""/>",
							field : "USE_YN"
						} ];
						var
						gridOptions = {
							columnDefs : columnDefs,
							rowData : [],
							onRowClicked : function() {
								var arr = mainTable.getSelectedRows();
								$("#LOGIN_ID").attr("readonly", true);
								user.getInfo('/api/user/info?USER_ID='
										+ arr[0].USER_ID);
							},
							enableSorting : true,
							enableColResize : true,
							rowSelection : 'single'
						}
						mainTable = newGrid("mainTable", gridOptions);
						search();
					});

					//조회
					function search() {
						user.search('/api/user/list', function(data) {
							mainTable.setData(data);
						});
					}

					//저장
					function saveUser() {
						if(validate("input_area"))
						{
							user.save('/api/user/save', function(data) {
								user.getData().USER_ID=data.USER_ID
								search();
							});
						}
					}

					//삭제
					function deleteUser() {
						user.del('/api/user/delete', function() {
							search();
						});
					}

					//추가
					function newUser(){
						$("#LOGIN_ID").attr("readonly", false);
						user.setData({USE_YN:'Y'});
						$("#LOGIN_ID").focus();
					}

					//비밀번호 초기화
					function passwordReset() {
						post('/api/user/save_reset_password', user.getData(),
							function(data) {
								alert("<spring:message code="passwordAuth.success" text="" />");

							})
					}
				</script>



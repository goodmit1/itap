<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<!--  사용자별 권한 관리  -->
<style>
#mainTable { height:calc(100vh - 180px); }
#mainTable2 { height:calc(100vh - 300px); clear:both }
</style>

<!-- 조회조건 및 버튼 -->
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->


		<div class="col col-sm-4">
			<fm-input id="S_PRGM_NM" name="PRGM_NM" title="<spring:message code="title_prgm"/>"></fm-input>

		</div>
		<div class="col col-sm-4">
			<fm-input id="S_USER_NAME" name="USER_NAME"  title="<spring:message code="FM_USER_USER_NAME" text="사용자" />"></fm-input>


		</div>
		<div class="col btn_group">
			<input type="button" class="btn searchBtn" value="<spring:message code="btn_search" text="" />" onclick="search()" />
			<fm-sbutton cmd="update" class="btn btn-primary" onclick="memoryReload()"><spring:message code="label_apply_mem" text="메모리 반영"/></fm-sbutton>
			<fm-sbutton cmd="delete" class="btn btn-primary" onclick="delPrgm()"><spring:message code="btn_delete" text=""/></fm-sbutton>
			<fm-sbutton cmd="update" class="btn btn-primary" onclick="savePrgm()"><spring:message code="btn_save" text="" /></fm-sbutton>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">


	<div class="table_title">
		<spring:message code="label_prgm_permission"/>

		<div class="btn_group">
			<fm-sbutton cmd="update" data-toggle="modal" data-target="#myModal" title="<spring:message code="btn_new" text="" />"  class="btn icon"><i class="fa fat add fa-plus-square"></i></fm-sbutton>

	 	</div>
	</div>

	<!--  목록 -->
	<div id="mainTable2"  class="ag-theme-fresh"></div>

	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><spring:message code="label_add_prgm_permission"/></h4>
				</div>
				<div class="modal-body">
					<div style="height: 100px">
						<div class="col col-sm-6">
							<fm-popup id="USER_NAME" name="USER_NAME" title="<spring:message code="FM_USER_USER_NAME" text="사용자" />"></fm-popup>
							<input type="hidden" id="USER_ID" name="USER_ID">

						</div>
						<div class="col col-sm-6">
							<fm-popup id="PRGM_NM" name="PRGM_NM" title="<spring:message code="title_prgm"/>"></fm-popup>
							<input type="hidden" id="PRGM_ID" name="PRGM_ID">
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<fm-sbutton cmd="update" onclick="newPrgm()"
						class="btn btn-default"><spring:message code="label_confirm" /></fm-sbutton>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
</div>
				<script>
					var grid2;
					var prgm = new Req();
					$(document).ready(function() {

						//SELECT_YN
						var
						columnDefs2 = [ {
							headerName : "",
							field : "PRGM_ID",
							hide : true
						}, {
							headerName : "",
							field : "USER_ID",
							hide : true
						}, {
							headerName : "<spring:message code="FM_USER_USER_NAME" text="사용자" />",
							field : "USER_NAME"
						}, {
							headerName : "<spring:message code="title_prgm"/>",
							field : "PRGM_NM"
						}, {
							headerName : "<spring:message code="btn_search" text="" />",
							field : "SELECT_YN",
							maxWidth : 70,
							cellRenderer : YNCheckboxRenderer
						}, {
							headerName : "<spring:message code="btn_save" text="" />",
							field : "UPDATE_YN",
							maxWidth : 70,
							cellRenderer : YNCheckboxRenderer
						}, {
							headerName : "<spring:message code="btn_delete" text=""/>",
							field : "DELETE_YN",
							maxWidth : 70,
							cellRenderer : YNCheckboxRenderer
						},
						//{headerName: "추가", field: "INSERT_YN", maxWidth: 70,cellRenderer:YNCheckboxRenderer},
						//{headerName: "인쇄", field: "PRINT_YN", maxWidth: 70,cellRenderer:YNCheckboxRenderer}
						];
						var gridOptions2 = {
							columnDefs : columnDefs2,
							rowData : [],
							enableSorting : true,
							enableColResize : true,
							getRowNodeId : function(data) {
								return "" + data.PRGM_ID + "." + data.USER_ID;
							},
							editable : true,

						};
						grid2 = newGrid("mainTable2", gridOptions2)

						search();
					})

					// 추가
					function newPrgm() {
						if (prgm.getData().PRGM_ID == null) {
							alert(msg_select_first);
							return;
						}
						if (prgm.getData().USER_ID == null) {
							alert(msg_select_first);
							return;
						}
						grid2.insertRow({
							PRGM_ID : prgm.getData().PRGM_ID,
							PRGM_NM : prgm.getData().PRGM_NM,
							USER_ID : prgm.getData().USER_ID,
							USER_NAME : prgm.getData().USER_NAME,
							SELECT_YN : 'Y',
							INSERT_YN : 'Y',
							DELETE_YN : 'Y',
							UPDATE_YN : 'Y',
							PRINT_YN : 'Y'
						});

					}

					//메모리 반영
					function memoryReload() {
						post('/api/user_prgm/save_memory', {}, function() {
							alert("<spring:message code="msg_applied"/>");
						})
					}

					// 조회
					function search() {
						prgm.search('/api/user_prgm/list', function(data) {
							grid2.setData(data);

						});

					}

					//삭제
					function delPrgm() {

						prgm.delGrid(grid2, '/api/user_prgm/delete_multi',
								function(data) {
									search();
								})

					}

					//저장
					function savePrgm() {
						prgm.saveGrid(grid2, "/api/user_prgm/save_multi",
								function() {
									search();
								})
					}
				</script>


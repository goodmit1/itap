<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/index">
    <layout:put block="content">
    	<form id="mainForm" >
    	<fmtags:include page="list.jsp" />
    	</form>
    </layout:put>
  
</layout:extends>	  
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 코드 관리 -->
<style>
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col " >
			<fm-input id="S_DD_ID" name="DD_ID" title="<spring:message code="FM_DDIC_DD_ID" text="" />"></fm-input>
		</div>
		<div class="col col-sm" >
			<fm-input id="S_DD_NM" name="DD_NM" title="<spring:message code="FM_DDIC_DD_NM" text="" />"></fm-input>
		</div>
		<div class="col  col-sm" >
			<fm-input id="S_DD_VALUE" name="DD_VALUE" title="<spring:message code="FM_DDIC_DD_VALUE" text="" />"></fm-input>
		</div>
		<div class="col  col-sm" >
			<fm-input id="S_KO_DD_DESC" name="KO_DD_DESC" title="<spring:message code="FM_DDIC_KO_DD_DESC" text="코드명" />"></fm-input>
		</div>
		<div class="col btn_group nomargin">
			<button type="button" id="searchBtn" onclick="searchDDIC()" class="searchBtn btn"><spring:message code="btn_search" text="" /></button>
		</div>
		<div class="col btn_group_under">
			<fm-popup-button popupid="multilanguage_popup"
				popup="/admin/popup/multilanguage_form_popup.jsp" cmd="update" class="exeBtn" param="form_data">
					<spring:message code="FM_DDIC_FM_NL_POP" text="다국어"/>
			</fm-popup-button>
			<fm-sbutton cmd="update" class="btn btn-primary newBtn" onclick="newDDIC()"><spring:message code="btn_new" text="신규"/></fm-sbutton>
			<fm-sbutton cmd="delete" class="btn btn-primary delBtn" onclick="delDDIC()"><spring:message code="btn_delete" text="삭제"/></fm-sbutton>
			<fm-sbutton cmd="update" class="btn btn-primary saveBtn" onclick="saveDDIC()"><spring:message code="btn_save" text="저장"/></fm-sbutton>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
		<div class="search_info">
			<spring:message code="mainTable_Search_information" text="검색정보" />
			<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportDDIC()" class="layout excelBtn"></button>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height:450px"></div>
	</div>
	<jsp:include page="detail.jsp"></jsp:include>
	</div>
</div>
<script>

var ddic = new Req();
function exportDDIC(){
	mainTable.exportCSV({fileName:'ddic.csv'})
}
function saveDDIC(){
	ddic.save("/api/fm_ddic/save", function(){
		searchDDIC();
	})
}
function delDDIC(){
	ddic.del("/api/fm_ddic/delete", function(){
		searchDDIC();
	})
}
function newDDIC(){
	document.getElementById("DD_ID").disabled = false;
	document.getElementById("DD_VALUE").disabled = false;
	 
	form_data.IDU = "";
	form_data.DD_ID = "";
	form_data.DD_NM = "";
	form_data.DD_VALUE = "";
	form_data.KO_DD_DESC = "";
	form_data.SORT = "";
	form_data.USE_YN = "Y";
	searchDDIC();
}
function searchDDIC(){
	ddic.search('/api/fm_ddic/list', function(data){
			mainTable.setData(data);
	});
}

var mainTable;

$(document).ready(function(){
	var columnDefs =[{
			headerName: "<spring:message code="FM_DDIC_DD_ID" text="그룹 ID" />",
			field: "DD_ID",
			maxWidth: 200
		}, {
			headerName: "<spring:message code="FM_DDIC_DD_NM" text="그룹명" />",
			field: "DD_NM",
			maxWidth: 250,
		}, {
			headerName: "<spring:message code="FM_DDIC_DD_VALUE" text="코드값" />",
			field: "DD_VALUE",
			maxWidth: 100,
		}, {
			headerName: "<spring:message code="FM_DDIC_KO_DD_DESC" text="코드명" />",
			field: "KO_DD_DESC",
		}, {
			headerName: "<spring:message code="FM_DDIC_SORT" text="정렬순서" />",
			field: "SORT",
			maxWidth: 100,
		}, {
			headerName: "<spring:message code="USE_YN" text=""/>",
			field: "USE_YN",
			maxWidth: 100,
		}
	     ];
	var gridOptions = {
		    columnDefs: columnDefs,
		    rowData: [],
			rowSelection : 'single',
		    enableSorting: true,
		    enableColResize: true,
		    onSelectionChanged : function() {
				var arr = mainTable.getSelectedRows();
				ddic.getInfo('/api/fm_ddic/info?DD_ID='+ arr[0].DD_ID + '&DD_VALUE=' + arr[0].DD_VALUE, function(data){});
			}
		};
	mainTable = newGrid("mainTable", gridOptions)

	searchDDIC();
});
</script>
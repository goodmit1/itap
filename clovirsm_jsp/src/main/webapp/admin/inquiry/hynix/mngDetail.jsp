<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- 입력 Form -->
<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body" id="input_area">
		<div class="col col-sm-3">
			<fm-input id="INQ_TITLE" name="INQ_TITLE" title="<spring:message code="NC_INQ_INQ_TITLE" text="문의제목"/>"></fm-input>
		</div>
		<div class="col col-sm-3">
			<fm-select url="/api/code_list?grp=INQ" id="INQ_CD"
				name="INQ_CD" title="<spring:message code="NC_INQ_INQ" text="문의유형코드"/>"></fm-select>
		</div>
		<div class="col col-sm-3">
			<fm-select url="/api/code_list?grp=INQ_SVC" id="SVC_CD"
				name="SVC_CD" title="<spring:message code="NC_INQ_SVC" text="서비스"/>"></fm-select>
		</div>
		<div class="col col-sm-3">
			<fm-output id="INS_NAME" name="INS_NAME" title="<spring:message code="NC_INQ_INS_NAME" text="문의자"/>"></fm-input>
		</div>
		<div class="col col-sm-12" style="height: 349px;">
			<fm-textarea id="INQUIRY" name="INQUIRY"   title="<spring:message code="NC_INQ_INQUIRY" text="문의" />"></fm-textarea>
		</div>
		<div class="col col-sm-12" style="height: 349px;">
			<fm-textarea id="ANSWER" name="ANSWER" title="<spring:message code="NC_INQ_ANSWER" text="답변" />"></fm-textarea>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$("#INQUIRY").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false});
		$("#ANSWER").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false});
	})
</script>
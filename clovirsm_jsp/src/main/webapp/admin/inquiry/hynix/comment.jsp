<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>


<div class="comment_div">
	<div class="comment_title">COMMENT</div>
	<div class="comment_div_input">
		<textarea id="comment_input" class="comment_input" placeholder="<spring:message code="comment_info" text="" />"></textarea>
		<input type="button" value="Comment" onclick="addComment()">
	</div>
	<div class="comment_div_list" id="comment_div_list">
	</div>
</div>
<script>
var listLast = new Object();
var test ;
function addComment(){
	 
	if($("#comment_input").val() != ''){
		var param = new Object();
		param.INQ_ID = ${popupid}_param.INQ_ID;
		param.INQ_DEPTH = 1;
		param.INQ_COMMENT = $("#comment_input").val();
		post('/api/inquiryCmt/save', param, function(data){
			$("#comment_input").val('');
			$("#comment_div_list").html("");
			commentList();
			alert(msg_complete);
			});
	}
}
function addReComment(id,seq){
	if($("#comment_input_"+seq).val() != ''){
		var param = new Object();
		param.INQ_ID = ${popupid}_param.INQ_ID;
		param.INQ_CMT_PARENTS = id;
		param.INQ_DEPTH = 2;
		param.INQ_SEQ = Number(seq);
		param.INQ_COMMENT = $("#comment_input_"+seq).val();
		post('/api/inquiryCmt/save', param, function(data){
			$("#comment_input").val('');
			$("#comment_div_list").html("");
			commentList();
			alert(msg_complete);
			});
	}
}
function reComment(that, id, seq){
	test = that;
	 
	 
	if(!$(test).parent().parent().next().hasClass("comment_div_input")){
		html = '<div class="comment_div_input reComent">';
		html += '<textarea id="comment_input_'+seq+'" class="comment_input" placeholder="<spring:message code="comment_info" text="" />"></textarea>';
		html += '<input type="button" value="Comment" width: 15.3%; onclick="addReComment(\''+id+'\',\''+seq+'\')">';
		html += '</div>';
		
		$(that).parent().parent().after(html);
		$("#comment_input_"+seq).focus();
	} else{
		$("#comment_input_"+seq).focus();
	}
}
function commentDraw(data){
	if(data.list.length > 0 ){
		listLast = data.list[data.list.length-1];
		for(var i = 0 ; i < data.list.length; i++){
			if(!data.list[i].hasOwnProperty("INQ_CMT_PARENTS")){
				var html = "<div class='comment_div_list_item' id='comment_div_list_item_"+data.list[i].INQ_SEQ+"'>";
				html += '<div class="comment_div_list_item_mid">';
				html += '<div class="comment_div_list_item_info"><span style="float:left;">'+data.list[i].USER_NAME+'</span><span style="float: left; padding-left: 15px; color: #7b7b7b;">'+formatDate(data.list[i].INS_TMS,"datetime")+'</span>';
				html += '<input type="button" class="reComment" value="답글" onclick="reComment(this,\''+data.list[i].INQ_CMT_ID+'\',\''+data.list[i].INQ_SEQ+'\')"/>';
				html += '</div>';
				html += '<div class="comment_div_list_item_cmt">'+data.list[i].INQ_COMMENT+'</div>';
				html += '</div>';
				html += '</div>';
				
			}
			else{
				var html = "<div class='comment_div_list_item reComent' id='comment_div_list_item_"+data.list[i].INQ_SEQ+"'>";
				html += '<div class="comment_div_list_item_mid">';
				html += '<div class="comment_div_list_item_info"><span style="float:left;">'+data.list[i].USER_NAME+'</span><span style="float: left; padding-left: 15px; color: #7b7b7b;">'+formatDate(data.list[i].INS_TMS,"datetime")+'</span></div>';
				html += '<div class="comment_div_list_item_cmt">'+data.list[i].INQ_COMMENT+'</div>';
				html += '</div>';
				html += '</div>';
			}
			$("#comment_div_list").append(html);
		}
	}
}

function commentList(){
	var param = new Object();
	param.INQ_ID = ${popupid}_param.INQ_ID;
	post('/api/inquiryCmt/list', param, function(data){
		 
		$(".comment_title").html("COMMENT ("+data.list.length+")");
		commentDraw(data);
	});
	
	
}
</script>
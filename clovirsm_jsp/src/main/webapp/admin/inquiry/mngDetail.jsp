<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- 입력 Form -->
		<div class="col col-sm-6">
			<fm-output id="UPD_NAME" name="UPD_NAME" title="<spring:message code="NC_INQ_UPD_NAME" text="답변자"/>"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="UPD_TMS" :value="formatDate(form_data.UPD_TMS,'datetime')" name="UPD_TMS" title="<spring:message code="NC_INQ_UPD_TMS" text="답변일시"/>"></fm-output>
		</div>
		<div class="col col-sm-12" style="height: 349px;">
			<fm-textarea id="ANSWER" name="ANSWER" title="<spring:message code="NC_INQ_ANSWER" text="답변" />"></fm-textarea>
		</div>
<script>
	$(document).ready(function(){
		$("#ANSWER").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false});
	})
</script>
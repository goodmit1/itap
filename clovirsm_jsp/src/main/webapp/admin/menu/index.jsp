<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/index">
    <layout:put block="content">
    	<form id="mainForm"  >
    	<jsp:include page="list.jsp"></jsp:include>
    	</form>
    </layout:put>
    <layout:put block="popup_area">	
    <jsp:include page="/admin/popup/menu.jsp"></jsp:include>
    <script>
    	var popup_team = newPopup("PARENT_ID_NM", "menu_popup");

    	popup_team.setMapping({"MENU_NM":"PARENT_ID_NM", "ID":"PARENT_ID"});
    	
    	
    	
    	</script>
	</layout:put>  
</layout:extends>	  
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!--  게시물 관리 -->
<link href="/res/css/editor.css" type="text/css" rel="stylesheet"/>
<script src="/res/js/editor.js"></script>
<style>
#START_DATE_HH, #END_DATE_HH {
	margin-bottom: 0px;
}
.calendar-btn {
    padding: 0px 6px;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm-4">
			<fm-input id="S_TITLE" name="TITLE" title="<spring:message code="label_title"/>"></fm-input>
		</div>
		<div class="col btn_group nomargin">
			<button type="button" class="btn btn-primary searchBtn" onclick="search()" value="<spring:message code="btn_search" text="" />" />
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<div class="btn_group">
		<c:if test="${sessionScope.ADMIN_YN == 'Y'}">
			<fm-sbutton cmd="update" class="btn btn-primary contentsBtn tabBtnImg new" onclick="newNoti()" ><spring:message code="btn_new" text="" /></fm-sbutton>
			<fm-sbutton cmd="delete" class="btn btn-primary contentsBtn tabBtnImg del" onclick="deleteNoti()" ><spring:message code="btn_delete" text=""/></fm-sbutton>
			<fm-sbutton cmd="update" class="btn btn-primary contentsBtn tabBtnImg save" onclick="saveNoti()" ><spring:message code="btn_save" text="" /></fm-sbutton>
		</c:if>
		</div>
	</div>
	<div id="mainTable" style="height: 300px" class="ag-theme-fresh"></div>

	<div class="form-panel detail-panel panel panel-default" style="margin-top: 20px; border: 0px; min-width: 1600px;">
		<div id="inputList" style="display:none;">
	      	<jsp:include page="inputList.jsp"></jsp:include>
        </div>
        <div id="outputList" style="display:none;">
	       	<jsp:include page="outputList.jsp"></jsp:include>
        </div>
	</div>
</div>
<script>
					var Noti = new Req();
					mainTable;
					$(function() {
						
						if(accessInfo.update){
								$("#inputList").css("display","block");				
						} else{
								$("#outputList").css("display","block");
						}
						
						var
						columnDefs = [ {
							headerName : "ID",
							field : "ID",
							width : 50,
						}, {
							headerName : "<spring:message code="label_title"/>",
							field : "TITLE"
						},
// 						{
// 							headerName : "<spring:message code="label_top_position"/>",
// 							field : "TOP_YN",
// 							width : 60,
// 						},
// 						{
// 							headerName : "<spring:message code="NC_DISK_TYPE_DEL_YN" text="삭제 여부"/>",
// 							field : "DEL_YN",
// 							width : 60,
// 						},
						{
							headerName : "<spring:message code="label_post_term"/>",
							field : "START_DATE",
							width : 160,
							valueGetter:function(params)
							{
								var date = '';
								 
								if(params.data.START_DATE) date += formatDatePattern( new Date(params.data.START_DATE),'yyyy-MM-dd HH<spring:message code="hour"/>')
								if(params.data.END_DATE) date += "~" + formatDatePattern( new Date(params.data.END_DATE),'yyyy-MM-dd HH<spring:message code="hour"/>')
								return date;
							},
						}, {
							headerName : "<spring:message code="label_read_cnt"/>",
							field : "HITS",
							cellClass : "text-right",
							width : 60,
						}, {
							headerName : "<spring:message code="INS_TMS"/>",
							field : "INS_TMS",
							valueFormatter:function(params)
							{
								return formatDate(params.value,'datetime')
							},
							width : 80,
						} ];
						var
						gridOptions = {
							columnDefs : columnDefs,
							rowData : [],
							getRowNodeId : function(data) {
			    			    	return "" + data.ID;
			    			},
			    			onRowClicked : function() {
								var arr = mainTable.getSelectedRows();
								$("#LOGIN_ID").attr("readonly", true);

								Noti.getInfo('/api/notice/info?ID='
										+ arr[0].ID, function(data){
										noticefileAttach_list( arr[0].ID);
										if(data.START_DATE)
										{
											var date = formatDate(data.START_DATE, 'datetime');
											Noti.putData({START_DATE_DT: date.substring(0,10), START_DATE_HH :date.substring(11,13) });
										}
										if(data.END_DATE)
										{
											var date = formatDate(data.END_DATE, 'datetime');
											Noti.putData({END_DATE_DT: date.substring(0,10), END_DATE_HH :date.substring(11,13) });
										}
									$("#CONTENTS").Editor("setText", data.CONTENTS ? data.CONTENTS:"");
									Noti.search('/api/noticeTrg/list?ID=' + arr[0].ID, function(data){
										var selectedValues = [];
										for(var i in data.list){
											selectedValues.push(data.list[i].TEAM_CD);
										}
										 
										$("#TRG_TEAM_CD").val(selectedValues);

									})
								});
							},
							enableSorting : true,
							enableColResize : true,
							rowSelection : 'single'
						}

						mainTable = newGrid("mainTable", gridOptions);
						search();
					});

					//조회
					function search() {
						search_data.CATEGORY ="NOTICE";
						Noti.search('/api/notice/list', function(data) {
							mainTable.setData(data);
							if(Noti.getData().ID)
							{
								mainTable.setSelectedById(Noti.getData().ID, true);
							}


						});
					}
					
					function validateFile(param){
						var array = [];
						var reg = new RegExp('(.*?)\.(jsp|sh|exe|bat|cmd|php|ps1|pv|js|asp|pl)$');
						var result;
						var files = $('#files > input').val();
						if(files.length > 0){
							//true
							result = reg.test(files);
						}
						return result
					}

					//저장
					function saveNoti() {
						if(validate("input_area")) {
							form_data.CONTENTS = $("#CONTENTS").Editor("getText" )
							if(form_data.START_DATE_DT) {
								form_data.START_DATE = form_data.START_DATE_DT  +  " " + (form_data.START_DATE_HH ? form_data.START_DATE_HH:'00') + ':00:00';
							} else {
								delete form_data.START_DATE ;
							}
							if(form_data.END_DATE_DT) {
								form_data.END_DATE = form_data.END_DATE_DT  + " " + (form_data.END_DATE_HH?form_data.END_DATE_HH:'23') + ':00:00';
							} else {
								delete form_data.END_DATE ;
							}
							//파일 저장시 지정된 파일규격인지 확인
							if(validateFile(form_data)){
								alert('<spring:message code="File extends" text="허용되지 않는 확장자 입니다(jsp|sh|exe|bat|cmd|php|ps1|pv|js|asp|pl)" />')
								return
							} 
							Noti.saveFiles('/api/notice/save_fileAttach',"files", function(data) {
								if(data.ID) Noti.getData().ID=data.ID;
								post("/api/noticeTrg/save_multi", {ID: form_data.ID, TEAM_CD:$("#TRG_TEAM_CD").val()}, function(){
									alert(msg_complete);
									search();
								})
							});
							 
						}
					}

					//삭제
					function deleteNoti() {
						if(confirm(msg_confirm_delete))
						{
							post('/api/notice/delete', {ID:form_data.ID}, function(){
								alert(msg_complete);
								search();
							})
						}

					}

					// 추가
					function newNoti(){
						$("#CONTENTS").Editor("setText","");
						Noti.setData({DEL_YN:'N',TOP_YN:'N'});
						$("#TITLE").focus();
						$("#TRG_TEAM_CD").val("");
					}

				</script>



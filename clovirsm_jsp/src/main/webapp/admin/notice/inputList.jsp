<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>

			<div class="panel-body">

					<input type="hidden" id="ID" name="ID">


					<div class="col col-sm-6">
						<fm-input id="TITLE" name="TITLE" required="true" title="<spring:message code="label_title"/>" maxlength="100"></fm-input>
					</div>

 					<div class="col col-sm-3">
 						<fm-select url="/api/code_list?grp=sys.yn" id="TOP_YN" 
 							name="TOP_YN" title="<spring:message code="" text="DASHBOARD 표시" />" ></fm-select>
					</div> 
					<div class="col col-sm-6"  style="height:39px">
						<table>
						<tr style="vertical-align: bottom;">
							<td>
								<fm-date id="START_DATE_DT" class="inputWidth" name="START_DATE_DT" div_style="width: 125px;" input_style="width: 70%;"
										 title="<spring:message code="label_post_term"/>"></fm-date>
							</td>
							<td>
								<fm-select :options="hh" id="START_DATE_HH" name="START_DATE_HH" ></fm-select>
							</td>
							<td class="tilt">~<td>
							<td >
								<fm-date id="END_DATE_DT" name="END_DATE_DT"  div_style="width: 125px;" input_style="width: 70%;"></fm-date>
							</td>
							<td>
								<fm-select :options="hh" id="END_DATE_HH" name="END_DATE_HH" ></fm-select>
							</td>
						</tr>
						</table>
					</div>
					<div class="col col-sm-6" style="height:39px">
						<fm-select2 id="TRG_TEAM_CD" name="TRG_TEAM_CD" url="/api/etc/team_list" select_style="min-width: 520px;" multiple="true" title="<spring:message code="" text="부서" />"></fm-select2>
					</div>
					<div class="col col-sm-6">
						<fm-file name="files" id="files" multiple="true" title="<spring:message code="NC_REQ_ATT_FILE_PATH"/>" ></fm-file>
    				</div>
					<div class="col col-sm-6">
						<jsp:include page="/home/common/_fileAttach.jsp">
							<jsp:param value="notice" name="kubun"/>
							<jsp:param value="Y" name="editable"/>
						</jsp:include>
					</div>


					<div class="col col-sm-12" style="height: 349px;">
						<fm-textarea id="CONTENTS" name="CONTENTS" title=""></fm-textarea>
					</div>

			</div>
			<script>
			
			
			$(document).ready(function(){
				setTimeout(function() {
					$("#CONTENTS").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false,'source':false});
				}, 200);
				if(!accessInfo.update)
				{
					$("#menuBarDiv_CONTENTS").hide();
				}
				$("#statusbar_CONTENTS").hide();
			})
			</script>
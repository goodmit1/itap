<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8" %>
<script type="text/x-template" id="modal-template">
  <transition name="modal">
    <div :id="id" class="modal  fade popup_dialog" role="dialog" :data-backdrop="modalbackdrop">
      <div class="modal-dialog"  :class="dialogClass">

    <!-- Modal content-->
		<div class="modal-content  container-fluid">
      		<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal"></button>
				<slot name="header"><span :class="cmd" :id="id + '_title'">{{ title }}</span></slot>
          	</div>

          	<div class="modal-body" :class="">
            	<slot >
                </slot>
			</div>
          	<div class="modal-footer">
            	<slot name="footer">
             	</slot>
				<!--<button type="button" class="btn" data-dismiss="modal">닫기</button>-->
          	</div>
        </div>
      </div>
    </div>
  </transition>
</script>
<script type="text/x-template" id="date-fromto-template">
<transition name="date-fromto">
  <span :id="id">
	<div class="col" v-if="kubunYn">
		<fm-select :name="kubun_id"  :id="kubun_id" :title="title" :options="{'D':'<spring:message code="label_kubun_day"/>','M':'<spring:message code="label_kubun_month"/>','H':'<spring:message code="label_kubun_hour"/>'}" ></fm-select>
	</div>
	<div class="col" v-if="!kubunYn">
		<label class="control-label grid-title value-title">{{title}}</label>
	</div>
	<span v-show="kubun=='D'">
		<div class="col">
			<fm-date :id="start_dt_id" :name="start_dt_id"></fm-date>
		</div>
		<div class="col">
					<span  class="tilt">~</span>
		</div>

		<div class="col">
			<fm-date :id="finish_dt_id" :name="finish_dt_id"></fm-date>
		</div>
	</span>
	<span v-show="kubun == 'M'">
		<div class="col yymm">

				<fm-select :options="year" :id="start_yy_id" :name="start_yy_id" ></fm-select>
				<fm-select :options="month" :id="start_mm_id" :name="start_mm_id" ></fm-select>

		</div>
		<div class="col">
					<span  class="tilt">~</span>
		</div>
		<div class="col yymm">

			<fm-select :options="year" :id="finish_yy_id" :name="finish_yy_id" ></fm-select>
			<fm-select :options="month" :id="finish_mm_id" :name="finish_mm_id" ></fm-select>

		</div>
	</span>
	<span v-show="kubun == 'H'">
		<div class="col">
			<fm-date :id="dt_id" :name="dt_id" ></fm-date>
		</div>
		<div class="col ">

				<fm-select :options="hh" :id="start_hh_id" :name="start_hh_id" ></fm-select>
		</div>
		<div class="col">
					<span  class="tilt">~</span>
		</div>
		<div class="col">
				<fm-select :options="hh" :id="finish_hh_id" :name="finish_hh_id" ></fm-select>

		</div>
	</span>
  </span>
</transition>
</script>
<script type="text/x-template" id="auto-reload-template">
<transition name="auto-reload">
	<div style="display: inline-block;height: 22px;">
		<input type="hidden" id="fncName">
		<input id="interval" style="width:20px;font-size:13px;height:100%">
		<input type="button" onclick="interval('play', this);return false;" style="margin: 0 0 0 5px;height:100%">
		<input type="button" onclick="interval('stop', this);return false;" style="margin: 0 0 0 5px;display: none;height:100%">
	</div>
</transition>
</script>

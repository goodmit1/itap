var colors = ['#fd5d55','#f7bc60', '#75c548', '#ff9c75',  '#b3bf2e', '#fc7d86', '#48bdb6', '#53b1d0', '#6393ca','#5d6ea3'];

function get_pivot_config(setting){
	var columnDefs = [
		{headerName : setting.ytitle, field: "name"}
	];
	for(var i=0; i < setting.categories.length; i++){
		 
		columnDefs.push({headerName :setting.categories[i], field:setting.categories[i]})
	}
	return get_grid_config(columnDefs, setting.data, setting.title)
	 
	
}

function get_grid_config(columnDefs, data, title){
		var setting = {};
		setting.rowData  = data;
		setting.columnDefs = columnDefs
	 
		setting.enableSorting =true
		setting.enableColResize = true
		setting.title = title;
		setting.pinnedBottomRowData = getTableSum(columnDefs, data);
		return setting;
	}
	function getTableSum(columnDefs, data){
		var result = [];
		var obj = {};
		for(var i=0; i < columnDefs.length; i++ ){
			var val = '';
			if(i==0){
				val = 'Total';
			}
			else{
				var total = 0;
				for(var j=0; j < data.length; j++){
					var column = 0; 
					console.log(typeof(data[j][columnDefs[i].field]));
					console.log(data[j][columnDefs[i].field]);
					if(typeof(data[j][columnDefs[i].field]) == 'string'){
						total = '';
					} else{
						if(data[j][columnDefs[i].field])
							column += data[j][columnDefs[i].field];
						total += 1*column;
					}
				}
				val = total;
			}
			obj[columnDefs[i].field] = val;
			
		}
		result.push(obj);
		return result;
	}
function get_column_config( setting  ){
		 		return {
		 		    chart: {
		 		        type: 'column'
		 		    },
		 		    title: {
		 		        text: setting.title,
		 				align: 'left'
		 		    },
		 		    xAxis: {
		 		    	type: 'category',
		 				categories: setting.categories,
		 				 
		 		    },
		 		    yAxis: {
		 		        min: 0,
		 		        title: {
		 		            text: setting.ytitle
		 		        }
		 		    },
		 		   
		 		    tooltip: {
		 		        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		 		        pointFormatter: function(){
		 		        	return '<tr><td style="color:' + this.color +';padding:0">' + this.point.name + ': </td>' +		 		        
		 		            '<td style="padding:0"><b>' + + getYLabel(setting, this.y) + '</b></td></tr>'
		 		         },
		 		        footerFormat: '</table>',
		 		        shared: true,
		 		        useHTML: true
		 		    },
		 		    plotOptions: {
		 		        column: {
		 		            pointPadding: 0.2,
		 		            borderWidth: 0,
		 		            stacking: setting.stacking?'normal':'',
		 		            dataLabels: {
		 		                enabled: true,
		 		                formatter: function(){ 
			                		var pcnt = (this.y / this.series.data.map(p => p.y).reduce((a, b) => a + b, 0)) * 100;
			                		return getYLabel(setting ,this.y, pcnt)   ;
			                	} 
		 		            },
		 		            events:{
				            	click: function(e){
				            		if(setting.objName){
				            			eval(setting.objName +".setFilter('" + setting.name + "',e.point.name,'" + setting.xtitle + "','" + setting.objName + "')")
				            		}
				            	}
				            }
		 		        }
		 		    },
		 		   colors: colors,
		 		    series: setting.data
		 		} 
    }
function getValWithUnit(setting , y ){
	if(setting.unit == '천원'){
		y= (y/1000).toFixed(0);
	}
	return formatNumber(y) + (setting.unit? setting.unit:'EA') 
}
function getYLabel(setting , y, pcnt){
	 
	var label = getValWithUnit(setting, y)  +  (setting.percent && pcnt ? ' ('+  pcnt.toFixed(0)  + '%)':' ') 
	return label;
}   
function get_bar_config( setting  ){
	 
	return {
		
		
		chart: {
			type: 'bar'
		},
		title: {
			text: setting.title,
			align: 'left'
		},
		xAxis: {
			type: 'category',
			categories: setting.categories,
			crosshair: true
		},
		yAxis: {
			min: 0,
			title: {
				text: setting.ytitle
			}
		},
		
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormatter: function(){
				 
				
				return '<tr><td style="padding:0"><b>'+getYLabel(setting, this.y) + '</b></td></tr>'
			},
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		legend: {
	        enabled: false
	    },
		plotOptions: {
			 bar: {
				 	stacking: setting.stacking?'normal':'',
		            dataLabels: {
			                enabled: true,
			                formatter: function(){ 
			                		var pcnt = (this.y / this.series.data.map(p => p.y).reduce((a, b) => a + b, 0)) * 100;
			                		return   getYLabel(setting ,this.y, pcnt)    ;
			                	} 
			            },
					 
					events:{
			            	click: function(e){
			            		if(setting.objName){
			            			eval(setting.objName +".setFilter('" + setting.name + "',e.point.name,'" + setting.xtitle + "','" + setting.objName + "')")
			            		}
			            	}
			            }
		        }
	    },
	    colors: colors,
		series: setting.data
	} 
}

function get_pie_config( setting  ){
	return {
		chart: {
			type: 'pie'
		},
		title: {
			text: setting.title,
			align: 'left'
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px; color:{series.color}" >{point.key}</span><table>',
			pointFormatter: function(){ 
				return '<tr><td style="padding:0"><b>' + getYLabel(setting, this.y, this.percentage) +'</b></td></tr>'
			},
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
	            cursor: 'pointer',
	            dataLabels: {
	                enabled: false
	            },
	            showInLegend: false,
	            dataLabels: {
	                enabled: true,
	                formatter: function(){
	                	 
	                	return this.point.name + " " + getYLabel(setting, this.y, this.percentage)
	                 
	                }
	            },
	            events:{
	            	click: function(e){
	            		if(setting.objName){
	            			eval(setting.objName +".setFilter('" + setting.name + "',e.point.name,'" + setting.xtitle + "','" + setting.objName + "')")
	            		}
	            	}
	            }
	        }
		},
		colors: colors,
		series :  setting.data 
	} 
}
function get_line_config( setting  ){
	return   {
		    chart: {
 		        type: 'line'
 		    },
 		    title: {
 		        text: setting.title,
 				align: 'left'
 		    },
 		    xAxis: {
 		    	type: 'category',
 				categories: setting.categories,
 				crosshair: true
 		    },
 		    yAxis: {
 		        min: 0,
 		        title: {
 		            text: setting.ytitle
 		        }
 		    },
 		   
 		    tooltip: {
 		        shared: true,
 		        useHTML: true
 		    },
 		    plotOptions: {
 		        column: {
 		            pointPadding: 0.2,
 		            borderWidth: 0,
 		            stacking: setting.stacking?'normal':'',
 		            dataLabels: {
 		                enabled: true
 		            },
 		            events:{
		            	click: function(e){
		            		if(setting.objName){
		            			eval(setting.objName +".setFilter('" + setting.name + "',e.point.name,'" + setting.xtitle + "','" + setting.objName + "')")
		            		}
		            	}
		            }
 		        }
 		    },
 		   colors: colors,
 		    series: setting.data
 		} 
}
function get_solidgauge( setting ){
	return {
		 chart: {
	            type: 'solidgauge'
	        },

	        title: null,

	        pane: {
	            center: ['50%', '50%'],
	            size: '100%',
	            startAngle: 0,
	            endAngle: 360,
	            background: {
	                backgroundColor: 'white',
	                innerRadius: '0%',
	                outerRadius: '80%',
	                shape: 'circle'
	            }
	        },

	        tooltip: {
	            enabled: false
	        },
	        yAxis: {
	            min: 0,
	            max: 100,
	            title: {
	                text: 'Speed'
	            },
	 
	            stops: [
	                [0, '#55BF3B'], // green
	                [0.7, '#ffd966'], // yellow
	                [0.8, '#ff7f61'] // red
	                ], 
	            lineWidth: 0,
	            minorTickInterval: null,
	            tickPixelInterval: 160,
	            tickWidth: 0,
	            title: {
	                y: -160
	            },
	            labels: {
	                enabled: false
	            }
	        },

	        credits: {
	            enabled: false
	        },

	        plotOptions: {
	            solidgauge: {
	                dataLabels: {
	                    enabled: true,
	                    y: -35,
	                    borderWidth: 0,
	                    useHTML: true,
	                    format: '<div style="text-align:center"><span style="font-size:35px;color:black;">{y}%</span><br/>' +
	                        '<span style="font-size: 13px; color: #505b82; font-weight: 900;">'+setting.title+'</span></div>'
	                }
	            }
	        },
	        series: [{
	            innerRadius: '80%',
	            //name: 'Speed',
	            data: setting.data,
	            color: 'green'
	        }]
		
	}
}
function get_area_config( setting  ){
	return {
		chart: {
	        type: 'area'
	    },
	    title: {
	        text: setting.title,
			align: 'left'
	    },
	    
	    xAxis: {
	    	type: 'category',
				categories: setting.categories,
				crosshair: true 
	    },
	    yAxis: {
	        title: {
	            text: setting.ytitle
	        }
	    },
	    plotOptions: {
	        area: {
	            marker: {
	                enabled: false,
	                symbol: 'circle',
	                radius: 2,
	                states: {
	                    hover: {
	                        enabled: true
	                    }
	                }
	            },
	            events:{
	            	click: function(e){
	            		if(setting.objName){
	            			eval(setting.objName +".setFilter('" + setting.name + "',e.point.name,'" + setting.xtitle + "','" + setting.objName + "')")
	            		}
	            	}
	            }
	        }
	    },
	    colors: colors,
		series: setting.data
	} 
}

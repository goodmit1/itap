function makeLineChart(id, dataMap, type, color, y_max) {

		try
		{
			setChartButton();
		}catch(e){}
	   if(!type) type = '';
	   if(!color) color = '';
	   if(!y_max) y_max = null;
	   var obj =  new Highcharts.Chart({

		    chart: {
		        renderTo: 'container_' + id,
		        type: type
	        },
	        title : {
	           text : ""
	        },
	        xAxis : {
	           type : 'datetime',
	           labels: {
	               useHTML:true,//Set to true
	               style:{
	            	   width:'80px',
	                   'min-width':'80px',
	                   whiteSpace:'normal'//set to normal

	               },
	               step: 1,
	               rotation: 45
	           },
	           dateTimeLabelFormats : {
					millisecond: '%Y-%m-%d<br/>%H:%M',
					second:      '%Y-%m-%d<br/>%H:%M',
					minute:      '%Y-%m-%d<br/>%H:%M',
					hour:        '%Y-%m-%d<br/>%H:%M',
					day:         '%Y-%m-%d',
					week:        '%Y-%m-%d',
					month:       '%Y-%m-%d',
					year:        '%Y-%m-%d'
	           }
	        },
	        legend : {
	           enabled : false

	        },
	        plotOptions:{ area:{ fillOpacity:0.1}},	
	        yAxis : {
	           title : {
	              text : ''
	           },
	           min : 0,
	           max : y_max,
	           plotLines : [ {
	              value : 0,
	              width : 1,
	              color : '#808080'
	           } ]
	        },
	        tooltip : {
	           valueSuffix : ''
	        },

	        series : [ {
	           name : id,
	           color:color,
	           data : (dataMap instanceof Array ? dataMap : (dataMap[id] == null ? []
	                 : dataMap[id].valList))
	        } ]
	     });


	   chartObj[id] = obj;
	}

function makeMultiLineChart(id, dataMap, type, unit,  y_max) {
	 
	 
	try
	{
		setChartButton();
	}catch(e){}
   if(!type) type = '';
  
   if(!y_max) y_max = null;
   var obj =  new Highcharts.Chart({

	    chart: {
	        renderTo: 'container_' + id.replace(/\|/gi, ''),
	        type: type
        },
        title : {
           text : ""
        },
        xAxis : {
           type : 'datetime',
           labels: {
               useHTML:true,//Set to true
               style:{
            	   width:'80px',
                   'min-width':'80px',
                   whiteSpace:'normal'//set to normal

               },
               step: 1,
               rotation: 45
           },
          dateTimeLabelFormats : {
				millisecond: '%Y-%m-%d<br/>%H:%M',
				second:      '%Y-%m-%d<br/>%H:%M',
				minute:      '%Y-%m-%d<br/>%H:%M',
				hour:        '%Y-%m-%d<br/>%H:%M',
				day:         '%Y-%m-%d',
				week:        '%Y-%m-%d',
				month:       '%Y-%m-%d',
				year:        '%Y-%m-%d'
           }
        },
        legend : {
           enabled : false

        },
        plotOptions:{ area:{ fillOpacity:0.5}},	
        yAxis : {
           title : {
              text : unit
           },
           min : 0,
           max : y_max,
           plotLines : [ {
              value : 0,
              width : 1,
              color : '#808080'
           } ]
        },
        tooltip : {
           valueSuffix : ''
        },

        series : dataMap.y
     });


   chartObj[id] = obj;
}
	function drawVMPerf(  args , monitor_arr, color_arr, y_max) {


	   var dataMap = makeMap(args.LIST);
	   for ( var key in monitor_arr) {
	       
	      makeLineChart(monitor_arr[key], dataMap, "area", color_arr[key], y_max[key]);
	   }
	}

	function excelExportAll(fileName)
	{
		if($("#excelAll").length==0)
		{
			$("#vmChartArea").append('<div id="excelAll" style="display:none"></div>');
		}
		var obj =  new Highcharts.Chart({chart:{renderTo:'excelAll'}});
		obj.options.exporting.filename = fileName
		var dataAll = [];
		var titleArr = [];
		for(var index in chartObj) {
		    var data = chartObj[index].getDataRows();
		    margeExcelData(dataAll, data, titleArr);
		}
		var a = '<html xmlns:o="urn:schemas-microsoft-com:office:office" ' +
	    'xmlns:x="urn:schemas-microsoft-com:office:excel" ' +
	    'xmlns="http://www.w3.org/TR/REC-html40">' +
	    '<head><!--[if gte mso 9]><xml><x:ExcelWorkbook>' +
	    '<x:ExcelWorksheets><x:ExcelWorksheet>' +
	    '<x:Name>Ark1</x:Name>' +
	    '<x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions>' +
	    '</x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook>' +
	    '</xml><![endif]-->' +
	    '<style>td{border:none;font-family: Calibri, sans-serif;} ' +
	    '.number{mso-number-format:"0.00";} ' +
	    '.text{ mso-number-format:"\@";}</style>' +
	    '<meta name=ProgId content=Excel.Sheet>' +
	    '<meta charset=UTF-8>' +
	    '</head><body>' +
		'<table><thead><tr>';
		for(var i=0; i < titleArr.length; i++)
		{
			a += '<th scope="col" class="text">' + titleArr[i] +'</th>';
		}
		a +='</tr></thead><tbody>' ;
		for(var i=0; i < dataAll.length; i++)
		{
			a += '<tr>';
			var data1 = dataAll[i];
			for( var j=0; j < data1.length; j++)
			{
				if(j==0)
				{
					a += '<th scope="row"  class="';
				}
				else
				{
					a += '<td class="';
				}
				if(isNaN(data1[j]))
				{
					a +='text';
				}
				else
				{
					a += 'number' ;
				}
				a += '">' + data1[j];
				if(j==0)
					a += '</td>';
				else
					a += '</th>';
			}
			a += '</tr>';
		}

		a += '</tbody></table><body></html>' ;

		Highcharts.downloadURL(getBlobFromContent(a, 'application/vnd.ms-excel') || "data:application/vnd.ms-excel;base64,"+Highcharts.win.btoa(unescape(encodeURIComponent(a))),fileName + ".xls")
	}
	 function getBlobFromContent(content, type) {
		 var win = Highcharts.win,nav = win.navigator, doc = win.document, domurl = win.URL || win.webkitURL || win, isEdgeBrowser = /Edge\/\d+/.test(nav.userAgent);
         var nav = win.navigator, webKit = (nav.userAgent.indexOf('WebKit') > -1 &&
             nav.userAgent.indexOf('Chrome') < 0), domurl = win.URL || win.webkitURL || win;
         try {
             // MS specific
             if (nav.msSaveOrOpenBlob && win.MSBlobBuilder) {
                 var blob = new win.MSBlobBuilder();
                 blob.append(content);
                 return blob.getBlob('image/svg+xml');
             }
             // Safari requires data URI since it doesn't allow navigation to blob
             // URLs.
             if (!webKit) {
                 return domurl.createObjectURL(new win.Blob(['\uFEFF' + content], // #7084
                 { type: type }));
             }
         }
         catch (e) {
             // Ignore
         }
     }
	function makeMap(list, titleField, valField) {
		   if(!valField) valField="VAL";
		   if(!titleField) titleField="CRE_TIME";
		   var result = new Object();
		   for (var i = 0; list != null &&  i < list.length; i++) {
		      var m = list[i]; // usage=[...]
		      var result1 = new Object();
		      for ( var key in m) {
		         var list1 = m[key];
		         var valList = new Array();

		         for (var j = 0; j < list1.length; j++) {
		            var m1 = list1[j];

		            valList.push([ m1[titleField], m1[valField]]);



		         }
		         result1.valList = valList;


		      }
		      result[key] = result1;
		   }
		   return result;
		}
	function getGaugeChart(id, max, title, data, unit)
	{
	   // The speed gauge
	     $('#' + id).highcharts(Highcharts.merge(gaugeOptions, {
	       yAxis: {
	         min: 0,
	         max: max,
	         title: {
	           text: title
	         }
	       },

	       credits: {
	         enabled: false
	       },

	       series: [{
	         name: title,
	         data: data,
	         dataLabels: {
	           format: '<div style="text-align:center"><span style="font-size:25px;color:' +
	             ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
	             '<span style="font-size:12px;color:silver">' + unit + '</span></div>'
	         },
	         tooltip: {
	           valueSuffix: ' ' + unit
	         }
	       }]

	     }));
	}
	function convertColor(org)
	{
		if(org==null)  return "rgba(98,174, 145,0.7)";
		org = org.toLowerCase();
		if(org=='yellow') return "rgba(253, 152, 98, 0.7)";
		else if(org=='red') return "rgba(224,30, 60,0.7)" ;
		else   return "rgba(98,174, 145, 0.7)";

	}
	function columnChartConfig(  title , xtitle, ytitle , data  )
	{


		return  {

			 chart: {
			        type: 'column'
			    },
			    title: {
			        text: title
			    },
			    xAxis: {
			        type: 'category',
			        title:{
			        	text: xtitle
			        }
			    },
			    yAxis: {
			        title: {
			            text: ytitle
			        }

			    },
			    legend: {
			        enabled: false
			    },
			    plotOptions: {
			        series: {
			            borderWidth: 0,
			            dataLabels: {
			                enabled: true,
			                format: '{point.y}'
			            }
			        }
			    },

			    tooltip: {
			        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}%</b> of total<br/>'
			    },
			series :[ {  'data':data } ]


		}
	}
	
	function bubbleChartConfig(  title , xtitle, xguideLineVal, ytitle , yguideLineVal, data, zoom  )
	{


		return  {

		  chart: {
		    type: 'bubble',
		    plotBorderWidth: 1,
		    zoomType: zoom
	    	//zoomType: 'xy ' 줌기능 on
		  },

		  legend: {
		    enabled: false
		  },

		  title: {
		    text: title,

		  } ,

		  xAxis: {
		    gridLineWidth: 1,
		    title: {
		      text: xtitle
		    } ,
		    plotLines: [xguideLineVal == null ? {}:{
		      color: 'orange',
		      dashStyle: 'dot',
		      width: 2,
		      value: xguideLineVal ,
		      zIndex: 3
		    }]
		  },

		  yAxis: {
		    startOnTick: false,
		    endOnTick: false,
		    title: {
		      text: ytitle
		    } ,
		    maxPadding: 0.2,

		    plotLines: [yguideLineVal == null ? {}:{
		      color: 'orange',
		      dashStyle: 'dot',
		      width: 2,
		      value: yguideLineVal ,
		      zIndex: 3
		    }]
		  },

		  plotOptions: {
		    series: {
		      dataLabels: {
		        enabled: false,
		        format: '{point.name}'
		      }
		    }
		  },
		  series :[ {  'data':data } ]


		}
	}


	var gaugeOptions = {

		       chart: {
		         type: 'solidgauge'
		       },

		       title: null,

		       pane: {
		         center: ['10%', '15%'],
		         size: '10%',
		         startAngle: -90,
		         endAngle: 90,
		         background: {
		           backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
		           innerRadius: '6%',
		           outerRadius: '60%',
		           shape: 'arc'
		         }
		       },

		       tooltip: {
		         enabled: false
		       },

		       // the value axis
		       yAxis: {
		         stops: [
		           [0.1, '#55BF3B'], // green
		           [0.5, '#DDDF0D'], // yellow
		           [0.9, '#DF5353'] // red
		         ],
		         lineWidth: 0,
		         minorTickInterval: 0,
		         tickPixelInterval: 100,
		         tickWidth: 0,
		         title: {
		           y: -70
		         },
		         labels: {
		           y: 16
		         }
		       },

		       plotOptions: {
		         solidgauge: {
		           dataLabels: {
		             y: 5,
		             borderWidth: 0,
		             useHTML: true
		           }
		         }
		       }
		     };
		function arrDefault(def)
		{
		    var u = new Array();
		    u[0] =  def ? def : 0 ;
		    return u;
		}


		var chartObj = {};
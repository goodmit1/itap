var Chart = function(table, timefield, isEs, range)
{
	this.table = table;
	this.filter = '';
	this.timefield = timefield;
	this.raw_data = [];
	this.chart_data = [];
	this.isEs = isEs;
	this.range = range;
	this.filterInfo = {};
	this.addFilter = function( field, value, title, objName ){
		var arr   = [];
		this.filterInfo[field] = arr;
		 
	 
		if(value && value != ''){ 
			arr.push({value:value, title:title, objName:objName})
		}
	}
	this.isTarget = function(data){
		 var keys = Object.keys(this.filterInfo);
		 var result = true; 
		 var that = this;
	     keys.forEach(function(key, i){
	    	 var arr = that.filterInfo[key];
	    	 if(arr.length==0) return;
	    	 var isFind  = false;
	    	 for(var i=0; i < arr.length; i++){
	    		if( arr[i].value instanceof Array){
	    			for(var j=0; j < arr[i].value.length ; j++){
		    			if( arr[i].value[j]==data[key]){
			    			isFind = true;
			    			break;
			    		}
	    			}
	    		}
	    		else{
		    		if( arr[i].value==data[key]){
		    			isFind = true;
		    			break;
		    		}
	    		}
	    	 }
	    	 if(!isFind){
	    		 result = false;
	    		 return ;
	    		   
	    	 }
	     });
	     return result;
	}
	this.applyFilter = function(){
		this.chart_data = [];
		for(var i=0; i < this.raw_data.length; i++){
			if(this.isTarget( this.raw_data[i])){
				this.chart_data.push(this.raw_data[i]);
			}
		}
		drawAllChart(true,this.range);
		
		 
	
	}
	this.getDataByAPI = function( fromDate, toDate){
		
		  
 		var param = { };
 		if(fromDate){
 			param.FROM_DT = fromDate;
 		}
 		if(toDate){
 			param.TO_DT = toDate  ;
 		}
 		this.FROM_DT = param.FROM_DT;
 		this.TO_DT = param.TO_DT;
 		var that = this;
 		post('/api/monitor/list/' + table + '/', param, function(data1){
 			that.raw_data = data1;
 			that.chart_data = data1;
 			drawAllChart('',that.range);
 		 })
 	 
	},
	 
	this.setFilter = function( field, value, title, objName){
		if(field && !value){
			value = $("#" + field + "_filterID").val();
		}
		this.addFilter( field, value, title, objName )
		this.applyFilter();
		if(title){
			var filter_area = $("#filter_area");
			if(filter_area.length==0){
				$( ".stat_charts" ).prepend('<div id="filter_area" ></div>');
				filter_area = $("#filter_area");
			}
			
			var html = '<a class="pic" href="#" data-field="' + field + '" data-value="' + value + '" onclick="' + objName + '.removeFilter( this );return false"><span class="del">x</span>' + title + ':' + value + '</a>'
			filter_area.append(html);
		}
	},
	this.removeFilter = function( obj ){
		var key = $(obj).attr("data-field");
		var val = $(obj).attr("data-value");
		 var arr = this.filterInfo[key];
		 for(var i=0; i < arr.length; i++){
			 if(arr[i].value==val){
				 arr.splice(i,1);
				 break;
			 }
		 }
		 $(obj).remove();
		 this.applyFilter();
		 
	},
	this.resetFilter = function( field ){
		if(field){
			$("#" + field + "_filterID").val("");
			delete this.filterInfo[field]
		}
		else{
			this.filterInfo = {};
		}
		$("#filter_area").remove();
		this.applyFilter();
	},
	this.getData= function( fromDate, toDate){
		if(this.table){
			if(!this.isEs){
				this.getDataByAPI( fromDate, toDate)
			}
			else{
				this.getDataByES( fromDate, toDate)
			}
		} else{
			this.getDataObject(data)
		}
	},
	this.getCategories= function(field){
		return getCategories(this.raw_data, field);
	}
	this.getDataByES = function( fromDate, toDate){
		
		  
 		var param = {"table": this.table};
 		if(fromDate){
 			param.filter = '{range :{ "' + this.timefield + '": { "gte": "' + fromDate + '","lte": "' + toDate + '" } } }';
 		}
 		var that = this;
 		 post('/api/etc/es_search', param, function(data1){
 			that.raw_data = data1;
 			that.chart_data = data1;
 			drawAllChart('',that.range);
 		 })
 	 
	}
	this.getDataObject = function( data1 ){
		var that = this;
		if(typeof data1 == "object"){
			that.raw_data = data1;
			that.chart_data = data1;
			drawAllChart('',that.range);
		}
	}
};

		
function drawAllChart(excludeFilter,range){
	var chartlist = '';
	var that = this;
	if(range){
		chartlist = $("."+range+" .chart");
	} else{
		chartlist = $(".chart");
	}
	for(var i=0; i < chartlist.length; i++){
		if($(chartlist[i]).attr("data-setting")){
			if(excludeFilter){
				if("filter" == $(chartlist[i]).attr("data-type")){
					continue;
				}
			}
			eval("var setting=" + $(chartlist[i]).attr("data-setting") + "()");
			
			eval("draw_" + $(chartlist[i]).attr("data-type") + "('" + $(chartlist[i]).attr("id") + "', setting)");
		}
		 
	}}

function draw_filter(container, setting){
	$("#" + container).html("");
	$("#" + container).append("<div class='chart_title'>"+setting.title+"</div>");
	$("#" + container).append("<div id='filterLabelSelect' class='filterLabelSelect'><div><select " + (setting.multiple?"multiple":"") + " name='"+setting.name+"' id='"+setting.name+"_filterID' style='width:400px;' class='form-control input hastitle' onchange='" + setting.objName  + ".setFilter(\"" + setting.name + "\");'></select></div><div id='filter_btn' class='filter_btn'></div></div>");
	
	
	 
	    fillOptionByData(setting.name+"_filterID", setting.data , '' );
	    $("#"+setting.name+"_filterID").select2({});
	    initSelect2();
	 
}

function draw_grid(container, gridOptions  ){
	$("#" + container).html("");
	$("#" + container).append('<div id="' + container + '_grid" class="ag-theme-fresh"></div>');
	
	$("#"+container + "_grid").parent().prepend('<button type="button" style="top: 0px;" title="엑셀다운로드" onclick="exportExcel(\''+container+'_grid\', \''+gridOptions.title+'\' )" class="layout excelBtn"></button>');
	if(gridOptions.title){
		$("#"+container+ "_grid").css("height","365px");
		$("#"+container+ "_grid").parent().prepend("<div class='chart_title'>"+gridOptions.title+"</div>");
	}
	else
		$("#"+container+ "_grid").parent().css("height","345px");
	
	return newGrid(container+ "_grid", gridOptions);
	
}
function draw_chart(container, config){
	$("#" + container).html("");
	$("#" + container).append('<div id="' + container + '_chart"  style="height: 100%;"></div>');
	return Highcharts.chart(container + "_chart", config);
} 

function draw_metric(container, config){
	var num = formatNumber(config.val);
	if(config.numContraction){
		num = formatNumberM(config.val, config.unit);
	}
	$("#" + container).html("");
	$("#" + container).append('<div id="' + container + '_metric"  ></div>');
	if(config.img){
		$("#"+container+"_metric").append('<div class="'+isNullValue(config.imgClass)+'"><img src="'+config.img+'" width="50"></div>');
	}
	if(config.url)
		$("#"+container+"_metric").append('<div class="metric_num '+isNullValue(config.numClass)+'" ><a href='+config.url+'>'+num+'</a></div><div class="metric_text '+isNullValue(config.textClass)+'" >'+config.title+'</div>');
	else{
		if(config.functionName){
			$("#"+container+"_metric").append('<div class="metric_num '+isNullValue(config.numClass)+'" onclick="'+config.functionName+'()" >'+num+'</div><div class="metric_text '+isNullValue(config.textClass)+'">'+config.title+'</div>');
		} else{
			$("#"+container+"_metric").append('<div class="metric_num '+isNullValue(config.numClass)+'">'+num+'</div><div class="metric_text '+isNullValue(config.textClass)+'" >'+config.title+'</div>');
		}
	}
	
	
	if(config.setBgColor){
		if(config.val > 0){
			$("#"+container).css("background-color", config.BgColor);
		}
	}
}



function getGroupByData(data1, groupbyfield){
	var gdata = data1[groupbyfield];
	if(!gdata){
			gdata = '-'
	}
	else{
		try{
			var char1 = gdate.substring(0,1);
			if(char1>='0' && chart1<='9')
			{
				var date1 = new Date(gdata);
				if(date1=='Invalid Date'){
					return gdata;
				}
				return formatDate(date1,'date');
			}
		}
		catch(e)
		{}
	}
	return gdata;
}
function getCategories(data, field){
	var category={};
	for(var i=0; i <data.length; i++){
		var cdata = data[i][field];
		if(!cdata){
			cdata = '-'
		}
		category[cdata]='';
	}
	return obj2array(category, true);
}

/** heapmap용 data **/
function data4HeapMap(data, xField, yField){
	var xcategories = getCategories(data, xField);
	var ycategories = getCategories(data, yField);
	var result1 = {};
	for(var i=0; i <data.length; i++){
		 
		var xIdx = xcategories.indexOf( "" + data[i][xField])
		var yIdx = ycategories.indexOf( "" + data[i][yField])
		if(xIdx == -1){
			xIdx = xcategories.indexOf("-");
		}
		if(yIdx == -1){
			yIdx = xcategories.indexOf("-");
		}
		var arr = result1[xIdx  + "," + yIdx];
		if(!arr){
			result1[xIdx + "," + yIdx] = [ xIdx, yIdx, 1];
		}
		else{
			arr[2] = arr[2]+1;
		}
	}
	return {'xcategories':xcategories,'ycategories':ycategories, 'data': obj2array(result1, false)};
}


/** data filter **/
//filterStr : data.DEL_YN=='Y' && data.COST_UNIT =='vm'
function filter(datas, filterStr){
	var result = [];
	for(var i=0; i < datas.length; i++){
		 var data = datas[i];
		 var isok = eval(filterStr);
		 if(isok){
			 result.push(data);
		 }
	}
	return result;
}

/** metric 용 계산 **./
 * 
 * @param datas
 * @param field : sum할 field, 없으면 전체 count
 * @returns
 */
function summary(datas, field  ){
	if(!field){
		return datas.length;
	}
	var total = 0;
	for(var i=0; i < datas.length; i++){
		var val = datas[i][field];
		total += 1* (val? val:0);
	}
	return total;
}

/**
 * data가 [["name", 값]...] 인 경우 작동
 * @param setting
 * @param topCnt
 * @param otherStr
 * @returns
 */
function extractTop(setting, topCnt, otherStr){
	
	var arr = array_sort( setting.data[0].data, 1, true);
	 
	var result = [];
	for(var i=0; i < topCnt &&  i < arr.length; i++){
		result.push(arr[i]);
	}
	if(otherStr){
		var otherTotal = 0;
		for(var i=topCnt; i < arr.length; i++){
			otherTotal += arr[i][1];
		}
		if(otherTotal>0){
			result.push([otherStr,otherTotal] );
		}
	}
	setting.data[0].data = result;
	return result;
}
function extractTopMulti(setting, topCnt, otherStr){
	
	var arr = array_sort( setting.data, "total", true);
	 
	var result = [];
	for(var i=0; i < topCnt &&  i < arr.length; i++){
		result.push(arr[i]);
		
	}
	if(otherStr){
		var otherTotal = {name:otherStr, total:0, data:[]};
		for(var j=0; j < setting.categories.length; j++){
			otherTotal[setting.categories[j]]=0;
			otherTotal.data.push(0);
		}
		for(var i=topCnt; i < arr.length; i++){
			for(var j=0; j < setting.categories.length; j++){
				otherTotal[setting.categories[j]] += nvl(arr[i][setting.categories[j]] , 0)
				otherTotal.data[j] +=  nvl( arr[i][setting.categories[j]],0);
			}
			otherTotal.total += arr[i].total;
		}
		if(otherTotal.total>0){
			result.push(otherTotal);
		}
	}
	setting.data  = result;
	return result;
}
function grid_merge(targetArr, srcArr, srcField, targetField){
	var srcMap = {};
	for(var i=0; i < srcArr.length; i++){
		srcMap[srcArr[i][srcField]] = srcArr[i];
	}
	for(var i=0; i < targetArr.length; i++){
		var src = srcMap[targetArr[i][targetField]];
		if(src){
			extendNotExist(targetArr[i], src);
			delete srcMap[targetArr[i][targetField]];
		}
	}
}
/**
 * grid 
 * @param data
 * @param fields : 첫번쨰 필드로 grouping하고 field:count는 갯수
 * var columnDefs = [
		 			{headerName : '업무', field: "CATEGORY1"},
		 			{headerName : '개수', field: "count"},
		 			{headerName : 'CPU', field: "CPU_CNT", aggr:"sum"},
		 			{headerName : '메모리(GB)', field: "RAM_SIZE", aggr:"sum"},
		 			{headerName : '디스크(GB)', field: "DISK_SIZE", aggr:"sum" },
		 			
		 		];
 * @returns
 */
 

function getGroupsByData(data1, fields, obj){
	var gdata='';

	for(var j=0; j < fields.length; j++ ){
		var gdata1 = getGroupByData(data1,fields[j].field);
		obj[fields[j].field] = gdata1;
		gdata += gdata1 +'|';
	}
	return gdata;
}	
function calData4Grid(data, fields){
	var result = {};
	var groupFields = [];
	var calFields = [];
	for(var j=0; j < fields.length; j++ ){
		if( fields[j].aggr || fields[j].field == 'count'){
			if(fields[j].aggr){
				calFields.push(fields[j]);
			}
			continue;
		}
		groupFields.push(fields[j]);
	}
	
	
	for(var i=0; i <data.length; i++){
		var newObj = {};
		var gdata = getGroupsByData (data[i],groupFields, newObj);
		var obj = result[gdata]  ;
		if(!obj){
			newObj.count=1;			 
			result[gdata] = newObj;
			obj = newObj;
		}
		else{
			obj.count = obj.count+1;
		}
		for(var j=0; j < calFields.length; j++ ){
			calFields[j].format = 'number';
			if(calFields[j].aggr){
				var val = data[i][calFields[j].field]
				obj[calFields[j].field] = (obj[calFields[j].field]? obj[calFields[j].field]:0)+val
			}
		}
	}
	return obj2array(result, false)
}
function makeChartData(data, columnDefs){
 
		var data  = calData4Grid(data, columnDefs );
		return makeChartDataFormat( data,columnDefs)
}
function makeChartDataFormat( data,columnDefs){
		if(columnDefs.length==2){
			var result = []
			for(var i=0; i <data.length; i++){
				result.push([data[i][columnDefs[0].field], data[i][columnDefs[1].field]])
			}
			var sorted_result = array_sort(result, 0 );
			return { 'data':  [ {'name':columnDefs[1].headerName, 'data': sorted_result} ]};
		}
		else{
			var categories = getCategories(data, columnDefs[0].field);
			var result = []
			for(var j=1; j < columnDefs.length; j++){
				var obj = {'name': columnDefs[j].headerName , data:[]};
				result.push(obj)
				for(var k=0; k < categories.length; k++){
					obj.data[k] = 0;
				}
				
				for(var i=0; i <data.length; i++){
					var xIdx = categories.indexOf(data[i][columnDefs[0].field]);
					obj.data[xIdx] = data[i][columnDefs[j].field]
				}
				
			}
			var sorted_result = array_sort(result, 'name' );
			return {'categories':categories, 'data': sorted_result};
		}
	}

function columnAvg(data, columnField, totalColumnField){
	var column = 0;
	var totalColumn = 0 ;
	var avg = []
	var result = 0;
	if(data.length > 0){
	for(var i = 0 ; i < data.length ; i++){
		for(var key in data[i]){
			if(key == columnField){
				column += data[i][columnField];
			}
			if(key == totalColumnField){
				totalColumn += data[i][totalColumnField];
			}
		}
	}
		result = Math.floor(column/totalColumn * 100);
		avg.push( isNaN(result)  ? 0 : result);
		return {'data' : avg};
	}
}

function countByGroup(data, groupbyfield, columnfield, op){
 		if(columnfield){
 			var result={}
 	 		 
	 		var categories = getCategories(data, columnfield);
	 		for(var i=0; i <data.length; i++){
	 			var gdata = getGroupByData(data[i],groupbyfield);
	 			 
	 			var obj = result[gdata]; 
	 			if(!obj){
	 				obj = {};
	 				obj['name'] = gdata;
	 				obj.total = 0;
	 				obj.data = [];
	 				for(var j=0; j < categories.length; j++){
	 					obj.data.push(0);
	 				}
	 				result[gdata] = obj;
	 			}
	 			 
				var cdata = data[i][columnfield];
				var idx = categories.indexOf(cdata)
				if(op){
					var pos = op.indexOf("|");
					var opField = op.substring(0, pos);
					var opr = op.substring( pos+1);
					if(opr == 'sum'){ 
						obj.data[idx] = obj.data[idx]  + 1* data[i][opField];
					}
				}
				else{
					obj.data[idx] = obj.data[idx]+1;
				}
				
				obj[cdata] = obj.data[idx];
	 				 
	 			 
	 		}
	 		
	 		sorted_result = array_sort(obj2array(result,false), 'name' );
	 		for(var i=0; i < sorted_result.length; i++){
	 			var total = 0;
	 			for(var j=0; j < sorted_result[i].data.length; j++){
	 				total += 1*sorted_result[i].data[j]
	 			}
	 			sorted_result[i].total = total;
	 		}
	 		return {'categories':categories, 'data': sorted_result};
 		} else{
 			
 			var columnDefs = [{field:groupbyfield}];
 			if(op){
 				var oprArr = op.split('|'); 
 				columnDefs.push({field:oprArr[0], aggr:oprArr[1]})
 			}
 			else{
 				columnDefs.push({field:'count'});
 			}
 			return makeChartData(data, columnDefs);
 			
 		}
 	}

	function exportExcel(id, title){
		var table = getGrid(id);
		table.exportCSV({fileName:title});
	}
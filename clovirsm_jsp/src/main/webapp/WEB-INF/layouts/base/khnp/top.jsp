<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="com.clovirsm.service.admin.CategoryMapService"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.fliconz.fm.common.vo.TreeNodeMap"%>
<%@page import="com.fliconz.fm.common.util.TextHelper"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page import="com.clovirsm.service.monitor.MonitorService"%>
<%@page import="com.fliconz.fw.runtime.util.DateUtil"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.clovirsm.service.admin.CategoryMapService"%>
<%@page import="com.fliconz.fm.security.FMSecurityContextHelper"%>
<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html; charset=UTF-8" %><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%! JSONObject getMenuJSON(TreeNodeMap c, String lang){
	JSONObject json = new JSONObject();
	json.put("id", c.getMenuId());
	json.put("title", c.getMenuNm(lang));
	json.put("desc", c.getMenuDesc(lang));
	json.put("path", c.getPrgmPath());
	return json;
 }
 void putChildMenu(JSONObject pjson, TreeNodeMap p, String lang){
	 JSONArray menuJSONArr = new JSONArray();
		for(TreeNodeMap c:p.getElements()){
			JSONObject json = getMenuJSON(c,lang);
			menuJSONArr.put(json);
			putChildMenu(json, c,lang);
		}
		if(menuJSONArr.length()>0){
			pjson.put("child", menuJSONArr);
		}
 }
%>
<% 
pageContext.setAttribute("__USER_NAME__", UserVO.getUser().getUserName());
 
pageContext.setAttribute("__TEAM_NAME__", UserVO.getUser().getTeam() != null ? UserVO.getUser().getTeam().getTeamNm() : "");
if(UserVO.getUser().getLastAccessTms() != null)
{
	pageContext.setAttribute("__LOGIN_INFO__", DateUtil.format(UserVO.getUser().getLastAccessTms(),"yyyy-MM-dd HH:mm:ss") + " / " + UserVO.getUser().getLastLoginIp());
}
else
{
	pageContext.setAttribute("__LOGIN_INFO__", "");
}
String menuJSON = (String)session.getAttribute("menuJSON");
if(menuJSON == null){
	TreeNodeMap allMenu = (TreeNodeMap)UserVO.getUser().getUserMenuManager().getFirstSubMenu();
	String lang = UserVO.getUser().getLocale();
	JSONObject menuRoot = new JSONObject();
	putChildMenu(menuRoot, allMenu, lang);
	menuJSON = menuRoot.toString(); 
	session.setAttribute("menuJSON", menuJSON);
	/*
	CategoryMapService categoryMapService = (CategoryMapService)SpringBeanUtil.getBean("categoryMapService");
	Map param = new HashMap();
	param.put("KUBUN","U");
	List<Map> categorys = categoryMapService.list(param);
	Map categoryMap = new LinkedHashMap();
	for(Map m : categorys){
		categoryMap.put(m.get("CATEGORY_ID"), m.get("CATEGORY_FULL_NM"));
	}
	UserVO.getUser().getMemberData().put("CATEGORY_MAP", categoryMap);
	*/
}

	CategoryMapService categoryMapService = (CategoryMapService)SpringBeanUtil.getBean("categoryMapService");
 	
	Map searchParam = new HashMap();
	searchParam.put("KUBUN", "U");
	List list = categoryMapService.list(searchParam);
	
	request.setAttribute("list", list);

		
%>
	 
	<div class="topArea">
		<div id="topArea">
			<div class="topLogo" onclick="topLogo()">
				<img src="/res/img/clovirsm-v.png" height="50px;">
			</div>
			<div id="userInfo" class="userInfo"  style="display:inline-block;">
				<%--<div class="myTask" onclick="userTask();">
					<img src="/res/img/hynix/oval.png">
				</div>--%>
				<div class="userNameInfo">
					<span class="team">[${__TEAM_NAME__}]</span>&nbsp;
					<a href="#" onclick="userInfo();" data-toggle="modal"  class="user_name">${__USER_NAME__}</a>
					<%--<i class="fa fa-caret-dow"></i>--%>
				</div>
				<div class="userInfoDetail" id="userInfoBtn">
					<div style="margin-top:25px;">${__USER_NAME__}</div>
					<div style="font-weight: normal;">${__TEAM_NAME__}</div>
					<c:if test="${sessionScope.from_sso eq null}">
						<div class="logout_btn" onclick="window.location.href='/j_spring_security_logout';return false;"><spring:message code="logout"></spring:message></div>
					</c:if>
				</div>
				<div class="userTaskDetail" id="userTaskDetail">
					<div class="taskHeader">
						<div class="taskTitle"><spring:message code="my_task" text="담당업무"></spring:message></div>
						<div class="taskClose" onclick="userTask();"></div>
					</div>
					<div class="taskContents">
					<c:choose>
						<c:when test="${!empty list }"> 
							<c:forEach items="${ list }" var="i" varStatus="status">
								<div class="taskList">${ i.CATEGORY_FULL_NM }</div>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<div style="text-align: center; margin-bottom: 10px; margin-top: 20px;"><img src="/res/img/hynix/caution.png"></div>
							<div style="text-align: center;"><spring:message code="no_task" text="담당업무"></spring:message></div>
						</c:otherwise>
					</c:choose>
					</div>
				</div>
				<div class="userInfoDetail" id="userInfoBtn">
				</div>
			</div>
		</div>
	</div>
  	<div class="firstMenu">
  		<div id="topMenu">
	  		<div id="firstMenu" ></div>
	  	 	<div class="rightMenu">
				<div class="sm-btn topRightMenu qna-btn" onclick="topBtnClick('qna');" style="cursor: pointer;">
					<div class="circle" id="NOT_ANSWER">0</div>
				</div>
				<c:if test="${sessionScope.APPROVER_YN == 'Y'}">
				<div class="sm-btn topRightMenu req-btn" onclick="topBtnClick('req');" style="cursor: pointer;">
					<div class="circle" id="REQ_CNT">0</div></div></c:if>
				<div class="col col-sm">	
					<span id="VM_INFO"  ></span>
				</div>
	  	 		<div class="sm-btn topRightMenu menu-btn" onclick="showMenu();" style="cursor: pointer;">
				</div>
	  	 	</div>
	  	 	<div class="allMenu" style="display: none;">
	  	 		<div class="allMenuHeader"><div class="title"><spring:message code="menu" text="메뉴"/></div><div class="close" onclick="allMenuClose();"></div></div>
	  	 		<div class="allMenuContents"></div>
	  	 	</div>
  	 	</div>
 	</div>
 	<div class="left_show" style="display: none;" onclick="left_show();"></div>
 	
 	<script>
 	var modalbackdrop = "static";
 	var menuJSON = ${sessionScope.menuJSON}
 	function getMenuPath(obj){
 		if(obj.path){
 			return obj.path;
 		}
 		
 		else{
 			if(obj.hasOwnProperty("child"))
	 			return getMenuPath(obj.child[0]);
 		}
 	}
 	window.old_alert = window.alert;
	window.old_confirm = window.confirm;
	window.alert = function(msg){
		old_alert(msg + " ");
	};
	window.confirm = function(msg){
		return old_confirm(msg + " ");
	};
 	function getMenuById(child, id ){
 		for(var i=0; i < child.length; i++){
 			if(child[i].id == id){
 				return child[i]
 			}
 		}
 		return null;
 	}
 	function getQnaReqCnt(){
		$.get('/api/monitor/get_qna_req_cnt', function(data){
			$("#NOT_ANSWER").text(data.QNA_CNT);
			$("#REQ_CNT").text(data.REQ_CNT);
			$("#WORK_CNT").text(data.WORK_CNT);
		});
	}

	function topBtnClick(type){
		 
		if(type == "qna"){
			location.href = "/admin/inquiry/index.jsp"
		}else{
			location.href = "/clovirsm/workflow/approval/index.jsp"
		}
		 
	}

	$(function(){
		 
		getQnaReqCnt();
	 
	});
 	function displayTopMenu(menu){
 		var html = "<li class='sub_menu menu_" + menu.id ;
			if(accessInfo.menuPathIds.indexOf(menu.id)>=0){
				html += " selected";
			}
			html +="'><a href='" + getMenuPath(menu) ;
			 
			html += "'>" + menu.title + "</a></li>";
			return html;
 	} 
 	
 	function displayMenu(menu){
 		var html = "<li class='sub_menu menu_" + menu.id ;
			if(accessInfo.menuPathIds.indexOf(menu.id)>=0){
				html += " selected";
			} else{
				html += " noselected";
			}
 			if(menu.hasOwnProperty("child")){
				html +=" child' onclick='showChildMenu(this);'>"+ menu.title + "</li>";
 			} else{
				html +="'><a href='" + getMenuPath(menu)+"'>" + menu.title + "</a></li>";
 			}
			return html;
 	}
 	
 	function displayAllMenu(menu){
 		var html = '';
 			html += "<div class='allMenuContentsDeptOne'>";
 			html += "<div class='allMenuContentsDeptOneTitle'>"+menu.title+"</div>";
 			if(menu.hasOwnProperty("child")){
	 			for(var z = 0 ; z < menu.child.length ; z++){
	 				html += "<div class='allMenuContentsDeptTwo'>";
	 				html += "<div class='allMenuContentsDeptTwoTitle'><a href='"+ getMenuPath(menu.child[z]) +"'>"+menu.child[z].title+"</a></div>";
	 			} 		
 			}
 			
			html += "</div>";
		return html;
 	} 
 	$(document).ready(function(){
 		var arr = menuJSON.child;
 		for(var i=0; i < arr.length; i++){
	 		$(".allMenuContents").append(displayAllMenu(arr[i]));
 			$("#firstMenu").append(displayTopMenu(arr[i]));
 		}
 	})
 	function topLogo(){
 		location.href="/home/home.jsp";
 	}
 	function taskClose(){
 		
 	}
 	function showMenu(){
 		$(".allMenu").slideDown( "fast", function() {
 		    // Animation complete.
 		  });
 	}
 	function allMenuClose(){
 		$(".allMenu").slideUp( "fast", function() {});
 	}
 	
 	function showChildMenu(that){
 		 
 		if($(that).next().css("display") == "none"){
 			$(that).next().slideDown( "fast", function() {});
 		} else{
 			$(that).next().slideUp( "fast", function() {});
 		}
 		
 	}
 	</script>

<%@page import="com.fliconz.fm.common.util.TextHelper"%>
<%@page import="com.fliconz.fm.mvc.security.MenuUtil"%>
<%@page import="java.util.List"%>
<%@page import="java.lang.Integer"%>
<%@page import="java.util.Map"%>
<%@page import="com.fliconz.fm.common.vo.TreeNodeMap"%>
<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%!

boolean selsectMenu(int menuId, List<Integer> selected){
	boolean isSelected = false;
	if(selected != null && selected.size()>0){
		for(Integer s: selected){
			if(menuId == s){
				isSelected=true;
				break;
			}
		}
	}
	return isSelected;
}

void writeMenu(TreeNodeMap menu, List<Integer> selected, JspWriter out, String lang, int level) throws Exception{
	if("Y".equals(menu.getString("IS_POPUP"))) return ;
	boolean isLeaf = menu.isLeafNode();
	boolean isSelected = selsectMenu(menu.getMenuId(), selected);
	if(!isLeaf){
		out.write("<li data-target=\"#menu_" + menu.getString("ID") + "\" class=\"" + (isSelected?"active":"") + (isLeaf?" leaf":""));
	} else {
		out.write("<li class=\"" + (isSelected?"active":"no-active") + (isLeaf?" leaf":""));
	}
	int padding = 25;
	String icon = (String)menu.get("ICON");
	if(icon != null && !"".equals(icon) && !"ui-icon-menu".equals(icon.trim()) && !"ui-icon-sub".equals(icon.trim())){
		out.write(" " + menu.get("ICON"));
		padding = 40;
	}
	out.write("\"");
	if(menu.getString("PRGM_PATH") != null){
		out.write(" onclick=\"go(event,'" + menu.getString("PRGM_PATH") + "');return false\"");
	}
	if(menu.getString("MENU_DESC") != null){
		out.write(" title='" + menu.getMenuDesc(lang) + "'");
	}
	padding = padding + 15 *level;
	out.write("><a class=\"tree-toggle\" style=\"padding-left:"+padding+"px;\">");
	out.write("<span id= "+menu.getString("ID")+">" + TextHelper.nvl(menu.getMenuNm(lang),"--")  + "</span>");
	
	if(menu.getString("ID").equals("86")){
	
		out.write("<div id='approvalCnt' class='menuNewCunt'>&nbsp</div>");
	}
	if(!isLeaf){
		out.println("<span class='arrow'></span>");
	}
	if(menu.getString("ID").equals("85")){
		out.write("<div id='approval' class='menuNewCunt'>N</div>");
	}
	out.write("</a>");
	if(!isLeaf){
		out.println("<ul class=\"tree sub-menu collapse " + (isSelected ? "in":"") + " menu_level_" + level + "\" id=\"menu_" + menu.getString("ID") + "\">\n");
		for(TreeNodeMap c:menu.getElements()){
			writeMenu(c, selected, out, lang, level+1);
		}
		out.println("</ul>\n");
	}
	out.println("</li>\n");
}
%>
<%
UserVO.getUser().getUserMenuManager().setSelectedFirstMenu(28);
List navi = MenuUtil.getSelectedPathMenuId();
System.out.println("========= navi=["+navi+"]");
TreeNodeMap allMenu = (TreeNodeMap)UserVO.getUser().getUserMenuManager().getFirstSubMenu();
Map<Integer, TreeNodeMap> myMenu =(Map) UserVO.getUser().getUserMenuManager().getLeftMyMenuModel();
%>
<script type="text/javascript">
	$(document).ready(function(){
		getQnaReqCntW();
		$('.tree-toggle').on('click.toggle', function(e){
			$(this).parent().children('ul.tree').toggle(200);
		});
		post("/api/fm_ddic/info?DD_ID=ENV&DD_VALUE=manager", {}, function(data) {
			var f = false;
			if(data.KO_DD_DESC) {
				var arr = data.KO_DD_DESC.split(",");
				if(arr && arr.length != 0) {
					var html = "";
					arr.forEach(function(el){
						html += "<div><i class='fa fa-info-circle'></i>" + el + "</div>";
					})
					/* for(var i in arr) {
						html += "<div><i class='fa fa-info-circle'></i>" + arr[i] + "</div>";
					} */
					$(".menuFooter").append(html);
					f = true;
				}
			}
			if(f) {
				$(".menuFooter").show();
				$(".nav-side-menu").css("height", "calc(100% - 186px)").css("-ms-overflow-style","none");
			}
		}, true);
	$(".active ui-icon-portal > a").css("color","red");

	});

	function menuChange(name){
		if(name=="allMenu"){
			$('#menu-content').css("display","block");
			$('#menu-favorites').css("display","none");
			$('#allMenu').addClass("active");
			$('#myMenu').removeClass("active");
		}else{
			$('#menu-content').css("display","none");
			$('#menu-favorites').css("display","block");
			$('#allMenu').removeClass("active");
			$('#myMenu').addClass("active");
		}
	}
	function getQnaReqCntW(){

		$.getJSON('/api/monitor/get_qna_req_cnt',   function(data){
			if(APPROVER_YN ==='Y'){
				$("#approvalCnt").text(data.REQ_CNT);
				$(".menuNewCunt").css("display","inline-block");
			}
			if(data.REQ_CNT == null || data.REQ_CNT == "0"){
				$(".menuNewCunt").css("display","none");
			}
		});
	}
</script>

<div class="nav-side-menu">
	<div class="menu-tab">
		<div id="allMenu" onclick="menuChange('allMenu');" class="active"><spring:message code="all_menu" text="전체메뉴" /></div>
		<div id="myMenu" onclick="menuChange('myMenu');"><spring:message code="my_menu" text="마이메뉴" /></div>
	</div>
	<div class="menu-list">
		<ul id="menu-content" class="menu-content">
<%
	if(allMenu != null){
		for(TreeNodeMap c:allMenu.getElements()){
			writeMenu(c, navi, out, UserVO.getUser().getLocale(), 1);
		}
	}
%>
		</ul>
		<ul id="menu-favorites" class="menu-content" style="display:none;">
<%

	if(myMenu != null){

			for(Integer key : myMenu.keySet()){
				boolean isSelected = selsectMenu(myMenu.get(key).getMenuId(), navi);
				if(myMenu.get(key).getString("PRGM_PATH") != null){
				out.println("<li class=\"tree sub-menu favorites\" onclick=\"go(event,'" + myMenu.get(key).getString("PRGM_PATH") + "');return false\">\n");
				if(isSelected){
					out.println("<span class='active' name="+myMenu.get(key).getMenuId()+">"+TextHelper.nvl(myMenu.get(key).getMenuNm(UserVO.getUser().getLocale()),"--")+"</span>");
				} else{
					out.println("<span>"+TextHelper.nvl(myMenu.get(key).getMenuNm(UserVO.getUser().getLocale()),"--")+"</span>");
				}
				out.println("</li>\n");
				}
			}

	}
%>
		</ul>
	</div>
	<div class="menuFooter" style="display: none">
		<div style="margin-top: 20px;">
			<span><spring:message code="portal_mng_text" text="포탈관련 문의 사항"/></span>
		</div>
	</div>
</div>

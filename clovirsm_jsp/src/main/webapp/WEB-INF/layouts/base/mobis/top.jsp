<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page import="com.clovirsm.service.monitor.MonitorService"%>
<%@page import="com.fliconz.fw.runtime.util.DateUtil"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.fliconz.fm.security.FMSecurityContextHelper"%>
<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8" %><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
pageContext.setAttribute("__USER_NAME__", UserVO.getUser().getUserName());
pageContext.setAttribute("__TEAM_NAME__", UserVO.getUser().getTeam() != null ? UserVO.getUser().getTeam().getTeamNm() : "");
if(UserVO.getUser().getLastAccessTms() != null)
{
	pageContext.setAttribute("__LOGIN_INFO__", DateUtil.format(UserVO.getUser().getLastAccessTms(),"yyyy-MM-dd HH:mm:ss") + " / " + UserVO.getUser().getLastLoginIp());
}
else
{
	pageContext.setAttribute("__LOGIN_INFO__", "");
}
session.setAttribute("NAS_SHOW", PropertyManager.getBoolean("clovirsm.display.nas", true));
%>
<script>
	var modalbackdrop = true;
	$(document).ready(function(){
		//setInterval(getKeywordMarguee, 10000);
		$('body').on('click.any', function(e){
			if($(e.target).parents('.total_search_form').length == 0) {
				$('#search-result-popup').remove();
			}
		});
	});

	function getKeywordMarguee(){
		post('/taview/keyword_mon/keyword_mon.jsp',{}, function(data){
			var arr = data.DETECT;
			var text = '';
			for(var i=0; i < arr.length; i++){
				text += arr[i].KEYWORD + ' ' + arr[i].TODAY_SEARCHCOUNT  + ' / '
			}

			arr = data.INTEREST;
			for(var i=0; i < arr.length; i++){
				text += arr[i].KEYWORD + ' ' + arr[i].TODAY_SEARCHCOUNT  + ' / '
			}
			$("#marquee").html(text);
		});
	}


	var last_search_value = '';
	function do_elastic_search(){
		var search_value = $('#search_value').val();
		var doSearch = false;
		if(last_search_value != search_value && search_value != ''){
			last_search_value = search_value;
			doSearch = true;
		} else if(search_value == ''){
			last_search_value = search_value;
		}

		if(doSearch) {
			var param = {
				search_value: last_search_value
			}
			$.ajax({
				type: 'POST',
				data: param,
				dataType: 'json',
				url: '/api/search/vm.do',
				success: function(data) {
					$('#search-result-popup').remove();
					var $result = $('<div id="search-result-popup" class="search-result-popup"></div>');
					if(data.result.hits.hits && data.result.hits.hits.length > 0){
						var $result = $('<div id="search-result-popup" class="search-result-popup"></div>');
						var listCount = data.result.hits.hits.length > 10 ? 10: data.result.hits.hits.length;
						for(var i = 0; i < listCount; i++){
							var vm = data.result.hits.hits[i]._source;
							var text = vm.vm_nm;
							var title = '<spring:message code="NC_DC_DC_NM"/>: ' + vm.dc_nm + '\n'
								+ 'VM: ' + vm.vm_nm
								+ (vm.guest_nm == null ? '': ('\nOS: ' + vm.guest_nm))
								+ (vm.private_ip == null ? '': ('\nIP: ' + vm.private_ip))
								+ (vm.cmt == null ? '': ('\n<spring:message code="label_note"/>: ' + vm.cmt));
							var $label = $('<label class="search_result-item" title="'+title+'">'+ text +'</label>').prop('data', vm);
							$label.appendTo($result);
							$label.on('click.goToVM', function(e){
								var data = $(this).prop('data');
								var url = '/clovirsm/resources/vm/index.jsp';
								var param = 'dc_id=' + data.dc_id + '&keyword=' + data.vm_nm;
								window.location.href = url + '?' + param;
							}).on('mousedown.highlight', function(e){
								$(this).addClass('highlight');
							}).on('mouseup.highlight', function(e){
								$(this).removeClass('highlight');
							});
						}
						$result.appendTo($('#total_search_form'));
					}else {
						var $label = $('<label class="no-search-result"><spring:message code="label_no_result"/></label>');
						$label.appendTo($result);
					}
					$result.appendTo($('#total_search_form'));
				}
			});
		}
	}

	function do_search(){
		var search_value = $('#search_value').val();
		var doSearch = false;
		if(last_search_value != search_value && search_value != ''){
			last_search_value = search_value;
			doSearch = true;
		} else if(search_value == ''){
			last_search_value = search_value;
		}

		if(doSearch) {
			var param = {
				keyword: last_search_value
			}
			$.ajax({
				type: 'POST',
				data: param,
				dataType: 'json',
				url: '/api/search/vm.do',
				success: function(data) {
					$('#search-result-popup').remove();
					var $result = $('<div id="search-result-popup" class="search-result-popup"></div>');
					if(data.total > 0){
						var $result = $('<div id="search-result-popup" class="search-result-popup"></div>');
						var listCount = data.list.length;
						for(var i = 0; i < listCount; i++){
							var vm = data.list[i];
							var text = vm.keyword;

							var $label = $('<label class="search_result-item" >'+ text +'</label>').prop('data', vm);
							$label.appendTo($result);
							$label.on('click.goToVM', function(e){
								var _data = $(this).prop('data');
								var url = '/clovirsm/resources/vm/index.jsp';
								var param = {
									dc_id: _data.DC_ID,
									vm_nm: _data.VM_NM
								}
								post_to_url(url, param);
							}).on('mousedown.highlight', function(e){
								$(this).addClass('highlight');
							}).on('mouseup.highlight', function(e){
								$(this).removeClass('highlight');
							});
						}
						$result.appendTo($('#total_search_form'));
					}else {
						var $label = $('<label class="no-search-result"><spring:message code="label_no_result"/></label>');
						$label.appendTo($result);
					}
					$result.appendTo($('#total_search_form'));
				}
			});
		}
	}

</script>
<div id="topbar" class="navbar-fixed-top">
	<div id="topbar-ci" class="topbar-ci"  >
		<img src="/res/img/dashboard/${ sessionScope.SITE_CODE }/logo.png" class="${ sessionScope.SITE_CODE }_logo" onclick="window.location.href='/home/home.jsp';"/>
		<button id="menu-button"  class="menu-toggle" type="button">
		</button>
	</div>
	<div id="total_search_form" class="total_search_form">
		<fm-ibutton id="search_value" name="search_value" callback="do_search"></fm-ibutton>
	</div>
	<div id="userInfo" style="display:inline-block;">
		<span class="team">[${__TEAM_NAME__}]</span>&nbsp;
		${__USER_NAME__}
		<span id='lastLoginInfo'></span>
		<div class="topBtnArea" style="display: inline-block;">
			<span style="position:relative"  >
						<span class="sm-btn qna-btn"><a href="#"   title="" onclick="topBtnClick('qna');">Q&A</a></span>
						<div class="circle" id="NOT_ANSWER">0</div>
			</span>
			<span style="position:relative"  >
			<span class="sm-btn qna-btn"><a href="#"   title="" onclick="topBtnClick('req');">
				<c:if test="${sessionScope.APPROVER_YN == 'Y'}"><spring:message code="label_appr"/></c:if>
				<c:if test="${sessionScope.APPROVER_YN != 'Y'}"><spring:message code="label_work"/></c:if>
				</a></span>
				<c:if test="${sessionScope.APPROVER_YN == 'Y'}"><div class="circle" id="REQ_CNT">0</div></c:if>
				<c:if test="${sessionScope.APPROVER_YN != 'Y'}"><div class="circle" id="WORK_CNT">0</div></c:if>
				</a></span>
			<span style="position:relative">
				<span class="sm-btn qna-btn"><a href="#"   title="" onclick="langChange()"><%= UserVO.getUser().getLocale()%></a></span>
			</span>
			<c:if test="${sessionScope.from_sso eq null}">
				<button class="logout_img" title="<spring:message code="COMMON_LOGOUT"/>" onclick="window.location.href='/j_spring_security_logout';return false;"></button>
			</c:if>
		</div>


	</div>
</div>
<script>
	function getQnaReqCnt(){
		$.getJSON('/api/monitor/get_qna_req_cnt',   function(data){
			$("#NOT_ANSWER").text(data.QNA_CNT);
			$("#REQ_CNT").text(data.REQ_CNT);
			$("#WORK_CNT").text(data.WORK_CNT);
		});
	}
	function langChange(){
		var lang = "<%= UserVO.getUser().getLocale()%> ";
		var url = window.location.href ; //http:// 형식
		var uri = url.substring(url.indexOf('//')+2,url.length); // http:// 자르고 뒷 부분
		var path = uri.substring(uri.indexOf('/'),uri.length); 
		
		if(path.lastIndexOf('#') > 0){ //#뒤에 붙을때 제거
			path = path.substr(0,path.lastIndexOf('#'))
		} 
		
		if(path.lastIndexOf('?') > 0){ // ? 파라미터 초기화
			path = path.substr(0,path.lastIndexOf('?'))
		}
		 
		if(lang.indexOf('ko') === 0){
			 
			location.href = path+"?lang=en";
		}else{
			 
			location.href = path+"?lang=ko";
		}
	
	}
	
	function topBtnClick(type){
		if(type == "qna"){
			location.href = "/admin/inquiry/index.jsp"
		}else{
		<c:if test="${sessionScope.APPROVER_YN == 'Y'}">
		
			location.href = "/clovirsm/workflow/approval/index.jsp"
		</c:if>
		<c:if test="${sessionScope.APPROVER_YN != 'Y'}">
			location.href = "/clovirsm/workflow/work/index.jsp"
	
		</c:if>
		}
	}

	$(function(){
		$("#search_value").prop("readonly",false)
		getQnaReqCnt();
		setInterval(function(){
			getQnaReqCnt();
		}, 60000);
	})
</script>
<style>
.mobis_logo {
	width: 110px !important;
}
</style> 

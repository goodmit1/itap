<%@page import="com.fliconz.fm.mvc.security.MenuUtil"%>
<%@page import="com.fliconz.fm.usermenu.UserMenuManager"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.fliconz.fm.common.vo.TreeNodeMap"%>
<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%
request.setAttribute("MENU", MenuUtil.getMenuTitleNavi());
%>
<script>
	<c:if test="${sessionScope.MYMENU }">
	var mymenu = accessInfo.mymenu;
	var mymenuId = accessInfo.menuId;

	$(document).ready(function(){
		if(mymenu == true){
			$('#favoriteButton').removeClass("add");
			$('#favoriteButton').addClass("remove");
		}
	});

	function favoriteButton(that){

		if(mymenu == true){
			post('/api/my/deleteMyMenu?MENU_ID='+ mymenuId,null, function(data)
					{
						mymenu = false;
						location.reload();
					}
				)
		}
		else{
			post('/api/my/saveMyMenu?MENU_ID='+ mymenuId,null, function(data)
					{
						mymenu = true;
						location.reload();
					}
				)
		}
	}
	</c:if>
</script>
<div id="ribbon" class="navi">
	<div>
		<ol class="breadcrumb">
			<c:forEach var="navi" items="${MENU.navi}">
				<li>${ navi }</li>
			</c:forEach>
		</ol>
	</div>
	<div>
		<h4>${MENU.title}</h4>
		<h6>${MENU.desc}</h6>
		<c:if test="${sessionScope.MYMENU }">
		<button class="favoriteButton add" id="favoriteButton" onclick="favoriteButton(this);"><spring:message code="favorite" text="즐겨찾기"/></button>
		</c:if>
	</div>
</div>
<%@page import="com.fliconz.fm.mvc.service.DashboardMngService"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page import="com.clovirsm.service.monitor.MonitorService"%>
<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page import="com.clovirsm.service.workflow.NextApproverService"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>

<%
	DashboardMngService service = (DashboardMngService)SpringBeanUtil.getBean("dashboardMngService");

	MonitorService monitorService = (MonitorService)SpringBeanUtil.getBean("monitorService");
	
	NextApproverService nextApproverService = (NextApproverService)SpringBeanUtil.getBean("nextApproverService");

	Map param=new HashMap();
	param.put("_IS_ADMIN_", monitorService.isAdmin() ? "Y" : "N");
	session.setAttribute("ADMIN_YN", monitorService.isAdmin()?"Y":"N");
	session.setAttribute("APPROVER_YN", nextApproverService.isApprover()?"Y":"N");
	List<Map> list = (List<Map>)service.selectListByQueryKey("user_dashboard_item",param );
	request.setAttribute("list", list);

session.setAttribute("FW_SHOW", PropertyManager.getBoolean("clovirsm.display.fw", false));
session.setAttribute("IMG_SHOW", PropertyManager.getBoolean("clovirsm.display.img", false));
session.setAttribute("SNAPSHOT", PropertyManager.getBoolean("clovirsm.display.snapshot", false));
%>
<div class="dashboardBtnArea" style="padding: 5px 15px;">
		<fmtags:include page="btn_create.jsp" />
		<div class="selectDashboard"   style="display:none">
			<ul>
				<li v-for="item in items">
					<input type="checkbox" v-bind:id="item.value" v-bind:value="item.value" v-model="checkedNames">
					<label v-bind:for="item.value">{{ item.name }}</label>
				</li>
			</ul>
			<input type="button" id="cancelDashboardItems" style="float:right" value="<spring:message code="btn_cancel" text="취소"/>">
			<input type="button" id="saveDashboardItems" style="float:right" value="<spring:message code="btn_save" text="저장"/>">
		</div>
		<div class="Rounded-Rectangle-3" style="display: inline-block; ">
			<img id="openDashboardEdit" src="/res/img/icon_setting.png" style="width:30px;margin-left: 19px;float: right;">
		</div>
		<div class="Rounded-Rectangle-3" style="display: inline-block;margin-right: 5px ">
			<img id="dashboardRefresh" src="/res/img/dashboard/refresh-arrow.png" style="width: 16px;height: 16px;">
		</div>
	</div>
	
	<div class="dashboardArea" style="clear:both">
		<c:forEach items="${ list }" var="i">
			<div class="dashboardItem col-md-${ i.COL }" id="${ i.ITEM_ID }"  >
			<jsp:include page="${ i.ITEM_PATH }" >
				<jsp:param name="TITLE" value="${ i.ITEM_NM }"/>
			</jsp:include>
			</div>
		</c:forEach>
		 
	</div>
	<script>
	$(function(){
		getNotiTopMsg();
	});
	</script>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>

<div id="myInfo" class="modal  fade popup_dialog" role="dialog">
	<div class="modal-dialog  ">

		<!-- Modal content-->
		<div class="modal-content  container-fluid">
			<div class="modal-header">
				<button type="button" id="myInfoCloseBtn" class="close" data-dismiss="modal">&times;</button>
				<spring:message code="COMMON_USER_EDIT"/>

			</div>

			<div class="modal-body">
				<iframe src="/home/_myInfoEdit.jsp" name="child1" id="child1"
					style="height: 250px; width: 100%; border: 0;"></iframe>
			</div>
			<div class="modal-footer">
				<input type="button" class="btn btn-primary" onclick="save()" value="<spring:message code="btn_save" text="" />" />
				<button type="button" class="btn" data-dismiss="modal"><spring:message code="btn_close" text="" /></button>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$('#myInfo').on('shown.bs.modal', function (e) {
		  child1.loadData();
		})
})
function save()
{
	child1.saveUser();
}
function close()
{
	$("#myInfoCloseBtn").click();
}
</script>

</fm-modal>

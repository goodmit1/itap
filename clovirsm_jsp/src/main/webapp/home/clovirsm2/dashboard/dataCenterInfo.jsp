<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
 <%@page contentType="text/html; charset=UTF-8"%>
 <script src="/res/js/stat_chart_config.js" ></script>
<script src="/res/js/stat.js" ></script>
<style>
	.chart_red{
		background-color: #ff7f61 !important;
	}
	.chart_yellow{
		background-color: #ffd966 !important;
	}
</style>
<div class="panel panel-default chart_area" id="" style="margin-bottom:1px;">
	<div class="panel-heading"><div style="display: inline-block; padding-left:5px;"><spring:message code="" text="용도별 인프라 현황"/></div> 
		<div style="display: inline-block; margin-left: 20px;">
			<div style="display: inline-block; width: 10px; height: 10px; background-color: #ff7f61; border-radius: 50px; margin-right: 8px; margin-left: 8px;"></div><div style="display: inline-block;font-size: 10px;position: relative;top: -1px;">80% 이상</div>
			<div style="display: inline-block; width: 10px; height: 10px; background-color: #ffd966; border-radius: 50px; margin-right: 8px; margin-left: 8px;"></div><div style="display: inline-block;font-size: 10px;position: relative;top: -1px;">70% ~ 80%</div>
		</div>
	</div>
	<div id="panel-body" style="height:279px;">
		<div class="col col-sm-12">
       		<div id="dataCenterTable" class="ag-theme-fresh" style="height:280px;" ></div>
        </div>	
	</div>
</div>
<div class="panel panel-default chart_area" id="" style=" border: none;   background: none;">
	<div id="panel-body" style="height:205px;">
        <div class="col col-sm-12 stat_charts dataCenter">
        	<div class="col col-sm-3">
	  			<div id="disk_pie" class="chart" data-type="chart" data-setting="disk_pie" style="height:175px; margin-left: 0px;"></div>
			</div>
			<div class="col col-sm-3">
		  		<div id="memory_pie" class="chart" data-type="chart" data-setting="memory_pie" style="height:175px;"></div>
			</div>
			<div class="col col-sm-3">
		  		<div id="cpu_pie" class="chart" data-type="chart" data-setting="cpu_pie" style="height:175px;"></div>
			</div>
			<div class="col col-sm-3">
		  		<div id="gpu_pie" class="chart" data-type="chart" data-setting="gpu_pie" style="height:175px; margin-right: 0px;"></div>
			</div>
        </div>
 	 </div>
</div>

<script>
		var chartdata ;
		var dataCenterTable;
		$(document).ready(function(){
			var dataCentercolumnDefs = [
				{headerName : '데이터센터', field: "DC_NM"	},
				{headerName : 'Host 개수', field: "HOST_CNT"},
				{headerName : 'VM 개수', field: "VM_CNT"},
				{headerName : 'CPU(vcore)', field: "CPU", valueGetter:function(params) {
					return params.data.CPU+'/'+params.data.TOTAL_CPU;
				},
				cellClassRules: {
					'chart_red': function(params) {
				        return 80 <= Math.floor(params.data.CPU/params.data.TOTAL_CPU*100);
				      },
					'chart_yellow': function(params) {
				        return 80 > Math.floor(params.data.CPU/ params.data.TOTAL_CPU*100) && 70 <= Math.floor(params.data.CPU/params.data.TOTAL_CPU*100);
				      }
			    }
				},
				{headerName : '메모리(TB)', field: "MEM", valueGetter:function(params) {
					return params.data.MEM+'/'+params.data.TOTAL_MEM;
				},
				cellClassRules: {
					'chart_red': function(params) {
				        return 80 <= Math.floor(params.data.MEM/params.data.TOTAL_MEM*100);
				      },
					'chart_yellow': function(params) {
				        return 80 > Math.floor(params.data.MEM/params.data.TOTAL_MEM*100) && 70 <= Math.floor(params.data.MEM/params.data.TOTAL_MEM*100);
				      }
			    }},
				{headerName : 'Disk(TB)', field: "DS", valueGetter:function(params) {
					return params.data.DS+'/'+params.data.TOTAL_DS;
				},
				cellClassRules: {
					'chart_red': function(params) {
				        return 80 <= Math.floor(params.data.DS/params.data.TOTAL_DS*100);
				      },
					'chart_yellow': function(params) {
				        return 80 > Math.floor(params.data.DS/params.data.TOTAL_DS*100) && 70 <= Math.floor(params.data.DS/params.data.TOTAL_DS*100);
				      }
			    }},
				{headerName : 'GPU(Q)', field: "GPU", valueGetter:function(params) {
					return params.data.GPU+'/'+params.data.TOTAL_GPU;
				},
				cellClassRules: {
					'chart_red': function(params) {
				        return 80 <= Math.floor(params.data.GPU/params.data.TOTAL_GPU*100);
				      },
					'chart_yellow': function(params) {
				        return 80 > Math.floor(params.data.GPU/params.data.TOTAL_GPU*100) && 70 <= Math.floor(params.data.GPU/params.data.TOTAL_GPU*100);
				      }
			    }},
				{headerName : '경고개수', field: "ALARM_CNT",
				cellClassRules: {
					'chart_red': function(params) {
				        return params.data.ALARM_CNT > 0
				      }}},
				{headerName : '라이센스', field: "USED_LI", valueGetter:function(params) {
					return params.data.USED_LI+'/'+params.data.LICENSE_TOTAL;
				},
				cellClassRules: {
					'chart_red': function(params) {
				        return 80 <= Math.floor(params.data.USED_LI/params.data.LICENSE_TOTAL*100);
				      },
					'chart_yellow': function(params) {
				        return 80 > Math.floor(params.data.USED_LI/params.data.LICENSE_TOTAL*100) && 70 <= Math.floor(params.data.USED_LI/params.data.LICENSE_TOTAL*100);
				      }
			    }}
				
			];
			
			var	dataCenterGridOptions = {
				hasNo : false,
				columnDefs : dataCentercolumnDefs,
				//rowModelType: 'infinite',
				rowSelection : 'single',
				sizeColumnsToFit: true,
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				enableColResize : true,
			    enableServerSideSorting: false,
			    onRowClicked  : function() {
					var arr = dataCenterTable.getSelectedRows();
					if(arr[0].DC_ID != '') {
						chartdata.setFilter("DC_ID",arr[0].DC_ID);
						$("select[id^=DC_ID]").val(arr[0].DC_ID);
						$("select[id^=DC_ID]").trigger("change");
					}
				}
			}
			dataCenterTable = newGrid("dataCenterTable", dataCenterGridOptions);
			dataInit();
		})
	function createFooter(fee)
	{
		var result= [];
		result.push({SVC_CD_NM:'<spring:message code="label_all" text="전체" />', SVC_NM:'',CUD_CD_NM:'', FEE:fee});
		return result;
	}
	function dataInit(){
			$.post('/api/monitor/list/list_DC_TABLE/', function(data){
				dataCenterTable.setData(data);
				chartdata = new Chart(null,null,null,'dataCenter');
				setTimeout(function(){ 
					chartdata.getDataObject(data);
				}, 500)
				sumTable(data);
				
			});
	}
	function disk_pie(){
		var setting = columnAvg(chartdata.chart_data, "DS", "TOTAL_DS" );
		setting.title = 'DISK';
 		return get_solidgauge(setting);
	}
	function memory_pie(){
		var setting = columnAvg(chartdata.chart_data, "MEM", "TOTAL_MEM" );
		setting.title = 'MEMORY';
 		return get_solidgauge(setting);
	}
	function cpu_pie(){
		var setting = columnAvg(chartdata.chart_data, "CPU", "TOTAL_CPU" );
		setting.title = 'CPU';
 		return get_solidgauge(setting);
	}
	function gpu_pie(){
		var setting = columnAvg(chartdata.chart_data, "GPU", "TOTAL_GPU" );
		setting.title = 'GPU';
 		return get_solidgauge(setting);
	}
	
	function sumTable(data){
		var HOST_CNT = 0;
		var VM_CNT = 0;
		var CPU = 0;
		var TOTAL_CPU = 0;
		var MEM = 0;
		var TOTAL_MEM = 0;
		var DS = 0;
		var TOTAL_DS = 0;
		var GPU = 0;
		var TOTAL_GPU = 0;
		var ALARM_CNT = 0;
		var USED_LI = 0;
		var LICENSE_TOTAL = 0;
		 for(var i = 0 ; i < data.length ; i++){
			 HOST_CNT += data[i].HOST_CNT;
			 VM_CNT += data[i].VM_CNT;
			 CPU += data[i].CPU;
			 TOTAL_CPU += data[i].TOTAL_CPU;
			 MEM += data[i].MEM;
			 TOTAL_MEM += data[i].TOTAL_MEM;
			 DS += data[i].DS;
			 TOTAL_DS += data[i].TOTAL_DS;
			 GPU += data[i].GPU;
			 TOTAL_GPU += data[i].TOTAL_GPU;
			 ALARM_CNT += data[i].ALARM_CNT;
			 USED_LI += data[i].USED_LI;
			 LICENSE_TOTAL += data[i].LICENSE_TOTAL;
		 }
		 
		 dataCenterTable.gridOptions.api.setPinnedTopRowData(createInputFooter(HOST_CNT,VM_CNT,CPU,TOTAL_CPU,MEM,TOTAL_MEM,DS,TOTAL_DS,GPU,TOTAL_GPU,ALARM_CNT,USED_LI,LICENSE_TOTAL));

	}
	function createInputFooter(HOST_CNT,VM_CNT,CPU,TOTAL_CPU,MEM,TOTAL_MEM,DS,TOTAL_DS,GPU,TOTAL_GPU,ALARM_CNT,USED_LI,LICENSE_TOTAL){
		var result= [];
		result.push({DC_NM:'<spring:message code="total_count" text="전체" />', HOST_CNT: HOST_CNT, VM_CNT: VM_CNT, CPU: CPU, TOTAL_CPU: TOTAL_CPU, MEM:Math.floor(MEM * 1000) / 1000, TOTAL_MEM:Math.floor(TOTAL_MEM * 1000) / 1000, DS: Math.floor(DS * 1000) / 1000 , TOTAL_DS: Math.floor(TOTAL_DS * 1000) / 1000, GPU:GPU, TOTAL_GPU: TOTAL_GPU, ALARM_CNT:ALARM_CNT, USED_LI: Math.floor(USED_LI), LICENSE_TOTAL:Math.floor(LICENSE_TOTAL)});
		return result;
	}
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<c:if test="${ADMIN_YN == 'Y'}">
	<style>
	.vmText {
	    top: 30px;
	}
	</style>
</c:if>
<div id="vm_cnt" class="vmDisplay">
	<div class="vmImg">
		<div class="vmCircleArea"><div class="vmCircle"></div></div>
		<div class="vmImgText"><spring:message code="vm_state" text="보유 현황"/></div>
	</div>
	<div class="vmText">
		<div class="vm_info">
			<span><spring:message code="month_cost" text="당월 비용"/> : </span><span onclick="useHistory()" style="cursor: pointer;"><span> {{formatNumber(count_data.ALL_FEE)}} <spring:message code="money" text="원"/></span></span>
			<fm-popup-button style="display:none;" popupid="useHistory_popup" popup="/clovirsm/popup/useHistoryPopup.jsp" param="" ></fm-popup-button>
		</div>
		<div class="vm_info">
			<span><spring:message code="server_cnt" text="서버 개수"/> : </span><span><a href="/clovirsm/resources/vm/index.jsp">{{count_data.VM_COUNT}} VM</a></span>
		</div>
		<div class="vm_info">
			<span>Power On : </span><span><a href="/clovirsm/resources/vm/index.jsp?RUN_CD=R">{{count_data.ONCNT}} VM</a></span>
		</div>
		<div class="vm_info">
			<span>Power Off : </span><span><a href="/clovirsm/resources/vm/index.jsp?RUN_CD=S">{{count_data.OFFCNT}} VM</a></span>
		</div>
		<c:if test="${ADMIN_YN == 'Y'}">
			<div class="vm_info">
				<span><spring:message code="" text="업무"/> : </span><span><a href="/clovirsm/admin/category/index.jsp?NEW_CATEGORY_CODE=true">{{count.NEW_CATEGORY_CNT}} / {{count.ALL_CATEGORY_CNT}}<span style="font-size:10px">&nbsp</span></a></span>
			</div>
			<div class="vm_info">
				<span><spring:message code="" text="템플릿"/> : </span><span><a href="/clovirsm/admin/vraMng/index.jsp?NEW_CATALOG=true">{{count.NEW_TEMPLATE_CNT}} / {{count.ALL_TEMPLATE_CNT}}<span style="font-size:10px">&nbsp</span></a></span>
			</div>
		</c:if>
		<div class="vm_info">
				<span><spring:message code="" text="만료된 VM"/> : </span><span><a href="/clovirsm/workflow/expire/index.jsp">{{count_data.VM_EXPIRED}}</a></span>
		</div>
	</div>
</div>
<script>



var vm_cnt = new Vue({
  el: '#vm_cnt',
  data: {
	  count_data: {ALL_FEE:0, VM_COUNT:0, ONCNT:0, OFFCNT:0},
	  count: {NEW_CATEGORY_CNT:0, ALL_CATEGORY_CNT:0, NEW_TEMPLATE_CNT:0, ALL_TEMPLATE_CNT:0}
  },
  methods: {
	  getVmInfo: function () {
		var that = this;
		var date = new Date();
		var yyyymm = that.date_change(date);
		$.get('/api/monitor/list/list_vm_cnt/?YYYYMM='+ yyyymm, function(data)
			{
				 
				that.count_data= data[0];

			}
		)
    },
  	  date_change: function(date){
	  	  var year = date.getFullYear();
	  	  var month = date.getMonth()+1;
	  	  month = (month < 10) ? '0' + month : month;
	      var day = date.getDate();
	      day = (day < 10) ? '0' + day : day;
	      return year+""+month;
  	  },
    getCNT: function () {
		var that = this;
		$.get('/api/monitor/list/list_CATEGORY_TEMPLATE_CNT/', function(data)
			{
				 
				that.count= data[0];

			}
		)
    },
  }
});

$(document).ready(function(){
	vm_cnt.getVmInfo();
	refCallback.push("vm_cnt.getVmInfo");
	if(ADMIN_YN == "Y"){
		vm_cnt.getCNT();
		refCallback.push("vm_cnt.getCNT");
	}
})
	
	function useHistory(){
		$("#useHistory_popup_button").trigger('click');
	}
</script>
<%@page import="java.util.List"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="com.clovirsm.service.monitor.MonitorService" %>
<%
	MonitorService monitorService = (MonitorService)SpringBeanUtil.getBean("monitorService");
	List list = monitorService.getNoticeList();
	request.setAttribute("NOTICE", list);
%>
<style>
	@media (min-width:768px){.modal-dialog{width:800px;margin:55px auto}}

</style>
<script>
function viewNoticeTop(id,display)
{
	cookieId=id
	var count = 0;
	showLoading();
	$("#noticeTopFrame_"+id).attr("src","/home/_viewNotice.jsp?ID=" + id );
	$("#notice_top_popup_"+id).modal(display);	
	
	$(document).on('show.bs.modal', '.modal', function (event) {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });
}
</script>

<div>
	<c:if test="${fn:length(NOTICE) > 0}">
		<c:forEach items="${NOTICE}" var="notice" varStatus="index">
			<div id="notice_top_popup_${notice.ID}" class="modal  fade popup_dialog" role="dialog">
				<div class="modal-dialog  ">
			      <!-- Modal content-->
			      <div class="modal-content  container-fluid">
			         <div class="modal-header header-title">
			            <button type="button" id="noticeCloseBtn" onclick="checkYN()" class="close" data-dismiss="modal">&times;</button>
			            <spring:message code="title_noti" text="" />
			         </div>
			
			         <div class="modal-body">
			            <iframe name="noticeFrame" id="noticeTopFrame_${notice.ID}" scrolling="no"
			               style="height: 600px; width: 100%; border: 0;"></iframe>
			         </div>
			         <div class="modal-footer">
			            <div id="chk_left" style="float: left;">
			               <input type="checkbox" name="chkbox" id="checkCloseYn_${notice.ID}" value="Y"/> <spring:message code="keep_windows_week" text="일주일간 이 창을 열지 않음" />
			            </div>
			            <%-- <button type="button" onclick="checkYN()" class="btn" data-dismiss="modal"><spring:message code="btn_close" text="" /></button> --%>
			         </div>
			      </div>
			   </div>
			</div>
		</c:forEach>
	</c:if>
</div>
<script>

function close()
{
   $("#noticeCloseBtn").click();
}

function  checkYN(){
    
    
   var val = $('#checkCloseYn_'+cookieId).prop("checked");
    
   if(val)
   {
      setCookie('cookie'+cookieId,'Y',7);
   }
}

</script>

</fm-modal>
 <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/popup_main">
<layout:put block="content">
<style>
  body{
	background-color : white;
  }
  .panel-body{
  	height : 500px;
  }
.choice  {
	background-color: #d2e2e4;
	border: 1px solid #aaa;
	border-radius: 4px;
	cursor: default;
	float: right;
	margin-right: 5px;
	margin-top: 5px;
	padding: 0 5px;
	list-style: none;
   }
</style>
<div id="input_area"  >

	<div class="form-panel detail-panel panel panel-default padding0">
		<div class="panel-body" style="overflow-y: auto; word-break: break-word;">
			<div>
				<div class="col col-sm-12" style="border-bottom: none; padding-left: 10px; display: inline-block; width: 80%;">
					<h3 class="noticeTitle">{{form_data.TITLE}}</h3>
				</div>
				<div class="col col-sm-12 small text-right" style="border-bottom: none; width: 20%;  display: inline-block; line-height: 7;">
					<div ><spring:message code="INS_TMS"/> : {{formatDate(form_data.INS_TMS,'datetime')}}</div>
				</div>
			</div>
			<div class="col col-sm-12 bottom" style="border-bottom: none;  margin-top: 10px; border-top: 1px solid #ddd;">
				<jsp:include page="/home/common/_fileAttach.jsp">
					<jsp:param value="notice" name="kubun"/>
				</jsp:include>
			</div>
			<div class="col col-sm-12" style="clear:both;border-bottom: none; padding: 20px;">
				<div v-html="form_data.CONTENTS" style="height: 330px; overflow: auto;"></div>
			</div>

		</div>
	</div>
</div>
<script>

var req = new Req();
$(document).ready(function(){
	req.getInfo("/api/notice/info?ID=${param.ID}", function(data){
		noticefileAttach_list("${param.ID}");
		parent.hideLoading();
		$(".noticeTitle").attr("title",data.TITLE);
	});

})

</script>
</layout:put>
</layout:extends>

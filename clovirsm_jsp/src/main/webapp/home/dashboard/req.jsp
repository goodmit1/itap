<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<div class="panel panel-default chart_area " id="reqList">
	<div class="panel-heading"><a href="/clovirsm/requestHistory/index.jsp"><spring:message code="dashboard_req" text="" /></a></div>
	<div class="panel-body">
		<div class="dashboard_list" style="width:100%">
			<table>
				<colgroup>
					<col width="70%" />
					<col width="20%" />
					<col width="10%" />
				</colgroup>
			 	<thead>
				    <tr>
				      <th><spring:message code="label_title"/></th>
				      <th><spring:message code="INS_TMS"/></th>
				      <th><spring:message code="NC_INQ_ANSWER"/></th>
				    </tr>
			  	</thead>
			  	<tbody>
			  		<tr v-for="req in reqList" style="width:100%">
			  			<td class="textleft"><span class="dashboard_list_nm qna ellipsis">{{req.REQ_NM}}</span></td>
			  			<td><span class="dashboard_noti_date">{{req.INS_TMS | date('yyyy-MM-dd')}}</span></td>
			  			<td><img class="req_img" :src="'/res/img/dashboard/req_' + req.APPR_STATUS_CD + '.png'" :title="req.APPR_STATUS_CD_NM"></img></td>
			  		</tr>
			  	</tbody>
			</table>
		</div>
		<!-- <ul class="dashboard_list">
			<li class="dashboard_list_item" v-for="req in reqList" style="width:100%">
				<div class="dashboard_list_nm qna ellipsis">{{req.REQ_NM}}</div>
				<div class="dashboard_list_icon noti_date">
					<span class="dashboard_noti_date">{{req.INS_TMS | date('yyyy-MM-dd')}}</span>
					<img class="req_img" :src="'/res/img/dashboard/req_' + req.APPR_STATUS_CD + '.png'" :title="req.APPR_STATUS_CD_NM"></img>
				</div>
			</li>
		</ul>  -->
	</div>
</div>
<script>
	var reqList = new Vue({
		el: '#reqList',
		data: {
			reqList: []
		},
		methods: {
			getReqInfo: function () {
				var that = this;
				$.get('/api/monitor/list/list_NC_REQ/', function(data){
					that.reqList= data;
				});
			}
		}
	});

	$(document).ready(function(){
		reqList.getReqInfo();
		refCallback.push("reqList.getReqInfo");
	});
</script>

<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<style>
#req_cnt_area .req_cnt1 {
	width: 150px;
}
.req_cnt_label {
	font-size: 17px;
	color: #767676;
	font-weight: bold;
	position: absolute;
    left: 95px;
    bottom: 40px;
}
#R_CNT_area {
	right: 99px;
	top: 9px;
}
#C_CNT_area {
	right: 85px;
	top: 45px;
}
#U_CNT_area {
    right: 42px;
    bottom: 72px;
}
#D_CNT_area {
    right: 84px;
    bottom: 2px;
}
</style>
<div class="panel panel-default chart_area " id="req_cnt_area">
	<div class="panel-heading"><spring:message code="label_req_info"/></div>
	<div class="panel-body">
		<div class="sm_panel_content">
		 	<img src="/res/img/dashboard/admin_req_cnt.png" style="margin-top: 35px;"/>
			<div id="C_CNT_area" class="req_cnt1"><span class="work_sm_val" id="C_CNT">{{count_data.C_CNT}}</span>
				<span class="work_sm_title" onclick="location.href='/clovirsm/workflow/approval/index.jsp'"><spring:message code="btn_new_req" text="신규요청"/></span>
			</div>
			<div id="U_CNT_area" class="req_cnt1"><span class="work_sm_val" id="U_CNT">{{count_data.U_CNT}}</span>
				<span class="work_sm_title" onclick="location.href='/clovirsm/workflow/approval/index.jsp'"><spring:message code="label_change_req" text="변경요청"/></span>
			</div>
			<div id="D_CNT_area" class="req_cnt1"><span class="work_sm_val" id="D_CNT">{{count_data.D_CNT}}</span>
				<span class="work_sm_title" onclick="location.href='/clovirsm/workflow/approval/index.jsp'"><spring:message code="label_collect_req" text="회수요청"/></span>
			</div>
			<div class="req_cnt_label"><spring:message code="label_appr_state" text="결재현황"/></div>
		</div>
	</div>
</div>
<script>
var req_cnt_area = new Vue({
	el: '#req_cnt_area',
	data: {
		count_data: {C_CNT:0, U_CNT:0, R_CNT:0, D_CNT:0}
	},
	methods: {
		getReqInfo: function () {
			var that = this;
			$.get('/api/monitor/list/list_ADM_CNT/', function(data) {
					that.count_data= data[0];
				}
			)
		}
	}
});
$(document).ready(function(){
	req_cnt_area.getReqInfo();
	refCallback.push("req_cnt_area.getReqInfo");
})
</script>

<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<div class="panel panel-default chart_area " id="reqVMList">
	<div class="panel-heading"><spring:message code="recent_items"/></div>
	<div class="panel-body">
		<div class="dashboard_list" style="width:100%">
			<table>
				<colgroup>
					<col width="30%" />
					<col width="20%" />
					<col width="20%" />
					<col width="20%" />
				</colgroup>
			 	<thead>
				    <tr>
				      <th><spring:message code="label_title"/></th>
				      <th><spring:message code="application_classification" text="신청 구분"/></th>
				      <th><spring:message code="INS_TMS"/></th>
				      <th><spring:message code="STATUS" text="상태"/></th>
				    </tr>
			  	</thead>
			  	<tbody>
			  		<tr v-for="req in reqVMList.list">
			  			<td class="textleft"><a style="cursor: pointer;"   :data-id=" req.SVC_ID"  >{{req.SVC_NM}}</a></td>
			  			<td><span >{{req.SVC_CD_NM}} {{req.CUD_CD_NM}}</span></td>
			  			<td><span >{{formatDate(req.INS_TMS ,'date')}}</span></td>
			  			<td>{{req.APPR_STATUS_CD_NM}}</td>
			  		</tr>
			  	</tbody>
			</table>
		</div>


	</div>
</div>
<script>

	function adminCheck(){
		var val = ADMIN_YN;
		if(val === 'Y')
		{
			return val;
		}
		else{
			return '';
		}
		
		
	}

	var reqVMList = new Vue({
		el: '#reqVMList',
		data: {
			reqVMList: []
		},
		methods: {
			getVmReqInfo: function () {
				 
				var that = this;
				$.get('/api//resource/req_history/list?pageNo=1&pageSize=10', function(data){
					 
					that.reqVMList= data;
				});
			}
		}
	});

	$(document).ready(function(){
		reqVMList.getVmReqInfo();
		refCallback.push("reqVMList.getVmReqInfo");
	});
</script>

<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
 <%@page contentType="text/html; charset=UTF-8"%>
<script src="/res/js/solid-gauge.js"></script>
			<div class="" id="car_dept_stat_area">
				<img src="/res/img/dashboard/bg_car.png" />
				<div class="dashboard_title"><spring:message code="title_group_res_use" /></div>
				<div id="container">
					<div id="circle">
						<ul>
						</ul>
					</div>
					<div class="center">
						<div class="chart">
							<div id="cpu">
							</div>
							<div id="ram">
							</div>
							<div id="disk">
							</div>
							<div id="vm">
							</div>
						</div>
						<div class="btn_area">
							<input type="button" onclick="getItems('U');return false;" id="car_dept_up_btn"   value="<spring:message code="btn_up" />"   />
							<input type="button" onclick="getItems('D');return false;" id="car_dept_down_btn" value="<spring:message code="btn_down"/>" />
						</div>
					</div>
				</div>
			</div>

<script>
//<![CDATA[
var data = []
var deg = 0;
var curr = 0;
var curr_deg = 0;
var circle_r = 280;
var li_r = 55;
var level = 2;
var parentList = [null];
var max = {};
var unit = {};
unit.cpu  = "<spring:message code="label_cnt_unit"/>";
unit.vm   = "<spring:message code="label_cnt_unit"/>";
unit.ram  = "<spring:message code="label_disk_size_unit"/>";
unit.disk = "<spring:message code="label_disk_size_unit"/>";


function getCarStatMonitorInfo(param)
{
	$.post('/api/monitor/list/list_CAR_STAT/', param, function(data1){
		data = data1;
		onAfterGetData( data);
		});
}

$(function(){
	$("#car_dept_stat_area #circle").css("width", (circle_r * 2) + "px");
	$("#car_dept_stat_area #circle").css("height", (circle_r * 2) + "px");
	getItems();
	refCallback.push("getItems");
});

function itemClick(idx){
	var angle = getAngle(idx);
	curr = idx;
	$("#car_dept_stat_area #circle").css("transform", "rotate(" + (angle * -1.0) + "deg)");
	curr_deg = angle;
	$("#car_dept_stat_area #circle li").css("transform", "rotate(" + angle + "deg)")
	$("#car_dept_stat_area #circle li").removeClass("selected");
	$("#car_dept_stat_area #circle li:nth-child(" + ++idx + ")").addClass("selected");
	btnShowYn(curr);
	if(data[curr] != null) {
		makeChart(data[curr], "CPU_CNT", max.cpu, "cpu", "CPU", "#7092c6");
		makeChart(data[curr], "RAM_SIZE", max.ram, "ram", "RAM", "#e72e25");
		makeChart(data[curr], "DISK_SIZE", max.disk, "disk", "DISK", "#699583");
		makeChart(data[curr], "CNT", max.vm, "vm", "VM", "#1c6ccf");
	}
}
function getItems(kubun){
	$("#car_dept_stat_area #circle ul").html("");
	$("#car_dept_stat_area #circle").removeClass("delay");
	$("#car_dept_stat_area #circle").css("transform", "rotate(1800deg)");
	var param = [];
	if(kubun == "U") {
		level--;
	}else if(kubun == "D") {
		level++;
		parentList[level - 2] = data[curr].TEAM_CD;
	}
	param.push({name: "LEVEL", value: level});
	if(parentList[level - 2] != null){
		param.push({name: "PARENT_CD", value: parentList[level - 2]});
	}
	 
	getCarStatMonitorInfo(param);
}

function btnShowYn(idx){
	var row = data[idx];
	var showCnt = 0;
	if(parentList[level - 2] != null){
		$("#car_dept_up_btn").show();
		showCnt++;
	}else {
		$("#car_dept_up_btn").hide();
	}
	if(row == null || row.LEAF_YN == 'Y'){
		$("#car_dept_down_btn").hide();
	}else {
		$("#car_dept_down_btn").show();
		showCnt++;
	}
	if(showCnt == 2){
		$(".btn_area input").css("width", "50%");
	}else {
		$(".btn_area input").css("width", "100%");
	}
}

function onAfterGetData(  data){
	$("#car_dept_stat_area #circle").addClass("delay");
	curr_deg = -1800;

	btnShowYn(0);
	makeCircle(data);
	max.cpu = 0;
	max.ram = 0;
	max.disk = 0;
	max.vm = 0;
	for(var i in data){
		var row = data[i];
		max.cpu  += row.CPU_CNT;
		max.ram  += row.RAM_SIZE;
		max.disk += row.DISK_SIZE;
		max.vm   += row.CNT;
	}
	itemClick(0);
}

function makeChart(row, key, max, id, title, color){
	var tbYn = false;
	if(id == "disk" || id == "ram") {
		tbYn = true;
	}
	var config = {
		chart: { type: 'solidgauge' },
		title: "",
		pane: {
			center: ['50%', '85%'],
			size: '100%',
			startAngle: -90,
			endAngle: 90,
			background: {
				backgroundColor: '#EEE',
				innerRadius: '60%',
				outerRadius: '100%',
				shape: 'arc'
			}
		},
		tooltip: { enabled: false },
		yAxis: {
			stops: [[0, color]],
			lineWidth: 0,
			tickAmount: 2,
			labels: {
				y: 16,
				formatter: function() {
					if(tbYn){
						if(this.value != null && !isNaN(this.value) && this.value > 1024) {
							var i = formatNumber(this.value / 1024, 1) + 'TB';
							if(i.indexOf('.0TB') >= 0) {
								i = i.replace('.0TB', 'TB');
							}
							return i;
						}
					}
					return formatNumber(this.value);
				},
				style: {"color": "#FFFFFF"}
			},
			min: 0,
			max: max,
			tickInterval: 0.001,
			tickWidth: 0,
			title: null,
			tickPositions: [0, max],
			tickColor: "#FFFFFF"
		},
		credits: { enabled: false },
		plotOptions: {
			solidgauge: {
				dataLabels: {
					y: 15,
					borderWidth: 0,
					useHTML: true,
					formatter: function() {
						return '<div style="text-align:center"><span style="font-size:15px;color: ' + color + '">' + ((tbYn && this.y > 1024) ? formatNumber(this.y / 1024, 1) : formatNumber(this.y)) + '<span style="font-size: 10px;">' + ((tbYn && this.y > 1024) ? 'TB' : unit[id]) + '</span></span><br/><span style="font-size:10px;color: #FFF">' + title + '</span></div>';
					}
				}
			}
		},
		series: [{
			data: [row[key]]
		}]
	};

	return Highcharts.chart(id, config);
}
function getAngle(idx){
	var plus_angle = deg * idx;
	var minus_angle = plus_angle - 360;
	var mod = curr_deg % 360;
	 
	 
	 
	if(Math.abs(plus_angle - mod) > Math.abs(minus_angle - mod)){
		return curr_deg + minus_angle - mod;
	}else{
		return curr_deg + plus_angle - mod;
	}
}

function makeCircle(data){
	deg = 360 / data.length;
	var parent = $("#car_dept_stat_area #circle ul");
	parent.html("");
	for(var i in data){
		var row = data[i];
		var pos = getPostion(i);
		var html = '<li id="circle' + i + '" class="delay" onclick="itemClick(' + i + ')" style="top: ' + pos.y + 'px; left: ' + pos.x + 'px;">';
		html += '<div>';
		html += '<img src="' + getRandomIcon() + '">';
		html += '</img>';
		html += '</div>';
		html += '<span>' + row.TEAM_NM + '</span>';
		html += '</li>';
		parent.append(html);
	}
	$("#car_dept_stat_area #circle li").css("width", (li_r * 2) + "px");
	$("#car_dept_stat_area #circle li").css("height", (li_r * 2) + "px");
}

function getRandomIcon(){
	var random = Math.floor((Math.random() * 13) + 1);
	return "/res/img/dashboard/car_dept_icon/car_icon_" + random + ".svg"
}

function getPostion(idx){
	var result = {};
	var angle = (180 - deg * idx) * Math.PI / 180;
	result.x = circle_r - li_r + Math.sin(angle) * circle_r;//x
	result.y = circle_r - li_r + Math.cos(angle) * circle_r;//y
	return result;
}
//]]>
</script>

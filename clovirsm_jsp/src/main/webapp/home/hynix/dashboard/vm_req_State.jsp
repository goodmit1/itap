<%@page import="com.fliconz.fm.security.FMSecurityContextHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%
String url1="/clovirsm/workflow/templateReq/index.jsp";
String url2="/clovirsm/resources/vm/index.jsp";
String url3="/clovirsm/workflow/expire/index.jsp";
String url4="/clovirsm/resources/vra/index.jsp";
String url5="/clovirsm/admin/category/index.jsp";
if( FMSecurityContextHelper.checkMethodPermissionByURL("update",url1)){
	url1="#none";
}	
if( FMSecurityContextHelper.checkMethodPermissionByURL("update",url2)){
	url2="#none";
}	
if( FMSecurityContextHelper.checkMethodPermissionByURL("update",url3)){
	url3="#none";
}	
if( FMSecurityContextHelper.checkMethodPermissionByURL("update",url4)){
	url4="#none";
}	
if( FMSecurityContextHelper.checkMethodPermissionByURL("update",url5)){
	url5="#none";
}	
pageContext.setAttribute("url1", url1);
pageContext.setAttribute("url2", url2);
pageContext.setAttribute("url3", url3);
pageContext.setAttribute("url4", url4);
pageContext.setAttribute("url5", url5);
	%>
<c:if test="${ADMIN_YN == 'Y'}">
	<style>
		.vmText {
			top: 30px;
		}
		
		.metric_num {
			text-align: center;
			font-size: 80px;
			font-weight: bolder;
			line-height: 1;
			 margin-top: 35px;
			margin-bottom: 16px;
			font-family: sans-serif;
		}
		.stat_charts .chart {
		    margin: 2px;
		}
		.imgClass{
			text-align: center; position: relative; top: 30px;
		}
		.numClass{
			position: relative; top: 43px;  font-size: 40px; font-weight: 100;
		}
		.textClass{
			position: relative; top: -51px; color: #a4a7a9;
		}
	</style>
</c:if>
<script src="/res/js/stat_chart_config.js" ></script>
<script src="/res/js/stat.js" ></script>
<style>
.stat_charts .chart {
	height:204px !important;
}
</style>
<div id="vm_cnt" class="stat_charts reqState " style="height:416px;margin-bottom: 20px;" >
 
	<div class="col col-sm-3" style="width: 215px;">
  		<div id="vm_fee" class="chart" data-type="metric" data-setting="vm_fee" style="margin-top: 0px; margin-left: 0px;"></div>
	</div>
	<div class="col col-sm-3" style="width: 130px;">
  		<div id="vm_temp_req" class="chart" data-type="metric" data-setting="vm_temp_req" style="margin-top: 0px; margin-left: 0px;"></div>
	</div>
	<div class="col col-sm-3" style="width: 130px;">
  		<div id="vm_expired" class="chart" data-type="metric" data-setting="vm_expired" style="margin-top: 0px; margin-left: 0px;"></div>
	</div>
	<div class="col col-sm-3" style="width: 130px;">
  		<div id="vm_delete" class="chart" data-type="metric" data-setting="vm_delete" style="margin-top: 0px; margin-left: 0px;"></div>
	</div>
	<div class="col col-sm-3" style="width: 215px;">
  		<div id="vm_count" class="chart" data-type="metric" data-setting="vm_count" style="margin-top: 0px; margin-left: 0px;"></div>
	</div>
	<div class="col col-sm-3" style="width: 130px;;">
  		<div id="vm_req" class="chart" data-type="metric" data-setting="vm_req" style="margin-top: 0px; margin-left: 0px;"></div>
	</div>
	<div class="col col-sm-3" style="width: 130px;">
  		<div id="templete" class="chart" data-type="metric" data-setting="templete" style="margin-top: 0px; margin-left: 0px;"></div>
	</div>
	<div class="col col-sm-3" style="width: 130px;">
  		<div id="new_task" class="chart" data-type="metric" data-setting="new_task" style="margin-top: 0px; margin-left: 0px;"></div>
	</div>
	 
	 
	<fm-popup-button style="display:none;" popupid="useHistory_popup" popup="/clovirsm/popup/useHistoryPopup.jsp" param="" ></fm-popup-button>
	  
</div>
<script>

var chart = new Chart(null,null,null,'reqState');
var unionAllData = new Object; 
var vm_cnt = new Vue({
  el: '#vm_cnt',
  data: {
	  count_data: {ALL_FEE:0, VM_COUNT:0, ONCNT:0, OFFCNT:0},
	  count: {NEW_CATEGORY_CNT:0, ALL_CATEGORY_CNT:0, NEW_TEMPLATE_CNT:0, ALL_TEMPLATE_CNT:0, VM_EXPIRED:0,VM_DELETED:0},
	  req_data: {CC:0, SD:0, SU:0, EU:0, VU:0, R:0, A:0 , D:0}

  },
  methods: {
	  getVmInfo: function () {
		var that = this;
		var date = new Date();
		var yyyymm = that.date_change(date);
		$.get('/api/monitor/list/list_vm_cnt/?YYYYMM='+ yyyymm, function(data)
			{
				 
				that.count_data= $.extend(that.count_data, data[0]);

			}
		)
    },
  	  date_change: function(date){
	  	  var year = date.getFullYear();
	  	  var month = date.getMonth()+1;
	  	  month = (month < 10) ? '0' + month : month;
	      var day = date.getDate();
	      day = (day < 10) ? '0' + day : day;
	      return year+""+month;
  	  },
    getCNT: function () {
		var that = this;
		$.get('/api/monitor/list/list_CATEGORY_TEMPLATE_CNT/', function(data)
			{
				 
				that.count=  $.extend(that.count,data[0]);

			}
		)
    },getReqInfo: function () {
		var that = this;
		$.get('/api/monitor/list/list_req_cnt/?dd=7', function(data)
			{
				for(var i = 0; i < data.length; i++){
					var cd = data[i].CD;
					that.req_data[cd] = data[i].NUM;
				}
			}
		)
    }
  }
});

$(document).ready(function(){
	vm_cnt.getVmInfo(); 
	refCallback.push("vm_cnt.getVmInfo");
	if(ADMIN_YN == "Y"){
		vm_cnt.getCNT();
		vm_cnt.getReqInfo();
		setTimeout(function(){ 
			unionAllData = $.extend({}, vm_cnt.count, vm_cnt.count_data);
			unionAllData = $.extend({}, unionAllData, vm_cnt.req_data);
			chart.getDataObject(unionAllData);
		}, 800);

		
		refCallback.push("vm_cnt.getCNT");
	}
})
	
function vm_fee(){
	var setting = {};
	var data = chart.chart_data.ALL_FEE;
	setting.title = "<spring:message code="month_cost" text="당월 비용" />";
	setting.val = data;
	setting.unit = "만원";
	setting.functionName = 'useHistory';
	setting.img = '/res/img/hynix/newHynix/dashboard/bill.png';
	setting.imgClass="imgClass";
	setting.numClass="numClass";
	setting.textClass="textClass";
	setting.numContraction= true;
	return setting;
}
function vm_req(){
	var setting = {};
	var data = chart.chart_data.R;
	setting.title = "<spring:message code="mail_title_req" text="결재 요청" /><div style='font-size: 10px;'>(<spring:message code="count" text="건수" />)</div>";
	setting.val = data;
	setting.img = '/res/img/hynix/newHynix/dashboard/approval.png';
	setting.imgClass="imgClass";
	setting.numClass="numClass";
	setting.textClass="textClass";
	return setting;
}
function vm_temp_req(){
	var setting = {};
	var data = chart.chart_data.NEW_TEMPLATE_CNT;
	setting.title = "<spring:message code="title_catalog_req" text="카탈로그 요청" /><div style='font-size: 10px;'>(<spring:message code="count" text="건수" />)</div>";
	setting.val = data;
	setting.img = '/res/img/hynix/newHynix/dashboard/newtemplate.png';
	setting.imgClass="imgClass";
	setting.numClass="numClass";
	setting.textClass="textClass";
	setting.url = "${url1}";
	return setting;
}
function vm_expired(){
	var setting = {};
	var data = chart.chart_data.VM_EXPIRED;
	setting.title = "<spring:message code="expired" text="만료예정" /> VM <div style='font-size: 10px;'>(7<spring:message code="label_date" text="일" />)</div>";
	setting.val = data;
	setting.setBgColor = true;
	setting.BgColor = "rgb(234, 221, 218)";
	setting.img = '/res/img/hynix/newHynix/dashboard/expiration.png';
	setting.imgClass="imgClass";
	setting.numClass="numClass";
	setting.textClass="textClass";
	setting.url = "${url2}?EXPIRE_DAY=7";
	return setting;
}
function vm_delete(){
	var setting = {};
	var data = chart.chart_data.VM_DELETED;
	setting.title = "<spring:message code="deleted" text="삭제예정" /> VM <div style='font-size: 10px;'>(7<spring:message code="label_date" text="일" />)</div>";
	setting.val = data;
	setting.setBgColor = true;
	setting.BgColor = "rgb(234, 221, 218)";
	setting.img = '/res/img/hynix/newHynix/dashboard/delete.png';
	setting.imgClass="imgClass";
	setting.numClass="numClass";
	setting.textClass="textClass";
	setting.url = "${url3}?DELETE_DAY=7";
	return setting;
}
function vm_count(){
	var setting = {};
	var data = chart.chart_data.VM_COUNT;
	setting.title = "<spring:message code="deploy" text="배포" /> VM<div style='font-size: 10px;'>(<spring:message code="count" text="건수" />)</div>";
	setting.val = data;
	setting.url = '${url4}';
	setting.img = '/res/img/hynix/newHynix/dashboard/template.png';
	setting.imgClass="imgClass";
	setting.numClass="numClass";
	setting.textClass="textClass";
	return setting;
}
function templete(){
	var setting = {};
	var data = chart.chart_data.ALL_TEMPLATE_CNT;
	setting.title = "<spring:message code="NC_VRA_CATALOG_CATALOG_ID" text="카탈로그" /><div style='font-size: 10px;'>(<spring:message code="count" text="건수" />)</div>";
	setting.val = data;
	setting.img = '/res/img/hynix/newHynix/dashboard/template.png';
	setting.imgClass="imgClass";
	setting.numClass="numClass";
	setting.textClass="textClass";
	return setting;
}
function new_task(){
	var setting = {};
	var data = chart.chart_data.NEW_CATEGORY_CNT;
	setting.title = "<spring:message code="new_task" text="신규업무" /><div style='font-size: 10px;'>(<spring:message code="count" text="건수" />)</div>";
	setting.val = data;
	setting.img = '/res/img/hynix/newHynix/dashboard/newwork.png';
	setting.imgClass="imgClass";
	setting.numClass="numClass";
	setting.textClass="textClass";
	setting.url ="${url5}?NEW_CATEGORY_CODE=true";
	return setting;
}
function useHistory(){
	$("#useHistory_popup_button").trigger('click');
}
</script>
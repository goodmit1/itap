<%@page import="com.fliconz.fm.security.FMSecurityContextHelper"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<%
String url="/clovirsm/monitor/host/index.jsp";
if( FMSecurityContextHelper.checkMethodPermissionByURL("update",url)){
	url="#none";
}
pageContext.setAttribute("url", url);
	%>
		<!-- Host 상태 -->
		<div class="panel panel-default chart_area "  >
			<div class="panel-heading"><a href="${url}">Host <spring:message code="monitor_perf" text="성능"/></a></div>
			<div class="panel-body" style="position:relative">
				<div id="host_dc_area" class="dc_area hover">
				</div>
		 		<div id="monitor_host"  class="chart-width-subtitle"></div>
			</div>
		</div>


		<script>
			function getHostMonitorInfo(DC_ID)
			{
				$.get('/api/monitor/list/list_NC_MONITOR_HOST/?DC_ID=' + DC_ID, function(data)
				{
					drawHostMonitorChart(data, DC_ID)
				});
			}
			function drawHostMonitorChart(list, DC_ID) {


				if(!list) return;

				var data = [];
				var green_cnt = 0;
				var yellow_cnt = 0;
				var red_cnt = 0;

				for(var i=0; i< list.length; i++)
				{
					if(list[i].OVERALLSTATUS == "RED")
					{
						red_cnt++;
					}
					else if(list[i].OVERALLSTATUS == "YELLOW")
					{
						yellow_cnt++;
					}
					else
					{
						green_cnt++;
					}
					data.push(  { z:10, x: nvl(list[i].CPU_USAGE,0), y: nvl(list[i].MEM_USAGE,0),  name: list[i].NAME, msg: list[i].ALARM_NM, color: list[i].OVERALLSTATUS==null? null:convertColor(list[i].OVERALLSTATUS) } )	;
				}
				//data.push(  { x: 30, y: 40,  name: "test"} );
				var tooltip={
					    useHTML: true,
					    headerFormat: '<table>',
					    pointFormat: '<tr><th colspan="2"><h3>{point.name}</h3></th></tr>' +
					      '<tr><th>CPU:</th><td>{point.x}%</td></tr>' +
					      '<tr><th>MEMORY:</th><td>{point.y}%</td></tr>' +
					      '<tr><th colspan=2 >{point.msg}</td></tr>',
					    footerFormat: '</table>',
					    followPointer: true
					  } ;
				var config = bubbleChartConfig( '', 'cpu(%)', 20, 'mem(%)', 20, data,'')
				config.plotOptions.bubble={
					cursor:"pointer",
					minSize: 1,
					maxSize:20,
					events:{
						click:function(e)
						{

							 
							location.href="/clovirsm/monitor/host/index.jsp?HOST=" + e.point.name + "&DC_ID=" + DC_ID;
						}

					}
				}
				config.subtitle = { useHTML : true, align:'right', text :   '<span class="vm_num red" title="<spring:message code="STATE_RED" text="" />">' + red_cnt + '</span><span class="vm_num yellow" title="<spring:message code="STATE_YELLOW" text="경고" />">' + yellow_cnt + '</span><span class="vm_num green" title="<spring:message code="STATE_GREEN" text="정상" />">' + green_cnt + '</span>' };

				config.tooltip=tooltip;
				config.xAxis.max=105;
				config.yAxis.max=105;
				//config.yAxis.min=0;
				//config.xAxis.min=0;
				 
				Highcharts.chart('monitor_host', config);
			}
			chgDcCallback.host_dc_area = "getHostMonitorInfo";
			$(document).ready(function(){
			})

			</script>

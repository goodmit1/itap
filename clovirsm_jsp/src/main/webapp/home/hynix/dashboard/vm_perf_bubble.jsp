<%@page import="com.fliconz.fm.security.FMSecurityContextHelper"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<%
String url="/clovirsm/monitor/vm/index.jsp";
if( FMSecurityContextHelper.checkMethodPermissionByURL("update",url)){
	url="#none";
}
pageContext.setAttribute("url", url);
	%>
		<!-- VM 상태 -->
		<div class="panel panel-default chart_area "  >
			<div class="panel-heading"><a href="${url}">VM <spring:message code="monitor_perf" text="성능"/></a></div>
			<div class="panel-body" style="position:relative">

					 
			 		<div id="monitor_vm"  class="chart-width-subtitle"></div>

			</div>
		</div>
		<script>
			 
			function getVMMonitorInfo( )
			{
				$.get('/api/monitor/list/list_ALARM_VM/', function(data)
				{
					drawVMMonitorChart(data)
				});
			}

			 
			function drawVMMonitorChart(list) {

				var data = [];
				var green_cnt = 0;
				var yellow_cnt = 0;
				var red_cnt = 0;

				for(var i=0; list != null && i < list.length; i++)
				{
					if(list[i].CATEGORY == "RED")
					{
						red_cnt++;
					}
					else if(list[i].CATEGORY == "YELLOW")
					{
						yellow_cnt++;
					}
					else
					{
						green_cnt++;
					}
					data.push(  { DC_ID:list[i].DC_ID, DC_NM:list[i].DC_NM, ALARM_NM:list[i].ALARM_NM, id: list[i].VM_ID, x: nvl(list[i].CPU_USAGE,0), y: nvl(list[i].MEM_USAGE,0), z: nvl(list[i].DISK_USAGE,0), name: list[i].VM_NM, color:   convertColor(list[i].CATEGORY )} )	;
				}

				var tooltip={
					    useHTML: true,
					    headerFormat: '<table>',
					    pointFormat: '<tr><th colspan="2"><h3>{point.DC_NM} {point.name}</h3></th></tr>' +
					      '<tr><th>CPU:</th><td>{point.x}%</td></tr>' +
					      '<tr><th>MEMORY:</th><td>{point.y}%</td></tr>' +
					      '<tr><th>DISK:</th><td>{point.z}%</td></tr>' +
					      '<tr><td colspan=2>{point.ALARM_NM}</td></tr>',
					    footerFormat: '</table>',
					    followPointer: true
					  } ;

				var config = bubbleChartConfig( '', 'cpu(%)', 50, 'mem(%)', 50, data,'')
				config.subtitle = { useHTML : true, align:'right', text :   '<span class="vm_num red" title="<spring:message code="STATE_RED" text="위험" />">' + red_cnt + '</span><span class="vm_num yellow" title="<spring:message code="STATE_YELLOW" text="경고" />">' + yellow_cnt + '</span>' };
				config.plotOptions.bubble={
						cursor:"pointer",
						minSize: 1,
						maxSize:20,
						events:{
							click:function(e)
							{

								location.href="/clovirsm/monitor/admin_vm/index.jsp?DC_ID=" + e.point.DC_ID +"&keyword=" + e.point.name
							}

						}
					}
				config.tooltip=tooltip;
				//config.yAxis.min=0;
				//config.xAxis.min=0;
				config.xAxis.max=105;
				config.yAxis.max=105;
				 
				 
				vm_chart = Highcharts.chart('monitor_vm', config);
			}
			var vm_chart
			$(document).ready(function(){

				 
					getVMMonitorInfo( );
					//refCallback.push({callback: "getVMMonitorInfo", param: ''});
				 

			})

		</script>

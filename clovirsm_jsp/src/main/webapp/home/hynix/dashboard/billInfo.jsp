<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:p="http://primefaces.org/ui"
	xmlns:pe="http://primefaces.org/ui/extensions">
	<h:head>

	</h:head>
	<h:body>
		<ui:composition>
			<p:remoteCommand name="getBillingData"
				actionListener="#{dashboardView.getMMPay()}"
				oncomplete="fillPayData(xhr, status, args)" process="@this"></p:remoteCommand>
			<div class="dashboard_chart_title">
									#{msg.dashboard_title_billing} <input type="button" value="#{msg.dashboard_manual}" /> <input
										type="button"
										onclick="location.href='/clovirsm/myinfo/inquiry/inquiry.xhtml'"
										value="#{msg.dashboard_inquiry}" />
								</div>
								<div id="container_mmpay"></div>
			<script>
			$(document).ready(function() {
				if( $("#container_mmpay").length > 0){
					getBillingData();
					refCallback.push("getBillingData");
				}
			});
			function fillPayData(xhr, status, args) {
				Highcharts.theme = {

					chart : {
						backgroundColor : "#403d5d",

					},
					xAxis : {
						labels : {
							style : {
								fontSize : '13px',
								color : '#ffffff'

							}
						}
					},
					yAxis : {
						labels : {
							style : {
								fontSize : '13px',
								color : '#ffffff'

							}
						}
					}
				};
				Highcharts.setOptions(Highcharts.theme);
				var dataMap = new Array();
				var temp = new Object();
				temp.mmpay = args.LIST;
				dataMap.push(temp);
				makeLineChart("mmpay", makeMap(dataMap, "YYYYMM", "USE_FEE"),
						'column', '#74e0ff')
			}
			</script>
		</ui:composition>
	</h:body>
</html>

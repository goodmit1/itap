<%@page import="com.fliconz.fm.security.FMSecurityContextHelper"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%
String url="/clovirsm/monitor/alarm/index.jsp";
if( FMSecurityContextHelper.checkMethodPermissionByURL("update",url)){
	url="#none";
}
pageContext.setAttribute("url", url);
	%>
<div class="panel panel-default chart_area " id="alarmList">
	<div class="panel-heading"><a href="${url}"><spring:message code="title_list_alarm" text="" /></a></div>
	<div class="panel-body">
		 
			<div id="alarm_dc_area" class="dc_area">
			</div>
		 
		<div class="dashboard_list" style=" height: 300px; overflow-y: auto;">
			<table>
			<colgroup>
				<col width="10%" />
				<col width="25%" />
				<col width="35%" />
				<col width="10%" />
				<col width="20%" />
				</colgroup>
			  <thead>
			    <tr>
			      <th><spring:message code="warnings_list_object"/></th>
			      <th><spring:message code="warnings_list_kinds"/></th>
			      <th><spring:message code="warnings_list_message"/></th>
			      <th><spring:message code="warnings_list_status"/></th>
			      <th><spring:message code="warnings_list_occtime"/></th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr  v-for="alarm in alarmList">
			      <td>{{alarm.TARGET_TYPE}}</td>
			      <td class="ClickTarget" :title="alarm.ALARM_NM + '/' + alarm.TARGET"
					:svcnm="alarm.ALARM_NM" :target="alarm.TARGET"
					:targettype="alarm.TARGET_TYPE" :dc="alarm.DC_ID">{{alarm.TARGET}}</td>
			      <td>{{alarm.ALARM_NM}}</td>
			      <td><img class="alarm_img" :src="'/res/img/' + alarm.CATEGORY + '-icon.png'"  ></img></td>
			      <td>

			          <span class="dashboard_noti_date">{{ alarm.CRE_TIME  |date('datetime')}}</span>
			      </td>

			    </tr>
			  </tfoot>
			</table>
		</div>
	</div>
</div>
<style>
.dashboard_list .dashboard_list_nm.ellipsis {
	cursor: pointer;
}
</style>
<script>
	var alarmList = new Vue({
		el: '#alarmList',
		data: {
			alarmList: []
		},
		methods: {
			getReqInfo: function (DC_ID) {
				var that = this;
				$.get('/api/monitor/list/list_ALARM/?READ_YN=N&DC_ID=' + DC_ID, function(data){
					that.alarmList= data;
					setTimeout(function(){
						$(".dashboard_list .ClickTarget").click(function(){
							var type = $(this).attr("targettype");
							var target = $(this).attr("target");
							var svcnm = $(this).attr("svcnm");
							var dc = $(this).attr("dc");
							 
							switch (type) {
								case "DS":
									location.href = "/clovirsm/monitor/ds/index.jsp?NAME=" + target + "&DC_ID=" + dc;
									break;
								case "VM":
									location.href = "/clovirsm/monitor/admin_vm/index.jsp?keyword=" + target+ "&DC_ID=" + dc;
									break;
								case "HOST":
									location.href = "/clovirsm/monitor/host/index.jsp?NAME=" + target + "&DC_ID=" + dc;
									break;
							}
						})}, 100);
				});
			}
		}
	});

	 
		chgDcCallback.alarm_dc_area = "alarmList.getReqInfo";
	$(document).ready(function(){
		 
			refCallback.push({callback: "alarmList.getReqInfo", param: ''});
		 
	});
</script>

<%@page import="com.fliconz.fm.mvc.service.DashboardMngService"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page import="com.clovirsm.service.monitor.MonitorService"%>
<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page import="com.clovirsm.service.workflow.NextApproverService"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>

<%
	DashboardMngService service = (DashboardMngService)SpringBeanUtil.getBean("dashboardMngService");

	MonitorService monitorService = (MonitorService)SpringBeanUtil.getBean("monitorService");
	
	NextApproverService nextApproverService = (NextApproverService)SpringBeanUtil.getBean("nextApproverService");

	Map param=new HashMap();
	param.put("_IS_ADMIN_", monitorService.isAdmin() ? "Y" : "N");
	session.setAttribute("ADMIN_YN", monitorService.isAdmin()?"Y":"N");
	session.setAttribute("APPROVER_YN", nextApproverService.isApprover()?"Y":"N");
	List<Map> list = (List<Map>)service.selectListByQueryKey("user_dashboard_item",param );
	request.setAttribute("list", list);

session.setAttribute("FW_SHOW", PropertyManager.getBoolean("clovirsm.display.fw", false));
session.setAttribute("IMG_SHOW", PropertyManager.getBoolean("clovirsm.display.img", false));
session.setAttribute("SNAPSHOT", PropertyManager.getBoolean("clovirsm.display.snapshot", false));
%>
<script>
function clickService(){
	location.href="/clovirsm/resources/vra/newVra/index.jsp"
}
function clickChange(){
	location.href="/clovirsm/resources/vm/changeSpec/index.jsp"
}
function clickDel(){
	location.href="/clovirsm/resources/vm/delVm/index.jsp"
}
function clickExprie(){
	location.href="/clovirsm/resources/extendPeriod/index.jsp"
}
function clickRecovery(){
	location.href="/clovirsm/resources/recovery/index.jsp"
}
$("#left_menu").hide();
$(".content-body").attr("style","width:1280px");
$(".main").attr("style","width:1280px");
</script>
<c:if test="${sessionScope.ADMIN_YN == 'Y'}">
<style>
.chart_area .panel-body {
    background-color: white;
    height: 374px;
    overflow: hidden;
}

</style>
<div class="dashboardBtnArea" style="padding: 5px 15px;">
		<fmtags:include page="btn_create.jsp" />
		<div class="selectDashboard"   style="display:none">
			<ul>
				<li v-for="item in items">
					<input type="checkbox" v-bind:id="item.value" v-bind:value="item.value" v-model="checkedNames">
					<label v-bind:for="item.value">{{ item.name }}</label>
				</li>
			</ul>
			<input type="button" id="cancelDashboardItems" style="float:right" value="<spring:message code="btn_cancel" text="취소"/>">
			<input type="button" id="saveDashboardItems" style="float:right" value="<spring:message code="btn_save" text="저장"/>">
		</div>
		<div class="Rounded-Rectangle-3" style="display: inline-block; ">
			<img id="openDashboardEdit" src="/res/img/icon_setting.png" style="width:30px;margin-left: 19px;float: right;">
		</div>
		<div class="Rounded-Rectangle-3" style="display: inline-block;margin-right: 5px ">
			<img id="dashboardRefresh" src="/res/img/dashboard/refresh-arrow.png" style="width: 16px;height: 16px;">
		</div>
	</div>
	<div class="dashboardArea" style="clear:both">
		<c:forEach items="${ list }" var="i">
			<div class="dashboardItem col-md-${ i.COL }" id="${ i.ITEM_ID }"  >
			<jsp:include page="${ i.ITEM_PATH }" >
				<jsp:param name="TITLE" value="${ i.ITEM_NM }"/>
			</jsp:include>
			</div>
		</c:forEach>
		 
	</div>
</c:if>
<c:if test="${sessionScope.ADMIN_YN == 'N'}">
<style>
.chart_area .panel-body {
    background-color: white;
    height: 137px;
    overflow: hidden;
}

</style>
	<div class="dashboardArea user" style="clear:both; width: 1260px;  margin: 0 auto;">
		<div class="dashboardTop">
			<div class="dashboardTopLeft">
				<div class="dashboardTopItem white" style="width: 491px; height: 378px; padding-left: 7px; ">
					<jsp:include page="slideNotice.jsp" />
				</div>
			</div>
			<div class="dashboardTopRight">
				<div class="dashboardTopItem white" style="width: 485px; padding: 0px; ">
					<jsp:include page="vm_cnt.jsp" />
				</div>
				<div class="dashboardTopItem white" style="     width: 236px; padding: 0px;  margin-right: 10px;">
					<jsp:include page="qna.jsp" />
				</div>
				<div class="dashboardTopItem white" style=" width: 485px; padding: 0px; ">
					<jsp:include page="req_status.jsp" />
				</div>
				<div class="dashboardTopItem white" style="     width: 236px; padding: 0px; margin-right: 10px;">
					<jsp:include page="notice.jsp" />
				</div>
			</div>
		</div>	
		<div class="dashboardMid" >
			<div class="dashboardMidItem plus" onclick="clickService();" style="margin-left: 7px;"><div><spring:message code="NEW_VM_TITLE_1" text="신규서비스"/></div></div>
			<div class="dashboardMidItem change" onclick="clickChange();"><div><spring:message code="NEW_VM_TITLE_3" text="자원변경"/></div></div>
			<div class="dashboardMidItem delete" onclick="clickDel();"><div><spring:message code="NEW_VM_TITLE_4" text="삭제 요청"/></div></div>
			<div class="dashboardMidItem expire" onclick="clickExprie();"><div><spring:message code="NEW_VM_TITLE_5" text="기간연장 요청"/></div></div>
			<div class="dashboardMidItem recovery" onclick="clickRecovery();"><div><spring:message code="title_server_reuse" text="서버복원"/></div></div>
		</div>
		<div class="dashboardBottom" >
			<div class="dashboardBottomItem" style=" width: 98%; margin: 0; margin-left: 7px;">
				<jsp:include page="expire.jsp" />
			</div>
		</div>
		
	</div>
</c:if>
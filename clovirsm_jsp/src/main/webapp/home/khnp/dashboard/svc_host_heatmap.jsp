<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>

	<script type="text/javascript" src="/res/js/treemap.js"></script>
 
		<!-- Host 상태 -->
		<div class="panel panel-default chart_area "  >
			<div class="panel-heading"><a href="/clovirsm/monitor/svchostcnt2/index.jsp"  >호스트 업무 중복 배포</a></div>
			<div class="panel-body" style="position:relative">
				 
		 		<div id="monitor_svc_host_count"  class="chart-width-subtitle" style="height:400px"></div>
			</div>
		</div>


		<script>
		<jsp:include page="/clovirsm/monitor/svchostcnt2/_common.jsp"></jsp:include>
		function getSVCHostMonitorInfo()
		{
			 
				
				$.get('/api/monitor/list/list_svc_count_by_tag/?' + getTagUrlParam() + 'CNT=1'  , function(data1)
				{
					 drawSVCHostChart(data1);
				});
			 
			
		}
		function  drawSVCHostChart(data){
			var chart_data=[];
			for(var i=0; i < data.length; i++){
				 
				var data1 = $.extend(data[i],{name:getTagKey(data[i]), value:data[i].VM_CNT, color:getAreaColor(data[i].MAX_VM_CNT)});
				chart_data.push(data1);
				 
			}
			 
			Highcharts.chart('monitor_svc_host_count', {
				   legend: {
				        enabled: false 
				    },

				    tooltip: {
				        formatter: function () {
				            return '<spring:message code="TASK"/> : ' + getTagTaskNm(this.point ) + ' <br>' +
				           '중복 : ' + this.point.MAX_VM_CNT + ' <br>' ;
				          
				        }
				    },
				    series: [{
				        type: 'treemap',
				        layoutAlgorithm: 'squarified',
				        data:  chart_data,
				        dataLabels: {
				            enabled: true,
				            color: '#000000',
				            style: {
			                    textOutline: false 
			                }
				        }
				    }],
				    title: {
				        text: ''
				    },
				    plotOptions: {
				    	series: {
		                	events: {
		                    	click: function (e) {
		                    		location.href="/clovirsm/monitor/svchostcnt2/index.jsp?DC_ID=" + e.point.DC_ID
		                    	}
				    		}
				    	}
				    }
			});
		}
			$(document).ready(function(){
				getSVCHostMonitorInfo();
				refCallback.push("getSVCHostMonitorInfo");
			})
			 

			</script>

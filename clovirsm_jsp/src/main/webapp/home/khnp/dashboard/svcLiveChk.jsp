<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%
	String currentSvr = System.getProperty("serverName");
	pageContext.setAttribute("currentSvr", currentSvr);
%>
<div class="panel panel-default chart_area " id="liveList">
	<div class="panel-heading">
		<div style="display: inline-block; padding-left:5px;"><spring:message code="" text="서비스 Live 상태" /></div> 
		<div style="display: inline-block; margin-left: 20px;">
			<div style="display: inline-block; width: 10px; height: 10px; background-color: #48bdb6; border-radius: 50px; margin-right: 8px; margin-left: 8px;"></div><div style="display: inline-block;font-size: 10px;position: relative;top: -1px;">Service On</div>
			<div style="display: inline-block; width: 10px; height: 10px; background-color: #e27066; border-radius: 50px; margin-right: 8px; margin-left: 8px;"></div><div style="display: inline-block;font-size: 10px;position: relative;top: -1px;">Service Off</div>
		</div>
	</div>
	<div class="panel-body">
		<div id="dcList">
			
		</div>
	</div>
</div>
<style>
</style>
<script>
	function dcList() {
				var liveList = new Object(); 
				$.get('/api/monitor/list/list_NC_SVC_LIVE_CHK/', function(data) {
							for(var i = 0 ; i < data.length ; i++){
								if(liveList[data[i].CONN_TYPE]){
									if(liveList[data[i].CONN_TYPE][0].CONN_TYPE == data[i].CONN_TYPE)
										liveList[data[i].CONN_TYPE].push(data[i]);
								} else{
									var liveObjectList = new Array();
									liveObjectList.push(data[i]);
									liveList[data[i].CONN_TYPE] = liveObjectList;
								}
							}
							drawDcList(liveList);
						});
			
			}
	function drawDcList(object){
		
		var html="";
		for(list in object){
			html += "<div class='dcObject'> <div class='dcObjectTitle'>"+list+"</div>";
			for(var z = 0 ;  z < object[list].length; z++){
				var name = '${currentSvr}' == object[list][z].CONN_NM ? object[list][z].CONN_NM + '<div>(me)</div>' : object[list][z].CONN_NM ;
				if(object[list][z].LIVE_YN === 'Y')
					html += "<div class='liveOn'><a href='javascript:openUrl(\""+object[list][z].URL+"\")'>"+name+"</a></div>"
				else
					html += "<div class='liveOff'><a href='javascript:openUrl(\""+object[list][z].URL+"\")'> "+name+"</a></div>";
					
			}
			html += "</div>"
		}
		$("#dcList").append(html);
	}
	function openUrl(url){
		window.open(
		         url,
		         "_blank",
		         "toolbar=yes,scrollbars=yes,resizable=yes"
		       );
	}
	$(document).ready(function() {
		dcList();
	});
</script>

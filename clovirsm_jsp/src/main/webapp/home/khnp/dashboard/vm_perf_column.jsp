<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>

		<!-- VM 상태 -->
		<div class="panel panel-default chart_area "  >
			<div class="panel-heading"><a href="/clovirsm/monitor/vm/index.jsp">VM<spring:message code="monitor_perf" text="성능"/></a></div>
			<div class="panel-body" style="position:relative">

					<div id="team_area"></div>
			 		<div id="monitor_vm"  class="chart-width-subtitle"></div>

			</div>
		</div>
		<script>
			function getAllTeam()
			{
				$.get('/api/etc/team_list', function(list){
					var teamSelect = '';
					if(list)
					{
						teamSelect = '<select onchange="chgTeam(this)">';
						for( var key in list )
						{
							teamSelect += '<option value="' + key + '">' + list[key] + '</option>';
						}
						teamSelect += '</select>';
						$("#team_area").html(teamSelect);
						getVMMonitorInfo($("#team_area option:first").val());

					}
				})
			}
			function getVMMonitorInfo(teamCd)
			{
				$.get('/api/monitor/list/list_NC_MONITOR_VM/?TEAM_CD=' + teamCd, function(data)
				{
					drawVMMonitorChart(data)
				});
			}

			function chgTeam(sel)
			{

				getVMMonitorInfo($(sel).val());
			}
			function drawVMMonitorChart(list) {

				var data = [];
				var green_cnt = 0;
				var yellow_cnt = 0;
				var red_cnt = 0;

				for(var i=0; list != null && i < list.length; i++)
				{
					if(list[i].CATEGORY == "RED")
					{
						red_cnt++;
					}
					else if(list[i].CATEGORY == "YELLOW")
					{
						yellow_cnt++;
					}
					else
					{
						green_cnt++;
					}
				}

					data.push( {
	                    name: "RED",
	                    y: red_cnt,
	                    color : convertColor('RED')
	                },{
	                    name: "YELLOW",
	                    y: yellow_cnt,
	                    color : convertColor('YELLOW')
	                },{
	                    name: "GREEN",
	                    y: green_cnt,
	                    color : convertColor('GREEN')
	                } )	;
				 
				 

				var tooltip={
					    useHTML: true,
					    headerFormat: '<table>',
					    pointFormat: '<tr><th colspan="2"><h3>{point.name}</h3></th></tr>' +
					      '<tr><th>Count:</th><td>{point.y}</td></tr>',
					    footerFormat: '</table>',
					    followPointer: true
					  } ;

				var config = columnChartConfig( ' ', ' ', ' ', data);
				config.subtitle = { useHTML : true, align:'right', text :   '<span class="vm_num red">' + red_cnt + '</span><span class="vm_num yellow">' + yellow_cnt + '</span><span class="vm_num green">' + green_cnt + '</span>' };
				config.plotOptions.column={
						cursor:"pointer",
						events:{
							click:function(e)
							{

								location.href="/clovirsm/monitor/vm/index.jsp";
							}

						}
					}
				config.tooltip=tooltip;
				config.yAxis.min=0;
				config.xAxis.min=0;

				vm_chart = Highcharts.chart('monitor_vm', config);
			}
			var vm_chart
			$(document).ready(function(){

				if(ADMIN_YN=='Y'){
					getAllTeam();
					refCallback.push("getAllTeam");
				} else {
					getVMMonitorInfo('');
					refCallback.push({callback: "getVMMonitorInfo", param: ''});
				}

			})

		</script>

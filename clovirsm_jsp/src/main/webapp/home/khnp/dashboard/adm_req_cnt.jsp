<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<div class="panel panel-default chart_area " id="req_cnt_area">
	<div class="panel-heading"><spring:message code="label_req_info"/></div>
	<div class="panel-body panel-sm nopadding">
		<div class="sm_panel_content" style="display:flex; padding:0px;margin:0px; position:inherit;height: 100%;">
			<div class="panel_fit color_area blue_area" id="adm_C_CNT_area" style="border-radius:0px;">
				<img src="/res/img/hynix/newHynix/dashboard/shape-4223.png"  />
				<div class="sm_panel_title"><spring:message code="btn_new_req" text="신규요청"/></div>
				<hr>
				<a href="/clovirsm/workflow/approval/index.jsp"><div class="panel_value"  >{{count_data.C_CNT}}</div></a>
			</div>
			<div class="panel_fit color_area orange_area" id="adm_U_CNT_area" style="border-radius:0px;">
				<img src="/res/img/hynix/newHynix/dashboard/shape-1695.png"/>
				<div class="sm_panel_title"><spring:message code="label_change_req" text="변경요청"/></div>
				<hr>
				<a href="/clovirsm/workflow/approval/index.jsp"><div class="panel_value"  >{{count_data.U_CNT}}</div></a>
			</div>
			<div class="panel_fit color_area red_area" id="adm_D_CNT_area" style="border-radius:0px;">
				<img src="/res/img/hynix/newHynix/dashboard/shape-4222.png"  />
				<div class="sm_panel_title"><spring:message code="label_collect_req" text="회수요청"/></div>
				<hr>
				<a href="/clovirsm/workflow/approval/index.jsp"><div class="panel_value"  >{{count_data.D_CNT}}</div></a>
			</div>
		</div>
	</div>
</div>
<script>
var req_cnt_area = new Vue({
	el: '#req_cnt_area',
	data: {
		count_data: {C_CNT:0, U_CNT:0, R_CNT:0, D_CNT:0}
	},
	methods: {
		getReqInfo: function () {
			var that = this;
			$.get('/api/monitor/list/list_ADM_CNT/', function(data) {
					that.count_data= data[0];
				}
			)
		}
	}
});
$(document).ready(function(){
	req_cnt_area.getReqInfo();
	refCallback.push("req_cnt_area.getReqInfo");
})
</script>


<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.clovirsm.service.workflow.NextApproverService"%>
<!--  접근 목록 조회 -->
<style>
	#mainTable { height:calc(100vh - 220px);}
	#INS_TMS_TO{
		width: 100px;
	}
	#INS_TMS_FROM{
		width: 100px;
	}
	<style>
</style>
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm" style="margin: 0px; padding-left: 15px;">
			<fm-date-from-to id="INS_TMS"   name="INS_TMS" title="<spring:message code="" text="신청일자"/>"  ></fm-date-from-to>
		</div>
		<div class="col col-sm">
			<fm-input id="S_SVC_NM" name="SVC_NM" title="<spring:message code="" text="신청" />" input_style="width: 175px;"></fm-input>
		</div>
		<c:if test="${ADMIN_YN == 'Y'}">
		<div class="col col-sm">
			<fm-input id="S_USER_NAME" name="USER_NAME" title="<spring:message code="" text="요청자" />"></fm-input>
		</div>
		</c:if>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=SVCCUD" id="S_SVCCUD" emptystr="<spring:message code="label_all" text="" />"
                  name="SVCCUD" title="<spring:message code="application_classification" text="신청구분"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=APP_KIND" id="S_APPR_STATUS_CD" emptystr="<spring:message code="label_all" text="" />"
                  name="APPR_STATUS_CD" title="<spring:message code="" text="신청상태"/>">
			</fm-select>
		</div>
<!-- 		<div class="col col-sm"> -->
<!-- 			<fm-select url="/api/code_list?grp=TASK_STATUS_CD" id="S_TASK_STATUS_CD" emptystr=" " -->
<%--                   name="TASK_STATUS_CD" title="<spring:message code="deploy_status" text="배포 상태"/>"> --%>
<!-- 			</fm-select> -->
<!-- 		</div> -->
		<div class="col btn_group nomargin">
			<fm-popup-button popupid="CC_popup" style="display:none" popup="detail_popup.jsp" cmd="update" param="approvalReqParam"><spring:message code="tab_approval_request" text="결재 요청서"/></fm-popup-button>
			<fm-popup-button popupid="ZC_popup" style="display:none" popup="/clovirsm/workflow/templateReq/detail_popup.jsp" cmd="update" param="approvalReqParam"><spring:message code="tab_approval_request" text="결재 요청서"/></fm-popup-button>
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
	<%-- 	<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		<fm-sbutton id="deleteBtn" cmd="delete" class="delBtn contentsBtn tabBtnImg del " onclick="${popupid}_req_cancel()"   ><spring:message code="" text="신청취소"/></fm-sbutton>
		</div>
		
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 550px;" ></div>
	</div>
	</div>
<%-- 	<jsp:include page="../../popup/vra_detail_form_popup.jsp?action=view"></jsp:include> --%>
</div>
<script>
	var req = new Req();
	var mainTable;
	var approvalReqParam = null;

	$(function() {
		var
		columnDefs = [
			{
				hide: true,
				width: 150,
				field : "SVC_ID" 
			},{
				headerName : "<spring:message code="" text="신청" />",
				width: 200,
				field : "SVC_NM",
				cellRenderer : function(params){
					console.log(params.data);
					if(params.data)
						return params.data.SVC_NM;
					else
						return '';
					
				}
			},{
				headerName : "<spring:message code="" text="신청일자"/>",
				width: 180,
				field : "INS_TMS",
				format:'datetime'
			},{
				headerName : "<spring:message code="application_classification" text="신청 구분" />",
				width: 150,
				field : "SVCCUD_NM" 
			},{
				headerName : "<spring:message code="" text="신청상태" />",
				width: 100,
				field : "APPR_STATUS_CD_NM",
				tooltipField:"APPR_COMMENT"
			},{
				headerName : "<spring:message code="" text="결재자" />",
				width: 100,
				field : "STEP_NM",
			},
			{
				headerName : "<spring:message code="deploy_status" text="배포 상태" />",
				width: 150,
				field : "TASK_STATUS_CD_NM",
				cellRenderer: function(params) {
					if(params.data){
						if(params.data.APPR_STATUS_CD == 'A'){
							var date = new Date(params.data.APPR_TMS);
							
							if(params.data.TASK_STATUS_CD == 'S'){
								return params.data.TASK_STATUS_CD_NM +" "+ formatDatePattern(date,'yyyy-MM-dd HH:mm');
							} else if(params.data.TASK_STATUS_CD == 'F'){
								return '<sapn style="color:#C00000">'+  params.data.TASK_STATUS_CD_NM +" "+ formatDatePattern(date,'yyyy-MM-dd HH:mm') +'</span>';
							} else
								return params.data.TASK_STATUS_CD_NM ;
						} else
							return params.data.TASK_STATUS_CD_NM ? params.data.TASK_STATUS_CD_NM : 'N/A';
					}
				}
			}
		];
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowModelType: 'infinite',
			rowSelection : 'single',
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onRowClicked: function(){
				var arr = mainTable.getSelectedRows();
				if(arr.length > 0){
			    	approvalReqParam = arr[0];
			    	if(approvalReqParam.SVC_CD=='Z'){
			    		approvalReqParam.TEMPLATE_ID=approvalReqParam.SVC_ID;
			    		$('#ZC_popup_button').trigger('click');
			    	}
			    	else{
			    		$('#CC_popup_button').trigger('click');
			    	}	
			    	
			    	
			    	if(arr[0].TASK_STATUS_CD == 'S' || arr[0].APPR_STATUS_CD == 'D' || arr[0].APPR_STATUS_CD == 'A'){
			    		$("#deleteBtn").hide();
			    	} else{
			    		$("#deleteBtn").show();
			    	}
				}
			}
		}
		mainTable = newGrid("mainTable", gridOptions);
// 		var today = formatDate(new Date(), 'date');
// 		req.setSearchData({INS_TMS_TO: today, INS_TMS_FROM: today});
		search();
	});
	function initFormData(){
 
		var now = new Date();
		var firstDate, lastDate;
		 
		firstDate = new Date(now.getFullYear(), now.getMonth(), 1);
		lastDate = new Date(now.getFullYear(), now.getMonth()+1, 0);
		search_data.INS_TMS_FROM = formatDate(firstDate,'date');
		search_data.INS_TMS_TO = formatDate(lastDate,'date');
	}
	 
 
	function search() {
 
		req.searchPaging('/api/resource/req_history/list', mainTable, function(data) {
			removeOptionByVal("S_APPR_STATUS_CD","C")
		});
	}
  
	// 엑셀 내보내기
	function exportExcel() {
		mainTable.exportCSV({fileName:'Application Status List'});
	}
	
	function ${popupid}_req_cancel(){
		if(!confirm('<spring:message code="msg_approval_cancel_confirm"/>')) return;
			var arr = mainTable.getSelectedRows();
			var param = {
				REQ_ID: arr[0].REQ_ID,
				INS_ID: arr[0].INS_ID
			}
			post('/api/request_history/cancel', param, function(data){
				search();
			});
		}
</script>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  접근 목록 조회 -->
<style>
	#mainTable { height:calc(100vh - 220px); }
</style>
<script src="/res/js/lib/ace/ace.js"></script>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm">
			<fm-select url="/api/etc/team_list" id="S_TEAM_CD" emptystr=" " name="TEAM_CD" title="<spring:message code="FM_TEAM_TEAM_NM" text="" />"></fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_REQ_NM" name="REQ_NM" title="<spring:message code="label_title" text="결재정보" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_USER_NAME" name="USER_NAME" title="<spring:message code="FM_USER_USER_NAME" text="요청자" />"></fm-input>
		</div>
		<div class="col">
			<table>
				<tr>
					<td>
						<fm-date id="S_INS_TMS_FROM" name="INS_TMS_FROM" title="<spring:message code="NC_REQ_INS_TMS" text="요청일시"/>"></fm-date>
					</td>
					<td class="tilt">~</td>
					<td>
						<fm-date id="S_INS_TMS_TO" name="INS_TMS_TO" ></fm-date>
					</td>
				</tr>
			</table>
		</div>
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>

			<fm-popup-button style="display: none;" popupid="approval_req_form_popup" popup="approval_request_popup.jsp" cmd="update" param="approvalReqParam"><spring:message code="tab_approval_request" text="결재 요청서"/></fm-popup-button>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		<fm-sbutton cmd="update" class="btn contentsBtn tabBtnImg del" onclick="openReasonPopup('deny')"><spring:message code="btn_approval_deny" text=""/></fm-sbutton>
		<fm-sbutton cmd="update" class="btn contentsBtn tabBtnImg save" onclick="openReasonPopup('approval')"><spring:message code="btn_approval_accept" text=""/></fm-sbutton>
		<fm-sbutton id=jsonEditor cmd="update" class="btn contentsBtn tabBtnImg new hide" onclick="openReasonPopup('vra_jsonEditor')" v-if="ADMIN_YN == 'Y'"><spring:message code="" text="JSON 수정"/></fm-sbutton>
	
			<fm-popup-button style="display: none;" popupid="deny_popup" popup="/clovirsm/popup/reason_popup.jsp?title=btn_approval_deny" param="approvalReqParam" callback="denyMulti"></fm-popup-button>
			<fm-popup-button style="display: none;" popupid="approval_popup" popup="/clovirsm/popup/reason_popup.jsp?title=btn_approval_accept" param="approvalReqParam" callback="approveMulti"></fm-popup-button>
			<fm-popup-button style="display: none;" popupid="vra_jsonEditor_popup" popup="/clovirsm/popup/vra_jsonEditor_popup.jsp" param="approvalReqParam" callback="approveMulti"></fm-popup-button>
	
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 450px;" ></div>
	</div>
	</div>
</div>
<script>
	var req = new Req();
	mainTable;
	var approvalReqParam = null;

	function openReasonPopup(kubun){
		approvalReqParam = mainTable.getSelectedRows();
		if(approvalReqParam.length==0){
			alert(msg_select_first);
			return;
		}
		$("#" + kubun + "_popup_button").click();
	}
	$(function() {
		var
		columnDefs = [{width: 52, minWidth: 52,maxWidth: 52,   checkboxSelection: true, 
		    headerCheckboxSelection: true, headerCheckboxSelectionFilteredOnly:true },
			{
				headerName : "<spring:message code="NC_REQ_REQ_ID" text="No" />",
				maxWidth: 170,
				width: 170,
				field : "REQ_ID"
			},{
				headerName : "<spring:message code="label_title" text="결재정보" />",
				field : "REQ_NM",
				width: 980,
				maxWidth: 980,
				cellRenderer:function(params){
					 
					return '<a href="#" onclick="openDetail(' + params.rowIndex + ');return false">' + params.value + '</a>';
				}
			},{
				headerName : "<spring:message code="NC_REQ_CMT" text="설명" />",
				field : "CMT",
				width: 0,
				hide : true
			},{
				headerName : "<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />",
				maxWidth: 160,
				width: 160,
				field : "TEAM_NM"
			},{
				headerName : "<spring:message code="FM_USER_USER_NAME" text="요청자" />",
				maxWidth: 160,
				width: 160,
				field : "USER_NAME"
			},{
				headerName : "<spring:message code="INS_TMS" text="요청일시"/>",
				maxWidth: 180,
				width: 180,
				field : "INS_TMS",
				valueGetter: function(params) {
			    	return formatDate(params.data.INS_TMS,'datetime')
				}
			},{
				headerName : "<spring:message code="NC_REQ_APPR_STEP" text="단계" />",
				//maxWidth: 120,
				hide: true,
				width: 120,
				field : "STEP_NM"
			}
		];
		var
		gridOptions = {
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			rowSelection : 'multiple',
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onRowClicked: function(e){
				arr = mainTable.getSelectedRows();
				if(arr.length == 1){
					if(arr[0].APPR_STATUS_CD == 'R')
						$("#jsonEditor").removeClass("hide");
					else
						$("#jsonEditor").addClass("hide");
				} else{
					$("#jsonEditor").addClass("hide");
				}
			}
		}
		mainTable = newGrid("mainTable", gridOptions);
// 		var today = formatDate(new Date(), 'date');
// 		req.setSearchData({INS_TMS_TO: today, INS_TMS_FROM: today});
		search();
	});
	function approveMulti(cd, cmt){
		var param = {};
		param.APPR_COMMENT = cmt;
		param.APPR_CMT_CD = cd;
		param.selected_json= JSON.stringify(mainTable.getSelectedRows());
		post('/api/approval/approve_multi', param, function(data){
			if(data.err_list){
				alert(err_list);
			}
			search();
		})
	}
	function denyMulti(cd, cmt){
		var param = {};
		param.APPR_COMMENT = cmt;
		param.APPR_CMT_CD = cd;
		param.selected_json= JSON.stringify(mainTable.getSelectedRows());
		post('/api/approval/deny_multi', param, function(data){
			if(data.err_list){
				alert(err_list);
			}
			search();
		})
	}
	function openDetail(idx){
		approvalReqParam = mainTable.getData()[idx];
		$('#approval_req_form_popup_button').trigger('click');
	}
	// 조회
	function search() {
		req.search('/api/approval/list' , function(data) {
			mainTable.setData(data);
		});
	}

	// 엑셀 내보내기
	function exportExcel() {
		exportExcelServer("mainForm", '/api/approval/list_excel', 'Approval',mainTable.gridOptions.columnDefs, req.getRunSearchData())
	}
</script>
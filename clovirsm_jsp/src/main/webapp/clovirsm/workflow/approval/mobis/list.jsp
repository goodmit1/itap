<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<!--  접근 목록 조회 -->
<style>
	#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm">
			<fm-select url="/api/etc/team_list" id="S_TEAM_CD" emptystr=" " name="TEAM_CD" title="<spring:message code="FM_TEAM_TEAM_NM" text="" />"></fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_REQ_NM" name="REQ_NM" title="<spring:message code="NC_REQ_REQ_NM" text="제목"/>"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_USER_NAME" name="USER_NAME" title="<spring:message code="FM_USER_USER_NAME" text="요청자" />"></fm-input>
		</div>
		<div class="col">
			<table>
				<tr>
					<td>
						<fm-date id="S_INS_TMS_FROM" name="INS_TMS_FROM" title="<spring:message code="NC_REQ_INS_TMS" text="요청일시"/>"></fm-date>
					</td>
					<td class="tilt">~</td>
					<td>
						<fm-date id="S_INS_TMS_TO" name="INS_TMS_TO" ></fm-date>
					</td>
				</tr>
			</table>
		</div>
		<div class="col btn_group">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
			<fm-popup-button style="display: none;" popupid="approval_req_form_popup" popup="../approval/mobis/approval_request_popup.jsp" cmd="update" param="approvalReqParam"><spring:message code="tab_approval_request" text="결재 요청서"/></fm-popup-button>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title">
		<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div>
	</div>
	<div id="mainTable" class="ag-theme-fresh" style="height: 450px;" ></div>
</div>
<script>
	var req = new Req();
	mainTable;
	var approvalReqParam = null;

	$(function() {
		var
		columnDefs = [
			{
				headerName : "<spring:message code="NC_REQ_REQ_NM" text="제목"/>",
				field : "REQ_NM",
				width: 980,
				//maxWidth: 980,
				tooltipField:"REQ_NM",
			},{
				headerName : "<spring:message code="NC_REQ_CMT" text="설명" />",
				field : "CMT",
				/* width: 0, */
				hide : true
			},{
				headerName : "<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />",
				//maxWidth: 160,
				width: 160,
				field : "TEAM_NM",
				tooltipField:"TEAM_NM",
			},{
				headerName : "<spring:message code="FM_USER_USER_NAME" text="요청자" />",
				//maxWidth: 160,
				width: 160,
				field : "USER_NAME"
			},{
				headerName : "<spring:message code="INS_TMS" text="요청일시"/>",
				//maxWidth: 180,
				width: 180,
				field : "INS_TMS",
				valueGetter: function(params) {
					if(params.data && params.data.INS_TMS){
						return formatDate(params.data.INS_TMS,'datetime')
	          		  }
	      		  	return "";
				}
			}
		];
		var
		gridOptions = {
			hasNo : true,		
			columnDefs : columnDefs,
			rowModelType: 'infinite',
			sizeColumnsToFit: true,
			rowSelection : 'single',
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: true,
			onRowClicked: function(){
				var arr = mainTable.getSelectedRows();
				if(arr.length > 0){
			    	var nc_req = arr[0];
			    	approvalReqParam = nc_req;
			    	$('#approval_req_form_popup_button').trigger('click');
				}
			}
		}
		mainTable = newGrid("mainTable", gridOptions);
// 		var today = formatDate(new Date(), 'date');
// 		req.setSearchData({INS_TMS_TO: today, INS_TMS_FROM: today});
		search();
	});


	// 조회
	function search() {
		req.searchPaging('/api/approval/list' ,mainTable, function(data) {
			mainTable.setData(data);
		});
		/* req.search('/api/approval/list' , function(data) {
			mainTable.setData(data);
		}); */
	}

	// 엑셀 내보내기
	function exportExcel() {
		exportExcelServer("mainForm", '/api/approval/list_excel', 'Approval',mainTable.gridOptions.columnDefs, req.getRunSearchData())
	}
</script>
<%@page contentType="text/html; charset=UTF-8"%><%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }
	#UPD_TMS_FROM{
		width:100px;
	}
	#UPD_TMS_TO{
		width:100px;
	}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm-12" style="margin: 0px;padding-left: 15px;">
			<div class="col">
				<table>
					<tr>
						<td>
							<fm-date id="S_UPD_TMS_FROM" name="UPD_TMS_FROM" title="<spring:message code="working_date" text="작업일자" />"></fm-date>
						</td>
						<td class="tilt">~</td>
						<td>
							<fm-date id="S_UPD_TMS_TO" name="UPD_TMS_TO" ></fm-date>
						</td>
					</tr>
				</table>
			</div>
		</div>
		 <div class="col col-sm">
			<fm-select url="/api/code_list?grp=SVCCUD" id="S_SVCCUD"
			    emptystr="<spring:message code="label_all" text="" />"
				name="SVCCUD" title="<spring:message code="kubun" text="신청구분" />">
			</fm-select>
		</div>
<%--  		<jsp:include page="/clovirsm/popup/_FAB_serach_.jsp"></jsp:include> --%>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=TASK_STATUS_CD" id="S_TASK_STATUS_CD"
			    emptystr="<spring:message code="label_all" text="" />"
				name="TASK_STATUS_CD" title="<spring:message code="NC_VM_USER_TASK_STATUS_CD" text="작업상태"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_TITLE" name="TITLE" title="<spring:message code="title" text="제목" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_USER_NAME" name="USER_NAME" title="<spring:message code="REQ_USER" text="요청자" />"></fm-input>
		</div>
		<div class="col btn_group nomargin">
			<button type="button" class="btn searchBtn" onclick="search()"></button>
		</div>
 
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
			<spring:message code="mainTable_Search_information" text="검색정보" />
			<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
			<fm-sbutton cmd="update" class="sync  contentsBtn tabBtnImg" onclick="deploy()"><spring:message code="btn_redeploy" text="재배포" /></fm-sbutton>
			<fm-sbutton cmd="update" class="del  contentsBtn tabBtnImg" onclick="deployDelete()"><spring:message code="deploy_cancel" text="배포 취소" /></fm-sbutton>
			<fm-sbutton cmd="update" class="sync  contentsBtn tabBtnImg" onclick="reserve_deploy()" v-if="'${sessionScope.SITE_CODE}' != 'clovircm'"><spring:message code="btn_reserve_deploy" text="배포 예약" /></fm-sbutton>
			<fm-popup-button style="display: none;" popupid="deployFail_Cancel_popup" popup="/clovirsm/popup/reason_popup.jsp?title=deploy_cancel" param="mainTable.getSelectedRows()[0]" callback="deployDel"></fm-popup-button>
			<fm-popup-button popupid="CC_popup" style="display:none" popup="/clovirsm/workflow/requestItemsHistory/detail_popup.jsp" cmd="update" param="param"><spring:message code="tab_approval_request" text="결재 요청서"/></fm-popup-button>
			<fm-popup-button style="display: none;" popupid="reserve_popup" popup="/clovirsm/popup/deployFail_popup.jsp" param="mainTable.getSelectedRows()[0]"></fm-popup-button>
		</div>
	</div>	
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 550px;" ></div>
	</div>
	<br />
	 
</div>
<script>
	var failReq = new Req();
	mainTable;
	var param = new Object();
	$(function() {
		var columnDefs = [{headerName : "<spring:message code="kubun" text="신청구분" />", field : "SVCCUD_NM"  },
		                  {headerName : "<spring:message code="title" text="제목" />", field : "TITLE", tooltipField:'TITLE',cellRenderer:function(params){
		                	  return '<a hraf="#" onclick="deploFailClick(\''+params.data.SVC_CD+'\',\''+params.data.SVC_ID+'\',\''+params.data.INS_DT+'\');">'+params.data.TITLE+'</a>'
		                  }},
		                  {headerName : "<spring:message code="STATUS" text="상태" />", field : "FAIL_MSG",tooltipField:'FAIL_MSG', 
		                	  cellRenderer:function(params){
		                	  if(params.data.TASK_STATUS_CD == 'F'){	 
			                	  if(params.data.TASK_STATUS_CD_ORG=='W'){
			                		  return '배포중';
			                	  }
			                	  else{
			                		  console.log(params)
			                		  return "<a href='#' onclick='openLog(" + params.rowIndex + ");return false'>" + params.data.TASK_STATUS_CD_NM + ":" + params.data.FAIL_MSG + "</a>";
			                	  }
		                	  }
		                	  else{
		                		  return params.data.TASK_STATUS_CD_NM
		                	  }
		                  }},
		                  {headerName : "<spring:message code="" text="구분" />", field : "P_KUBUN_NM"},
		                  {headerName : "<spring:message code="" text="작업일자" />",format:'datetime', field : "DEPLOY_DT"},
		                  {headerName : "<spring:message code="UPD_ID_NM" text="작업자" />",  field : "UPD_ID_NM"},
		                  {
		          			headerName : '<spring:message code="" text="예정일자" />',
		          			field : 'RSV_DT',
		          			maxWidth: 150,
		          			width: 130,
		          			valueGetter:function(params){
		          				if(params.data.RSV_DT){
			          				var str = params.data.RSV_DT;
			          				return str.substring(0,4) +"-"+str.substring(4,6)+"-"+str.substring(6,8)+" "+str.substring(8)+":00";
		          				} else{
		          					return '';
		          				}
		          			}
		          		  },
		                  {headerName : "<spring:message code="REQ_TMS" text="" />", format:'datetime',field : "INS_TMS" },
		                  {headerName : "<spring:message code="REQ_USER" text="요청자" />",  field : "INS_ID_NM" },
		                 
		                  ]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowSelection : 'single',
			editable: false,
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onRowClicked : function() {
				var arr = mainTable.getSelectedRows();
				if(arr[0].TASK_STATUS_CD=='F'){
					$(".tabBtnImg").prop("disabled",false)
				}
				else{
					$(".tabBtnImg").prop("disabled",true)
				}
			}
		}
		$(".tabBtnImg").prop("disabled",true)
		mainTable = newGrid("mainTable", gridOptions);
		searchvue.form_data.TASK_STATUS_CD = "F";
 
		search();

	});
	 
	function initFormData(){
 
		var now = new Date();
		var firstDate, lastDate;
		 
		firstDate = new Date(now.getFullYear(), now.getMonth(), 1);
		lastDate = new Date(now.getFullYear(), now.getMonth()+1, 0);
		search_data.UPD_TMS_FROM = formatDate(firstDate,'date');
		search_data.UPD_TMS_TO = formatDate(lastDate,'date');
	}
 
	function openLog(idx){
		$("#log_popup").modal();
		showErrMsg(mainTable.getData()[idx].FAIL_MSG);
	}	
	// 조회
	function search() {
		failReq.search('/api/deploy_fail/list', function(data){mainTable.setData(data)});
	}

	function deployDel(cd, cmt){
		var param =mainTable.getSelectedRows()[0]
		param.ADMIN_CMT =cmt;
		param.ADMIN_CMT_CD =cd;
		post('/api/deploy_fail/delete',param,function(data){
			if(data){
				alert(msg_complete);
				search();
				 
			} else{
				alert(msg_jsp_error);
			}
		});
	}
	function reserve_deploy(){
		var arr = mainTable.getSelectedRows();
		if(arr.length==0)
		{
			alert(msg_select_first);
			return;
		}
		post('/api/deploy_fail/chkBeforeDeploy',  arr[0], function(data){
			if(data.last_chk_msg==null || confirm(data.last_chk_msg + '<spring:message code="msg_continue" text="계속진행하시겠습니까?" />')){
				$("#reserve_popup_button").trigger('click');
			}});
	} 
	function deployDelete(){
		$("#deployFail_Cancel_popup_button").trigger('click');
// 		post('/api/deploy_fail/delete', mainTable.getSelectedRows()[0], function(){
// 			alert('ok');
// 			search();
// 		})
	}
	
	function deploy()
	{
		var arr = mainTable.getSelectedRows();

		 
		if(arr.length==0)
		{
			alert(msg_select_first);
			return;
		}
		if(arr[0].TASK_STATUS_CD_ORG=='W'){
			alert('배포중입니다.');
			return ;
		}
		post('/api/deploy_fail/chkBeforeDeploy',  arr[0], function(data){
			if(data.last_chk_msg==null || confirm(data.last_chk_msg)){
				post('/api/deploy_fail/update_redeploy',  arr[0], function(data){
					alert(msg_complete);
					search();
				})
			}
		})
		
	}
	
	function deploFailClick(SVC_CD,SVC_ID,INS_DT){
		param.SVC_CD = SVC_CD;
		param.SVC_ID = SVC_ID;
		param.INS_DT = INS_DT;
		
		$('#CC_popup_button').trigger('click');
	}

	// 엑셀 내보내기
	function exportExcel() {
		mainTable.exportCSV({fileName:'DeployFail'})
		 // exportExcelServer("mainForm", '/api/deploy_fail/list_excel', 'vmuser',mainTable.gridOptions.columnDefs, failReq.getRunSearchData())
	}

</script>
<%@page import="com.clovirsm.service.ComponentService"%>
<%@page contentType="text/html; charset=UTF-8"%><%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<% request.setAttribute("DELETE_DAY", ComponentService.getEnv("expire.delete.day", "30")); %>
<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm">
			<fm-input id="S_TITLE" name="TITLE" title="<spring:message code="NC_VM_VM_NM" text="서버명"/>"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="IP" name="IP" title="<spring:message code="NC_FW_PUBLIC_IP" text="" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_USER_NAME" name="USER_NAME" title="<spring:message code="NC_REQ_INS_ID_NM" text="요청자" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="DELETE_DAY" class="DELETE_DAY" name="DELETE_DAY" title="<spring:message code="due_deleted" text="삭제예정일" />(D-Day)">
			</fm-input>
		</div>
		<div class="col btn_group nomargin">
			<button type="button" class="btn searchBtn" onclick="search()"><spring:message code="btn_search" text="" /></button>
		</div>
<!-- 		<div class="col btn_group_under"> -->
			<!-- <fm-sbutton cmd="update" class="exeBtn" onclick="createNewUser()"><spring:message code="btn_task_create_account" text="계정작업" /></fm-sbutton>
			 -->

<%-- 			<c:if test="${'Y'.equals(sessionScope.FW_API_YN)}"> --%>
<%-- 			<fm-sbutton cmd="update" class="newBtn" onclick="deployFW()"><spring:message code="btn_task_fw_auto" text="자동작업" /></fm-sbutton> --%>
<%-- 			</c:if> --%>
<%-- 			<fm-sbutton cmd="update" class="newBtn" onclick="complete()"><spring:message code="btn_task_yes" text="" /></fm-sbutton> --%>
<!-- 			<fm-popup-button  popupid="fw_vmuser_cancel_form_popup" -->
<!-- 				popup="/clovirsm/popup/fw_vmuser_cancel_form_popup.jsp" cmd="update" class="exeBtn" param="'message'" -->
<!-- 				callback=""> -->
<%-- 				<spring:message code="label_work_msg" text="작업메시지"/> --%>
<!-- 			</fm-popup-button> -->
<!-- 			<fm-popup-button  popupid="fw_vmuser_cancel_form_popup" -->
<!-- 				popup="/clovirsm/popup/fw_vmuser_cancel_form_popup.jsp" cmd="update" class="delBtn" param="'cancel'" -->
<!-- 				callback=""> -->
<%-- 				<spring:message code="btn_task_no" text="작업거부"/> --%>
<!-- 			</fm-popup-button> -->

<!-- 		</div> -->
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
			<spring:message code="mainTable_Search_information" text="검색정보" />
			<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>

			<fm-sbutton cmd="update" class="contentsBtn tabBtnImg del" onclick="del()"><spring:message code="delete_server" text="서버 삭제" /></fm-sbutton>
			<fm-sbutton cmd="update" class="sync  contentsBtn tabBtnImg" onclick="reuse()"><spring:message code="server_recovery" text="서버 복원" /></fm-sbutton>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 550px;" ></div>
		<fm-popup-button style="display:none;" popupid="reuse_popup" popup="/clovirsm/popup/vm_recovery_popup.jsp?isAppr=N" cmd="" param="form_data" callback="search"></fm-popup-button>
	</div>
	<br />

</div>
<script>
	var expireReq = new Req();
	mainTable;

	$(function() {
		var columnDefs = [
		                  {headerName : "<spring:message code="NC_VM_VM_NM" text="서버이름" />", width:150, field : "TITLE", tooltipField:'TITLE'},
		                  {headerName : "<spring:message code="TASK" text="업무" />", field : "CAYEGORY_NM", tooltipField:'CAYEGORY_NM'},
		                  {headerName : "<spring:message code="NC_FW_PUBLIC_IP" text="서버IP" />", field : "IP", width:120,  tooltipField:'IP'},
		                  {headerName : "<spring:message code="NC_VM_P_KUBUN" text="구분" />", field : "P_KUBUN_NM"},
		                  {headerName : "<spring:message code="NC_DC_DC_NM" text="데이터센터명" />", field : "DC_NM" },
		                  {headerName : "<spring:message code="EXPIRE_DT" text="만료일" />",format:'datetime', width:120, field : "EXPIRED_TMS"},
		                  {
		          			headerName : '<spring:message code="due_deletion" text="삭제 예정일"/>',
		          			field : 'DUE_DELETE',
		          			width: 120,
		          			minWidth: 110,
		          			valueGetter: function(params) {
		          				if(params.data) return formatDate(params.data.DUE_DELETE,'date')+" (D-"+ (params.data.DUE_DELETE_COUNT).toFixed(0)+")";
		          			}

		          		},
		                  {headerName : "<spring:message code="label_note" text="비고" />",  field : "FAIL_MSG", width:120,
		          				valueGetter: function(params) {
		          					return params.data.FAIL_MSG && params.data.FAIL_MSG.indexOf('EXPIRE')==0? '<spring:message code="use_expire" text="기간만료" />': '<spring:message code="delete_request" text="삭제요청" />'
		                	  	},
		                  },
		                  {headerName : "<spring:message code="NC_VM_REQ_USER" text="요청자" />",  field : "INS_ID_NM", width:120 },

		                  ]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowSelection : 'single',
			editable: false,
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onRowClicked: function(){
				var arr = mainTable.getSelectedRows();
				expireReq.getInfo("/api/vm/info?VM_ID=" + arr[0].SVC_ID)
		    }
		}
		mainTable = newGrid("mainTable", gridOptions);

		search();
	});

	// 조회
	function search() {
		expireReq.setSearchData("DAY","${DELETE_DAY}");
		expireReq.search('/api/expire/list', function(data){mainTable.setData(data)});
	}


	function del()
	{
		var data = new Object();
		var arr = mainTable.getSelectedRows();
		if(arr.length > 0){
			if(confirm(msg_confirm_delete)){
				post('/api/expire/vm_delete', arr[0], function(data){
					$('#${popupid}').modal('hide');
					search();
					if(${popupid}_callback != ''){
						eval(${popupid}_callback + '(${popupid}_vue.form_data);');
					}
				});
			}
		}
		else
			alert("<spring:message code="select_first" text="" />")
	}
	function reuse()
	{
		var arr = mainTable.getSelectedRows();
		if(arr.length > 0)
			$("#reuse_popup_button").click();
		else
			alert("<spring:message code="select_first" text="" />")
	}

	// 엑셀 내보내기
	function exportExcel() {
		mainTable.exportCSV({fileName:'expire'})
		  //exportExcelServer("mainForm", '/api/expire/list_excel', 'expire',mainTable.gridOptions.columnDefs, expireReq.getRunSearchData())
	}

	function initFormData()
	{
		<c:if test="${param.DELETE_DAY != null}">
			search_data.EXPIRE_DAY = "${param.DELETE_DAY}"
		</c:if>
	}
</script>
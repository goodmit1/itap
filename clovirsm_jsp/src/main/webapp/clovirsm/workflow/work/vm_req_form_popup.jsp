<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("isEditable", !"view".equals(action));
%>
<style>
	#${popupid} .modal-dialog {
		width: 1200px;
	}

</style>
<fm-modal id="${popupid}" title="<spring:message code="label_vm_req" text="" />">
	<span slot="footer">
		<input id="${popupid}_save_button" type="button" class="btn" value="<spring:message code="btn_save" text="" />" onclick="${popupid}_save()" v-if="${isEditable} && '${popupid}' != 'vm_delete_req_form_popup'" />
	</span>
	<ul class="nav nav-tabs">
		<li>
			<a href="#${popupid}Tab1" data-toggle="tab"><spring:message code="NC_VM_SPEC_NM" text="사양"/></a>
		</li>
		<li>
			<a href="#${popupid}Tab2" data-toggle="tab"><spring:message code="FM_USER_USER_NAME" text="사용자" /></a>
		</li>
	</ul>
	<div class="tab-content ">
		<div class="form-panel detail-panel tab-pane" id="${popupid}Tab1">
			<div class="col col-sm-12" v-if="${isEditable} && '${popupid}' != 'vm_delete_req_form_popup'">
				<fm-select url="/api/etc/spec_list" id="${popupid}_SPEC_ID" emptystr=" " name="SPEC_ID" title="<spring:message code="label_spec"/>" keyField="SPEC_ID" titleField="SPEC_NM" onchange="${popupid}_changeSpec" style="float:right;"></fm-select>
			</div>
			<form id="${popupid}_form" action="none">
				<input type="hidden" name="VM_ID" id="${popupid}_VM_ID"  />
				<div class="panel-body">
					<div class="col col-sm-12">
						<fm-output id="${popupid}_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="VM명"/>"></fm-output>
					</div>
					<div class="col col-sm-12">
						<fm-output id="${popupid}_OS_NM" name="OS_NM" title="<spring:message code="NC_VM_OS_NM" text="템플릿명"/>"></fm-output>
					</div>
					<div class="col col-sm-12" v-if="${isEditable} && '${popupid}' == 'vm_insert_req_form_popup'">
						<fm-select url="/api/code_list?grp=PURPOSE" id="${popupid}_PURPOSE" emptystr=" " name="PURPOSE" title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>"></fm-select>
					</div>
					<div class="col col-sm-12" v-if="${isEditable} && '${popupid}' == 'vm_insert_req_form_popup'">
						<fm-select id="${popupid}_CPU_CNT" name="CPU_CNT" :options="${popupid}_CPUOptions" style="width:100%;display:inline-block;" title="<spring:message code="NC_VM_CPU_CNT" text="CPU"/>" disabled="disabled"></fm-select>
					</div>
					<div class="col col-sm-12" v-if="${isEditable} && '${popupid}' == 'vm_insert_req_form_popup'">
						<fm-spin id="${popupid}_DISK_SIZE" name="DISK_SIZE" style="width:calc(100% - 80px);display:inline-block;" onchange="${popupid}_validationDisk()" min="1" title="<spring:message code="NC_VM_DISK_SIZE" text="디스크"/>"></fm-spin>
						<fm-select id="${popupid}_DISK_UNIT" name="DISK_UNIT" :options="diskUnitOptions" style="width:80px;display:inline-block;" onchange="${popupid}_validationDisk()"></fm-select>
					</div>
					<div class="col col-sm-12" v-if="${isEditable} && '${popupid}' == 'vm_insert_req_form_popup'">
						<fm-select id="${popupid}_RAM_SIZE" name="RAM_SIZE" :options="${popupid}_RAMOptions" style="width:100%;display:inline-block;" title="<spring:message code="NC_VM_RAM_SIZE" text="메모리"/>" disabled="disabled"></fm-select>
					</div>
					<div class="col col-sm-12" v-if="${!isEditable} || '${popupid}' != 'vm_insert_req_form_popup'">
						<fm-select url="/api/code_list?grp=PURPOSE" id="${popupid}F_PURPOSE" emptystr=" " name="PURPOSE" title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>" disabled="disabled"></fm-select>
					</div>
					<div class="col col-sm-12" v-if="${isEditable} && '${popupid}' == 'vm_update_req_form_popup'">
						<fm-output name="OLD_CPU_CNT" title="<spring:message code="NC_VM_CPU_CNT" text="CPU"/>" style="width:50%;display:inline-block;"></fm-output>
						<fm-select id="${popupid}_CPU_CNT" name="CPU_CNT" :options="${popupid}_CPUOptions" style="width:50%;display:inline-block;" disabled="disabled"></fm-select>
					</div>
					<div class="col col-sm-12" v-if="${isEditable} && '${popupid}' == 'vm_update_req_form_popup'">
						<fm-output name="OLD_DISK_SIZE" title="<spring:message code="NC_VM_DISK_SIZE" text="디스크"/>" style="width:50%;display:inline-block;"></fm-output>
						<fm-spin id="${popupid}_DISK_SIZE" name="DISK_SIZE" style="width:calc(100% - 80px);display:inline-block;" onchange="${popupid}_validationDisk()" min="1"></fm-spin>
						<fm-select id="${popupid}_DISK_UNIT" name="DISK_UNIT" :options="diskUnitOptions" style="width:80px;display:inline-block;" onchange="${popupid}_validationDisk()"></fm-select>
					</div>
					<div class="col col-sm-12" v-if="${isEditable} && '${popupid}' == 'vm_update_req_form_popup'">
						<fm-output name="OLD_RAM_SIZE" title="<spring:message code="NC_VM_CPU_CNT" text="CPU"/>" style="width:50%;display:inline-block;"></fm-output>
						<fm-select id="${popupid}_RAM_SIZE" name="RAM_SIZE" :options="${popupid}_RAMOptions" style="width:50%;display:inline-block;" disabled="disabled"></fm-select>
					</div>
					<div class="col col-sm-12" v-if="${!isEditable} || '${popupid}' == 'vm_delete_req_form_popup'">
						<fm-select id="${popupid}_CPU_CNT" name="CPU_CNT" :options="${popupid}_CPUOptions" style="width:100%;display:inline-block;" title="<spring:message code="NC_VM_CPU_CNT" text="CPU"/>" disabled="disabled"></fm-select>
					</div>
					<div class="col col-sm-12" v-if="${!isEditable} || '${popupid}' == 'vm_delete_req_form_popup'">
						<fm-spin id="${popupid}_DISK_SIZE" name="DISK_SIZE" style="width:calc(100% - 80px);display:inline-block;" title="<spring:message code="NC_VM_DISK_SIZE" text="디스크"/>" disabled="disabled"></fm-spin>
						<fm-select id="${popupid}_DISK_UNIT" name="DISK_UNIT" :options="diskUnitOptions" style="width:80px;display:inline-block;" disabled="disabled"></fm-select>
					</div>
					<div class="col col-sm-12" v-if="${!isEditable} || '${popupid}' == 'vm_delete_req_form_popup'">
						<fm-select id="${popupid}_RAM_SIZE" name="RAM_SIZE" :options="${popupid}_RAMOptions" style="width:100%;display:inline-block;" title="<spring:message code="NC_VM_RAM_SIZE" text="메모리"/>" disabled="disabled"></fm-select>
					</div>
					<div class="col col-sm-12">
						<fm-textarea id="${popupid}_CMT" name="CMT" style="width:100%;display:inline-block;" title="<spring:message code="NC_VM_CMT" text="설명"/>" disabled="disabled"></fm-textarea>
					</div>
				</div>
			</form>
		</div>
		<div  class="form-panel detail-panel tab-pane" id="${popupid}Tab2" style="height:280px">
			<div class="col col-sm-6" v-if="${isEditable} && '${popupid}' != 'vm_delete_req_form_popup'">
				<fm-ibutton id="${popupid}_ADD_SEARCH_USER_NAME" name="INS_NM">
					<fm-popup-button popupid="${popupid}_add_user_search_form_popup" popup="/popup/user_search_form_popup.jsp" cmd="update" param="$('#${popupid}_ADD_SEARCH_USER_NAME').val()" callback="${popupid}_add_select_user"><spring:message code="btn_change_owner" text="담당자변경"/></fm-popup-button>
				</fm-ibutton>
				<input type="text" name="DUMMY" style="display:none"/>
			</div>
			<div class="col col-sm-6" v-if="${isEditable} && '${popupid}' != 'vm_delete_req_form_popup'">
				<button id="${popupid}UserSearch" onclick="${popupid}UserSearch()"><spring:message code="btn_reload" text="새로고침"/></button><button id="delete_check_user" onclick="${popupid}_delete_check_user(${popupid}UserListGrid)"><spring:message code="btn_delete" text=""/></button>
			</div>
			<div class="col col-sm-12">
				<div id="${popupid}UserListGrid" class="ag-theme-fresh" style="height: 200px" ></div>
			</div>
			<div class="col col-sm-6"v-if="${isEditable} && '${popupid}' != 'vm_delete_req_form_popup'">
				<span><label for="${popupid}_G_USER_NAME" class="control-label grid-title"><spring:message code="label_userName" text="" /></label><span id="${popupid}_G_USER_NAME" class="output hastitle"></span></span>
			</div>
			<div class="col col-sm-6"v-if="${isEditable} && '${popupid}' != 'vm_delete_req_form_popup'">
				<span><label for="${popupid}_G_TEAM_NM" class="control-label grid-title"><spring:message code="NC_VM_TEAM_NM" text="팀" /></label> <span id="${popupid}_G_TEAM_NM" class="output hastitle"></span></span>
			</div>
			<div class="col col-sm-6"v-if="${isEditable} && '${popupid}' != 'vm_delete_req_form_popup'">
				<span><label for="${popupid}_G_USER_IP" class="control-label grid-title">IP</label> <input name="USER_IP" id="${popupid}_G_USER_IP" class="form-control hastitle" onchange="${popupid}_changeValueForGrid(this, ${popupid}UserListGrid)"></span>
			</div>
			<div class="col col-sm-6"v-if="${isEditable} && '${popupid}' != 'vm_delete_req_form_popup'">
				<span><label for="${popupid}_G_PORT" class="control-label grid-title">Port</label> <input name="PORT" id="${popupid}_G_PORT" class="form-control hastitle" onchange="${popupid}_changeValueForGrid(this, ${popupid}UserListGrid)"></span>
			</div>
		</div>
	</div>
	<script>
		var ${popupid}_CPUOptions = ${popupid}_makeOptions(32, '', 'core');

		var ${popupid}_RAMOptions = ${popupid}_makeOptions(64, '', 'GB');

		var diskUnitOptions = {
			G: 'GB',
			T: 'TB'
		}

		function ${popupid}_makeOptions(maxSize, title_prefix, title_postfix){
			title_prefix = title_prefix?title_prefix: '';
			title_postfix = title_postfix?title_postfix: '';
			var ramList = {};
			for(var i = 1; i <= maxSize; i++){
				eval('ramList['+i+']= title_prefix+i+title_postfix;');
			}
			return ramList;
		}

		function ${popupid}_makeOptions2(maxSize, title_prefix, title_postfix){
			title_prefix = title_prefix?title_prefix: '';
			title_postfix = title_postfix?title_postfix: '';
			var ramList = {};
			for(var i = 1; i <= maxSize; i++){
				eval('ramList['+i+']= title_prefix+i+title_postfix;');
			}
			return ramList;
		}

		var popup_${popupid}_form = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});

		function ${popupid}_click(vue, param){
			if(param.VM_ID){
				if(param.DISK_SIZE >= 1024) {
					param.DISK_UNIT = 'T';
				} else {
					param.DISK_UNIT = 'G';
				}
				${popupid}_param = $.extend({}, param);
				${popupid}_vue.form_data = ${popupid}_param;
				${popupid}UserSearch();
				return true;
			} else {
				${popupid}_param = null;
				alert(msg_select_first);
				return false;
			}
		}

		function ${popupid}_onAfterOpen(){
			$('#${popupid} ul.nav.nav-tabs > li:first > a').tab('show');
		}

		function makeVM_${popupid}Param(addParam){
			var param = $.extend(${popupid}_param, {
				VM_ID: ${popupid}_param.VM_ID,
				CPU_CNT: $('#${popupid}_CPU_CNT').val(),
				DISK_SIZE: $('#${popupid}_DISK_SIZE').val(),
				DISK_UNIT: $('#${popupid}_DISK_UNIT').val(),
				RAM_SIZE: $('#${popupid}_RAM_SIZE').val(),
				SPEC_ID: $('#${popupid}_SPEC_ID').val(),
				OLD_VM_NM: ${popupid}_param.OLD_VM_NM,
				OLD_DISK_SIZE: ${popupid}_param.OLD_DISK_SIZE,
				OLD_DISK_UNIT: ${popupid}_param.OLD_DISK_UNIT,
				NC_VM_USER_LIST: JSON.stringify(${popupid}UserListGrid.getData())
			});
			if(addParam) param = $.extend(param, addParam);
			return param;
		}

		function ${popupid}_save(){
			if($('#${popupid}_DISK_SIZE').hasClass('invalid-size')){
				alert('<spring:message code="msg_disk_size_re_setting"/>');
				$('#${popupid}_DISK_SIZE').select();
				return;
			}
			var param = makeVM_${popupid}Param();
			var action = 'insertReq';
			if(${popupid}_param.CUD_CD == 'U') action = 'updateReq';
			post('/api/vm/'+action, param, function(data){
				alert(msg_complete_work);
				workSearch();
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_changeSpec(target){
			var spec = $(target).find(':selected').prop('data');
			$('#${popupid}_CPU_CNT').val(spec.CPU_CNT);
			var unitComp = 1;
			if(${popupid}_param.OLD_DISK_UNIT == 'T') unitComp = 1024;

			var unitComp2 = 1;
			if(spec.DISK_UNIT == 'T') unitComp2 = 1024;
			if(${popupid}_param.OLD_DISK_SIZE*unitComp <= spec.DISK_SIZE*unitComp2) {
				$('#${popupid}_DISK_SIZE').val(spec.DISK_SIZE);
				$('#${popupid}_DISK_UNIT').val(spec.DISK_UNIT);
			}
			$('#${popupid}_RAM_SIZE').val(spec.RAM_SIZE);
		}

		function ${popupid}_validationDisk(){
			if(${popupid}_param.CUD_CD == 'C') return;
			var unitComp = 1;
			if(${popupid}_param.OLD_DISK_UNIT == 'T') unitComp = 1024;

			var unitComp2 = 1;
			if($('#${popupid}_DISK_UNIT').val() == 'T') unitComp2 = 1024;
			if(${popupid}_param.OLD_DISK_SIZE*unitComp > $('#${popupid}_DISK_SIZE').val()*unitComp2) {
				$('#${popupid}_DISK_SIZE').addClass('invalid-size').css('color', 'red');
			} else {
				$('#${popupid}_DISK_SIZE').removeClass('invalid-size').css('color', '#555555');
			}
		}

		var ${popupid}UserReq = new Req();
		${popupid}UserListGrid;

		$(function() {
			var
			${popupid}UserListGridColumnDefs = [ {
				headerName : '<spring:message code="FM_USER_USER_NAME" text="구분" />',
				field : 'CUD_CD_NM',
				width: 80,
				cellStyle: function(params){
					var style = {
						color: 'black'
					}
					if(params.data.CUD_CD == 'D') style.color = 'red';
					return style;
				}
			},{
				headerName : '<spring:message code="FM_USER_USER_NAME" text="이름" />',
				field : 'USER_NAME',
				width: 120
			},{
				headerName : '<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />',
				field : 'TEAM_NM',
				width: 100
			},{
				headerName : '<spring:message code="FM_USER_POSITION" text="직급" />',
				field : 'POSITION',
				width: 100
			},{
				headerName : '<spring:message code="NC_VM_USER_LOGIN_ID" text="사번" />',
				field : 'LOGIN_ID',
				width: 180
			},{
				headerName : '<spring:message code="NC_VM_USER_USER_IP" text="IP" />',
				field : 'USER_IP',
				width: 130
			},{
				headerName : '<spring:message code="NC_VM_USER_PORT" text="Port" />',
				field : 'PORT',
				width: 70
			}  ];
			var
			${popupid}UserListGridOptions = {
				hasNo : true,
				columnDefs : ${popupid}UserListGridColumnDefs,
				//rowModelType: 'infinite',
				rowSelection : 'single',
				sizeColumnsToFit: false,
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				enableColResize : true,
				enableServerSideSorting: false,
				onRowClicked: function(){
					if(${isEditable} && '${popupid}' != 'vm_delete_req_form_popup'){
						var arr = ${popupid}UserListGrid.getSelectedRows();
						var data = arr[0];
						$('#${popupid}_G_USER_NAME').text(data.USER_NAME);
						$('#${popupid}_G_TEAM_NM').text(data.TEAM_NM);
						$('#${popupid}_G_USER_IP').prop('data', data).val(data.USER_IP);
						$('#${popupid}_G_PORT').prop('data', data).val(data.PORT);
						data.prev_USER_IP = data.USER_IP;
						data.prev_PORT = data.PORT;
					}
				}
			}
			${popupid}UserListGrid = newGrid("${popupid}UserListGrid", ${popupid}UserListGridOptions);
			$('#${popupid}_DISK_SIZE').css('width', '205px');
		});

		function ${popupid}UserSearch(){
			${popupid}UserReq.searchSub('/api/vmuser/list', {VM_ID: ${popupid}_param.VM_ID}, function(data) {
				${popupid}UserListGrid.setData(data);
			});
		}

		function ${popupid}_delete_check_user(grid){
			var arr = ${popupid}UserListGrid.getSelectedRows();
			var data = arr[0];
			if(data.IU == 'I'){
				grid.deleteRow();
			} else {
				data.IU = 'U';
				data.CUD_CD = 'D';
				data.CUD_CD_NM = '<spring:message code="btn_delete" text=""/>';
				grid.updateRow(data);
			}
		}

		function ${popupid}_add_select_user(data, callback){
			var rows = ${popupid}UserListGrid.getData();
			for(var i = 0 ; i < rows.length; i++){
				var row = rows[i];
				if(data.USER_ID == row.USER_ID) {
					alert('<spring:message code="already_selected_user" text="" />');
					return;
				}
			}
			data.IU = 'I';
			data.CUD_CD = 'C';
			data.CUD_CD_NM = '<spring:message code="label_create"/>';
			${popupid}UserListGrid.insertRow(data);

			$('#${popupid}_G_USER_NAME').text(data.USER_NAME);
			$('#${popupid}_G_TEAM_NM').text(data.TEAM_NM);
			$('#${popupid}_G_USER_IP').prop('data', data).val(data.USER_IP);
			$('#${popupid}_G_PORT').prop('data', data).val(data.PORT);
			data.prev_USER_IP = data.USER_IP;
			data.prev_PORT = data.PORT;
			if(callback) callback();
		}

		function ${popupid}_changeValueForGrid(obj, grid){
			var data = $(obj).prop('data');
			if(data && data != null){
				if(data.CUD_CD == 'D'){
					if(!confirm('<spring:message code="msg_delete_user_edit"/>')) {
						$(obj).val(data['prev_' + obj.name]);
						return;
					}
				}
				data[obj.name] = $(obj).val();
				if(data.IU != 'I') {
					data.IU = 'U';
					data.CUD_CD = 'U';
					data.CUD_CD_NM = '<spring:message code="btn_edit" text=""/>';
				}
				data['prev_' + obj.name] = data[obj.name];
				grid.updateRow(data);
			}
		}
	</script>
</fm-modal>
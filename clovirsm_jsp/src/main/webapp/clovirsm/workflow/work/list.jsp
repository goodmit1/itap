<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!--  게시물 관리 -->
<link href="/res/css/editor.css" type="text/css" rel="stylesheet"/>
<script src="/res/js/editor.js"></script>
<style>
	#mainTable { height:calc(100vh - 220px); }
	#mainTable_fee { font-weight: bold}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<%-- <div class="col btn_group" style="float: left;">
			<fm-popup-button popupid="vm_insert_popup" popup="../../resources/reqVm/popup.jsp?action=insert&direct=Y&callback=workflowSearch" cmd="update" class="newBtn" param="{}"><spring:message code="btn_server_create" text="서버생성요청"/></fm-popup-button>
		<c:if test="${sessionScope.FW_SHOW }">
			<fm-popup-button popupid="fw_insert_popup" popup="../../popup/fw_detail_form_popup.jsp?action=insert&direct=Y&callback=workflowSearch" cmd="update" class="newBtn" param="{}"><spring:message code="label_fw_req" text="방화벽 생성 요청"/></fm-popup-button>
		</c:if>
		<c:if test="${sessionScope.IMG_SHOW }">
			<fm-popup-button popupid="img_insert_popup" popup="../../popup/img_detail_form_popup.jsp?action=insert&direct=Y&callback=workflowSearch" cmd="update" class="newBtn" param="{}"><spring:message code="btn_image_create" text="개인템플릿생성요청"/></fm-popup-button>
		</c:if>
		<c:if test="${sessionScope.VRA_SHOW }">
			<fm-popup-button popupid="vra_insert_popup" popup="../../resources/vra/newVra/popup.jsp?action=insert&direct=Y&callback=workflowSearch" cmd="update" class="newBtn" param="{}"><spring:message code="btn_vra_create" text="VRA생성요청"/></fm-popup-button>
		</c:if>
		</div> --%>
		<div class="col btn_group" style="float: right;">
			<fm-sbutton cmd="update" class="delBtn" onclick="cancel()"><spring:message code="btn_cancel" text="작업취소"/></fm-sbutton>
			<fm-popup-button popupid="work_form_popup" popup="work_form_popup.jsp" cmd="update" class="newBtn" param="mainTable.getSelectedRows()"><spring:message code="title_approval_request" text="요청서 작성"/></fm-popup-button>

			
			<fm-popup-button style="display: none;" popupid="vm_detail_tab_popup" popup="/clovirsm/popup/vm_detail_tab_form_popup.jsp?direct=Y&action=insert&callback=workflowSearch" cmd="update" param="vmReq.getData()"><spring:message code="btn_modify" text="생성 정보 수정"/></fm-popup-button>
			<fm-popup-button style="display: none;" popupid="vm_update_popup" popup="/clovirsm/popup/vm_detail_form_popup.jsp?direct=Y&action=update&callback=workflowSearch" cmd="update" param="vmReq.getData()"><spring:message code="btn_spec_modify" text="사양 변경"/></fm-popup-button>
			<fm-popup-button style="display: none;" popupid="vm_req_info_popup" popup="/clovirsm/popup/vm_detail_form_popup.jsp" cmd="update" param="vmReq.getData()"><spring:message code="label_vm_info" text="서버 정보"/></fm-popup-button>

			<fm-popup-button style="display: none;" popupid="fw_insert_save_popup" popup="/clovirsm/popup/fw_detail_form_popup.jsp?direct=Y&action=insert&callback=workflowSearch" cmd="update" param="fwReq.getData()"><spring:message code="btn_modify" text="생성 정보 수정"/></fm-popup-button>
			<fm-popup-button style="display: none;" popupid="fw_update_popup" popup="/clovirsm/popup/fw_detail_form_popup.jsp?direct=Y&action=update&callback=workflowSearch" cmd="update" param="fwReq.getData()"><spring:message code="btn_update_req" text="변경 요청"/></fm-popup-button>
			<fm-popup-button style="display: none;" popupid="fw_req_info_popup" popup="/clovirsm/popup/fw_detail_form_popup.jsp" cmd="update" param="fwReq.getData()"><spring:message code="label_fw_info" text="접근제어 정보"/></fm-popup-button>

			<fm-popup-button style="display: none;" popupid="img_insert_save_popup" popup="/clovirsm/popup/img_detail_form_popup.jsp?direct=Y&action=insert&callback=workflowSearch" cmd="update" param="imgReq.getData()"><spring:message code="btn_modify" text="생성 정보 수정"/></fm-popup-button>
			<fm-popup-button style="display: none;" popupid="img_req_info_popup" popup="/clovirsm/popup/img_detail_form_popup.jsp?direct=Y" cmd="update" param="imgReq.getData()"><spring:message code="label_img_info" text="템플릿 정보"/></fm-popup-button>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area" style="height: 450px;">
	<div class="table_title">


		<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
			<button type="button" title="<spring:message code="btn_reload" text="새로고침"/>" onclick="workflowSearch()" class="btn" id="reeloadlBtn"></button>
		</div>
	</div>
	<div id="mainTable" class="ag-theme-fresh" style="height: 450px;" ></div>
<%-- 	<jsp:include page="../../popup/vra_detail_form_popup.jsp?action=update"></jsp:include> --%>
</div>
<script>
	var req = new Req();
	var vmReq = new Req();
	var fwReq = new Req();
	var imgReq = new Req();

	mainTable;
	var rowDetailParam = null;
	$(function() {
		var
		columnDefs = [
			{
				headerName : '',
				field : "SVC_ID",
				hide: true
			},{
				headerName : "<spring:message code="NC_TASK_ING_TASK_CD" text="신청정보" />",
				maxWidth: 160,
				width: 160,
				editable: false,
				field : "SVC_CD_NM",
				valueGetter: function(params) {
			    	return params.data.SVC_CD_NM + ' ' + params.data.CUD_CD_NM;
				}
			},{
				headerName : "<spring:message code="NC_REQ_SVC_NM" text="서비스명" />",
				maxWidth: 1040,
				width: 1040,
				editable: false,
				field : "SVC_NM"
			},{
				headerName : "<spring:message code="label_DD_FEE" text="비용(일간)" />",
				width: 100,
				editable: false,
				field : "FEE", cellStyle:{'text-align':'right'} , valueFormatter: function(params) {
			    	return  formatNumber(params.value);
				}
			},{
				headerName : "<spring:message code="" text="요청일자"/>",
				maxWidth: 180,
				width: 180,
				editable: false,
				field : "INS_DT",
				valueGetter: function(params) {
			    	return formatDate(params.data.INS_DT,'datetime')
				}
			}
		];

		var
		gridOptions = {
			columnDefs : columnDefs,
			cacheBlockSize: 100,
			rowSelection : 'multiple',
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			editable: true,
			sizeColumnsToFit: true,
			pinnedBottomRowData:createFooter(0),
			enableServerSideSorting: false,
			onRowClicked: function(e){
				var row = e.data;
				 
				if(row.SVC_CD == 'C'){
					 
					openVra4Work(row.SVC_ID, row.INS_DT);
	    			return;
		    	}else if(row.SVC_CD == 'S'){
					vmReq.getInfo('/api/vm/info?VM_ID='+ row.SVC_ID + '&INS_DT=' + row.INS_DT, function(data){
						rowDetailParam = data;
						if(data.CUD_CD == 'C'){
							$('#vm_detail_tab_popup_button').trigger('click');
						} else if(data.CUD_CD == 'U'){
							$('#vm_update_popup_button').trigger('click');
						} else if(data.CUD_CD == 'D'){
							$('#vm_req_info_popup_button').trigger('click');
						}
					});
				} else if(row.SVC_CD == 'F'){
					fwReq.getInfo('/api/fw/info?FW_ID='+ row.SVC_ID + '&INS_DT=' + row.INS_DT, function(data){
						rowDetailParam = data;
						if(data.CUD_CD == 'C'){
							$('#fw_insert_save_popup_button').trigger('click');
						} else if(data.CUD_CD == 'U'){
							$('#fw_update_popup_button').trigger('click');
						} else if(data.CUD_CD == 'D'){
							$('#fw_req_info_popup_button').trigger('click');
						}
					});
				} else if(row.SVC_CD == 'G'){
					imgReq.getInfo('/api/image/info?IMG_ID='+ row.SVC_ID + '&INS_DT=' + row.INS_DT, function(data){
						rowDetailParam = data;
						if(data.CUD_CD == 'C'){
							$('#img_insert_save_popup_button').trigger('click');
						} else if(data.CUD_CD == 'D'){
							$('#img_req_info_popup_button').trigger('click');
						}
					});
				}
			}
		}
		mainTable = newGrid("mainTable", gridOptions);
		workflowSearch();
		<c:if test="${param.click != null}">
			setTimeout(function(){
				$("#${param.click}_popup_button").click();
			}, 1000);
		</c:if>
	});
	var total = 0;
	function createFooter(fee)
	{
		var result= [];
		result.push({SVC_CD_NM:'<spring:message code="label_all" text="전체" />', SVC_NM:'',CUD_CD_NM:'', FEE:fee});
		return result;
	}
	//서버 생성 요청
	function createServerRequest(){
		window.location.href = '/clovirsm/resources/reqVm/index.jsp';
	}

	// 조회
	function workflowSearch() {
		req.search('/api/work/list' , function(data) {
			mainTable.setData(data);
			total = 0;
			for(var i=0; i < data.list.length; i++)
			{
				total += data.list[i].FEE;
			}

			mainTable.gridOptions.api.setPinnedBottomRowData(createFooter(total));
		});
	}

	//작업 취소
	function cancel(){
		var rows = mainTable.getSelectedRows();
		if(rows.length > 0){
			var msg = getMessage("<spring:message code="msg_work_cancel"/>", rows[0].SVC_NM);
			msg = rows.length == 1 ? msg: getMessage("<spring:message code="msg_multi_work_cancel"/>", [rows[0].SVC_NM, rows.length-1]);
			if(!confirm(msg)) return;
			var param = {
				checked: JSON.stringify(rows),
			}
			post('/api/work/cancel', param,  function(data){
				alert(msg_complete_work);
				workflowSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}

	// 엑셀 내보내기
	function exportExcel() {
		exportExcelServer("mainForm", '/api/work/list_excel', 'Work',mainTable.gridOptions.columnDefs, req.getRunSearchData())
	}
</script>
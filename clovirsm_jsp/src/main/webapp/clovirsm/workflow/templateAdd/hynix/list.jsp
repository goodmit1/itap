<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.clovirsm.service.workflow.NextApproverService"%>
<!--  접근 목록 조회 -->
<style>
	#mainTable { height:calc(100vh - 220px); }
	.col label{
	    font-weight: bold;
	    padding: 0px !important;
    	padding-top: 5px !important;
	}
	.col input{
		border: 1px solid #b5b5b5 !important;
	    height: 30px !important;
	}
</style>
<div class="fullGrid" id="input_area" style="border:none;">
<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body" id="inputForm" >
<!-- 		<div class="col col-sm-12" style="border-bottom: 1px solid #ddd;"> -->
<%-- 			<fm-multi-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.listCategory" titlefield="CATEGORY_NM" id="S_CATEGORY"  defaultField="CATEGORY_IDS" keyfield="CATEGORY_ID" parfield="PAR_CATEGORY_ID" rootdata="0"  title=" <spring:message code='TASK' text='업무'/>" selectcmd="serviceStyle" label_style="text-align: center; font-weight: bolder; height: 50px; padding-top: 17px; --%>
<!-- 							" name="CATEGORY_ID"></fm-multi-select> -->
<!-- 			<div style="position: absolute; top: 0px; right: 20px;;"> -->
<%-- 				<span><spring:message code="NO_TASK" text="업무가없으십니까?" /></span> --%>
<%-- 				<span><input type="checkbox" name="chk_info" style="position: relative; top: 11px;" id="chk_task"><spring:message code="label_yes" text="예" /></span> --%>
<!-- 			</div> -->
<!-- 		</div> -->
		<fm-popup-button style="display:none;" popupid="taskPurposeMapping"
			popup="/clovirsm/popup/taskpurposeMapping.jsp" callback="purpose_auto" cmd="update" param="temp">
		<spring:message code="btn_modify" text="생성 정보 수정" /></fm-popup-button>
		<div class="col col-sm-6" id="CATEGORY_DIV" style="border-bottom: 1px solid #ddd; ">
			<fm-select2 url="" id="_svc" name="CATEGORY_ID"
				title=" <spring:message code='TASK' text='업무'/>" required="true" onchange="purpose_auto()"></fm-select2>
		</div>
		<div class="col col-sm-5" style="border-bottom: 1px solid #ddd;">
			<fm-select id="S_PURPOSE" name="PURPOSE" title="<spring:message code="PURPOSE" text="용도"/>" required="required"></fm-select>
		</div>
		<div class="col col-sm-1" style="border-bottom: 1px solid #ddd;">
			<button type="button" onclick="taskPurposeMappingClick()" class="btn icon layout postion" style="position: relative;top: 6px; padding: 0px; left: 0px;"><i class="fa fat add fa-plus-square"></i></button>
		</div>
		<div class="col col-sm-12" style="border-bottom: 1px solid #ddd;">
			<fm-input id="S_TITLE" name="TITLE" title="<spring:message code="label_title" text="제목" />" required="required"></fm-input>
		</div>
		<div class="col col-sm-6" style="border-bottom: 1px solid #ddd;">
			<fm-select url="/api/code_list?grp=FAB" id="S_FAB" name="FAB"  title="FAB" required="true">
		</div>
		<div class="col col-sm-6" style="border-bottom: 1px solid #ddd;">
			<fm-output title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>" div_style="" value="<spring:message code="NC_OS_P_KUBUN_P" text="운영"/>"  ></fm-output>
		</div>
		<div class="col col-sm-6" style="border-bottom: 1px solid #ddd;">
			<fm-input id="S_OS" name="OS" title="OS" required="required"></fm-input>
		</div>
		<div class="col col-sm-6" style="border-bottom: 1px solid #ddd;">
			<fm-input id="S_OS_VER" name="OS_VER" title="<spring:message code="OS_VER" text="OS 버전" />" required="required"></fm-input>
		</div>
		<div class="col col-sm-6" style="border-bottom: 1px solid #ddd;">
			<fm-input id="S_CPU_CNT" name="CPU_CNT" title="CPU" numberValidate="numberValidate" required="required"></fm-input>
		</div>
		<div class="col col-sm-6" style="border-bottom: 1px solid #ddd;">
			<fm-input id="S_RAM_SIZE" name="RAM_SIZE" title="Memory(GB)" numberValidate="numberValidate" required="required"></fm-input>
		</div>
		<div class="col col-sm-6" style="border-bottom: 1px solid #ddd;">
			<fm-input id="S_DISK_SIZE" name="DISK_SIZE" title="Disk(GB)" numberValidate="numberValidate" required="required"></fm-input>
		</div>
		<div class="col col-sm-6" style="border-bottom: 1px solid #ddd;">
			<fm-input id="S_DISK_PATH" name="DISK_PATH" title="Disk Path"></fm-input>
		</div>
		<div class="col col-sm-12" style="border-bottom: 1px solid #ddd;">
			<fm-textarea id="S_INSTAL_SW" name="INSTAL_SW" title="S/W <spring:message code="NC_VRA_CATALOG_CMT" text="설명"/>" placeholder="<spring:message code="INFO_SW" text="설명"/>" label_style="height: 300px;" sty="height: 280px; border: 1px solid #b5b5b5;   padding:10px;"></fm-textarea>
		</div>
		<div class="col col-sm-12" style="border-bottom: 1px solid #ddd;">
			<fm-textarea id="S_ETC" name="ETC" title="<spring:message code="ETC" text="기타"/>" label_style="height: 200px;" placeholder="<spring:message code="tempate_req_etc" text="입력 받아야 할 변수가 있다면 config파일위치와 파일내의 위치를 기술해 주세요\n 예)tomcat 설치Dir/conf/server.xml의  &lt;Connector connectionTimeout='20000' port='8080' protocol='HTTP/1.1' redirectPort='8443'/&gt;의 port 부분"/>" sty="height: 180px; border: 1px solid #b5b5b5;   padding:10px;"></fm-textarea>
		</div>
		<div class="col col-sm-7">
			<fm-file name="files" id="files" multiple="true" title="<spring:message code="NC_REQ_ATT_FILE_PATH"/>" ></fm-file>
 				</div>
		<div class="col col-sm-5">
			<jsp:include page="/home/common/_fileAttach.jsp">
				<jsp:param value="template_req" name="kubun"/>
				<jsp:param value="Y" name="editable"/>
			</jsp:include>
		</div>
	</div>
</div>
	<div style="text-align: center;">
		<input type="button" class="btn saveBtn popupBtn cancel" value="<spring:message code="btn_list" text="목록" />" id="tamplet" onclick="listTamplate()" />
		<input type="button" class="btn saveBtn popupBtn prev" value="<spring:message code="btn_save" text="저장" />" onclick="saveTemplate()" />
	</div>
</div>
<script>
	var req = new Req();
	var temp = new Object();
	function saveTemplate(){
		req
		if(numValidate("inputForm") && validate("inputForm")){
			showLoading();
			req.putData({"CATEGORY_ID":$("#_svc").val(), "PURPOSE":$("#S_PURPOSE").val(), "KUBUN" :'prd'});
			req.saveFiles('/api/template_req/save_fileAttach',"files", function(data) {
				hideLoading();
				location.href="/clovirsm/workflow/requestItemsHistory/index.jsp";
			});
		}
	}
	function listTamplate(){
		location.href="/clovirsm/workflow/requestItemsHistory/index.jsp"
	}
	
	function setCategory(){
		fillOption("_svc", "/api/category/map/list/list_NC_CATEGORY_MAP_SELECT_TASK/?ALL_USE_YN=Y", '', function(data){
	
		}, "CATEGORY_ID", "CATEGORY_NM");
	}
	function purpose_auto(){
		temp.CATEGORY_ID = $("#_svc").val();
		fillOption("S_PURPOSE", '/api/category/map/list/list_NC_CATEGORY_MAP_PURPOSE/?CATEGORY_ID='+ $("#_svc").val(), '', function(data){
			
		}, "ID", "TITLE");
		
	}
	
	function taskPurposeMappingClick() {
		if($("#_svc").val() != null && $("#_svc").val() != ''){
			$('#taskPurposeMapping_button').trigger('click');
		} else{
			alert("<spring:message code="" text="업무를 선택해주세요."/>")
		}
	};
	$(document).ready(function(){
		req.putData({"FAB":search_data.keyword, "KUBUN":search_data.P_KUBUN});
		setCategory();

		$("#chk_task").change(function(){
			 
			if($("#chk_task").is(":checked")){
				$("#S_CATEGORY label").html("<spring:message code="HIGHER_TASK" text="상위 업무"/>")
				$("#categoryNm").css("display","block");
				$("#S_CATEGORY select:last").hide();
			}else{
				$("#S_CATEGORY label").html("<spring:message code="TASK" text="업무"/>")
				$("#categoryNm").css("display","none");
				$("#S_CATEGORY select:last").show()
			}
		});
		
		<c:if test="${param.CHK_TASK eq 'Y'}">
		setTimeout(function(){
				$("#chk_task").click();
			}, 1000)
	 
		</c:if>
	});
</script>
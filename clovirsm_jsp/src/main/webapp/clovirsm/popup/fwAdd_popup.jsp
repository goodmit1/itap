<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
    String popupid = request.getParameter("popupid");
    request.setAttribute("popupid", popupid);
%>
<style>


</style>

<fm-modal id="${popupid}" title='<spring:message code="" text="접근제어 추가"/>' cmd="header-title">
	<span slot="footer">
		<button type="button" class="contentsBtn tabBtnImg save top5" onclick="saveFw()"><spring:message code="btn_save" text="저장"/></button>
	</span>
    <div id="fwFiled" class="form-panel detail-panel" style="height: 140px;">
        <div class="col col-sm-6">
            <fm-ibutton id="${popupid}_FW_USER_NAME" name="FW_USER_NAME"  required="true" title="<spring:message code="FM_USER_USER_NAME" text="이름" />" >
                <fm-popup-button v-show="false" popupid="${popupid}_user_search_popup" popup="/popup/user_search_form_popup.jsp" cmd="update" param="$('#${popupid}_USER_NAME').val()" callback="${popupid}_select_user"  ><spring:message code="title_chg_owner" text="담당자변경"/></fm-popup-button>
            </fm-ibutton>
        </div>
        <div class="col col-sm-6">
            <fm-input id="${popupid}_FW_CIDR" name="FW_CIDR" required="true" placeholder="<spring:message code="mulit_sortation_placeholder" text="여러 개인 경우 ,로 구분해서 입력하세요." />" title="<spring:message code="NC_FW_PORT_USER" text="사용자 IP" />"   ></fm-input>
        </div>
        <div class="col col-sm-6">
            <fm-input id="${popupid}_FW_PORT" name="FW_PORT" required="true" placeholder="<spring:message code="mulit_sortation_placeholder" text="여러 개인 경우 ,로 구분해서 입력하세요." />" title="<spring:message code="NC_FW_PORT_ALLOW" text="허용 PORT" />"   ></fm-input>
        </div>
        <div class="col col-sm-6">
            <fm-spin id="${popupid}_FW_USE_MM" name="FW_USE_MM" style="width: 300px" title="<spring:message code="NC_FW_USE_MM_M" text="사용기간(개월)" />" min="6" ></fm-spin>
        </div>
        <div class="col col-sm-6">
            <fm-output id="${popupid}_FW_TEAM_NM" name="FW_TEAM_NM" title="<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />"></fm-output>
        </div>
    </div>
    <script>
        var appr = null;
        var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
        var ${popupid}_param = {};
        var ${popupid}_vue = new Vue({
            el: '#${popupid}',
            data: {
                form_data: ${popupid}_param
            }
        });

        function ${popupid}_click(vue, param, callback){

            ${popupid}_parent_vue = vue;
            if(param != null){
                ${popupid}_param = param;
            } else{
                ${popupid}_param = {};
            }
            ${popupid}_vue.form_data = ${popupid}_param;
            ${popupid}_vue.form_data.callback=callback;
            return true;
        }


        function ${popupid}_select_user(user, callback){

            setVueData(${popupid}_vue, 'FW_TEAM_CD', user.TEAM_CD);
            setVueData(${popupid}_vue, 'FW_TEAM_NM', user.TEAM_NM);
            setVueData(${popupid}_vue, 'FW_USER_ID', user.USER_ID);
            setVueData(${popupid}_vue, 'FW_USER_NAME', user.USER_NAME);
            setVueData(${popupid}_vue, 'FW_CIDR', user.LAST_LOGIN_IP);
            setVueData(${popupid}_vue, 'FW_LOGIN_ID', user.LOGIN_ID);
            if(callback) callback();
        }

        function saveFw(){
            if(validate("fwFiled")){
                ${popupid}_param.insertRow({
                    CIDR : ${popupid}_param.FW_CIDR,
                    TEAM_CD : ${popupid}_param.FW_TEAM_CD,
                    TEAM_NM :  ${popupid}_param.FW_TEAM_NM,
                    USER_NAME : ${popupid}_param.FW_USER_NAME,
                    USE_MM : ${popupid}_param.FW_USE_MM,
                    PORT : $("#${popupid}_FW_PORT").val(),
                    VM_USER_YN : 'Y',
                    LOGIN_ID : ${popupid}_param.FW_LOGIN_ID,
                    FW_USER_ID : ${popupid}_param.FW_USER_ID,
                    CUD_CD : "C"
                })
                $('#${popupid}').modal('hide');

            }
        }



    </script>
    <style>

    </style>
</fm-modal>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
	 
		var envAllArray = new Array();
		/** vro visible **/
		function addVisible(id, conditions){
			var targets = {};
			for(var i=0; i < conditions.length; i++){
				var condition = conditions[i];
				  Object.keys(condition).forEach(function(key){
					if(key != 'value'){
						Object.keys(condition[key]).forEach(function(key1){
							targets[key1] = '';
							$("#" + key1).change(function(e){
								if( chkVROValue(key, $(e.target).val(),condition[key][key1], condition.value)){
									
									$("." + id + "-area").show();
								}
								else{
									$("." + id + "-area").hide();
								}
								
							});
						
						});
					}
				  });
			} 
			Object.keys(targets).forEach(function(key){
				$("#" + key).trigger("change");
			});
		 
		}
		/** vro value chk **/
		function chkVROValue(op, val, cval, normal){
			var result = false;
			if(val != null){
				if(op=='equals'){
					result = (val == cval); 
				}
				else if(op=='startsWith'){
					result = val.indexOf(cval)==0;
				}
				else if(op=='notEmpty'){
					result = val != '';
				}
				else if(op=='endsWith'){
					result = val.indexOf(cval) == (val.length - cval.length);
				}
				else if(op=='contains'){
					result = val && val.indexOf(cval)>=0;
				}
			}
			return normal? result : !result;
		}
		function commonDiv(content, i){
			var html = '<div class="col col-sm-12 ' + content.name + '-area"  ><div  id="' + content.name + '-option"><label for="'+content.name+'" class="control-label grid-title value-title stepLabel ">'+content.title+'</label></div></div>'
			$('#step'+Number(i+2)).append(html);
		 
		}
		
		function stringInput(content, i ){
			commonDiv(content, i);
			$('#' + content.name + '-option').append('<input name="'+content.name+'" id="'+content.name +'" value="'+ nvl(content.default,'') +'" class="form-control input hastitle stepInput">');
			inputVaildate(content);
		}
		function dateInput(content, i){
			 
			commonDiv(content, i);
			$('#' + content.name + '-option').append('<input name="'+content.name+'" id="'+content.name +'" value="'+ nvl(content.default,'') +'" class="form-control input hastitle dateInput stepInput">');
			var option = { language : 'ko', pickTime : false,format:'YYYY-MM-DD', useCurrent:false};
			content.min="-30d"
			content.max="-1d"
			if(content.min){
				option.minDate = addDate(new Date(), content.min)
			}
			if(content.max){
				option.maxDate = addDate(new Date(), content.max)
			}
			$(".dateInput").datetimepicker(option);
		}
		function dateTimeInput(content, i){
			 
			commonDiv(content, i);
			$('#' + content.name + '-option').append('<input name="'+content.name+'" id="'+content.name +'" value="'+ nvl(content.default,'') +'" class="form-control input hastitle dateTimeInput stepInput">');
			$(".dateTimeInput").datetimepicker({ language : 'ko', pickTime : true,format:'YYYY-MM-DDTHH:mm:ss.000\\Z', useCurrent:false});
		}
		function hiddenInput(content, i){
			$('#step'+Number(i+2)).append('<input type="hidden" name="'+content.name+'"   id="'+content.name +'" class="form-control input hastitle stepSelect">');
			if(content['api']){ 
				externApiSelect(content)
			}
			else if(content.default){
				$("#" + content.name).val(content.default)
			}
		}
		function stringSelect(content, i){
			commonDiv(content, i);
			$('#' + content.name + '-option').append('<select name="'+content.name+'" value="'+ nvl(content.default,'')+'" id="'+content.name +'" class="form-control input hastitle stepSelect">');
			if( content['enum']) {
				for(var d = 0 ; d < content['enum'].length ; d++){
					$('#' + content.name + '-option select').append('<option value="'+content['enum'][d]+'">'+content['enum'][d]+'</option>');
				}
			}
			if(content['oneOf']){ 
				for(var d = 0 ; d < content['oneOf'].length ; d++){
					$('#' + content.name + '-option select').append('<option value="'+content['oneOf'][d].const+'">'+content['oneOf'][d].title+'</option>');
				}
			}
			if(content['api_url']){ 
				fillOption(content.name, content['api_url'], '', function(data){
					});
			}
			if(content['api']){ 
				//if(action == "disable"){
				//	$('#' + content.name + '-option select').append('<option value="'+ ${popupid}_param.DATA_JSON[content.name]+'">'+ ${popupid}_param.DATA_JSON[content.name]+'</option>');
				//}
				//else{
					externApiSelect(content)
				//}
				
			}
			else if(content.default){
				$("#" + content.name).val(content.default)
			}
			inputVaildate(content);
		}
		
		function integerInput(content, i){
			commonDiv(content, i );
			$('#' + content.name + '-option').attr("numberValidate","numberValidate");
			$('#' + content.name + '-option').append('<input name="'+content.name+'" value="'+nvl(content.default,'')+'" id="' + content.name +'" class="form-control input hastitle stepInput" required="required">');
			inputVaildate(content);
		}
		
		function integerSelect(content, i){
			
			commonDiv(content, i );
			$('#' + content.name + '-option').append('<select name="'+content.name+'" value="'+nvl(content.default,'')+'" id="'+content.name + '" class="form-control input hastitle stepSelect" >');
				
			if( content['enum']) {
				for(var d = 0 ; d < content['enum'].length ; d++){
					$('#' + content.name + '-option select').append('<option value="'+content['enum'][d]+'">'+content['enum'][d]+'</option>');
				}
			}
			else if(content['oneOf']){ 
				for(var d = 0 ; d < content['oneOf'].length ; d++){
					$('#' + content.name + '-option select').append('<option value="'+content['oneOf'][d].const+'">'+content['oneOf'][d].title+'</option>');
				}
			}
			else{
				var minimum = 1;
				var maximum = content.maximum;
				if(content.minimum){
					minimum = content.minimum;
				}
				$('#' + content.name + '-option').attr("numberValidate","numberValidate");
				for(var d = minimum ; d <= maximum ; d++){
					if(d == content.default)
						$('#'+content.name + '-option select').append('<option selected value="'+d+'">'+d+'</option>');
					else
						$('#'+content.name + '-option select').append('<option value="'+d+'">'+d+'</option>');
				}
				if(content.default){
					$("#" + content.name).val(content.default)
				}
				inputVaildate(content);
			}
		}
		var select2Orders = new Select2Orders();
		var applicationList;
		function fillApplication(obj,   linuxYn){
		 	if(!applicationList) return;
			if(!linuxYn){
				linuxYn = getLinuxYN(obj)
			}
		 	
			var type = obj.attr("data-type");
			var arr = [];
			if(linuxYn != null){
				for(var i=0; i < applicationList.length; i++){
					if(applicationList[i].OS_TYPE=='all' || applicationList[i].OS_TYPE ==  ( linuxYn=="Y"?"linux": (linuxYn=='N'? "window":'')) ){
						arr.push(applicationList[i])
					}
				}
			}
			fillOptionByData(obj.attr("name"), arr, true,  ( linuxYn=="Y"?"LINUX_SW_PATH":"WIN_SW_PATH") , "SW_NM" );
			dataJSONSet(obj.attr("name"));
			if(type == "array"){
				select2Orders.make(obj.attr("name"),   '<spring:message code="msg_drag" text="선택 후 drag&drop으로 순서를 조정할 수 있습니다." />');
				 
				//$("#"+name).val([]);

			} else{
				$('#'+name).select2();
			}
		}
		function changeApplication(name, type ,purpose){
			post('/api/sw/list/list_NC_SW_APP/?&CATEGORY_ID='+ $("_svc").val() +'&PURPOSE='+purpose, {}, function(data){
				applicationList = data;
				fillApplication($("#" + name))
			})
			
			
			 
			 
		}

		function diskDisplay(content, i ){
			var html = '';
			html += '<div class="col col-sm-12  ' + content.name + '-area" required="required"  >';
			html += '<div  id="' + content.name +'-option">';
			html += '<label for="'+content.name+'_'+i+'" class="control-label grid-title value-title stepLabel">Disk(GB)</label>';
			html += '<select name="' + content.name + '" value="'+ nvl(content.default,'') +'" id="' + content.name + '" class="form-control input  hastitle stepSelect diskSelect" > </select>';
			html += '</div>';
			html += '</div>';
			
			return html;
		}
		 
		
		function flavoreDisplay(content, i ){
			var html = '';
			html += '<div class="col col-sm-12 ' + content.name + '-area" required="required"  >';
			html += '<div  id="' + content.name +'-option">';
			html += '<label for="'+content.name+'_'+(i+1)+'" class="control-label grid-title value-title stepLabel"><spring:message code="NC_VM_SPEC_SPEC_INFOM" text="ì¬ì"/></label>';
			html += '<select name="' + content.name + '" value="'+ nvl(content.default,'') +'" id="' + content.name + '" onchange="" class="form-control input hastitle flavor stepSelect" style="display:inline-block; width: 300px !important;  margin-right: 10px;"> </select>';
			//html += '<div style="display: inline-block;" id="flavorPay"><spring:message code="label_cost" text=""/></div>';
			html += '</div>';
			html += '</div>';
			
			return html;
		}
		function refInput(content, i ){
			if(content.type == 'include'){
				$('#step'+Number(i+2)).append('<div id="' + content.name + '-div"></div>');
				$.get('/clovirsm/resources/vra/newVra/include/' + content.fieldName + '.jsp?popupid=' + popupid + '&stepIdx=' + (i+2) + '&name=' + content.name, function(data){
					$('#' + content.name + '-div').append(data);
				})
				return true;
			}
			 
			if(content.fieldName == 'VC:VirtualMachine'){
				var html = '';
			 
				html += '<div class="col col-sm-12 vm  ' + content.name + '-area"  >';
				html += '<label for="'+content.name+'_'+(i+1)+'" class="control-label grid-title value-title stepLabel"    >' + content.title + '</label>';
				html += '<select name="'+content.name+'" value="'+nvl(content.default,'')+'" id="' + content.name +'" class="form-control input hastitle stepInput"' +  (content.type=='array' ? ' multiple':'') + '></select>'     ;
				html += '</div>';
				
				$('#step'+Number(i+2)).append(html);
				$("#" + content.name).change(function(){
					chgVM(this)
				}) 
				
				selectVMList(content.name);
				
				
				$('#_fab, #_p_kubun, #_purpose, #_svc').change(function() {
					selectVMList(content.name, content.maxItems);
				});
				return true;
		
			}
			return false;
		}
		function selectVMList(contentName, maxLength){
					var name = contentName;
					var fab = $("#_fab").val();
					var kubun = $("#_p_kubun").val();
					var purpose = $("#_purpose").val();
					var svc = $("#_svc").val();
					fillOption( name, '/api/monitor/list/list_VM_VRA/?FAB='+fab+'&P_KUBUN='+kubun + '&PURPOSE=' + purpose + '&CATEGORY=' + svc, true ,function(){
							var p = {};
							if(maxLength){
								p.maximumSelectionLength = maxLength
							}
							$('#'+name).select2(p);
							 
							if(content.type == "array"){
							 
								 
								$("#"+name).val([]);
		
							}  
							
						}, "VM_VRA_ID", ["VM_NM", "IP"]);
		}
		function chgVM(obj){
			var vms = $(obj).find("option:selected");
			var oldLinuxYn=null;
			for(var i=0; i < vms.length; i++){
				if(oldLinuxYn == null){
					oldLinuxYn = vms[i].data.LINUX_YN;
				}
				else if(vms[i].data.LINUX_YN != oldLinuxYn){
					oldLinuxYn = 'B';
					break;
				}
			 
			}
			fillApplication($(obj).parents(".step").find(".application:first select"), oldLinuxYn);
		}
		function displayApp(i,   contents ){
			for(var z = 0 ; z < contents.length ; z++){
				var content = contents[z];
				if(checkName(content)){
					var isref = refInput(content, i );
					if(!isref){
						if(content.type == 'string'){
							if(!isNull(content['enum']) || !isNull(content['oneOf']) || !isNull(content['api']) || !isNull(content['api_url'])){
								stringSelect(content, i );
							}else{
								stringInput(content, i );
							}
						} else if(content.type == 'integer' || content.type == 'number' ){
							if(content.minimum || content.maximum || !isNull(content['enum']) || !isNull(content['oneOf'])){
								integerSelect(content, i );
							}else{
								integerInput(content, i );
							}
						} else if(content.type=='dateTime'){
							dateTimeInput(content, i )
						} else if(content.type=='date'){
							dateInput(content, i )
						
						} else if(content.type=='hidden'){
							hiddenInput(content, i )	
						} else if(content.type=='validationMessage'){
							validationMessageDisplay(content, i )
						}   
						
					}
					//dataJSONSet(content.name);
				} else{
					nameInput(content, i);
				}

				putDefaultDataJSON(content)
			}
			for(var z = 0 ; z < contents.length ; z++){
				var content = contents[z];
				if(!isNull(content['visible'])){
					addVisible(content.name, content['visible'] );
				}
			}
			externalChange();
		}
		function validationMessageDisplay(content, i ){
			var html ='<div id="' + content.name + '" class="validationMessage" style="display:none"></div>';
			$('#step'+Number(i+2)).append(html);			
			externApiSelect(content);
		}
		function inputVaildate(content){
			if(!isNull(content.required) ){
				$('#' + content.name + '-option').attr("required","required");
			} 
			
			if(!isNull(content.encrypted)){
				if(content.encrypted){
					$('#' + content.name + '-option input').attr("type","password");
				}
			}
			
			if(!isNull(content.pattern)){
				$('#' + content.name + '-option input').attr("onkeyup","patternCheck(this,'"+content.pattern+"')");
			}
		}
		
		function putEnvDataJSON( stepName){
			var envlist = new Array();
			var arr = $("div[data-step-name='"+stepName+"'] .app_env :input")
			for(var i = 0 ; i < arr.length ; i ++){
				 
					var envObject = "";
					var name = arr[i].name;
					var val =  $(arr[i]).val();
					if(val){
						envObject = name +"="+val;
						envlist.push(envObject);
					}
				 
			 }
				
				return envlist;
		}
		function callExternAPI(content){
		
			var api = JSON.stringify(content.api) ;
			for(var i=0; i < content.api.dependOn.length; i++){
				var val = $("#"+content.api.dependOn[i]).val();
				if(content.api.dependOn[i] == '_svc'){
					val = getVueObj($("#_svc")).getEtcValue("CATEGORY_CODE");
				}
				if(val==null){
					val = "";
				}
				if(val == ''){
					return;
				}
				api = api.replace('$' + '{' + content.api.dependOn[i] + '}', val);
			}
			var _param = {'api': api, 'CONN_ID':eval(popupid+'_param.CONN_ID')}
			post(  '/api/vra_catalog/extern_api', _param, function(data){
				 if(content.type && content.type=='validationMessage'){
				 	$("#" + content.name).text(data);
				 	if(data && data != ''){
				 		$("#" + content.name).show();
				 	}
				 	else{
				 		$("#" + content.name).hide();
				 	}
				 }
				 
				 else if($("#" + content.name).length>0 && $("#" + content.name)[0].tagName=='SELECT'){
				 
				  	fillOptionByData(content.name , data);
				 	$("#" + content.name).val(contents.default)
				 }
				 else{
				 	$("#" + content.name).val(data);
				 	if($("#" + content.name) == 0 || $("#" + content.name).attr("type") == "hidden"){
				 		changeExtern(content.name);
				 		 
				 	}
				 }
			} );
		}
		var externDependOn={};
		function externApiSelect(content){
			if(content.api.dependOn.length==0){
				callExternAPI(content);
			}
			else{
				for(var i=0; i < content.api.dependOn.length; i++){
					var arr = externDependOn[content.api.dependOn[i]]
					if(arr){
						arr.push(content);
					}
					else{
						arr = [];
						arr.push(content);
						externDependOn[content.api.dependOn[i]] = arr;
					}
					
				}
				callExternAPI(content);
			}
			
			
			$("#" + content.name).change(function(){
				changeExtern($(this).attr("id"));
				 
				 
			})
			
		}
		function changeExtern(key){
			var arr = externDependOn[key];
				if(arr){
					for(var i=0; i < arr.length; i++){
						callExternAPI(arr[i]);
					}
				}
		}
		function externalChange(){
			Object.keys(externDependOn).forEach(function(key){
				if(key && key.indexOf("_") != 0){
					$("#" + key).change(function(){
						var arr = externDependOn[key];
						if(arr){
							for(var i=0; i < arr.length; i++){
								callExternAPI(arr[i]);
							}
						}
					});
				}
			});
		}
		function addApplicationEnv(that){
			var stepName = $(that).parents(".step").attr("data-step-name");
			var stepIdx =  getStepIdx(that);
			var id = $(that).attr("id");
			var swArray = new Array();
			var purpose = null
			var param = new Object();
			for(var n = 0 ; n < $("#"+id+" option:selected").length ; n++){
				if($("#"+id+" option:selected")[n].value != ''){
					swArray.push($("#"+id+" option:selected")[n].data.SW_NM);
				}
			}
			displayAppEnvByNm(swArray, stepIdx-2 )
			 
		}
		function getAllSwEnv(callback){
			post("/api/sw/env/list", null , function(data){
				if(data){
					 
					for(var n = 0 ; n < data.list.length ; n++){
						var envObject = new Object();
						envObject = data.list[n];
						envAllArray.push(envObject);
					}
				}
				if(callback){
					callback.call(null, data);
				}
			});
		} 
		function displayAppEnvByNm(  appList, stepIdx  ){
			var list = [];
	 		for(var m = 0 ; m < envAllArray.length ; m++){
				  for(var n = 0 ; n < appList.length; n++){
					  if(appList[n] == envAllArray[m].sw_nm){
						  list.push(envAllArray[m]);
					  }
				  }
			  }
			displayApp(stepIdx   ,  list );
			for(var i=0; i <  list.length;i++){
				$("#" +  list[i].name + "-option").parent().addClass("app_env");
			}	  
		}
		function imageDisplay(content, i){
			
			var html = '';
			html += '<div class="col col-sm-12 ' + content.name + '-area" required="required"   >';
			html += '<div  id="' + content.name + '-option">';
			html += '<label for="'+content.name+'_'+i+'" class="control-label grid-title value-title stepLabel">OS</label>';
			html += '<select name="' + content.name + '" value="'+ nvl(content.default,'') +'" id="' + content.name + '" class="form-control input   hastitle stepSelect imgSelect" onchange="imgChange()"> </select>';
			html += '</div>';
			html += '</div>';
			
			return html;
		}
		
		function cpuDisplay(content, i ){
			var html = '';
			html += '<div class="col col-sm-12 ' + content.name + '-area" required="required"   >';
			html += '<div  id="' + content.name +'-option">';
			html += '<label for="'+content.name+'_'+i+'" class="control-label grid-title value-title stepLabel">CPU</label>';
			if( getKubunVal() != null){
				html += '<select name="' + content.name + '" value="'+ nvl(content.default,'') +'" id="' + content.name + '" class="form-control input  hastitle stepSelect cpuSelect" > </select>';
			}
			else{
					html += '<input name="' + content.name + '" value="'+ nvl(content.default,'') +'" id="' + content.name + '" class="form-control input  hastitle stepSelect cpuSelect" >';
		
			}
			html += '</div>';
			html += '</div>';
			
			return html;
		}
		function memDisplay(content, i ){
			var html = '';
			html += '<div class="col col-sm-12 ' + content.name + '-area" required="required"  >';
			html += '<div  id="' + content.name +'-option">';
			html += '<label for="'+content.name+'_'+i+'" class="control-label grid-title value-title stepLabel">Memory(GB)</label>';
			if( getKubunVal() != null){
				html += '<select name="' + content.name + '" value="'+ nvl(content.default,'') +'" id="' + content.name + '" class="form-control input  hastitle stepSelect memSelect" > </select>';
			}
			else {
				html += '<input name="' + content.name + '" value="'+ nvl(content.default,'') +'" id="' + content.name + '" class="form-control input  hastitle stepSelect memSelect" >';
			}
			html += '</div>';
			html += '</div>';
			
			return html;
		}
		function patternCheck(_this, pattern){
			var rege = new RegExp(pattern);
			var value = $(_this).val();
			if(!rege.test(value)){
				alert("정확한 값을 입력하세요!");
				$(_this).val("");
				return ;
			}
		}
		
		function checkName(content){
			var name = content.fieldName;
			 
			var html='';
			
			if(name =='capacityGb' || name == 'flavor' || name == 'image' || name == "application"  || name == "cpuCount"  || name == "totalMemoryMB"){
				return false;
			} else{
				return true;
			}
			
		}
		function selectRetouch(content){
			 
				var vals = {};
				var html = '';
				if(!isNull(content['enum'])){
					for(var i=0;  i < content["enum"].length; i++){
						vals[content["enum"][i]] = '-';
						html += '<option>' + content["enum"][i];
					}
				}
				else if(!isNull(content['oneOf'])){
					for(var i=0;   i < content["oneOf"].length; i++){
						vals[content["oneOf"][i].const] = '-';
						html += '<option value="' + content["oneOf"][i].const + '">' + content["oneOf"][i].title;
					}
				}
				if(html=='') return;
				var options = $("#" + content.name + " option");
				if(options.length==0){
					$("#" + content.name).html(html);
				}
				else{
					for(var i=0; i < options.length; i++){
						if( vals[ $(options[i]).val() ] == null){
							 $(options[i]).remove();
						}
					}
					
				}
				$("#" + content.name).addClass("enum");
			 
			
		}
		function nameInput(content, i  ){
			var name = content.fieldName;
			var html = '';
			if(name == 'flavor'){
				html = flavoreDisplay(content, i );
				$('#step'+Number(i+2)).append(html);
				fillOption(content.name, '/api/code_list?grp=dblist.com.clovirsm.common.Component.selectSpec', '', function(data){
					dataJSONSet(content.name);
				}, "ID", "TITLE");
			} else if(name == 'image'){
				html = imageDisplay(content, i );
				$('#step'+Number(i+2)).append(html);
				$("#" + content.name).change(function(){
					fillApplication($('#step'+Number(i+2)).find(".application:first select"))
				})
				fillOption(content.name, '/api/code_list?grp=dblist.com.clovirsm.common.Component.selectOSType&CONN_ID=' + eval(popupid+"_param.CONN_ID"), '',function(){
					dataJSONSet(content.name);
					selectRetouch(content);
				}, "ID", "TITLE");
			} else if(name == "application"){
		        applicationDisplay(content,i);
		        changeApplication(content.name,content.type,  i );
			} else if(name == "cpuCount"){
				html = cpuDisplay(content, i );
				$('#step'+Number(i+2)).append(html);
				selectRetouch(content);
			} else if(name == "totalMemoryMB"){
				html = memDisplay(content, i );
				$('#step'+Number(i+2)).append(html);
				selectRetouch(content);
			} else if(name == "capacityGb"){
				html = diskDisplay(content, i );
				$('#step'+Number(i+2)).append(html);
			}
			 
		}	
		function getStepIdx(obj){
			 var id = $(obj).parents(".step").attr("id");
			 return 1*id.substring(4);
		}
		
		
		function clickApplicationEnv(that){
			 
			delApplicationEnv(that);
		}
		function delApplicationEnv(that){
			$(that).parents(".step").find(".app_env").remove();
			addApplicationEnv(that);
		}	
		function getLinuxYN(stepIdx){
			var imgSelect = null;
			if(stepIdx instanceof Object){
				imgSelect= stepIdx.parents(".step").find(".imgSelect option:selected")
			}
			else{
				imgSelect= $("#step" + stepIdx + " .imgSelect option:selected")
			}
			if(imgSelect && imgSelect.length>0 && imgSelect[0].data){
				return imgSelect[0].data.LINUX_YN 
			}
			return null;
		}
		function applicationDisplay(content,i){
			var html = '';
			var purpose = "";
			try{
				purpose = (step_purpose && step_purpose.length>i) ? step_purpose[i] : "";
			}catch(e){	}
			var purpose_value = purpose != "" ? purpose : "all";
			html += '<div class="col col-sm-12 application  ' + content.name + '-area" >';
			html += '<label for="'+content.name+'_'+(i+1)+'" class="control-label grid-title value-title stepLabel"   >Application<br>(<spring:message code="msg_install" text="설치순서대로 선택해 주세요." />)</label>';
			html += '<select  data-type="' + content.type + '"  purpose="'+   purpose_value  + '" name="'+content.name+'" id="'+content.name +'"  ' + (content.type=='array' ? 'multiple':'') + '  class="form-control input hastitle stepSelect" style="display:inline-block;  margin-left: 0px !important;"> </select>';
			html += '</div>';
			
			$('#step'+Number(i+2)).append(html);
			$("#" + content.name).on("change", function(){
					clickApplicationEnv(this)
			}) 
			
			
			return html;
		}
		
		function imgChange(){ 
			if(varStep){
				var linuxYn = getLinuxYN(varStep);
				if(linuxYn == "Y"){
					$("#os_local_user").parent().wrap("<div required='required'></div>")
				} else{
					if($("#os_local_user").parent().parent().attr("required") == "required")
						$("#os_local_user").parent().unwrap();
				}
			}
		}
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmtags" uri="http://fliconz.kr/jsp/tlds/fmtags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%
	String popupid = request.getParameter("popupid");
	String action = request.getParameter("action");
	String callback = request.getParameter("callback");

	request.setAttribute("callback", callback);

	if (popupid == null || "".equals(popupid)) {
		popupid = "";
	}
	if (action == null || "".equals(action)) {
		action = "insert";
	}
	request.setAttribute("popupid", popupid);
	request.setAttribute("action", action);
%>


<style>
#${
popupid


}
>
.modal-dialog {
	width: 930px;
}

#${
popupid


}
>
.modal-body {
	height: 600px;
}

#${
popupid


}
.output {
	display: inline;
	padding-left: 10px;
	background-color: white;
	border: none;
}

#${
popupid


}
select
,
#${
popupid


}
input


.input
,
#${
popupid


}
textarea {
	padding: 5px;
	border: 1px solid #b5b5b5;
}

.stepInput {
	max-width: 500px !important;
	margin-left: 5px !important;
}

.stepSelect {
	width: 500px !important;
	margin-left: 5px !important;
}

.step {
	padding: 10px;
}

.titleStyle {
	height: 28px;
	padding-top: 2px;
	padding-left: 12px;
	font-size: 20px;
	margin-top: 50px;
	margin-bottom: 20px;
	font-weight: 600;
}

#preBtn {
	display: none;
}

#finishBtn {
	display: none;
}

.back_blue:hover {
	background: #1b4583;
	color: white !important;
}

.back_blue:active {
	background: #1b4583;
	color: white !important;
}

.back_blue:focus {
	background: #1b4583;
	color: white !important;
}

.input-group-btn-vertical>.btn {
	height: 16px;
	width: 10px;
}

.input-group-btn-vertical i {
	position: absolute;
	top: 0;
	left: 6px;
}

.stepLabel {
	padding-left: 25px !important;
	text-align: left !important;
	max-width: 300px !important;
	width: 300px !important;
}

.stepTableHader {
	background: #eee;
	border: 1px solid #ddd;
	padding: 10px;
}

.stepTable {
	border: 1px solid #ddd;
	padding: 10px;
}

.stepTableBottom {
	background: #eee;
	padding: 10px;
	border: 1px solid #ddd;
}

.serviceStyle {
	border: 1px solid rgb(181, 181, 181);
	min-width: 100px;
	padding: 5px;
}

.modal-body .select2-container--default.select2-container--focus .select2-selection--multiple
	{
	border: 1px solid rgb(181, 181, 181);
	outline: 0;
}

.modal-body .col-sm-12 .select2-container {
	width: 500px !important;
	margin-left: 5px;
}

.modal-body .col-sm-8 .select2-container {
	width: 427px !important;
	margin-left: 0px;
}

.modal-body .menu-layout-3 .select2-container .select2-selection--single
	{
	height: 95%;
	border-radius: 0px;
}

.modal-dialog .col {
	border-top: 1px solid #ddd;
	margin-top: -1px;
	border-bottom: 1px solid #ddd;
}

.modal-dialog .menu-layout-3 .select2-container .select2-selection--single
	{
	height: 27.53px;
	border-radius: 0px;
	padding-top: 2px;
}

.modal-dialog .popup_area .value-title {
	min-width: 142px;
	max-width: 140px;
	width: auto;
	background-color: #ebf2ff;
	font-weight: bold;
	text-align: center;
	height: 50px;
	font-size: 15px;
	padding-top: 17px;
}

.editor {
	height: 150px;
	padding: 1%;
	border: 1px solid #b5b5b5;
	border-radius: 0;
	word-wrap: break-word;
	overflow: auto;
	width: 82.6%;
	float: right;
	margin-right: 6px;
}

.application label {
	height: 65px !important;
}

.appTitle {
	font-size: 16px;
	font-weight: 900;
	color: #3c404a;
}

.panel-body {
	padding: 0px;
}

#newVmPopup_form .approvalName-detail.in {
    position: absolute;
    top: 6px;
    color: white;
    font-weight: 900;
}
#newVmPopup_form .approvalName-detail.out {
    position: absolute;
    top: 6px;
    color: #bbbbbb;
    font-weight: 900;
}
#newVmPopup_form .approvalName-detail {
    position: absolute;
    top: 6px;
    color: #00AC4F;
    font-size: 14px;
    font-weight: 900;
    z-index: 80;
    height: 25px;
    overflow: hidden;
    text-overflow: ellipsis;
}

#newVmPopup_form .progress-coming-back {
	position: absolute;
    top: 0px;
    width: 162px;
    height: 30px;
    border-radius: 45px;
    background-image: none;
    z-index: 30;
    padding: 8px;
    padding-top: 10px;
    background-color: white;
    border: 1px solid #d2d4d9;
}
#newVmPopup_form .progress-done{
	position: absolute;
    top: 0px;
    width: 162px;
    height: 30px;
    border-radius: 45px;
    background-image: none;
    z-index: 30;
    padding: 8px;
    padding-top: 10px;
    background-color: white;
    border: 1px solid #d2d4d9;
}
#newVmPopup_form .progress-done div{
	background-color: #d2d4d9 !important;
    color: white !important;
    height: 24px;
    width: 24px;
    font-size: 14px;
    font-weight: 900;
    padding: 3px;
    padding-left: 7px;
    border-radius: 50%;
    position: relative;
    top: -7px;
    left: -5px;
}
#newVmPopup_form .progress-coming-in {
    width: 162px;
    position: absolute;
    height: 30px;
    background: none;
    border: 1px solid #00AC4F;
    background-color: #00AC4F;
    border-radius: 45px;
    text-align: center;
    padding: 1px;
    font-size: 20px;
    color: white;
    font-weight: bold;
    top: 0px;
    padding-top: 6px;
}

#newVmPopup_form .vraProgress-coming-back {
    background-color: white !important;
    color: #00AC4F !important;
}

#newVmPopup_form .progress-coming-back div{
    background-color: #d2d4d9 !important;

}

#newVmPopup_form .progress-coming-in div{
    background-color: #ffffff;
    color: #00AC4F;
    height: 24px;
    width: 24px;
    font-size: 14px;
    font-weight: 900;
    padding: 3px;
    padding-left: 2px;
    border-radius: 50%;
    position: relative;
    top: -3px;
    left: 1px;
}


#newVmPopup_form .progress-done-in{
	width: 162px;
    position: absolute;
    height: 30px;
    background: none;
    border: 1px solid #00AC4F;
    background-color: #00AC4F;
    border-radius: 45px;
    text-align: center;
    padding: 1px;
    font-size: 20px;
    color: white;
    font-weight: bold;
    top: 0px;
    padding-top: 6px;
}

#newVmPopup_form .progress-done-in div{
	background-color: #ffffff;
    color: #00AC4F;
    height: 24px;
    width: 24px;
    font-size: 14px;
    font-weight: 900;
    padding: 3px;
    padding-left: 2px;
    border-radius: 50%;
    position: relative;
    top: -3px;
    left: 1px;
}
</style>

<fm-modal id="${popupid}" title="<spring:message code="new_service" text="신규 배포 요청"/>" cmd="header-title">
	<span slot="footer" id="${popupid}_footer">
		<input type="button" class="btn delBtn popupBtn cancel top5" value="<spring:message code="label_cancel" text=""/>" onclick="cancelBtn();" />
		<input type="button"class="btn exeBtn popupBtn prev top5" id="preBtn" value="<spring:message code="label_back" text="이전"/>" 	onclick="preBtn();" />
		<input type="button"	class="btn exeBtn popupBtn next top5" id="nextBtn" value="<spring:message code="label_next" text="다음"/>" onclick="nextBtn();" />
		<input type="button" v-if="'${action}' == 'insert'"	class="btn exeBtn back_blue popupBtn finish top5" id="finishBtn" value="<spring:message code="msg_jsf_complete" text="완료"/>" onclick="finishBtn();" />
		<input type="button" v-if="'${action}' == 'update'" class="btn exeBtn back_blue popupBtn change top5" id="finishBtn" value="<spring:message code="btn_edit" text="수정"/>" onclick="finishBtn();" />
</span>
<form id="${popupid}_form" action="none">
	<template>
	<div class="panel-body">
		<div class="appTitle"></div>
		<div class="appProgressBar" id="appProgressBar">
			<div class="bar_progress in" style="width: 98%;">
				<div class="progress-coming-in zindex" id="step_p_1">
					<div>1</div>
				</div>
				<div class="progress-done zindex">
					<div>3</div>
				</div>
				<span id="processStep">
					<div class="vraProgress-coming-back progress-coming-back zindex" id="step_p_2" style="left:calc(30% + -20px);">
						<div>2</div>
					</div>
					<div class="approvalName-detail vraApprovalName-detail out" style="left: calc(30% + 15px); width: 120px; text-align: left;"><spring:message code="" text="접근제어" /></div>
				
					<div class="vraProgress-coming-back progress-coming-back zindex" id="step_p_2" style="left:calc(58% + -20px);">
						<div>3</div>
					</div>
					<div class="approvalName-detail vraApprovalName-detail out" style="left: calc(58% + 15px); width: 120px; text-align: left;"><spring:message code="spec" text="사양선택" /></div>
				 </span>
				<div class="Rounded-Rectangle-Full" style="width: 100%; top: 15px;"></div>
				<div class="approvalName-detail vraApprovalName first in"
					style="left: 34px;">
					<spring:message code="basic_information" text="기본정보" />
				</div>
				<div class="approvalName-detail vraApprovalName last out">
					<spring:message code="finish" text="완료" />
				</div>
			</div>
		</div>


		<div class="contents" id="contents" style="overflow: hidden auto;">
			<div id="stepStatus" class="stepStatus">
				<div id="step1_Status">
					STEP 1.
					<spring:message code="basic_information" text="기본정보" />
				</div>
				<div id="step2_Status" class="hide">
					STEP 2.
					<spring:message code="" text="접근제어" />
				</div>
				<div id="step3_Status" class="hide">
					STEP 3.
					<spring:message code="spec" text="사양선택" />
				</div>
				<div id="step4_Status" class="hide">
					STEP 4.
					<spring:message code="finish" text="완료" />
				</div>
			</div>
			<div id="step1" class="step">
				<div class="col col-sm-12">
					<fm-input id="S_REQ_TITLE" name="REQ_TITLE"
						title="<spring:message code="REQ_TITLE" text="요청명"/>"
						required="true"></fm-input>
				</div>
				<div class="createInfo col col-sm-8" id="CATEGORY_DIV">
					<fm-select2 url="" id="S_CATEGORY" name="CATEGORY"
						title=" <spring:message code='TASK' text='업무'/>" required="true"
						select_style="width: calc(100% - 150px);"></fm-select2>
				</div>
				<div class="createInfo col col-sm-4">
					<fm-select url="/api/code_list?grp=PURPOSE" id="S_PURPOSE"
						name="PURPOSE"
						title="<spring:message code="PURPOSE" text="용도"/>" required="true"
						select_style="width: calc(100% - 150px);"> </fm-select>
				</div>
				<div class="createInfo col col-sm-4">
					<fm-select url="/api/code_list?grp=P_KUBUN" id="S_P_KUBUN"
						name="P_KUBUN" onchange="kubunChange(true);"
						title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>"
						required="true" select_style="width: calc(100% - 150px);">
					</fm-select>
				</div>
				<div class="createInfo col col-sm-4">
					<fm-select id="S_USE_MM" name="USE_MM"
						title="<spring:message code="NC_VM_USE_MM" text="사용기간(개월)"/>"
						required="true" select_style="width: calc(100% - 150px);">
					</fm-select>
				</div>
				<div class="col col-sm-12">
					<div>
						<label for="S_CMT" class="control-label grid-title value-title"
							style="height: 150px;">설명</label>
						<div class="editor" id="S_CMT" contenteditable="true"
							v-html="form_data.CMT"></div>
					</div>
				</div>
			</div>
			<div id="step2" class="step hide">
				<div style="text-align: right; position: relative; top: -23px;">
					<fm-sButton id="${popupid}_add_fw_popup" cmd="update" class="btn delBtn popupBtn cancel" style="height: 25px; min-width: 60px;" onclick="${popupid}_add_fw(${popupid}_vmFwListGrid)" :disabled="${action != 'insert'}"><spring:message code="btn_add" text="추가"/></fm-sButton>
					<fm-sButton id="${popupid}_delete_check_fw_popup" cmd="update" class="btn exeBtn popupBtn prev" style="height: 25px; min-width: 60px;" onclick="${popupid}_delete_check_fw(${popupid}_vmFwListGrid)" :disabled="${action != 'insert'}"><spring:message code="btn_delete" text="삭제"/></fm-sButton>
				</div>
				<div id="${popupid}_vmFwListGrid" class="ag-theme-fresh" style="height: 200px; position: relative; top: -15px;" ></div>
				<fm-popup-button v-show="false" popupid="${popupid}_fwAdd_popup" popup="/clovirsm/popup/fwAdd_popup.jsp" cmd="update" param="${popupid}_vmFwListGrid"  ></fm-popup-button>
			</div>
			<div id="step3" class="step hide">
				<div id="specDefualt">
					<div class="col col-sm-12 copyVM">
						<fm-select2 id="S_FROM_ID" name="FROM_ID" url="/api/monitor/list/list_VM_NM/"
							keyfield="VM_ID" titlefield="VM_NM"
							title="<spring:message code="NC_VM_FROM_NM"  />"
							required="true"></fm-select2>
					</div>
					<div class="col col-sm-12">
						<fm-select id="S_CPU_CNT" name="CPU_CNT"
							title="<spring:message code="CPU_CNT" text="CPU"/>"
							required="true"></fm-select>
					</div>
					<div class="col col-sm-12">
						<fm-select id="S_RAM_SIZE" name="RAM_SIZE"
							title="<spring:message code="RAM_SIZE" text="Memory"/>(GB)"
							required="true"></fm-select>
					</div>
					<div class="col col-sm-6">
						<fm-select id="S_DISK_SIZE" name="DISK_SIZE"
							title="<spring:message code="DATA_DISK_SIZE" text="Data Disk"/>(GB)"
							required="true"></fm-select>
					</div>
					<div class="col col-sm-6">
						<fm-select id="S_DISK_TYPE"
								   name="DISK_TYPE_ID"
								   class="DATA_DISK_TYPE"
								   url="/api/code_list?grp=dblist.com.clovirsm.common.Component.selectDiskType"
								   title="<spring:message code="DATA_DISK_TYPE" text="Data Type"/>"
								   required="true"
						></fm-select>
					</div>
				</div>
				<div id="specAWS" class="hide">
					<fm-select2 id="SPEC_ID" name="SPEC_ID"
							keyfield="SPEC_ID" titlefield="SPEC_NM" onchange="changeSpec(this)"
							title="<spring:message code="DATA_DISK_TYPE" text="사양"  />" select_style=" width: 420px;"
							required="true"></fm-select2>
					<input type="hidden" id="SS_CPU_CNT" name="CPU_CNT">
					<input type="hidden" id="SS_RAM_SIZE" name="RAM_SIZE">
					<input type="hidden" id="SS_DISK_SIZE" name="DISK_SIZE">
					<input type="hidden" id="SS_DISK_TYPE" name="DISK_TYPE_ID">
				</div>
			</div>
			<div id="step4" class="step hide">
				<table style="width:100%;">
				<tr><td class="stepTableHader"><spring:message code="REQ_TITLE" text="요청명"/></td><td class="stepTable"><span id="REQ_TITLE_output"></span></td></tr>
				<tr><td class="stepTableHader"><spring:message code="NC_OS_TYPE_OS_NM" text="요청명"/></td><td class="stepTable"><span id="OS_NM_output"></span></td></tr>
				<tr><td class="stepTableHader"><spring:message code="PURPOSE" text="업무"/></td><td class="stepTable"><span id="CATEGORY_NM_output"></span></td></tr>
				<tr><td class="stepTableHader"><spring:message code="NC_VM_P_KUBUN" text="구분"/></td><td class="stepTable"><span id="PURPOSE_NM_output"></span> <span id="P_KUBUN_NM_output"></span></td></tr>
				<tr><td class="stepTableHader"><spring:message code="NC_VM_USE_MM" text="사용기간(개월)"/></td><td class="stepTable"><span id="USE_MM_output"></span></td></tr>
				<tr><td class="stepTableHader"><spring:message code="label_spec" text="사양"/></td><td class="stepTable"><span id="spec_output"></span></td></tr>
				</table>

			</div>
		</div>

		<div id="stepFinish" class="contents stepFinish hide"
			style="padding-top: 30px;">
			<div id="stepFinishCmt">

			</div>
			<div class="btn_group">
				<fm-sbutton class="exeBtn popupBtn finish stepBtn" cmd="search"
					onclick="endModal()" style="font-size: 16px;"> <spring:message
					code="FINISH_NEXT_PAGE" text="" /></fm-sbutton>
			</div>
		</div>
	</div>
	</template>
</form>


<script>
		var varStep = 1;
		var action="${action}"
		var ${popupid}_userArr;
		var ${popupid} = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {USE_MM:12, CMT:"<spring:message code="new_vra_CMT" text="1.서버 용도<br><br>2.서비스 목적<br><br>3.관련업무<br><br>4.기타"/>"};
		var ${popupid}_parent_vue = null;

		var time = 0;
		var contents = {};
		var paramStep = 4;
		var DATA_JSON = new Object();
		
		var ${popupid}_vmFwListGrid;
	
		var popupid= "${popupid}";
		var FORM_INFO = new Object();
		var ${popupid}_callback = '${callback}';
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		${popupid}_fw_init();
		
		function setCategory(param){
			fillOption("S_CATEGORY", "/api/code_list?grp=dblist.com.clovirsm.common.Component.listLeafCategory" + (param.CATEGORY ? "&CATEGORY="+ param.CATEGORY :""), '', function(data){


				if(data.length == 1){
					$("#CATEGORY_DIV .select2").attr("style","display:none");
					$("#CATEGORY_DIV div:first").append("<div class='output'>"+data[0].CATEGORY_NM+"</div>");
					${popupid}_param.CATEGORY= data[0].CATEGORY_ID;
					$("#S_CATEGORY").val(data[0].CATEGORY_ID)
				} else{
					var categoryIds = ${popupid}_param.CATEGORY_IDS;
					if(categoryIds != null){
						var categoryArry = removeEmpty(categoryIds.split(","));
						${popupid}_param.CATEGORY=categoryArry[categoryArry.length-1];
					}
					else{
						${popupid}_param.CATEGORY=$("#S_CATEGORY option:first").val()
					}
					//$("#S_CATEGORY").select2();
				}
			}, "CATEGORY_ID", "CATEGORY_NMS");
		}

		function makeOutputHidden(id, val, text){
			var name = $("#" + id).attr("name")
			var parent = $("#" + id).parent();
			parent.find(".select2").remove();
			$("#" + id).remove();
			var html = '<div  class="output">' + text + '<input type="hidden" id="' + id + '" name="' + name + '" value="' + val + '"></div>';
			parent.append(html);

		}

		function ${popupid}_click_default(inputs,  key1, key2){
			if(inputs[key1] && inputs[key1]['default']){
				${popupid}_param[key2] = inputs[key1]['default']
				setTimeout(function(){
					makeSelectOutputHidden(key2)
				}, 500)

			}
		}
		function ${popupid}_click(vue, param){
			${popupid}_parent_vue = vue;
			$('.appTitle').text(param.OS_NM + ' <spring:message code="addTemplatInfo" text=" 템플릿을 사용하여 신규 서비스를 요청합니다."/>');

			if(param != null ){
				${popupid}_param = $.extend(${popupid}_param,param);
			}

			if(param.OS_ID != '0' ){
				$(".copyVM").hide();
			}
			else{
				$(".copyTemplate").hide();
			}
			
			if("${action}" == "update"){
				if(param.CATALOGREQ_ID){
					${popupid}_vue.form_data = ${popupid}_param;
					return true;
				}
				else {
					delete_req_form_popup_param = null;
					alert(msg_select_first);
					return false;
				}
			}else{
				setCategory(param);
				${popupid}_vue.form_data = ${popupid}_param;
				return true;
			}
		}

		// 취소버튼에 대한 function
		function cancelBtn(){
			$('#${popupid}').modal('hide');
		}

		//  다음 버튼에 대한 function
		function nextBtn(){
			// validate 메소드로 빈곳을 체크 , numValidate으로 숫자 체크
			if(validate("contents") && numValidate("contents")){
				varStep++;
				if(varStep > 1){
					$("#contents #step"+(varStep-1)).addClass("step hide");
					$("#contents #step"+varStep).removeClass("hide");
					$("#preBtn").css("display","inline-block");
					$('#step1_Status').addClass("hide");
					$('#step'+(varStep-1)+'_Status').addClass("hide");
					$('#step'+varStep+'_Status').removeClass("hide");
					
					if(varStep == 3){
					 	if(${popupid}_vmFwListGrid.getData().length == 0){
					 		alert("접근제어 하나이상 설정해야합니다.");
					 		preBtn();
						}		
					 	checkSpec();
					}
				}
				if (varStep == paramStep){
					resultData();
					$("#contents #step"+varStep).addClass("step hide");
					$('#step'+(varStep-1)+'_Status').addClass("hide");
					$('#step'+paramStep+'_Status').removeClass("hide");
					$("#contents #step"+paramStep).removeClass("hide");
					$("#finishBtn").css("display","inline-block");
					$("#nextBtn").css("display","none");
					$(".appTitle").css("display","none");
				}
				stepProgressDisplay(varStep);
				//select2Orders.onShow();

			}

		}

		// 뒤로가기 버튼에 대한 function
		function preBtn(){
			varStep--;
			$("#finishBtn").css("display","none");
			if(varStep < paramStep){
				$("#nextBtn").css("display","inline-block");
				$("#contents #step"+(varStep+1)).addClass("step hide");
				$("#contents #step"+varStep).removeClass("hide");
				$("#contents #step"+paramStep).addClass("step hide");
				$('#step'+varStep+'_Status').removeClass("hide");
				$('#step'+(varStep+1)+'_Status').addClass("hide");
			}
			if(varStep == 1){
				$('#step1_Status').removeClass("hide");
				$("#preBtn").css("display","none");
			}
			stepProgressDisplay(varStep);
		}

		// step에 따라 view 제어
		function stepProgressDisplay(varStep){
			$("#${popupid} .vraApprovalName-detail").removeClass("in");
			$("#${popupid} .vraApprovalName-detail").addClass("out");
			if(varStep == 1){
				$("#${popupid} #step_p_1").attr("class","progress-coming-in");
				$("#${popupid} .vraApprovalName.first").addClass("in");
				$("#${popupid} .bar_progress > span > .progress-coming-in ").attr("class","progress-coming-back vraProgress-coming-back");
			} else if(varStep == paramStep){
				$("#${popupid} .progress-done").attr("class","progress-done-in");
				$("#${popupid} .bar_progress > span > .progress-coming-back , .bar_progress > span > .progress-coming-in").eq(varStep-3).prevAll(".progress-coming-in, .progress-coming-out").attr("class","progress-coming-in vraProgress-coming-back");
				$("#${popupid} .bar_progress > span > .progress-coming-back , .bar_progress > span > .progress-coming-in").eq(varStep-3).attr("class","progress-coming-in vraProgress-coming-back");
				$("#${popupid} .approvalName-detail.last").removeClass("out");
				$("#${popupid} .approvalName-detail.last").addClass("in");
				$("#${popupid} .bar_progress > span > .vraApprovalName-detail").eq(varStep-3).attr("class","approvalName-detail vraApprovalName-detail");
				$("#${popupid} .bar_progress > span > .vraApprovalName-detail").eq(varStep-3).prevAll(".vraApprovalName-detail").attr("class","approvalName-detail vraApprovalName-detail");
				$("#${popupid} .bar_progress > span > .progress-coming-back ").attr("class","progress-coming-in vraProgress-coming-back");
			} else{
				$("#${popupid} #step_p_1").attr("class","progress-coming-in vraProgress-coming-back");
				$("#${popupid} .progress-done-in").attr("class","progress-done");
				$("#${popupid} .vraApprovalName.first").removeClass("in");
				$("#${popupid} .vraApprovalName.last").addClass("out");
				$("#${popupid} .vraApprovalName.last").removeClass("in");
// 				$("#step_p_"+paramStep+"").attr("class","progress-coming-in");
				$("#${popupid} .bar_progress > span > .progress-coming-back , .bar_progress > span > .progress-coming-in").eq(varStep-2).prevAll(".progress-coming-in, .progress-coming-out").attr("class","progress-coming-in vraProgress-coming-back");
				$("#${popupid} .bar_progress > span > .progress-coming-back , .bar_progress > span > .progress-coming-in").eq(varStep-2).nextAll(".progress-coming-in, .progress-coming-out").attr("class","progress-coming-back vraProgress-coming-back");
				$("#${popupid} .bar_progress > span > .progress-coming-back , .bar_progress > span > .progress-coming-in").eq(varStep-2).attr("class","progress-coming-in");
				$("#${popupid} .bar_progress > span > .vraApprovalName-detail").eq(varStep-2).prevAll(".approvalName-detail").attr("class","approvalName-detail vraApprovalName-detail");
				$("#${popupid} .bar_progress > span > .vraApprovalName-detail").eq(varStep-2).nextAll(".approvalName-detail").attr("class","approvalName-detail vraApprovalName-detail out");
				$("#${popupid} .bar_progress > span > .vraApprovalName-detail").eq(varStep-2).attr("class","approvalName-detail vraApprovalName-detail in");
			}
		}


		function checkSpec(){
			if(${popupid}_param.HV_CD == 'V'){
				kubunChange(false);
			} else{
				initSpecData();
				$("#specDefualt").addClass("hide");
				$("#specAWS").removeClass("hide");
			}
		}

		function changeSpec(that){
			if($("#SPEC_ID option:selected")[0]){
				var data = $("#SPEC_ID option:selected")[0].data;
				$("#SS_CPU_CNT").val(data.CPU_CNT);
				$("#SS_RAM_SIZE").val(data.RAM_SIZE);
				$("#SS_DISK_SIZE").val(data.DISK_SIZE);
				$("#SS_DISK_TYPE").val(data.DISK_TYPE_ID);
				
				${popupid}_param.DISK_SIZE = data.DISK_SIZE;
				${popupid}_param.DISK_TYPE = data.DISK_TYPE_ID;
				${popupid}_param.RAM_SIZE = data.RAM_SIZE;
				${popupid}_param.CPU_CNT = data.CPU_CNT;
				${popupid}_param.DISK_TYPE_ID = data.DISK_TYPE_ID;
				
				
			}
		}
		
		function getKubunVal(){
			return $("#S_P_KUBUN").val();
		}
		
		function kubunChange(first){
				var specParam = new Object();
				var dataObj = {'CPU':[], 'RAM':[], 'DISK':[], 'MONTH':[], 'GPU':[]}

				specParam.ID = getKubunVal();
				post("/api/code_list?grp=dblist.com.clovirsm.common.Component.selectSpecSetting",specParam, function(data){
					for(var q = 0 ; q < data.length; q++){
						valList = data[q].VAL1.split(",");

						for(var w = 0 ; w < valList.length; w++){
							var object = new Object();
							object.title = valList[w];
							object.val = valList[w];
							dataObj[data[q].KUBUN].push(object);
						}
					}
							fillOptionByData('S_RAM_SIZE', dataObj['RAM'], '', "val", "title");
							fillOptionByData('S_CPU_CNT', dataObj['CPU'], '', "val", "title");
							fillOptionByData('S_DISK_SIZE', dataObj['DISK'], '', "val", "title");
							defaultSpec();

					if(first){
						fillOptionByData("S_USE_MM", dataObj['MONTH'], null, "val", "title");
						//$("#S_USE_MM").val(monthArray[0].val)
						${popupid}_param.USE_MM=dataObj['MONTH'][0].val
					}
				},false);

		}

		function defaultSpec() {
			var defaultCpuSpec = document.querySelector('#S_CPU_CNT').options[1].value
			var defaultRamSpec =  document.querySelector('#S_RAM_SIZE').options[1].value
			var defaultDiskSpec =  document.querySelector('#S_DISK_SIZE').options[1].value
			var defaultDiskTypeSpec = document.querySelector('#S_DISK_TYPE').options[1].value

			document.querySelector('#S_CPU_CNT').value = defaultCpuSpec;
			document.querySelector('#S_RAM_SIZE').value = defaultRamSpec;
			document.querySelector('#S_DISK_SIZE').value = defaultDiskSpec;
			document.querySelector('#S_DISK_TYPE').value = defaultDiskTypeSpec;

			${popupid}_vue.form_data.CPU_CNT = defaultCpuSpec;
			${popupid}_vue.form_data.RAM_SIZE = defaultRamSpec;
			${popupid}_vue.form_data.DISK_SIZE = defaultDiskSpec;
			${popupid}_vue.form_data.DISK_TYPE_ID = defaultDiskTypeSpec;
		}


		function isNull(v) {
		    return (v === undefined || v === null) ? true : false;
		}

		function AddMonthDate(num){
			var date = new Date();

			date.addMonths( Number(num));
			return formatDate(date, 'date')
		}
		//DATAJSON에  ENV parameter 추가

		//DATAJSON에  공통 parameter 추가

		function resultData(){
			 $("#REQ_TITLE_output").text($("#S_REQ_TITLE").val());
			 $("#OS_NM_output").text(${popupid}_param.OS_NM);
			 if(${popupid}_param.HV_CD == "V"){
				 $("#spec_output").text(getSpecInfo(null, $("#S_CPU_CNT").val(), $("#S_RAM_SIZE").val(),   $("#S_DISK_SIZE").val(), 'G'));
			 }
			 else {
				 $("#spec_output").text(getSpecInfo(null, $("#SS_CPU_CNT").val(), $("#SS_RAM_SIZE").val(),   $("#SS_DISK_SIZE").val(), 'G'));
			 }
			 $("#CATEGORY_NM_output").text($("#S_CATEGORY option:selected").text());
			 $("#P_KUBUN_NM_output").text($("#S_P_KUBUN option:selected").text());
			 $("#PURPOSE_NM_output").text($("#S_PURPOSE option:selected").text());
			 $("#USE_MM_output").html($('#S_USE_MM').val()+'<spring:message code="MONTH" text="개월"/> <span style="color: #2b9fe2;">('+AddMonthDate($('#S_USE_MM').val())+'<spring:message code="EXPIRE" text="만료 예정"/>)</span>');
		}


		function finishBtn(){
			var param = ${popupid}_param ;
			if(${popupid}_param.HV_ID == "V")
				param.SPEC_ID='0'
			else{
				param.SPEC_ID=$("#SPEC_ID").val();
			}
			// param.DISK_TYPE_ID = 1
			if(param.OS_ID == '0' ){
				var orgVMInfo = $("#S_FROM_ID option:selected")[0].data
				param.FROM_ID = orgVMInfo.VM_ID;
				param.DC_ID = orgVMInfo.DC_ID;
			}
			param.CATEGORY_CODE =  $("#S_CATEGORY option:selected")[0].data.CATEGORY_CODE
			param.CATEGORY = $("#S_CATEGORY").val();
			param.PREDICT_DD_FEE = Number(($('#totalFee').val() ? $('#totalFee').val() : 0) + (${popupid}_param.FEE?${popupid}_param.FEE:0));
			param.APPR_STATUS_CD = 'W';
			param.NC_VM_FW_LIST = JSON.stringify(${popupid}_vmFwListGrid.getData());
			var url = '/api/vm/insertReq';
			param.CUD_CD = 'C'
			if ('${action}' == 'update'){
				param.CUD_CD = 'U';
				url = '/api/vra_catalog/updateReq';
			}


			param.DEPLOY_REQ_YN = 'Y';

			param.CMT = removeBRP($("#S_CMT").html())
 			post(url,param , function(data){
 					if(${popupid}_callback != ''){
 						eval(${popupid}_callback + '();');
 					}
 					$('#${popupid}').modal('hide');
					FMAlert("<img src='/res/img/hynix/confirm.png' style='margin-bottom:20px;margin-top: 20px;'><div style='font-size: 16px; font-weight: 900; margin-bottom:20px;'><spring:message code="REQ_MSG_M1" text=""/></div><div><spring:message code="REQ_MSG_M2" text=""/></div><div><spring:message code="REQ_MSG_M3" arguments='"+time+"' text=""/></div><div><spring:message code="REQ_MSG_M4" text=""/></div>",'<spring:message code="request_complete" text="요청완료" />','<spring:message code="go_history_page" text="신청이력페이지로 이동" />', function(){location.href="/clovirsm/workflow/requestItemsHistory/index.jsp"; } );

 			});
		}
		function endModal(){
			location.href="/clovirsm/workflow/requestItemsHistory/index.jsp";
			return true;

		}
		
		function ${popupid}_fw_init() {
			var
			${popupid}_vmFwListGridColumnDefs = [ {
				headerName : '<spring:message code="NC_FW_CUD_CD" text="구분" />',
				field : 'IU_NM',
				width: 120,
				cellStyle: function(params){
					var style = {
						color: 'black'
					}
					if(params.data.CUD_CD == 'D') style.color = 'red';
					return style;
				},
				valueGetter: function(params){
					if( params.data.IU_NM ) return params.data.IU_NM;
					else return nvl(params.data.CUD_CD_NM,'') + ' ' + nvl(params.data.APPR_STATUS_CD_NM,'');
				}
			},{
				headerName : '<spring:message code="NC_FW_CIDR" text="IP" />',
				field : 'CIDR',
				width: 130
			},{
				headerName : '<spring:message code="NC_FW_PORT" text="Port" />',
				field : 'PORT',
				width: 120
			},{
				headerName : '<spring:message code="NC_FW_FW_USER_ID_NM" text="이름" />',
				field : 'USER_NAME',
				width: 120
			},{
				headerName : '<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />',
				field : 'TEAM_NM',
				width: 150
			},{
				headerName : '<spring:message code="NC_FW_VM_USER_YN" text="계정생성여부" />',
				field : 'VM_USER_YN',
				width: 180,
				valueGetter: function(params){
					 
					if(params.data.VM_USER_YN == undefined || params.data.VM_USER_YN == '') return '';
					return params.data.VM_USER_YN  == 'Y' ? '<spring:message code="label_yes"/>': '<spring:message code="label_no"/>';
				}
			},{
				headerName : '<spring:message code="NC_FW_USE_MM" text="Port" />',
				field : 'USE_MM',
				width: 100
			},{
				field : 'LOGIN_ID',
				hide : true
			},{
				field : 'FW_USER_ID',
				hide : true
			}];
			
			var
			${popupid}_vmFwListGridOptions = {
				columnDefs : ${popupid}_vmFwListGridColumnDefs,
				//rowModelType: 'infinite',
				rowSelection : 'single',
				sizeColumnsToFit: true,
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				enableColResize : true,
				enableServerSideSorting: false,
				onRowClicked: function(){
					var arr = ${popupid}_vmFwListGrid.getSelectedRows();
					var data = arr[0];

					setVueData(${popupid}_vue, 'FW_IU', data.IU);
					setVueData(${popupid}_vue, 'FW_IU_NM', data.IU_NM);
					setVueData(${popupid}_vue, 'FW_CUD_CD', data.CUD_CD);
					setVueData(${popupid}_vue, 'FW_CIDR', data.CIDR);
					setVueData(${popupid}_vue, 'FW_PORT', data.PORT);
					setVueData(${popupid}_vue, 'FW_USE_MM', data.USE_MM);
					setVueData(${popupid}_vue, 'FW_TEAM_CD', data.TEAM_CD);
					setVueData(${popupid}_vue, 'FW_TEAM_NM', data.TEAM_NM);
					setVueData(${popupid}_vue, 'FW_USER_ID', data.FW_USER_ID);
					setVueData(${popupid}_vue, 'FW_USER_NAME', data.USER_NAME);
					setVueData(${popupid}_vue, 'FW_VM_USER_YN', data.VM_USER_YN);
					${popupid}_setEnabledFWForm(true);
				}
			}
			${popupid}_vmFwListGrid = newGrid("${popupid}_vmFwListGrid", ${popupid}_vmFwListGridOptions);
			$('#${popupid}_DISK_SIZE').css('width', '100px');
		}

		function ${popupid}_setEnabledFWForm(isEnabled){

				$('#${popupid}_FW_PORT').prop('disabled', !isEnabled);
				$('#${popupid}_FW_USE_MM').parent().find(':input').prop('disabled', !isEnabled);
				$('#${popupid}_FW_USER_NAME').parent().find(':input').prop('disabled', !isEnabled);
				$('#${popupid}_FW_VM_USER_YN').prop('disabled', !isEnabled);

		}

		function ${popupid}_vmFwSearch(){
			${popupid}_setEnabledFWForm(false);
			if(${popupid}_vue.form_data.VM_ID) {
				${popupid}_vmFwReq.searchSub('/api/fw/list', {VM_ID: ${popupid}_vue.form_data.VM_ID,INS_DT: ${popupid}_vue.form_data.INS_DT}, function(data) {
					${popupid}_vmFwListGrid.setData(data);
				});
			}
		}
		function ${popupid}_add_fw(grid){
			
			$("#${popupid}_fwAdd_popup_button").click();
		}

		function ${popupid}_initFWForm(){
			setVueData(${popupid}_vue, 'FW_IU', '');
			setVueData(${popupid}_vue, 'FW_IU_NM', '');
			setVueData(${popupid}_vue, 'FW_CUD_CD', '');
			setVueData(${popupid}_vue, 'FW_CIDR', '');
			setVueData(${popupid}_vue, 'FW_PORT', '');
			setVueData(${popupid}_vue, 'FW_USE_MM', '');
			setVueData(${popupid}_vue, 'FW_TEAM_CD', '');
			setVueData(${popupid}_vue, 'FW_TEAM_NM', '');
			setVueData(${popupid}_vue, 'FW_USER_ID', '');
			setVueData(${popupid}_vue, 'FW_USER_NAME', '');
			setVueData(${popupid}_vue, 'FW_VM_USER_YN', '');
		}

		function ${popupid}_delete_check_fw(grid){
			var rows = ${popupid}_vmFwListGrid.getSelectedRows();
			var data = rows[0];
			if(data == undefined) return;
			if(data.IU != undefined){
				if(data.IU == 'I'){
					grid.deleteRow();
					${popupid}_initFWForm();
					${popupid}_setEnabledFWForm(false);
				} else {
					data.IU = 'U';
					data.IU_NM = '<spring:message code="btn_delete" text="삭제"/>';
					data.CUD_CD = 'D';
					grid.updateRow(data);
				}
			} else {
				//if(data.CUD_CD && data.APPR_STATUS_CD)
				{
					data.IU = 'U';
					data.IU_NM = '<spring:message code="btn_delete" text="삭제"/>';
					data.CUD_CD = 'D';
					grid.updateRow(data);
				}
			}
		}
		
		function initSpecData(){
			post("/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC_SPEC",{"DC_ID": ${popupid}_param.DC_ID},function(data){
				fillOptionByData('SPEC_ID', data, '', "SPEC_ID", "SPEC_NM");
			});
		}
	</script> </fm-modal>
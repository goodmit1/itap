<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmtags" uri="http://fliconz.kr/jsp/tlds/fmtags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%
	String popupid = request.getParameter("popupid");
	String action = request.getParameter("action");
	String callback = request.getParameter("callback");

	request.setAttribute("callback", callback);

	if (popupid == null || "".equals(popupid)) {
		popupid = "";
	}
	if (action == null || "".equals(action)) {
		action = "insert";
	}
	request.setAttribute("popupid", popupid);
	request.setAttribute("action", action);
%>

 
<script src="/clovirsm/popup/vraInputJs.jsp"></script>
<style>
#${
popupid


}
.validationMessage{ 
	color:red;
	text-align: right;
}
#CATEGORY_DIV label{
	vertical-align:top
}
#CATEGORY_DIV .select2-container {
	width: calc(100% - 150px)  !important;
	 
}

.modal-dialog {
	width: 990px;
}

#${
popupid


}
.select2-search__field {
	width: 300px !important;
} 
.modal-body {
	height: 600px;
}

#${
popupid


}
.output {
	display: inline;
	padding-left: 10px;
	background-color: white;
	line-height: 43px;
	border: none;
	box-shadow: none !important;
    -webkit-box-shadow: none !important;
}

#${
popupid


}
select
,
#${
popupid


}
input


.input
,
#${
popupid


}
textarea {
	padding: 5px;
	border: 1px solid #b5b5b5;
}

.stepInput {
	max-width: 500px !important;
	 
}

.stepSelect {
	width: 500px !important;
	 
}

.step {
	padding: 10px;
}

.titleStyle {
	height: 28px;
	padding-top: 2px;
	padding-left: 12px;
	font-size: 20px;
	margin-top: 50px;
	margin-bottom: 20px;
	font-weight: 600;
}

#preBtn {
	display: none;
}

#finishBtn {
	display: none;
}

.back_blue:hover {
	background: #1b4583;
	color: white !important;
}

.back_blue:active {
	background: #1b4583;
	color: white !important;
}

.back_blue:focus {
	background: #1b4583;
	color: white !important;
}

.input-group-btn-vertical>.btn {
	height: 16px;
	width: 10px;
}

.input-group-btn-vertical i {
	position: absolute;
	top: 0;
	left: 6px;
}

.stepLabel {
	padding-left: 25px !important;
    text-align: left !important;
    max-width: 300px !important;
    width: 236px !important;
    margin-right: 3px !important;
}

.stepTableHader {
	background: #eee;
	border: 1px solid #ddd;
	padding: 10px;
}

.stepTable {
	border: 1px solid #ddd;
	padding: 10px;
}

.stepTableBottom {
	background: #eee;
	padding: 10px;
	border: 1px solid #ddd;
}

.serviceStyle {
	border: 1px solid rgb(181, 181, 181);
	min-width: 100px;
	padding: 5px;
}

.modal-body .select2-container--default.select2-container--focus .select2-selection--multiple
	{
	border: 1px solid rgb(181, 181, 181);
	outline: 0;
}

.modal-body .col-sm-12 .select2-container {
	width: 500px !important;
	 
}

.modal-body .col-sm-8 .select2-container {
	width: 427px !important;
	margin-left: 0px;
}

.modal-body .menu-layout-3 .select2-container .select2-selection--single
	{
	height: 95%;
	border-radius: 0px;
}

.modal-dialog .col {
	border-top: 1px solid #ddd;
	margin-top: -1px;
	border-bottom: 1px solid #ddd;
}

.modal-dialog .menu-layout-3 .select2-container .select2-selection--single
	{
	height: 27.53px;
	border-radius: 0px;
	padding-top: 2px;
}

.modal-dialog .popup_area .value-title {
	min-width: 142px;
	max-width: 140px;
	width: auto;
	background-color: #ebf2ff;
	font-weight: bold;
	text-align: center;
	height: 50px;
	font-size: 15px;
	padding-top: 17px;
}

.editor {
	height: 150px;
	padding: 1%;
	border: 1px solid #b5b5b5;
	border-radius: 0;
	word-wrap: break-word;
	overflow: auto;
	width: 82.6%;
	float: right;
	margin-right: 17px;
}

.application label {
	height: 65px !important;
}

.appTitle {
	font-size: 16px;
	font-weight: 900;
	color: #3c404a;
}

.panel-body {
	padding: 0px;
}
#newVRAPopup_form .approvalName-detail.in {
    position: absolute;
    top: 6px;
    color: white;
    font-weight: 900;
}
#newVRAPopup_form .approvalName-detail.out {
    position: absolute;
    top: 6px;
    color: #bbbbbb;
    font-weight: 900;
}
#newVRAPopup_form .approvalName-detail {
    position: absolute;
    top: 6px;
    color: #fb6c07;
    font-size: 14px;
    font-weight: 900;
    z-index: 80;
    height: 25px;
    overflow: hidden;
    text-overflow: ellipsis;
}

#newVRAPopup_form .progress-coming-back {
	position: absolute;
    top: 0px;
    width: 162px;
    height: 30px;
    border-radius: 45px;
    background-image: none;
    z-index: 30;
    padding: 8px;
    padding-top: 10px;
    background-color: white;
    border: 1px solid #d2d4d9;
}
#newVRAPopup_form .progress-done{
	position: absolute;
    top: 0px;
    width: 162px;
    height: 30px;
    border-radius: 45px;
    background-image: none;
    z-index: 30;
    padding: 8px;
    padding-top: 10px;
    background-color: white;
    border: 1px solid #d2d4d9;
}
#newVRAPopup_form .progress-done div{
	background-color: #d2d4d9 !important;
    color: white !important;
    height: 24px;
    width: 24px;
    font-size: 14px;
    font-weight: 900;
    padding: 3px;
    padding-left: 7px;
    border-radius: 50%;
    position: relative;
    top: -7px;
    left: -5px;
}
#newVRAPopup_form .progress-coming-in {
    width: 162px;
    position: absolute;
    height: 30px;
    background: none;
    border: 1px solid #fb6c07;
    background-color: #fb6c07;
    border-radius: 45px;
    text-align: center;
    padding: 1px;
    font-size: 20px;
    color: white;
    font-weight: bold;
    top: 0px;
    padding-top: 6px;
}

#newVRAPopup_form .vraProgress-coming-back {
    background-color: white !important;
    color: #fb6c07 !important;
}

#newVRAPopup_form .progress-coming-back div{
    background-color: #d2d4d9 !important;
	
}

#newVRAPopup_form .progress-coming-in div{
    background-color: #ffffff;
    color: #fb6c07;
    height: 24px;
    width: 24px;
    font-size: 14px;
    font-weight: 900;
    padding: 3px;
    padding-left: 2px;
    border-radius: 50%;
    position: relative;
    top: -3px;
    left: 1px;
}


#newVRAPopup_form .progress-done-in{
	width: 162px;
    position: absolute;
    height: 30px;
    background: none;
    border: 1px solid #fb6c07;
    background-color: #fb6c07;
    border-radius: 45px;
    text-align: center;
    padding: 1px;
    font-size: 20px;
    color: white;
    font-weight: bold;
    top: 0px;
    padding-top: 6px;
}

#newVRAPopup_form .progress-done-in div{
	background-color: #ffffff;
    color: #fb6c07;
    height: 24px;
    width: 24px;
    font-size: 14px;
    font-weight: 900;
    padding: 3px;
    padding-left: 2px;
    border-radius: 50%;
    position: relative;
    top: -3px;
    left: 1px;
}
</style>

<fm-modal id="${popupid}"
	title="<spring:message code="new_service" text="신규 배포 요청"/>"
	cmd="header-title"> <span slot="footer"
	id="${popupid}_footer"> <input type="button"
	class="btn delBtn popupBtn cancel top5"
	value="<spring:message code="label_cancel" text="취소"/>"
	onclick="cancelBtn();" /> <input type="button"
	class="btn exeBtn popupBtn prev top5" id="preBtn"
	value="<spring:message code="label_back" text="이전"/>"
	onclick="preBtn();" /> <input type="button"
	class="btn exeBtn popupBtn next top5" id="nextBtn"
	value="<spring:message code="label_next" text="다음"/>"
	onclick="nextBtn();" /> <input type="button"
	v-if="'${action}' == 'insert'"
	class="btn exeBtn back_blue popupBtn finish top5" id="finishBtn"
	value="<spring:message code="msg_jsf_complete" text="완료"/>"
	onclick="finishBtn();" /> <input type="button"
	v-if="'${action}' == 'update'"
	class="btn exeBtn back_blue popupBtn change top5" id="finishBtn"
	value="<spring:message code="btn_edit" text="수정"/>"
	onclick="finishBtn();" />

</span>
<form id="${popupid}_form" action="none">
	<template>
	<div class="panel-body">
		<div class="appTitle"></div>
		<div class="appProgressBar" id="appProgressBar">
			<div class="bar_progress in" style="width: 98%;">
				<div class="progress-coming-in zindex" id="step_p_1">
					<div>1</div>
				</div>
				<div class="vraProgress-done progress-done zindex">
					<div></div>
				</div>
				<span id="processStep"> </span>
				<div class="Rounded-Rectangle-Full" style="width: 100%; top: 15px;"></div>
				<div class="approvalName-detail vraApprovalName first in"
					style="left: 34px;">
					<spring:message code="msg_desc" text="기본정보" />
				</div>
				<div class="approvalName-detail vraApprovalName last out">
					<spring:message code="finish" text="완료" />
				</div>
			</div>
		</div>


		<div class="contents" id="contents" style="overflow: hidden auto;">
			<div id="stepStatus" class="stepStatus">
				<div id="step1_Status">
					STEP 1.
					<spring:message code="msg_desc" text="기본정보" />
				</div>
			</div>
			<div id="step1" class="step">
				<div class="col col-sm-12">
					<fm-input id="S_REQ_TITLE" name="REQ_TITLE"
						title="<spring:message code="REQ_TITLE" text="요청명"/>"
						required="true"></fm-input>
				</div>
				<div class="col col-sm-12" id="CATEGORY_DIV">
					<fm-select2 url="" id="_svc" name="CATEGORY"
						title=" <spring:message code='TASK' text='업무'/>" required="true"
						select_style="width: calc(100% - 150px);" onchange="purpose_auto()"></fm-select2>
				</div>
				<div class="col col-sm-3">
					<fm-select url="/api/code_list?grp=FAB" id="_fab" name="FAB" 
						title="FAB" required="true" callback="onAfterLoadFab"
						select_style=" width: calc(100% - 150px);"> </fm-input>
				</div>

				
				<div class="col col-sm-3">
					<fm-select url="" id="_purpose"
						name="S_PURPOSE"
						title="<spring:message code="PURPOSE" text="용도"/>" required="true"
						select_style="width: calc(100% - 150px);"> </fm-select>
			 
					<button type="button" onclick="taskPurposeMappingClick()" class="btn icon layout postion" id="purposeButton" style="position: absolute;top: 6px; padding: 0px !important; left: 90px; display:none;"><i class="fa fat add fa-plus-square"></i></button>
				</div>
				<div class="col col-sm-3">
					<fm-select url="/api/code_list?grp=P_KUBUN" id="_p_kubun"
						name="P_KUBUN" onchange="kubunChange(true);"
						callback="onAfterLoadKubun"
						title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>"
						required="true" select_style="width: calc(100% - 150px);">
					</fm-input>
				</div>
				<div class="createInfo col col-sm-3">
					<fm-select id="S_USE_MM" name="USE_MM"
						title="<spring:message code="NC_VM_USE_MM" text="사용기간(개월)"/>"
						required="true" select_style="width: calc(100% - 150px);">
					</fm-spin>
				</div>
				<div class="col col-sm-12">
					<div>
						<label for="S_CMT" class="control-label grid-title value-title"
							style="height: 150px;"><spring:message code="NC_IP_RSN" text="설명"/></label>
						<div class="editor" id="S_CMT" contenteditable="true"
							v-html="form_data.CMT"></div>
					</div>
				</div>
			</div>

		</div>

		<div id="stepFinish" class="contents stepFinish hide"
			style="padding-top: 30px;">
			<div id="stepFinishCmt"></div>

			<div class="btn_group">
				<fm-sbutton class="exeBtn popupBtn finish stepBtn" cmd="search"
					onclick="endModal()" style="font-size: 16px;"> <spring:message
					code="FINISH_NEXT_PAGE" text="" /></fm-sbutton>
			</div>
		</div>
		<fm-popup-button style="display:none;" popupid="taskPurposeMapping"
			popup="/clovirsm/popup/taskpurposeMapping.jsp" callback="purpose_auto" cmd="update" param="${popupid}_param">
		<spring:message code="btn_modify" text="생성 정보 수정" /></fm-popup-button>
	</div>
	</template>
</form>


<script>
		var varStep = 1;
		var action="${action}"
		var ${popupid}_userArr;
		var ${popupid} = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {USE_MM:12, FAB:"",P_KUBUN:"",PURPOSE:"", S_PURPOSE:"", CATEGORY:"", CMT:"<spring:message code="new_vra_CMT" text="1.서버 용도<br><br>2.서비스 목적<br><br>3.관련업무<br><br>4.기타"/>"};
		var ${popupid}_parent_vue = null;
		 
		var time = 0;
		var contents = {};
		var paramStep; 
		var DATA_JSON = new Object(); 
	 
		
		var popupid= "${popupid}";
		var FORM_INFO = new Object(); 
		var ${popupid}_callback = '${callback}';
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		function appEnvPut(){
			getAllSwEnv( function(){
				${popupid}_initPage();
			});
		} 
		function ${popupid}_initPage(){
			if(${popupid}_param.DATA_JSON){
				${popupid}_param.DATA_JSON = JSON.parse(${popupid}_param.DATA_JSON);
			}
			displayDraw(${popupid}_vue.form_data);
			
			
			if("${action}"== "disable"){
			
				inputDisable();
				 
				makeOutputHidden("_svc", ${popupid}_param.CATEGORY, ${popupid}_param.CATEGORY_NMS)
			}
		}
		
		function setCategory(param){
			fillOption("_svc", "/api/category/map/list/list_NC_CATEGORY_MAP_SELECT_TASK/?CATALOG_ID="+param.CATALOG_ID+"&ALL_USE_YN="+param.ALL_USE_YN, '', function(data){
				 
				if(data.length==0 && ADMIN_YN=='Y'){
					param.ALL_USE_YN = 'Y';
					setCategory(param);
					return;
				} 
				if(data.length == 1){
					$("#CATEGORY_DIV .select2").attr("style","display:none");
					$("#CATEGORY_DIV div:first").append("<div class='output'>"+data[0].CATEGORY_NM+"</div>");
					${popupid}_param.CATEGORY= data[0].CATEGORY_ID;
					$("#_svc").val(data[0].CATEGORY_ID)
					setTimeout(function(){ purpose_auto(); } , 100);
				} else{
					var categoryIds = ${popupid}_param.CATEGORY_IDS;
					if(categoryIds != null){
						var categoryArry = removeEmpty(categoryIds.split(","));
						${popupid}_param.CATEGORY=categoryArry[categoryArry.length-1];
					}
					else{
						//${popupid}_param.CATEGORY=$("#_svc option:first").val()
					}
					if(${popupid}_vue.form_data.all_inputs["_svc"].type=='array'){
						$("#_svc").val([])
						$("#_svc").attr("multiple", "multiple");
						 
						(new Select2Order("_svc", "<spring:message code="no_multiple_tasks" text="업무를 여러 개 선택하실 수 있습니다." />")).init()
						$(".select2-search__field").css("width","200px");
					}
					//$("#_svc").select2();
				}
			}, "CATEGORY_ID", "CATEGORY_NM");
		}
		 
		function makeOutputHidden(id, val, text){
			var name = $("#" + id).attr("name")
			var parent = $("#" + id).parent(); 
			parent.find(".select2").remove();
			$("#" + id).remove();
			var html = '<div  class="output">' + text + '<input type="hidden" id="' + id + '" name="' + name + '" value="' + val + '"></div>';
			parent.append(html);
		 
		}
		function ${popupid}_getFormData(){
			var pramTemp = new Object();
			pramTemp.CATALOG_ID = ${popupid}_vue.form_data.CATALOG_ID;
			pramTemp.ORG_CATALOG_ID = ${popupid}_vue.form_data.ORG_CATALOG_ID;
			pramTemp.FIXED_JSON = ${popupid}_vue.form_data.FIXED_JSON;
			if(!${popupid}_vue.form_data.step){
				post('/api/vra_catalog/list_step',pramTemp, function(data){
					 ${popupid}_vue.form_data.step = data;
					 ${popupid}_getFormData();
				});
			}
			else if(${popupid}_vue.form_data.CATALOGREQ_ID && !${popupid}_vue.form_data.FORM_INFO){
				post('/api/vra_catalog/info',pramTemp, function(data){
					${popupid}_vue.form_data.FORM_INFO = data.FORM_INFO;
					${popupid}_vue.form_data.DATA_JSON = data.DATA_JSON;
					${popupid}_vue.form_data.FEE = data.FEE;
					${popupid}_getFormData();
					
					 
				});
			}
			else{
				appEnvPut();
			}
			
		}
		function ${popupid}_click_default(inputs,  key1, key2){
			if(inputs[key1] && inputs[key1]['default']){
				${popupid}_param[key2] = inputs[key1]['default']
				return ${popupid}_param[key2];
				
			}
			return null;
		}
		function onAfterLoadFab(data){
			/*var all_inputs = ${popupid}_vue.form_data.all_inputs;
			try{
				removeOptionNotExist("_fab", 0, all_inputs["_fab"].enum);
			}
			catch(e){
				//console.log(e)
			}*/
		}
		function onAfterLoadKubun(data){
			/*var all_inputs = ${popupid}_vue.form_data.all_inputs;
			try{
				removeOptionNotExist("_p_kubun", 0, all_inputs["_p_kubun"].enum);
			}
			catch(e){
				//console.log(e)
			}*/
		}
		function ${popupid}_click(vue, param){
			${popupid}_parent_vue = vue;
			var name = param.CATALOG_NM;
			var msg = ' <spring:message code="addTemplatInfo" text=" 카탈로그를 사용하여 신규 서비스를 요청합니다." arguments="###"/>'
			msg = msg.replace('###', name);
			$('.appTitle').text(msg);
			 
			 
			if(param != null ){
				${popupid}_param = $.extend(${popupid}_param,param);
			}
			if(param.FORM_INFO){
				var inputs = JSON.parse(param.FORM_INFO).inputs;
				${popupid}_vue.form_data.all_inputs = inputs;
				if(param.FIXED_JSON){
					var fixed = JSON.parse(param.FIXED_JSON);
					 
					if(fixed["_purpose"]){
						 
						inputs["_purpose"] = $.extend(inputs["_purpose"],{ default : fixed["_purpose"] });
					}
					 
				}
				var purpose =  null;
				if(inputs){
					purpose = ${popupid}_click_default(inputs, "_purpose", 'S_PURPOSE');
					if(purpose != null){
						makeOutputHidden("_purpose",purpose, purpose.toUpperCase() )
					}
				} 
					 
				if(purpose == null){
					$("#purposeButton").css("display","block");
				}
				
				setTimeout(function(){
					
					makeSelectOutputHidden('FAB');
					makeSelectOutputHidden('P_KUBUN');
					kubunChange(true);
				},500)
			}
			
			//
			
			if(param.KUBUN=="O") {
				
				$(".createInfo").hide(); 
			}
			 
			
			 
			if("${action}" == "update"){
				if(param.CATALOGREQ_ID){
					//${popupid}_vue.form_data = ${popupid}_param;
					${popupid}_getFormData();
					
					return true;
				}
				else {
					delete_req_form_popup_param = null;
					alert(msg_select_first);
					return false;
				}
			}else{
				setCategory(param);
				//${popupid}_vue.form_data = ${popupid}_param;
				${popupid}_getFormData();
				return true;
			}
		}
		
		// 취소버튼에 대한 function
		function cancelBtn(){
			$('#${popupid}').modal('hide');
		}
		
		//  다음 버튼에 대한 function
		function nextBtn(){
			if($(".validationMessage:visible").length > 0){
				return false;
			}
			// validate 메소드로 빈곳을 체크 , numValidate으로 숫자 체크
			if(validate("contents") && numValidate("contents")){
				varStep++;
				if(varStep > 1){
					if($("#S_CMT").text().length > 2000){
						alert("설명을 2000자 내로 작성해 주세요.");		
						varStep--;
						return true;
					}
					var svc = getVueObj($("#_svc")).getEtcValue("CATEGORY_CODE")
					if(isEmpty(svc)){
						alert("업무코드가 없습니다. 관리자에게 연락하세요.");
						varStep--;
						return true;
					}
					if(!HangeulAndEnglishAndNumberVaildate($("#S_REQ_TITLE"))){
						varStep--;
						return true;
					}
					$("#contents #step"+(varStep-1)).addClass("step hide");
					$("#contents #step"+varStep).removeClass("hide");	
					$("#preBtn").css("display","inline-block");
					$('#step1_Status').addClass("hide"); 
					$('#step'+(varStep-1)+'_Status').addClass("hide"); 
					$('#step'+varStep+'_Status').removeClass("hide"); 
					if(varStep == 2)
						imgChange();
						kubunChange(false);
				} 
				if (varStep == paramStep){
					resultData();
					$("#contents #step"+varStep).addClass("step hide");
					$('#step'+(varStep-1)+'_Status').addClass("hide"); 
					$('#step'+paramStep+'_Status').removeClass("hide"); 
					$("#contents #step"+paramStep).removeClass("hide");	
					$("#finishBtn").css("display","inline-block");
					$("#nextBtn").css("display","none");
					$(".appTitle").css("display","none");
				}
				stepProgressDisplay(varStep);
				select2Orders.onShow();
				
			}
			
		}
		
		// 뒤로가기 버튼에 대한 function
		function preBtn(){
			varStep--;
			$("#finishBtn").css("display","none");
			if(varStep < paramStep){
				
				$("#nextBtn").css("display","inline-block");
				$("#contents #step"+(varStep+1)).addClass("step hide");
				$("#contents #step"+varStep).removeClass("hide");	
				$("#contents #step"+paramStep).addClass("step hide");
				$('#step'+varStep+'_Status').removeClass("hide");
				$('#step'+(varStep+1)+'_Status').addClass("hide");
				
			}
			if(varStep == 1){
				$('#step1_Status').removeClass("hide");
				$("#preBtn").css("display","none");	
			} 
			stepProgressDisplay(varStep);
		}
		
		// step에 따라 view 제어
		function stepProgressDisplay(varStep){
			$("#${popupid} .vraApprovalName-detail").removeClass("in");
			$("#${popupid} .vraApprovalName-detail").addClass("out");
			if(varStep == 1){
				$("#${popupid} #step_p_1").attr("class","progress-coming-in");
				$("#${popupid} .vraApprovalName.first").addClass("in");
				$("#${popupid} .bar_progress > span > .progress-coming-in ").attr("class","progress-coming-back vraProgress-coming-back");
				
			} else if(varStep == paramStep){
				
				$("#${popupid} #step_p_"+paramStep+"").attr("class","progress-done-in");
				$("#${popupid} .bar_progress > span > .progress-coming-back , .bar_progress > span > .progress-coming-in").eq(varStep-3).prevAll(".progress-coming-in, .progress-coming-out").attr("class","progress-coming-in vraProgress-coming-back");
				$("#${popupid} .bar_progress > span > .progress-coming-back , .bar_progress > span > .progress-coming-in").eq(varStep-3).attr("class","progress-coming-in vraProgress-coming-back");
				$("#${popupid} .approvalName-detail.last").removeClass("out");
				$("#${popupid} .approvalName-detail.last").addClass("in");
				$("#${popupid} .bar_progress > span > .vraApprovalName-detail").eq(varStep-3).attr("class","approvalName-detail vraApprovalName-detail");
				$("#${popupid} .bar_progress > span > .vraApprovalName-detail").eq(varStep-3).prevAll(".vraApprovalName-detail").attr("class","approvalName-detail vraApprovalName-detail");
				$("#${popupid} .bar_progress > span > .progress-coming-back ").attr("class","progress-coming-in vraProgress-coming-back");
				
			} else{
				$("#${popupid} #step_p_1").attr("class","progress-coming-in vraProgress-coming-back");
				$("#${popupid} .progress-done-in").attr("class","progress-done");
				$("#${popupid} .vraApprovalName.first").removeClass("in");
				$("#${popupid} .vraApprovalName.last").addClass("out");
				$("#${popupid} .vraApprovalName.last").removeClass("in");
// 				$("#step_p_"+paramStep+"").attr("class","progress-coming-in");
				$("#${popupid} .bar_progress > span > .progress-coming-back , .bar_progress > span > .progress-coming-in").eq(varStep-2).prevAll(".progress-coming-in, .progress-coming-out").attr("class","progress-coming-in vraProgress-coming-back");
				$("#${popupid} .bar_progress > span > .progress-coming-back , .bar_progress > span > .progress-coming-in").eq(varStep-2).nextAll(".progress-coming-in, .progress-coming-out").attr("class","progress-coming-back vraProgress-coming-back");
				$("#${popupid} .bar_progress > span > .progress-coming-back , .bar_progress > span > .progress-coming-in").eq(varStep-2).attr("class","progress-coming-in");
				$("#${popupid} .bar_progress > span > .vraApprovalName-detail").eq(varStep-2).prevAll(".approvalName-detail").attr("class","approvalName-detail vraApprovalName-detail");
				$("#${popupid} .bar_progress > span > .vraApprovalName-detail").eq(varStep-2).nextAll(".approvalName-detail").attr("class","approvalName-detail vraApprovalName-detail out");
				$("#${popupid} .bar_progress > span > .vraApprovalName-detail").eq(varStep-2).attr("class","approvalName-detail vraApprovalName-detail in");
				
				
			}
		}
		var step_purpose=[];
		// Step 과 contents를 그려주는 함수
		function displayDraw(param){
			 
			paramStep = param.step.length +2;
			var stepWidth = 100/(paramStep-1) - 5;
			

			// step 값 변경
			$('.progress-done').attr("id","step_p_"+paramStep);
			$('.progress-done').children('div').html(paramStep);
			
			
			//step을 출력 
			var left = 0;
			var leftPx= 0;
			var leftTextPx= 0;
			step_purpose=[];
			for(var i = 0 ; i < param.step.length ; i++){
				
				left += stepWidth;
				$('#processStep').append('<div class="vraProgress-coming-back progress-coming-back zindex" id="step_p_'+Number(i+2)+'" style="left:calc('+left+'% + -20px);"> '+
				'<div>'+Number(i+2)+'</div>'+
				'</div>');
			}
			
			//step name을 출력
			left = 0;
			for(var i = 0 ; i < param.step.length ; i++){
				left += stepWidth;
				leftTextPx += -10;
				if(param.step[i].STEP_NM == "ETC"){
					$('#processStep').append('<div class="approvalName-detail vraApprovalName-detail out" style="left: calc('+left+'% + 15px); width: 120px; text-align: left;"><spring:message code="etc" text="기타"/></div>');
					$('#stepStatus').append('<div id="step'+(i+2)+'_Status" class="hide">STEP '+(i+2)+'. <spring:message code="etc" text="기타"/> </div>');
				}
				else{
					$('#processStep').append('<div class="approvalName-detail vraApprovalName-detail out" style="left: calc('+left+'% + 15px); width: 120px; text-align: left;">'+param.step[i].STEP_NM+'</div>');					
					$('#stepStatus').append('<div id="step'+(i+2)+'_Status" class="hide">STEP '+(i+2)+'. '+param.step[i].STEP_NM+'</div>');
				}
				leftTextPx += 40;
			}
			
			$('#stepStatus').append('<div id="step'+(param.step.length+2)+'_Status" class="hide" >STEP '+(param.step.length + 2)+'. <spring:message code="finish" text="완료"/></div>');
			
			//contents를 출력
			for(var i = 0 ; i < param.step.length ; i++){
				contents =  JSON.parse(param.step[i].INPUTS);
				var purpose =  ""; //TODO Role명을 찾아서 해야 함.
				var tag = param.step[i].TAGS+''; 
				var step = param.step[i].STEP_NM;
				var appEnvList;
				var z = 0;
				$('#contents').append('<div id="step'+Number(i+2)+'" class="step hide" data-step-name="' + param.step[i].STEP_NM + '">');
				
				  if(tag.indexOf('VM') != -1){
		               purpose =  ""; //TODO Role명을 찾아서 해야 함.
		               var pos = tag.indexOf("sw_purpose:");
		               if(pos>1){
		                  purpose = tag.substring(pos+8);
		               }
		               step_purpose[i] = purpose
				  }
				  displayApp(i, contents );
				  if(tag.indexOf("app:") != -1){
					  
					  
					  var app = getInnerStr(tag,"app:", ",") ;
					  var appList = app.split("|");
					  
					  param.step[i].applicationName =  getInnerStr(tag,"appName:", ",") ;
					  displayAppEnvByNm(appList, i);
				  }						
				  
   				   
			}
			 
			$('#contents').append('<div id="step'+paramStep+'" class="step hide">');
		}
		
		
		
		
		
		
		
		 
		function putDefaultDataJSON(  content){
			if(!DATA_JSON[content.name] &&  content.default){
				DATA_JSON[content.name] = content.default
			}
		}
		 
		
		function getKubunVal(){
			return $("#_p_kubun").val() ? $("#_p_kubun").val():"";
		}
		function kubunChange(first){
				var specParam = new Object();
				var dataObj = {'CPU':[], 'RAM':[], 'DISK':[], 'MONTH':[], 'GPU':[]}
				
				specParam.ID = getKubunVal();
				post("/api/code_list?grp=dblist.com.clovirsm.common.Component.selectSpecSetting",specParam, function(data){
					for(var q = 0 ; q < data.length; q++){
						valList = data[q].VAL1.split(",");
						
						for(var w = 0 ; w < valList.length; w++){
							var valListS = valList[w].split("|");
							var object = new Object();
							if(valListS.length > 1){
								object.title = valListS[1];
								object.val = valListS[0];
							} else{
								object.title = valList[w];
								object.val = valList[w];
							}
							dataObj[data[q].KUBUN].push(object);
							 
						}
					}	 
						  
						
						
					var mem = $(".memSelect:not(.enum)");
					if(mem.length > 0){
						for(var i = 0 ; i < mem.length ; i++){
							fillOptionByData(mem[i].name, dataObj['RAM'], '', "val", "title");
							dataJSONSet(mem[i].name);
						}
					}
				
					var cpu = $(".cpuSelect:not(.enum)");
						
					if(cpu.length > 0){
						for(var i = 0 ; i < cpu.length ; i++){
							fillOptionByData(cpu[i].name, dataObj['CPU'], '', "val", "title");
							dataJSONSet(cpu[i].name);
						}
					}
				
					var disk = $(".diskSelect");
					
					if(disk.length > 0){
						for(var i = 0 ; i < disk.length ; i++){
							fillOptionByData(disk[i].id, dataObj['DISK'], '', "val", "title");
							dataJSONSet(disk[i].name);
						}
					}
					if(first){
						fillOptionByData("S_USE_MM", dataObj['MONTH'], null, "val", "title");
						//$("#S_USE_MM").val(monthArray[0].val)
						${popupid}_param.USE_MM=dataObj['MONTH'][0].val
					}
					 
						
					 
				},false);
				
		}
		function dataJSONSet(name){
			try{
			
				$("#" + name).val(${popupid}_param.DATA_JSON[name])
			}
			catch(e)
			{
				$("#" + name).val(DATA_JSON[name])
			}
		}
		/*function flavorChange(i){
			if($('#flavor_'+i+' option:selected')[0].data)
				$('#flavorPay').html('<spring:message code="label_cost" text="비용(원)"/> : '+ formatNumber($('#flavor_'+i+' option:selected')[0].data.FEE));
			
		}*/
		
		function isNull(v) {
		    return (v === undefined || v === null) ? true : false;
		}
		
		function AddMonthDate(num){
			var date = new Date();
			
			date.addMonths( Number(num));
			return formatDate(date, 'date')

			
		}
		//DATAJSON에  ENV parameter 추가
		
		//DATAJSON에  공통 parameter 추가
		function putExtraDataJSON(){
			DATA_JSON._svc = getVueObj($("#_svc")).getEtcValue("CATEGORY_CODE")
			 
			DATA_JSON._fab = $("#_fab").val();
			DATA_JSON._p_kubun = $("#_p_kubun").val();
			DATA_JSON._purpose = $("#_purpose").val().toLocaleLowerCase();
			
		}
		
		 
		function resultData(){
			if("${action}" != "disable"){
				DATA_JSON = new Object(); 
			 
				
				//var formData = $("#${popupid}_form").serializeObject()
				for(var i = 0 ; i < ${popupid}_param.step.length; i++){
					var contents =  JSON.parse(${popupid}_param.step[i].INPUTS);
					for(var z = 0 ; z < contents.length; z++){
						var content = contents[z];
						var name = content.name;
						var val =  $("#" + name).val();
						if(select2Orders.has(name)){
							val =select2Orders.get( name).val();
						}
					 	if(content.fieldName=='application'){
					 		 
					 		var envname = "_"+ name +"_param";
							DATA_JSON[envname] = putEnvDataJSON( ${popupid}_param.step[i].STEP_NM);
					 	}
					 	if(content.type=="include"){
					 		try{
					 			val = eval("get_" + name +  "()");
					 			 
					 		}
					 		catch(e){
					 			alert(e.message);
					 			return;
					 		}
					 	}
						if(val != null) DATA_JSON[name] = val
					}
					if(${popupid}_param.step[i].applicationName){
						DATA_JSON["_" + ${popupid}_param.step[i].applicationName + "_param"] = putEnvDataJSON(${popupid}_param.step[i].STEP_NM);
					}
					
				}
				 
				putExtraDataJSON();
				if(DATA_JSON["_svc"] == '' || !DATA_JSON["_svc"]){
					alert("<spring:message code="no_task_code" text="업무 코드가 없습니다. 관리자에게 문의하세요."/>");
					$('#${popupid}').modal('hide');
					return;
				}
				
			} 
			else{
				DATA_JSON = ${popupid}_vue.form_data.DATA_JSON
			}
			FORM_INFO = ${popupid}_param.FORM_INFO;
			 
			DEPLOY_TIME = ${popupid}_param.DEPLOY_TIME;
			post('/api/monitor/info/select_dc_by_fab/', { FAB:$("#_fab").val(),P_KUBUN: $("#_p_kubun").val() }, function(data){
				${popupid}_param.DC_ID=data.DC_ID;
				if(data.ZONE_TAG) DATA_JSON._zone_tag=data.ZONE_TAG;
			})
			post('/api/vra_catalog/summary',{FORM_INFO : FORM_INFO, DATA_JSON : JSON.stringify(DATA_JSON), FIXED_JSON: ${popupid}_param.FIXED_JSON, DEPLOY_TIME: DEPLOY_TIME },function(data){
				resultPage(data);
			});
		}
		
		function getSeletedSWInfo(){
			var SW_INFO={};
			$(".application").each(function(index, item){
				SW_INFO[$(item).parents(".step").attr("data-step-name")] = $(".application select").val();
			})
			return SW_INFO;
			 
		}
		function resultPage(data){
			var fee = 0;
			var count = 0;
			time = 0;
			var html;
			
			var category = $("#_svc option:selected");
			var categoryNm = '';
			for(var i = 0; i < category.length; i++){
				
				if(i != category.length -1)
					categoryNm += category[i].text + " <br> ";
				else
					categoryNm += category[i].text;
				
			}
			$('#step'+paramStep).html('');
			$('#step'+paramStep).append('<table style="width:100%;" id="step'+paramStep+'-info-table"></table>');
			if(${popupid}_param.KUBUN != 'O'){
				html += '<tr><td class="stepTableHader"><spring:message code="title" text="제목"/></td>'+
						'<td class="stepTable" colspan="3">'+ $('#S_REQ_TITLE').val()+'</td></tr>'+
						'<tr><td class="stepTableHader"><spring:message code="TASK" text="업무"/></td><td  colspan="3" class="stepTable">'+categoryNm+'</td></tr>';
				$('#step'+paramStep).append('<div class="col col-sm-12" id="step'+paramStep+'-end-vm" style="border: none;margin-top: 15px; font-size: 17px; margin-bottom: 15px;"></div>');
				if(data.length != 0){
				$('#step'+paramStep+'-end-vm').append('VM <spring:message code="CREATE" text="생성"/>');
					$('#step'+paramStep).append('<table style="width:100%;" id="step'+paramStep+'-end-table"></table>');
					$('#step'+paramStep+'-end-table').append('<tr><td class="stepTableHader">VM <spring:message code="naming_rule" text="명명 규칙"/></td>'+
							'<td class="stepTableHader"><spring:message code="OS" text="OS"/></td>'+
							'<td class="stepTableHader"><spring:message code="SW" text="SW"/></td>'+
							'<td class="stepTableHader"><spring:message code="NC_VM_SPEC_SPEC_INFOM" text="사양"/></td>'+
							'<td class="stepTableHader"><spring:message code="ADD_DISK" text="이름"/>(GB)</td>'+
							'<td class="stepTableHader"><spring:message code="cnt" text="개수"/></td>'+
							'</tr>');
					for(var i = 0 ; i < data.length ; i++){
						$('#step'+paramStep+'-end-table').append('<tr><td class="stepTable">'+data[i].NAME+'</td>'+
						'<td class="stepTable">'+data[i].IMAGE+'</td>'+		
						'<td class="stepTable">'+data[i].SW +'</td>'+		
						'<td class="stepTable">'+data[i].FLAVOR+'</td>'+
						'<td class="stepTable">'+data[i].DISK_SIZE+'</td>'+
						'<td class="stepTable">'+data[i].COUNT+'</td>'+
						'</tr>');
						count += Number(data[i].COUNT);
						fee += Number(data[i].FEE);
						time += Number(data[i].DEPLOY_TIME);
					}
// 					$('#step'+paramStep+'-info-table').append('<tr>'+
// 							'<td class="stepTableHader"><spring:message code="TASK" text="업무"/></td>'+
// 							'<td class="stepTable" id="totalFee">'+formatNumber(fee)+'</td></tr>');
				}
			}
			 
			$('#step'+paramStep).append('<div class="col col-sm-12" id="st ep'+paramStep+'-end-info" style="border: none; margin-top: 15px; font-size: 17px; margin-bottom: 15px;"></div>');
			if(${popupid}_param.KUBUN != 'O'){
				html += '<tr><td class="stepTableHader"><spring:message code="label_term" text="기간"/></td> <td class="stepTable"> '+$('#S_USE_MM').val()+'<spring:message code="MONTH" text="개월"/> <span style="color: #2b9fe2;">('+AddMonthDate($('#S_USE_MM').val())+'<spring:message code="EXPIRE" text="만료 예정"/>)</span></td>';
				 
				//html+='<td class="stepTableHader"><spring:message code="DEPLOY_TIME" text="생성"/> </td><td class="stepTable"> '+ (time ? time: "10") +' <spring:message code="DEPLOY_TIME_m" text=""/></td></tr>';
			
			}
			else{
				$(".createInfo :input").val("");
				html += '<tr><td class="stepTableHader"><spring:message code="title" text="제목"/></td>'+
				'<td class="stepTable" colspan="3">'+ $('#S_REQ_TITLE').val()+'</td></tr>'+
				'<tr><td class="stepTableHader"><spring:message code="TASK" text="업무"/></td><td  colspan="3" class="stepTable">'+categoryNm+'</td></tr>';
		
				//html+='<tr><td class="stepTableHader"><spring:message code="DEPLOY_TIME" text="생성"/> </td><td class="stepTable"> '+ (time ? time: "10") +' <spring:message code="DEPLOY_TIME_m" text=""/></td></tr>';
			
			}
				$('#step'+paramStep+'-info-table').append(html);
		}
		 
		function finishBtn(){
			var param = ${popupid}_param ;
			
			 
			param.DATA_JSON = JSON.stringify(DATA_JSON);
			param.CATEGORY = getFirstVal($("#_svc").val());
			param.PREDICT_DD_FEE = Number(($('#totalFee').val() ? $('#totalFee').val() : 0) + (${popupid}_param.FEE?${popupid}_param.FEE:0));
			param.APPR_STATUS_CD = 'W';
			var url = '/api/vra_catalog/insertReq';
			param.CUD_CD = 'C'
			if ('${action}' == 'update'){
				param.CUD_CD = 'U';
				url = '/api/vra_catalog/updateReq';
			}
			
			param.REQ_TITLE=param.REQ_TITLE.replace(/\//g," ")

			param.DEPLOY_REQ_YN = 'Y';
			$("#S_CMT").html(removeBRP($("#S_CMT").html()))
			param.CMT = $("#S_CMT").text()
 			post(url,param , function(data){
 					if(${popupid}_callback != ''){
 						eval(${popupid}_callback + '();');
 					}
 					$('#${popupid}').modal('hide');
					FMAlert("<img src='/res/img/hynix/confirm.png' style='margin-bottom:20px;margin-top: 20px;'><div style='font-size: 16px; font-weight: 900; margin-bottom:20px;'><spring:message code="REQ_MSG_M1" text=""/></div><div><spring:message code="REQ_MSG_M2" text=""/></div><div><spring:message code="REQ_MSG_M3" arguments='"+time+"' text=""/></div><div><spring:message code="REQ_MSG_M4" text=""/></div>",'<spring:message code="request_complete" text="요청완료" />','<spring:message code="go_history_page" text="신청이력페이지로 이동" />', function(){location.href="/clovirsm/workflow/requestItemsHistory/index.jsp"; } );					 
 					
 			});
		}
		 
		function inputDisable(){
			$("#contents input").each(function(){
				var id = $(this).attr("id");
				var name = $(this).attr("name");
				makeOutputHiddenAll(id,name)
			});
				 
			$("#contents select").each(function(){
				var id = $(this).attr("id");
				var name = $(this).attr("name");
				makeOutputHiddenAll(id,name)
			});
			$("#contents textarea").attr("disabled",true);
			
		}
		 
		function makeOutputHiddenAll(id, name){
			var val =  ${popupid}_param[name];
			if(!val){
				val = ${popupid}_param.DATA_JSON[name];
			}
			makeOutputHidden(id, val, val)
		}
		
		function purpose_auto(obj){
			
			fillOption("_purpose", '/api/category/map/list/list_NC_CATEGORY_MAP_PURPOSE/?CATEGORY_ID='+ getFirstVal($("#_svc").val()), '', function(data){
				if(obj && obj.ID){
					${popupid}_param.S_PURPOSE = obj.ID.toLowerCase();
				} 
			}, "ID", "TITLE");
			
		}
		
		function makeSelectOutputHidden(name){
			var id = $("select[name='" + name + "']").attr("id")
			if( $("#"+id +" option:selected").text() == '') return;
			var html = '<div  class="output">' + $("#"+id +" option:selected").text() + '</div>';
			$("#" + id).after(html);
			$("#" + id).hide();
		}
 	
		function endModal(){
			location.href="/clovirsm/workflow/requestItemsHistory/index.jsp";
			return true;

		}
		
		function taskPurposeMappingClick() {
			if($("#_svc").val() != null && $("#_svc").val() != ''){
				${popupid}_param.CATEGORY_ID=getFirstVal($("#_svc").val());
				$('#taskPurposeMapping_button').trigger('click');
			} else{
				alert("<spring:message code="select_task" text="업무를 선택해주세요."/>")
			}
		};
	</script> </fm-modal>
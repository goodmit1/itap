<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%
	String popupid = request.getParameter("popupid");
	if(popupid == null || "".equals(popupid)){
		popupid = "thin_client_search";
	}
	request.setAttribute("popupid", popupid);
%>
<fm-modal id="${popupid}" title="<spring:message code="THIN_CLIENT_SEARCH" text="씬 클라이언트 검색" />" cmd="header-title" >
	<form id="${popupid}_form" action="none">
		<template>
			<div class="panel-body">
				<div class="search_area">
					<div class="col col-sm-3 colmargin3">
						<fm-select id="${popupid}_user_keyword_field" :options="{SN:'<spring:message code="NC_THIN_CLIENT_SN" text="SN" />', 'MODEL_NM':'<spring:message code="NC_THIN_CLIENT_MODEL_NM" text="모델명" />', MAC_ADR:'<spring:message code="NC_THIN_CLIENT_MAC_ADR" text="MAC" />'}" ></fm-select>
					</div>
					<div class="col col-sm-6 colmargin6">
						<fm-input id="${popupid}_user_keyword" name="keyword" placeholder="<spring:message code="label_keyword" text=""/>"  ></fm-input>
						<input type="text" name="DUMMY" style="display:none"/>
					</div>
					<div class="col btn_group nomargin">
							<input type="button" onclick="${popupid}_thin_client_search()" class="searchBtn btn" value="<spring:message code="btn_search" text="검색"/>" />
					</div>
				</div>
			</div>
			<div id="${popupid}_thinClientSearchGrid" style="height:250px" class="ag-theme-fresh"></div>
		</template>
	</form>

	<script>
		var ${popupid} = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {keyword:''};
		var ${popupid}_parent_vue = null;
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		function ${popupid}_click(vue, param){
			${popupid}_parent_vue = vue;
			${popupid}_vue.form_data = ${popupid}_param;
			${popupid}_thin_client_search('');
			return true;
		}

		function ${popupid}_thin_client_search(param){
			${popupid}_thinClientSearchReq.searchSub('/api/thin_client/list?STATUS=W', ${popupid}_param, function(data) {
				${popupid}_thinClientSearchGrid.setData(data);
			});
		}

		var ${popupid}_thinClientSearchReq = new Req();
		var ${popupid}_thinClientSearchGrid;
		$(document).ready(function(){
			var ${popupid}_thinClientSearchGridColumnDefs =[
				{headerName : "<spring:message code="NC_THIN_CLIENT_SN" text="SN" />", field : "SN"},
				{headerName : "<spring:message code="NC_THIN_CLIENT_MODEL_NM" text="모델명" />", field : "MODEL_NM"},
				{headerName : "<spring:message code="NC_THIN_CLIENT_MAC_ADR" text="MAC" />", field : "MAC_ADR"},
				{headerName : "<spring:message code="NC_THIN_CLIENT_STATUS" text="상태" />", field : "STATUS"
					, valueGetter:function(params){
						if(params.data && params.data.STATUS_NM){
							return params.data.STATUS_NM;
						}
						return "";
					}
					, cellStyle: function(params){
						if(params.data && params.data.STATUS == 'X'){
							return { color: 'red' };
						}
						return null;
					}
				}
			];
				
			var ${popupid}_thinClientSearchGridOptions = {
				columnDefs: ${popupid}_thinClientSearchGridColumnDefs,
				rowData: [],
				sizeColumnsToFit:true,
				enableSorting: true,
				rowSelection:'single',
			    enableColResize: true,
			    onRowDoubleClicked: function(){
					var arr = ${popupid}_thinClientSearchGrid.getSelectedRows();
					var callback = ${popupid}_parent_vue ? ${popupid}_parent_vue.callback: null;
					if(callback != null) eval( callback + '(arr[0], function(){ $(\'#${popupid}\').modal(\'hide\'); });');
					$('#${popupid}').modal('hide');
			    }
			};
			${popupid}_thinClientSearchGrid = newGrid("${popupid}_thinClientSearchGrid", ${popupid}_thinClientSearchGridOptions);
		});
	</script>
</fm-modal>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %><%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<!--  VM 정보 수정 -->
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("action", action);
	String direct = request.getParameter("direct");
	request.setAttribute("direct", direct);
	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);
%>
<style>
	#${popupid} .modal-dialog {
		width: 1200px;
	}
	/* #${popupid}_NAS_TABLE > div:nth-child(2) > i {
		top: 50%;
    	transform: translateY(-50%);
	} */
</style>
<fm-modal id="${popupid}" title="<spring:message code="label_vm_info" text=""/>" cmd="header-title">
<link href="/res/css/editor.css" type="text/css" rel="stylesheet"/>
<script src="/res/js/editor.js"></script>
	<span slot="footer">
		 
		<input v-if="isShowReqBtn('${isAppr}','${direct}' ,'${action}', form_data,'save')" type="button" class="btn saveBtn contentsBtn tabBtnImg top5 save" value="<spring:message code="btn_save" text="저장" />" onclick="${popupid}_save()" />
		<span v-if="isShowReqBtn('${isAppr}','${direct}' ,'${action}', form_data,'owner')"><fm-popup-button   popupid="vm_owner_change_popup" popup="/popup/owner_change_form_popup.jsp?svc=vm&key=VM_ID&callback=onAfterVMOwnerChg" cmd="update contentsBtn tabBtnImg top5 sync" param="vmReq.getData()"><spring:message code="btn_change_owner" text="담당자변경"/></fm-popup-button></span>
	</span>
	<div class="form-panel detail-panel">
		<form id="${popupid}_form" action="none">
			<div class="panel-body">
				<div class="col col-sm-12" style="border-left-color: transparent;border-right-color: transparent;">
					<div class="fm-output">
						<label class="control-label grid-title value-title" style="background-color:white;">서버정보</label>
					</div>
				</div>
				
				<div class="col col-sm-6">
					<fm-output id="${popupid}_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="VM명"/>"></fm-output>
				</div>
				<div class="col col-sm-6" v-if="${action == 'appr' }">
					<fm-output id="${popupid}_GUEST_NM" name="GUEST_NM" title="<spring:message code="NC_VM_GUEST_NM" text="OS명"/>"></fm-output>
				</div>
				<div class="col col-sm-6" v-else-if="${action != 'insert' && action != 'update'}">
					<fm-output id="${popupid}_GUEST_NM" name="GUEST_NM" title="<spring:message code="NC_VM_GUEST_NM" text="OS명"/>"></fm-output>
				</div>
				<div class="col col-sm-12" v-if="${action == 'insert' || action == 'update'}">
					<jsp:include page="/clovirsm/popup/_spec_form.jsp"></jsp:include>
				</div>
			  	<div class="col col-sm-6" >
				 	<fm-output id="${popupid}_SPEC_INFO" name="SPEC_INFO" title="<spring:message code="NC_VM_SPEC_NM" text="사양"/>" 
				 	:value="getSpecInfo(form_data,form_data.CPU_CNT, form_data.RAM_SIZE, form_data.OS_DISK_SIZE,'G')" ></fm-output>
				</div>
				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.listLeafCategory" id="${popupid}_CATEGORY"
						keyfield="CATEGORY_ID" titlefield="CATEGORY_NMS"
						:disabled="${isAppr != 'N' || action == null || action =='appr' }"
						name="CATEGORY" title="<spring:message code="TASK" text="부품"/>">
					</fm-select>
				</div>

				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-select url="/api/code_list?grp=PURPOSE" id="${popupid}_PURPOSE" emptystr=" " name="PURPOSE" title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>" onchange="${popupid}_setPurposeNm();" :disabled="  ${ isAppr != 'N' || action == null || action =='appr' }"></fm-select>
				</div>

				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-select url="/api/code_list?grp=P_KUBUN" id="${popupid}_P_KUBUN" emptystr=" "  name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>" onchange="${popupid}_setPKubunNm();" :disabled=" ${isAppr != 'N' || action == null || action =='appr' }"></fm-select>
				</div>
				<div class="col col-sm-6" v-if="${action != 'insert' && action != 'update'}">
					<fm-output id="${popupid}_INS_TMS" name="INS_TMS" title="<spring:message code="NC_VM_INS_TMS" text="등록일시" />" :value="expiryDate(form_data.INS_TMS, form_data.USE_MM)"></fm-output>
				</div>
				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-output id="${popupid}_INS_NM" name="INS_ID_NM" title="<spring:message code="NC_VM_INS_ID_NM" text="담당자" />"></fm-output>
				</div>
				<div class="col col-sm-6">
					<fm-output id="${popupid}_TEAM_NM" name="TEAM_NM" title="<spring:message code="FM_TEAM_TEAM_CD" text="부서" />"></fm-output>
				</div>
				
				<div class="col col-sm-6">
					<fm-output id="${popupid}_TEAM_MNGRS" name="TEAM_MNGRS" title="<spring:message code="FM_TEAM_TEAM_MNGR" text="부서관리자" />"></fm-output>
				</div>
				<div class="col col-sm-6">
					<fm-input id="${popupid}_VM_PORT" name="VM_PORT" title="<spring:message code="NC_THIN_CLIENT_PORT" text="접속포트" />"></fm-input>
				</div>
				<div class="col col-sm-6">
					<fm-output id="MAC_ADDR" name="MAC_ADDR" title="<spring:message code="NC_IP_MAC_ADDR" text="VM MAC" />"></fm-output>
				</div>
				<div class="col col-sm-12" v-if="${action != 'update'}" >
					<c:if test="${isAppr != 'N' || action == null}">
						<div v-if="${action != 'update'}">
							<div v-html="form_data.CMT" class="output hastitle" style="min-height: 200px;overflow-y: auto;word-break: break-word;"></div>
						</div>
					</c:if>
					<c:if test="${!(isAppr != 'N' || action == null)}">
						<div v-if="${action == 'appr' }">
							<div v-html="form_data.CMT" class="output hastitle" style="min-height: 200px;overflow-y: auto;word-break: break-word;"></div>
						</div>
						<div v-else-if="${action != 'update'}">
							<textarea id="${popupid}_CMT" name="CMT" style="width:calc(100% - 150px);"></textarea>
						</div>
					</c:if>
				</div>
				<div class="col col-sm-6" v-if="false">
					<fm-output id="${popupid}_PRIVATE_IP" name="PRIVATE_IP" title="<spring:message code="NC_VM_PRIVATE_IP" text="VM IP" />"></fm-output>
				</div>
				<div class="col col-sm-6" v-if="false">
					<fm-output id="${popupid}_MAC_ADDR" name="MAC_ADDR" title="<spring:message code="NC_IP_MAC_ADDR" text="VM MAC" />"></fm-output>
				</div>
				
				<div class="col col-sm-12" v-if="false">
					<fm-output id="${popupid}_SEC_PRECAUTION_CMT" name="SEC_PRECAUTION_CMT" title="<spring:message code="NC_OS_TYPE_SEC_PRECAUTION_CMT" text="보안 유의사항 확인" />"></fm-output>
				</div>
				
				<div class="col col-sm-12" v-if="form_data.THIN_REQ_YN == 'Y'"
					style="border-left-color: transparent;border-right-color: transparent;">
					<div class="fm-output">
						<label class="control-label grid-title value-title" style="background-color:white;">* 접속 단말 정보</label>
					</div>
				</div>
				<div class="col col-sm-6" v-if="form_data.THIN_REQ_YN == 'Y'">
					<fm-ibutton id="${popupid}_SEARCH_SN" name="SN" title="<spring:message code="NC_THIN_CLIENT_SN" text="SN" />">
						<fm-popup-button v-show="false" popupid="thin_client_search" popup="/clovirsm/popup/khnp/thin_client_search_popup.jsp" cmd="update" param="$('${popupid}_SEARCH_SN').val()" callback="${popupid}_thin_client_search_selected">
							<spring:message code="btn_search" text="씬 클라이언트 가져오기" />
						</fm-popup-button>
					</fm-ibutton>
				</div>
				<div class="col col-sm-6" v-if="form_data.THIN_REQ_YN == 'Y'">
					<fm-output id="${popupid}_MODEL_NM" name="MODEL_NM" title="<spring:message code="NC_THIN_CLIENT_MODEL_NM" text="모델명" />"></fm-output>
				</div>
				<div class="col col-sm-6" v-if="form_data.THIN_REQ_YN == 'Y'">
					<fm-output id="${popupid}_MAC_ADR" name="MAC_ADR" title="<spring:message code="NC_THIN_CLIENT_MAC_ADR" text="MAC" />"></fm-output>
				</div>
				<div class="col col-sm-6" v-if="form_data.THIN_REQ_YN == 'Y'">
					<fm-input id="${popupid}_IP" name="IP" title="<spring:message code="NC_THIN_CLIENT_IP" text="IP" />"></fm-input>
				</div>

				
			</div>
		</form>
	</div>
	<script>
		var param;
		var editor = false;
		var diskUnitOptions = {
			G: 'GB',
			T: 'TB'
		}

		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {
			SPEC_NM: null
			, CATEGORY: null
			, GUEST_NM: null
			, USE_MM:0
			, CMT: null
			, INS_TMS: null
			, INS_ID_NM: null
			, P_KUBUN: null
			, P_KUBUN_NM: null
			, PRIVATE_IP: null
			, MAC_ADDR: null
			, PURPOSE: null
			, VM_NM: null
			, VM_ID: null
			, DC_ID: null
			, DC_NM: null
			, OLD_SPEC_INFO: null
			, SN: null
			, ORG_SN: null
			, MODEL_NM: null
			, MAC_ADR: null
			, IP: null
			, STATUS: null
			, STATUS_NM: null
			, TEAM_NM: null
			, TEAM_MNGRS: null
			, PORT: null
			, VM_PORT: null
		}
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});

		function onAfterVMOwnerChg()
		{
			if(${popupid}_callback != ''){
				eval(${popupid}_callback + '(${popupid}_vue.form_data);');
			}
			$('#${popupid}').modal('hide');
		}
		 
		var ${popupid}_callback = '${callback}';
		function ${popupid}_click(vue, param){
			 
			this.param = param;
			var ac = "${action}"
			 
			 
			console.log(param);
			 
			if(param.DC_ID) {
				${popupid}_param = $.extend(${popupid}_param, param);
				<%--if(param.CATEGORY){--%>
				<%--	$("#${popupid}_CATEGORY").prop("disabled", true)--%>
				<%--}--%>
				<%-- 		--%>
				<%--if(param.P_KUBUN){--%>
				<%--	$("#${popupid}_P_KUBUN").prop("disabled", true)--%>
				<%--}--%>
				<%--if(param.PURPOSE){--%>
				<%--	$("#${popupid}_PURPOSE").prop("disabled", true)--%>
				<%--}--%>
				 
				<c:if test="${action != 'update' && !(isAppr != 'N' || action == null) && action != 'appr' }">
					$("#${popupid}_CMT").Editor("setText", ${popupid}_param.CMT ? ${popupid}_param.CMT:"");
				</c:if>
				return true;
			} else {
				
				${popupid}_param = null;
				alert(msg_select_first);
				return false;
			}
		}
	 
		function ${popupid}_onAfterOpen(){
			var headerTitle = '<spring:message code="label_vm_info" text=""/>';
			var status = '';
			if(${popupid}_vue.form_data.CUD_CD) status += ${popupid}_vue.form_data.CUD_CD_NM;
			if(${popupid}_vue.form_data.APPR_STATUS_CD) status += ' ' + ${popupid}_vue.form_data.APPR_STATUS_CD_NM;
			if(status != '') status = '(' + status + ')';
			headerTitle += status;
			$('#${popupid}_title').text(headerTitle);
		}

		function ${popupid}_save(){
			${popupid}_beforeSaveReq();
			${popupid}_vue.form_data.CMT = $("#${popupid}_CMT").Editor("getText" );
			var action = 'insertReq';
			if(${action == 'insert'}) action = 'insertReq';
			else if(${action == 'save'}) action = 'save';
			var param = ${popupid}_vue.form_data;
			post('/api/vm/' + action, param,  function(data){
			 
				alert(msg_complete);
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '(${popupid}_vue.form_data);');
				}
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_setPurposeNm(){
			var purposeNm = $('#${popupid}_PURPOSE option:selected').text();
			${popupid}_vue.form_data.PURPOSE_NM = purposeNm;
		}

		function ${popupid}_setPKubunNm(){
			var pkubunNm = $('#${popupid}_P_KUBUN option:selected').text();
			${popupid}_vue.form_data.P_KUBUN_NM = pkubunNm;
		}

		function ${popupid}_thin_client_search_selected(data){
			console.log('data', data)
			${popupid}_vue.form_data.SN = data.SN;
			${popupid}_vue.form_data.MODEL_NM = data.MODEL_NM;
			${popupid}_vue.form_data.MAC_ADR = data.MAC_ADR;
			${popupid}_vue.form_data.STATUS = 'I';
			${popupid}_vue.form_data.IP = null;
			${popupid}_vue.form_data.PORT = null;
		}
		 
		function ${popupid}_beforeSaveReq(){
			 
		}
		 
		$(function(){
			<c:if test="${action != 'update' && !(isAppr != 'N' || action == null)}">
				$("#${popupid}_CMT").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false});
			</c:if>
		})
		
	</script>
</fm-modal>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
	String url = request.getParameter("url");
	request.setAttribute("url", url);
%>
<style>
#${popupid} .modal-dialog{
    width: 1200px;
}
#${popupid} .application label{
 	font-size: 11px;
}
#${popupid} .application select{
 	font-size: 509px;
 }
#${popupid} .application .select2-container{
	width: 510px !important;
}
#${popupid} .multi_disk-area .output {
	width: 513px !important;
}
</style>
<fm-modal id="${popupid}" title='<spring:message code="" text="프리셋 카탈로그"/>' cmd="header-title">
	<span slot="footer" id="${popupid}_footer">
		<button type="button" id="save" class="contentsBtn tabBtnImg save top5" onclick="drv_save()"><spring:message code="btn_save" text="저장"/></button>
		<button type="button" id="del" class="contentsBtn tabBtnImg del top5" onclick="drv_del()" v-if="${popupid}_param.action == 'update'"><spring:message code="btn_del" text="삭제"/></button>
	</span>
	<div class="form-panel detail-panel">
		<div class="panel-body">
			<div class="col col-sm-12">
				<fm-input id="DRV_NM" name="DRV_NM" title="<spring:message code="" text="명칭"/>" required="required"></fm-input>
			</div>
			<div class="col col-sm-6">
				<fm-select2 url="" id="SS_CONN_ID" keyfield="CONN_ID" titlefield="CONN_NM" required="required" emptystr="&nbsp" class="select2"	name="CONN_ID" title="<spring:message code="" text="VRA 연결"/>" select_style="width:430px;" onchange="catalogSelect()"></fm-select2>
			</div>
			<div class="col col-sm-6">
				<fm-select2 id="CATALOG_ID" keyfield="CATALOG_ID" titlefield="CATALOG_NM" required="required" name="CATALOG_ID" disabled="disabled"  url="/api/vra_catalog_mng/list/list_NC_VRA_CATALOG/" select_style="width:430px;" title="<spring:message code="" text="카탈로그"/>"></fm-select2>
			</div>
			<div class="col col-sm-6">
				<div class="input-popup inline hastitle" style="width: 100%;">
					<label for="ICON_upload" class="control-label grid-title value-title"><spring:message code="CM_TMPL_ICON" text="아이콘" /></label>
					<div class="input-group input">
						<input type="file" id="${popupid}_ICON_upload" accept="image/*" name="ICON_upload" class="form-control input hastitle" onchange="uploadFile()" style="width: 285px;">
						<input type="hidden" id="${popupid}_ICON" name="ICON" :value="${popupid}_param.ICON">
						<img id="${popupid}_ICON_p" style="height: 30px;width:30px;">
						<button style=" min-width: 79px !important; right: 0px; top: 2px !important;" onclick="iconCreate();" class="contentsBtn tabBtnImg save top5" v-if="${popupid}_param.action == 'update'">아이콘생성</button>
					</div>
				</div>
			</div>
			<div class="col col-sm-6">
				<fm-input id="ICON_COLOR" name="ICON_COLOR" title="<spring:message code="CM_TMPL_ICON_COLOR" text="아이콘 배경 색상" />" onfocusout="updateColor()"></fm-input>
			</div>
			<div class="col col-sm-12" id="TEAM_sel" >
			
				<fm-select2 id="CATEGORY_IDS" keyfield="CATEGORY_ID" titlefield="CATEGORY_NMS"  name="CATEGORY_IDS"  url="/api/code_list?grp=dblist.com.clovirsm.common.Component.listLeafCategory" multiple="true"    title="<spring:message code="TASK" text="업무"/>" select_style="width:1010px;"></fm-select2>
			</div>
			
			<div class="col col-sm-12">
				<fm-textarea id="DRV_CMT" name="DRV_CMT" title="<spring:message code="" text="설명"/>" label_style="height: 100px;" sty="height: 80px;"></fm-output>
			</div>
			<div class="col col-sm-12">
				<jsp:include page="./inputFixedJson.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<fm-popup-button popupid="iconCreate_popup" popup="/clovirsm/popup/iconCreate_popup.jsp"  style="display:none;" param="param" callback=""></fm-popup-button>
	<script>
		var count = 0;
		var param;
		var popupid='${popupid}'
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {action:'',DRV_CMT:'',DRV_NM:'',CATEGORY_IDS:'',CONN_ID:'',CATALOG_ID:''};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		$(document).ready(function(){
			
		})
		function connData(){
			post("/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_ETC_CONN_NM",{"CONN_TYPE":"VRA"},function(data){
				fillOptionByData('SS_CONN_ID', data, '', "CONN_ID", "CONN_NM");
			});
		}
		function catalogSelect(){
			$("#CATALOG_ID").val(null);
			
			if($("#SS_CONN_ID").val() != null && $("#SS_CONN_ID").val() != ''){
				${popupid}_param.CONN_ID = $("#SS_CONN_ID").val()
				post("/api/vra_catalog_mng/list/list_NC_VRA_CATALOG/",{"CONN_ID" : $("#SS_CONN_ID").val()}, function(data){
					fillOptionByData('CATALOG_ID', data, '', "CATALOG_ID", "CATALOG_NM");
					$("#CATALOG_ID").attr("disabled" , false);
				});
			} else{
				$("#CATALOG_ID").attr("disabled" , true);
			}
		}
		function datainit(data){
				$("#${popupid}_ICON_p").attr("src", data.ICON);
				$("#${popupid}_ICON_upload").val("");
				updateColor(data.ICON_COLOR);
				if(data.CATEGORY_IDS && data.CATEGORY_IDS != ''){
					if(data.CATEGORY_IDS instanceof String){ 
						${popupid}_vue.form_data.CATEGORY_IDS = data.CATEGORY_IDS.split(",");
					}
					$("#CATEGORY_IDS").val(${popupid}_vue.form_data.CATEGORY_IDS);
				}
				else{
					$("#CATEGORY_IDS").val("");
				}
				if(data.CONN_ID && data.CONN_ID != ''){
					${popupid}_vue.form_data.CONN_ID = data.CONN_ID;
					$("#SS_CONN_ID").val(data.CONN_ID);
				}
				
				if(data.CATALOG_ID && data.CATALOG_ID != ''){
					${popupid}_vue.form_data.CATALOG_ID = data.CATALOG_ID;
					setTimeout(function(){$("#CATALOG_ID").val(data.CATALOG_ID);},100);
				}
			 	initFixed(data);
			 	
		}
		function ${popupid}_click(vue, param, callback){
			${popupid}_parent_vue = vue;
			if(param != null){
				connData();
				putAll(${popupid}_param, param);
				setTimeout(function(){datainit(${popupid}_param);  }, 1000);
				
				
			} else{
				${popupid}_param = {};
				initFixed();
			}
			
			${popupid}_vue.callback = callback;
			${popupid}_vue.form_data = ${popupid}_param;
			
			return true;
		}
		
		function drv_save(){
			//if(!validate("${popupid}")) return false;
			${popupid}_vue.form_data.CATALOG_ID = $("#CATALOG_ID").val();
			${popupid}_vue.form_data.CATEGORY_IDS = $("#CATEGORY_IDS").val();
			${popupid}_vue.form_data.FIXED_JSON = $("#FIXED_JSON").val();
			 
			post("/api/vra_drv/save", ${popupid}_vue.form_data, function(){
				alert("<spring:message code="saved" text="저장되었습니다"/>");
				search();
				$('#${popupid}').modal('hide');
			})
		}
		function drv_del(){
			${popupid}_vue.form_data.CATALOG_ID = $("#CATALOG_ID").val();
			post("/api/vra_drv/delete", ${popupid}_vue.form_data, function(){
				alert("<spring:message code="deleted" text="삭제되었습니다."/>");
				search();
				$('#${popupid}').modal('hide');
			})
		}
		function uploadFile() {
			var file = $("#${popupid}_ICON_upload")[0].files[0];
			var reader = new FileReader();
			
			reader.addEventListener("load", function () {
				${popupid}_vue.form_data.ICON = reader.result;
				$("#${popupid}_ICON_p").prop("src", ${popupid}_vue.form_data.ICON);
			}, false)
			if (file) {
				reader.readAsDataURL(file);
				
			}
		}
		function updateColor(ICON_COLOR){
			var color = '';
			 
			if(ICON_COLOR == null){
				ICON_COLOR = $("#ICON_COLOR").val();
			} 
			$("#${popupid}_ICON_p").css("background-color", ICON_COLOR);
		}
		
		function iconCreate(){
			
			param = { "CATEGORY_IDS" : $("#CATEGORY_IDS").val(), "ID" : ${popupid}, "DRV_ID" : ${popupid}_param.DRV_ID, "action" : "PRESET"};
			
			$('#iconCreate_popup_button').trigger('click');
		}
	</script>
</fm-modal>
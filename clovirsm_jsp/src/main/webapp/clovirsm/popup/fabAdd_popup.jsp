<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
%>
<style>


</style>

<fm-modal id="${popupid}" title='<spring:message code="" text="FAB 추가"/>' cmd="header-title">
	<span slot="footer">
		
		<button type="button" class="contentsBtn tabBtnImg save top5" onclick="itemSave()"><spring:message code="btn_save" text="저장"/></button>
	</span>
	<div class="form-panel detail-panel" style="height: 40px;">
		<div class="col col-sm-6" style="text-align: left;">
			<label for="DD_VALUE" style="min-width: 56px; background-color: white;" class="control-label grid-title value-title"><spring:message code="" text="코드"/></label>
			<input name="DD_VALUE" id="DD_VALUE" style="width: 290px;" class="form-control input hastitle"/>
		</div>
		<div class="col col-sm-6" style="text-align: left;">
			<label for="KO_DD_DESC" style="min-width: 56px; background-color: white;" class="control-label grid-title value-title"><spring:message code="" text="명칭"/></label>
			<input name="KO_DD_DESC" id="KO_DD_DESC" style="width: 290px;" class="form-control input hastitle"/>
		</div>
	</div>
	<script>
		var appr = null;
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		
		function ${popupid}_click(vue, param, callback){
			 
			${popupid}_parent_vue = vue;
			if(param != null){
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
			}
			${popupid}_vue.form_data = ${popupid}_param;
			${popupid}_vue.form_data.callback=callback;
			$("#item").focus();
			delete ${popupid}_param.newItem;
			return true;
		}
		function getDdValue(){
		 
			 
			return "appr"+ Object.keys(${popupid}_param).length; 
		}
		function itemSave(){
			var param = new Object();
			param.DD_ID = "FAB";
			param.DD_VALUE =  $("#DD_VALUE").val();
			param.DD_NM = "FAB";
			param.KO_DD_DESC = $("#KO_DD_DESC").val();
			param.EN_DD_DESC = $("#KO_DD_DESC").val();
			param.USE_YN = 'Y';
			param.EXT_DD_ID = $("#EXT_DD_ID").val();
			post('/api/ddicUser/save',param,function(data){
				$('#${popupid}').modal('hide');
				if(data){
					eval(${popupid}_vue.form_data.callback + "(param)");
					$('#${popupid}').modal('hide');
				} else{
					alert(msg_jsp_error);
				}
			});
		}
	</script>
	<style>

	</style>
</fm-modal>
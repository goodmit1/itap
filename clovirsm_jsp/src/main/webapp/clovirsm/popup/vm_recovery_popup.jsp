<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %><%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<!--  VM 정보 수정 -->
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("action", action);
	String direct = request.getParameter("direct");
	request.setAttribute("direct", direct);
	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);
%>
<style>
	#${popupid} .modal-dialog {
		width: 1200px;
	}
	/* #${popupid}_NAS_TABLE > div:nth-child(2) > i {
		top: 50%;
    	transform: translateY(-50%);
	} */
</style>
<fm-modal id="${popupid}" title="<spring:message code="server_recovery_req" text="서버 복원 요청"/>" cmd="header-title">

<link href="/res/css/editor.css" type="text/css" rel="stylesheet"/>
<script src="/res/js/editor.js"></script>
	<span slot="footer">
		<input type="button" class="btn saveBtn popupBtn finish top5 " value="<spring:message code="request" text="요청" />" onclick="${popupid}_save()" />
	</span>
	<div class="form-panel detail-panel">
		<form id="${popupid}_form" action="none">
			<div class="panel-body" id="panel-body">
				<div class="col col-sm-6">
					<fm-output id="${popupid}_DC_NM" name="DC_NM" title="<spring:message code="NC_DC_DC_NM" text="데이터 센터"/>"></fm-output>
				</div>
				<!--
				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-output id="${popupid}_TMPL_NM" name="TMPL_NM" title="<spring:message code="NC_VM_TMPL_NM" text="템플릿명"/>"></fm-output>
				</div>
				 -->
				<div class="col col-sm-6">
					<fm-output id="${popupid}_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="VM명"/>"></fm-output>
				</div>
<%-- 				<div class="col col-sm-6" v-if="${action == 'appr' }"> --%>
<%-- 					<fm-output id="${popupid}_GUEST_NM" name="GUEST_NM" title="<spring:message code="NC_VM_GUEST_NM" text="OS명"/>"></fm-output> --%>
<!-- 				</div> -->
				<div class="col col-sm-6" v-else-if="${action != 'insert' && action != 'update'}">
					<fm-output id="${popupid}_GUEST_NM" name="GUEST_NM" title="<spring:message code="NC_VM_GUEST_NM" text="OS명"/>"></fm-output>
				</div>
				
				<!--
				<div class="col col-sm-6" v-if="${action != 'insert' && action != 'update'}">
					<fm-output id="${popupid}_RUN_CD_NM" name="RUN_CD_NM" title="<spring:message code="NC_VM_RUN_CD_NM" text="서버 상태" />" :value="getServerStatus(form_data)"></fm-output>
				</div>
				 -->
<%-- 				 <div class="col col-sm-12" v-if="${action == 'insert' || action == 'update'}"> --%>
<%-- 							<jsp:include page="/clovirsm/popup/hynix/_spec_form.jsp"></jsp:include> --%>
<!-- 						</div> -->
			  	<div class="col col-sm-6" >
				 	<fm-output id="${popupid}_SPEC_INFO" name="SPEC_INFO" title="<spring:message code="NC_VM_SPEC_NM" text="사양"/>" 
				 	:value="getSpecInfo(form_data,form_data.CPU_CNT, form_data.RAM_SIZE, form_data.OS_DISK_SIZE,'G')" ></fm-output>
				</div>
				<div class="col col-sm-6" v-if="${action != 'update'}">
 
					<fm-select id="${popupid}_ADD_USE_MM" emtpyStr="" required="true" name="ADD_USE_MM" title="<spring:message code="USE_MM" text="사용기간" />"  :disabled="${isAppr != 'N' ||  action =='appr' }"></fm-select>
 
				</div>
				<div class="col col-sm-6" v-if="${action != 'insert' && action != 'update'}">
 
					<fm-output id="${popupid}_INS_TMS" name="INS_TMS" title="<spring:message code="NC_VM_INS_TMS" text="등록일시" />" :value="expiryDate(form_data.INS_TMS,1*form_data.USE_MM+1*form_data.ADD_USE_MM)"></fm-output>
 
				</div>
				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-output id="${popupid}_INS_NM" name="INS_ID_NM" title="<spring:message code="NC_VM_INS_ID_NM" text="담당자" />"></fm-output>
				</div>
				<div class="col col-sm-12">
					<fm-textarea id="${popupid}_REASON" name="REASON" title="<spring:message code="input" text="사유" />"></fm-output>
				</div>
			</div>
		</form>
		<div id="stepFinish" class="stepFinish" style="padding-top: 70px; display:none;">
			<div><spring:message code="REQ_MSG_M1" text=""/></div>
			<div><spring:message code="REQ_MSG_M2" text=""/></div>
			<div><spring:message code="REQ_MSG_M5" text=""/></div>
			<div><spring:message code="REQ_MSG_M6" text=""/></div>
			<div class="btn_group">
				<fm-sbutton  class="exeBtn popupBtn finish stepBtn" cmd="search" onclick="endModal()"><spring:message code="FINISH_NEXT_PAGE" text=""/></fm-sbutton>
			</div>
		</div>
	</div>
		<script>
		var param;
		var editor = false;
		var diskUnitOptions = {
			G: 'GB',
			T: 'TB'
		}

		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
 
		var ${popupid}_param = {SPEC_NM:'', CATEGORY:'', GUEST_NM:'',   CMT:'', INS_TMS:'', INS_ID_NM:'', P_KUBUN:'', P_KUBUN_NM:'', PURPOSE:'', VM_NM:'', VM_ID:'', DC_ID:'', DC_NM:'', OLD_SPEC_INFO:'',ADD_USE_MM:0};
 
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		
		function setUseMM(val){
			var specParam = new Object();
			var monthArray = new Array();
			specParam.ID = val;
			post("/api/code_list?grp=dblist.com.clovirsm.common.Component.selectSpecSetting",specParam, function(data){
				 
				for(var q = 0 ; q < data.length; q++){
					valList = data[q].VAL1.split(",");
				
				if(data[q].KUBUN == "MONTH"){
					for(var w = 0 ; w < valList.length; w++){
						var valListS = valList[w].split("|");
						var object = new Object();
						if(valListS.length > 1){
							object.title = valListS[1];
							object.val = valListS[0];
						} else{
							object.title = valList[w];
							object.val = valList[w];
						}
						monthArray.push(object);
					}
					
 
					fillOptionByData("${popupid}_ADD_USE_MM", monthArray, '', "val", "title");
 
				}
			}
			});
		}
		
		  
		function endModal(){
			location.href="/clovirsm/workflow/requestItemsHistory/index.jsp";
			return true;
		}
		var ${popupid}_callback = '${callback}';
		function ${popupid}_click(vue, param){
			
			${popupid}_vue.form_data = $.extend(${popupid}_vue.form_data, param)
			var ac = "${action}"
			 
			 
			setUseMM(${popupid}_vue.form_data.P_KUBUN);
 			return true;
			 
		}
		function date_change(date){
    		var year = date.getFullYear();
		    var month = date.getMonth()+1;
		    month = (month < 10) ? '0' + month : month;
		    var day = date.getDate();
		    day = (day < 10) ? '0' + day : day;
		    return year+"-"+month+"-"+day;

		}
		




		function ${popupid}_save(){
			var data = new Object();
			data.TITLE = ${popupid}_param.VM_NM;
			data.SVC_ID= ${popupid}_param.VM_ID;
			data.SVC_CD = 'S';
			data.ADD_USE_MM= $("#${popupid}_ADD_USE_MM").val();
			data.SVC_TYPE = 'V';
			data.REASON = $("#${popupid}_REASON").val();
			data.DEPLOY_REQ_YN = "Y";
			if(validate("panel-body")){
				post('/api/reuse/updateReq', data, function(data){
					 
					$('#${popupid}').modal('hide');
					FMAlert("<img src='/res/img/hynix/confirm.png' style='margin-bottom:20px;margin-top: 20px;'><div style='font-size: 16px; font-weight: 900; margin-bottom:20px;'><spring:message code="REQ_MSG_M1" text=""/></div><div><spring:message code="REQ_MSG_M2" text=""/></div><div><spring:message code="REQ_MSG_M5" text=""/></div><div><spring:message code="REQ_MSG_M6" text=""/></div>",'요청 완료','신청이력페이지로 이동', function(){location.href="/clovirsm/workflow/requestItemsHistory/index.jsp"; } );					 
					vmSearch();
					if(${popupid}_callback != ''){
						eval(${popupid}_callback + '(${popupid}_vue.form_data);');
					}
					 
					
				})
			}
		
		}

		  
	</script>
</fm-modal>
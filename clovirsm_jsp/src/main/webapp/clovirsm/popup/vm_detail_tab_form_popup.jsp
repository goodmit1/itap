<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %><%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String direct = request.getParameter("direct");
	request.setAttribute("direct", direct);

	String action = request.getParameter("action");
	request.setAttribute("action", action);

	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);
%>
<style>
	#${popupid} .modal-dialog {
		width: 1300px;
	}
	.Editor-editor {
    	height: 200px;
    }
	#vm_detail_tab_popup_VM_NM {
    width: calc(100% - 250px);
    min-width: 50px;
    height: 95%;
}
</style>
<fm-modal id="${popupid}" title="<spring:message code="label_vm_save" text="" />" cmd="header-title">
	<span slot="footer">
		<input v-if="${isAppr == 'N'} && ${action == 'insert' &&  WORK_SHOW == true} && isRequestable(form_data)" type="button" class="btn saveBtn" value="<spring:message code="btn_work" text="작업함에 담기"/>" onclick="${popupid}_to_work()" />
		<input v-if="${isAppr == 'N'} && ${action == 'insert'} && isRequestable(form_data)" type="button" class="btn exeBtn" value="<spring:message code="btn_request" text="바로 요청"/>" onclick="${popupid}_request()" />
		<input v-if="${isAppr == 'N'} && ((${action == 'insert'} && form_data.CUD_CD == 'C') || ${action == 'save'}) && isSavable(form_data)" type="button" class="btn saveBtn" value="<spring:message code="btn_save" text="저장" />" onclick="${popupid}_save()" />

	</span>
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#${popupid}_insertReqTab1" data-toggle="tab"><spring:message code="NC_VM_SPEC_NM" text="사양"/></a>
		</li>
		<c:if test="${sessionScope.FW_SHOW }">
		<li>
			<a href="#${popupid}_insertReqTab2" data-toggle="tab"><spring:message code="NC_VM_ACCESS_CONTROL" text="접근제어" /></a>
		</li>
		</c:if>
	</ul>
	<div class="tab-content ">
		<div class="form-panel detail-panel tab-pane active" id="${popupid}_insertReqTab1">
			<form id="${popupid}_vm_form" action="none">
				<input type="hidden" name="VM_ID" id="${popupid}_VM_ID"  />
				<fmtags:include page="/clovirsm/resources/reqVm/vm_input.jsp" />

			</form>
		</div>
		<c:if test="${sessionScope.FW_SHOW }">
		<div  class="form-panel detail-panel tab-pane" id="${popupid}_insertReqTab2" style="height:380px">
			<jsp:include page="/clovirsm/resources/reqVm/user_list.jsp"></jsp:include>
		</div>
		</c:if>
	</div>
	<script>


		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {
				SPEC_ID: 0,
				SPEC_NM: null,
				CPU_CNT: 0,
				RAM_SIZE: 0,
				DISK_SIZE: 0,
				DISK_UNIT: 'G',
				SPEC_INFO: null
		};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});


		$(document).ready(function(){
			<c:if test="${sessionScope.FW_SHOW }">${popupid}_fw_init();</c:if>
		})
		//서버생성 detail만 탐
		function ${popupid}_click(vue, param){
			if(param.OS_ID || param.FROM_ID){
				${popupid}_param = $.extend({}, param);
				${popupid}_vue.form_data = ${popupid}_param;
				<c:if test="${isAppr == 'N'}">
					$("#${popupid}_CMT").Editor("setText", ${popupid}_param.CMT ? ${popupid}_param.CMT:"");
				</c:if>
				try {
					var disk = getDiskSize(param.NC_VM_DATA_DISK);
					if(disk.DEF_DISK_SIZE == 0) {
						${popupid}_vue.form_data.OS_DISK_SIZE = ${popupid}_param.DISK_SIZE - disk.DISK_SIZE;
					}else {
						${popupid}_vue.form_data.OS_DISK_SIZE = disk.DEF_DISK_SIZE;
					}
					if(disk.DISK_SIZE % 1024 == 0) {
						disk.DISK_SIZE = Math.floor(disk.DISK_SIZE / 1024);
						${popupid}_vue.form_data.DISK_UNIT = 'T';
					}
					${popupid}_vue.form_data.DISK_SIZE = disk.DISK_SIZE;
				}catch(e){
					 
				}
				try {
					${popupid}_click_after();
					${popupid}_vmFwSearch();
				} catch(e){ }
				try {
					${popupid}_clearNas();
				} catch(e) {}
				<c:if test="${isAppr == 'Y'}">
					$("#${popupid} .modal-body button").hide();
					$("#${popupid} .modal-body input").prop("disabled", true)
					$("#${popupid} .modal-body select").prop("disabled", true)
					if(${popupid}_vue.form_data.NAS) {
						var nasArr = ${popupid}_vue.form_data.NAS.split(",");
						nasArr.forEach(function(value,index){
							${popupid}_makeNas(nasArr[index], false);							
						})
						/* for(var i in nasArr){
							${popupid}_makeNas(nasArr[i], true);
						} */
					}
				</c:if>
				<c:if test="${isAppr == 'N'}">
				if(${popupid}_vue.form_data.NAS) {
					var nasArr = ${popupid}_vue.form_data.NAS.split(",");
					nasArr.forEach(function(value,index){
						${popupid}_makeNas(nasArr[index], true);							
					})
					/* for(var i in nasArr){
						${popupid}_makeNas(nasArr[i], true);
					} */
				}
				</c:if>
				return true;
			} else {
				${popupid}_param = {};
				${popupid}_vue.form_data = ${popupid}_param;
				alert(msg_select_first);
				return false;
			}
		}

		function ${popupid}_onAfterOpen(){
			$('#${popupid} li:first > a').tab('show');

			var headerTitle = '<spring:message code="label_vm_info" text=""/>';
			var status = '';
			if(${popupid}_vue.form_data.CUD_CD) {
				if(${popupid}_vue.form_data.FROM_ID && ${popupid}_vue.form_data.FROM_ID.substring(0,1) == 'S'){
					status += '<spring:message code="label_copy" text="복사"/>';
				} else {
					status += ${popupid}_vue.form_data.CUD_CD_NM;
				}
			}
			if(${popupid}_vue.form_data.APPR_STATUS_CD) status += ' ' + ${popupid}_vue.form_data.APPR_STATUS_CD_NM;
			if(status != '') status = '(' + status + ')';
			headerTitle += status;
			$('#${popupid}_title').text(headerTitle);
		}

		<c:if test="${isAppr == 'N'}">
			$("#${popupid}_CMT").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false});
		</c:if>

	</script>
</fm-modal>
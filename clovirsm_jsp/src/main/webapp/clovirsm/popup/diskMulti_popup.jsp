<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
%>
<style>
	#${popupid} .modal-dialog{
		width: 990px;
	}
	.arrow-right:before{
		content: "\f178";
	    font-family: FontAwesome;
	    font-size: 30px;
	    color: #414141;
	    cursor: pointer;
	}
	.arrow-left:before{
	    content: "\f177";
	    font-family: FontAwesome;
	    font-size: 30px;
	    color: #414141;
	    cursor: pointer;
	}
</style>
<fm-modal id="${popupid}" title="<spring:message code="add_disk" text="추가"/>" cmd="header-title">
	<span slot="footer">
		<input type="button" class="btn contentsBtn tabBtnImg new" value="<spring:message code="label_add" text="추가"/>" onclick="${popupid}_disk_add();" />
	</span>
	<div class="form-panel detail-panel">
		<form id="${popupid}_popup_form" action="none">
			<div class="panel-body" style="overflow:auto">
				<div class="col col-sm-5" style="border: none; padding: 10px;">
					<div class="btn_group">
						<input id="diskFilter" style="vertical-align: middle;" onkeyup="diskListTable.gridOptions.api.setQuickFilter(this.value)" placeholder="Filter.." class="">
					</div>
					<div id="diskListTable" class="ag-theme-fresh" style="height: 300px" ></div>
				</div>
				<div class="col col-sm-1" style="border: none;padding: 10px;hight:300px;">
					<div class="arrow-right" style="margin-top: 130px; padding-left: 20px;" onclick="addItem()"></div>
					<div class="arrow-left" style=" padding-left: 20px;" onclick="removeItem()"></div>
				</div>
				<div class="col col-sm-6" style="border: none;padding: 10px;">
					<div class='btn_group'>
						<fm-select id="DISK_TYPE"  name="DISK_TYPE" onchange="diskChange()"></fm-select>
					</div>
					<div id="diskItemTable" class="ag-theme-fresh" style="height: 300px" ></div>
				</div>
			</div>
		</form>
	</div>
	<script>
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_objList;
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		var newObject = new Object();
		var ${popupid}_callback;
		var temp = "";
		var diskListTable;
		var diskItemTable;
		var datastore;
		var tempObject = new Object();
		function ${popupid}_click(vue, param, callback){
			if(param){
				${popupid}_param = $.extend({}, param);
				${popupid}_vue.form_data = ${popupid}_param;
				initTable();
				selectDiskType();
				${popupid}_callback = callback;
				return true;
			} else {
				${popupid}_param = {};
				alert(msg_select_first);
				return false;
			}
		}
		function selectDiskType(){
			post("/api/code_list?grp=db.com.clovirsm.common.Component.selectDiskType",null, function(data){
				  fillOptionByData("DISK_TYPE", data,null,null,null);
				  for(d in data){
					  newObject[d] = new Array();
				  }
				  
				  selectDisk();
			});
			
		}
		function selectDisk(){
			
			
			$.get('/api/dc_mng/list/list_NC_HV_OBJ/?DC_ID=' + ${popupid}_param.DC_ID, function(list){
				${popupid}_objList={ };
				for(var i=0; i < list.length;i++)
				{
					var obj = ${popupid}_objList[list[i].OBJ_TYPE_NM];
					if(!obj)
					{
						obj = [];
						${popupid}_objList[list[i].OBJ_TYPE_NM] = obj;
					}
					try
					{
						var o = new Object();
						o[list[i].OBJ_TYPE_NM] = list[i].OBJ_NM;
						obj.push(o);
					}
					catch(e)
					{}
				}

				datastore = JSON.parse(JSON.stringify(${popupid}_objList.Datastore));
				for(var i = 0 ; i < ${popupid}_objList.Datastore.length ; i++){
					var param = [];
					for(var z = 0 ; z < ${popupid}_param.oldDisk.length ; z++){
						console.log(${popupid}_objList.Datastore[i].Datastore, ${popupid}_param.oldDisk[z].DS_NM)
						if(${popupid}_objList.Datastore[i].Datastore === ${popupid}_param.oldDisk[z].DS_NM){
							param = newObject[${popupid}_param.oldDisk[z].DISK_TYPE_ID];
							if(!param){
								param = [];					
								param.push(${popupid}_param.oldDisk[z]);
							} else{
								param.push(${popupid}_param.oldDisk[z]);
							}
							
							newObject[${popupid}_param.oldDisk[z].DISK_TYPE_ID] = param;
						
							var idx = datastore.findIndex(function (x){
							    return x.Datastore == ${popupid}_param.oldDisk[z].DS_NM;
							})
							
							if(idx != -1)
								datastore.splice(idx,1);
						}
					}
				}				
				diskListTable.setData(datastore);
				diskItemTable.setData(newObject[$("#DISK_TYPE").val()]);
			});
			
			
			
		}
		
		function initTable(){
			var diskListTableColumnDefs = [
					{headerName : "<spring:message code="NC_DC_DISK_TYPE_DS_NM" text="" />",field : "Datastore"}
				];
			var
			diskListTableGridOptions = {
				editable:true,
				columnDefs : diskListTableColumnDefs,
// 				rowModelType: 'infinite',
				rowSelection : 'multiple',
				sizeColumnsToFit: true,
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				enableColResize : true,
				enableServerSideSorting: false,
			}
			diskListTable = newGrid("diskListTable", diskListTableGridOptions);
			
			
			var diskItemTableColumnDefs = [
					{headerName : "<spring:message code="NC_DC_DISK_TYPE_DS_NM" text="" />",field : "DS_NM"}
				];
			var
			diskItemTableGridOptions = {
				editable:true,
				columnDefs : diskItemTableColumnDefs,
				//rowModelType: 'infinite',
				//rowSelection : 'single',
				sizeColumnsToFit: true,
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				enableColResize : true,
				enableServerSideSorting: false,
			}
			diskItemTable = newGrid("diskItemTable", diskItemTableGridOptions);
		}
		function diskChange(){
			diskItemTable.setData(newObject[$("#DISK_TYPE").val()]);
		}

		function enterCheck(event){
			if (event.keyCode === 13) {
				event.preventDefault();
				${popupid}_library_add();
			};
		}
		
		function ${popupid}_disk_add(){
			var tempData = new Array();
			
			
			for(var i in newObject){
				for(z = 0 ; z < newObject[i].length ; z++){
					tempData.push(newObject[i][z]);
					var searchYN = diskTable.getData().findIndex(function (x){
					    return x.DS_NM == newObject[i][z].DS_NM;
					});
					if(searchYN != -1){
						newObject[i][z].IU = 'U'
					} else{
						newObject[i][z].IU = 'I'
					}
				}
				
			}
			
			diskTable.setData(tempData);
			$('#${popupid}').modal('hide');
			
			if(${popupid}_callback != ''){
				setTimeout(function(){
					eval(${popupid}_callback + '();');
				},100);
			}
		}
		
		function addItem(){
			var arr = diskListTable.getSelectedRows();
			var diskType = $("#DISK_TYPE").val();
			
			
			for(var z in newObject ){
				if(z == diskType){
					for(var i = 0 ; i < arr.length ; i++){
						var obj = new Object();
						obj.DS_NM = arr[i].Datastore;
						obj.DC_ID = ${popupid}_param.DC_ID;
						obj.DISK_TYPE_ID = diskType;
						newObject[z].push(obj);
					}
					diskListTable.deleteRow();
					diskItemTable.setData(newObject[z]);
					break;
				}
			}
			
		}
		function removeItem(){
			var arr = diskItemTable.getSelectedRows();
			var diskType = $("#DISK_TYPE").val();
			
			
			for(var z in newObject ){
				if(z == diskType){
					for(var i = 0 ; i < arr.length ; i++){
						var obj = new Object();
						obj.Datastore = arr[i].DS_NM;
						diskListTable.insertRow(obj);
						
						var idx = newObject[z].findIndex(function (x){
						    return x.DS_NM === arr[i].DS_NM
						})
						
						if(idx != -1){
							newObject[z].splice(idx,1);
						}
					}
					diskItemTable.deleteRow();
					break;
				}
			}
		}
	</script>
</fm-modal>
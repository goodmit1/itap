<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
String popupid = request.getParameter("popupid");
pageContext.setAttribute("popupid", popupid);
%>
<fm-modal id="${popupid}" title="<spring:message code="classification" text="업무분류"/>" cmd="header-title">
		<span slot="footer">
			<template v-if="isEdit">
				<fm-popup-button style="display:none;" popupid="category_edit_popup" popup="/clovirsm/popup/img_category_input_popup.jsp" cmd="update" param="${popupid}_vue.param"><spring:message code="NC_OS_TYPE_CATEGORY_EDIT" text="카테고리 수정"/></fm-popup-button>
				<input type="button" class="btn newBtn" value="<spring:message code="btn_new" text="" />" onclick="addCategory();return false;" >
				<input type="button" class="btn saveBtn" value="<spring:message code="btn_edit" text=""/>" onclick="renameCategory();return false;" >
				<input type="button" class="btn delBtn" value="<spring:message code="btn_delete" text=""/>" onclick="removeCategory();return false;" >
				<input type="button" class="btn exeBtn" value="<spring:message code="btn_reload" text="새로고침"/>" onclick='${popupid}_reloadTree();return false;' >
			</template>
			<template v-else>
				<input type="button" class="btn" value="<spring:message code="no_select" text="선택없음"/>" onclick="${popupid}_popup_select([{}]);return false;" >
				<input type="button" class="btn" value="<spring:message code="btn_reload" text="새로고침"/>" onclick='${popupid}_reloadTree();return false;' >
			</template>
		</span>
	<form id="${popupid}_popup_form" action="none">
		<div class="panel-body" style="height:600px;overflow:auto">
			 <fm-tree id="${popupid}_tree" url="/api/popup/category/list"
				:field_config="{onselect:${popupid}_popup_select, root:0,id:'CATEGORY',parent:'PAR_CATEGORY_ID',text:'CATEGORY_NM_CODE'}" >
			</fm-tree>
		</div>
	</form>

	<script>

	var isEdit = false;
	var cur;

	var ${popupid}_vue = makePopupVue("${popupid}", "${popupid}", {})

	function ${popupid}_switchEdit(force){
		isEdit = force != null ? force : true;
		${popupid}_vue.$forceUpdate();
		${popupid}_reloadTree();
	}
	function treeSearch(id, that){
			var val = $(that).val();
			$("#" + id).jstree("search", val, false, true);
		
	} 
	function ${popupid}_popup_select(selectedArr) {
		if(selectedArr[0] == null){
			return
		}
		if(isEdit){
			cur = selectedArr[0];
		}else{

			<c:if test="${param.sel_leaf_yn eq 'Y'}">
			var i = $.jstree.reference("#${popupid}_tree").get_json(selectedArr[0].CATEGORY, {flat: true});
			if(i.length > 1) {
				$('#${popupid}_tree').jstree("deselect_all");
				return;
			}
			</c:if>
			var callback = ${popupid}_vue.callback;
			 
			if(callback != null){
				 
				 
				eval( callback + '(selectedArr[0]);');
			}
			$("#${popupid}").modal('hide');
		}
	}

	function addCategory_callback(data){
		if(!data.NAME || data.NAME.trim() == ""){
			return;
		}
		var obj = {};
		obj.PAR_CATEGORY_ID = cur.CATEGORY;
		obj.CATEGORY_NM = data.NAME;
		obj.CATEGORY_CODE = data.CODE;
		post("/api/popup/category/save", obj, function(data){
			${popupid}_reloadTree();
		});
	}

	function renameCategory_callback(data){
		if(!data.NAME || data.NAME.trim() == ""){
			return;
		}
		cur.CATEGORY_NM = data.NAME;
		cur.CATEGORY_CODE = data.CODE;
		cur.CATEGORY_ID = cur.CATEGORY;
		post("/api/popup/category/save", cur, function(data){
			${popupid}_reloadTree();
		});
	}

	function addCategory(){
		if(cur == null){
			if(!confirm("<spring:message code="msg_category_add_root" text=""/>")){
				return;
			}
			cur = {CATEGORY: 0};
		}

		${popupid}_vue.param = {};
		${popupid}_vue.param.callback = "addCategory_callback";
		$("#category_edit_popup_button").click();
	}

	function renameCategory(){

		if(cur == null){
			alert(msg_select_first);
			return;
		}
		${popupid}_vue.param = $.extend({}, cur);
		${popupid}_vue.param.callback = "renameCategory_callback";
		$("#category_edit_popup_button").click();
	}

	function removeCategory(){
		if(cur == null){
			alert(msg_please_select_for_delete);
			return;
		}
		if(confirm("<spring:message code="msg_delete_confirm_for_category" text=""/>")){
			var i = $.jstree.reference("#${popupid}_tree").get_json(cur.CATEGORY, {flat: true});
			if(i.length > 1){
				if(confirm("<spring:message code="msg_delete_leaf_yn" text=""/>")){
				}else{
					alert("<spring:message code="msg_cant_delete_category_1" text=""/>");
					return;
				}
			}
			var list = [];
			for(var idx in i){
				list.push(i[idx].id);
			}
			cur.DELETE_LIST = list.join(",");
			post("/api/popup/category/delete", cur, function(data){
				${popupid}_reloadTree();
				ostypeSearch();
			});
		}
	}

	function ${popupid}_reloadTree(){
		$("#${popupid}_tree").trigger("reload");
		setTimeout(function(){
			cur = null;
			$('#${popupid}_tree').jstree("deselect_all");
			$("#${popupid}_tree").jstree("search", "");
			$("#${popupid}_form #searchInput").val("");
		},1000);
	}

	function ${popupid}_click(thisvue, param){
		${popupid}_vue.callback = thisvue.callback;
		${popupid}_switchEdit(param == true);
		return true;
	}
	function enterCheck(){
		if (event.keyCode == 13) {
			event.preventDefault();
		}
	}
	</script>


</fm-modal>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.clovirsm.service.ComponentService"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	ComponentService service =(ComponentService)SpringBeanUtil.getBean("componentService");
	List<Map> list = service.selectListByQueryKey("selectSpecSetting",new HashMap());
	JSONObject ramcpu = new JSONObject();
	for(Map m : list)
	{
		String val1 = (String)m.get("VAL1");
		if(val1 != null && val1.length()>0)
		{
			String[] arr = val1.split(",");
			JSONArray arr1 = new JSONArray();
			for(String a:arr)
			{
				arr1.put(a);
			}

			ramcpu.put( m.get("ID") + "." + m.get("KUBUN"), val1.split(","));
		}
	}
	request.setAttribute("RAMCPU_JSON", ramcpu.toString());
%>
<script>
	var RAMCPU_JSON =${RAMCPU_JSON};
	function ${popupid}_chgCPURAM() {
		var pKubun =  ${popupid}_vue.form_data.P_KUBUN;
		initSpecData();
		if($("#${popupid}_P_KUBUN").length != 0)
		{
			pKubun=$("#${popupid}_P_KUBUN").val();
		}

		if(pKubun != null && pKubun != '')
		{
			var cpuArr = RAMCPU_JSON[pKubun + ".CPU"];
			${popupid}_CPURAM_showhide(cpuArr,'CPU_CNT');
			var ramArr = RAMCPU_JSON[pKubun + ".RAM"];
			${popupid}_CPURAM_showhide(ramArr,'RAM_SIZE');
			var diskArr = RAMCPU_JSON[pKubun + ".DISK"];
			${popupid}_CPURAM_showhide(diskArr,'DATA_DISK_SIZE');
			var gpuArr = RAMCPU_JSON[pKubun + ".GPU"];
			${popupid}_CPURAM_showhide(gpuArr,'GPU_SIZE');
		}

		if(${popupid}_vue.form_data.SPEC_ID != null){

		}
	}
	function ${popupid}_CPURAM_showhide(arr, kubun) {
		if(arr && arr.length>0)
		{
			$("#${popupid}_" + kubun + "_sel").parent().show();
			$("#${popupid}_" + kubun + "_input").parent().parent("div").hide();
			fillOptionByData('${popupid}_' + kubun + '_sel', arr);
		}
		else
		{
			$("#${popupid}_" + kubun + "_sel").parent().hide();
			$("#${popupid}_" + kubun + "_input").parent().parent().show();
		}
	}
	function changeSpec(that){
		if($("#SPEC_ID option:selected")[0]){
			var data = $("#SPEC_ID option:selected")[0].data;

			${popupid}_param.DISK_SIZE = data.DISK_SIZE;
			${popupid}_param.DISK_TYPE = data.DISK_TYPE_ID;
			${popupid}_param.RAM_SIZE = data.RAM_SIZE;
			${popupid}_param.CPU_CNT = data.CPU_CNT;
			${popupid}_param.SPEC_ID = data.SPEC_ID;


		}
	}
	function initSpecData(){
		post("/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC_SPEC",{"DC_ID": ${popupid}_param.DC_ID},function(data){
			fillOptionByData('SPEC_ID', data, '', "SPEC_ID", "SPEC_NM");
			$("#SPEC_ID").val(${popupid}_param.OLD_SPEC_ID);
		});
	}
</script>
<div v-show="form_data.PUBLIC_YN == 'N'">
	<div class="col col-sm-6" >
		<div class="input-spinner inline compact hastitle"  >
			<label for="${popupid}_CPU_CNT" class="control-label grid-title value-title"><spring:message code="NC_VM_CPU_CNT" text="CPU" />(core)</label>
			<fm-spin id="${popupid}_CPU_CNT_input"  class="inline"    name="CPU_CNT" group_style="width: 65%"  min="1"  >
			</fm-spin>
			<fm-select  id="${popupid}_CPU_CNT_sel"  class="inline"  name="CPU_CNT" style="width:100px;display:none"></fm-select>
		</div>


	</div>
	<div class="col col-sm-6">
		<div class="input-spinner inline compact hastitle"  >
			<label for="${popupid}_RAM_SIZE" class="control-label grid-title value-title"><spring:message code="NC_VM_RAM_SIZE" text="Memory" />(GB)
			</label>
			<fm-spin id="${popupid}_RAM_SIZE_input"  class="inline"   name="RAM_SIZE" group_style="width: 65%" min="1"  >
			</fm-spin>
			<fm-select  id="${popupid}_RAM_SIZE_sel"  class="inline" name="RAM_SIZE" style="display:none"></fm-select>
		</div>


	</div>
	<div class="col col-sm-6" v-show="form_data.GPU_SIZE > 0">
		<div class="input-spinner inline compact hastitle"  >
			<label for="${popupid}_GPU_SIZE" class="control-label grid-title value-title"><spring:message code="NC_VM_GPU_SIZE" text="GPU" />(Q)
			</label>
			<fm-spin id="${popupid}_GPU_SIZE_input"  class="inline"   name="GPU_SIZE" group_style="width: 65%" min="1"  >
			</fm-spin>
			<fm-select  id="${popupid}_GPU_SIZE_sel"  class="inline" name="GPU_SIZE" style="display:none"></fm-select>
		</div>
	</div>
</div>
<div v-show="form_data.PUBLIC_YN == 'Y'">
	<div class="col col-sm-12" >
		<fm-select2 id="SPEC_ID" name="SPEC_ID"
					keyfield="SPEC_ID" titlefield="SPEC_NM" onchange="changeSpec(this)"
					title="<spring:message code="DATA_DISK_TYPE" text="사양"  />" select_style=" width: 420px;"
					required="true"></fm-select2>
	</div>
</div>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %><%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!--  VM 정보 수정 -->
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("action", action);
	String direct = request.getParameter("direct");
	request.setAttribute("direct", direct);
	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);
%>
<style>
	#${popupid} .modal-dialog {
		width: 1200px;
	}
	/* #${popupid}_NAS_TABLE > div:nth-child(2) > i {
		top: 50%;
    	transform: translateY(-50%);
	} */
	#vm_detail_tab_popup_vm_form > div > div:nth-child(12) > div > div{
		overflow-y: auto;
		word-break: break-all;
		-ms-overflow-y: auto;
		-ms-word-break: break-all;
	}
</style>
<fm-modal id="${popupid}" title="<spring:message code="label_vm_info" text=""/>" cmd="header-title">
<link href="/res/css/editor.css" type="text/css" rel="stylesheet"/>
<script src="/res/js/editor.js"></script>
	<span slot="footer">
		<input v-if="isShowReqBtn('${isAppr}','${direct}' ,'${action}', form_data,'work') && ${ WORK_SHOW == true}" type="button" class="btn saveBtn" value="<spring:message code="btn_work" text="작업함에 담기"/>" onclick="${popupid}_to_work()" />
		<input v-if="isShowReqBtn('${isAppr}','${direct}' ,'${action}', form_data,'work')" type="button" class="btn exeBtn" value="<spring:message code="btn_request" text="바로 요청"/>" onclick="${popupid}_request()" />
		<input v-if="isShowReqBtn('${isAppr}','${direct}' ,'${action}', form_data,'save')" type="button" class="btn saveBtn" value="<spring:message code="btn_save" text="저장" />" onclick="${popupid}_save()" />
		<span v-if="isShowReqBtn('${isAppr}','${direct}' ,'${action}', form_data,'owner')"><fm-popup-button   popupid="vm_owner_change_popup" popup="/popup/owner_change_form_popup.jsp?svc=vm&key=VM_ID&callback=onAfterVMOwnerChg" cmd="update" param="vmReq.getData()"><spring:message code="btn_change_owner" text="담당자변경"/></fm-popup-button></span>
		<fm-popup-button style="display:none;" popupid="${ popupid }_search_form_popup" popup="/clovirsm/popup/nas_search_form_popup.jsp" cmd="" param="" callback="${ popupid }_select_nas"></fm-popup-button>
	</span>
	<div class="form-panel detail-panel">
		<form id="${popupid}_form" action="none">
			<div class="panel-body">
				<div class="col col-sm-6">
					<fm-output id="${popupid}_DC_NM" name="DC_NM" title="<spring:message code="NC_DC_DC_NM" text="데이터 센터"/>"></fm-output>
				</div>
				<!--
				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-output id="${popupid}_TMPL_NM" name="TMPL_NM" title="<spring:message code="NC_VM_TMPL_NM" text="템플릿명"/>"></fm-output>
				</div>
				 -->
				<div class="col col-sm-6">
					<fm-output id="${popupid}_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="VM명"/>"></fm-output>
				</div>
				<div class="col col-sm-6" v-if="${action == 'appr' }">
					<fm-output id="${popupid}_GUEST_NM" name="GUEST_NM" title="<spring:message code="NC_VM_GUEST_NM" text="OS명"/>"></fm-output>
				</div>
				<div class="col col-sm-6" v-else-if="${action != 'insert' && action != 'update'}">
					<fm-output id="${popupid}_GUEST_NM" name="GUEST_NM" title="<spring:message code="NC_VM_GUEST_NM" text="OS명"/>"></fm-output>
				</div>
				
							
				<c:choose>
					<c:when test="${isAppr eq 'Y'}"> <!--  결재시 -->
						<div class="col col-sm-12" >
							<fm-output id="${popupid}_SPEC_INFO" name="OLD_SPEC_INFO" title="<spring:message code="NC_VM_SPEC_NM_BEFORE" text="기존 사양"/>" :value="form_data.OLD_SPEC_INFO ? form_data.OLD_SPEC_INFO: form_data.SPEC_INFO"></fm-output>
						</div>
						<div class="col col-sm-12 SPEC_INFO_div" >
							<fm-output id="${popupid}_SPEC_INFO" name="SPEC_INFO"   title="<spring:message code="NC_VM_SPEC_NM_AFTER" text="변경 사양"/>"
							:value="getSpecInfo(form_data.SPEC_NM,form_data.CPU_CNT, form_data.RAM_SIZE) + ' / <spring:message code="add" text="추가" /> <spring:message code="NC_IMG_DISK_SIZE" text="디스크사이즈" />:' + form_data.DISK_SIZE + form_data.DISK_UNIT + 'B'"    ></fm-output>
						</div>	
					</c:when>
					<c:otherwise>	
						<div class="col col-sm-6" >
							<fm-output id="${popupid}_SPEC_INFO" name="SPEC_INFO" title="<spring:message code="NC_VM_SPEC_NM_PRESENT" text="현재 사양"/>"
									   :value="getSpecInfo(null,form_data.CPU_CNT, form_data.RAM_SIZE, form_data.OS_DISK_SIZE,'G')" ></fm-output>
						</div>
						<div class="col col-sm-12" v-if="${action == 'insert' || action == 'update'}">
							<jsp:include page="/clovirsm/popup/mobis/_spec_form.jsp"></jsp:include>
						</div>
					</c:otherwise>
				</c:choose>	
				<!--
				<div class="col col-sm-6" v-if="${action != 'insert' && action != 'update'}">
					<fm-output id="${popupid}_RUN_CD_NM" name="RUN_CD_NM" title="<spring:message code="NC_VM_RUN_CD_NM" text="서버 상태" />" :value="getServerStatus(form_data)"></fm-output>
				</div>
				 -->
				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-select url="/api/site/querykey/selectPartCategory/" id="${popupid}_CATEGORY"
						keyfield="CATEGORY_ID" titlefield="CATEGORY_NM"
						:disabled="  ${ isAppr != 'N' || action == null || action =='appr' }"
						name="CATEGORY" title="<spring:message code="PART" text="부품"/>">
					</fm-select>
				</div>

				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-select url="/api/code_list?grp=PURPOSE" id="${popupid}_PURPOSE" emptystr=" " name="PURPOSE" title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>" onchange="${popupid}_setPurposeNm();" :disabled="  ${ isAppr != 'N' || action == null || action =='appr' }"></fm-select>
				</div>

				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-select url="/api/code_list?grp=P_KUBUN" id="${popupid}_P_KUBUN" emptystr=" "  name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>" onchange="${popupid}_setPKubunNm();" :disabled=" ${isAppr != 'N' || action == null || action =='appr' }"></fm-select>
				</div>
				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-spin id="${popupid}_USE_MM" name="USE_MM" title="<spring:message code="NC_FW_USE_MM_M" text="사용기간(개월)" />" min="6" :disabled="${isAppr != 'N' || action == null || action =='appr' }"></fm-spin>
				</div>
				<div class="col col-sm-12" v-if="${action == null || action == 'update' || action =='appr' }">
					<div class="hastitle">
						<label for="${popupid}_NAS" id='NAS_label'
							class="control-label grid-title value-title"
							 >
							<spring:message code="NC_NAS_NAS" text="NAS" />
							<c:if test="${isAppr eq 'N' && action == 'update'}">
								<button type="button" class="btn btn-primary btn icon" onclick="${popupid}_newNas()" title="btn_new">
									<i class="fa fat add fa-plus-square"></i>
								</button>
							</c:if>
						</label>
						<div class="hastitle output" id="${popupid}_NAS_TABLE" style="display: inline-flex; height:100%; overflow:auto; word-break:break-word;">
						</div>
					</div>
				</div>
				<div class="col col-sm-12" v-if="${action != 'update'}" >
					<c:if test="${isAppr != 'N' || action == null}">
						<div v-if="${action != 'update'}">
							<div v-html="form_data.CMT" class="output hastitle" style="min-height: 200px;overflow-y: auto;word-break: break-word;"></div>
						</div>
					</c:if>
					<c:if test="${!(isAppr != 'N' || action == null)}">
						<div v-if="${action == 'appr' }">
							<div v-html="form_data.CMT" class="output hastitle" style="min-height: 200px;overflow-y: auto;word-break: break-word;"></div>
						</div>
						<div v-else-if="${action != 'update'}">
							<textarea id="${popupid}_CMT" name="${popupid}_CMT" style="width:calc(100% - 150px);"></textarea>
						</div>
					</c:if>
				</div>
				<div class="col col-sm-6" v-if="${action != 'insert' && action != 'update'}">
					<fm-output id="${popupid}_INS_TMS" name="INS_TMS" title="<spring:message code="NC_VM_INS_TMS" text="등록일시" />" :value="formatDate(form_data.INS_TMS, 'datetime')"></fm-output>
				</div>
				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-output id="${popupid}_INS_NM" name="INS_ID_NM" title="<spring:message code="NC_VM_INS_ID_NM" text="담당자" />"></fm-output>
				</div>
			</div>
		</form>
	</div>
	<script>
		var editor = false;
		var diskUnitOptions = {
			G: 'GB',
			T: 'TB'
		}

		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {SPEC_NM:'', CATEGORY:'', GUEST_NM:'', USE_MM:0, CMT:'', INS_TMS:'', INS_ID_NM:'', P_KUBUN:'', P_KUBUN_NM:'', PURPOSE:'', VM_NM:'', VM_ID:'', DC_ID:'', DC_NM:'', OLD_SPEC_INFO:''};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});

		function onAfterVMOwnerChg()
		{
			if(${popupid}_callback != ''){
				eval(${popupid}_callback + '(${popupid}_vue.form_data);');
			}
			$('#${popupid}').modal('hide');
		}
		function ${popupid}_select_CATEGORY(data){
			${popupid}_vue.form_data.CATEGORY = data.CATEGORY;
			${popupid}_vue.form_data.CATEGORY_NM = data.CATEGORY_FULL_NM;
			$("#${popupid}_CATEGORY_NM").val(data.CATEGORY_FULL_NM);
		}
		var ${popupid}_callback = '${callback}';
		function ${popupid}_click(vue, param){
			${popupid}_clearNas();

			var ac = "${action}"
			 

			$('#${popupid}_DISK_SIZE').removeClass('invalid-size').css('color', '#555555');
			if(param.DC_ID) {
				${popupid}_param = $.extend(${popupid}_param, param);
				//${popupid}_vue.form_data = ${popupid}_param;
				editor = "${action}";
				var disk = getDiskSize(param.NC_VM_DATA_DISK);
				if(disk.DEF_DISK_SIZE == 0) {
					${popupid}_vue.form_data.OS_DISK_SIZE = ${popupid}_param.DISK_SIZE - disk.DISK_SIZE;
				}else {
					${popupid}_vue.form_data.OS_DISK_SIZE = disk.DEF_DISK_SIZE;
				}
				if(disk.DISK_SIZE % 1024 == 0 && disk.DISK_SIZE > 0) {
					disk.DISK_SIZE = Math.floor(disk.DISK_SIZE / 1024);
					${popupid}_vue.form_data.DISK_UNIT = 'T';
				}
				${popupid}_vue.form_data.DISK_SIZE = disk.DISK_SIZE;
				
				
				if('${action}' != 'update'){
					var insIdName = ${popupid}_vue.form_data.INS_ID_NM;
					 
					 
					if(!isEmpty(insIdName)){
						$("#${popupid}_INS_NM").html(insIdName);
					}
				}
									
				<c:if test="${action != 'update' && !(isAppr != 'N' || action == null) && action != 'appr' }">
					$("#${popupid}_CMT").Editor("setText", ${popupid}_param.CMT ? ${popupid}_param.CMT:"");
				</c:if>
				<c:if test="${action == null || action == 'update' || action == 'appr' }">

					<c:if test="${isAppr == 'Y'}">
							 
						var html = '';
						if( ${popupid}_vue.form_data.NAS != ${popupid}_vue.form_data.NAS_OLD) {
							$("#NAS_label").addClass("chg");
							html +=  (${popupid}_vue.form_data.NAS_OLD ? '<div class="col-xs-4">'+chgHighlight(${popupid}_vue.form_data.NAS,${popupid}_vue.form_data.NAS_OLD,'chg1',null):'') 
								+ '</div> <div class="col-xs-4" style="width: 9%; height: 100%; padding-left: 0px; margin: auto 0 auto;"> <i   class="fa fa-arrow-circle-right" style="font-size: 20px;"></i> </div> '
						}
						html +=  "<div class='col-xs-4'>" + chgHighlight(${popupid}_vue.form_data.NAS_OLD, ${popupid}_vue.form_data.NAS,null,'') + "</div>";
						html += '</div>'
						$("#${popupid}_NAS_TABLE").html(html);
						
						if( ${popupid}_vue.form_data.SPEC_ID != ${popupid}_vue.form_data.SPEC_ID_OLD || ${popupid}_vue.form_data.DISK_SIZE>0 ) {
							$(".SPEC_INFO_div label, .SPEC_INFO_div .output").addClass("chg");							
						}


					</c:if>
					<c:if test="${isAppr == 'N'}">
						if(${popupid}_vue.form_data.NAS) {
							var nasArr = ${popupid}_vue.form_data.NAS.split(",");
							nasArr.forEach(function(value,index){
								${popupid}_makeNas(nasArr[index], true);
								
							})
							//for(var i in nasArr){
							//}
						}
						 
					</c:if>
				</c:if>
				if("${action}"== "save"){
					/*if(${popupid}_vue.form_data.PURPOSE && ${popupid}_vue.form_data.PURPOSE != null &&  ${popupid}_vue.form_data.PURPOSE  != ''){
							$("#${popupid}_PURPOSE").prop("disabled",true);
					}
					else{
						$("#${popupid}_PURPOSE").prop("disabled",false);
					}
					if(${popupid}_vue.form_data.P_KUBUN && ${popupid}_vue.form_data.P_KUBUN != null &&  ${popupid}_vue.form_data.P_KUBUN  != ''){
							$("#${popupid}_P_KUBUN").prop("disabled",true);
					}
					else{
						$("#${popupid}_P_KUBUN").prop("disabled",false);
					}
					if( ${popupid}_vue.form_data.CATEGORY && ${popupid}_vue.form_data.CATEGORY != null &&  ${popupid}_vue.form_data.CATEGORY  != ''){
							$("#category_button").prop("disabled",true);
					}
					else{
						$("#${popupid}_category_button").prop("disabled",false);
					}*/

				}
				try {
					${popupid}_chgCPURAM() ;
				}catch(e){}
				try {
					${popupid}_click_after();
				}catch(e){}
				return true;
			} else {
				${popupid}_param = null;
				alert(msg_select_first);
				return false;
			}
		}

		function ${popupid}_onAfterOpen(){
			var headerTitle = '<spring:message code="label_vm_info" text=""/>';
			var status = '';
			if(${popupid}_vue.form_data.CUD_CD) status += ${popupid}_vue.form_data.CUD_CD_NM;
			if(${popupid}_vue.form_data.APPR_STATUS_CD) status += ' ' + ${popupid}_vue.form_data.APPR_STATUS_CD_NM;
			if(status != '') status = '(' + status + ')';
			headerTitle += status;
			$('#${popupid}_title').text(headerTitle);
		}

		function ${popupid}_to_work(){
			${popupid}_beforeSaveReq();
			<c:if test="${action != 'update' && !(isAppr != 'N' || action == null)}">
				${popupid}_vue.form_data.CMT = $("#${popupid}_CMT").Editor("getText" );
			</c:if>
			if(!${popupid}_validationSpecChange()) return;
			var action = 'insertReq';
			if(${action == 'update'}) action = 'updateReq';
			post('/api/vm/' + action, ${popupid}_vue.form_data,  function(data){
				${popupid}_tuingSpecInfo();
				if(confirm(msg_complete_work_move)){
					location.href="/clovirsm/workflow/work/index.jsp";
					return;
				}
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '(${popupid}_vue.form_data);');
				}
				$('#${popupid}').modal('hide');
			});
		}
		function ${popupid}_newNas(){
			$("#${ popupid }_search_form_popup_button").click();
		}
		function ${ popupid }_select_nas(data, callback){
			 
			if(callback){
				callback.call();
			}
			for(var i=0; i<data.length; i++){
				var row = data[i];
				if(!${popupid}_chk_dupl_nas(row.PATH)){
					continue;
				}
				${popupid}_makeNas(row.PATH, true);
			}
//			for(var i in data) {
//			}
		}

		function ${popupid}_clearNas(){
			$("#${popupid}_NAS_TABLE").html("");
		}

		function ${popupid}_makeNas(path, delAble) {
			var table = $("#${popupid}_NAS_TABLE");
			var html = '<div class="nas_item choice" style="width: fit-content;float:left;">';
			if(delAble == true){
				html += '<span class="choice_remove" onclick="delete_nas(this);">×</span>';
			}
			html += '<span class="nas_text">'
			html += path;
			html += "</span></div>";
			table.append(html);
		}

		function ${popupid}_chk_dupl_nas(path){
			var list = $("#${popupid}_NAS_TABLE .nas_text");
			for(var i=0; i<list.length; i++){
				if(isNaN(i) || i == null) {
					continue;
				}
				var val = $(list[i]).text();
				if(path == val) {
					return false;
				}
			}
			return true;
//			for(var i in list) {
//			}
		}

		function delete_nas(that){
			var _this = $(that);
			_this.parent().remove();
		}

		function ${popupid}_request(){
			${popupid}_beforeSaveReq();
			<c:if test="${action != 'update' && !(isAppr != 'N' || action == null)}">
				${popupid}_vue.form_data.CMT = $("#${popupid}_CMT").Editor("getText" );
			</c:if>
			if(!${popupid}_validationSpecChange()) return;
			${popupid}_vue.form_data.DEPLOY_REQ_YN = 'Y';
			var action = 'insertReq';
			if(${action == 'update'}) action = 'updateReq';
			post('/api/vm/' + action, ${popupid}_vue.form_data,  function(data){
				${popupid}_tuingSpecInfo();
				alert(msg_complete_work);
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '(${popupid}_vue.form_data);');
				}
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_save_update_req(){
			${popupid}_beforeSaveReq();
			<c:if test="${action != 'update' && !(isAppr != 'N' || action == null)}">
				${popupid}_vue.form_data.CMT = $("#${popupid}_CMT").Editor("getText" );
			</c:if>
			if(!${popupid}_validationSpecChange()) return;
			post('/api/vm/updateReq', ${popupid}_vue.form_data,  function(data){
				${popupid}_tuingSpecInfo();
				alert(msg_complete);
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '(${popupid}_vue.form_data);');
				}
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_save(){
			${popupid}_beforeSaveReq();
			<c:if test="${action != 'update' && !(isAppr != 'N' || action == null)}">
				${popupid}_vue.form_data.CMT = $("#${popupid}_CMT").Editor("getText" );
			</c:if>
			var action = 'insertReq';
			if(${action == 'insert'}) action = 'insertReq';
			else if(${action == 'save'}) action = 'save';
			post('/api/vm/' + action, ${popupid}_vue.form_data,  function(data){
				${popupid}_tuingSpecInfo();
				alert(msg_complete);
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '(${popupid}_vue.form_data);');
				}
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_getSpecSearchParam(){
			return {
				DC_ID: ${popupid}_vue.form_data.DC_ID,
				SPEC_NM: $('#${popupid}_SPEC_NM').val()
			}
		}

		function select_${popupid}_spec(spec, callback){
			 
			${popupid}_vue.form_data.SPEC_ID = spec.SPEC_ID;
			${popupid}_vue.form_data.SPEC_NM = spec.SPEC_NM;
			${popupid}_vue.form_data.CPU_CNT = spec.CPU_CNT;
			//${popupid}_vue.form_data.DISK_SIZE = spec.DISK_SIZE;
			//${popupid}_vue.form_data.DISK_UNIT = 'G';
			${popupid}_vue.form_data.RAM_SIZE = spec.RAM_SIZE;
			//${popupid}_vue.form_data.SPEC_INFO = getSpecInfo(${popupid}_vue.form_data);
			$("#${popupid}_SPEC_NM").val(spec.SPEC_NM);
			$("#${popupid}_NEW_SPEC_INFO").text(getSpecInfo(null, spec.CPU_CNT,spec.RAM_SIZE,${popupid}_vue.form_data.OS_DISK_SIZE, 'G'))
			// 
			//${popupid}_validationDisk();
			var obj = ${popupid}_vue.form_data;
			getDayFee(obj.DC_ID,obj.CPU_CNT, obj.RAM_SIZE, '', obj.OS_DISK_SIZE + obj.DISK_SIZE, 'G', obj.SPEC_ID,'${popupid}_DD_FEE' )
			if(callback) callback();
		}

		function ${popupid}_tuingSpecInfo(){
			var data = ${popupid}_vue.form_data;
			data.SPEC_INFO = getSpecInfo(data.SPEC_NM, data.CPU_CNT, data.RAM_SIZE, data.DISK_SIZE, data.DISK_UNIT, data.DISK_TYPE_NM);
		}

		function ${popupid}_setPurposeNm(){
			var purposeNm = $('#${popupid}_PURPOSE option:selected').text();
			${popupid}_vue.form_data.PURPOSE_NM = purposeNm;
		}

		function ${popupid}_setPKubunNm(){
			var pkubunNm = $('#${popupid}_P_KUBUN option:selected').text();
			${popupid}_vue.form_data.P_KUBUN_NM = pkubunNm;
		}

		/*function ${popupid}_validationDisk(){
			setTimeout(function(){
				 
				if(${popupid}_vue.form_data.CUD_CD == 'C' || (${popupid}_vue.form_data.RUN_CD != 'R' && ${popupid}_vue.form_data.RUN_CD != 'S')) return;
				var oldUnitComp = 1;
				if(${popupid}_vue.form_data.OLD_DISK_UNIT == 'T') oldUnitComp = 1024;

				var unitComp = 1;
				if(${popupid}_vue.form_data.DISK_UNIT == 'T') unitComp = 1024;
				 
				if(${popupid}_vue.form_data.OLD_DISK_SIZE*oldUnitComp > ${popupid}_vue.form_data.DISK_SIZE*unitComp) {
					$('#${popupid}_DISK_SIZE').addClass('invalid-size').css('color', 'red');
				} else {
					$('#${popupid}_DISK_SIZE').removeClass('invalid-size').css('color', '#555555');
				}
			}, 10);
		}*/

		function ${popupid}_beforeSaveReq(){
			var list = $("#${popupid}_NAS_TABLE .nas_text");
			var nasStr = [];
			
			for(var i=0; i<list.length; i++){
				if(isNaN(i) || i == null) {
					continue;
				}
				var val = $(list[i]).text();
				if(val && val.trim() != "") {
					nasStr.push(val);
				}
			}
			if(nasStr[0] != null) {
				${popupid}_vue.form_data.NAS = nasStr.join(",");
			}
//			for(var i in list) {
//			}
		}

		function ${popupid}_validationSpecChange(){
			if(${popupid}_vue.form_data.OLD_SPEC_ID == ${popupid}_vue.form_data.SPEC_ID
				&& ${popupid}_vue.form_data.OLD_CPU_CNT == ${popupid}_vue.form_data.CPU_CNT
				&& ${popupid}_vue.form_data.OLD_RAM_SIZE == ${popupid}_vue.form_data.RAM_SIZE
				&& ${popupid}_vue.form_data.OLD_DISK_SIZE == ${popupid}_vue.form_data.DISK_SIZE
				&& ${popupid}_vue.form_data.OLD_DISK_UNIT== ${popupid}_vue.form_data.DISK_UNIT
				&& ${popupid}_vue.form_data.OLD_NAS == ${popupid}_vue.form_data.NAS){
				 
				alert('<spring:message code="msg_change_spec"/>');
				return false;
			}

			if($('#${popupid}_DISK_SIZE').hasClass('invalid-size')){
				alert('<spring:message code="msg_disk_size_re_setting"/>');
				$('#${popupid}_DISK_SIZE').select();
				return false;
			}
			try {
				${popupid}_makeDataDiskParam(${popupid}_vue.form_data);
			} catch(e) {
				 
			}
			if(${popupid}_vue.form_data.DISK_UNIT == 'T') {
				${popupid}_vue.form_data.DISK_UNIT = 'G';
				${popupid}_vue.form_data.DISK_SIZE = ${popupid}_vue.form_data.DISK_SIZE * 1024;
			}
			${popupid}_vue.form_data.DISK_SIZE = Number(${popupid}_vue.form_data.DISK_SIZE) + Number(${popupid}_vue.form_data.OS_DISK_SIZE);
			return true;
		}
		$('#${popupid}_DISK_SIZE').css('width', '100px');
		$(function(){
			<c:if test="${action != 'update' && !(isAppr != 'N' || action == null)}">
				$("#${popupid}_CMT").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false});
			</c:if>
		})
	</script>
</fm-modal>
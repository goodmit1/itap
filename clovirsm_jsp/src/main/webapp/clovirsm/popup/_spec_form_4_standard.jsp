<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
    <div class="col col-sm-12">
		<fm-ibutton id="${popupid}_SPEC_NM" name="SPEC_NM" title="<spring:message code="NC_VM_SPEC_NM" text="사양"/>" class="inline" required="true"  >
			<fm-popup-button popupid="${popupid}_spec_search_popup" popup="../../popup/spec_search_form_popup.jsp" cmd="update" param="{}" callback="select_${popupid}_spec"><spring:message code="btn_spec_search" text="사양검색"/></fm-popup-button>
		</fm-ibutton>
		<fm-output id="${popupid}_SPEC_INFO" name="SPEC_INFO" :value="getSimpleSpecInfo(form_data)" class="inline white"  ></fm-output>
	</div>
	<div class="col col-sm-6">
		<fm-spin id="${popupid}_DISK_SIZE" title="<spring:message code="NC_IMG_DISK_SIZE" text="디스크사이즈" />" name="DISK_SIZE" required="true"  onchange="${popupid}_validationDisk()" min="1"  >
			<fm-select id="${popupid}_DISK_UNIT" name="DISK_UNIT" class="inline" :options="diskUnitOptions" style="width:50px;display:inline-block;" onchange="${popupid}_validationDisk()"  ></fm-select>

		</fm-spin>
	</div>
	<div class="col col-sm-6">
	<fm-output title="<spring:message code="label_DD_FEE" text="비용"/>" id="${popupid}_DD_FEE" name="DD_FEE" :value="formatNumber(form_data.DD_FEE)" class="inline white"  ></fm-output>
	</div>
<script>
function ${popupid}_calcDiskFee(){
	var obj = ${popupid}_vue.form_data;
	getDayFee(obj.DC_ID,obj.CPU_CNT, obj.RAM_SIZE, '',obj.DISK_SIZE, obj.DISK_UNIT, obj.SPEC_ID, '${popupid}_DD_FEE')
	${popupid}_vue.form_data.SPEC_INFO = getSimpleSpecInfo(obj);
}
function ${popupid}_click_after()
{
	${popupid}_calcDiskFee();
}
function ${popupid}_validationDisk(){
	setTimeout(function(){
		${popupid}_calcDiskFee();
	}, 10);
}
</script>
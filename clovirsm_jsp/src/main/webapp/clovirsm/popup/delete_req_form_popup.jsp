<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String svc = request.getParameter("svc");
	request.setAttribute("svc", svc);

	String key = request.getParameter("key");
	request.setAttribute("key", key);

	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);
%>
<fm-modal id="${popupid}" :title="(form_data.HEADER_NAME ? form_data.HEADER_NAME + ' ': '') + '<spring:message code="delete_request" text="삭제요청"/>'" cmd="header-title">
	<span slot="footer">
		<input type="button" class="btn ${popupid}_req_btn" value="<spring:message code="btn_work" text="작업함에 담기"/>" onclick="${popupid}_to_work()" />
		<input type="button" class="btn ${popupid}_req_btn" value="<spring:message code="btn_request" text="바로 요청"/>" onclick="${popupid}_request()" />
		<input type="button" class="btn ${popupid}_delete_btn" value="<spring:message code="btn_delete" text="삭제"/>" onclick="${popupid}_delete()" />
	</span>
	<form id="${popupid}_form" action="none">
		<div id="${popupid}_delete_req_confirm"><spring:message code="confirm_delete_request" text="삭제요청하시겠습니까?"/></div>
		<div id="${popupid}_delete_confirm"><spring:message code="confirm_delete" text="삭제하시겠습니까?"/></div>
	</form>
	<script>
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		var ${popupid}_callback = '${callback}';
		function ${popupid}_click(vue, param){
			 

			if(param && param.${key == null ? 'A':key}){
				${popupid}_param = $.extend({}, param);
				${popupid}_vue.form_data = ${popupid}_param;

				if(${popupid}_param.CUD_CD == 'C'){
					$('#${popupid}_delete_req_confirm').hide();
					$('.${popupid}_req_btn').hide();

					$('#${popupid}_delete_confirm').show();
					$('.${popupid}_delete_btn').show();

				} else {
					$('#${popupid}_delete_req_confirm').show();
					$('.${popupid}_req_btn').show();

					$('#${popupid}_delete_confirm').hide();
					$('.${popupid}_delete_btn').hide();
				}

				return true;
			} else {
				${popupid}_param = null;
				alert('<spring:message code="msg_please_select_for_delete" text="삭제할 항목을 선택하세요." />');
				return false;
			}
		}

		function ${popupid}_delete(){
			post('/api/${svc}/deleteReq', ${popupid}_param,  function(data){
				alert(msg_complete);
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '();');
				}
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_to_work(){
			post('/api/${svc}/deleteReq', ${popupid}_param,  function(data){
				if(confirm(msg_complete_work_move)){
					location.href="/clovirsm/workflow/work/index.jsp";
					return;
				}
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '();');
				}
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_request(){
			${popupid}_param.DEPLOY_REQ_YN = 'Y';
			post('/api/${svc}/deleteReq', ${popupid}_param,  function(data){
				alert(msg_complete_work);
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '();');
				}
				$('#${popupid}').modal('hide');
			});
		}

	</script>
</fm-modal>
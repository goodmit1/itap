<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
	String url = request.getParameter("url");
	request.setAttribute("url", url);
%>
<style>
#vmDelCheckList_popup .modal-body{
	min-height: 300px;
}
.checks {position: relative;}
.etrans{
	margin-bottom:10px;
}
.checks input[type="checkbox"] {  /* 실제 체크박스는 화면에서 숨김 */
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip:rect(0,0,0,0);
  border: 0
}
.checks input[type="checkbox"] + label {
  display: inline-block;
  position: relative;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
}
.checks input[type="checkbox"] + label:before {  /* 가짜 체크박스 */
  content: ' ';
  display: inline-block;
  width: 21px;  /* 체크박스의 너비를 지정 */
  height: 21px;  /* 체크박스의 높이를 지정 */
  line-height: 21px; /* 세로정렬을 위해 높이값과 일치 */
  margin: -2px 8px 0 0;
  text-align: center; 
  vertical-align: middle;
  background: #fafafa;
  border: 1px solid #cacece;
  border-radius : 3px;
  box-shadow: 0px 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
}
.checks input[type="checkbox"] + label:active:before,
.checks input[type="checkbox"]:checked + label:active:before {
  box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
}

.checks input[type="checkbox"]:checked + label:before {  /* 체크박스를 체크했을때 */ 
  content: '\2714';  /* 체크표시 유니코드 사용 */
  color: #99a1a7;
  text-shadow: 1px 1px #fff;
  background: #e9ecee;
  border-color: #adb8c0;
  box-shadow: 0px 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1);
}
.checks.etrans input[type="checkbox"] + label {
  padding-left: 30px;
}
.checks.etrans input[type="checkbox"] + label:before {
  position: absolute;
  left: 0;
  top: 0;
  margin-top: 0;
  opacity: .6;
  box-shadow: none;
  border-color: #6cc0e5;
  -webkit-transition: all .12s, border-color .08s;
  transition: all .12s, border-color .08s;
}

.checks.etrans input[type="checkbox"]:checked + label:before {
  position: absolute;
  content: "";
  width: 10px;
  top: -5px;
  left: 5px;
  border-radius: 0;
  opacity:1; 
  background: transparent;
  border-color:transparent #6cc0e5 #6cc0e5 transparent;
  border-top-color:transparent;
  border-left-color:transparent;
  -ms-transform:rotate(45deg);
  -webkit-transform:rotate(45deg);
  transform:rotate(0deg);
}

.checks.etrans input[type="checkbox"]:checked + label:before {
  /*content:"\2713";*/
  content: "\2714";
  top: 0;
  left: 0;
  width: 21px;
  line-height: 21px;
  color: #6cc0e5;
  text-align: center;
  border: 1px solid #6cc0e5;
}
#delVm:disabled{
  background-color: #de9696 !important;
}
</style>

<fm-modal id="${popupid}" title='<spring:message code="del_check_list" text="삭제체크리스트"/>' cmd="header-title">
	<span slot="footer" id="${popupid}_footer">
		<button type="button" id="delVm" class="detailBtn red divRelative" onclick="del_vm()" disabled><spring:message code="request" text="요청"/></button>
	</span>
	<div class="form-panel detail-panel">
		<div id="checkList"></div>
		<div id="stepFinish" class="stepFinish hide" style="padding-top: 70px;">
			<div><spring:message code="REQ_MSG_M1" text=""/></div>
			<div><spring:message code="REQ_MSG_M2" text=""/></div>
			<div><spring:message code="REQ_MSG_M5" text=""/></div>
			<div><spring:message code="REQ_MSG_M6" text=""/></div>
			<div class="btn_group">
				<fm-sbutton  class="exeBtn popupBtn finish stepBtn" cmd="search" onclick="endModal()"><spring:message code="FINISH_NEXT_PAGE" text=""/></fm-sbutton>
			</div>
		</div>
	</div>
	<script>
		var count = 0;
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		$(document).ready(function(){
		})

		
		
		function ${popupid}_click(vue, param, callback){
			${popupid}_parent_vue = vue;
			if(param != null){
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
			}
			${popupid}_vue.callback = callback;
			vm_del_check_search();
			 
			${popupid}_vue.form_data = ${popupid}_param;
			return true;
		}
		function vm_del_check_search(){
			count = 0;
			post('/api/code_list?grp=VM_DEL_CHECK',null,function(data){
				
				var keys = Object.keys(data);
			      keys.forEach(function(key, i)
			      {
			    	  count++;
			    	  var html= "<div class='checks etrans'>";
						html += "<input type='checkbox' onchange='vm_del_check_change();' name='chk_info' id='m"+i+"'><label for='m"+i+"'>"+data[key]+"</label>";
						html += "</div>";
						$("#checkList").append(html);
			      });
			});
		}
		function vm_del_check_change(){
			if($('input:checkbox[name=chk_info]').filter(':checked').size() == count){
				$("#delVm").attr("disabled",false);
			} else{
				$("#delVm").attr("disabled",true);
			}
		}
		function del_vm(){
			if($('input:checkbox[name=chk_info]').filter(':checked').size() == count){
				var selected_json = new Object();
				for(var i = 0 ; i < ${popupid}_param.length ; i++){
					if(${popupid}_param[i].hasOwnProperty('CMT'))
						delete ${popupid}_param[i].CMT;
				}
				selected_json.selected_json = JSON.stringify(${popupid}_param);
				selected_json.DEPLOY_REQ_YN = "Y";
				post('/api/vm/deleteReq_multi', selected_json ,function(){
					$('#${popupid}').modal('hide');
					FMAlert("<img src='/res/img/hynix/confirm.png' style='margin-bottom:20px;margin-top: 20px;'><div style='font-size: 16px; font-weight: 900; margin-bottom:20px;'><spring:message code="REQ_MSG_M1" text=""/></div><div><spring:message code="REQ_MSG_M2" text=""/></div><div><spring:message code="REQ_MSG_M5" text=""/></div><div><spring:message code="REQ_MSG_M6" text=""/></div>",'<spring:message code="request_complete" text="요청완료" />','<spring:message code="go_history_page" text="신청이력페이지로 이동" />', function(){location.href="/clovirsm/workflow/requestItemsHistory/index.jsp"; } );					 
					if(${popupid}_vue.callback) {
						eval(${popupid}_vue.callback + '(${popupid}_vue.form_data);');
					}
					
				});
			} else{
				$('#${popupid}').modal('hide');
				FMAlert("<img src='/res/img/hynix/caution.png' style='margin-bottom:20px;margin-top: 20px;'><div style='font-size: 16px; font-weight: 900; margin-bottom:20px;'><spring:message code="REQ_MSG_M1" text=""/></div><div><spring:message code="REQ_MSG_M2" text=""/></div>",'요청 실패',null, null);					 
				if(${popupid}_vue.callback) {
					eval(${popupid}_vue.callback + '(${popupid}_vue.form_data);');
				}
			}
			
		}
		function endModal(){
			location.href="/clovirsm/workflow/requestItemsHistory/index.jsp";
			return true;
		}
	</script>
	<style>

	</style>
</fm-modal>
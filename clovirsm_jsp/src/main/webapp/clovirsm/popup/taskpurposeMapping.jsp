<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
	String url = request.getParameter("url");
	request.setAttribute("url", url);
%>
<style>
#${popupid} .modal-body {
    height: 100px;
}
}
</style>

<fm-modal id="${popupid}" title='<spring:message code="add_purpose" text="용도 추가"/>' cmd="header-title">
	<span slot="footer">
		<input type="button" class="btn saveBtn popupBtn finish top5 " value="<spring:message code="btn_save" text="저장" />" onclick="savePurpose()" />
	</span>
	<div class="form-panel detail-panel">
		<div class="col col-sm-12" style="border: none;">
			<fm-input id="SS_PURPOSE" name="ID" input_style="width:150px;" label_style="border: none; background-color: white; padding-top: 14px !important;" title="<spring:message code="PURPOSE" text="용도"/>" maxlength="5" onkeyup='checkPurpose(this);'  required="required"></fm-input>
		</div>
		<div>※ <spring:message code="msg_five_character" text="5글자 미만 대문자를 입력하세요."/></div>
	</div>
	<script>
		var count = 0;
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		$(document).ready(function(){
			autocomplete("#SS_PURPOSE", "/api/ddicUser/list/list_FM_DDIC_search/?DD_ID=PURPOSE", "KO_DD_DESC", "KO_DD_DESC", 1,function(){});

		})
		
		function checkPurpose(that){
			
			   var regexp = /^[A-Z]*$/;
			   if(regexp.test($(that).val())){
				  return true;
			   } else{
				   $(that).val('');
				   alert('<spring:message code="msg_five_character" text="5글자 미만 대문자를 입력하세요."/>');
				   return false;
			   }
			
		}
		function savePurpose(){
			if(checkPurpose($("#SS_PURPOSE"))){
				var obj = new Object();
				obj.ID = $('#SS_PURPOSE').val();
				obj.CATEGORY_ID = ${popupid}_param.CATEGORY_ID;
				obj.KUBUN = "P";
				obj.USE_YN = "Y";
				post("/api/category/map/savePurpose", obj, function(){
					if(${popupid}_vue.callback) {
						eval(${popupid}_vue.callback + '( obj );');
					}
					$("#${popupid}").modal("hide");
				});
			}
		}
		function ${popupid}_click(vue, param, callback){
			${popupid}_parent_vue = vue;
			if(param != null){
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
			}
			console.log(callback);
			${popupid}_vue.callback = callback;
			${popupid}_vue.form_data = ${popupid}_param;
			return true;
		}
		
		
		$('#${popupid}').modal('hide');
	</script>
	<style>

	</style>
</fm-modal>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
%>
<style>

#reuse_popup .modal-dialog{
	width : 1000px;
}
#reuse_popup .modal-dialog .form-panel.detail-panel{
	height : 500px;
}
#reuse_popup .input-group.hastitle{
        width: 199px !important;
}
#S_ADD_USE_MM {
    width: 70%;
}
</style>

<fm-modal id="${popupid}" title='<spring:message code="title_server_reuse" text="서버 복원"/>' cmd="header-title">
	<span slot="footer">
		
		<button type="button" class="popupBtn next top5" onclick="expire()"><spring:message code="title_server_reuse" text="서버 복원"/></button>
	</span>
	<div class="form-panel detail-panel" style="height: 62px;">
		<div class="col col-sm-12" style="text-align: left;">
			<fm-date id="S_ADD_USE_MM" name="ADD_USE_MM " title="<spring:message code="USE_MM" text="사용기간" />"></fm-date>
		</div>
	</div>
	<script>
		var appr = null;
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		
		function ${popupid}_click(vue, param){
			 
			${popupid}_parent_vue = vue;
			if(param != null){
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
				alert(msg_select_first);
				return;
			}
			${popupid}_vue.form_data = ${popupid}_param;
			return true;
		}
		
		function day(d){
			var sdt = new Date();
			var edt = new Date(d);
			var dateDiff = Math.ceil((edt.getTime()-sdt.getTime())/(1000*3600*24));
			 
			return dateDiff
		}
		function expire(){
			if($("#S_ADD_USE_MM").val()){
				var param = ${popupid}_param;
				param.ADD_USE_MM = day($('#S_ADD_USE_MM').val());
				if(param.ADD_USE_MM > 0){
					post('/api/expire/update_reuse',param,function(data){
						if(data){
							alert(msg_complete);
							$('#${popupid}').modal('hide');
						} else{
							alert("<spring:message code="err_date" text=""/>");
						}
					});
				} else{
					alert(msg_jsp_error);
				}
				
			}
		}
	</script>
	<style>

	</style>
</fm-modal>
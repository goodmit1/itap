<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %><%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<!--  VM 정보 수정 -->
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	 
%>
<style>
	#${popupid} .modal-dialog {
		width: 600px;
	}
	/* #${popupid}_NAS_TABLE > div:nth-child(2) > i {
		top: 50%;
    	transform: translateY(-50%);
	} */
</style>
<fm-modal id="${popupid}" title="<spring:message code="btn_extendPeriod_req" text="기간 연장 요청"/>" cmd="header-title">

<link href="/res/css/editor.css" type="text/css" rel="stylesheet"/>
<script src="/res/js/editor.js"></script>
	<span slot="footer">
		<input type="button" class="btn saveBtn popupBtn finish top5 " value="<spring:message code="request" text="요청" />" onclick="${popupid}_save()" />
	</span>
	<div class="form-panel detail-panel">
		<form id="${popupid}_form" action="none">
			<div class="panel-body" id="panel-body">
				 
				<div class="col col-sm-12" v-if="${action != 'update'}">
 
					<fm-select id="${popupid}_ADD_USE_MM" emtpyStr="" required="true" name="ADD_USE_MM" title="<spring:message code="expiredate" text="연장기간(개월)" />"   ></fm-spin>
 
				</div>
				 
				<div class="col col-sm-12">
					<fm-textarea id="${popupid}_REASON" name="REASON" title="<spring:message code="REASON" text="사유" />"></fm-output>
				</div>
			</div>
		</form>
	</div>
	<script>
	var param;
	var editor = false;
 

	var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");

	var ${popupid}_param = {SPEC_NM:'', CATEGORY:'', GUEST_NM:'',   CMT:'', INS_TMS:'', INS_ID_NM:'', P_KUBUN:'', P_KUBUN_NM:'', PURPOSE:'', VM_NM:'', VM_ID:'', DC_ID:'', DC_NM:'', OLD_SPEC_INFO:'',ADD_USE_MM:0};

	var ${popupid}_vue = new Vue({
		el: '#${popupid}',
		data: {
			form_data: ${popupid}_param
		}
	});
	var monthList;
	function setUseMM( ){
		if(monthList){
			fillMonth(${popupid}_vue.form_data.p_kubun_arr)
		}
		else{
			var specParam = new Object();
			var monthArray = new Array();
			specParam.KUBUN = 'MONTH';
			post("/api/code_list?grp=dblist.com.clovirsm.common.Component.selectSpecSetting",specParam, function(data){
				monthList={};
				for(var q = 0 ; q < data.length; q++){
					monthList[data[q].ID] = data[q].VAL1.split(",");
				
				 
				}
				fillMonth(${popupid}_vue.form_data.p_kubun_arr);
			});
		}
		
	}
	 
	
	function fillMonth(kubunArr){
		var arr = monthList[kubunArr[0]]
		var result = [];
		for(var i=0; i<arr.length; i++){
			var allFind = true;
			for(var j=1; j < kubunArr.length; j++){
				var find = hasMonthValue(kubunArr[j], arr[i]);
				if(!find){
					allFind = false;
					break;
				}
			}
			if(allFind){
				var valListS = arr[i].split("|");
				var object = new Object();
				if(valListS.length > 1){
					object.title = valListS[1];
					object.val = valListS[0];
				} else{
					object.title = arr[i];
					object.val = arr[i];
				}
				result.push(object);
			}
		}
		if(result.length==0){
			alert("<spring:message code="select_same_p_kubun" text="같은 구분끼리 선택해 주세요." />")
		}
		fillOptionByData("${popupid}_ADD_USE_MM", result, '', "val", "title");
	}
	function hasMonthValue(kubun, val){
		var arr = monthList[kubun]
		for(var i=0; i < arr.length; i++){
			if(arr[i]==val){
				return true;
			}
		}
		return false;
	} 
	function endModal(){
		location.href="/clovirsm/workflow/requestItemsHistory/index.jsp";
		return true;
	}
	var ${popupid}_callback = '${callback}';
	function ${popupid}_click(vue, param){
		
		${popupid}_vue.form_data = $.extend(${popupid}_vue.form_data, param)
		var arr = param.VM_LIST;
		var p_kubun = {};
		for(var i=0; i < arr.length; i++){
			p_kubun[arr[i].P_KUBUN] = '-'
		}
		${popupid}_vue.form_data.p_kubun_arr = Object.keys(p_kubun);
		 
		setUseMM();
		return true;
		 
	}
	 

	 
	
	 
	function ${popupid}_save(){
		
	
		 
		if(validate("panel-body")){
			var selected_json = new Object();
			var arr = ${popupid}_vue.form_data.VM_LIST;
			for(var i=0; i < arr.length; i++){
				if(arr[i].hasOwnProperty("CMT"))
					delete arr[i].CMT;
				console.log(arr[i]);
				var data = arr[i];
				data.ADD_USE_MM= $("#${popupid}_ADD_USE_MM").val();
				data.TITLE = data.VM_NM;
				data.SVC_ID= data.VM_ID;
				data.SVC_CD = 'S';
				data.SVC_TYPE = 'E';
				data.REASON = $("#${popupid}_REASON").val();
			}
			selected_json.selected_json = JSON.stringify(${popupid}_vue.form_data.VM_LIST);
			selected_json.DEPLOY_REQ_YN = "Y";
			post('/api/extendPeriod/updateReq_multi', selected_json, function(data){
				$('#${popupid}').modal('hide');
				FMAlert("<img src='/res/img/hynix/confirm.png' style='margin-bottom:20px;margin-top: 20px;'><div style='font-size: 16px; font-weight: 900; margin-bottom:20px;'><spring:message code="REQ_MSG_M1" text=""/></div><div><spring:message code="REQ_MSG_M2" text=""/></div><div><spring:message code="REQ_MSG_M5" text=""/></div><div><spring:message code="REQ_MSG_M6" text=""/></div>",'<spring:message code="request_complete" text="요청완료" />','<spring:message code="go_history_page" text="신청이력페이지로 이동" />', function(){location.href="/clovirsm/workflow/requestItemsHistory/index.jsp"; } );
				vmSearch();
			});
		}
	
	}

	   
	</script>
</fm-modal>
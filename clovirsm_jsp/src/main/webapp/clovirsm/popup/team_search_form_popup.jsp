<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
String popupid = request.getParameter("popupid");
pageContext.setAttribute("popupid", popupid);
%>
<fm-modal id="${popupid}" title="<spring:message code="FM_TEAM_TEAM_CD"/>" cmd="header-title" >
		<span slot="footer">
			<input type="button" class="btn" value="<spring:message code="no_select" text="선택없음"/>" onclick="${popupid}_popup_select([{}]);return false;" >
			<input type="button" class="btn" value="<spring:message code="btn_reload" text="새로고침"/>" onclick='${popupid}_reloadTree();return false;' >
		</span>
	<form id="${popupid}_popup_form" action="none">
	<div class="panel-body" style="height:300px;overflow:auto">
		<fm-tree id="${popupid}_tree" url="/api/code_list?grp=dblist.com.clovirsm.common.Component.tree_FM_TEAM"
			:field_config="{onselect:${popupid}_popup_select, root:0,id:'TEAM_CD',parent:'PARENT_CD',text:'TEAM_NM'}" >
		</fm-tree>
	</div>
	</form>

	<script>

	var ${popupid}_vue = makePopupVue("${popupid}", "${popupid}", {})

	function ${popupid}_popup_select(selectedArr) {
		if(selectedArr[0] == null){
			return
		}
		var callback = ${popupid}_vue.callback;
		 
		if(callback != null){
			 
			 
			eval( callback + '(selectedArr[0]);');
		}
		$("#${popupid}").modal('hide');
	}

	function ${popupid}_reloadTree(){
		$("#${popupid}_tree").trigger("reload");
		setTimeout(function(){
			$('#${popupid}_tree').jstree("deselect_all");
			$("#${popupid}_tree").jstree("search", "");
			$("#${popupid}_form #searchInput").val("");
		},1000);
	}

	function ${popupid}_click(thisvue, param){
		${popupid}_vue.callback = thisvue.callback;
		return true;
	}

	</script>


</fm-modal>
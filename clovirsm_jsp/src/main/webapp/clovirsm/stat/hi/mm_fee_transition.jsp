<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
 

<layout:extends name="base/index">
    <layout:put block="content">
    	<div class="stat_charts">
    		<script src="/res/js/stat_chart_config.js" ></script>
			<script src="/res/js/stat.js" ></script>
    		<div class="col col-sm-12">
    			<jsp:include page="./_mm_sel.jsp"></jsp:include>
    		 </div>
    		<div class="col col-sm-4">
       			<div id="fab_line" class="chart"  data-type="chart" data-setting="fab_line_config"></div>
         	</div>	
    		<div class="col col-sm-4">
       			<div id="kubun_line" class="chart"  data-type="chart" data-setting="kubun_line_config"></div>
         	</div>	
    		<div class="col col-sm-4">
       			<div id="item_line" class="chart"  data-type="chart" data-setting="item_line_config"></div>
         	</div>	
    		<div class="col col-sm-6">
       			<div id="category_line" class="chart"  data-type="chart" data-setting="category_line_config"></div>
         	</div>	
    		<div class="col col-sm-6">
       			<div id="team_line" class="chart"  data-type="chart" data-setting="team_line_config"></div>
         	</div>	
         	<div class="col col-sm-12">
       			<div id="grid" class="chart ag-theme-fresh" data-type="grid" data-setting="grid_config"></div>
         	</div>	
         	 
          
       	</div>
		 <script>
		 	var chart =  new Chart("stat_NC_MM_STAT_FAB",null,false);
		 	$(document).ready(function(){
		 		$("#mm6").click();
		 		
		 	})
		 	function grid_config(){
		 		var setting = {};
		 	 
		 		
		 		setting.rowData=chart.chart_data;
		 		var columnDefs = [
		 			{headerName : '월', field: "YYYYMM"  },
		 			{headerName : 'FAB', field: "FAB"},
		 			{headerName : '구분', field: "P_KUBUN"},
		 			{headerName : '업무', field: "CATEGORY_NM"},
		 			{headerName : '팀', field: "TEAM_NM"},
		 			{headerName : '요소', field: "ITEM"},
		 			{headerName : '비용', field: "FEE", aggr:"sum"},
		 			 
		 			
		 		];
		 		var data  = calData4Grid(chart.chart_data, columnDefs );
		 		data = array_sort(data, 'FEE',true);
		 		return get_grid_config(columnDefs, data, "월별 비용 목록");
		 		 
		 	}

		 	function fab_line_config(){
		 		
		 		var setting = countByGroup(chart.chart_data,  "FAB" ,"YYYYMM","FEE|sum");
		 		 
		 		setting.title = "FAB별 추이";
		 		setting.ytitle = '비용';
		 		setting.unit="원"
// 		 		extractTopMulti(setting, 10, 'Other')
		 		return get_line_config(setting);
		 	}
		 	
			function kubun_line_config(){
		 		var setting = countByGroup(chart.chart_data,  "P_KUBUN" ,"YYYYMM","FEE|sum");
		 		setting.title = "구분별 추이";
		 		setting.ytitle = '비용';
		 		setting.unit="원"
// 		 		extractTopMulti(setting, 10, 'Other')
		 		return get_line_config(setting);
		 	}
			
			function item_line_config(){
		 		var setting = countByGroup(chart.chart_data,  "ITEM" ,"YYYYMM","FEE|sum");
		 		setting.title = "요소별 추이";
		 		setting.ytitle = '비용';
		 		setting.unit="원"
// 		 		extractTopMulti(setting, 10, 'Other')
		 		return get_line_config(setting);
		 	}
			function category_line_config(){
		 		var setting = countByGroup(chart.chart_data,  "CATEGORY_NM" ,"YYYYMM","FEE|sum");
		 		setting.title = "업무별 추이";
		 		setting.ytitle = '비용';
		 		setting.unit="원"
		 		extractTopMulti(setting, 10, 'Other')
		 		return get_line_config(setting);
		 	}
			function team_line_config(){
		 		var setting = countByGroup(chart.chart_data,  "TEAM_NM" ,"YYYYMM","FEE|sum");
		 		setting.title = "팀별 추이";
		 		setting.ytitle = '비용';
		 		setting.unit="원"
		 		extractTopMulti(setting, 10, 'Other')
		 		return get_line_config(setting);
		 	}
		 	
			 
		 </script>
		
	</layout:put>
</layout:extends>		
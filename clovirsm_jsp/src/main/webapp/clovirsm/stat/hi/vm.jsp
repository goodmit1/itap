<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
 

<layout:extends name="base/index">
    <layout:put block="content">
    	<div class="stat_charts">
    		<script src="/res/js/stat_chart_config.js" ></script>
			<script src="/res/js/stat.js" ></script>
			 
    		<div class="col col-sm-4">
         		<div id="os" class="chart" data-type="chart" data-setting="os_cnt"></div>
         	</div>	
    		<div class="col col-sm-4">
         		<div id="run_cd" class="chart" data-type="chart" data-setting="vm_state_cnt"></div>
         	</div>	
         	<div class="col col-sm-4">
         		<div id="p_kubun" class="chart" data-type="chart" data-setting="p_kubun_setting"></div>
         	</div>	
         	
         	<div class="col col-sm-6">
         		<div id="dept_grid"  class="chart  " data-type="grid" data-setting="dept_grid_setting"></div>
         	</div>	
         	<div class="col col-sm-6">
         		<div id="dept_bar" class="chart" data-type="chart" data-setting="dept_bar_setting"></div>
         	</div>	
         	
         	 <div class="col col-sm-6">
         		<div id="category_grid"  class="chart  " data-type="grid" data-setting="category_grid_setting"></div>
         	</div>
         	<div class="col col-sm-6">
         		<div id="category_bar" class="chart" data-type="chart" data-setting="category_bar_setting"></div>
         	</div>	
         		
         	 <div class="col col-sm-6">
         		<div id="user_grid"  class="chart chart_area_style" data-type="grid" data-setting="user_grid_setting"></div>
         	</div>	
         	<div class="col col-sm-6">
         		<div id="user_bar" class="chart" data-type="chart" data-setting="user_bar_setting"></div>
         	</div>	
         </div>	
		 <script>
		 	var chart = new Chart("stat_NC_VM" )
		 	$(document).ready(function(){
		 		chart.getData();
		 		
		 	})
		 	function dept_bar_setting(){
		 		var setting = countByGroup(chart.chart_data, "TEAM_NM", null );
		 		setting.title = "팀별 VM 개수";
		 		setting.ytitle  = " VM 건수";
		 		extractTop(setting, 10, 'Other');
		 		setting.xtitle  = "팀";
		 		setting.objName = "chart"
				setting.name = "TEAM_NM";
		 		return get_bar_config(setting);
		 	}
		 	function teamFilter(obj){
		 		var val = obj.innerText
		 		chart.setFilter("TEAM_NM",val,"팀","chart");
		 	}
		 	function userFilter(obj){
		 		var val = obj.innerText
		 		chart.setFilter("USER_NAME",val,"사용자","chart");
		 	}
		 	function categoryFilter(obj){
		 		var val = obj.innerText
		 		chart.setFilter("CATEGORY1",val,"업무","chart");
		 	}
		 	function dept_grid_setting(){
		 		 
		 		var columnDefs = [
		 			{headerName : '팀', field: "TEAM_NM", cellRenderer:function(params) {
		 				if(params.value=='Total') return params.value
		 				return '<a href="#" onclick="teamFilter(this);return false">' + params.value + '</a>'
		 				}
		 			},
		 			{headerName : '개수', field: "count"},
		 			{headerName : 'CPU', field: "CPU_CNT", aggr:"sum"},
		 			{headerName : '메모리(GB)', field: "RAM_SIZE", aggr:"sum"},
		 			{headerName : '디스크(GB)', field: "DISK_SIZE", aggr:"sum"},
		 			//{headerName : 'GPU(Q)', field: "GPU_SIZE", aggr:"sum" },
		 			
		 		];
		 		var data  = calData4Grid(chart.chart_data, columnDefs );
		 		data = array_sort(data, 'count',true);
		 		return get_grid_config(columnDefs, data, "팀별 VM 개수");
		 		
		 	 
		 	}
		 	function user_bar_setting(){
		 		var setting = countByGroup(chart.chart_data, "USER_NAME", null );
		 		setting.title = "사용자별 VM 개수";
		 		setting.ytitle  = " VM 건수";
		 		extractTop(setting, 10, 'Other');
		 		setting.xtitle  = "사용자";
		 		setting.objName = "chart"
				setting.name = "USER_NAME";
		 		return get_bar_config(setting);
		 	}
		 	function user_grid_setting(){
		 		 
		 		var columnDefs = [
		 			{headerName : '사용자', field: "USER_NAME", cellRenderer:function(params) {
		 				if(params.value=='Total') return params.value
		 				return '<a href="#" onclick="userFilter(this);return false">' + params.value + '</a>'
	 				}
	 			 	},
		 			{headerName : '개수', field: "count"},
		 			{headerName : 'CPU', field: "CPU_CNT", aggr:"sum"},
		 			{headerName : '메모리(GB)', field: "RAM_SIZE", aggr:"sum"},
		 			{headerName : '디스크(GB)', field: "DISK_SIZE", aggr:"sum"},
		 			//{headerName : 'GPU(Q)', field: "GPU_SIZE", aggr:"sum" },
		 			
		 		];
		 		var data  = calData4Grid(chart.chart_data, columnDefs );
		 		data = array_sort(data, 'count',true);
		 		return get_grid_config(columnDefs, data, "사용자별 VM 개수");
		 		
		 	 
		 	}
		 	function category_grid_setting(){
		 		 
		 		var columnDefs = [
		 			{headerName : '업무', field: "CATEGORY1", cellRenderer:function(params) {
		 				if(params.value=='Total') return params.value
		 				return '<a href="#" onclick="categoryFilter(this);return false">' + params.value + '</a>'
	 				}},
		 			{headerName : '개수', field: "count"},
		 			{headerName : 'CPU', field: "CPU_CNT", aggr:"sum"},
		 			{headerName : '메모리(GB)', field: "RAM_SIZE", aggr:"sum"},
		 			{headerName : '디스크(GB)', field: "DISK_SIZE", aggr:"sum" },
		 			//{headerName : 'GPU(Q)', field: "GPU_SIZE", aggr:"sum" },
		 			
		 		];
		 		var data  = calData4Grid(chart.chart_data, columnDefs );
		 		data = array_sort(data, 'count',true);
		 		return get_grid_config(columnDefs, data, "업무별 VM 개수");
		 		
		 	 
		 	}
		 	 
			function category_bar_setting(){
		 		var setting = countByGroup(chart.chart_data, "CATEGORY1", null );
		 		setting.title = "업무별 VM 개수";
		 		setting.ytitle  = " VM 건수";
		 		setting.xtitle  = " 업무";
		 		setting.objName = "chart"
				setting.name = "CATEGORY1";
		 		extractTop(setting, 10, 'Other');
		 		return get_bar_config(setting);
		 	}
		 	function p_kubun_setting(){
		 		var setting = countByGroup(chart.chart_data, "P_KUBUN_NM", null );
		 		setting.title = "운영/QA/개발 VM 개수";
		 		setting.objName = "chart"
				setting.name = "P_KUBUN_NM";
				setting.xtitle = "구분";
		 		return get_pie_config(setting);
		 	}
		 	function os_cnt(){
		 		var setting = countByGroup(chart.chart_data, "OS", null );
		 		setting.title = "OS별 VM 개수";
		 		setting.objName = "chart"
			 	setting.name = "OS";
			 	setting.xtitle = "OS";
		 		return get_pie_config(setting);
		 	}
		 	function fab_cnt(){
		 		var setting = countByGroup(chart.chart_data, "FAB_NM", null );
		 		setting.title = "FAB별 VM 개수";
		 		setting.objName = "chart"
			 	setting.name = "FAB_NM";
			 	setting.xtitle = "FAB";
		 		return get_pie_config(setting);
		 	}
		 	function vm_state_cnt(){
		 		var setting = countByGroup(chart.chart_data, "RUN_CD_NM", null );
		 		setting.title = "서버상태별 VM갯수";
		 		setting.objName = "chart"
		 		setting.name = "RUN_CD_NM";
		 		setting.xtitle = "서버상태";
		 		return get_pie_config(setting);
		 	}
		 </script>
		
	</layout:put>
</layout:extends>		
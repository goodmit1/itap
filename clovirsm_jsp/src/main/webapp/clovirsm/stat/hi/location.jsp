<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
 

<layout:extends name="base/index">
    <layout:put block="content">
    	<div class="stat_charts">
    		<script src="/res/js/stat_chart_config.js" ></script>
			<script src="/res/js/stat.js" ></script>
    		<div class="col col-sm-4">
       			<div id="location_dc_pie" class="chart"  data-type="chart" data-setting="location_dc_pie_config"></div>
         	</div>	 
         	<div class="col col-sm-4">
       			<div id="location_host_pie" class="chart"  data-type="chart" data-setting="location_host_pie_config"></div>
         	</div>	
         	<div class="col col-sm-4">
       			<div id="location_vm_pie" class="chart"  data-type="chart" data-setting="location_vm_pie_config"></div>
         	</div> 
         	<div class="col col-sm-12">
       			<div id="grid" class="chart ag-theme-fresh" data-type="grid" data-setting="grid_config"></div>
         	</div>	
         	 
          
       	</div>
		 <script>
		 	var chart =  new Chart("stat_LOCATION",null,false);
		 	$(document).ready(function(){
		 		goStat1(this,"m");
		 	})
		 	function grid_config(){
		 		var setting = {};
		 	 
		 		
		 		setting.rowData=chart.chart_data;
		 		var columnDefs = [
		 			{headerName : '<spring:message code="location" text="지역" />', field: "LOCATION"},
		 			{headerName : 'vCenter', field: "DC_NM" },
		 			{headerName : 'Host <spring:message code="quantity" text="수량" />', field: "HOST_CNT"  , aggr:"sum"},
		 			{headerName : 'Host CPU <spring:message code="quantity" text="수량" />(vcroe)', field: "TOTAL_CPU"  , aggr:"sum"},
		 			{headerName : 'VM <spring:message code="quantity" text="수량" />', field: "VM_CNT"  , aggr:"sum"},
		 			{headerName : '<spring:message code="label_mem" text="메모리" />(TB)', field: "TOTAL_MEM"  , aggr:"sum"},
		 			{headerName : '<spring:message code="tab_disk" text="디스크" />(TB)', field: "TOTAL_DS" , aggr:"sum"},
		 			 
		 			
		 		];
		 		var data  = calData4Grid(chart.chart_data, columnDefs );
		 		data = array_sort(data, 'FEE',true);
		 		return get_grid_config(columnDefs, data, "지역별 vCenter 목록");
		 		 
		 	}

		 	function location_dc_pie_config(){
		 		var setting = countByGroup(chart.chart_data,"LOCATION",null,null);
		 		setting.title = "지역별 vCenter현황";
		 		setting.ytitle = 'EA';
		 		setting.xtitle  = "지역";
		 		setting.objName = "chart"
				setting.name = "location";
		 		setting.unit="개"
		 		setting.percent = true
		 		return get_pie_config(setting);
		 	}
		 	function location_host_pie_config(){
		 		var setting = countByGroup(chart.chart_data,   "LOCATION",null, "HOST_CNT|sum");
		 		setting.title = "지역별 HOST 수량 현황";
		 		setting.ytitle = 'EA';
		 		setting.xtitle  = "지역";
		 		setting.objName = "chart"
				setting.name = "HOST_CNT";
		 		setting.unit="개"
		 		setting.percent = true
		 		return get_pie_config(setting);
		 	}
		 	function location_vm_pie_config(){
		 		var setting = countByGroup(chart.chart_data,   "LOCATION",null, "VM_CNT|sum");
		 		setting.title = "지역별 VM 수량 현황";
		 		setting.ytitle = '비용';
		 		setting.xtitle  = "구분";
		 		setting.objName = "chart"
				setting.name = "VM_CNT";
		 		setting.unit="개"
		 		setting.percent = true
		 		return get_pie_config(setting);
		 	}
		 	function team_line_config(){
		 		
		 		var setting = countByGroup(chart.chart_data,  "TEAM_NM" ,"YYYYMM","FEE|sum");
		 		 
		 		setting.title = "팀별 비용";
		 		setting.ytitle = '비용';
		 		extractTopMulti(setting, 10, 'Other')
		 		return get_line_config(setting);
		 	}
			 
			function category_line_config(){
		 		
		 		var setting = countByGroup(chart.chart_data, "CATEGORY_NM" ,"YYYYMM", "FEE|sum" );
		 		setting.title = "업무별 비용";
		 		setting.ytitle = '비용';
		 		extractTopMulti(setting, 10, 'Other')
		 		return get_line_config(setting);
		 	}
			function kubun_line_config(){
		 		
		 		var setting = countByGroup(chart.chart_data,   "P_KUBUN_NM"  ,"YYYYMM",  "FEE|sum");
		 		setting.title = "구분별 비용";
		 		setting.ytitle = '비용';
		 		setting.stacking = true;
		 		return get_column_config(setting);
		 	}
			
		  	function goStat1(that, term){
		  		var kubun = term.substring(term.length-1);
		  		var d = 0;
		  		if(term.length>1){
		  			d = 1 * term.substring(0,term.length-1)
		  		}
		  		var date = new Date();
		  		if(kubun=='m'){
		  			date.setDate(1);
		  			date.setMonth(date.getMonth() + d );
		  		}
		  		 
		  		
		  		goStat(that , formatDatePattern(date, 'yyyyMM'), formatDatePattern(new Date(),'yyyyMM'))
		  	}
		  	function goStat(that, from, to, isFromDate){
		  		chart.getData(from, to)
		  	}
		 </script>
		
	</layout:put>
</layout:extends>		
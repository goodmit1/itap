<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
 

<layout:extends name="base/index">
    <layout:put block="content">
    	<div class="stat_charts">
    		<script src="/res/js/stat_chart_config.js" ></script>
			<script src="/res/js/stat.js" ></script>
    		<div class="col col-sm-12">
    			<jsp:include page="./_date_sel.jsp"></jsp:include>
    		 </div>
    		<div class="col col-sm-6">
       			<div id="category" class="chart"  data-type="chart" data-setting="category_cnt"></div>
         	</div>	
         	<div class="col col-sm-6">
       			<div id="appr_status" class="chart ag-theme-fresh" data-type="grid" data-setting="appr_status_cnt_pivot"></div>
         	</div>	
         	<div class="col col-sm-4">
       			<div id="category_cnt_pie_chart" class="chart" data-type="chart" data-setting="category_cnt_pie_chart"></div>
         	</div>	
         	<div class="col col-sm-4">
       			<div id="team_cnt_pie_chart" class="chart" data-type="chart" data-setting="team_cnt_pie_chart"></div>
         	</div>	
         	<div class="col col-sm-4">
       			<div id="user_cnt_pie_chart" class="chart" data-type="chart" data-setting="user_cnt_pie_chart"></div>
         	</div>	
          
       	</div>
		 <script>
		 	var chart =  new Chart("stat_NC_REQ",null,false);
		 	$(document).ready(function(){
		 		var date = new Date();
				date.setDate(date.getDate() - 7);
		 		
		 		chart.getData(formatDate(date, 'date'),formatDate( new Date(), 'date'));
		 		
		 	})
		 	function allgridSetting(){
		 		var setting = {};
		 		setting.title = "[결재] 목록";
		 		
		 		setting.rowData=chart.chart_data;
		 		var columnDefs = [
		 			{headerName : '요청일', field: "INS_TMS" , format:'datetime'},
		 			{headerName : '구분', field: "SVCCUD_CD_NM"},
		 			{headerName : '결재상태', field: "APPR_STATUS_CD_NM"},
		 			{headerName : '업무', field: "CATEGORY_NM"},
		 			{headerName : '팀', field: "TEAM_NM"},
		 			{headerName : '사용자', field: "USER_NAME"},
		 			{headerName : 'VM 개수', field: "VM_CNT"},
		 			
		 		];
		 		setting.columnDefs = columnDefs
				 
		 		setting.enableSorting =true
		 		setting.enableColResize = true
		 		return setting;
		 	}

		 	function appr_status_cnt_pivot(){
		 		var setting = countByGroup(chart.chart_data, "CATEGORY_NM", "APPR_STATUS_CD_NM" );
		 		 
		 		var columnDefs = [
		 			{headerName : '업무', field: "CATEGORY_NM"},
		 			{headerName : 'VM 개수', field: "VM_CNT", aggr:"sum"},
		 			
		 		];
		 		grid_merge(setting.data, calData4Grid(chart.chart_data, columnDefs), "CATEGORY_NM","name");
		 		columnDefs = [
		 			{headerName : '업무', field: "name"},
		 			{headerName : '승인', field: "승인"},
		 			{headerName : '반려', field: "반려"},
		 			{headerName : '대기', field: "대기"},
		 			{headerName : '배포VM개수', field: "VM_CNT"},
		 			
		 		];
		 		return get_grid_config(columnDefs, setting.data, '[결재]요청갯수' )
		 		 
		 	}
		 	function category_cnt(){
		 		
		 		var setting = countByGroup(chart.chart_data, "SVCCUD_CD_NM", "APPR_STATUS_CD_NM" );
		 		setting.title = "[결재]요청갯수";
		 		setting.ytitle = '건수';
		 		setting.stacking = true;
		 		return get_column_config(setting);
		 	}
		 	function category_cnt_pie_chart(){
		 		var setting = countByGroup(chart.chart_data, "CATEGORY_NM", null );
		 		setting.title = "업무요청건수";
		 		return get_pie_config(setting);
		 	}
		 	function team_cnt_pie_chart(){
		 		var setting = countByGroup(chart.chart_data, "TEAM_NM", null );
		 		setting.title = "팀별요청건수";
		 		return get_pie_config(setting);
		 	}
		 	function user_cnt_pie_chart(){
		 		var setting = countByGroup(chart.chart_data, "USER_NAME", null );
		 		setting.title = "유저별요청건수";
		 		return get_pie_config(setting);
		 	}
		 </script>
		
	</layout:put>
</layout:extends>		
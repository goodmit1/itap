<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
 

<layout:extends name="base/index">
    <layout:put block="content">
    	<div class="stat_charts">
    		<script src="/res/js/stat_chart_config.js" ></script>
			<script src="/res/js/stat.js" ></script>
         	<div class="col col-sm-4">
       			<div id="license_host_pie" class="chart"  data-type="chart" data-setting="license_host_pie_config"></div>
         	</div>	
    		<div class="col col-sm-4">
       			<div id="license_cpu_pie" class="chart"  data-type="chart" data-setting="license_cpu_pie_config"></div>
         	</div>	 
         	<div class="col col-sm-4">
       			<div id="license_vsan_pie" class="chart"  data-type="chart" data-setting="license_vsan_pie_config"></div>
         	</div> 
   	    	<div class="col col-sm-2">
         		<div id="dc_metric" class="chart" data-type="metric" data-setting="dc_metric" style="height:175px;"></div>
         	</div>	
   	    	<div class="col col-sm-2">
         		<div id="vra_metric" class="chart" data-type="metric" data-setting="vra_metric" style="height:175px;"></div>
         	</div>	
   	    	<div class="col col-sm-2">
         		<div id="vrops_metric" class="chart" data-type="metric" data-setting="vrops_metric" style="height:175px;"></div>
         	</div>	
   	    	<div class="col col-sm-2">
         		<div id="vrli_metric" class="chart" data-type="metric" data-setting="vrli_metric" style="height:175px;"></div>
         	</div>	
         	<div class="col col-sm-2">
         		<div id="nsx_metric" class="chart" data-type="metric" data-setting="nsx_metric" style="height:175px;"></div>
         	</div>	
   	    	<div class="col col-sm-1">
         		<div id="vrni_metric" class="chart" data-type="metric" data-setting="vrni_metric" style="height:175px;"></div>
         	</div>	
   	    	<div class="col col-sm-1">
         		<div id="sddc_metric" class="chart" data-type="metric" data-setting="sddc_metric" style="height:175px;"></div>
         	</div>	
         	<div class="col col-sm-12">
       			<div id="grid" class="chart ag-theme-fresh" data-type="grid" data-setting="grid_config"></div>
         	</div>	
         	 
          
       	</div>
		 <script>
		 	var chart2 =  new Chart("stat_LICENSE_VRA_CNT",null,false);
		 	var chart =  new Chart("stat_LICENSE_VRA",null,false);
		 	$(document).ready(function(){
		 		goStat1(this,"m");
		 	})
		 	function grid_config(){
		 		var setting = {};
		 	 
		 		
		 		setting.rowData=chart.chart_data;
		 		var columnDefs = [
		 			{headerName : 'vCenter명', field: "DC_NM" },
		 			{headerName : 'Host 수량', field: "HOST_CNT"  , aggr:"sum"},
		 			{headerName : 'Vsphere', field: "HOST_CPU"  , aggr:"sum"},
		 			{headerName : 'VSAN', field: "VSAN"  , aggr:"sum"},
		 			{headerName : 'NSX', field: "NSX"  , aggr:"sum"},
		 			{headerName : 'VRA/VRO', field: "VRA"  , aggr:"sum"},
		 			{headerName : 'VROps', field: "VROPS"  , aggr:"sum"},
		 			{headerName : 'LogInsight', field: "VRLI"  , aggr:"sum"},
		 			{headerName : 'NetworkInsight', field: "VRNI"  , aggr:"sum"},
		 			{headerName : 'SDDC', field: "SDDC"  , aggr:"sum"},
		 			
		 		];
		 		var data  = calData4Grid(chart.chart_data, columnDefs );
		 		return get_grid_config(columnDefs, data, "vCenter 라이센스 목록");
		 		 
		 	}

		 	function license_cpu_pie_config(){
		 		var setting = countByGroup(chart.chart_data,"DC_NM",null,"HOST_CPU|sum");
		 		setting.title = "VSphere 수량 현황";
		 		setting.ytitle = 'EA';
		 		setting.xtitle  = "vcenter";
		 		setting.objName = "vCenter"
				setting.name = "HOST_CPU";
		 		setting.unit="개"
		 		setting.percent = true
		 		return get_pie_config(setting);
		 	}
		 	function license_host_pie_config(){
		 		var setting = countByGroup(chart.chart_data, "DC_NM",null, "HOST_CNT|sum");
		 		setting.title = "HOST 수량 현황";
		 		setting.ytitle = 'EA';
		 		setting.xtitle  = "vCenter";
		 		setting.objName = "chart"
				setting.name = "HOST_CNT";
		 		setting.unit="개"
		 		setting.percent = true
		 		return get_pie_config(setting);
		 	}
		 	function license_vsan_pie_config(){
		 		var setting = countByGroup(chart.chart_data,"DC_NM",null, "VSAN|sum");
		 		setting.title = "VSAN 수량 현황";
		 		setting.ytitle = 'EA';
		 		setting.xtitle  = "";
		 		setting.objName = "chart"
				setting.name = "VSAN";
		 		setting.unit="개"
		 		setting.percent = true
		 		return get_pie_config(setting);
		 	}
		 	function metric_setting(kubun, kubunStr){
		 		var setting = {};
		 		var data = summary(chart.chart_data, kubun);
		 		setting.title = kubunStr;
		 		setting.val = data;
		 		
		 		return setting;
		 	}
		 	function metric_setting2(kubun, kubunStr){
		 		var setting = {};
		 		var data = summary(chart2.chart_data, kubun);
		 		setting.title = kubunStr;
		 		setting.val = data;
		 		
		 		return setting;
		 	}
		 	
		 	function dc_metric(){ 		
		 		return metric_setting2('VCENTER','Vcenter 수량');
		 	}
		 	function vrli_metric(){
		 		return metric_setting('VRLI','Loginsight 수량');
		 	}
		 	function vrni_metric(){
		 		return metric_setting('VRNI','NetworkInsight 수량');
		 	}
		 	function vrops_metric(){
		 		return metric_setting('VROPS','VROps 수량');
		 	}
		 	function sddc_metric(){
		 		return metric_setting('SDDC','SDDC 수량');
		 	}
		 	function nsx_metric(){
		 		return metric_setting2('NSX','NSX 수량');
		 	}
		 	function vra_metric(){
		 		return metric_setting2('VRA','VRA 수량');
		 	}
		 	
		  	function goStat1(that, term){
		  		var kubun = term.substring(term.length-1);
		  		var d = 0;
		  		if(term.length>1){
		  			d = 1 * term.substring(0,term.length-1)
		  		}
		  		var date = new Date();
		  		if(kubun=='m'){
		  			date.setDate(1);
		  			date.setMonth(date.getMonth() + d );
		  		}
		  		 
		  		
		  		goStat(that , formatDatePattern(date, 'yyyyMM'), formatDatePattern(new Date(),'yyyyMM'))
		  	}
		  	function goStat(that, from, to, isFromDate){
		  		chart.getData(from, to)
		  		chart2.getData(from, to)
		  	}
		 </script>
		
	</layout:put>
</layout:extends>		
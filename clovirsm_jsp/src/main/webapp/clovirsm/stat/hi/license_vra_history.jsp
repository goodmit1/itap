<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
 

<layout:extends name="base/index">
    <layout:put block="content">
    	<div class="stat_charts">
    		<script src="/res/js/stat_chart_config.js" ></script>
			<script src="/res/js/stat.js" ></script>
			<div class="col col-sm-12">
    			<jsp:include page="./_mm_sel.jsp"></jsp:include>
    		 </div>
         	<div class="col col-sm-4">
       			<div id="license_host" class="chart"  data-type="chart" data-setting="license_host_line_config"></div>
         	</div>	
    		<div class="col col-sm-4">
       			<div id="license_cpu" class="chart"  data-type="chart" data-setting="license_cpu_line_config"></div>
         	</div>	 
         	<div class="col col-sm-4">
       			<div id="license_vsan" class="chart"  data-type="chart" data-setting="license_vsan_line_config"></div>
         	</div> 
   	    	 
         	<div class="col col-sm-12">
       			<div id="grid" class="chart ag-theme-fresh" data-type="grid" data-setting="grid_config"></div>
         	</div>	
         	 
          
       	</div>
		 <script>
		 	var chart2 =  new Chart("stat_LICENSE_VRA_CNT",null,false);
		 	var chart =  new Chart("stat_LICENSE_VRA",null,false);
		 	$(document).ready(function(){
		 		$("#mm6").click();
		 	})
		 	function grid_config(){
		 		var setting = {};
		 	 
		 		
		 		setting.rowData=chart.chart_data;
		 		var columnDefs = [
		 			{headerName : '월', field: "YYYYMM" },
		 			{headerName : 'vCenter명', field: "DC_NM" },
		 			{headerName : 'Host 수량', field: "HOST_CNT"  , aggr:"sum"},
		 			{headerName : 'Vsphere', field: "HOST_CPU"  , aggr:"sum"},
		 			{headerName : 'VSAN', field: "VSAN"  , aggr:"sum"},
		 			{headerName : 'NSX', field: "NSX"  , aggr:"sum"},
		 			{headerName : 'VRA/VRO', field: "VRA"  , aggr:"sum"},
		 			{headerName : 'VROps', field: "VROPS"  , aggr:"sum"},
		 			{headerName : 'LogInsight', field: "VRLI"  , aggr:"sum"},
		 			{headerName : 'NetworkInsight', field: "VRNI"  , aggr:"sum"},
		 			{headerName : 'SDDC', field: "SDDC"  , aggr:"sum"},
		 			
		 		];
		 		var data  = calData4Grid(chart.chart_data, columnDefs );
		 		return get_grid_config(columnDefs, data, "vCenter 라이센스 목록");
		 		 
		 	}

			function license_host_line_config(){
		 		
		 		var setting = countByGroup(chart.chart_data,  "DC_NM" ,"YYYYMM","HOST_CNT|sum");
		 		 
		 		setting.title = "HOST 추이";
		 		setting.ytitle = 'HOST 갯수';
 
// 		 		extractTopMulti(setting, 10, 'Other')
		 		return get_line_config(setting);
		 	}
			function license_cpu_line_config(){
		 		
		 		var setting = countByGroup(chart.chart_data,  "DC_NM" ,"YYYYMM","HOST_CNT|sum");
		 		 
		 		setting.title = "HOST CPU 추이";
		 		setting.ytitle = 'HOST CPU 갯수';
 
// 		 		extractTopMulti(setting, 10, 'Other')
		 		return get_line_config(setting);
		 	}
			function license_vsan_line_config(){
		 		
		 		var setting = countByGroup(chart.chart_data,  "DC_NM" ,"YYYYMM","VSAN|sum");
		 		 
		 		setting.title = "VSAN 추이";
		 		setting.ytitle = 'VSAN 갯수';
 
// 		 		extractTopMulti(setting, 10, 'Other')
		 		return get_line_config(setting);
		 	}
		 </script>
		
	</layout:put>
</layout:extends>		
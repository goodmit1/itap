<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
 

<layout:extends name="base/index">
    <layout:put block="content">
    	<div class="stat_charts">
    		<script src="/res/js/stat_chart_config.js" ></script>
			<script src="/res/js/stat.js" ></script>
    		<div class="col col-sm-4">
       			<div id="fab_pie" class="chart"  data-type="chart" data-setting="fab_pie_config"></div>
         	</div>	 
         	<div class="col col-sm-4">
       			<div id="kubun_pie" class="chart"  data-type="chart" data-setting="kubun_pie_config"></div>
         	</div>	
         	<div class="col col-sm-4">
       			<div id="item_pie" class="chart"  data-type="chart" data-setting="item_pie_config"></div>
         	</div> 
         	<div class="col col-sm-6">
       			<div id="category_line" class="chart"  data-type="chart" data-setting="category_bar_config"></div>
         	</div>	
    		<div class="col col-sm-6">
       			<div id="team_line" class="chart"  data-type="chart" data-setting="team_bar_config"></div>
         	</div>	
         	<div class="col col-sm-12">
       			<div id="grid" class="chart ag-theme-fresh" data-type="grid" data-setting="grid_config"></div>
         	</div>	
         	 
          
       	</div>
		 <script>
		 	var chart =  new Chart("stat_NC_MM_STAT_FAB",null,false);
		 	$(document).ready(function(){
		 		goStat1(this,"m");
		 	})
		 	function grid_config(){
		 		var setting = {};
		 	 
		 		
		 		setting.rowData=chart.chart_data;
		 		var columnDefs = [
		 			{headerName : 'FAB', field: "FAB"},
		 			{headerName : '구분', field: "P_KUBUN"},
		 			{headerName : '업무', field: "CATEGORY_NM"},
		 			{headerName : '팀', field: "TEAM_NM"},
		 			{headerName : '요소', field: "ITEM"},
		 			{headerName : '비용', field: "FEE", aggr:"sum"},
		 			 
		 			
		 		];
		 		var data  = calData4Grid(chart.chart_data, columnDefs );
		 		data = array_sort(data, 'FEE',true);
		 		return get_grid_config(columnDefs, data, "월별 비용 목록");
		 		 
		 	}

		 	function fab_pie_config(){
		 		var setting = countByGroup(chart.chart_data,"FAB",null, "FEE|sum");
		 		setting.title = "FAB별 현황";
		 		setting.ytitle = '비용';
		 		setting.xtitle  = "FAB";
		 		setting.objName = "chart"
				setting.name = "FAB";
		 		setting.unit="천원"
		 		setting.percent = true;
		 		return get_pie_config(setting);
		 	}
		 	function item_pie_config(){
		 		var setting = countByGroup(chart.chart_data,   "ITEM",null, "FEE|sum");
		 		setting.title = "요소별 비용";
		 		setting.ytitle = '비용';
		 		setting.xtitle  = "요소";
		 		setting.objName = "chart"
				setting.name = "ITEM";
		 		setting.unit="천원"
	 			setting.percent = true;
		 		return get_pie_config(setting);
		 	}
		 	function kubun_pie_config(){
		 		var setting = countByGroup(chart.chart_data,   "P_KUBUN",null, "FEE|sum");
		 		setting.title = "팀별 현황";
		 		setting.ytitle = '비용';
		 		setting.xtitle  = "구분";
		 		setting.objName = "chart"
				setting.name = "P_KUBUN";
		 		setting.unit="천원"
	 			setting.percent = true;
		 		return get_pie_config(setting);
		 	}
		 	function team_bar_config(){
		 		
		 		var setting = countByGroup(chart.chart_data,  "TEAM_NM" , null,"FEE|sum");
		 		setting.objName = "chart"
		 		setting.title = "팀별 비용";
		 		setting.name = "TEAM_NM"
		 		setting.xtitle  = "팀";
		 		setting.percent=true;
		 		setting.unit="천원"
		 		setting.ytitle = '비용';
		 		extractTop(setting, 10, 'Other')
		 		return get_bar_config(setting);
		 	}
			 
			function category_bar_config(){
		 		
		 		var setting = countByGroup(chart.chart_data, "CATEGORY_NM" ,null, "FEE|sum" );
		 		setting.title = "업무별 비용";
		 		setting.ytitle = '비용';
		 		setting.objName = "chart"
		 		setting.percent=true;
		 		setting.name = "CATEGORY_NM"
		 		setting.xtitle  = "업무";
		 		setting.unit="천원"
		 		extractTop(setting, 10, 'Other')
		 		return get_bar_config(setting);
		 	}
		  	function goStat1(that, term){
		  		var kubun = term.substring(term.length-1);
		  		var d = 0;
		  		if(term.length>1){
		  			d = 1 * term.substring(0,term.length-1)
		  		}
		  		var date = new Date();
		  		if(kubun=='m'){
		  			date.setDate(1);
		  			date.setMonth(date.getMonth() + d );
		  		}
		  		 
		  		
		  		goStat(that , formatDatePattern(date, 'yyyyMM'), formatDatePattern(new Date(),'yyyyMM'))
		  	}
		  	function goStat(that, from, to, isFromDate){
		  		chart.getData(from, to)
		  	}
		 </script>
		
	</layout:put>
</layout:extends>		
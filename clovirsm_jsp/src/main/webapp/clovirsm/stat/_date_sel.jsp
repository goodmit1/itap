<%@page contentType="text/html; charset=UTF-8" %>    
    <div style="float:right">
      <table style="float:right; position: relative; bottom: 8px;"><tr><td>
	     <div class="input-group" style="width: 80px;"><input type="text" name="FROM_DT" id="FROM_DT" class="form-control  dateInput"> <span class="input-group-addon calendar-btn"><i class="icon-append fa fa-calendar"></i></span></div>
	     </td><td>~</td>
	     <td>
			<div class="input-group" style="width: 80px;"><input type="text" name="TO_DT" id="TO_DT" class="form-control  dateInput"> <span class="input-group-addon calendar-btn"><i class="icon-append fa fa-calendar"></i></span></div>
		</td>
		<td><input type="button" value="조회" class="contentsBtn tabBtnImg sync" id="goStatFrom" style="margin-left: 30px; top: -2px;" onclick="goStatFromTo(this);">
		</td></tr>
	  </table>
	  <div id="timeStat" style="    display: inline-block;">
	      <input type="button" class="contentsBtn tabBtnImg sync" value="오늘" onclick='goStat(this,"now/d","now/d");'>  
	      <input type="button" class="contentsBtn tabBtnImg sync" value="어제 ~" onclick='goStat(this,"now-1d/d","now");'>
	      <input type="button" class="contentsBtn tabBtnImg save" value="7일전 ~" onclick='goStat(this,"now-7d/d","now");'>  
	      <input type="button" class="contentsBtn tabBtnImg sync" value="이번주" onclick='goStat(this,"now/w","now/w");'>  
	      <input type="button" class="contentsBtn tabBtnImg sync" value="이번달" onclick='goStat(this,"now/M","now");'>  
      </div>
	  <script>
	  	var url = "${url}";
	  	function goStatFromTo(that){
	  		$("#goStatFrom").removeClass("sync");
	  		$("#goStatFrom").addClass("save");
	  		$("#timeStat input[type='button']").removeClass("save");
	  		$("#timeStat input[type='button']").addClass("sync");
	  		if($("#FROM_DT").val()==''){
	  			alert(msg_empty_value);
	  			$("#FROM_DT").focus();
	  			return;
	  		}
	  		
	  		var toDate ; 
	  		if($("#TO_DT").val()==''){
	  			toDate = "now";	
	  		}
	  		else{
	  			toDate = "'" +  $("#TO_DT").val() + "T23:59:59.000Z'";
	  		}
	  		goStat(that,"'" + $("#FROM_DT").val() + "T00:00:00.000Z'", toDate, true);
	  		 
	  	}
	  	function clearDateVal(){
	  		$("#FROM_DT").val("");
	  		$("#TO_DT").val("");
	  	}
	  	function goStat(that, from, to, isFromDate){
	  		var pos = url.lastIndexOf("_g");
	  		var url1 = url.substring(0, pos ) + "_g=("+ "time:(from:" + encodeURIComponent(from) + ",to:" + encodeURIComponent(to)  + "))";
	  		$("#goStatFrom").removeClass("save");
	  		$("#goStatFrom").addClass("sync");
	  		$("#timeStat input[type='button']").removeClass("save");
	  		$("#timeStat input[type='button']").addClass("sync");
	  		$(that).removeClass("sync");
	  		$(that).addClass("save");
	  		$("#statWin").attr("src", url1);
	  		if(!isFromDate){
	  			clearDateVal();
	  		}
	  	}
	  </script>
     </div> 
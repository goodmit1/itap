<%@page import="com.clovirsm.hv.IBeforeAfter"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="com.clovirsm.sys.hv.ICreateInfo4Org"%>
<%@page import="com.clovirsm.sys.hv.DC"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.clovirsm.sys.hv.DCService"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%
DCService service = (DCService)SpringBeanUtil.getBean("DCService");
Map param = new HashMap();
String dcId = "528716005374355"; // request.getParameter("DC_ID");
String compId = "102216547406718";
String teamCd = "8";
param.put("DC_ID", dcId);
param.put("COMP_ID", compId);
param.put("TEAM_CD", teamCd);
DC dc = service.getDC(dcId);
param.putAll(dc.getProp());
IBeforeAfter before  = dc.getBefore( service );
ICreateInfo4Org createInfo = (ICreateInfo4Org)SpringBeanUtil.getBean("createInfo4Org");
 
	boolean isNew = createInfo.getOrgDCInfo(service, dcId, param);
	if(isNew)
	{	
		before.onBeforeCreateVM(true,param);
		dc.getAPI().onFirstVM(param);
	}
 
%>    

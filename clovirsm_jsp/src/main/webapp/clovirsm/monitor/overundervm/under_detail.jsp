<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
 	<div class="table_title layout name">
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="underTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<fm-popup-button popupid="vm_update_popup" popup="/clovirsm/popup/vm_detail_form_popup.jsp?action=update&callback=onAfterModify" cmd="update" class="ontentsBtn tabBtnImg save" param="ureq.getData()"><spring:message code="btn_spec_modify" text="사양 변경"/></fm-popup-button>
		</div>
	</div> 
	<div class="layout background mid">
		<div id="underTable" class="ag-theme-fresh" style="height: 250px" ></div>
	</div>
	<script>
		var underTable;
		var ureq = new Req();

		$(function() {


			var columnDefs = [
				{headerName : "<spring:message code="OVERUNDER_VM_NM" text="" />",field : "VM_NM", minWidth:120, cellStyle:{'text-align':'center'}},
				{headerName : "<spring:message code='TASK' text='업무'/>",field : "CATEGORY_NM"},
				{headerName : "<spring:message code="OVERUNDER_VM_CPU" text="" />",field : "CPU_CNT", minWidth:120, cellStyle:{'text-align':'center'}},
				{headerName : "<spring:message code="OVERUNDER_VM_REC_OVER_CPU" text="권장 CPU 증감" />",field : "UNDER_CPU",minWidth:120,tooltipField:'VM_NM', cellStyle:{'text-align':'center'}},
				{headerName : "<spring:message code="OVERUNDER_VM_MEM" text="" />",field : "RAM_SIZE",minWidth:120, cellStyle:{'text-align':'center'}},
				{headerName : "<spring:message code="OVERUNDER_VM_REC_OVER_MEM" text="권장 메모리 증감" />", minWidth:120, field : "UNDER_MEM", cellStyle:{'text-align':'center'}},
				{headerName : "<spring:message code="FM_TEAM_TEAM_CD" text="팀" />",field : "TEAM_NM", cellStyle:{'text-align':'center'}, minWidth:160},
				{headerName : "<spring:message code="INS_ID" text="담당자" />",field : "USER_NAME", cellStyle:{'text-align':'center'}, minWidth:140},
				];
			var
			gridOptions = {
				hasNo : true,
				columnDefs : columnDefs,
				//rowModelType: 'infinite',
				rowSelection : 'single',
				sizeColumnsToFit: true,
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				enableColResize : true,
				enableServerSideSorting: false,
				onSelectionChanged : function() {
					 
					var arr = overTable.getSelectedRows();
					ureq.getInfo('/api/vm/info?VM_ID='+arr[0].VM_ID)
				},
			}
			underTable = newGrid("underTable", gridOptions);
			under_search();
			 
		});
		function under_search()
		{
			ureq.search('/api/overunder/list/list_NC_UNDER_VM/' , function(data) {
				underTable.setData(data);
			});
		}
	</script>	
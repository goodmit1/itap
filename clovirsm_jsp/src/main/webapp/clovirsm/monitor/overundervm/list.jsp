<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  서버 목록 조회  -->
<div class="search-panel panel panel-default">
	<div class="table_title layout top">변경 적용 권장 항목</div>
	<div id="overunder" style="display: flex; padding-bottom: 10px;">
		<div class="overunder_layout">
			<h4 class="overunser_subtile">다운사이징</h4>
			<div class="overunder_img" style="background-image: url(/res/img/hynix/newHynix/shape-1940.png);">
				<div style="height: 100%;">
					<span class="under_cnt_font green" style="background: none;">{{ overunder_List[0].VM_CNT }}</span>
					<div style="display: inline-block;">
						<div class="under_cnt_font_vm">vm</div>
						<div class="under_cnt_font_text">다운사이징 권장</div>
					</div>
				</div>
			</div>
			<div>
				<div class="overunder_row">
					<div class="overunder_row_text">리소스</div>	
					<div class="overunder_row_text">권장 감소량</div>	
				</div>
				<div class="overunder_row inner">
					<div class="overunder_row_text inner">CPU</div>	
					<div class="overunder_row_text inner">{{ overunder_List[0].OVER_CPU_CNT }} vCPU</div>	
				</div>
				<div class="overunder_row inner bottom">
					<div class="overunder_row_text inner">Memory</div>	
					<div class="overunder_row_text inner">{{ overunder_List[0].OVER_MEM_CNT }} GB</div>	
				</div>
			</div>
		</div>
		<div class="overunder_layout">
			<h4 class="overunser_subtile">업사이징</h4>
			<div class="overunder_img" style="background-image: url(/res/img/hynix/newHynix/shape-1940-copy-2.png);">
				<div style="height: 100%;">
					<span class="under_cnt_font red" style="background: none;">{{ overunder_List[1].VM_CNT }}</span>
					<div style="display: inline-block;">
						<div class="under_cnt_font_vm">vm</div>
						<div class="under_cnt_font_text">업사이징 권장</div>
					</div>
				</div>
			</div>
			<div class="overunder_row">
				<div class="overunder_row_text">리소스</div>	
				<div class="overunder_row_text">권장 증가량</div>	
			</div>
			<div class="overunder_row inner">
				<div class="overunder_row_text inner">CPU</div>	
				<div class="overunder_row_text inner">{{ overunder_List[1].OVER_CPU_CNT }}  vCPU</div>	
			</div>
			<div class="overunder_row inner bottom">
				<div class="overunder_row_text inner">Memory</div>	
				<div class="overunder_row_text inner">{{ overunder_List[1].OVER_MEM_CNT }} GB</div>	
			</div>
		</div>	
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:450px">

	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab1" data-toggle="tab"><spring:message code="OVERUNDER_OVER_VM" text="크기 초과 VM"/></a>
		</li>
		<li>
			<a href="#tab2" data-toggle="tab"><spring:message code="OVERUNDER_UNDVER_VM" text="크기 미만 VM"/></a>
		</li>
		<%-- <li class="tab_right" style="text-align: center; float: right; background: none !important;">
			<fm-popup-button popupid="vm_update_popup" popup="/clovirsm/popup/vm_detail_form_popup.jsp?action=update&callback=onAfterModify" class="vm_spec_btn" param="req.getData()"><spring:message code="btn_spec_modify_req" text="사양 변경 요청" /></fm-popup-button>
		</li> --%>
	</ul>
	<div class="tab-content ">
		<div class="form-panel detail-panel tab-pane active" id="tab1">
			<jsp:include page="over_detail.jsp"></jsp:include>
		</div>
		<div  class="form-panel detail-panel tab-pane" id="tab2">
			<jsp:include page="under_detail.jsp"></jsp:include>
		</div>
	</div>
</div>
<script>
	var req = new Req();
	
	var overunder = new Vue({
		el:'#overunder',
		data: {
			overunder_List:[]
		},
		methods:{
			getReqInfo: function () {
				var that = this;
				$.get('/api/overunder/list/NC_OVERUNDER_CNT/', function(data){
					 
					that.overunder_List = data;
				});
			}
		},
		
	})
	$(document).ready(function(){
		overunder.getReqInfo();
		//refCallback.push('overunder.getReqInfo');
	});

</script>



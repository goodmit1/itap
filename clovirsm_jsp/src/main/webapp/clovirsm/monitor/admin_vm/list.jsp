<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
	String VM_NM = request.getParameter("VM_NM");
	request.setAttribute("VM_NM", VM_NM == null ? "null" : "'" + VM_NM + "'");
	String DC_ID = request.getParameter("DC_ID");
	request.setAttribute("DC_ID", DC_ID == null ? "null" : "'" + DC_ID + "'");
%>
<style>
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<c:if test="${sessionScope.GRP_ADMIN_YN == 'Y'}">
			<div class="col col-sm">
				<fm-select url="/api/etc/team_list" id="S_TEAM_CD" emptystr=" "
					name="TEAM_CD"
					title="<spring:message code="FM_TEAM_TEAM_CD" text="팀" />"></fm-select>
			</div>
			<div class="col col-sm">
				<fm-select
					url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC"
					id="S_DC_ID" emptystr=" " keyfield="DC_ID" titlefield="DC_NM"
					name="DC_ID"
					title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
				</fm-select>
			</div>
		</c:if>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=RUN_CD" id="S_RUN_CD" emptystr=" "
				name="RUN_CD"
				title="<spring:message code="NC_VM_RUN_CD" text="서버상태"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=P_KUBUN" id="S_P_KUBUN"
				emptystr=" " name="P_KUBUN"
				title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>">
			</fm-select>
		</div>
		 <div class="col col-sm">
			<fm-input id="S_HOST_NM"  name="HOST_NM" 
				title="<spring:message code="HOST_NM" text="HOST" />">
			</fm-input>
		</div>
	 
		<div class="col col-sm" >
				<fm-select2 id="S_CATEGORY" title="<spring:message code="TASK" text="업무"/>" 
					name="CATEGORY" emtpyStr=""   url="/api/code_list?grp=dblist.com.clovirsm.common.Component.listLeafCategory" 
					keyfield="CATEGORY_ID" titlefield="CATEGORY_NMS" select_style="width:250px;" ></fm-select2>			
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=PURPOSE" id="S_PURPOSE"
				emptystr=" " name="PURPOSE"
				title="<spring:message code="PURPOSE" text="용도"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="keyword_input" class="keyword" name="keyword" input_style="width: 160px;"
				placeHolder="<spring:message code="NC_VM_VM_NM" text="서버명" />/<spring:message code="NC_VM_GUEST_NM" text="OS명" />/<spring:message code="NC_VM_PRIVATE_IP" text="IP" />/<spring:message code="NC_VM_INS_ID_NM" text="담당자" />"
				title="<spring:message code="keyword_input" text="키워드" />">
			</fm-input>
		</div>
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="vmSearch()">
			<spring:message code="btn_search" text="검색" /></fm-sbutton>
			
		</div>
		<div id="popup-button-html"></div>
	</div>
</div>
<div class="fullGrid" id="input_area" style="height: 340px">
	<div class="box_s">
	<div class="table_title layout name">
		<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
		 
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
			<c:if test="${sessionScope.ADMIN_YN == 'Y'}">	    
<%-- 			<fm-sbutton cmd="update" class="contentsBtn tabBtnImg sync" onclick="javascript:powerOn();"><spring:message code="btn_poweron" text="시작"/></fm-sbutton>	     --%>
<%-- 			<fm-sbutton cmd="update" class="contentsBtn tabBtnImg del" onclick="javascript:powerOff();"><spring:message code="btn_poweroff" text="정지"/></fm-sbutton> --%>
<%-- 			<fm-sbutton cmd="update" class="contentsBtn tabBtnImg sync" onclick="javascript:reboot();"><spring:message code="btn_reboot" text="재부팅"/></fm-sbutton> --%>
			</c:if>
			<c:if test="${sessionScope.LOG_SHOW == 'Y'}">
			<fm-popup-button popupid="category1" class="contentsBtn tabBtnImg sync"
					popup="/clovirsm/monitor/log.jsp"
					cmd="update" param="form_data.VM_NM" >LOG</fm-popup-button>
			</c:if>		
					
					
			 
		</div>
	</div>
	<div id="mainTable" class="ag-theme-fresh" style="height: 300px"></div>
	</div>
	<div class="gab"></div>
	<jsp:include page="detail.jsp"></jsp:include>
</div>
<script>
   var vmReq = new Req();
   mainTable;
   $(function() {
       var
       columnDefs = [ {
           headerName : '',
           hide : true,
           field : 'VM_ID'
       },{
           headerName : '<spring:message code="NC_DC_DC_NM" text="데이터센터명" />',
           field : 'DC_NM',
          
           width: 200,
           
       },{
           headerName : '<spring:message code="NC_VM_VM_NM" text="서버명" />',
           field : 'VM_NM',
          
           width: 180,
       
           tooltip: function(params){
               if(params.data) return params.data.VM_NM;
           }
       },{
           headerName : '<spring:message code="NC_VM_PRIVATE_IP" text="IP" />',
           field : 'IP',
          
           width: 140,
         
       },{
           headerName : '<spring:message code="NC_VM_RUN_CD" text="서버상태" />',
           field : 'RUN_CD',
           
           width: 100,
          
           tooltip: function(params){
               var serverStatus = '-';
               if(params.data && params.data.RUN_CD) {
                   serverStatus = params.data.RUN_CD_NM;
                   if(params.data && params.data.CUD_CD){
                       if(params.data.CUD_CD == 'C') serverStatus = params.data.CUD_CD_NM + ' ' + params.data.APPR_STATUS_CD_NM;
                       else  serverStatus += '('+params.data.CUD_CD_NM + ' ' + params.data.APPR_STATUS_CD_NM + ')';
                   }
               }
               return serverStatus;
           },
           cellRenderer: function(params) {
               var serverStatus = '';
               if(params.data && params.data.RUN_CD) {
            	  
                   if(params.data.RUN_CD == 'R'){
	                   serverStatus += "<spring:message code="SERVICE_START" text="시작" />"
                   }
                   if(params.data.RUN_CD == 'S'){
	                   serverStatus += "<spring:message code="SERVICE_STOP" text="정지" />"
                   }
                   if(params.data.OVERALLSTATUS){
            		   serverStatus +='<img src="/res/img/' + params.data.OVERALLSTATUS + '-icon.png">'
            	   }
               }
               return serverStatus;
           }
       },{
           headerName : '<spring:message code="NC_OS_TYPE_GUEST_NM" text="OS명" />',
           field : 'GUEST_NM',
          
           width: 280,
          
           tooltip: function(params){
               if(params.data) return params.data.GUEST_NM;
           }
       },{
           headerName : '<spring:message code="HOST_NM" text="Host" />',
           field : 'HOST_NM',
         
           width: 150,
         
       },{headerName : "MEM(GB)",field : "RAM_SIZE", cellStyle:{'text-align':'right'}, width:100},
		{headerName : "CPU(Core)",field : "CPU_CNT", cellStyle:{'text-align':'right'}, width:100},
		{headerName : "DISK(GB)",field : "DISK_SIZE", cellStyle:{'text-align':'right'}, width:100,
			headerTooltip:'<spring:message code="monitor_disk_used" text="" />(GB)',valueGetter:function(params){
			return params.data.DISK_USAGE? (params.data.DISK_SIZE * params.data.DISK_USAGE / 100)  : '';
		}},
		{headerName : "GPU",field : "GPU_MODEL",   width:100, valueGetter:function(params){
				if( params.data.GPU_MODEL ){
					return params.data.GPU_MODEL + '-' +  params.data.GPU_SIZE + 'q'
				}
				else{
					return ''
				}
			}
		},
		{
           headerName : '<spring:message code="NC_VM_P_KUBUN" text="구분" />',
           field : 'P_KUBUN_NM',
           
           sort_field: 'P_KUBUN',
           width: 120,
           
       },{
           headerName : "<spring:message code="NC_OS_TYPE_SERVICE"/>",
           field : "CATEGORY_NM",
          
           tooltipField:'CATEGORY_NM',
           
       },{
           headerName : '<spring:message code="FM_TEAM_TEAM_CD" text="팀" />',
           field : 'TEAM_NM',
          
           width: 120,
            
       },{
           headerName : '<spring:message code="FM_TEAM_INS_ID_NM" text="담당자" />',
           field : 'INS_ID_NM',
        
           width: 120,
           
       },{
           headerName : '<spring:message code="NC_VM_INS_TMS" text="생성일시"/>',
           field : 'INS_TMS',
           
           width: 110,
           
           valueGetter: function(params) {
               if(params.data) return formatDate(params.data.INS_TMS,'date')
           }
       }
       ];
       var
       gridOptions = {
           hasNo : true,
           columnDefs : columnDefs,
//            rowModelType: 'infinite',
           cacheBlockSize: 100,
           sizeColumnsToFit:false,
           rowSelection : 'single',
           rowData : [],
           resizable: true,
           enableSorting : true,
           enableColResize : true,
           enableServerSideSorting: true,
           onSelectionChanged : function(e) {
               var arr = mainTable.getSelectedRows();
               form_data = arr[0];
               $(".detailBtn").prop("disabled",false)
               getVMInfo(arr[0] )
           },

       }
       mainTable = newGrid("mainTable", gridOptions);
     
       $(".detailBtn").prop("disabled",true)
       vmSearch();
   });
   function initFormData(){
	  
   }
   function getVMInfo(data )
   {
        chgTab(tabIdx,data);

   }

   function openVMSavePopup( )
   {

       var data = vmReq.getData();
       if(data.CUD_CD){
           if(data.CUD_CD == 'C'){
               $('#vm_detail_tab_popup_button').trigger('click');
           } else if(data.CUD_CD == 'U'){
               $('#vm_update_popup_button').trigger('click');
           } else if(data.CUD_CD == 'D'){
               $('#vm_req_info_popup_button').trigger('click');
           } else {
               $('#vm_save_popup_button').trigger('click');
           }
       } else {
           $('#vm_save_popup_button').trigger('click');
       }

   }


   // 조회
   function vmSearch(callback) {
	   vmReq.search('/api/vm/list/list_admin_vm/', function(data){
    	    
    	   mainTable.setData(data);
       });
   }

 //서버 시작
	function powerOn(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			post('/api/vm/powerOn', form_data, function(){
				vmSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}

	//서버 정지
	function powerOff(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			post('/api/vm/powerOff', form_data, function(){
				vmSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}

	//서버 재부팅
	function reboot(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			post('/api/vm/reboot', form_data, function(){
				vmSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}
	 
   // 엑셀 내보내기
   function exportExcel(){
	   mainTable.exportCSV({fileName:"VM_MONITOR"});
//        exportExcelServer("mainForm", '/api/vm/list_excel', 'Snapshot',mainTable.gridOptions.columnDefs, vmReq.getRunSearchData())
   }

</script>

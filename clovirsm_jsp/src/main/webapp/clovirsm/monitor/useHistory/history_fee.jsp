<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<div id="fee_history_bar" class="chart_area"></div>
<script>
		var fee_chart;
		$(document).ready(function(){
				
		}) ;
		
		function drawFeeHistoryChart(  list) {
			 
			var config = {
					  chart: {
						    type: 'area'
					  } 	,
					  title: {
					    text: '<spring:message code="month_fee_history" text="월별 비용 추이"/>'
					  },
					  xAxis: {
					        type: 'category'
					    },
					  yAxis: [
						  {
						    title: {
						      text: '<spring:message code="COL_FEE"/>'
						    },
						    min :0

						  }

						  
						],
					  legend: {
					    enabled: true
					  },

					  series:[ 
						  {
							  name:'<spring:message code="label_cost"/>', 
							  data: list,
							  color : '#ffaa28'
						  }
					  ]
					 
				};



		      
			 fee_chart = Highcharts.chart('fee_history_bar', config);

				 
				

			}


			</script>

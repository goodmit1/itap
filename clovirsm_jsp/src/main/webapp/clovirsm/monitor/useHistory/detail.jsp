<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<table id="useHistoryTotalForm" class="useHistoryTotalForm">
	<tr style="height: 110px;">
		<td class='table_label label_fee' style="width: 8%"><spring:message
				code="label_use_fee" /></td>
		<td class='table_input num' style="width: 15%">{{form_data.FEE |
			number}} <span class="unit"><spring:message
					code="label_price_unit" /></span>
		</td>
		<td class='table_label label_ip' style="width: 8%"><spring:message
				code="label_server" /></td>
		<td class='table_input num' style="width: 10%">

			{{form_data.VM_CNT| number}} <span class="unit"><spring:message
					code="label_amount" /></span>
		</td>
		<td class='table_label label_server' style="width: 8%"><spring:message
				code="label_cpu" text="CPU"/></td>
		<td class='table_input num' colspan="1" style="width: 10%">
			{{form_data.CPU_CNT| number}} <span class="unit">vCore</span>
		</td>
		<td class='table_label label_disk' style="width: 8%"><spring:message
				code="label_disk" /></td>
		<td class='table_input num' style="width: 10%">
			{{ (1.0 * form_data.DISK_SIZE / 1024).toFixed(1)  }} <span class="unit">TB</span>
		</td>

		<td class='table_label label_server' style="width: 8%"><spring:message
				code="label_mem" /></td>
		<td class='table_input num' colspan="1" style="width: 10%">
			{{form_data.RAM_SIZE| number}} <span class="unit">GB</span>
		</td>
		<c:if test="${sessionScope.IMG_SHOW }">
			<td class='table_label label_img' style="width: 8%"><spring:message
					code="label_img" /></td>
			<td class='table_input num' colspan="1" style="width: 10%">
				{{form_data.IMG_CNT| number}} <span class="unit"><spring:message
						code="label_amount" /></span>
			</td>
		</c:if>
	</tr>


</table>
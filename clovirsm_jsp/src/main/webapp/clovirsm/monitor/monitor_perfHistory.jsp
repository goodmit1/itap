<%@page import="com.clovirsm.service.monitor.MonitorService"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%

MonitorService service=(MonitorService)SpringBeanUtil.getBean("monitorService");
String[] types = service.getPerfHistoryTypes("V", request.getParameter("key"));
pageContext.setAttribute("perfHistoryTypes", types);

%>
<link rel="Stylesheet" type="text/css" 	href="/res/css/vm_chart.css" />
<style>
#hideArea .input-group-addon 
{
    padding:0px;
}
#hideArea input.form-control
{
	height:20px;
}
</style>
<script src="/res/js/perf-chart.js"></script>			

		<div id="vmChartArea">
			<jsp:include page="_time_sel_area.jsp"/>
		 	<div style="clear: both"  id="chartAreaTable"></div>
		</div>
<script>
var isMonitor = false;
var interval =null;
var vmChartArea = new Vue({
	  el: '#vmChartArea',
	  data: {
		  
		  monitor_arr:[],
		  color_arr:[ "#2e8fe2", "#ffaa28", "#54bf19", "#726ac2" ],
		  y_max:[null,null,null,null],
	  	  form_data:{}

	  },
	  methods: {
		init:function()
	  	  {
			this.chkPeriod($(".termArea dd")[0]);
			
			var table = $("#chartAreaTable")
			
			<c:forEach items="${perfHistoryTypes}" varStatus="status" var="perf" >
				this.monitor_arr.push("${perf}");
			<c:set var="msgKey" value="monitor_${perf}"></c:set>
				 
			if('${perf}' == "cpu_usage"){
				this.y_max[${status.index}] = 100; 
			}else if('${perf}' == "mem_usage"){
				this.y_max[${status.index}] = 100;
			}

			table[0].innerHTML += this.getAreaHtml('<spring:message code="${msgKey}" text="" />', '${perf}');
			
			</c:forEach>
	  	  },
	  	chgPeriod : function (obj) {
			if(this.chkPeriod(obj) == null){
				this.reloadVMPerf();
			}
		  },
		
		chkPeriod : function(obj) {
			var val = $(obj).attr("period");
			if (this.oldPeriodId) {
				$("#" + this.oldPeriodId).removeClass("selected");
			}
			this.oldPeriodId = obj.id;
			$(obj).addClass("selected");
			if(val == null || "" == val){
				$(obj).parent().find("span[id$='hideArea']").show();
				this.period=null;
				return true;
			}else{
				//$("#START_DT").setDate(null);
				//$("#FINISH_DT").setDate(null);
				
				$(obj).parent().find("span[id$='hideArea']").hide();
			}
			this.period=val;
		},
		reloadVMPerf:function(selRow)
		{
			if(selRow && selRow != null) {
				this.selRow = selRow;
			}
			if(!this.selRow) return;
			var param1={};
			 
			param1.PERIOD=this.period;
			 
			if(this.period == null){
				if($("#START_DT").val() != ""){
					param1.START_DT=$("#START_DT").val()+" "+$("#START_DT_S").val();
					 
				}
				if($("#FINISH_DT").val() != ""){
					param1.FINISH_DT=$("#FINISH_DT").val()+" "+$("#FINISH_DT_S").val();
					 
				}
			}
// 			$("#FINISH_DT_S").val()
			var that= this;
			 
			if("cpu_usage" == "${perf}"){
				that.y_max = 100;
			} else if("mem_usage" == "${perf}"){
				that.y_max = 100;
			}
			post('/api/monitor/perf_history/' + this.selRow.DC_ID + '/${param.key}/' + this.selRow.name +'/',  param1, function(data)
					{
						 
						this.drawVMPerf(  data, that.monitor_arr, that.color_arr, that.y_max);
					}
				)
		},
		getAreaHtml:function(title, id)
		{
			var html = '<div class="chart_50"><div  class="ui-panel ui-widget ui-widget-content ui-corner-all"  ><div  class="ui-panel-content ui-widget-content"><label   class="ui-outputlabel ui-widget chart_title">' + title + '</label>'
			html += '<div id="container_' + id + '" style="min-width: 350px; height: 350px; margin: 10px auto"></div></div></div></div>'
			return html
		}
	  }
});
		 
$(document).ready(function(){
	vmChartArea.init();
	
	$("#vmChartArea dd").click(function() {

		vmChartArea.chgPeriod(this);
	});

})

function monitorStartAndStop(){
	
	if(isMonitor){
		isMonitor = false;
		$("#isMonitor").attr("src","/res/img/mobis/start_icon.png");
		clearInterval(interval);
	}else{
		isMonitor = true;
		$("#isMonitor").attr("src","/res/img/mobis/stop_icon.png");
		var time = $("#reloadTime").val() * 60000;
		interval = setInterval(function() {reloadVMPerf();}, time);
	}
}
function reloadVMPerf()
{	
	var now = new Date();
	vmChartArea.reloadVMPerf(null);
	
}

		</script>
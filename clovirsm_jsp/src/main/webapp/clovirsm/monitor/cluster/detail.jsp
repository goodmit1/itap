<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<ul class="nav nav-tabs">
	  	<li class="active">
	        <a  href="#tab1" data-toggle="tab" onclick="setTabIdx( 0)">VM</a>
		</li>
	 	<li >
	        <a  href="#tab2" data-toggle="tab" onclick="setTabIdx( 1)"><spring:message code="monitor_perf" text="성능"/></a>
		</li>
		<li >
	        <a  href="#tab3" data-toggle="tab" onclick="setTabIdx( 2)" ><spring:message code="monitor_event" text="이벤트"/></a>
		</li>
		<li >
	        <a  href="#tab4" data-toggle="tab" onclick="setTabIdx( 3)" ><spring:message code="monitor_alarm" text="경고"/></a>
		</li>

	</ul>
	<div class="tab-content "  >
		<div style="height:600px" class="form-panel detail-panel tab-pane active" id="tab1" >
			<jsp:include page="../monitor_vm_list.jsp">
				<jsp:param value="CLUSTER" name="key"/>
			</jsp:include>
		</div>
		<div  style="height:800px" class="form-panel detail-panel tab-pane" id="tab2"  >
			<jsp:include page="../monitor_perfHistory.jsp">
				<jsp:param value="CLUSTER" name="key"/>
			</jsp:include>
		</div>
		<div  class="form-panel detail-panel tab-pane" id="tab3"  >
			<jsp:include page="../monitor_event.jsp">
				<jsp:param value="CLUSTER" name="key"/>
			</jsp:include>
		</div>
		<div  class="form-panel detail-panel tab-pane" id="tab4"  >
			<jsp:include page="../monitor_alarm.jsp">
				<jsp:param value="CLUSTER" name="key"/>
			</jsp:include>
		</div>
	</div>
	<script>
		var selRow;
		var tabIdx = 0;
		function setTabIdx(idx)
		{
			tabIdx = idx;
			goTabContent();
		}
		function goTabContent(param1 )
		{

			if(param1)
			{
				this.selRow = param1;
			}
			if(!selRow) return;
			if(tabIdx==0)
			{
				vmTable.getReqInfo(selRow);
			}
			else if(tabIdx==1)
			{

				vmChartArea.reloadVMPerf(selRow);
			}
			else if(tabIdx==2)
			{
				eventTable.getReqInfo(selRow);
			}
			else if(tabIdx==3)
			{
				alarmTable.getReqInfo(selRow);
			}
		}
	</script>
<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
	String VM_NM = request.getParameter("VM_NM");
	request.setAttribute("VM_NM", VM_NM == null ? "null" : "'" + VM_NM + "'");
	String DC_ID = request.getParameter("DC_ID");
	request.setAttribute("DC_ID", DC_ID == null ? "null" : "'" + DC_ID + "'");
%>

<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<c:if test="${sessionScope.GRP_ADMIN_YN == 'Y'}">
			<div class="col col-sm">
				<fm-select url="/api/etc/team_list" id="S_TEAM_CD" emptystr=" "
					name="TEAM_CD"
					title="<spring:message code="FM_TEAM_TEAM_CD" text="팀" />"></fm-select>
			</div>
			<div class="col col-sm">
				<fm-select
					url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC"
					id="S_DC_ID" emptystr=" " keyfield="DC_ID" titlefield="DC_NM"
					name="DC_ID"
					title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
				</fm-select>
			</div>
		</c:if>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=RUN_CD" id="S_RUN_CD" emptystr=" "
				name="RUN_CD"
				title="<spring:message code="NC_VM_RUN_CD" text="서버상태"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=P_KUBUN" id="S_P_KUBUN"
				emptystr=" " name="P_KUBUN"
				title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-select-yn id="S_EXPIRE_YN" emptystr=" " name="EXPIRE_YN"
				title="<spring:message code="NC_VM_EXPIRE_YN" text="만료여부"/>">
			</fm-select-yn>
		</div>
		<div style="">
		<c:if test="${sessionScope._USER_TYPE_ == '19'}">
			<div class="col col-sm-2"
				style="width: 13.666667%; min-width: 283px;">
				<fm-ibutton id="S_CATEGORY_NM" name="CATEGORY_NM"
					title="<spring:message code="NC_OS_TYPE_CATEGORY"/>" class="inline">
				<fm-popup-button popupid="category1"
					popup="/clovirsm/popup/img_category_search_form_popup.jsp"
					cmd="update" param="false" callback="select_S_CATEGORY"></fm-popup-button>
				</fm-ibutton>
				<input type="text" style="display: none" id="S_CATEGORY"
					name="CATEGORY">
			</div>
		</c:if>
		<div class="col col-sm">
			<fm-input id="keyword_input" class="keyword" name="keyword"
				placeHolder="<spring:message code="NC_VM_VM_NM" text="서버명" />/<spring:message code="NC_VM_GUEST_NM" text="OS명" />/<spring:message code="NC_VM_PRIVATE_IP" text="IP" />/<spring:message code="NC_VM_INS_ID_NM" text="담당자" />"
				title="<spring:message code="keyword_input" text="키워드" />">
			</fm-input>
		</div>
		<div class="col col-search">
			<fm-sbutton cmd="search" class="searchBtn" onclick="vmSearch()">
			<spring:message code="btn_search" text="검색" /></fm-sbutton>
		</div>
		</div>
		<div id="popup-button-html"></div>
	</div>
</div>
<div class="fullGrid" id="input_area" style="height: 340px; width: 100%;">
	<div class="table_title layout name">
		<div class="search_info">
			<spring:message code="mainTable_Search_information" text="검색정보" />
			<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp) --%>
			<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button"
				title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>"
				onclick="exportExcel()" class="layout excelBtn"></button>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 300px"></div>
	</div>
	<div class="gab"></div>
	<jsp:include page="detail.jsp"></jsp:include>
</div>
<script>
   var vmReq = new Req();
   mainTable;
   $(function() {
       var
       columnDefs = [ {
           headerName : '',
           hide : true,
           field : 'VM_ID'
       },{
           headerName : '<spring:message code="NC_DC_DC_NM" text="데이터센터명" />',
           field : 'DC_NM',
           maxWidth: 250,
           width: 250,
           minWidth: 200
       },{
           headerName : '<spring:message code="NC_VM_VM_NM" text="서버명" />',
           field : 'VM_NM',
           maxWidth: 180,
           width: 180,
           minWidth: 150,
           tooltip: function(params){
               if(params.data) return params.data.VM_NM;
           }
       },{
           headerName : '<spring:message code="NC_VM_P_KUBUN" text="구분" />',
           field : 'P_KUBUN_NM',
           maxWidth: 120,
           sort_field: 'P_KUBUN',
           width: 120,
           minWidth: 120
       },{
           headerName : "<spring:message code="NC_REQ_SVC_CD" text="서비스"/>",
           field : "CATEGORY_NM",
           minWidth: 120
       },{
           headerName : '<spring:message code="NC_VM_PRIVATE_IP" text="IP" />',
           field : 'PRIVATE_IP',
           maxWidth: 140,
           width: 140,
           minWidth: 140
       },{
           headerName : '<spring:message code="NC_VM_RUN_CD" text="서버상태" />',
           field : 'RUN_CD',
           maxWidth: 200,
           width: 100,
           minWidth: 100,
           tooltip: function(params){
               var serverStatus = '-';
               if(params.data && params.data.RUN_CD) {
                   serverStatus = params.data.RUN_CD_NM;
                   if(params.data && params.data.CUD_CD){
                       if(params.data.CUD_CD == 'C') serverStatus = params.data.CUD_CD_NM + ' ' + params.data.APPR_STATUS_CD_NM;
                       else  serverStatus += '('+params.data.CUD_CD_NM + ' ' + params.data.APPR_STATUS_CD_NM + ')';
                   }
               }
               return serverStatus;
           },
           cellRenderer: function(params) {
               var serverStatus = '-';
               if(params.data && params.data.RUN_CD) {
                   <c:if test="${sessionScope.ADMIN_YN == 'N'}">
                       if(params.MY_FW_TASK_STATUS_CD  && params.MY_FW_TASK_STATUS_CD != 'S'){
                           return "<spring:message code="admin_working" text="관리자 작업중" />"
                       }
                   </c:if>
                   if(params.data.RUN_CD == 'R'){
	                   serverStatus = "<spring:message code="SERVICE_START" text="시작" />"
                   }
                   if(params.data.RUN_CD == 'S'){
	                   serverStatus = "<spring:message code="SERVICE_STOP" text="정지" />"
                   }
                   
               }
               return serverStatus;
           }
       },{
           headerName : '<spring:message code="NC_OS_TYPE_GUEST_NM" text="OS명" />',
           field : 'GUEST_NM',
           maxWidth: 400,
           width: 280,
           minWidth: 280,
           tooltip: function(params){
               if(params.data) return params.data.GUEST_NM;
           }
       },{
           headerName : '<spring:message code="FM_TEAM_TEAM_CD" text="팀" />',
           field : 'TEAM_NM',
           maxWidth: 120,
           width: 120,
           minWidth: 120
       },{
           headerName : '<spring:message code="FM_TEAM_INS_ID_NM" text="담당자" />',
           field : 'INS_ID_NM',
           maxWidth: 120,
           width: 120,
           minWidth: 100
       },{
           headerName : '<spring:message code="NC_VM_INS_TMS" text="생성일시"/>',
           field : 'INS_TMS',
           maxWidth: 110,
           width: 110,
           minWidth: 110,
           valueGetter: function(params) {
               if(params.data) return formatDate(params.data.INS_TMS,'date')
           }
       },
		{headerName : "<spring:message code="monitor_mem_usage" text="" />",field : "MEM_USAGE", cellStyle:{'text-align':'right'}, minWidth:160},
		{headerName : "<spring:message code="monitor_cpu_usage" text="" />",field : "CPU_USAGE", cellStyle:{'text-align':'right'}, minWidth:140},
		{headerName : "<spring:message code="monitor_disk_used" text="" />(GB)",field : "DISK_USAGE", cellStyle:{'text-align':'right'}, minWidth:170, valueGetter:function(params){
			return params.data.DISK_USAGE? (params.data.DISK_SIZE * params.data.DISK_USAGE / 100)  : '';
		}}  ];
       var
       gridOptions = {
           hasNo : true,
           columnDefs : columnDefs,
           rowModelType: 'infinite',
           groupHeaderHeight:75,
           rowSelection : 'single',
           rowData : [],
           resizable: true,
           enableSorting : true,
           enableColResize : true,
           enableServerSideSorting: true,
           onSelectionChanged : function(e) {
               var arr = mainTable.getSelectedRows();
               getVMInfo(arr[0] )
           },
 
       }
       mainTable = newGrid("mainTable", gridOptions);
      
       vmSearch();
   });
   function getVMInfo(data )
   {
        chgTab(tabIdx,data);
            
   }
   
   function openVMSavePopup( )
   {
 
       var data = vmReq.getData();
       if(data.CUD_CD){
           if(data.CUD_CD == 'C'){
               $('#vm_detail_tab_popup_button').trigger('click');
           } else if(data.CUD_CD == 'U'){
               $('#vm_update_popup_button').trigger('click');
           } else if(data.CUD_CD == 'D'){
               $('#vm_req_info_popup_button').trigger('click');
           } else {
               $('#vm_save_popup_button').trigger('click');
           }
       } else {
           $('#vm_save_popup_button').trigger('click');
       }
 
   }
 
 
   // 조회
   function vmSearch(callback) {
       vmReq.setSearchData('MONITOR','Y');
       vmReq.clearData();
       vmReq.searchPaging('/api/vm/list' , mainTable, function(data){
           $('ul.vmDetailTab').find('li:first a').trigger('click');
           $('ul.vmDetailTab').find('li').addClass('disabledTab');
 
           setButtonClickable('vm_copy_popup_button', false);
           setButtonClickable('img_insert_popup_button', false);
           setButtonClickable('vm_update_popup_button', false);
           setButtonClickable('vm_delete_popup_button', false);
           //setButtonClickable('vm_owner_change_popup_button', false);
           setButtonClickable('vm_action_button', false);
 
           if(data.total > 0){
               
               mainTable.setSelected(0);
               
               if(callback) callback();
           }
       });
   }
 
 
 
   // 엑셀 내보내기
   function exportExcel(){
       exportExcelServer("mainForm", '/api/vm/list_excel', 'Snapshot',mainTable.gridOptions.columnDefs, vmReq.getRunSearchData())
   }
 
</script>

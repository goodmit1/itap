<%@page import="com.clovirsm.sys.hv.vmware.VROpsAction"%>
<%@page import="com.clovirsm.service.monitor.MonitorService"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%

 
%>
<link rel="Stylesheet" type="text/css" 	href="/res/css/vm_chart.css" />
<style>
#hideArea .input-group-addon 
{
    padding:0px;
}
#hideArea input.form-control
{
	height:20px;
}
</style>
<script src="/res/js/perf-chart.js"></script>			

		<div id="vmChartArea">
			<%@include file="../_time_sel_area.jsp"%>
		 	<div style="clear: both"  id="chartAreaTable"></div>
		</div>
<script>  
Highcharts.getOptions().colors=['#9DC8C8','#D1B6E1','#58C9B9', '#519D9E','#77AAAD','#6E7783','#D8E6E7'];
var isMonitor = false;
var interval =null;
var vmChartArea = new Vue({
	  el: '#vmChartArea',
	  data: {
		  
	 
		   
	  	  form_data:{}

	  },
	  methods: {
		init:function()
	  	  {
			this.chkPeriod($(".termArea dd")[0]);
			
			 
	  	  },
	  	chgPeriod : function (obj) {
			if(this.chkPeriod(obj) == null){
				this.reloadVMPerf();
			}
		  },
		
		chkPeriod : function(obj) {
			var val = $(obj).attr("period");
			if (this.oldPeriodId) {
				$("#" + this.oldPeriodId).removeClass("selected");
			}
			this.oldPeriodId = obj.id;
			$(obj).addClass("selected");
			if(val == null || "" == val){
				$(obj).parent().find("span[id$='hideArea']").show();
				return true;
			}else{
				//$("#START_DT").setDate(null);
				//$("#FINISH_DT").setDate(null);
				
				$(obj).parent().find("span[id$='hideArea']").hide();
			}
			this.period=val;
		},
		reloadVMPerf:function(selRow)
		{
			if(selRow && selRow != null) {
				this.selRow = selRow;
			}
			if(!this.selRow) return;
			var param1={};
			param1.PERIOD=this.period;
			if($("#START_DT").val() != ""){
				param1.START_DT=$("#START_DT").val()+" "+$("#START_DT_S").val();
				 
			}
			if($("#FINISH_DT").val() != ""){
				param1.FINISH_DT=$("#FINISH_DT").val()+" "+$("#FINISH_DT_S").val();
				 
			}
			
// 			$("#FINISH_DT_S").val()
			var that= this;
			 
			post('/api/monitor/vrops/meta',{VM_HV_ID: this.selRow.VM_HV_ID, VM_NM:this.selRow.VM_NM}, function (data){
				 
				var table = $("#chartAreaTable");
				table[0].innerHTML = '';
				for(var i=0; i < data.metrics.length; i++){
					table[0].innerHTML += that.getAreaHtml(data.metrics[i]["ops:name"] , data.metrics[i]["ops:unit"] , selRow.VM_NM, data.metrics[i]["ops:key"], data.resourceId);	
					
				}
				param1.resourceId = data.resourceId;
				for(var i=0; i < data.metrics.length; i++){
					param1.statKeys=data.metrics[i]["ops:key"];
					param1.unit=data.metrics[i]["ops:unit"]
					post('/api/monitor/vrops/data',  param1, function(data)	{
								 
						makeMultiLineChart(data.statKeys, data, "area",   data.unit  );
					}
					)
				}
			})
			 
		},
		getAreaHtml:function(title, unit, vmNm, id, resId)
		{
			var html = '<div class="chart_50"><div  class="ui-panel ui-widget ui-widget-content ui-corner-all"  ><div  class="ui-panel-content ui-widget-content"><label data-vmNm="' + vmNm + '" data-unit="' + unit + '" data-statKey="' + id + '" data-resourceId="' + resId + '"  class="ui-outputlabel ui-widget chart_title">' + title + '</label><input type="button" title="<spring:message code="add_home" text="홈에 추가"/>" class="btn addHomeBtn" onclick="addHome(this)" value="+" />'
			html += '<div id="container_' + id.replace(/\|/gi, '') + '" style="min-width: 350px; height: 350px; margin: 10px auto"></div></div></div></div>'
			return html
		}
	  }
});
		 
$(document).ready(function(){
	vmChartArea.init();
	reloadVMPerf();
	$("#vmChartArea dd").click(function() {
	vmChartArea.chgPeriod(this);

	});

})

function addHome(obj){
	var label = $(obj).parent().find("label")
	var path = 'dashboard/vrops_chart.jsp?unit=' + label.attr('data-unit') + '&resourceId=' + label.attr("data-resourceid") + '&statKey=' + label.attr('data-statKey') ;
	var param = {};
	param.PATH = path;
	param.TITLE = label.attr("data-vmNm") + ' ' + label.text();
	param.COL="4"
	post('/api/dashboard_mng/insert/dashboard', param, function(data){
		alert('OK');
	})
}
function monitorStartAndStop(){
	
	if(isMonitor){
		isMonitor = false;
		$("#isMonitor").attr("src","/res/img/mobis/start_icon.png");
		clearInterval(interval);
	}else{
		isMonitor = true;
		$("#isMonitor").attr("src","/res/img/mobis/stop_icon.png");
		var time = $("#reloadTime").val() * 60000;
		interval = setInterval(function() {reloadVMPerf();}, time);
	}
}
function reloadVMPerf()
{	
	var now = new Date();
	vmChartArea.reloadVMPerf(null);
	
}

		</script>
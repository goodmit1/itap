<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>

<!-- 팀관리 -->
<style>
.checkbox-inline { width:200px }
</style>

<!--  조회 조건 및 버튼 -->
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col col-sm ">
			<fm-input id="S_TEAM_CD" name="TEAM_CD" title="<spring:message code="FM_TEAM_TEAM_CD" text="" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_TEAM_NM" name="TEAM_NM" title="<spring:message code="FM_TEAM_TEAM_NM" text="" />"></fm-input>

		</div>
		<div class="col btn_group nomargin">
			<button type="button" class="searchBtn btn" onclick="searchTeam()"><spring:message code="btn_search" text="" /></button>
		</div>
		
		<div class="col btn_group rt">
			<fm-sbutton cmd="update" class="newBtn btn btn-primary" onclick="newTeam()"><spring:message code="btn_new" text="" /></fm-sbutton>
			<fm-sbutton cmd="delete" class="delBtn btn btn-primary" onclick="delTeam()"><spring:message code="btn_delete" text=""/></fm-sbutton>
			<fm-sbutton cmd="update" class="saveBtn btn btn-primary" onclick="saveTeam()"><spring:message code="btn_save" text="" /></fm-sbutton>
		</div>
	</div>
</div>
<div class="fullGrid tem" id="input_area">
	<!--  팀 목록 tree -->
	<div class="col-sm-3 h100 tree">
		<div class="panel panel-default h100">
			<div class="panel-body">
				<fm-tree id="tree" url="/api/site_group/list?COMP_ID=102216547406718"
					:field_config="{onselect:menu_click, root:0,id:'TEAM_CD',parent:'PARENT_CD',text:'TEAM_NM'}"></fm-tree>
			</div>
		</div>
	</div>

	<!--  입력 Form -->
	<div class="col-sm-9 right-panel">
		<div class="form-panel detail-panel panel panel-default h100">

			<div class="panel-body">

				<div class="col col-sm-6">
					<fm-input id="TEAM_CD" name='TEAM_CD' title="<spring:message code="NC_NAT_NAT_ID" text="ID" />" ></fm-input>
				</div>
				<div class="col col-sm-6">
					<fm-input id="TEAM_NM" name="TEAM_NM" title="<spring:message code="FM_TEAM_TEAM_NM" text="" />" required="true"></fm-input>
				</div>
				<div class="col col-sm-6">
					<fm-popup id="PARENT_TEAM_NM" name="PARENT_TEAM_NM" title="<spring:message code="FM_TEAM_PARENT_CD_NM" text=""/>"></fm-popup>
					<input type="hidden" name="PARENT_CD" id="PARENT_CD">
				</div>
				<div class="col col-sm-6">
					<fm-select url="/api/code_list?grp=sys.yn" id="USE_YN"
						name="USE_YN" title="<spring:message code="USE_YN" text=""/>"></fm-select>
				</div>
				<fmtags:include page="team_code.jsp" />


				<script>
					$(document).ready(function(){
						$("#TEAM_CD").prop("readonly",true);
					})

					var isOK = true;
					var team = new Req();

					//조회
					function searchTeam() {
						$("#tree").trigger("search", [ team.getSearchData() ]);

					}

					//삭제
					function delTeam() {
						team.del('/api/site_group/delete', function() {
							$("#tree").trigger("reload");
							team.setData({a:""});
						});
					}

					//저장
					function saveTeam() {
						if(validate("input_area"))
						{
							if(!team.getData().PARENT_CD || team.getData().PARENT_CD=='')
							{
								team.getData().PARENT_CD = '0';
							}
							isOK = false;
							team.save('/api/site_group/save', function(data) {
								isOK = true;
								team.getData().TEAM_CD=data.TEAM_CD
								$("#tree").trigger("reload");
							});
						}
					}

					//추가
					function newTeam() {
						var curr_data = team.getData()  ;
						if(!isOK)
						{
							curr_data = {PARENT_CD:0, PARENT_TEAM_NM:''};
						}
						var data = {
							USE_YN : "Y",
							"PARENT_CD" : curr_data.TEAM_CD ? curr_data.TEAM_CD:0,
							"PARENT_TEAM_NM" : curr_data.TEAM_NM
						};
						team.setData(data);
						$("#TEAM_NM").focus();
					}

					// tree node 클릭시 팀 상세 정보 조회
					function menu_click(selectedArr) {
						isOK = true;
						team.getInfo('/api/site_group/info?TEAM_CD='
								+ selectedArr[0].TEAM_CD);
					}
				</script>

			</div>
		</div>
	</div>
</div>


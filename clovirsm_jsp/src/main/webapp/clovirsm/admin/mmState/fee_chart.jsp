<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div id="monitor_fee" class="chart_area col col-sm-4"></div>
<script>
var seriesTemp = [{data:[]}];
var seriesFee = [{data:[]}];
var chartFee
function loadFeeChart(){
	setDate();
	post("/api/mmpay/list", searchvue.form_data, drawFeeChart, true);
}


function toggleFeeHistory(keyword, title, idx, selected){
	 
	if(!selected) {
		if(seriesFee[0].data.length != 0) {
			for(var i = 0; i < chartFee.series[0].data.length; i++) {
				if(chartFee.series[0].data[i].name == title) {
					chartFee.series[0].data[i].remove();
					break;
				}
			}
		}
	} else {
		for(var i=0; i < seriesTemp[0].data.length; i++) {
			if(seriesTemp[0].data[i].name == title) {
				 
				seriesFee[0].data.push({name: seriesTemp[0].data[i].name, y: seriesTemp[0].data[i].y});
				break;
			}
		}
		chartFee.series[0].setData(seriesFee[0].data, true);
	}
}
function drawFeeChart(args) {
	seriesFee = [{data:[]}];
	var list = args.list;
	 
	for(var i=0; i < list.length; i++) {
		seriesFee[0].data.push({name:list[i].TEAM_NM, y:list[i].FEE});
		seriesTemp[0].data.push({name:list[i].TEAM_NM, y:list[i].FEE});
	}
	 

	var config1 = {
		chart: {
			type: 'pie'
		},
		title: {
			text: '<spring:message code="NC_MM_STAT_FEE_PIE_TITLE" text="그룹별 이용요금"/>'
		},
		legend: {
			enabled: true
		},
		series: seriesFee
	};

	chartFee=Highcharts.chart('monitor_fee', config1);
	for(var i=0; i< 5 ; i++){
		mmGrid.setSelected(i,true);
	}
}
</script>
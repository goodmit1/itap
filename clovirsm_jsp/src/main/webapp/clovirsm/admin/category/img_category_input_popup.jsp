<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<fm-modal id="category_edit_popup" cmd="header-title" title="<spring:message code="label_category_edit" text="서비스 정보"/>" >
	<span slot="footer">
		<input type="button" id="category_delBtn" class="btn delBtn forCategoryEdit  popupBtn finish top5" value="<spring:message code="btn_delete" text=""/>" onclick="category_delete()" />
		<input type="button" class="btn newBtn forCategoryEdit  popupBtn prev top5" value="<spring:message code="new_sub_category" text="하위 신규 서비스" />" onclick=" new_sub_category()" />
		<input type="button" class="btn exeBtn popupBtn next top5" value="<spring:message code="label_confirm" text=""/>" onclick=" category_save()" />
		
		
	</span>
	<div class="form-panel detail-panel">
		<form id="category_edit_popup_form" action="none" onsubmit='return false'  >
			<div class="panel-body">
				<div class="col col-sm-12">
					<fm-input id="IMG_CATEGORY_NAME" name="CATEGORY_NM" placeholder="<spring:message code="category_name_multi_input_msg" text=">을 구분하여 입력하시면 하위까지 생성됩니다."/>" title="<spring:message code="name" text="명칭"/>" onKeyDown="enterCheck(event);"></fm-input>
					{{form_data.PAR_CATEGORY_ID}}
				</div>
				 
			</div>
		</form>
	</div>
	<script>
		 
		var category_edit_popup_vue = new Vue({
			el: '#category_edit_popup',
			data: {
				form_data: {}
			}
		});
		function category_edit_popup_click(  param){
			if(!param.CATEGORY_ID)
			{
				$(".forCategoryEdit").hide();
				$("#category_edit_popup_title").text("<spring:message code="label_category_edit" text="서비스 정보"/><spring:message code="label_add" text="추가"/>")
			}
			else
			{
				$(".forCategoryEdit").show();
				$("#category_edit_popup_title").text("<spring:message code="label_category_edit" text="서비스 정보"/><spring:message code="label_modify" text="변경"/>")
			}
			 
			category_edit_popup_vue.callback = param.callback;
			category_edit_popup_vue.form_data.CATEGORY_NM=param.CATEGORY_NM;
			category_edit_popup_vue.form_data.PAR_CATEGORY_ID=param.PAR_CATEGORY_ID;
			category_edit_popup_vue.form_data.CATEGORY_ID=param.CATEGORY_ID;
			$("#IMG_CATEGORY_NAME").val(param.CATEGORY_NM);
			 
			return true;
		}
		function new_sub_category(){
			var param = {PAR_CATEGORY_ID:category_edit_popup_vue.form_data.CATEGORY_ID}
			category_edit_popup_click(param)
		}
		function category_delete()
		{
			var param =  category_edit_popup_vue.form_data;
			if(confirm(msg_confirm_delete))
			{	
				post('/api/ostype/delete_category', param, function(data){
					ostypeSearch();
					$("#category_edit_popup").modal("hide");
				})
			}
		}
		function enterCheck(event){
			if(event.keyCode == 13){
				event.preventDefault();
				category_save();
			}
		}
		function category_save()
		{
			if(!validate("category_edit_popup")) return;
			var name = $("#IMG_CATEGORY_NAME").val();
			 
			if(category_edit_popup_vue.form_data.CATEGORY_ID  && name.indexOf(">")>=0)
			{
				alert("<spring:message code="msg_avail" text="사용할 수 없는 문자열이 있습니다."/>(>)");
				return ;
			}	
			var param = {CATEGORY_ID:category_edit_popup_vue.form_data.CATEGORY_ID,PAR_CATEGORY_ID:category_edit_popup_vue.form_data.PAR_CATEGORY_ID, CATEGORY_NM:name}
			post('/api/ostype/save_category', param, function(data){
					ostypeSearch();
					$("#category_edit_popup").modal("hide");
				})
			 
		}
		
	</script>
	<style>
	</style>
</fm-modal>
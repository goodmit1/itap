<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- 입력 Form -->

<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body" >
			<div class="col col-sm-12">
				<fm-input id="SW_NM" name="SW_NM" title="Application" onchange="nameChange();" required="required"></fm-input>
			</div>
			<div class="col col-sm-6">
				<fm-input id="SW_VER" name="SW_VER" title="VERSION" onchange="nameChange();" required="required"></fm-input>
			</div>
			<div class="col col-sm-6">
				<fm-select url="/api/code_list?grp=S_PURPOSE" id="PURPOSE" emptystr=" " name="PURPOSE" title="<spring:message code="CATEGORY" text="종류"/>">
			</div>
			<div class="col col-sm-6	">
				<fm-select id="ALL_USE_YN" name="ALL_USE_YN"  url="/api/code_list?grp=sys.yn"  title="<spring:message code="NC_VRA_CATALOG_ALL_USE_YN" text="ALL_USE_YN" />"></fm-select>
			</div>
			<div class="col col-sm-6	">
				<fm-select id="OS_TYPE" name="OS_TYPE"  url="/api/code_list?grp=OS_TYPE"  title="<spring:message code="" text="OS 타입" />"></fm-select>
			</div>
			<div class="col col-sm-12" v-if="form_data.ALL_USE_YN=='N'">
				<fm-select2 id="CATEGORY_IDS" keyfield="CATEGORY_ID" titlefield="CATEGORY_NMS"  name="CATEGORY_IDS"  url="/api/code_list?grp=dblist.com.clovirsm.common.Component.listLeafCategory" multiple="true"    title="<spring:message code="TASK" text="업무"/>"></fm-select2>
			</div>
			<div class="col col-sm-12" required="required" v-if="form_data.LINUX_SW_PATH && form_data.LINUX_SW_PATH.indexOf('.yaml')>0">
				<fm-input id="ansiblePlayBook" name="LINUX_SW_PATH" title='Ansible Play Book' disabled="true"  div_style="width: calc(100% - 60px); display:inline-block;" ></fm-input>
				<div style="display:inline-block;">
					<button class="shellBtn" id="shellBtn" onclick="shellEdit($('#ansiblePlayBook') , 'ansiblePlayBook'); " type="button" disabled><spring:message code="EDIT" text="편집"/> </button>
				</div>
			</div>
			<div class="col col-sm-12" required="required" v-if="form_data.LINUX_SW_PATH && form_data.LINUX_SW_PATH.indexOf('.sh')>0 && form_data.OS_TYPE == 'linux' || form_data.OS_TYPE == 'all' ">
				<fm-input id="LINUX_SW_PATH" name="LINUX_SW_PATH" title='Linux shell script' disabled="true"  div_style="width: calc(100% - 60px); display:inline-block;" ></fm-input>
				<div style="display:inline-block;">
					<button class="shellBtn" id="shellBtn" onclick="shellEdit($('#LINUX_SW_PATH') , 'shell'); " type="button"  ><spring:message code="EDIT" text="편집"/> </button>
				</div>
			</div>
			<div class="col col-sm-12" required="required" v-if="form_data.LINUX_SW_PATH && form_data.LINUX_SW_PATH.indexOf('.sh')>0 && form_data.OS_TYPE == 'window' || form_data.OS_TYPE == 'all' ">
				<fm-input id="WIN_SW_PATH" name="WIN_SW_PATH" title='Windows shell script' disabled="true"  div_style="width: calc(100% - 60px); display:inline-block;" ></fm-input>
				<div style="display:inline-block;">
					<button class="shellBtn" id="shellBtn" onclick="shellEdit($('#WIN_SW_PATH') , 'shell'); " type="button"  ><spring:message code="EDIT" text="편집"/> </button>
				</div>
			</div>
			<div class="col col-sm-12" >
				<fm-textarea id="SW_DESC" name="SW_DESC" title="<spring:message code="NC_VRA_CATALOG_CMT" text="설명"/>" label_style="height: 300px;" sty="height: 280px; border: 1px solid #b5b5b5;   padding:10px;"></fm-textarea>
			</div>
			<input type="hidden" name="SW_ID" id="SW_ID">
	</div>
</div>



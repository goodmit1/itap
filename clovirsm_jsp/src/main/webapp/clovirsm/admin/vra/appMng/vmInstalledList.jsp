 <%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="fullGrid" id="input_area" style="height:240px">
	<div class="table_title layout name">
		<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>
		<div class="btn_group">


			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel_snapshot()" class="layout excelBtn" id="excelBtn"></button>
			<button type="button" title="<spring:message code="btn_reload" text="새로고침"/>" onclick="vmInstallSearch(vmInstallReq.SW_ID)" class="contentsBtn tabBtnImg sync"><spring:message code="btn_reload" text="새로고침"/></button>

		</div>
	</div>
	<div id="vmInstallTable" class="ag-theme-fresh" style="height: 200px" ></div>
</div>
<script>
	var vmInstallReq = new Req();
	vmInstallTable;

	$(function() {
		var
		vmInstallColumnDefs = [ {
			headerName : '',
			hide : true,
			field : 'VM_ID'
		},  {
			headerName : '<spring:message code="VM_NAME" text="VM 명" />',
			field : 'VM_NM',
			width: 300
		},{
			headerName : 'IP',
			field : 'PRIVATE_IP',
			width: 110
		},{
			headerName : '<spring:message code="mgr_name" text="담당자" />',
			field : 'USER_NAME',
			width: 130
		},{
			headerName : '<spring:message code="FM_TEAM_TEAM_CD" text="팀" />',
			field : 'TEAM_NM',
			width: 130
		},{
			headerName : 'OS',
			field : 'OS_NM',
			width: 130
		},{
			headerName : 'CPU',
			field : 'CPU_CNT',
			width: 130
		},{
			headerName : 'Memory',
			field : 'RAM_SIZE',
			width: 130
		}
		 ];
		var
		vmInstallGridOptions = {
			hasNo : true,
			columnDefs : vmInstallColumnDefs,
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false
		}
		vmInstallTable = newGrid("vmInstallTable", vmInstallGridOptions);
	});

	// 조회
	function vmInstallSearch(swId) {
		
		
		if(!swId && req.getData())
		{
			 
			sw_Id = req.getData().SW_ID;
		}	
		if(sw_Id){
			 
			vmInstallReq.searchSub('/api/sw/list/list_VM_INSTALL/', { SW_ID: sw_Id }, function(data) {
				vmInstallTable.setData(data);
			});
		}
	}
	function exportExcel_snapshot()
	{
		vmInstallTable.exportCSV({fileName:'SW_install'})
	}

</script>



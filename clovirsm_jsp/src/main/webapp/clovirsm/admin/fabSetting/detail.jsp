<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!-- 입력 Form -->
<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm-6">
			<fm-select url="/api/code_list?grp=FAB" id="FAB" name="ID" title="FAB" required="true">
		</div>
		<div class="col col-sm-5">
			<fm-ibutton id="USER_NAME" name="DC_ID"  required="true" title="<spring:message code="admin" text="관리자" />">
				<fm-popup-button v-show="false" popupid="${popupid}_user_search_popup" popup="/popup/user_search_form_popup.jsp" cmd="update" param="$('#USER_NAME').val()" callback="${popupid}_select_user"  ><spring:message code="admin" text="관리자"/></fm-popup-button>
			</fm-ibutton>
			<input id="USER_ID" name="DC_ID" style="display:none">
		</div>
		<div class=" col col-sm-1">
			<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg save top5" onclick="fabSave();"><spring:message code="btn_save" text="저장"/></fm-sbutton>
		</div>
	</div>
	
	 
</div>

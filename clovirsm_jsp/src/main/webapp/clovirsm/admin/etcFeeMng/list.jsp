<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다  -->
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC"  id="OS_DC_ID" keyfield="DC_ID" titlefield="DC_NM"	name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
		</div>
		<div class="col btn_group nomargin" style="padding-left:5px;">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
			<spring:message code="mainTable_Search_information" text="검색정보" />
			<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
			<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg top save" onclick="save()"><spring:message code="btn_save" text="" /></fm-sbutton>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 450px" ></div>
	</div>
	<br />
</div>
<script>
	var etcFeeReq = new Req();
	mainTable;
	var hostFee = 0;
	var vmFee = 0;
	$(function() {
		var columnDefs = [{headerName : "<spring:message code="NC_ETC_FEE_SVC_NM" text="서비스명" />", field : "SVC_NM"},
		                  {headerName : "<spring:message code="" text="단가" />", format: "number" , field : "MM_FEE", editable: true},
		                  {headerName : "<spring:message code="" text="수량(HOST)" />", format: "number", field : "HOST_CNT"},
		                  {headerName : "<spring:message code="" text="수량(VM)" />", format: "number",field : "VM_CNT"},
		                  {headerName : "<spring:message code="" text="금액(HOST)" />", format: "number", field : "HOST_FEE"},
		                  {headerName : "<spring:message code="" text="금액(VM)" />", format: "number", field : "VM_FEE"}
		                  ]
		var
		gridOptions = {
			hasNo : false,
			columnDefs : columnDefs,
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			pinnedBottomRowData:createFooter(0),
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onCellValueChanged: function(params) {
				var data = mainTable.getData();
				data[params.rowIndex].HOST_FEE = params.newValue * Math.floor(data[params.rowIndex].HOST_CNT);
				data[params.rowIndex].VM_FEE = params.newValue * Math.floor(data[params.rowIndex].VM_CNT);
				mainTable.updateRow(data);
				
				hostFee = 0;
				vmFee = 0;
				for(var i=0; i < data.length; i++)
				{
					hostFee +=nvl(data[i].MM_FEE,0)* Math.floor(data[i].HOST_CNT);
					vmFee += nvl(data[i].MM_FEE,0) * Math.floor(data[i].VM_CNT);
					data[i].HOST_FEE = nvl(data[i].MM_FEE,0) * Math.floor(data[i].HOST_CNT);
					data[i].VM_FEE = nvl(data[i].MM_FEE,0) * Math.floor(data[i].VM_CNT);
				}
				
				mainTable.gridOptions.api.setPinnedBottomRowData(createFooter(hostFee, vmFee));
				
			}
		}
		mainTable = newGrid("mainTable", gridOptions);
		setTimeout(function(){
			search();
		}, 1000);
		 
	});

	// 조회
	function search() {
		etcFeeReq.search('/api/etc_fee_mng/etcDetail', function(data){
			hostFee = 0;
			vmFee = 0;
			for(var i=0; i < data.length; i++)
			{
				hostFee += nvl(data[i].MM_FEE,0) * Math.floor(data[i].HOST_CNT);
				vmFee += nvl(data[i].MM_FEE,0) * Math.floor(data[i].VM_CNT);
				data[i].HOST_FEE = nvl(data[i].MM_FEE,0) * Math.floor(data[i].HOST_CNT);
				data[i].VM_FEE = nvl(data[i].MM_FEE,0) * Math.floor(data[i].VM_CNT);
			}
			mainTable.setData(data)
			mainTable.gridOptions.api.setPinnedBottomRowData(createFooter(hostFee, vmFee));
		
		});
	}


	function save() {
		mainTable.gridOptions.api.stopEditing();
		var arr = mainTable.getData();
		for(var i=0; i<arr.length; i++){
			arr[i].IU = "U";
		}
		var selected_json = JSON.stringify(arr);
		post("/api/etc_fee_mng/save_multi", {"selected_json" : selected_json}, function(){
			alert("<spring:message code="saved" text="저장되었습니다"/>");
			search();
		})
		
		 
	}

	function createFooter(hostFee1, vmFee1)
	{
		var result= [];
		result.push({SVC_NM:'<spring:message code="label_all" text="전체" />', MM_FEE:'', HOST_CNT:'' , VM_CNT:'', HOST_FEE:hostFee1, VM_FEE:vmFee1});
		return result;
	}
	
	// 엑셀 내보내기
	function exportExcel() {
		mainTable.exportCSV({fileName:'fee'})
	 
	}

</script>
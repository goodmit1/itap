<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm">
			<fm-input id="S_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="서버명"/>"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_USER_NAME" name="USER_NAME" title="<spring:message code="NC_VRA_CATALOGREQ_USER_NAME" text="" />"></fm-input>
		</div>
		<div class="col btn_group">
			<input type="button" class="btn searchBtn" onclick="search()" value="<spring:message code="btn_search" text="" />" />
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title">
	<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="amount" text="총 개수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div>
	</div>
	<div id="mainTable" class="ag-theme-fresh" style="height: 450px;" ></div>
	<br />
</div>
<script>
	var notusevmReq = new Req();
	mainTable;

	$(function() {
		var columnDefs = [{headerName : "<spring:message code="NC_VM_VM_NM" text="" />", field : "VM_NM"},
		                  {headerName : "<spring:message code="INS_ID" text="" />", field : "USER_NAME"},
		                  {headerName : "<spring:message code="NC_VM_USER_TEAM_NM" text="" />", field : "TEAM_NM"},
		                  {headerName : "IP",field : "PRIVATE_IP"},
		                  {
		          			headerName : '<spring:message code="NC_VM_CPU_CNT" text="CPU" />',
		          			field : 'CPU_CNT',
		          			maxWidth: 80,
		          			width: 80,
		          			minWidth: 80,
		          			cellStyle:{'text-align':'right'},
		          			valueFormatter:function(params){
		          				return formatNumber(params.value);
		          			}
		          		},{
		          			headerName : '<spring:message code="NV_VM_RAM_CNT" text="MEM" />(GB)',
		          			field : 'RAM_SIZE',
		          			maxWidth: 80,
		          			width: 80,
		          			minWidth: 80,
		          			cellStyle:{'text-align':'right'},
		          			valueFormatter:function(params){
		          				return formatNumber(params.value);
		          			}
		          		},{
		          			headerName : '<spring:message code="NC_VM_DISK_CNT" text="DISK"/>(GB)',
		          			field : 'DISK_SIZE',
		          			maxWidth: 100,
		          			width: 100,
		          			minWidth: 100,
		          			cellStyle:{'text-align':'right'},
		          			valueFormatter:function(params){
		          				return formatNumber(params.value);
		          			}
		          		}, 
		          		{headerName : "<spring:message code="monitor_cpu_usage" text="" />", field : "PREV_CPU_MAX", cellClass:function(params) { return (params.value<cpu_max?'chg1':''); }},
		          		{headerName : "<spring:message code="monitor_mem_usage" text="" />", field : "PREV_MEM_MAX", cellClass:function(params) { return (params.value<mem_max?'chg1':''); }},
		                {headerName : "Disk <spring:message code="monitor_usage" text="" />", field : "DISK_USAGE", cellClass:function(params) { return (params.value<disk_max?'chg1':''); }},
		                  
		                  ]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false
		}
		mainTable = newGrid("mainTable", gridOptions);
		search();
	});
	var cpu_max;
	var mem_max;
	var disk_max;
	// 조회
	function search() {
		notusevmReq.search('/api/not_use_vm/list/over_size/', function(data){
			cpu_max = data.CPU_MAX;
			mem_max = data.MEM_MAX;
			disk_max = data.DISK_MAX;
			mainTable.setData(data.LIST)
		});
	}

	function setGridValues(key, value){
		var arr = mainTable.getSelectedRows();
		for(var i=0; i<arr.length; i++){
			arr[i][key] = value;
		}
//		for(var i in arr){
//		}
	}

	// 엑셀 내보내기
	function exportExcel() {
		mainTable.exportCSV({fileName:'Oversize'})
	}

</script>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 코드 관리 -->
<style>
div.ag-header.ag-pivot-off > div.ag-header-viewport > div > div > div:nth-child(1) > div.ag-cell-label-container.ag-header-cell-sorted-none{
	width: auto;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm" >
			<fm-input id="S_IP" name="IP" title="<spring:message code="NC_IP_IP" text="IP" />"></fm-input>
		</div>
		<div class="col col-sm" >
			<fm-input id="S_RSN" name="RSN" title="<spring:message code="NC_IP_RSN" text="설명" />"></fm-input>
		</div>
		<div class="col btn_group nomargin">
			<button type="button" id="searchBtn" onclick="searchIp()" class="searchBtn btn"><spring:message code="btn_search" text="" /></button>
		</div>
		<div class="col btn_group_under">
			<fm-sbutton cmd="update" class="btn btn-primary newBtn btn btn-primary" onclick="newIp()"><spring:message code="btn_new" text="신규"/></fm-sbutton>
			<fm-sbutton cmd="delete" class="btn btn-primary delBtn btn btn-primary" onclick="delIp()"><spring:message code="btn_delete" text=""/></fm-sbutton>
			<fm-sbutton cmd="update" class="btn btn-primary saveBtn btn btn-primary" onclick="saveIp()"><spring:message code="btn_save" text="저장"/></fm-sbutton>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="amount" text="총 개수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportIp()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportIp()" class="layout excelBtn"></button>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height:450px"></div>
	</div>
	<jsp:include page="detail.jsp"></jsp:include>
</div>
<script>

var ip = new Req();
function exportIp(){
	mainTable.exportCSV({fileName:'ip.csv'})
}
function saveIp(){
	ip.save("/api/ip/save", function(){
		searchIp();
	})
}
function delIp(){
	ip.del("/api/ip/delete", function(){
		searchIp();
	})
}
function newIp(){
	$("#IP").prop("disabled", false)
	$("#IP").focus();
	form_data.IDU = "I";
	form_data.STATUS = "V";
	form_data.IP = "";
	form_data.RSN = "";
}
function searchIp(){
	searchvue.form_data.STATUS = "V";
	if($('#S_IP').val() != null){
		searchvue.form_data.IP = $('#S_IP').val();
	}
	if($('#S_RSN').val() != null){
		searchvue.form_data.RSN = $('#S_RSN').val();
	}
	ip.search('/api/ip/list', function(data){
		$("#IP").prop("disabled", true)
		mainTable.setData(data);
	});
}

var mainTable;

$(document).ready(function(){
	var columnDefs = [{
			headerName: "<spring:message code="NC_IP_RSV_IP" text="IP" />",
			field: "IP",
			maxWidth: 200,
		}, {
			headerName: "<spring:message code="NC_IP_RSN" text="설명" />",
			field: "RSN",
		}
	     ];
	var gridOptions = {
		    columnDefs: columnDefs,
		    rowData: [],
			rowSelection : 'single',
		    enableSorting: true,
		    enableColResize: true,
		    onSelectionChanged : function() {
				var arr = mainTable.getSelectedRows();
				ip.setData(arr[0]);
			}
		};
	mainTable = newGrid("mainTable", gridOptions)

	searchIp();
});
</script>
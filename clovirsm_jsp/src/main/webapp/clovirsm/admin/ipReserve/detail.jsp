<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!-- 입력 Form -->
<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm-6">
			<fm-input id="IP" name="IP" :disabled="true" required="true" title="<spring:message code="NC_IP_IP" text="IP" />" placeholder="<spring:message code="ip_multi_input" text="-로 범위지정가능 예:10.0.1.1-10" />" ></fm-input>
		</div>
		<div class="col col-sm-6">
			<fm-input id="RSN" name="RSN" required="true" title="<spring:message code="NC_IP_RSN" text="설명" />"></fm-input>
		</div>
	</div>
	
	 
</div>

<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  서버 목록 조회 -->
<style>
.plus_btn{
    width: 20px;
    padding: 0px 3px !important;
    font-weight: 100;
    font-size: 10px;
    }
.pic{
    margin-left: 15px;
    font-weight: bold;
    color: #00a832;
   }    
.pic .del{
    display: inline-block;
    overflow: hidden;
    width: 9px;
    height: 9px;
    margin: -2px 10px 0 6px;
    background: url(https://ssl.pstatic.net/imgshopping/static/pc-real-2017/img/search/sp_search_v4.png) no-repeat -180px -50px;
    line-height: 9999px;
    vertical-align: middle;
   }     
#filter_area{
 	margin-left: 50px;
 }  
.bar {
    margin: 0 -4px 0 12px;
    padding: 0;
  }
.clear {
    display: inline-block;
    overflow: hidden;
    margin-left: 15px;
    font-size: 12px;
    color: #6b6b6a;
    vertical-align: middle;
	}   
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col col-sm">
			<fm-input id="S_OS_NM" name="OS_NM" title="<spring:message code="NC_OS_TYPE_OS_NM" text="템플릿명" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_OS_NM" name="CATEGORY_NM" title="<spring:message code="CATEGORY_NM" text="서비스명" />"></fm-input>
		</div>
		 <div class="col col-sm">
			<fm-input id="S_OS_NM" name="CATEGORY_CODE" title="<spring:message code="NC_OS_TYPE_VM_NAMING" text="VM Naming" />"></fm-input>
		</div>
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="ostypeSearch()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
		
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:540px">
	<div class="box_s">
		<div class="table_title layout name">
			<%-- <div style="float:left">
			<spring:message code="count" text="건수"/> : <span id="osGrid_total">0</span>
			</div>
			<div id="filter_area" style="float:left"></div>
			<div class="btn_group">
				<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
			</div> --%>
			<div class="search_info">
				<spring:message code="mainTable_Search_information" text="검색정보" />
				<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="osGrid_total">0</span>&nbsp)</span>
			</div>
			<div class="btn_group">
				<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
				<fm-sbutton id="insert_ostype_button" cmd="update" class="newBtn contentsBtn tabBtnImg new" onclick="newCategory('0')"><spring:message code="btn_new" text="신규"/><spring:message code="NC_OS_TYPE_CATEGORY" text="Service" />1</fm-sbutton>
			</div>
			
		</div>
		<div class="layout background mid">
			<div id="osGrid" class="ag-theme-fresh" style="height: 600px" ></div>
		 </div>
	 </div>
</div>
<script>
	var ostypeReq = new Req();
	osGrid;

	
	
	function allClearFilter()
	{
		osGrid.gridOptions.api.setFilterModel(null);
		osGrid.gridOptions.api.onFilterChanged();
		 $("#filter_area").html(""); 
	}
	function clearFilter(obj, field){
		var filter = osGrid.gridOptions.api.getFilterInstance(field);
		 filter.setFilter("");

		 osGrid.gridOptions.api.onFilterChanged();
		 $(obj).remove();
		 if($("#filter_area .pic").length==0)
		 {
			 $("#filter_area").html(""); 
		 }
	}
	function applyAllFilter()
	{
		 $("#filter_area .pic").each(function(){
			 
			 
			 var filter = osGrid.gridOptions.api.getFilterInstance($(this).attr("data-field"));
			 filter.setFilter($(this).attr("data-val"));

			 osGrid.gridOptions.api.onFilterChanged();
		 })
	}
	function filter(field,text)	{
		var filter = osGrid.gridOptions.api.getFilterInstance(field);
		 filter.setFilter(text);

		 osGrid.gridOptions.api.onFilterChanged();
		 if( $("#filter_area").html()=="") {
			 $("#filter_area").append('<a id="_resetFilter" href="#" class="clear" onclick="allClearFilter();return false"><span class="ico_clear"></span><spring:message code="all_clear" text="전체해제"/></a><span class="bar">|</span>')
		 }
		 $("#filter_area").append("<a class='pic' href='#' data-field='" + field + "' data-val='" + text + "' onclick='clearFilter(this, \"" + field + "\");return false'>" + text + "<span class='del'>Clear</span></a>");
	}
	var columnDefs;
	$(function() {
		columnDefs = [{headerName : "<spring:message code="NC_OS_TYPE_VM_NAMING" text="VM Naming" />", field : "CATEGORY_CODE", editable : true },
			          {headerName : "<spring:message code="NC_OS_P_KUBUN_P" text="운영"/>", field : "P_OS_NM", cellRenderer:function(params) {
			                	  	return addTmplBtn(params.data,'P', params.rowIndex);
			                }},
		              {headerName : "<spring:message code="NC_OS_P_KUBUN_D" text="개발" />", field : "D_OS_NM", cellRenderer:function(params) {
		                	  		return addTmplBtn(params.data,'D', params.rowIndex);
	                	  	}}
		                
		              ]
		var
		gridOptions = {
			hasNo : false,
			columnDefs : columnDefs,
			
			//rowModelType: 'infinite',
			//rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [] ,
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onRowClicked: function(){
				var arr = osGrid.getSelectedRows();
				 
			},
			onCellClicked : function(params){
				
					if("A"==params.event.srcElement.tagName) return ;
					if(params.colDef.field.indexOf("NAME")<0 || params.value=="" || !params.value) return;
					filter(params.colDef.field, params.value)
			},
			onCellValueChanged: function(params) {
				if(params.newValue != params.oldValue)
				{
					if("CATEGORY_CODE" == params.colDef.field)
					{
						
						var obj = {CATEGORY_ID:params.data.CATEGORY_ID, CATEGORY_CODE:params.newValue}
						 
						post("/api/popup/category/save", obj, function(data){
							//var changedData = [params.data];
							//osGrid.gridOptions.api.updateRowData({update: changedData});
							//osGrid.gridOptions.api.refreshCells()
							osGrid.gridOptions.api.redrawRows()
						});	
					}	
				}	
		       
		    },
		}
		osGrid = newGrid("osGrid", gridOptions);
		ostypeSearch();
	});

	function addCategoryBtn(params )
	{
		var data = params.data;
		var name = params.value
		var field = params.colDef.field;
		var pos = field.indexOf("_");
		var idx = field.substring(pos+1);
		  
		if(name && name != '')
		{
			return "<a href=\"#\" onclick=\"updateCategory('" + data["ID_" + idx] + "','" + name + "');return false\">" + name + '</a>';	
		}
		else if(!isEmpty(data["NAME_" + (idx-1)]) && isEmpty(data.CATEGORY_CODE) &&  isEmpty(data.P_OS_ID) && isEmpty(data.D_OS_ID)  )
		{
			
			return '<button type="button" class="btn btn-primary plus_btn" onclick="newCategory(' + data["ID_" + (idx-1)] + ')" title="New Category"><i class="fa fa-plus"></i></button>'
		}
	}
	function updateCategory(id, name) 
	{
		 
		var param = {CATEGORY_ID:id, CATEGORY_NM:name }
		openCategoryPopup(param);
	}
	function newCategory(parent) 
	{
		var param = {PAR_CATEGORY_ID:parent}
		openCategoryPopup(param);
	}
	function openCategoryPopup(param)
	{
		category_edit_popup_click(param)
		$("#category_edit_popup").modal();
	}
	function addTmplBtn(data, kubun, rowIdx){
		if(! data[kubun + "_OS_NM"]  ){
			return '<button type="button" class="btn btn-primary plus_btn" data-P_KUBUN="' + kubun + '" onclick="newTemplate(this,' + data.CATEGORY_ID  + ')" title="New Template"><i class="fa fa-link"></i></button>'
		}
		else{
			return '<a href="#" data-P_KUBUN="' + kubun + '" onclick="updateTemplate(this,' + rowIdx + ');return false">' + data[kubun + "_OS_NM"] + '</a>'; 	
		}
	}
	function newTemplate(obj, parent) 
	{
		var param = {CATEGORY:parent, P_KUBUN:$(obj).attr("data-P_KUBUN")}
		openTemplatePopup(param);
	}
	function updateTemplate(obj,rowIdx) 
	{
	 
		var param = osGrid.getRowDataById(rowIdx);
		param.P_KUBUN= $(obj).attr("data-P_KUBUN");
		param.OS_ID=param[param.P_KUBUN + "_OS_ID"];
		param.OS_NM=param[param.P_KUBUN + "_OS_NM"];
		param.DC_ID=param[param.P_KUBUN + "_DC_ID"];
		param.CMT=param[param.P_KUBUN + "_CMT"];
		openTemplatePopup(param);
	}
	function openTemplatePopup(param)
	{
		template_edit_popup_click(param)
		$("#template_edit_popup").modal();
	}
	// 조회
	function ostypeSearch() {
		ostypeReq.search('/api/ostype/list_include_category' , function(data) {
			 var maxLevel = data.maxLevel;
			 var colArr = [];
			 for(var i=0; i < maxLevel; i++) {
				 colArr.push({	
					 headerName : "<spring:message code="NC_OS_TYPE_CATEGORY" text="Service" />" + (i+1),   
				 	field : "NAME_" + i,  
				 	cellRenderer:function(params) {
             	  		return addCategoryBtn(params  );
	                }})
			 }
			 
			 osGrid.gridOptions.api.setColumnDefs($.merge(colArr, columnDefs));
			 osGrid.setData(data);
			 applyAllFilter();
			 
		});
		
	}
	/*
	function select_S_CATEGORY(data){
		searchvue.form_data.CATEGORY = data.CATEGORY;
		searchvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
		$("#S_CATEGORY_NM").val(data.CATEGORY_NM);
	}
	function select_CATEGORY(data){
		inputvue.form_data.CATEGORY = data.CATEGORY;
		inputvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
		$("#CATEGORY_NM").val(data.CATEGORY_NM);
	}
	function onRowClick(data){
		if(data.APPR_STATUS_CD || data.CUD_CD){
			setButtonClickable('save_ostype_button', false);
			setButtonClickable('delete_ostype_button', false);
			setInputEnable('CMT', false);
		} else {
			setButtonClickable('save_ostype_button', true);
			setButtonClickable('delete_ostype_button', true);
			setInputEnable('CMT', true);
		}
	}

	//저장
	function ostypeSave(){

			ostypeReq.save('/api/ostype/save', function(data){
				mappingTable.stopEditing();
				post('/api/ostype/save_mapping', {OS_ID: data.OS_ID, LIST: JSON.stringify(mappingTable.getData())}, function(){
					ostypeSearch();
					alert(msg_complete);
				})
			});

	}

	function insertOstype(){
		form_data.CMT      = '';
		form_data.DD_FEE   = '';
		form_data.DEL_YN   = '';
		form_data.HH_FEE   = '';
		form_data.INS_ID   = '';
		form_data.INS_IP   = '';
		form_data.INS_PGM  = '';
		form_data.INS_TMS  = '';
		form_data.KUBUN    = '';
		form_data.LINUX_YN = '';
		form_data.MM_FEE   = 0;
		form_data.OS_ID    = '';
		form_data.OS_NM    = '';
		form_data.PART_YN  = '';
		form_data.UPD_ID   = '';
		form_data.UPD_IP   = '';
		form_data.UPD_PGM  = '';
		form_data.UPD_TMS  = '';
		form_data.USER_NM  = '';
		form_data.IDU  = 'I';
		mappingSearch('');
		$("#tab1_btn").click();
	}
	function deleteOstype(){
		if(form_data.OS_ID && form_data.OS_ID != null){
			ostypeReq.del('/api/ostype/delete', function(){
				ostypeSearch();
				alert(msg_complete);
			});
		} else {
			alert(msg_select_first);
		}
	}
	*/
	// 엑셀 내보내기
	function exportExcel(){
		osGrid.exportCSV({fileName:'Template'});
		//exportExcelServer("mainForm", '/api/ostype/list_excel', 'OS_TYPE',osGrid.gridOptions.columnDefs, ostypeReq.getRunSearchData())
	}

</script>



<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%
String DC_ID = request.getParameter("DC_ID");
request.setAttribute("DC_ID", DC_ID == null ? "null" : "'" + DC_ID + "'");
%>
<style>
#mainTable { height:calc(100vh - 220px); }
#subTable{
	border-bottom: 1px solid #626262;
	margin-bottom: 20px;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->
		<%-- <div class="col col-sm">
			<fm-input id="LICENSE_KEY" name="LICENSE_KEY" title="<spring:message code="NC_LICENSE_LICENSE_KEY" text="" />"></fm-input>
		</div> --%>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="DC_ID"
				keyfield="DC_ID" titlefield="DC_NM" emptystr=""
				name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>"></fm-select>
		</div>
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text=""/></fm-sbutton>
		</div>
		<div class="col btn_group_under">
			<fm-sbutton cmd="search" class="exeBtn contentsBtn tabBtnImg collection" onclick="sync()"  ><spring:message code="btn_sync" text="동기화"/></fm-sbutton>
		</div>
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
		<div class="search_info">
			<spring:message code="mainTable_Search_information" text="검색정보" />
			<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
			<%-- <fm-sbutton cmd="search" class="exeBtn contentsBtn tabBtnImg sync" onclick="sync()"  ><spring:message code="btn_sync" text="동기화"/></fm-sbutton> --%>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 400px" ></div>
	</div>
	</div>
	<div class="box_s">
	<div class="table_title layout name"><spring:message code="subTable_use_host" text="사용 HOST" /></div><jsp:include page="detail.jsp"></jsp:include>
	</div>
</div>
<script>
var req = new Req();
var mainTable;
var license_total = 0;
var license_used = 0;
var license_left = 0;


$(function() {


	var columnDefs = [{headerName : "<spring:message code="NC_LICENSE_LICENSE_KEY" text="" />",field : "LICENSE_KEY"},
		{headerName : "<spring:message code="NC_LICENSE_LICENSE_NAME" text="" />",field : "LICENSE_NAME", width:300, valueGetter:function(params){
			return params.data.LICENSE_NAME + (params.data.COST_UNIT ? "(" +  params.data.COST_UNIT   + ")" :""); 
		}},
		{headerName : "<spring:message code="NC_LICENSE_LICENSE_TOTAL" text="LICENSE_TOTAL" />", cellStyle:{'text-align':'right'}, width:100,valueFormatter:function(params){
			return formatNumber(params.value);
		}
		,field : "LICENSE_TOTAL"},
		{headerName : "<spring:message code="NC_LICENSE_LICENSE_USED" text="LICENSE_USED" />",width:100, cellStyle:{'text-align':'right'}, valueFormatter:function(params){
			return formatNumber(params.value);
		}
		,field : "LICENSE_USED"},
		{headerName : "<spring:message code="NC_LICENSE_LICENSE_LEFT" text="" />",width:100, cellStyle:{'text-align':'right'}, valueFormatter:function(params){
			return formatNumber(params.value);
		}
		,field : "LICENSE_LEFT"},
		{headerName : "<spring:message code="NC_DC_DC_ID" text="" />",width:100, field : "DC_NM"}
		]
	var
	gridOptions = {
		hasNo : true,
		columnDefs : columnDefs,
		//rowModelType: 'infinite',
		pinnedBottomRowData:createFooter(0),
		rowSelection : 'single',
		sizeColumnsToFit: true,
		cacheBlockSize: 100,
		rowData : [],
		enableSorting : true,
		enableColResize : true,
		enableServerSideSorting: false,
		onSelectionChanged : function() {
			var arr = mainTable.getSelectedRows();
			subSearch(arr[0].LICENSE_KEY)
		},
	}
	mainTable = newGrid("mainTable", gridOptions);

	setTimeout(function() {
		if(${DC_ID}){
			$("#DC_ID").val(${DC_ID});
			searchvue.form_data.DC_ID = ${DC_ID};
		}
		search();
	}, 1000);
});
function initFormData() {

}
//동기화
function sync() {
	post('/api/license/sync',{}, function(data){
		alert(msg_complete);
		search() ;
	} )
}
// 조회
function search() {
	req.search('/api/license/list' , function(data) {
		 
		mainTable.setData(data);
		license_total = 0;
		license_used = 0;
		license_left = 0;
		for(var i=0; i < data.list.length; i++)
		{
			if(data.list[i].LICENSE_TOTAL != null && data.list[i].LICENSE_TOTAL != "")
				license_total += data.list[i].LICENSE_TOTAL;
			if(data.list[i].LICENSE_USED != null && data.list[i].LICENSE_USED != "")
				license_used += data.list[i].LICENSE_USED;
			if(data.list[i].LICENSE_LEFT != null && data.list[i].LICENSE_LEFT != "")
				license_left += data.list[i].LICENSE_LEFT;
		}

		mainTable.gridOptions.api.setPinnedBottomRowData(createFooter(license_total,license_used,license_left));
	});
}
function createFooter(license_total,license_used,license_left,dc_nm) {
	var result= [];
	result.push({LICENSE_KEY:'<spring:message code="total_count" text="전체" />', LICENSE_NAME:'',LICENSE_TOTAL: license_total, LICENSE_USED: license_used,LICENSE_LEFT: license_left,DC_NM:''});
	return result;
}
// 엑셀 내보내기
function exportExcel() {
	mainTable.exportCSV({fileName:'license'})
	   
}

</script>



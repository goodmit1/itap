<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<layout:extends name="base/index">
    <layout:put block="content">
    	<form id="mainForm"  >
   			<fmtags:include page="list.jsp" />
    	</form>
    </layout:put>
    <layout:put block="popup_area"> 
	</layout:put>
</layout:extends>
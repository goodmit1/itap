<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/index">
    <layout:put block="content">
    	<form id="mainForm"  >
    	<jsp:include page="list.jsp"></jsp:include>
    	</form>
    </layout:put>
    <layout:put block="popup_area">
    <jsp:include page="/clovirsm/popup/img_category_search_form_popup.jsp"></jsp:include>
    <script>
    	var popup_category = newPopup("CATEGORY_NM", "category_popup");
    	popup_category.setMapping({"CATEGORY_NM":"CATEGORY_NM", "CATEGORY":"CATEGORY"});

    	var popup_category_edit = newPopup("CATEGORY_NM_EDIT_button", "category_popup");
    	popup_category_edit.setMapping({"CATEGORY_NM":"CATEGORY_NM", "CATEGORY":"CATEGORY"});

    	var popup_category_search = newPopup("S_CATEGORY_NM", "category_popup");
    	popup_category_search.setMapping({"CATEGORY_NM":"CATEGORY_NM", "CATEGORY":"CATEGORY"});
   	</script>
	</layout:put>
</layout:extends>
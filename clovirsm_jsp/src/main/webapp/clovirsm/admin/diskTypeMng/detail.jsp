<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 입력 Form -->
<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm-4">
			<fm-input id="DISK_TYPE_NM"  name="DISK_TYPE_NM" required="true" title="<spring:message code="NC_DISK_TYPE_DISK_TYPE_NM" text="디스크 타입" />"></fm-input>
			<input type="text" style="display:none" id="DISK_TYPE_ID" name="DISK_TYPE_ID" :value="form_data.DISK_TYPE_ID">
		</div>
		<div class="col col-sm-4">
			<fm-input id="DD_FEE" required="true" name="DD_FEE" title="<spring:message code="NC_DISK_TYPE_DD_FEE" text="" />"></fm-input>
		</div>
		<div class="col col-sm-4">
			<fm-select-yn id="DEL_YN"
				name="DEL_YN" title="<spring:message code="NC_DISK_TYPE_DEL_YN" text="삭제 여부"/>">
			</fm-select>
		</div>
		<div class="col col-sm-4">
			<fm-output id="USER_NM" name="USER_NM" title="<spring:message code="FM_USER_USER_NAME" text="사용자" />"  ></fm-output>
		</div>
		<div class="col col-sm-4">
			<fm-output id="INS_TMS" name="INS_TMS" :value="formatDate(form_data.INS_TMS,'datetime')" title="<spring:message code="NC_DISK_TYPE_INS_TMS" text="등록일시" />"  ></fm-output>
		</div>
	</div>
	<script>
	</script>
</div>
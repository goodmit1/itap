<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<div class="table_title">
				<spring:message code="NC_DC_OS_TYPE_TITLE" text="표준 템플릿"/> <spring:message code="count" text="건수"/> : <span id="osTable_total">0</span>
				<div class="btn_group">
				 		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel(osTable,'Template')" class="btn"><i class="fa fa-file-excel-o"></i></button>
				</div>

	</div>

	<div id="osTable" class="ag-theme-fresh" style="height: 200px" ></div>
	<script>
		var osTable;

		function getOSData(param)
		{
			post('/api/dc_mng/list/list_NC_DC_OS_TYPE/', param, function(data){
				osTable.setData(data);
			})
		}
		$(function() {


			var columnDefs = [{headerName : "<spring:message code="NC_OS_TYPE_OS_NM" text="" />",field : "OS_NM"},
				{headerName : "<spring:message code="NC_DC_OS_TYPE_TMPL_PATH" text="경로" />",field : "TMPL_PATH", editable:true},

				];
			var
			gridOptions = {

				editable:true,
				hasNo : true,
				columnDefs : columnDefs,
				//rowModelType: 'infinite',
				//rowSelection : 'single',
				sizeColumnsToFit: false,
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				enableColResize : true,
				enableServerSideSorting: false
			}
			osTable = newGrid("osTable", gridOptions);


		});
	</script>
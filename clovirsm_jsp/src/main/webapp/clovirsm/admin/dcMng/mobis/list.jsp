<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<style>
#mainTable { height:calc(100vh - 220px); }
#input_area .value-title {
    min-width: 150px;
    max-width: none;
    width: auto;
    text-align: right;
}
#input_area .form-control, #input_area .output {
	    width: calc(100% - 250px);
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->


			<div class="col col-sm">
					<fm-select url="/api/code_list?grp=HYPERVISOR" id="S_HV_CD" emptystr=" "
								name="HV_CD" title="<spring:message code="NC_DC_HV_CD" text=""/>"></fm-select>
			</div>


			<div class="col col-sm">
					<fm-input id="S_DC_NM" name="DC_NM" title="<spring:message code="name" text="명칭" />"></fm-input>
			</div>
		<div class="col btn_group">

			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text=""/></fm-sbutton>
		</div>
		<div class="col btn_group_under">

			<fm-sbutton class="detailBtn" cmd="update" class="exeBtn" onclick="collect()"   ><spring:message code="btn_COLLECT" text="수집"/></fm-sbutton>
			
			<fm-popup-button popupid="new_dc" popup="/clovirsm/admin/dcMng/newDC.jsp" cmd="update" param="" callback="onAfterNew"><spring:message code="btn_new" text=""/></fm-popup-button>
			
			
			<fm-sbutton cmd="delete" class="delBtn detailBtn" onclick="del()"   ><spring:message code="btn_del" text=""/></fm-sbutton>
			<fm-sbutton cmd="update" class="saveBtn detailBtn" onclick="save()"   ><spring:message code="btn_save" text="저장"/></fm-sbutton>
		</div>
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div class="table_title">
				<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
				<div class="btn_group">
				 		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel(mainTable,'DC')" class="excelBtn" id="excelBtn"></button>
				</div>

	</div>

	<div id="mainTable" class="ag-theme-fresh" style="height: 200px" ></div>
	<jsp:include page="detail.jsp"></jsp:include>
	
	<div class="col-sm-6">
		<jsp:include page="disk_list.jsp"></jsp:include>
	</div>
	<div class="col-sm-6">
		<jsp:include page="prop_list.jsp"></jsp:include>
	</div>
	<div class="col-sm-6">
		<jsp:include page="etc_list.jsp"></jsp:include>
	</div>
</div>
<script>
					var req = new Req();
					var mainTable;
					var objList={ };
					var dcSelRow={}
					$(function() {
						
						$(".detailBtn").prop("disabled", true);
						var columnDefs = [{headerName : "<spring:message code="id" text="ID" />",field : "DC_ID", width:120},
							{headerName : "<spring:message code="name" text="명칭" />",field : "DC_NM"},
							{headerName : "<spring:message code="NC_DC_HV_CD" text="" />",field : "HV_CD_NM", width:100},
							{headerName : "<spring:message code="NC_DC_DEL_YN" text="" />",field : "DEL_YN_NM", width:80},
							{headerName : "<spring:message code="NC_DC_CONN_URL" text="" />",field : "CONN_URL"},
							/* {headerName : "<spring:message code="NC_DC_START_IP" text="" />",field : "START_IP"}, */
							{headerName : "<spring:message code="NC_DC_CMT" text="" />",field : "CMT"},
							]
						var
						gridOptions = {
							hasNo : true,
							columnDefs : columnDefs,
							//rowModelType: 'infinite',
							rowSelection : 'single',
							sizeColumnsToFit: true,
							cacheBlockSize: 100,
							rowData : [],
							enableSorting : true,
							enableColResize : true,
							 enableServerSideSorting: false,
							 onSelectionChanged : function() {
									var arr = mainTable.getSelectedRows();
									goDetail(arr[0]);

								},
						}
						mainTable = newGrid("mainTable", gridOptions);

						search();
					});
					function initFormData()
					{

					}
					function onAfterNew(data)
					{
						search();
						goDetail(data);
					}
					
					function checkDataDisabled() //data-disabled 여부에 따라 버튼 활성화 여부 체크
					{
						var el = document.getElementsByClassName('detailBtn')
						for(var i=0; i<el.length;i++)
						{
							if(el[i].getAttribute('data-disabled') === 'true'){
								el[i].disabled=true;
							}
						}
					}
					function goDetail(data)
					{
						$(".detailBtn").prop("disabled", false);
						checkDataDisabled();
						dcSelRow = data;
						$.get('/api/dc_mng/list/list_NC_HV_OBJ/?DC_ID=' + data.DC_ID, function(list){
							objList={ };
							for(var i=0; i < list.length;i++)
							{
								var obj = objList[list[i].OBJ_TYPE_NM];
								if(!obj)
								{
									obj = [];
									objList[list[i].OBJ_TYPE_NM] = obj;
								}
								try
								{
									obj.push(list[i].OBJ_NM);
								}
								catch(e)
								{}
							}

						});
						req.getInfo('/api/dc_mng/info?DC_ID=' + data.DC_ID, function(data){

								});

						//getOSData(arr[0]);
						getDiskData(data);
						getPropData(data);
						getEtcData(data);
					}
					 
					function insert()
					{
						req.setData({DEL_YN:'N', HV_CD:'V'});
						getPropData({HV_CD:'V'});
					}
					// 삭제
					function del() {
						req.del('/api/dc_mng/delete' , function(data) {
							search();
						});
					}
					// 조회
					function search() {


						req.search('/api/dc_mng/list' , function(data) {
							mainTable.setData(data);
						});
					}
					// 수집
					function collect() {
						if(!form_data.DC_ID)
						{
							alert(msg_select_first);
							return false;
						}
						post('/api/dc_mng/collect/' + form_data.DC_ID , {}, function(){
							alert(msg_complete_work);
						})
					}

					// 저장
					function save() {
						if(!validate("inputForm")) return false;
						if(!diskTable.chkValue()) return;
						//osTable.stopEditing();
						diskTable.stopEditing();
						propTable.stopEditing();
						etcTable.stopEditing();
						setTimeout(function()
						{
							//form_data.NC_DC_OS_TYPE = JSON.stringify( osTable.getSelectedRows());
							form_data.NC_DC_DISK_TYPE = JSON.stringify( diskTable.getSelectedRows());
							form_data.NC_DC_PROP = JSON.stringify( propTable.getSelectedRows());
							form_data.NC_DC_ETC_CONN = JSON.stringify( etcTable.getSelectedRows());
							req.save('/api/dc_mng/save', function(){
								search();
								clearTableData();
							});
						},100);
					}


					function clearTableData(){
						diskTable.clearData();
						propTable.getSelectedRows();
						etcTable.getSelectedRows();
					}
					// 엑셀 내보내기
					function exportExcel(table, fileName)
					{
						table.exportCSV({fileName:fileName});
					}

				</script>



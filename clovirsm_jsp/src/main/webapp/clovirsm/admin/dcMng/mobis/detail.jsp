<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- 입력 Form -->

	<div class="form-panel detail-panel detail-resize panel panel-default">

			<div class="panel-body" id="inputForm">



					<div class="col col-sm-4">
						<fm-input id="DC_NM" name="DC_NM" required="true"  title="<spring:message code="NC_DC_DC_NM" text="데이터 센터" />"></fm-input>
					</div>
					<div class="col col-sm-4">
						<fm-output name="HV_CD_NM"  title="<spring:message code="NC_DC_HV_CD" text="Hypervisor코드"/>"></fm-output>
						 
					</div>
					<div class="col col-sm-4">
						<fm-select url="/api/code_list?grp=sys.yn" id="DEL_YN"
								name="DEL_YN" title="<spring:message code="NC_DC_DEL_YN" text="삭제 여부"/>" tooltip="<spring:message code="DEL_YN_EX" text="사용자 화면에 표시여부 '예'일시 표시 X"/>"></fm-select>
					</div>
					<div class="col col-sm-4">
						<fm-input id="CONN_URL" name="CONN_URL" required="true"  title="<spring:message code="NC_DC_CONN_URL" text="CONN_URL" />"></fm-input>
					</div>
					<div class="col col-sm-4">
						<fm-input id="CONN_USERID" name="CONN_USERID" required="true"  title="<spring:message code="NC_DC_CONN_USERID" text="CONN_USERID" />"></fm-input>
					</div>
					<div class="col col-sm-4">
						<fm-password id="CONN_PWD" name="CONN_PWD"  title="<spring:message code="NC_DC_CONN_PWD" text="CONN_PWD" />"></fm-password>
					</div>
					<div class="col col-sm-4">
						<fm-input id="REAL_DC_NM" name="REAL_DC_NM" required="true"  title="<spring:message code="NC_DC_REAL_DC_NM" text="REAL_DC_NM" />" tooltip="<spring:message code="vcenter_dc_nm" text="vCenter에 등록된 데이터 센터명" />"></fm-input>
					</div>
					<div class="col col-sm-4">
						<fm-select url="/api/code_list?grp=db.com.clovirsm.common.Component.selectDiskType" id="IMG_DISK_TYPE_ID" name="IMG_DISK_TYPE_ID" required="true"  title="<spring:message code="NC_DC_IMG_DISK_TYPE_ID" text="템플릿 디스크종류" />"></fm-select>
					</div>
					<div class="col col-sm-4">
						<fm-input id="INIT_POOL_SIZE" name="INIT_POOL_SIZE" title="<spring:message code="NC_DC_CONNECTION_COUNT" text="초기 연결 갯수" />" tooltip="초기 Connection  Pool 갯수"></fm-input>
					</div>
					<div class="col col-sm-6" style="height: 80px; width:100%">
						<fm-textarea id="CMT" name="CMT"   title="<spring:message code="NC_DC_CMT" text="CMT" />"></fm-input>
					</div>

	</div>

</div>
<script>
	$(document).ready(function(){

	})
</script>

<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<div class="table_title layout name">
				<spring:message code="NC_DC_ETC_CONN__TABLE_TITLE__" text="기타"/> <span> ( <spring:message code="count" text="건수"/> : <span id="diskTable_total">0</span> ) </span>
				<div class="btn_group">
				 		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel(etcTable,'Connection')" class="excelBtn layout" id="excelBtn"></button>
				</div>

	</div>

	<div id="etcTable" class="ag-theme-fresh" style="height: 200px" ></div>
	<script>
		var osTable;

		function getEtcData(param)
		{
			post('/api/dc_mng/list/list_NC_DC_ETC_CONN/', param, function(data){
				etcTable.setData(data);
			})
		}
		$(function() {


			var columnDefs = [{headerName : "<spring:message code="NC_DC_ETC_CONN_CONN_TYPE" text="" />",field : "CONN_TYPE"},
				{headerName : "<spring:message code="NC_DC_ETC_CONN_CONN_URL" text="경로" />",field : "CONN_URL", editable:true},
				{headerName : "<spring:message code="NC_DC_ETC_CONN_CONN_USERID" text="" />",field : "CONN_USERID", editable:true},
				{headerName : "<spring:message code="NC_DC_ETC_CONN_CONN_PWD" text="" />",field : "CONN_PWD", editable:true, valueFormatter:function(params){
					return "**";
				}},
				{headerName : "<spring:message code="NC_DC_ETC_CONN_CONN_PROP" text="" />",field : "CONN_PROP", editable:true},
				];
			var
			gridOptions = {

				editable:true,
				hasNo : true,
				columnDefs : columnDefs,
				//rowModelType: 'infinite',
				//rowSelection : 'single',
				sizeColumnsToFit: false,
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				enableColResize : true,
				enableServerSideSorting: false
			}
			etcTable = newGrid("etcTable", gridOptions);


		});
	</script>
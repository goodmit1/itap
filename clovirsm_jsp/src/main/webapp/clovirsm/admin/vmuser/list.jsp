<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm">
			<fm-select url="/api/etc/team_list" id="S_TEAM_CD" emptystr=" " name="TEAM_CD" title="<spring:message code="NC_VM_TEAM_NM" text="팀" />"></fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_SERVER_IP" name="SERVER_IP" title="<spring:message code="NC_FW_PUBLIC_IP" text="" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="서버명"/>"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_TASK_STATUS_CD" id="S_TASK_STATUS_CD"
				keyfield="TASK_STATUS_CD" titlefield="TASK_STATUS_CD_NM" emptystr=" "
				name="TASK_STATUS_CD" title="<spring:message code="NC_VM_USER_TASK_STATUS_CD" text="작업상태"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_USER_NAME" name="USER_NAME" title="<spring:message code="FM_USER_USER_NAME" text="사용자" />"></fm-input>
		</div>
		<div class="col btn_group">
			<input type="button" class="btn searchBtn" onclick="search()" value="<spring:message code="btn_search" text="" />" />
		</div>
		<div class="col btn_group_under">
			<fm-sbutton cmd="update" onclick="complete()"><spring:message code="btn_task_yes" text="" /></fm-sbutton>
			<fm-popup-button id="fw_vmuser_cancel_form_popup_button" popupid="fw_vmuser_cancel_form_popup"
				popup="/clovirsm/popup/fw_vmuser_cancel_form_popup.jsp" cmd="update" param="'message'"
				callback="">
				<spring:message code="label_work_msg" text="작업메시지"/>
			</fm-popup-button>
			<fm-popup-button id="fw_vmuser_cancel_form_popup_button" popupid="fw_vmuser_cancel_form_popup"
				popup="/clovirsm/popup/fw_vmuser_cancel_form_popup.jsp" cmd="update" param="'cancel'"
				callback="">
				<spring:message code="btn_task_no" text="작업거부"/>
			</fm-popup-button>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title">
		<spring:message code="amount" text="총 개수"/> : <span id="mainTable_total">0</span>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div>
	</div>
	<div id="mainTable" class="ag-theme-fresh" style="height: 600px" ></div>
	<br />
</div>
<script>
	var vmuserReq = new Req();
	mainTable;

	$(function() {
		var columnDefs = [{headerName : "<spring:message code="NC_FW_SERVER_IP" text="" />", field : "SERVER_IP"},
		                  {headerName : "<spring:message code="NC_VM_VM_NM" text="" />", field : "VM_NM"},
		                  {headerName : "<spring:message code="NC_FW_USER_IP" text="" />", field : "USER_IP"},
		                  {headerName : "<spring:message code="NC_FW_PORT" text="" />", field : "PORT"},
		                  {headerName : "<spring:message code="FM_TEAM_TEAM_NM" text="" />", field : "TEAM_NM"},
		                  {headerName : "<spring:message code="INS_ID" text="" />", field : "USER_NAME"},
		                  {headerName : "<spring:message code="REQ_TMS" text="" />", field : "INS_TMS",
		                		valueGetter: function(params) {
	                			  if(params.data && params.data.INS_TMS){
	                				  if(params.data.UPD_TMS){
							    		  return formatDate(params.data.UPD_TMS,'datetime')
	                				  }
						    		  return formatDate(params.data.INS_TMS,'datetime')
	                			  }
	                			  return "";
						  }},
		                  {headerName : "<spring:message code="NC_FW_TASK_STATUS_CD" text="" />", field : "TASK_STATUS_CD_NM"},
		                  ]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowSelection : 'multiple',
			editable: true,
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false
		}
		mainTable = newGrid("mainTable", gridOptions);
		searchvue.form_data.TASK_STATUS_CD = "W";
		search();
	});

	// 조회
	function search() {
		vmuserReq.search('/api/vm_user_mng/list', function(data){mainTable.setData(data)});
	}

	function cancel(msg, callback){
		if(confirm(msg_confirm_save)){
			setGridValues("FAIL_MSG", msg);
			setGridValues("TASK_STATUS_CD", "D");
			vmuserReq.saveGrid(mainTable, "/api/vm_user_mng/update", function(){
				search();
				callback();
			})
		}
	}

	function message(msg, callback){
		if(confirm(msg_confirm_save)){
			setGridValues("FAIL_MSG", msg);
			vmuserReq.saveGrid(mainTable, "/api/vm_user_mng/update", function(){
				search();
				callback();
			})
		}
	}

	function complete(){
		if(mainTable.getSelectedRows()[0]){
			if(confirm(msg_confirm_save)){
				setGridValues("TASK_STATUS_CD", "S");
				vmuserReq.saveGrid(mainTable, "/api/vm_user_mng/update", function(){
					search();
				})
			}
		}else{
			alert(msg_select_first)
		}
	}

	function setGridValues(key, value){
		var arr = mainTable.getSelectedRows();
		for(var i=0; i<arr.length; i++){
			arr[i][key] = value;
		}
//		for(var i in arr){
//		}
	}

	// 엑셀 내보내기
	function exportExcel() {
		 mainTable.exportCSV({fileName:'VMUser'})
		 // exportExcelServer("mainForm", '/api/vm_user_mng/list_excel', 'vmuser',mainTable.gridOptions.columnDefs, vmuserReq.getRunSearchData())
	}

</script>
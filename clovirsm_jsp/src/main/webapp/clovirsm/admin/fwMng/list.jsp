<%@page contentType="text/html; charset=UTF-8"%><%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm">
			<fm-select url="/api/etc/team_list" id="S_TEAM_CD" emptystr=" " name="TEAM_CD" title="<spring:message code="NC_VM_TEAM_NM" text="팀" />"></fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_SERVER_IP" name="SERVER_IP" title="<spring:message code="NC_FW_PUBLIC_IP" text="" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="서버명"/>"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_TASK_STATUS_CD" id="S_TASK_STATUS_CD"
				keyfield="TASK_STATUS_CD" titlefield="TASK_STATUS_CD_NM" emptystr=" "
				name="TASK_STATUS_CD" title="<spring:message code="NC_VM_USER_TASK_STATUS_CD" text="작업상태"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_USER_NAME" name="USER_NAME" title="<spring:message code="FM_USER_USER_NAME" text="사용자" />"></fm-input>
		</div>
		<div class="col btn_group nomargin">
			<button type="button" class="btn searchBtn" onclick="search()"><spring:message code="btn_search" text="" /></button>
		</div>
<!-- 		<div class="col btn_group_under"> -->
			<!-- <fm-sbutton cmd="update" class="exeBtn" onclick="createNewUser()"><spring:message code="btn_task_create_account" text="계정작업" /></fm-sbutton>
			 -->
			 
<%-- 			<c:if test="${'Y'.equals(sessionScope.FW_API_YN)}"> --%>
<%-- 			<fm-sbutton cmd="update" class="newBtn" onclick="deployFW()"><spring:message code="btn_task_fw_auto" text="자동작업" /></fm-sbutton> --%>
<%-- 			</c:if> --%>
<%-- 			<fm-sbutton cmd="update" class="newBtn" onclick="complete()"><spring:message code="btn_task_yes" text="" /></fm-sbutton> --%>
<!-- 			<fm-popup-button  popupid="fw_vmuser_cancel_form_popup" -->
<!-- 				popup="/clovirsm/popup/fw_vmuser_cancel_form_popup.jsp" cmd="update" class="exeBtn" param="'message'" -->
<!-- 				callback=""> -->
<%-- 				<spring:message code="label_work_msg" text="작업메시지"/> --%>
<!-- 			</fm-popup-button> -->
<!-- 			<fm-popup-button  popupid="fw_vmuser_cancel_form_popup" -->
<!-- 				popup="/clovirsm/popup/fw_vmuser_cancel_form_popup.jsp" cmd="update" class="delBtn" param="'cancel'" -->
<!-- 				callback=""> -->
<%-- 				<spring:message code="btn_task_no" text="작업거부"/> --%>
<!-- 			</fm-popup-button> -->

<!-- 		</div> -->
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
			<spring:message code="mainTable_Search_information" text="검색정보" />
			<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
			<c:if test="${'Y'.equals(sessionScope.FW_API_YN)}">
			<fm-sbutton cmd="update" class="newBtn contentsBtn tabBtnImg" onclick="deployFW()"><spring:message code="btn_task_fw_auto" text="자동작업" /></fm-sbutton>
			</c:if>
			<fm-sbutton cmd="update" class="newBtn contentsBtn tabBtnImg" onclick="complete()"><spring:message code="btn_task_yes" text="" /></fm-sbutton>

	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 450px;" ></div>
	</div>
	<br />
	<c:if test="${'Y'.equals(sessionScope.FW_API_YN)}"><spring:message code="btn_fw_auto_guide" text=""/></c:if>
</div>
<script>
	var fwMngReq = new Req();
	mainTable;

	$(function() {
		var columnDefs = [{headerName : "<spring:message code="NC_FW_SERVER_IP" text="" />", field : "SERVER_IP",tooltipField:'VM_NM'},
		                  {headerName : "<spring:message code="NC_VM_VM_NM" text="" />", field : "VM_NM", tooltipField:'VM_NM'},
		                  {headerName : "<spring:message code="NC_FW_USER_IP" text="" />", field : "CIDR",tooltipField:'CIDR',},
		                  {headerName : "<spring:message code="allowedPort" text="" />", cellStyle:{'text-align':'right'}, field : "PORT"},
		                  {
		          			headerName : '<spring:message code="NC_FW_TEAM_NM" text="담당자팀" />',
		          			field : 'TEAM_NM',
		          			maxWidth: 130,
		          			width: 130,
		          			headerTooltip:'<spring:message code="NC_FW_TEAM_NM" text="담당자팀" />',
		          			tooltipField:'TEAM_NM',
			          		},{
			          			headerName : '<spring:message code="NC_FW_INS_NAME" text="대상자" />',
			          			field : 'USER_NAME',
			          			maxWidth: 120,
			          			width: 120,
			          			headerTooltip:'<spring:message code="NC_FW_INS_NAME" text="대상자" />',
			          			tooltipField:'USER_NAME',
			          		},
		                  {headerName : "<spring:message code="REQ_TMS" text="" />", field : "INS_TMS",
			          			tooltip:function(params){
		                	  		return formatDate(params.data.INS_TMS,'datetime')
		                  		},
		                		valueGetter: function(params) {
	                			  if(params.data && params.data.INS_TMS){
	                				  if(params.data.UPD_TMS){
							    		  return formatDate(params.data.UPD_TMS,'datetime')
	                				  }
						    		  return formatDate(params.data.INS_TMS,'datetime')
	                			  }
	                			  return "";
						  }},
						  {headerName : "<spring:message code="CMT" text="" />", field : "CMT",tooltipField:"CMT", cellRenderer:function(params){
							  if(params.data.DEL_YN == 'Y') {
								  return '<font color="red"><spring:message code="delete" text="" /></font> ' +  nvl(params.value,'');
							  }
							  else {
								  return params.value;
							  }
						  }},
		                  {headerName : "<spring:message code="NC_FW_TASK_STATUS_CD" text="" />", field : "TASK_STATUS_CD_NM", tooltipField:'FAIL_MSG'},
		                  
		                  <c:if test="${'Y'.equals(sessionScope.VM_USER_YN)}"> 
			                  {headerName : "<spring:message code="NC_FW_VM_USER_YN" text="" />", field : "ACCT_AUTO_CREATE_YN", cellRenderer:function(params){
			                	if(params.data.VM_USER_YN == 'Y')
			                	{
			                		if(params.data.ACCT_AUTO_CREATE_YN=='Y')
			                		{
			                			return "<spring:message code="NC_FW_VM_ACCT_AUTO_CREATE" text="자동생성" />"
			                		}
			                		else
			                		{
			                			return "<spring:message code="NC_FW_VM_ACCT_AUTO_CREATE_FAIL" text="자동생성 실패" />"
			                		}
			                	}
			                	else
			                	{
			                		return "";
			                	}
			                  }},
		                  </c:if>
			              <c:if test="${'Y'.equals(sessionScope.FW_API_YN)}">
			                  {headerName : "<spring:message code="FW_DEPLOY_RESULT" text="FW연동" />", field : "FW_DEPLOY_RESULT" , tooltipField:'FW_DEPLOY_RESULT', cellRenderer:function(params){
			                	 if(params.value == "Y") {
			                		 return "<spring:message code="success" text="성공" />"
			                	 } else if(!params.value || params.value == "") {
			                		 return "<spring:message code="before" text="반영전" />"
			                	 } else {
			                		 return "<spring:message code="fail" text="실패" />"  ;
			                  	 }
			                  }}
		                  </c:if>
		                  ]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowSelection : 'multiple',
			editable: true,
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false
		}
		mainTable = newGrid("mainTable", gridOptions);
		searchvue.form_data.TASK_STATUS_CD = "W";
		search();
	});

	// 조회
	function search() {
		fwMngReq.search('/api/fw_mng/list', function(data){mainTable.setData(data)});
	}

	function cancel(msg, callback){
		if(confirm(msg_confirm_save)){
			setGridValues("FAIL_MSG", msg);
			setGridValues("TASK_STATUS_CD", "D");
			fwMngReq.saveGrid(mainTable, "/api/fw_mng/update", function(){
				search();
				callback();
			})
		}
	}

	function message(msg, callback){
		if(confirm(msg_confirm_save)){
			setGridValues("FAIL_MSG", msg);
			fwMngReq.saveGrid(mainTable, "/api/fw_mng/update", function(){
				search();
				callback();
			})
		}
	}

	function complete(){
		if(mainTable.getSelectedRows()[0]){
			if(confirm(msg_confirm_save)){
				setGridValues("TASK_STATUS_CD", "S");
				fwMngReq.saveGrid(mainTable, "/api/fw_mng/update", function(){
					search();
				})
			}
		}else{
			alert(msg_select_first)
		}
	}

	function createNewUser()
	{
		var arr = mainTable.getSelectedRows();

		var paramArr = [];
		for(var i=0; i <arr.length; i++)
		{
			if(arr[i].VM_USER_YN=='Y')
			{
				paramArr.push(arr[i])
			}
		}
		if(paramArr.length==0)
		{
			alert(msg_select_first);
			return;
		}
		post('/api/fw_mng/syncUser', {LIST: JSON.stringify(paramArr) }, function(data){
			alert(msg_complete);
			search();
		})
	}
	function deployFW()
	{
		var arr = mainTable.getSelectedRows();

		var paramArr = [];
		for(var i=0; i <arr.length; i++)
		{
			if(arr[i].FW_DEPLOY_RESULT !='Y' ||  arr[i].VM_USER_YN =='Y' )
			{
				paramArr.push(arr[i])
			}
		}
		if(paramArr.length==0)
		{
			alert(msg_select_first);
			return;
		}
		post('/api/fw_mng/deployFw', {LIST: JSON.stringify(paramArr) }, function(data){
			alert(msg_complete);
			search();
		})
	}
	function setGridValues(key, value){
		var arr = mainTable.getSelectedRows();
		for(var i=0; i<arr.length; i++){
			arr[i][key] = value;
		}
//		for(var i in arr){
//		}
	}

	// 엑셀 내보내기
	function exportExcel() {
		mainTable.exportCSV({fileName:'fw'})
		 
	}

</script>
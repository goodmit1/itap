<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<style>
	.height24{
		height: 24px;
	}
</style>
<!--  서버 목록 조회  -->
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다  -->
		<div class="col col-sm">
			<fm-input id="S_MODEL_NM" name="MODEL_NM" title="<spring:message code="NC_THIN_CLIENT_MODEL_NM" text="모델명" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_SN" name="SN" title="<spring:message code="NC_THIN_CLIENT_SN" text="SN" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=CLIENT_STATUS" id="S_STATUS"
				keyfield="STATUS" titlefield="STATUS_NM" emptystr=" "
				name="STATUS" title="<spring:message code="NC_THIN_CLIENT_STATUS" text="상태"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_MAC_ADR" name="MAC_ADR" title="<spring:message code="NC_THIN_CLIENT_MAC_ADR" text="MAC" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_IP" name="IP" title="<spring:message code="NC_THIN_CLIENT_IP" text="IP" />"></fm-input>
		</div>
		<div class="col btn_group nomargin" style="padding-left:5px;">
			<fm-sbutton cmd="search" class="searchBtn" onclick="thinClientSearch()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
		<div id="popup-button-html">
		</div>
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:450px">
	<div class="table_box-shadow">
		<div class="table_title layout name">
			<div class="search_info">
				<spring:message code="mainTable_Search_information" text="검색정보" />
				<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
			</div>
			<div class="btn_group">
				<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
				<button type=button" id="disposal_btn" class="btn btn-primary fm-popup-button update" onclick="dispose_thin_client(); return false;" style="color: red !important; border-color: red !important;" disabled="disabled">
					<spring:message code="btn_disposal" text="폐기"/>
				</button>
				<fm-popup-button popupid="thin_client_detail_popup" popup="thin_client_detail_popup.jsp?callback=onAfterModify" cmd="update" callback="thinClientSearch">
					<spring:message code="btn_new" text="추가"/>
				</fm-popup-button>				
			</div>
		</div>
		<div class="layout background mid">
			<div id="mainTable" class="ag-theme-fresh" style="height: 450px" ></div>
		</div>
	</div>
</div>
<script>
	var thinClientReq = new Req();
	mainTable;
	var selectedThinClient = null;
	$(function() {
		var columnDefs = [{headerName : "<spring:message code="NC_THIN_CLIENT_MODEL_NM" text="모델명" />", field : "MODEL_NM"},
		                  {headerName : "<spring:message code="NC_THIN_CLIENT_SN" text="SN" />", field : "SN"},
		                  {headerName : "<spring:message code="NC_THIN_CLIENT_MAC_ADR" text="MAC" />", field : "MAC_ADR"},
		                  {headerName : "<spring:message code="NC_THIN_CLIENT_IP" text="IP" />", field : "IP"},
		                  {headerName : "<spring:message code="NC_THIN_CLIENT_VM" text="VM" />", field : "VM_NM"},
		                  {headerName : "<spring:message code="NC_THIN_CLIENT_STATUS" text="상태" />", field : "STATUS", valueGetter:function(params){
	                             if(params.data && params.data.STATUS_NM){
	                                 return params.data.STATUS_NM;
	                             }
	                             return "";
	                          }
		                  	  , cellStyle: function(params){
		                  		if(params.data && params.data.STATUS == 'X'){
	                                 return { color: 'red' };
	                             }
		                  		return null;
		                  	  }
		                  	}
		                  ]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowModelType: 'infinite',
			cacheBlockSize: 10,
			rowSelection : 'single',
			rowData : [],
			suppressHorizontalScroll: false,
			rowStyle : {},
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: true,
			onSelectionChanged : function() {
				var arr = mainTable.getSelectedRows();
				selectedThinClient = arr[0];
				if(selectedThinClient.STATUS == 'W'){
					$('#disposal_btn').prop('disabled', false);
				} else {
					$('#disposal_btn').prop('disabled', true);
				}
			},
			onRowDoubleClicked: function(event){
				var arr = mainTable.getSelectedRows();
				thinClientReq.getInfo('/api/thin_client/info?SN='+ arr[0].SN, function(data){
					console.log('info callback', data);
					selectedThinClient = data;
					isInit = false;
					$('#thin_client_detail_popup_button').click();
				});
			}
		}
		mainTable = newGrid("mainTable", gridOptions);
		thinClientSearch();
	});

	// 조회
	function thinClientSearch() {
		thinClientReq.searchPaging('/api/thin_client/list' , mainTable, function() {
			mainTable.gridOptions.api.sizeColumnsToFit()
		});
	}
	
	var isInit = true;
	function thin_client_detail_popup_onBeforeOpen(){
		console.log('thin_client_detail_popup_onBeforeOpen');
		if(isInit){
			selectedThinClient = null;
		}
		isInit = true;
		return true;
	}

	function chkSelection(){
		var arr = mainTable.getSelectedRows();
		if(arr[0]){
			return true;
		}
		alert(msg_select_first);
		return false
	}
	
	function dispose_thin_client(){
		if(chkSelection()){
			if(selectedThinClient.STATUS == 'X'){
				alert('이미 폐기되었습니다.');
				return false;
			}
			if(confirm(selectedThinClient.MODEL_NM+'['+selectedThinClient.SN+']을 폐기하시겠습니까?')){
				post('/api/thin_client/delete', selectedThinClient,  function(data){
					alert('폐기되었습니다');
					thinClientSearch();
					$('#thin_client_detail_popup').modal('hide');
				});
			}
		}
	}
	
	// 엑셀 내보내기
	function exportExcel(){
		var excelColDef = [
			{headerName : "<spring:message code="NC_THIN_CLIENT_MODEL_NM" text="모델명" />", field : "MODEL_NM"},
            {headerName : "<spring:message code="NC_THIN_CLIENT_SN" text="SN" />", field : "SN"},
            {headerName : "<spring:message code="NC_THIN_CLIENT_STATUS" text="상태" />", field : "STATUS"}, 
           	{headerName : "<spring:message code="NC_THIN_CLIENT_STATUS_NM" text="상태명" />", field : "STATUS_NM"},
            {headerName : "<spring:message code="NC_THIN_CLIENT_MAC_ADR" text="MAC" />", field : "MAC_ADR"},
            {headerName : "<spring:message code="NC_THIN_CLIENT_IP" text="IP" />", field : "IP"},
            {headerName : "<spring:message code="NC_VM_VM_ID" text="VM_ID" />", field : "VM_ID"},
            {headerName : "<spring:message code="NC_VM_VM_NM" text="VM_NM" />", field : "VM_NM"}
		];
		exportExcelServer("mainForm", '/api/thin_client/list_excel', 'Thin Client',excelColDef, thinClientReq.getRunSearchData())
	}

</script>



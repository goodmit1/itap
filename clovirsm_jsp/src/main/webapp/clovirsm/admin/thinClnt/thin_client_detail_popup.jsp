<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<fm-modal id="thin_client_detail_popup" title="<spring:message code="btn_vm_polling" text="씬 클라이언트"/>" cmd="header-title">
	<span slot="footer">
		<button type="button" class="btn btn-primary fm-popup-button update" id="cancel" onclick="thin_client_detail_popup_hide()"><spring:message code="btn_cancel" text="취소"/></button>
		<button type="button" class="btn btn-primary fm-popup-button update" id="request" onclick="save_thin_client()"><spring:message code="btn_save" text="저장"/></button>
	</span>
	<form  class="form-panel detail-panel" id="thin_client_detail_popup_form" action="none" style="height:200px">
		<div class="col col-sm-12">
			<fm-input id="SN" name="SN" title="<spring:message code="NC_THIN_CLIENT_SN" text="SN" />" :disabled="form_data.SN != null"></fm-input>
		</div>
		<div class="col col-sm-12">
			<fm-input id="MODEL_NM" name="MODEL_NM" title="<spring:message code="NC_THIN_CLIENT_MODEL_NM" text="모델명" />"></fm-input>
		</div>
		<div class="col col-sm-12">
			<fm-input id="MAC_ADR" name="MAC_ADR" title="<spring:message code="NC_THIN_CLIENT_MAC_ADR" text="MAC" />"></fm-input>
		</div>
		<div class="col col-sm-12" v-if="form_data.STATUS == 'I'">
			<fm-output id="IP" name="IP" title="<spring:message code="NC_THIN_CLIENT_IP" text="IP" />"></fm-output>
		</div>		
	</form>

	<script>
		var org_form_data = (selectedThinClient && selectedThinClient != null) ? selectedThinClient: {
			SN: null,
			MODEL_NM: null,
			MAC_ADR: null,
			IP: null,
			STATUS: 'W',
			VM_ID: null
		};
		var popup_get_vm_form = newPopup("thin_client_detail_popup_button", "thin_client_detail_popup");
		var thin_client_detail_popup_vue = new Vue({
			el: '#thin_client_detail_popup',
			data: {
				form_data: $.extend({}, org_form_data)
			}
		});
		
		function thin_client_detail_popup_click(vue, param){
			console.log('thin_client_detail_popup_click');
			return true;
		}
				
		function save_thin_client(){
			console.log(org_form_data, thin_client_detail_popup_vue.form_data);
			if(org_form_data.MODEL_NM == thin_client_detail_popup_vue.form_data.MODEL_NM
				&& org_form_data.MAC_ADR == thin_client_detail_popup_vue.form_data.MAC_ADR){
				thin_client_detail_popup_hide();
				return;
			}
			if(confirm('저장하시겠습니까?')){
				post('/api/thin_client/save', thin_client_detail_popup_vue.form_data,  function(data){
					alert('저장되었습니다');
					thinClientSearch();
					thin_client_detail_popup_hide();
				});
			} else {
				thin_client_detail_popup_hide();
			}
		}
		
		function thin_client_detail_popup_hide(){
			$('#thin_client_detail_popup').modal('hide')
		}
		
	</script>
</fm-modal>
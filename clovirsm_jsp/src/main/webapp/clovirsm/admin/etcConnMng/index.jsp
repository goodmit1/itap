<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@ taglib prefix="fmtags" uri ="http://fliconz.kr/jsp/tlds/fmtags" %> 

<layout:extends name="base/index">
    <layout:put block="content">
    	<form id="mainForm" onsubmit="return false">
    	<fmtags:include page="list.jsp"></fmtags:include>
    	</form>
    </layout:put>
    <layout:put block="popup_area">
    	<jsp:include page="/admin/popup/user.jsp"></jsp:include>
    	<jsp:include page="/admin/popup/team.jsp"></jsp:include>
    	<script>
    	var popup_team = newPopup("PARENT_TEAM_NM", "team_popup");
    	popup_team.setInputInfo({"keyword":"PARENT_TEAM_NM"});
    	popup_team.setMapping({"TEAM_NM":"PARENT_TEAM_NM", "TEAM_CD":"PARENT_CD"});
    	
     
    	var popup_user = newPopup("TEAM_OWNER_NM", "user_popup");
    	popup_user.setInputInfo({"keyword":"TEAM_OWNER_NM","TEAM_CD":"TEAM_CD"});
    	 
    	popup_user.setMapping({"USER_NAME":"TEAM_OWNER_NM", "USER_ID":"TEAM_OWNER"});



    	</script>
	</layout:put>
</layout:extends>
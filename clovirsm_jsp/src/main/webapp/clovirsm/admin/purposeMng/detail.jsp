<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- 입력 Form -->

<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body" >
		<div class="col col-sm-6" id="CATEGORY_DIV" style="border-bottom: 1px solid #ddd; ">
			<fm-select2 url="" id="_svc" name="CATEGORY_ID"
				title=" <spring:message code='TASK' text='업무'/>" required="true" onchange="purpose_auto()"></fm-select2>
		</div>
		<div class="col col-sm-5" style="border-bottom: 1px solid #ddd;">
			<fm-select id="S_PURPOSE" name="PURPOSE" title="<spring:message code="PURPOSE" text="용도"/>" required="required"></fm-select>
		</div>
		<div class="col col-sm-1" style="border-bottom: 1px solid #ddd;">
			<button type="button" onclick="taskPurposeMappingClick()" class="btn icon layout postion" style="position: relative;top: 6px; padding: 0px; left: 0px;"><i class="fa fat add fa-plus-square"></i></button>
		</div>
	</div>
</div>

<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- 입력 Form -->

<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body" >
			<div class="col col-sm-6">
				<fm-input id="SCRIPT_NM" name="SCRIPT_NM" title="<spring:message code="label_title" text="제목" />"></fm-input>
			</div>
			<div class="col col-sm-6	">
				<fm-select id="LINUX_YN" name="LINUX_YN"  url="/api/code_list?grp=sys.yn"  title="<spring:message code="LinuxYN" text="리눅스 여부" />"></fm-select>
			</div>
			<div class="col col-sm-12" required="required">
				<fm-input id="FILE_NM" name="FILE_NM" title='File명' div_style="width: calc(100% - 60px); display:inline-block;" ></fm-input>
				<div style="display:inline-block;">
					<button class="shellBtn" id="shellBtn"  onclick="shellEdit($('#FILE_NM') , 'shell'); " type="button" disabled><spring:message code="EDIT" text="편집"/> </button>
				</div>
			</div>
			<div class="col col-sm-12" >
				<fm-textarea id="SCRIPT_DESC" name="SCRIPT_DESC" title="<spring:message code="NC_VRA_CATALOG_CMT" text="설명"/>" label_style="height: 300px;" sty="height: 280px; border: 1px solid #b5b5b5;   padding:10px;"></fm-textarea>
			</div>
			<input type="hidden" name="SW_ID" id="SW_ID">
	</div>
</div>

<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>

<script src="/res/js/lib/ace/ace.js"></script>


<style>
#mainTable { height:calc(100vh - 220px); }

.shellBtn{
    border: none;
    background: #6E7783;
    color: white;
    border-radius: 5px;
    width: 45px;
    height: 25px;
}
.shellBtn:disabled {
    border: none;
    background: #bfbfbf;
    color: white;
    border-radius: 5px;
    width: 45px;
    height: 25px;
}
#editor { 
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
    }

</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col col-sm">
			<fm-input id="S_SCRIPT_NM" name="SCRIPT_NM" title="<spring:message code="label_title" text="" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-select id="S_LINUX_YN" name="LINUX_YN"  url="/api/code_list?grp=sys.yn" emptystr=" " title="<spring:message code="LinuxYN" text="리눅스 여부" />"></fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_FILE_NM" name="FILE_NM" title="<spring:message code="fileName" text="File명" />"></fm-input>
		</div>
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text="조회"/></fm-sbutton>
		</div>
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
				<spring:message code="mainTable_Search_information" text="검색정보" />
				<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)</span>
				<div class="btn_group">
					<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn" id="excelBtn"></button>
					<fm-sbutton cmd="update" class="newBtn contentsBtn tabBtnImg new " onclick="insert()"   ><spring:message code="btn_new" text="신규"/></fm-sbutton>
					<fm-sbutton cmd="delete" class="delBtn contentsBtn tabBtnImg del " onclick="del()"   ><spring:message code="btn_del" text="삭제"/></fm-sbutton>
					<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg save " onclick="save()"   ><spring:message code="btn_save" text="저장"/></fm-sbutton>
				</div>

	</div>

	<div id="mainTable" class="ag-theme-fresh" style="height: 500px" ></div>
	<jsp:include page="detail.jsp"></jsp:include>
<fm-popup-button style="display: none;" popupid="shell_popup" popup="/clovirsm/popup/shellPopup.jsp" cmd="update" param="req.getData()"></fm-popup-button>
</div>
<script>
	var req = new Req();
	var mainTable;
	var arr ;
	$(function() {

		var columnDefs = [
			{headerName : "",field : "SCRIPT_ID", hide : true},
			{headerName : "<spring:message code="label_title" text="종류" />",field : "SCRIPT_NM"},
			{headerName : "<spring:message code="fileName" text="File명" />",field : "FILE_NM"},
			{headerName : "<spring:message code="LinuxYN" text="리눅스 여부" />", field : "LINUX_YN"},
			{headerName : "<spring:message code="INS_TMS" text="" />",field : "INS_DT"},
			]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onSelectionChanged : function() {
					arr = mainTable.getSelectedRows();
					req.getInfo("/api/script/info?SCRIPT_ID="+arr[0].SCRIPT_ID, function(data){
							$("#FILE_NM").attr("disabled", true);
							$("#shellBtn").attr("disabled", false);
							});
				},
		}
		mainTable = newGrid("mainTable", gridOptions);

		search();
	});
	
	
	// 조회
	function search() {
		req.search('/api/script/list' , function(data) {
			mainTable.setData(data);
		});
	}

	function insert(){
		$("#FILE_NM").attr("disabled",false);
		$("#shellBtn").attr("disabled", true);
		req.setData();
	}
	
	function del(){
		req.del('/api/script/delete', function(){
			search();
		});
	}
	function shellEdit(ob, type){
		
		if($("#FILE_NM").val() != ''  ){
			var title = $(ob).parent('div').children('label').html();
			var name = $(ob).val();
			req.putData({'title' : title , "type" : type, "name" : name, "url" : 'script', "purpose" : 'script'});
			$("#shell_popup_button").click();
		}
	}
	// 저장
	function save() {
		var w = mainTable.getSelectedRows();
		
		req.save('/api/script/scriptSave', function(){
			search();
		});
	}
	// 엑셀 내보내기
	function exportExcel()
	{
		 mainTable.exportCSV({fileName:'script'})
		 // exportExcelServer("mainForm", '/api/script/list_excel', 'Script',mainTable.gridOptions.columnDefs, req.getRunSearchData())
	}
	
	
	
</script>



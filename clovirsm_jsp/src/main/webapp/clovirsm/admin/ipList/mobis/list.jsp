<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<style>
#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<div class="col col-sm">
			<table class="hasTiltTbl"><tr><td><fm-input  id="S_START_IP" name="START_IP" title="IP"  required="true" tooltip="<spring:message code="start_ip" text="시작 IP" />"></fm-input></td><td class="tilt">~</td><td><fm-input id="S_END_IP" required="true"   name="FINISH_IP" ></fm-input></td></table>
			
			
		</div> 
		<div class="col btn_group nomargin">
			
			<fm-sbutton cmd="search" class="searchBtn" onclick="search('defalut')"  ><spring:message code="btn_search" text=""/></fm-sbutton>
		</div>
		<div class="col btn_group_under">
			<fm-sbutton cmd="search" class="exeBtn" onclick="sync()"  ><spring:message code="btn_sync_ip" text="IP정보수집"/></fm-sbutton>
		</div>
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 450px;" ></div>
 	</div>
	</div>
</div>
<script>
var req = new Req();
var mainTable;


$(function() {

	var columnDefs = [{headerName : "IP",field : "IP", width:150},
		{headerName : "<spring:message code="NC_DC_DC_NM" text="DC_NM" />",field : "DC_NM", width:150},
		{headerName : "<spring:message code="NC_VM_VM_NM" text="VM_NM" />",field : "VM_NM" , width:250},
		{headerName : "<spring:message code="STATUS" text="STATUS" />",field : "STATUS_NM", width:100},
		{headerName : "<spring:message code="NC_VM_GUEST_NM" text="GUEST_NM" />",field : "GUEST_NM"},
		{headerName : "<spring:message code="NC_VM_P_KUBUN" text="P_KUBUN" />",field : "P_KUBUN_NM"},
		{headerName : "<spring:message code="NC_VM_PURPOSE" text="PURPOSE" />",field : "PURPOSE_NM"},
		{headerName : "<spring:message code="FM_TEAM_TEAM_CD" text="FM_TEAM_TEAM_CD" />",field : "TEAM_NM"},
		{headerName : "<spring:message code="INS_ID" text="INS_ID" />",field : "INS_ID_NM"},
		
		
		]
	var
	gridOptions = {
		hasNo : true,
		columnDefs : columnDefs,
		rowModelType: 'infinite',
		//rowSelection : 'single',
		sizeColumnsToFit: true,
		cacheBlockSize: 100,
		rowData : [],
		enableSorting : true,
		enableColResize : true,
		enableServerSideSorting: true,
		onSelectionChanged : function() {
			 
		},
	}
	mainTable = newGrid("mainTable", gridOptions);
	search('main');
	 
});
function initFormData() {
	
}
//동기화
function sync() {
	post('/api/ip/sync',{}, function(data){
		alert("<spring:message code="ip_collect_req_msg" text="IP정보를 수집요청하였습니다.(몇 분 소요)"/>");
		 
	} )
}
// 조회
function search(state) {
	 
	if(state === 'main')
	{
		req.searchPaging('/api/ip/list' ,mainTable, function(data) {
			mainTable.gridOptions.api.sizeColumnsToFit();
		});	
		/* req.search('/api/ip/list' , function(data) {
			mainTable.setData(data);
		});		 */		
	}
	else{
		if(!validate("search_area")) return;
		var start_ip=$('#S_START_IP').val()
		var end_ip=$('#S_END_IP').val()
		var arr = [start_ip,end_ip]
		
		for(var i=0; i<arr.length; i++){
			if(!ipValidate(arr[i])){
				return;
			}
		}
//		for(var i in arr){
//		}
		req.searchPaging('/api/ip/list' ,mainTable, function(data) {
			mainTable.gridOptions.api.sizeColumnsToFit();
		});	
		/* req.search('/api/ip/list' , function(data) {
			mainTable.setData(data);
		});		 */
	}
	
}
// 엑셀 내보내기
function exportExcel() {
	exportExcelServer("mainForm", '/api/ip/list_excel', 'IP',mainTable.gridOptions.columnDefs, req.getRunSearchData())
	//mainTable.exportCSV({fileName:'ip.csv'})
}

</script>



<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
String popupid = request.getParameter("popupid");
request.setAttribute("popupid", popupid);
%>
<fm-modal id="${popupid}" title="<spring:message code="new_ip_input" text="신규 IP대역 "/>" cmd="header-title">
	<span slot="footer">
		 <input type="button" class="btn ${popupid}_save_btn popupBtn finish top5" value="<spring:message code="btn_save" text="저장"/>" onclick="${popupid}_save()" />
	 
	</span>
	<div class="form-panel detail-panel">
	<form id="${popupid}_form" action="none">
		<div class="panel-body" style="height:300px;overflow:auto">
		<fm-input id="INPUT_IP" required="true" name="ips" title="<spring:message code="title_ip_input" text="IP대역"/>" placeholder="ex)10.2.1.10.0/24" />
		</div>
	</form>
	</div>
	<script>
		var ${popupid}_vue = makePopupVue("${popupid}", "${popupid}", {ips:''});
		function ${popupid}_click(thisvue, param){
			${popupid}_vue.callback=thisvue.callback;
			return true;
		}
		function ${popupid}_save()
		{
			if(!validate("${popupid}_form")) return false;
			
			post('/api/ip/insert', ${popupid}_vue.form_data, function(data){
				
				 
				if( ${popupid}_vue.callback) eval( ${popupid}_vue.callback + '();');
				$("#${popupid}").modal('hide');
			});
		}
	</script>
</fm-modal>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/index">
    <layout:put block="content">
    	<form id="mainForm" >
    		<jsp:include page="list.jsp"></jsp:include>
    	</form>
    </layout:put>
  
</layout:extends>	  
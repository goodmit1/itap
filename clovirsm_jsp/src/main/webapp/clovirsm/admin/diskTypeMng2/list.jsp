<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %> 
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.clovirsm.service.admin.DiskTypeMngService"%>
<%
	DiskTypeMngService diskTypeMngService = (DiskTypeMngService)SpringBeanUtil.getBean("diskTypeMngService");
	Map param=new HashMap();
	List<Map> list = diskTypeMngService.list(param);
	request.setAttribute("list", list);
%>
<!--  서버 목록 조회 -->
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col btn_group rt">
			<fm-sbutton id="insert_diskTypeMng_button" cmd="update" class="newBtn" onclick="insertDiskType()" ><spring:message code="btn_new"  text="신규"/></fm-sbutton>
			<fm-sbutton id="save_diskTypeMng_button"   cmd="update" class="saveBtn" onclick="diskTypeMngSave()"><spring:message code="btn_save" text="저장"/></fm-sbutton>
		</div>
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:1080px; padding: 10px;">
	<c:forEach var="i" items="${list}" varStatus="status">
		<div class="contentsItem">
			<div class="contentsItemTop">
				<div class="itemIcon" style=""></div>
				<div class="itemName">${i.DISK_TYPE_NM}</div>
			</div>
			
			<div class="contentsItemMid">
				<div>
					<div class="contentsItemPay"><span>\</span> ${i.HH_FEE}</div>
					
				</div>
			</div>
			<div class="contentsItembot">
				<div class="contentsItemTeam">
					<div class="contentsItemTeamAll">
						<div class="teamCancel">x</div>
						<div class="teamName">전체사용</div>
					</div>
				</div>
				<div class="contentsItemTeamAdd">
					팀추가
				</div>
			</div>
		</div>
	</c:forEach>
</div>
<script>
	var diskTypeMngReq = new Req();
	$(function() {
		diskTypeMngSearch();
	});
	// 조회
	function diskTypeMngSearch() {
		diskTypeMngReq.search('/api/disk_type_mng/list' , function(data) {
		});
	}

	function onRowClick(data){
		setButtonClickable('save_diskTypeMng_button', true);
		setButtonClickable('delete_diskTypeMng_button', true);
	}

	//저장
	function diskTypeMngSave(){
		 
			diskTypeMngReq.save('/api/disk_type_mng/save', function(){
				diskTypeMngSearch();
				alert('<spring:message code="saved" text="저장되었습니다"/>');
			});
		 
	}

	function insertDiskType(){
		form_data.DISK_TYPE_ID = ""
		form_data.HH_FEE       = ""
		form_data.DD_FEE       = ""
		form_data.FEE_UNIT     = ""
		form_data.DISK_TYPE_NM = ""
		form_data.MM_FEE       = ""
		form_data.INS_TMS      = ""
		form_data.DEL_YN       = "N"
	}

	// 엑셀 내보내기
	function exportExcel(){
		diskTypeGrid.exportCSV({fileName:'diskType'}) 
	}

</script>



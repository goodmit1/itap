<%@page contentType="text/html; charset=UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 입력 Form -->
<%--<div id="vm_detail_wrap_hide" class="form-panel panel panel-default detail-panel" >--%>
<div id="vm_detail_wrap_hide" class="form-panel panel panel-default detail-panel" style="display: none">
	<table border="1" style="border: 1px solid black; margin: auto; width: 35%;" align ="center">
		<tr bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">서버명</th>
			<td colspan="3" style="font-weight: bold" v-html="form_data.VM_NM">
				<hr/>
			</td>
		</tr>
		<tr bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">OS명</th>
			<td colspan="3" style="font-weight: bold"  v-html="form_data.GUEST_NM"></td>
		</tr>
		<tr bordercolor="#000000">
<%--			<fm-output2  id="P_KUBUN" emptystr=" "  :value="  nvl( form_data.CATEGORY_NM,'') + ' > '   + nvl(form_data.P_KUBUN_NM,'')  + ' '  + nvl(form_data.PURPOSE_NM,'')"--%>
<%--						 name="P_KUBUN_NM" title="<spring:message code="TASK" text="업무"/>">--%>
<%--			</fm-output2>--%>
			<th colspan="2" bgColor="#edeef3">업무</th>
			<td colspan="3" style="font-weight: bold" v-html="nvl( form_data.CATEGORY_NM,'') + ' > '   + nvl(form_data.P_KUBUN_NM,'')  + ' '  + nvl(form_data.PURPOSE_NM,'')"></td>
		</tr>
		<tr bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">사용용도</th>
			<td colspan="3" style="font-weight: bold"  v-html="form_data.PURPOSE"></td>
		</tr>
		<tr bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">구분</th>
			<td colspan="3" style="font-weight: bold"  v-html="form_data.P_KUBUN_NM"></td>
		</tr>
		<tr bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">등록일시</th>
			<td colspan="3" style="font-weight: bold"  v-html="formatDate(form_data.INS_TMS, 'datetime')"></td>
<%--			<fm-output2 id="INS_TMS" name="INS_TMS" title="<spring:message code="NC_VM_INS_TMS" text="등록일시" />" :value="formatDate(form_data.INS_TMS, 'datetime')"></fm-output2>--%>
		</tr>
		<tr bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">부서</th>
			<td colspan="3" style="font-weight: bold"  v-html="form_data.TEAM_NM"></td>
		</tr>
		<tr bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">부서장</th>
			<td colspan="3" style="font-weight: bold"  v-html="form_data.TEAM_MNGRS"></td>
		</tr>
		<tr bordercolor="#000000" style="height: 150px;">
			<th colspan="2" bgColor="#edeef3">설명</th>
			<td colspan="3" style="font-weight: bold"  v-html="cmtValue(form_data.CMT)"></td>
		</tr>
		<tr bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">VM IP</th>
			<td colspan="3" style="font-weight: bold"  v-html="form_data.PRIVATE_IP"></td>
		</tr>
		<tr bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">VM MAC</th>
			<td colspan="3" style="font-weight: bold"  v-html="form_data.MAC_ADDR"></td>
		</tr>
		<tr v-if="form_data.THIN_REQ_YN == 'Y'">
			<th colspan="2" bgColor="#edeef3">접속포트</th>
			<td colspan="3" style="font-weight: bold"  v-html="form_data.PORT"></td>
		</tr>
		<tr bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">보안 유의사항 확인</th>
			<td colspan="3" style="font-weight: bold"  v-html="cmtValue(form_data.SEC_PRECAUTION_CMT)"></td>
		</tr>
		<tr bordercolor="#000000">
			<th colspan="5" style="background-color: white">접속 단말 정보</th>
		</tr>
		<tr bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">SN</th>
			<td colspan="3" style="font-weight: bold"  v-html="form_data.SN"></td>
		</tr>
		<tr v-if="form_data.THIN_REQ_YN == 'Y'" bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">모델명</th>
			<td colspan="3" style="font-weight: bold"  v-html="form_data.MODEL_NM"></td>
		</tr>
		<tr v-if="form_data.THIN_REQ_YN == 'Y'" bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">MAC</th>
			<td colspan="3" style="font-weight: bold"  v-html="form_data.MAC_ADR"></td>
		</tr>
		<tr bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">IP</th>
			<td colspan="3" style="font-weight: bold"  v-html="form_data.IP"></td>
		</tr>
		<tr  v-if="form_data.THIN_REQ_YN == 'Y'" bordercolor="#000000">
			<th colspan="5" style="background-color: white">접속 단말 보안 예외 신청 항목</th>
		</tr>
		<tr  v-if="form_data.THIN_REQ_YN == 'Y'" bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">NAC 설치 예회 신청</th>
			<td>
				<div class="output  hastitle">
					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right: 5px;"/>
					<label>NAC 설치 예회 신청</label><br/>
				</div>
			</td>
		</tr>
		<tr  v-if="form_data.THIN_REQ_YN == 'Y'" bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">무결성 검사 예외 신청</th>
			<td>
				<div class="output  hastitle">
					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right: 5px;"/>
					<label>무결성 검사 예외 신청</label><br/>
				</div>
			</td>
		</tr>
		<tr v-if="form_data.THIN_REQ_YN == 'Y'" bordercolor="#000000">
			<th colspan="2" bgColor="#edeef3">무결성 검사 예외<br/> 보안 프로그램</th>
			<td>
				<div class="output  hastitle">
					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>백신(V3)</label><br/>
					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>백신(알약)</label><br/>
					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>매체제어(nProtect)</label><br/>
					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>문서보안</label><br/>
					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>출력문보안</label><br/>
					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>자산관리</label>
				</div>
			</td>
		</tr>
<%--		<tr>--%>
<%--			<th colspan="2" bgColor="#edeef3">MAC</th>--%>
<%--			<td colspan="3" style="font-weight: bold"  v-html="form_data.MAC_ADR.SN"></td>--%>
<%--		</tr>--%>
<%--		<tr>--%>
<%--			<th colspan="2" bgColor="#edeef3">MAC</th>--%>
<%--			<td colspan="3" style="font-weight: bold"  v-html="form_data.MAC_ADR.SN"></td>--%>
<%--		</tr>--%>

<%--		<tr class="col col-sm-6" v-if="form_data.THIN_REQ_YN == 'Y'">--%>
<%--			<fm-output2 id="MODEL_NM" name="MODEL_NM" title="<spring:message code="NC_THIN_CLIENT_MODEL_NM" text="모델명" />"></fm-output2>--%>
<%--		</tr>--%>
<%--		<tr class="col col-sm-6" v-if="form_data.THIN_REQ_YN == 'Y'">--%>
<%--			<fm-output2 id="MAC_ADR" name="MAC_ADR" title="<spring:message code="NC_THIN_CLIENT_MAC_ADR" text="MAC" />"></fm-output2>--%>
<%--		</tr>--%>
<%--		<tr class="col col-sm-6" v-if="form_data.THIN_REQ_YN == 'Y'">--%>
<%--			<fm-output2 id="IP" name="IP" title="<spring:message code="NC_THIN_CLIENT_IP" text="IP" />"></fm-output2>--%>
<%--		</tr>--%>
<%--		<tr class="col col-sm-12" v-if="form_data.THIN_REQ_YN == 'Y'"--%>
<%--			 style="border-left-color: transparent;border-right-color: transparent;">--%>
<%--			<div class="fm-output">--%>
<%--				<th class="control-label grid-title value-title" style="background-color:white;">접속 단말 보안 예외 신청 항목</th>--%>
<%--			</div>--%>
<%--		</tr>--%>
<%--		<div class="col col-sm-12" v-if="form_data.THIN_REQ_YN == 'Y'">--%>
<%--			<div class="fm-output">--%>
<%--				<label class="control-label grid-title value-title">NAC 설치</label>--%>
<%--				<div class="output  hastitle">--%>
<%--					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right: 5px;"/><label>NAC 설치 예회 신청</label>--%>
<%--				</div>--%>
<%--			</div>--%>
<%--		</div>--%>
<%--		<div class="col col-sm-12" v-if="form_data.THIN_REQ_YN == 'Y'">--%>
<%--			<div class="fm-output">--%>
<%--				<label class="control-label grid-title value-title">무결성 검사</label>--%>
<%--				<div class="output  hastitle">--%>
<%--					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right: 5px;"/><label>무결성 검사 예외 신청</label>--%>
<%--				</div>--%>
<%--			</div>--%>
<%--		</div>--%>
<%--		<div class="col col-sm-12" v-if="form_data.THIN_REQ_YN == 'Y'">--%>
<%--			<div class="fm-output">--%>
<%--				<label class="control-label grid-title value-title" style="height:180px;">무결성 검사 예외<br/> 보안 프로그램</label>--%>
<%--				<div class="output  hastitle">--%>
<%--					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>백신(V3)</label><br/>--%>
<%--					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>백신(알약)</label><br/>--%>
<%--					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>매체제어(nProtect)</label><br/>--%>
<%--					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>문서보안</label><br/>--%>
<%--					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>출력문보안</label><br/>--%>
<%--					<input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>자산관리</label>--%>
<%--				</div>--%>
<%--			</div>--%>
<%--		</div>--%>
<%--		<div class="col col-sm-12" v-if="form_data.THIN_REQ_YN == 'Y'">--%>
<%--			<fm-output id="THIN_REQ_CMT" name=THIN_REQ_CMT title="<spring:message code="NC_THIN_CLIENT_THIN_REQ_CMT" text="예외사유" />"></fm-output>--%>
<%--		</div>--%>
	</table>
</div>
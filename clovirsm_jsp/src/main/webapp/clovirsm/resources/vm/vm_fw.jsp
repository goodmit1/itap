<%@page contentType="text/html; charset=UTF-8"%><%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="fullGrid" id="input_area" style="height:240px">
	<div class="table_title">
		<div class="btn_group">
			<span class="sub_btn_group">

				<fm-input id="fw_input" class="keyword" name="keyword" div_style="display: inline-block;" onkeydown="fwSearch()"
						  placeHolder="<spring:message code="NC_VM_VM_NM" text="서버명" />/<spring:message code="NC_VM_GUEST_NM" text="OS명" />/<spring:message code="NC_VM_PRIVATE_IP" text="IP" />/<spring:message code="NC_VM_INS_ID_NM" text="담당자" />"
						  title="<spring:message code="keyword_input" text="키워드" />">
				</fm-input>
				<fm-sbutton cmd="search" class="searchBtn" onclick="fwSearch()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
			<fm-popup-button popupid="vm_fw_insert_popup" popup="../../popup/fw_detail_form_popup.jsp?action=insert&callback=vmFWSearch" cmd="update" class="newBtn" param="getNewFWPopupData()"><spring:message code="btn_insert_req" text="생성 요청"/></fm-popup-button>
			<fm-popup-button v-show="false" popupid="vm_fw_insert_save_popup" popup="../../popup/fw_detail_form_popup.jsp?action=insert&callback=vmFWSearch" cmd="update" param="getFWPopupData()"><spring:message code="btn_modify" text="생성 정보 수정"/></fm-popup-button>
			<fm-popup-button v-show="false" popupid="vm_fw_save_popup"  popup="../../popup/fw_detail_form_popup.jsp?action=save&callback=vmFWSearch" cmd="update" param="getFWPopupData()"><spring:message code="btn_modify" text="정보 수정"/></fm-popup-button>
			<fm-popup-button popupid="vm_fw_update_popup" popup="../../popup/fw_detail_form_popup.jsp?action=update&callback=vmFWSearch" cmd="update" class="saveBtn" param="getFWPopupData()"><spring:message code="btn_update_req" text="변경 요청"/></fm-popup-button>
			<fm-popup-button popupid="vm_fw_delete_popup" popup="../../popup/delete_req_form_popup.jsp?svc=fw&key=FW_ID&callback=vmFWSearch" cmd="delete" class="delBtn" param="getFWPopupData()"><spring:message code="btn_delete_req" text="삭제 요청"/></fm-popup-button>
			</span>
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel_fw()" class="btn" id="excelBtn"></button>
			<button type="button" title="<spring:message code="btn_reload" text="새로고침"/>" onclick="vmFWSearch()" class="btn" id="reeloadlBtn"></button>
		</div>
	</div>
	<div id="vmFWGrid" class="ag-theme-fresh" style="height: 200px" ></div>
</div>
<script>
	var vmFWReq = new Req();
	vmFWGrid;

	function getNewFWPopupData(){
		var data = vmFWReq.getData();
		if(data != null) {
			var data1 = {};
			data1.DC_ID = data.DC_ID;
			data1.VM_ID= data.VM_ID;
			data1.VM_NM = data.VM_NM;
			data1.SERVER_IP = data.SERVER_IP;
			data1.PRIVATE_IP = data.PRIVATE_IP;
			data1.VM_USER_YN ='N';
			data1.USE_MM = 12;
			return data1;
		}
		return {};
	}
	function getFWPopupData(){
		var data = vmFWReq.getData();
		if(data != null) {
			delete data.INS_DT;
		}
		return data;
	}

	$(function() {
		var
		fwColumnDefs = [ {
			headerName : 'No',
			hide : false, width:80,
			field : 'FW_ID',
			cellRenderer:function(params)
			{
				return "<a href='#' onclick='openFWPopup(" + params.node.rowIndex + ");return false'>" + (params.node.rowIndex+1) + "</a>"
			}
		},{
			headerName : '',
			hide : true,
			field : 'VM_ID'
		},{
			headerName : '<spring:message code="NC_FW_USER_IP" text="사용자 IP" />',
			field : 'CIDR',
			//maxWidth: 120,
			width: 80,
			tooltip: function(params){
				if(params.data) return params.data.CIDR;
			}
		},{
			headerName : '<spring:message code="allowedPort" text="허용 Port" />',
			field : 'PORT',
			width: 130,
			cellStyle: {'text-align':'right'},
			tooltip: function(params){
				if(params.data) return params.data.PORT;
			}
		},{
			headerName : '<spring:message code="NC_FW_TEAM_NM" text="대상자 팀" />',
			field : 'TEAM_NM',
			maxWidth: 130,
			width: 130,
			tooltip: function(params){
				if(params.data) return params.data.TEAM_NM;
			}
		},{
			headerName : '<spring:message code="NC_FW_INS_NAME" text="대상자" />',
			field : 'USER_NAME',
			maxWidth: 120,
			width: 120,
			tooltip: function(params){
				if(params.data) return params.data.USER_NAME;
			}
		},
		
		<c:if test="${'Y'.equals(sessionScope.VM_USER_YN)}"> {
			headerName : 'OS <spring:message code="NC_FW_VM_USER_YN" text="계정생성여부"/>',
			field : 'VM_USER_YN_NM',
			maxWidth: 180,
			width: 120,
			headerTooltip:'OS <spring:message code="NC_FW_VM_USER_YN" text="계정생성여부"/>',
		},</c:if>
		{
			headerName : '<spring:message code="NC_FW_USE_MM_M" text="사용 기간(개월)" />',
			field : 'USE_MM',
			maxWidth: 140,
			width: 140,
			cellStyle:{'text-align':'right'},
			valueFormatter:function(params){
				return formatNumber(params.value);
			}
		},{
			headerName : '<spring:message code="NC_FW_CUD_CD" text="구분"/>',
			field : 'CUD_CD_NM',
			maxWidth: 180,
			width: 140,
			valueGetter: function(params) {
				return getStatusMsg(params.data);
			}
		},{
			headerName : '<spring:message code="NC_FW_CMT" text="설명" />',
			field : 'CMT',
			maxWidth: 320,
			width: 320,
			valueGetter: function(params) {
		    	return (params.data.FAIL_MSG?params.data.FAIL_MSG + ' ' :'')+ (params.data.CMT?params.data.CMT:'')
			},
			tooltip: function(params){
				if(params.data) return params.data.CMT;
			}
		},{
			headerName : '<spring:message code="NC_FW_INS_DT" text="신청일시"/>',
			field : 'INS_DT',
			maxWidth: 180,
			width: 180,
			valueGetter: function(params) {
		    	return formatDate(params.data.INS_DT,'datetime')
			}
		},];
		var
		fwGridOptions = {
			hasNo : false,
			columnDefs : fwColumnDefs,
			// rowSelection : 'single',
			rowSelection : 'multiple',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onSelectionChanged: function(){
				var arr = vmFWGrid.getSelectedRows();
				var data = arr[0];
				vmFWReq.setData(data);


				setButtonClickable('vm_fw_update_popup_button', !data.CUD_CD   && isRequestable(data));


				setButtonClickable('vm_fw_delete_popup_button', isDeletable(data) || isRequestable(data));

			}
		}
		vmFWGrid = newGrid("vmFWGrid", fwGridOptions);
	});

	function openFWPopup(idx)
	{
		var data = vmFWGrid.getRowData(idx)
		vmFWReq.setData(data);
		$('#vm_fw_save_popup_button').trigger('click');

	}
	function getSelectedFW(){
		var rows = vmFWGrid.getSelectedRows();
		if(rows.length > 0){
			var data = rows[0];
			data.USER_NAME = '';
			return data;
		}
	}

	function onChangedOwner(data){
		var rowIndex = vmFWGrid.getSelectedRowIndex();
		var rowData = vmFWGrid.getRowData(rowIndex);
		for(name in data){
			rowData[name] = data[name];
		}
		rowData.FW_USER_ID = data.USER_ID;
		rowData.INS_NAME = data.USER_NAME;
		vmFWGrid.updateRow(rowData);
	}

	// 조회
	function vmFWSearch() {
		setButtonClickable('vm_fw_insert_popup_button', isOwner(form_data));
		if(vmReq.getData() && vmReq.getData().VM_ID){
			var VM_ID = vmReq.getData().VM_ID;
			setButtonClickable('vm_fw_update_popup_button',false);
			//setButtonClickable('vm_fw_delete_popup_button', false);
			vmFWReq.searchSub('/api/fw/list', { VM_ID: VM_ID }, function(data) {
				vmFWGrid.setData(data);
			});
		}
	}

	// 엑셀 내보내기
	function exportExcel_fw(){
		vmFWGrid.exportCSV({fileName:'FireWall'})
	}

	function fwSearch() {
		var fwValue = document.getElementById('fw_input').value;

		if(event.type === 'click'){
			vmFWGrid.setQuickFilter(fwValue);
			rowSelected(fwValue);
		}
		//13=enter
		if(event.type === 'keydown' && event.keyCode === 13){
			vmFWGrid.setQuickFilter(fwValue);
			rowSelected(fwValue);
		}

	}

	function rowSelected(value) {
		var index;
		if(vmFWGrid.getData() !== null || vmFWGrid.getData() !== undefined){
			var fwDataList =  vmFWGrid.getData();
			if (value !== null || value !== undefined){
				// index = fwDataList.findIndex(data => data.USER_NAME === fwValue)
				index = fwDataList.findIndex(function (data) {
					return data.USER_NAME === value
				})
			}
			index !== -1 ? vmFWGrid.setSelected(index) :  '';
		}
	}


</script>



 <%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="fullGrid" id="input_area" style="height:240px">
	<div class="btn_group">
		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcelHistory()" class="layout excelBtn"></button>
	</div>
	<div id="historyTable" class="ag-theme-fresh" style="height: 200px" ></div>
</div>
<script>
	var historyReq = new Req();
	historyTable;

	$(function() {
		var
		historyColumnDefs = [ {
			headerName : '',
			hide : true,
			field : 'VM_ID'
		},{
			headerName : '<spring:message code="history" text="이력" />',
			field : 'CUD_CD',
			width: 110,
			cellRenderer: function(params) {
				 
				var serverStatus = '';
					if(params.data && params.data.CUD_CD){
						if(params.data.CUD_CD == 'C') serverStatus = '<spring:message code="label_create" text="생성" />';
						else if(params.data.CUD_CD == 'U') serverStatus = '<spring:message code="label_change" text="변경" />';
						else if(params.data.CUD_CD == 'D') serverStatus = '<spring:message code="label_collect" text="회수" />';
						
					}
				return serverStatus;
			}
		},{
			headerName : '<spring:message code="NV_REQ_DETAIL_APPR_STATUS_CD" text="결재상태" />',
			field : 'APPR_STATUS_CD',
			width: 110,
			cellRenderer: function(params) {
				 
				var serverStatus = '';
					if(params.data && params.data.APPR_STATUS_CD){
						if(params.data.APPR_STATUS_CD == 'A') serverStatus = '<spring:message code="btn_approval_accept" text="승인" />';
						else if(params.data.APPR_STATUS_CD == 'D') serverStatus = '<spring:message code="btn_approval_deny" text="반려" />';
						else if(params.data.APPR_STATUS_CD == 'R') serverStatus = '<spring:message code="label_wait" text="승인대기" />';
						else if(params.data.APPR_STATUS_CD == 'W') serverStatus = '<spring:message code="label_save_ongoing" text="변경 요청중" />';
					}
				return serverStatus;
			}
		},{
			headerName : '<spring:message code="NC_REQ_INS_ID_NM" text="요청자" />',
			field : 'USER_NAME',
			width: 110
		},{
			headerName : '<spring:message code="NC_LICENSE_NUMCPUPKGS" text="CPU개수" />',
			field : 'CPU_CNT',
			width: 110,
			cellRenderer: function(params) {
				 
				var serverStatus = params.data.CPU_CNT+' CORE';
				return serverStatus;
			}
		},{
			headerName : '<spring:message code="NC_VM_DISK_CNT" text="DISK" />',
			field : 'DISK_SIZE',
			width: 110,
			cellRenderer: function(params) {
				var serverStatus = params.data.DISK_SIZE+' GB';
				return serverStatus;
			}
		},{
			headerName : '<spring:message code="NV_VM_RAM_CNT" text="MEM" />',
			field : 'RAM_SIZE',
			width: 110,
			cellRenderer: function(params) {
				var serverStatus = params.data.RAM_SIZE+' GB';
				return serverStatus;
			}
		},{
			headerName : '<spring:message code="NC_FW_INS_DT" text="신청일시" />',
			field : 'INS_DT',
			width: 110,
			valueGetter: function(params) {
				if(params.data) return formatDate(params.data.INS_DT,'date')
			}
		} ];
		var
		historyGridOptions = {
			hasNo : true,
			columnDefs : historyColumnDefs,
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false
		}
		historyTable = newGrid("historyTable", historyGridOptions);
	});


	// 조회
	function historySearch() {
		if(vmReq.getData() && vmReq.getData().VM_ID){
			var VM_ID = vmReq.getData().VM_ID;
			historyReq.searchSub('/api/vm/list/select_vm_history/', { VM_ID: VM_ID }, function(data) {
				historyReq.VM_ID = VM_ID;
				historyTable.setData(data);
			});
		}
	}
	function exportExcelHistory(){
		historyTable.exportCSV({fileName:'VM_HISTORY'})
	}

</script>



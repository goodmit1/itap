<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="layout_flex" style="display: flex;">
	<div class="row_flex">
		<div class="tabel_inner_icon"  style="background-color: #fd5d55;">
			<div><img src="/res/img/hynix/cost.png"></div>
			<div class="userinfoName"><spring:message code="label_use_fee" /></div>
			<div class="userinfoPay">
				<span id="ALL_FEE"></span>
				<span class="unit"><spring:message code="label_price_unit" /></span>
			</div>
		</div> 
	</div>
	<div class="row_flex">
		<div class="tabel_inner_icon" style="background-color: #6393ca;">
			<div><img src="/res/img/hynix/server.png"></div>
			<div class="userinfoName"><spring:message code="label_server" /></div>
			<div class="userinfoPay">
				<span id ="VM_COUNT"></span>
				<span class="unit"><spring:message code="label_amount" /></span>
			</div>
		</div> 
	</div>
	<div class="row_flex">
		<div class="tabel_inner_icon" style="background-color: #737984;">
			<div><img src="/res/img/hynix/cpu.png"></div>
			<div class="userinfoName"><spring:message code="label_cpu" text="CPU"/></div>
			<div class="userinfoPay">
				<span id="ALL_CPU_CNT"></span>
				<span class="unit">vCore</span>
			</div>
		</div> 
	</div>
	<div class="row_flex">
		<div class="tabel_inner_icon" style="background-color: #53b1d0;">
			<div><img src="/res/img/hynix/disk.png"></div>
			<div class="userinfoName"><spring:message code="label_disk" /></div>
			<div class="userinfoPay">
				<span id="ALL_DISK_SIZE"></span>
				<span class="unit">TB</span>
			</div>
		</div> 
	</div>
	<div class="row_flex" style="margin-right: 0px;">
		<div class="tabel_inner_icon" style="background-color: #5d6ea3;">
			<div><img src="/res/img/hynix/memory.png"></div>
			<div class="userinfoName"><spring:message code="label_mem" /></div>
			<div class="userinfoPay">
				<span id="ALL_RAM_SIZE"></span>
			    <span class="unit">GB</span>
		    </div>
		</div> 
	</div>
</div>
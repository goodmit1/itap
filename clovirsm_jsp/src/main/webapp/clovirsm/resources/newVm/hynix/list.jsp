<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>

<style>
.stepContents {
    width: 100%;
    display: inline-block;
    margin: auto;
    position: relative;
}
.area_border{
    min-width: 1080px;
    margin: auto;
    width: 100%;
}
.stepContents .click_area {
    border-radius: 4px;
    background-color: #ffffff;
    width: 330px;
    margin-right: 10px;
    margin-bottom: 10px;
    height: 200px;
    display: inline-block;
    float: left;
    padding-bottom: 29px;
    padding-left: 20px;
    cursor: pointer;
    border: 1px solid #d2d4d9;
    position: relative;
}
/* .stepContents > .click_area.top { */
/* 	top: 0px; */
/* } */
/* .stepContents > .click_area.left { */
/* 	left: 0px; */
/* } */
.stepContents .click_area img {
    margin-top: 24px;
    width: 70px;
    height: 70px;
    float: left;
}
.text_arae {
    height: 170px;
    width: 235px;
    position: absolute;
    top: 10px;
    left: 110px;
}
.stepContents .click_area .title_area {
    font-weight: 800;
    text-align: left;
    color: #000;
    margin-top: 20px;
    margin-bottom: 10px;
    font-size: 16px;
}
.stepContents .click_area .cmt_area {
    /* margin-top: 120px; */
    font-size: 14px;
    text-align: left;
}
.stepContents .click_area .cmt_area ul {
    padding-left: 0px;
    list-style: none;
    width: 75%;
    font-size: 12px;
}
.stepContents .click_area .cmt_area li{
	margin-bottom: 0px;
}

.area_border_div_left{
	margin: auto; 
	float: left;
	width: 370px; 
	
}
.area_border_div_right{
	margin: auto; 
	float: left;
	width: 710px;
}
.next_area {
	width: 80px;
    background-color: #fb6c07;
    height: 198px;
    position: absolute;
    top: 0;
    right: 0;
    background-image: url(/res/img/hynix/fill-1.png);
    background-repeat: no-repeat;
    background-position-x: 30px;
    background-position-y: 90px;
    display: none;
}
.stepTitle {
    text-align: left;
    padding: 10px;
    display: inline-block;
}
.selectbox {
    width: 250px;
    display: inline-block;
}
.selectbox select{
    height: 40px;
    border: 1px solid #d2d4d9;
    padding: 5px;
}
.selectDiv{
    display: inline-block;
    text-align: left;
    margin-right: 30px;
    margin-bottom: 10px;
}
.stepTitle span{
    color: #e60724;
}
</style>
<div id="input_area" style="text-align: left; border:none;">
	<div id="step1" class="selectDiv">
		<div class="stepTitle"><span>&#8226;</span>&nbsp<spring:message code="NC_VM_P_KUBUN" text="구분"/></div>
		<div class="selectbox">
		    	<select id="stepOne" onchange="itemCheck(this);">
		    	<option value="" disabled selected hidden><spring:message code="info_select_kubun" text="구분을 선택해주세요."/></option>
		    </select>
		</div>
	</div>
	<div id="step2" class="selectDiv">
		<div class="stepTitle"><span>&#8226;</span>&nbspFAB</div>
		<div class="selectbox">
		    	<select id="stepTwo">
		    	<option value="" disabled selected hidden><spring:message code="info_select_fab" text="FAB을 선택해주세요."/></option>
		    	</select>
		</div>
	</div>
	<div id="step3" class="selectDiv">
		<div class="stepTitle"><span>&#8226;</span>&nbsp<spring:message code="NC_REQ_SVC_CD" text="서비스"/></div>
		
		<div class="stepContents">
			<div class="area_border">
					<div class="click_area top" onclick="itemClick('vm','/clovirsm/resources/vra/newVra/index.jsp');">
						<img src="/res/img/hynix/new.png">
						<div class="text_arae">
							<div class="title_area"><spring:message code="btn_insert_req" text="신규 요청"/></div>
							<div class="cmt_area">
								<ul>
									<li><spring:message code="NEW_VM_CMT_2_1" text="서버의 묶음 생성 요청"/></li>
								</ul>
							</div>
						</div>
						<div class="next_area">
						
						</div>
					</div>
					<div class="click_area left" onclick="itemClick('spec','/clovirsm/resources/vm/changeSpec/index.jsp');">
						<img src="/res/img/hynix/change.png">
						<div class="text_arae">					
							<div class="title_area"><spring:message code="NEW_VM_TITLE_3" text="자원변경 요청"/></div>
							<div class="cmt_area">
								<ul>
									<li><spring:message code="NEW_VM_CMT_3_4" text="CPU, Mem, Disk 사이즈 변경요청"/></li>
								</ul>
								 
							</div>
						</div>
						<div class="next_area">
						
						</div>
					</div>
					<div class="click_area" onclick="itemClick('delete','/clovirsm/resources/vm/delVm/index.jsp');">
						<img src="/res/img/hynix/group-11.png">
						<div class="text_arae">
							<div class="title_area"><spring:message code="btn_delete_req" text="삭제 요청"/></div>
							<div class="cmt_area">
								<ul>
									<li><spring:message code="NEW_VM_CMT_4_1" text="서버 삭제 요청"/></li>
								</ul>
							</div>
						</div>
						<div class="next_area">
						
						</div>
					</div>
					<div class="click_area" id="extendPeriod" onclick="itemClick('extend','/clovirsm/resources/extendPeriod/index.jsp');">
						<img src="/res/img/hynix/group-19.png">
						<div class="text_arae">
							<div class="title_area"><spring:message code="NEW_VM_TITLE_5" text="기간연장 요청"/></div>
							<div class="cmt_area">
								<ul>
									<li><spring:message code="NEW_VM_CMT_5_1" text="서버 사용 기간 연장 요청"/></li>
								</ul>
							</div>
						</div>
						<div class="next_area">
						
						</div>
					</div>
					<div class="click_area top" onclick="itemClick('recovery','/clovirsm/resources/recovery/index.jsp');">
						<img src="/res/img/hynix/group-18.png">
						<div class="text_arae">
							<div class="title_area"><spring:message code="title_server_reuse" text="서버 복원"/></div>
							<div class="cmt_area">
								<ul>
									<li><spring:message code="NEW_VM_CMT_6_1" text="서버의 복구 요청"/></li>
								</ul>
							</div>
						</div>
						<div class="next_area">
						
						</div>
					</div>
					<div class="click_area top" onclick="itemClick('templateAdd','/clovirsm/workflow/templateAdd/index.jsp')">
						<img src="/res/img/hynix/template.png">
						<div class="text_arae">
							<div class="title_area"><spring:message code="NEW_VM_CMT_8" text="카탈로그 추가 요청"/></div>
							<div class="cmt_area">
								<ul>
									<li><spring:message code="NEW_VM_CMT_8_1" text="신규요청에 없는 카탈로그 추가 요청"/></li>
								</ul>
							</div>
						</div>
						<div class="next_area">
						
						</div>
					</div>
			</div>
		</div>
	</div>
</div>
<script>

function itemClick(name,url){
	var stepOne = $("#stepOne").val();
	var stepTwo = $("#stepTwo").val();
	if( stepOne == null ){
		alert("<spring:message code="info_select_kubun" text="구분을 선택해주세요."/>");
		$("#stepOne").focus();
		return ;		
	}
	if( stepTwo == null){
		alert("<spring:message code="info_select_fab" text="FAB을 선택해주세요."/>");
		$("#stepTwo").focus();
		return ;		
	}
	
	if(name == 'vm')
		location.href= url+"?P_KUBUN="+stepOne+"&FAB="+stepTwo;
	else
		location.href= url+"?P_KUBUN="+stepOne+"&keyword="+stepTwo;
}
function callback() {
	 
}
function DropdataSelectBox(url,id){
	post(url,{},function (data){
		for(i in data){
			$("#"+id).append("<option class='stepItem' value='"+i+"' id='"+i+"'>"+data[i]+"</option>");
		}
	});
}

function itemCheck(that){
	if($(that).val() == 'prd' ){
		$("#extendPeriod").hide();
	} else{
		$("#extendPeriod").show();
	}
}

function dataInit(){
	DropdataSelectBox("/api/code_list?grp=P_KUBUN","stepOne");
	DropdataSelectBox("/api/code_list?grp=FAB","stepTwo");
}

$(document).ready(function(){
	dataInit();
	
	$(".stepContents").on("click",'.stepItem',function(){
		var parentId = $(this).parent().attr("id");
			$("#"+parentId).children().removeClass("active");
			$(this).addClass("active");
	});
});

</script>
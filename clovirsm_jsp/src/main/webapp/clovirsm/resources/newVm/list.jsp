<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%
	boolean vra = PropertyManager.getBoolean("clovirsm.display.vra", false);
	session.setAttribute("vra", vra);
%>
<div id="input_area" style="text-align: center; border:none;">
	<div class="parent_area">
		<div class="area_border">
		 <c:choose>
			 <c:when test="${sessionScope.vra}">
				<div class="click_area top" onclick="vraClick();">
					<img src="/res/img/hynix/new.png">
					<div class="text_arae">
						<div class="title_area"><spring:message code="new_service" text="신규 서비스"/></div>
						<div class="cmt_area">
							<ul>
								<li><spring:message code="NEW_VM_CMT_2_1" text="서버의 묶음 생성 요청"/></li>
							</ul>
						</div>
					</div>
					<div class="next_area">
					
					</div>
				</div>
				</c:when>
				<c:otherwise>
				<div class="click_area top" onclick="vmClick();">
					<img src="/res/img/hynix/new.png">
					<div class="text_arae">
						<div class="title_area"><spring:message code="new_service" text="신규 서비스"/></div>
						<div class="cmt_area">
							<ul>
								<li><spring:message code="NEW_VM_CMT_2_1" text="서버의 묶음 생성 요청"/></li>
							</ul>
						</div>
					</div>
					<div class="next_area">
					
					</div>
				</div>
				</c:otherwise>
				 </c:choose>
 				<div class="click_area left" onclick="editClick();">
					<img src="/res/img/hynix/change.png">
					<div class="text_arae">					
						<div class="title_area"><spring:message code="NEW_VM_TITLE_3" text="자원 변경"/></div>
						<div class="cmt_area">
							<ul>
								<li><spring:message code="NEW_VM_CMT_3_4" text="CPU, Mem, Disk 사이즈 변경요청"/></li>
							</ul>
							 
						</div>
					</div>
					<div class="next_area">
					
					</div>
				</div>
				<div class="click_area" onclick="delClick();">
					<img src="/res/img/hynix/group-11.png">
					<div class="text_arae">
						<div class="title_area"><spring:message code="NEW_VM_TITLE_4" text="삭제 요청"/></div>
						<div class="cmt_area">
							<ul>
								<li><spring:message code="NEW_VM_CMT_4_1" text="서버 삭제 요청"/></li>
							</ul>
						</div>
					</div>
					<div class="next_area">
					
					</div>
				</div>
				<div class="click_area" onclick="extendPeriodClick();">
					<img src="/res/img/hynix/group-19.png">
					<div class="text_arae">
						<div class="title_area"><spring:message code="NEW_VM_TITLE_5" text="기간연장 요청"/></div>
						<div class="cmt_area">
							<ul>
								<li><spring:message code="NEW_VM_CMT_5_1" text="서버 사용 기간 연장 요청"/></li>
							</ul>
						</div>
					</div>
					<div class="next_area">
					
					</div>
				</div>
				<div class="click_area top" onclick="vmRecoveryClick();">
					<img src="/res/img/hynix/group-18.png">
					<div class="text_arae">
						<div class="title_area"><spring:message code="title_server_reuse" text="서버 복원"/></div>
						<div class="cmt_area">
							<ul>
								<li><spring:message code="NEW_VM_CMT_6_1" text="서버의 복구 요청"/></li>
							</ul>
						</div>
					</div>
					<div class="next_area">
					
					</div>
				</div>
		</div>
	</div>
</div>
<style>
.parent_area {
    width: 100%;
    height: 740px;
    display: inline-block;
    margin: auto;
    position: relative;
}
.area_border{
    min-width: 1080px;
    margin: auto;
    height: 700px;
    width: 100%;
}
.parent_area .click_area {
    border-radius: 4px;
    background-color: #ffffff;
    width: 440px;
    margin-left: 10px;
    margin-bottom: 10px;
    height: 200px;
    display: inline-block;
    float: left;
    padding-bottom: 29px;
    padding-left: 20px;
    cursor: pointer;
    border: 1px solid #d2d4d9;
    position: relative;
}
/* .parent_area > .click_area.top { */
/* 	top: 0px; */
/* } */
/* .parent_area > .click_area.left { */
/* 	left: 0px; */
/* } */
.parent_area .click_area img {
    margin-top: 24px;
    width: 70px;
    height: 70px;
    float: left;
}
.text_arae {
    height: 170px;
    width: 235px;
    position: absolute;
    top: 10px;
    left: 110px;
}
.parent_area .click_area .title_area {
    font-weight: 800;
    text-align: left;
    color: #000;
    margin-top: 20px;
    margin-bottom: 10px;
    font-size: 16px;
}
.parent_area .click_area .cmt_area {
    /* margin-top: 120px; */
    font-size: 14px;
    text-align: left;
}
.parent_area .click_area .cmt_area ul {
    padding-left: 0px;
    list-style: none;
    width: 95%;
    font-size: 12px;
}
.parent_area .click_area .cmt_area li{
	margin-bottom: 0px;
}

.area_border_div_left{
	margin: auto; 
	float: left;
	width: 370px; 
	
}
.area_border_div_right{
	margin: auto; 
	float: left;
	width: 710px;
}
.next_area {
	width: 80px;
    background-color: #00AC4F;
    height: 198px;
    position: absolute;
    top: 0;
    right: 0;
    background-image: url(/res/img/hynix/fill-1.png);
    background-repeat: no-repeat;
    background-position-x: 30px;
    background-position-y: 90px;
}
</style>
<script>
function customClick() {
	location.href="/clovirsm/resources/vra/newVra/index.jsp?KUBUN=C";
}
function vraClick() {
	location.href="/clovirsm/resources/vra/newVra/index.jsp";
}
function vmClick() {
	location.href="/clovirsm/resources/vm/newVm/index.jsp";
}
function editClick() {
	location.href="/clovirsm/resources/vm/changeSpec/index.jsp";
}
function delClick() {
	location.href="/clovirsm/resources/vm/delVm/index.jsp";
}
function extendPeriodClick() {
	location.href="/clovirsm/resources/extendPeriod/index.jsp";
}
function vmRecoveryClick() {
	location.href="/clovirsm/resources/recovery/index.jsp";
}
function callback() {
	 
}
</script>
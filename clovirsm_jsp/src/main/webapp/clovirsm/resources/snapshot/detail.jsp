<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- 입력 Form -->
	<div class="form-panel detail-panel panel panel-default">

			<div class="panel-body">



					<div class="col col-sm-4">
						<fm-output id="SNAPSHOT_NM" name="SNAPSHOT_NM"  required="true" title="<spring:message code="NC_SNAPSHOT_SNAPSHOT_NM" text="스냅샷명" />"></fm-output>
					</div>
					<div class="col col-sm-4">
						<fm-output id="DC_NM"  name="DC_NM"    title="<spring:message code="NC_DC_DC_NM" text="데이터센터명" />"  ></fm-output>
					</div>
					<div class="col col-sm-4">
						<fm-output id="VM_NM"   name="VM_NM"   title="<spring:message code="NC_VM_VM_NM" text="서버명" />"  ></fm-output>
					</div>
					<div class="col col-sm-4">
						<fm-output id="USER_NAME"   name="USER_NAME"   title="<spring:message code="FM_TEAM_USER_NAME" text="담당자" />"  ></fm-output>
					</div>
					<div class="col col-sm-4">
						<fm-output id="INS_TMS"   name="INS_TMS"   title="<spring:message code="INS_TMS" text="등록일시" />"  ></fm-output>
					</div>
					<div class="col col-sm-4">
						<fm-output id="CMT"   name="CMT" :value="form_data.CMT "  title="<spring:message code="CMT" text="설명" />"  ></fm-output>
					</div>
			</div>

	</div>
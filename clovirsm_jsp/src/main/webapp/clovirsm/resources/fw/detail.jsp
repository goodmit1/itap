<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 입력 Form -->
<div class="table_title"> 
		<div class="btn_group">
			<fm-popup-button    popupid="fw_save_popup"   popup="../../popup/fw_detail_form_popup.jsp?action=save&callback=fwSearch" cmd="update" class="saveBtn" param="fwReq.getData()" disabled="disabled"><spring:message code="btn_modify" text="정보 수정"/></fm-popup-button>
		</div>
	</div>
<div class="form-panel detail-panel panel panel-default">
	
	<div class="panel-body ">
		<div class="col col-sm-6">
			<fm-output id="DC_NM" name="DC_NM"  required="true" title="<spring:message code="NC_DC_DC_NM" text="데이터 센터" />"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="VM_NM"  name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="서버명" />"  ></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="SERVER_IP"  name="SERVER_IP" title="<spring:message code="NC_FW_PUBLIC_IP" text="서버IP" />"  ></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="CIDR"  name="CIDR" title="<spring:message code="NC_FW_CIDR" text="대상IP" />:<spring:message code="NC_FW_PORT" text="Port" />" :value="form_data.CIDR != undefined ? (form_data.PORT != '' ? form_data.CIDR + ':' + form_data.PORT : '') : '' "></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="TEAM_NM"  name="TEAM_NM" title="<spring:message code="NC_FW_TEAM_NM" text="사용자팀" />"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="USER_NAME"  name="USER_NAME" title="<spring:message code="NC_FW_USER_NAME" text="사용자" />"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="TASK_STATUS_CD_NM"  name="TASK_STATUS_CD_NM" title="<spring:message code="NC_IMG_TASK_STATUS_CD_NM" text="작업상태" />" :value="getStatusMsg(form_data)"  :ostyle="getCUDCDStyle(form_data)"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="USE_MM" name="USE_MM" title="<spring:message code="NC_FW_USE_MM_M" text="사용기간(개월)" />" :value="getUseMMAndExpireDT(form_data, '<spring:message code="dt_month"/>','<spring:message code="dt_expiredate"/>')" :ostyle="getUseMMStyle(form_data)"></fm-output>
		</div>
		<div class="col col-sm-12">
			<div style="height: 200px;">
				<label for="CMT" class="control-label grid-title value-title"><spring:message code="NC_FW_CMT" text="설명" /></label>
				<div v-html="form_data.CMT" class="output hastitle" style="min-height: 200px;overflow-y: auto;word-break: break-word;"></div>
			</div>
		</div>
		 
		<!-- <div class="col col-sm-6">
			<fm-output id="INS_TEAM_NM"  name="INS_TEAM_NM" title="<spring:message code="NC_FW_INS_TEAM_NM" text="담당자팀" />"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="INS_NAME"  name="INS_NAME" title="<spring:message code="NC_FW_INS_NAME" text="담당자" />"></fm-output>
		</div>
		 -->
	</div>
</div>
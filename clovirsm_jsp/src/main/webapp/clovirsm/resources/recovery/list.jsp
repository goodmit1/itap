<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page import="com.fliconz.fm.mvc.util.MsgUtil"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>

<%
String VM_NM = request.getParameter("VM_NM");
request.setAttribute("VM_NM", VM_NM == null ? "null" : "'" + VM_NM + "'");
String DC_ID = request.getParameter("DC_ID");
request.setAttribute("DC_ID", DC_ID == null ? "null" : "'" + DC_ID + "'");
 
%>
<style>


</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<c:if test="${sessionScope.GRP_ADMIN_YN == 'Y'}">
			<div class="col col-sm">
				<fm-select url="/api/etc/team_list" id="S_TEAM_CD" emptystr=" " name="TEAM_CD" title="<spring:message code="FM_TEAM_TEAM_CD" text="팀" />"></fm-select>
			</div>
		<c:if test="${ADMIN_YN == 'Y'}">
			<div class="col col-sm">
				<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="S_DC_ID"
					emptystr=" " keyfield="DC_ID" titlefield="DC_NM"
					name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
				</fm-select>
			</div>
		</c:if>
		</c:if>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=P_KUBUN" id="S_P_KUBUN" emptystr=" "
				name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>">
			</fm-select>
		</div>
		<div class="col col-sm-3" style="width: 13.666667%;min-width:240px;">
				<fm-ibutton id="S_CATEGORY_NM" name="CATEGORY_NM" title="<spring:message code="TASK"/>" class="inline">
					<fm-popup-button popupid="category1" popup="/clovirsm/popup/img_category_search_form_popup.jsp" cmd="update" param="false" callback="select_S_CATEGORY"></fm-popup-button>
				</fm-ibutton>
				<input type="text" style="display:none" id="S_CATEGORY" name="CATEGORY">
		</div>
		<div class="col col-sm">
			<fm-input id="keyword_input" class="keyword" name="keyword" input_style="width:170px;"
				placeHolder="<spring:message code="NC_VM_VM_NM" text="서버명" />/<spring:message code="NC_VM_GUEST_NM" text="OS명" />/<spring:message code="NC_VM_PRIVATE_IP" text="IP" />/<spring:message code="NC_VM_INS_ID_NM" text="담당자" />"
				title="<spring:message code="keyword_input" text="키워드" />">
			</fm-input>
		</div>
		
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="vmSearch()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
		<div id="popup-button-html">
		</div>
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:auto;">
	<div class="table_title layout name">
		<%-- <spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp;(&nbsp;<spring:message code="count" text="건수"/>&nbsp;:&nbsp;<span id="mainTable_total">0</span>&nbsp;)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
			<input type="button" class="btn delBtn contentsBtn tabBtnImg save" value="<spring:message code="server_recovery_req" text="서버 복원 요청" />" onclick="click_reqRecovery();" />
			<fm-popup-button style="display:none;" popupid="vmExpire_popup" popup="/clovirsm/popup/vm_recovery_popup.jsp?isAppr=N" cmd="" param="reqList" callback=""></fm-popup-button>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 600px" ></div>
	</div>

</div>
<script>
	var vmReq = new Req();
	mainTable;
	var reqList;

	$(function() {
		var
		columnDefs =[
			 
		{
			headerName : '<spring:message code="NC_VM_VM_NM" text="서버명" />',
			field : 'VM_NM',
			width: 180,
			minWidth: 150,
			tooltip: function(params){
				if(params.data) return params.data.VM_NM;
			}
		},{
			headerName : '<spring:message code="NC_VM_P_KUBUN" text="구분" />',
			field : 'P_KUBUN_NM',
			maxWidth: 120,
			sort_field: 'P_KUBUN',
			width: 120,
			minWidth: 120
		},{
			headerName : '<spring:message code="label_spec" text="Spec" />',
			field : 'SPEC_INFO',
			maxWidth: 140,
			width: 140,
			minWidth: 140
		},{
			headerName : '<spring:message code="NC_OS_TYPE_GUEST_NM" text="OS명" />',
			field : 'GUEST_NM',
			maxWidth: 400,
			width: 280,
			minWidth: 280,
			tooltip: function(params){
				if(params.data) return params.data.GUEST_NM;
			}
		},{
			headerName : '<spring:message code="NC_VM_PRIVATE_IP" text="IP" />',
			field : 'PRIVATE_IP',
			maxWidth: 140,
			width: 140,
			minWidth: 140
		},{
			headerName : '<spring:message code="TASK" text="업무명" />',
			field : 'CATEGORY_NM',
			maxWidth: 180,
			width: 120,
			minWidth: 120
		}
// 		,{
// 			headerName : '<spring:message code="EXPIRE_DT" text="만료일"/>',
// 			field : 'EXPIRE_DT',
// 			width: 140,
// 			minWidth: 140,
// 			format:'datetime'
			 
// 		}
		,{
			headerName : '<spring:message code="NC_VM_INS_TMS" text="생성일시"/>',
			field : 'INS_TMS',
			width: 140,
			minWidth: 110,
			valueGetter: function(params) {
				if(params.data) return formatDate(params.data.INS_TMS,'date')
			}
		},{
			headerName : '<spring:message code="due_deletion" text="삭제 예정일"/>',
			field : 'DUE_DELETE',
			width: 140,
			minWidth: 110,
			valueGetter: function(params) {
				if(params.data) return formatDate(params.data.DUE_DELETE,'date')+"(D-"+params.data.DUE_COUNT+")";
			}
	
		}, /*{
			headerName : '<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />',
			field : 'TEAM_NM',
			maxWidth: 120,
			width: 120,
			minWidth: 120
		},{
			headerName : '<spring:message code="FM_TEAM_INS_ID_NM" text="담당자" />',
			field : 'INS_ID_NM',
			maxWidth: 120,
			width: 120,
			minWidth: 100
		}*/ ];
		var gridOptions = {
			columnDefs: columnDefs,
			rowData: [],
			sizeColumnsToFit:true,
			enableSorting: true,
			rowSelection:'single',
			checkboxSelection: true,
		    enableColResize: true,
		    onRowSelected: function(){
				var arr = mainTable.getSelectedRows();
				 
				reqList = arr;
		    }
		};
		mainTable = newGrid("mainTable", gridOptions);
		 
		vmSearch();
		
	});
	function click_reqRecovery(){
		if(mainTable.getSelectedRows().length != 0){
			var data = mainTable.getSelectedRows()[0];
			reqList = data;
			 
			$("#vmExpire_popup_button").click();
		} else{
			alert(msg_select_first);
		}
	}
	// 조회
	function vmSearch(callback) {
		searchvue.form_data.MONITOR = "Y";
		searchvue.form_data.RECOVERY= "Y";
		searchvue.form_data.EXTEND_PERIOD= "Y";
		vmReq.searchSub('/api/vm/list', searchvue.form_data, function(data) {
			mainTable.setData(data);
		});
	}
	
	function select_S_CATEGORY(data){
		searchvue.form_data.CATEGORY = data.CATEGORY;
		searchvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
		$("#S_CATEGORY_NM").val(data.CATEGORY_NM);
	}

	// 엑셀 내보내기
	function exportExcel(){
		mainTable.exportCSV({fileName:'vm'})
		//exportExcelServer("mainForm", '/api/vm/list_excel', 'Snapshot',mainTable.gridOptions.columnDefs, vmReq.getRunSearchData())
	}

</script>



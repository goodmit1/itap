<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="col col-sm-12 ${param.name}-area">
	<div class="hastitle ">
		<label for="${param.name}_DATA_DISK_TYPE"
			class="control-label grid-title value-title stepLabel" id="disLabel"
			style="vertical-align: top;"> <spring:message code="ADD_DISK" text="추가 디스크" />(GB)
			<button type="button" class="btn btn-primary btn update"
				onclick="${param.name}_newDisk()" title="btn_new">
				<i class="fa fa-plus"></i>
			</button>
		</label>
		<div class="hastitle output" style="width: calc(100% - 239px); height: 100%; padding: 0px; float: right;">
			<table id="${param.name}_data_disk_tbl" style="margin-top: 7px;">
				<tr style="line-height:30px">
					<td> 
						<select id="${param.name}_DATA_DISK_SIZE_sel"
							name="${param.name}_DATA_DISK_SIZE" 
							class="form-control input hastitle stepInput diskSelect" style="width:100px;" >
						</select>
						<input id="${param.name}_DATA_DISK_SIZE_input" 
							name="${param.name}_DATA_DISK_SIZE" 
							class="form-control input hastitle stepInput diskSelect" style="width:100px;" >
						 
					</td>
					 
					<td style="padding-left:10px" class="mountPathArea" >
						<input class="form-control input hastitle stepInput DATA_DISK_NAME"
						 style="vertical-align:middle;width:300px" placeholder="Mount Path 예)/data(리눅스), D(윈도우)"
						 onkeyup="chk_${param.name}(this)">
						</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<script>
var p_${param.name}={height : 42.99 , linuxYN: 'Y'  }
function set_${param.name}(){
	var json = ${param.popupid}_param.DATA_JSON;
	try{ 
		var arr1 = json["${param.name}"];
		if(arr1){
			 
			for(var i=0; i<arr1.length; i++){
				var arr2 = arr1[i].split("=");
				if(i>1){
					 ${param.name}_newDisk( )
				}
				var trObj = $("#${param.name}_data_disk_tbl tbody tr")[i];
				$(trObj).find(".diskSelect").val(arr2[1])
				$(trObj).find(".DATA_DISK_NAME").val(arr2[0])
				 
			}
		}
		
		if(yn==null){
			 p_${param.name}.linuxYN = getLinuxYN(${param.stepIdx}-1)
		}
	}
	catch(e){}
}
set_${param.name}();
 
function ${param.name}_newDisk( ){
		p_${param.name}.height += 28.99;
		var tbl = $("#${param.name}_data_disk_tbl tbody  ");
		var trObj = $("<tr>" +  $("#${param.name}_data_disk_tbl tbody tr:first").html()  + "</tr>");
		$("#disLabel").css("height",p_${param.name}.height);
		tbl.append(trObj); 
			trObj.find("td:last-child").append('<button type="button" class="btn btn-primary btn update" onclick="${param.name}_delDisk(this)" title="btn_del"><i class="fa fa-minus"></i></button>');
	
		return trObj;

	}
function ${param.name}_delDisk(obj){
		p_${param.name}.height -= 28.99;			
		$("#disLabel").css("height",p_${param.name}.height);
		$(obj).parents("tr:first").remove();
		 
	}
	$(document).ready(function(){
		if(getKubunVal()==null){
			$("#${param.name}_DATA_DISK_SIZE_sel").remove();
		}
		else{
			$("#${param.name}_DATA_DISK_SIZE_input").remove();
		}
	})
function chk_${param.name}(obj){
	 p_${param.name}.linuxYN = getLinuxYN(${param.stepIdx})
 	 if(p_${param.name}.linuxYN=='N')
 	 {
	 		return chkWinDriver(obj.value);
	 }
	 return true;
}	
function get_${param.name}(){
	 var trs = $("#${param.name}_data_disk_tbl tr");
	 var result = [];
	
	 for(var i=0; i < trs.length; i++){
	 	var size = $(trs[i]).find(".diskSelect").val();
	 	if(!size || size=='') continue;
	 	result.push( $(trs[i]).find(".DATA_DISK_NAME").val() + "=" + size);
	 }
	 return result;
}

</script>
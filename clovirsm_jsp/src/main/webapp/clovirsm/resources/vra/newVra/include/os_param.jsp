<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="col col-sm-12  ${param.name}-area" id="os_local_user-option">
<div ><label for="os_local_user" class="control-label grid-title value-title stepLabel"><spring:message code="local_account_id" text="로컬 계정 ID" /></label><input name="os_local_user" 
value="" id="os_local_user" placeholder="<spring:message code="msg_account_id" text="신규계정ID를 입력해 주세요" />" class="form-control input hastitle stepInput">
</div>
</div>

<div class="col col-sm-12  ${param.name}-area"  id="os_ad_user-option"  >
<div  ><label for="os_ad_user" class="control-label grid-title value-title stepLabel"><spring:message code="domain_account_id" text="도메인 계정 ID" /></label><input name="os_ad_user" 
	value="" id="os_ad_user" placeholder="<spring:message code="msg_domin_id" text="기존 도메인 계정ID를 입력해 주세요" />" class="form-control input hastitle stepInput">
</div>
</div>
<script>
function set_${param.name}(){
	 
	var json = ${param.popupid}_param.DATA_JSON;
	try{ 
		var val1 = json["${param.name}"];
		if(val1){
			 
			for(var i=0; i<val1.length; i++){
				var arr2 = val1[i].split("=");
				if($("#" + arr2[0]).val() !=''){
					$("#" + arr2[0]).val($("#" + arr2[0]).val( ) + "," + arr2[1]);
				}
				else{
					$("#" + arr2[0]).val(  arr2[1]);
				}
			}
		}
	}
	catch(e){}
	if(getLinuxYN(${param.stepIdx}) != 'N'){
		$("#os_ad_user-option").hide();
	}	
}  
 

set_${param.name}()

$("#step${param.stepIdx} .imgSelect").change(function(obj){
	 
	if(getLinuxYN(${param.stepIdx}) != 'N'){
		$("#os_ad_user-option").hide();
	}
	else{
		$("#os_ad_user-option").show();
	}	
});

function get_${param.name}(){
	var user = $("#os_local_user").val();
	var arr = user.split(",")
	var val = [];
	for(var i=0; i< arr.length; i++){
		if(arr[i])
			val.push("os_local_user=" + arr[i]);
	}
	 
	if(getLinuxYN(${param.stepIdx}) == 'N'){
		user = $("#os_ad_user").val();
		arr = user.split(",")
	 
		for(var i=0; i< arr.length; i++){
			if(arr[i])
				val.push("os_ad_user=" + arr[i]);
		}
	 
	}
	return val;
}

</script>
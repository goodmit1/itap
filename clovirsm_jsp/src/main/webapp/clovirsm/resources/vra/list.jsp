<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<style>
	#mainTable { height:calc(100vh - 220px); }
	#S_REQ_TITLE {width : 200px}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<jsp:include page="/clovirsm/popup/_FAB_serach_.jsp"></jsp:include>
		<div class="col col-sm">
			<fm-input id="S_REQ_TITLE" name="REQ_TITLE" title="<spring:message code="REQ_TITLE" text="요청명" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=P_KUBUN" emptystr=" " id="S_P_VM_KUBUN" name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-select2 url="/api/vra_catalog/list/list_NC_VRA_CATALOG/" id="S_CATALOG_ID" emptystr=" "
						keyfield="CATALOG_ID" titleField="CATALOG_NM"
						name="CATALOG_ID" title="<spring:message code="tab_img" text="템플릿"/>" select_style="width:150px;">
			</fm-select2>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=TASK_STATUS_CD" id="S_TASK_STATUS_CD" name="TASK_STATUS_CD" emptystr=" " title="<spring:message code="TASK_STATUS_CD" text="" />" ></fm-input>
		</div>
		<c:if test="${ADMIN_YN == 'Y'}">
			<div class="col col-sm" style="width: 267px;">
				<fm-ibutton id="S_CATEGORY_NM" name="CATEGORY_NM" title="<spring:message code="TASK"/>" class="inline">
					<fm-popup-button popupid="s_category" popup="/clovirsm/popup/img_category_search_form_popup.jsp" cmd="update" param="false" callback="select_S_CATEGORY"></fm-popup-button>
				</fm-ibutton>
				<input type="text" style="display:none" id="S_CATEGORY1" name="CATEGORY">
			</div>
		</c:if>
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text="조회"/></fm-sbutton>
		</div>
		<!-- 		<div class="col btn_group_under"> -->
					
		<%-- <fm-sbutton cmd="update" class="newBtn" onclick="location.href='/clovirsm/resources/vra/newVra/index.jsp'"   ><spring:message code="btn_new" text="신규"/></fm-sbutton> --%>
		<%-- 			<fm-popup-button popupid="delete_req_form_popup" popup="delete_req_form_popup.jsp" cmd="delete" class="delBtn" param="req.getData()"><spring:message code="btn_delete_req" text="삭제요청"/></fm-popup-button> --%>
		<!-- 		</div> -->
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
		<div class="table_title layout name">
			<%-- <spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>
			<div class="btn_group">
				<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
			</div> --%>
			<div class="search_info">
				<spring:message code="mainTable_Search_information" text="검색정보" />
				<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
			</div>
			<div class="btn_group">
				<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
				<%-- 			<fm-popup-button popupid="vra_change_form_popup" popup="input.jsp" cmd="update" class="saveBtn contentsBtn tabBtnImg change" param="req.getData()"><spring:message code="btn_modify" text="정보 수정"/></fm-popup-button> --%>
				<%-- <fm-sbutton cmd="update" class="newBtn" onclick="location.href='/clovirsm/resources/vra/newVra/index.jsp'"   ><spring:message code="btn_new" text="신규"/></fm-sbutton> --%>
				<c:if test="${ADMIN_YN == 'Y'}">
					<fm-popup-button id="changeOwner" popupid="vra_owner_change_form_popup" popup="/popup/owner_change_form_popup.jsp?svc=vra_catalog&key=CATALOGREQ_ID&callback=search" cmd="update" class="saveBtn" param="req.getData()" ><spring:message code="btn_change_owner" text="담당자변경"/></fm-popup-button>
					<fm-sbutton id="resendMail"  class="btn btn-primary contentsBtn tabBtnImg save" cmd="update" onclick="reSendMail()"  ><spring:message code="re_mail" text="메일 재전송"/></fm-sbutton>
					<fm-popup-button id="deleteReq" popupid="delete_req_form_popup" popup="delete_req_form_popup.jsp" cmd="delete" class="delBtn contentsBtn tabBtnImg del" param="req.getData()"><spring:message code="btn_delete_req" text="삭제요청"/></fm-popup-button>
					<fm-sbutton title="vra와 상태가 일지하지 않을 때 사용" id="syncStatus" class="btn btn-primary contentsBtn tabBtnImg sync" cmd="update" onclick="statusSync()"  ><spring:message code="btn_status_sync" text="상태 Sync"/></fm-sbutton>
				</c:if>
				<%-- 				<fm-popup-button popupid="update_req_form_popup" popup="/clovirsm/popup/new_vra_popup.jsp?action=update" cmd="update" class="updateBtn contentsBtn tabBtnImg save" param="req.getData()"><spring:message code="btn_update_req" text="변경 요청"/></fm-popup-button> --%>
			</div>
		</div>
		<div class="layout background mid">
			<div id="mainTable" class="ag-theme-fresh" style="height: 260px" ></div>
		</div>
	</div>
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab1" data-toggle="tab" onclick="setTabIdx( 1)"><spring:message code="title_subTable" text="상세정보"/></a>
		</li>
		<li>
			<a href="#tab2" data-toggle="tab" onclick="setTabIdx( 2)"><spring:message code="tab_server" text="서버"/></a>
		</li>
		<!--
        <li >
             <a href="#tab3" data-toggle="tab" onclick="setTabIdx( 3)">N/W Traffic</a>
        </li>
         -->
	</ul>
	<div class="tab-content " style="height:420px">
		<div class="tab-buttons">
		</div>
		<div class="form-panel detail-panel tab-pane active" id="tab1">
			<jsp:include page="detail.jsp"></jsp:include>
		</div>
		<div class="form-panel detail-panel tab-pane" id="tab2">
			<jsp:include page="vm_list.jsp"></jsp:include>
		</div>
		<!-- <div class="form-panel detail-panel tab-pane" id="tab3" >
				<jsp:include page="nw_traffic.jsp"></jsp:include> 
			</div>
			 -->
	</div>
</div>
<script>
	var req = new Req();
	var mainTable;
	var info = null;
	var vmid = "";
	$(function() {
		var columnDefs = [
			{headerName : "FAB",field : "FAB", width: 100, minWidth: 100},
			{headerName : "<spring:message code="REQ_TITLE" text="요청명" />",field : "REQ_TITLE",tooltipField:"REQ_TITLE", width: 250, minWidth: 250},
			{headerName : "<spring:message code="NC_VM_P_KUBUN" text="구분" />",field : "P_KUBUN_NM",tooltipField:"P_KUBUN_NM", width: 120, minWidth: 120},
			{headerName : "<spring:message code="TASK" text="업무" />",width:200, field : "CATEGORY_NM", width: 250, minWidth: 250},
			{headerName : "<spring:message code="tab_img" text="" />", field : "CATALOG_NM", width:250, minWidth: 250},
			{headerName : "<spring:message code="TASK_STATUS_CD" text="" />",field : "APPR_STATUS_CD_NM", width:100, minWidth: 100, valueGetter:function(params){
					return getStatusMsg(params.data)
				}},
			{headerName : "<spring:message code="dt_expiredate" text="" />",field : "EXPIRE_DT", width:250, minWidth: 250, format:'datetime'},
			{headerName : "<spring:message code="vm_use" text="VM갯수" />",field : "USE_VM", width: 120, minWidth: 120 },
			{headerName : "<spring:message code="NC_VRA_CATALOGREQ_USER_NAME" text="" />",field : "USER_NAME", width: 150, minWidth: 150},
			{headerName : "<spring:message code="NC_VM_INS_TMS" text="" />",field : "INS_TMS", width: 200, minWidth: 200, format:'datetime'},
		];
		var
				gridOptions = {
					hasNo : true,
					columnDefs : columnDefs,
					//rowModelType: 'infinite',
					rowSelection : 'single',
					cacheBlockSize: 100,
					rowData : [],
					enableSorting : true,
					enableColResize : true,
					enableServerSideSorting: false,
					onSelectionChanged : function() {
						var arr = mainTable.getSelectedRows();
						req.getInfo('/api/vra_catalog/info?CATALOGREQ_ID='+ arr[0].CATALOGREQ_ID, function(data){
							info = data;
							console.log
							if(arr[0].TASK_STATUS_CD == "S"){
								$("#resendMail").show();
								$("#syncStatus").show();
								$("#changeOwner").show();
							} else if(arr[0].TASK_STATUS_CD == "W"){
								$("#syncStatus").hide();
							} else{
								$("#changeOwner").hide();
								$("#resendMail").hide();
								$("#syncStatus").show();
							}
							if(arr[0].P_KUBUN == 'prd'){
								$("#deleteReq").show();
							}
							
							if(isSavable(data)){
								$("#delete_req_form_popup").prop("disabled", false);
								$("#vra_owner_change_form_popup").prop("disabled", false);
							} else {
								$("#delete_req_form_popup").prop("disabled", true);
								$("#vra_owner_change_form_popup").prop("disabled", true);
							}
							req.putData({TEAM_NM:arr[0].TEAM_NM, USER_NAME:arr[0].USER_NAME, VM_ID: arr[0].FROM_ID})
							form_data.HOURS="24";
							setTabIdx(tabIdx);
						});
						post('/api/vra_catalog/list/list_NC_VM/', arr[0], function(data){

							vmTable.setData(data);
						});

					},
				}
		mainTable = newGrid("mainTable", gridOptions);
		search();
	});

	function initFormData(){
		form_data.CUD_CD_NM='';
		form_data.APPR_STATUS_CD_NM='';
	}
	var tabIdx ;
	function setTabIdx(idx){
		tabIdx = idx ;
		if(tabIdx ==3 ){
			onclickNWTraffic()
		}
	}
	function statusSync(){
		if(!form_data.CATALOGREQ_ID){
			alert(msg_select_first);
			return false;
		}
		var param = {CATALOGREQ_ID:form_data.CATALOGREQ_ID};
		post('/api/vra_catalog//updateStatusSync', param, function(data){
			alert('<spring:message code="request_finish_wait" text="요청하였습니다. 5분후에 확인해 주세요." />')
			search();
		})
	}
	// 조회
	function search() {
		req.search('/api/vra_catalog/list' , function(data) {

			mainTable.setData(data);
		});
	}

	// 삭제
	function deleteReq() {
		req.del('/api/snapshot/delete', function() {
			search();
		});
	}

	function redistribution_btn(data){
		post('/api/vra_catalog/updateReDeploy?CATALOGREQ_ID='+data.CATALOGREQ_ID, null, function(data){
			alert('<spring:message code="label_req_complete" text="요청이 완료되었습니다"/>');
			search();
		});
	}

	function select_S_CATEGORY(data){
		searchvue.form_data.CATEGORY = data.CATEGORY;
		searchvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
		$("#S_CATEGORY_NM").val(data.CATEGORY_NM);
	}
	// 엑셀 내보내기
	function exportExcel(){
		mainTable.exportCSV({fileName:'vra'})
		//exportExcelServer("mainForm", '/api/snapshot/list_excel', 'Snapshot',mainTable.gridOptions.columnDefs, req.getRunSearchData())
	}
	function reSendMail(){
		var arr = mainTable.getSelectedRows();
		if(arr.length > 0){
			post("/api/site/vra/mail_resend",arr[0],function(){
				alert("전송되었습니다");
			});
		} else{
			alert(msg_select_first);
		}
	}
</script>

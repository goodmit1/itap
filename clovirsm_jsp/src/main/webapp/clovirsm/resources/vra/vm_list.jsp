<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<div class="table_title layout name">
				VM <spring:message code="count" text="건수"/> : <span id="vmTable_total">0</span>
				<div class="btn_group">
				 		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcelVM()" class="layout excelBtn" id="excelBtn"></button>
				</div>

	</div>

	<div id="vmTable" class="ag-theme-fresh" style="height: 200px" ></div>
	<script>
		var vmTable;


		$(function() {


			var columnDefs = [{headerName : "<spring:message code="NC_VM_VM_NM" text="" />",field : "VM_NM", width: 200, minWidth: 200},
				 
				{headerName : "IP",field : "PRIVATE_IP", width:120, minWidth:120},
				{
					headerName : '<spring:message code="NC_OS_TYPE_GUEST_NM" text="OS명" />',
					field : 'GUEST_NM',
					 
					width: 280,
					minWidth: 280,
					tooltip: function(params){
						if(params.data) return params.data.GUEST_NM;
					}
				},
				 {
					headerName : '<spring:message code="NC_VM_CPU_CNT" text="CPU" />',
					field : 'CPU_CNT',
					width: 100,
					minWidth: 100,
					format:'number'
				},{
					headerName : '<spring:message code="NV_VM_RAM_CNT" text="Memory" />(GB)',
					field : 'RAM_SIZE',
					width: 100,
					minWidth: 100,
					format:'number'
				},{
					headerName : '<spring:message code="NC_VM_DISK_CNT" text="DISK"/>(GB)',
					field : 'DISK_SIZE',
					width: 120,
					minWidth: 120,
					format:'number'
				} ,
				{headerName : "<spring:message code="NC_VM_RUN_CD" text="" />",   minWidth:120,width: 120,field : "RUN_CD",
	  				cellRenderer :function(params){
	  					if(params.data.RUN_CD == 'R')
			  				return '<spring:message code="btn_poweron" text="시작" />'
		  				else if(params.data.RUN_CD == 'S')
		  					return '<spring:message code="btn_poweroff" text="정지" />'
		  		}},
		  		{
						headerName : 'SW',
						field : 'SW_NMS',						 
						width: 200,
						minWidth: 200
				} ,	
				// {headerName : "<spring:message code="NC_OS_TYPE_OS_NM" text="" />",field : "OS_NM"},
				];
			var
			gridOptions = {
				hasNo : true,
				columnDefs : columnDefs,
				//rowModelType: 'infinite',
				//rowSelection : 'single',
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				enableColResize : true,
				enableServerSideSorting: false
			}
			vmTable = newGrid("vmTable", gridOptions);


		});
	</script>
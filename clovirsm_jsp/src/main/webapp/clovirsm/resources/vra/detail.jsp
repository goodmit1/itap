<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- 입력 Form -->

	<div class="form-panel detail-panel panel panel-default">

			<div class="panel-body">



					<div class="col col-sm-6">
						<fm-output id="REQ_TITLE"   name="REQ_TITLE"   title="<spring:message code="name" text="명칭" />"  ></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="P_KUBUN_NM"   name="P_KUBUN_NM"  :value=" nvl( form_data.P_KUBUN_NM, '') +  ' ' + nvl( form_data.PURPOSE_NM,'') "style="width:350px" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>"  ></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="CATEGORY_NMS"   name="CATEGORY_NMS"   title="<spring:message code="TASK" text="업무" />"  ></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="TASK_STATUS_CD" :value="getStatuVRAsMsg(form_data)" ostyle="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;" style="width:500px" title="<spring:message code="TASK_STATUS_CD" text="결재상태" />"></fm-output>
<%-- 						<fm-sbutton id="redistribution" class="fm-popup-button detailBtn red" onclick="redistribution_btn(form_data)" cmd="search" v-if='form_data.TASK_STATUS_CD == "F" && ADMIN_YN == "Y"' ><spring:message code="redistribution" text="재배포" /></fm-sbutton> --%>
					</div>
					<div class="col col-sm-6" style="position: relative;">
						<fm-output id="CATALOG_NM"   name="CATALOG_NM"   title="<spring:message code="NC_VRA_CATALOG_CATALOG_ID" text="카탈로그" />"  ></fm-output>
<!-- 						<fm-popup-button popupid="topology" popup="/clovirsm/popup/vratopology.jsp" param="info" cmd="detailBtn blue">Topology</fm-popup-button> -->
					</div>
					<div class="col col-sm-6">
						<fm-output id="INS_TMS"   name="INS_TMS" title="<spring:message code="NC_VM_INS_TMS" text="작성일" />"  :value="formatDate(form_data.INS_TMS, 'datetime')"></fm-output>
					</div>
						<div class="col col-sm-6">
						<fm-output id="USE_MM_TMS"   name="USE_MM_TMS" title="<spring:message code="dt_expiredate" text="만료일" />"  :value="getUseMMAndExpireDT(form_data, '<spring:message code="dt_month"/>','<spring:message code="dt_expiredate"/>')"></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="USER_NAME"   name="USER_NAME" style="width:300px"  title="<spring:message code="NC_VRA_CATALOGREQ_USER_NAME" text="" />"  ></fm-output>
					</div>
					
					<div class="col col-sm-12">
						<div class="fm-output"><label for="CMT" class="control-label grid-title value-title" style="height: 130px;"><spring:message code="CMT" text="설명" /></label> 
							<div id="CMT" name="CMT" class="output  hastitle" style="height: 110px;" v-html="form_data.CMT"></div>
						</div>
					</div>
			</div>

	</div>
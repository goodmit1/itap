<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div id="newVraModal" class="modal fade" role="dialog" style="z-index:2000;">

  <div class="modal-dialog" style="width: 990px">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="header-title"><spring:message code="title_catalog_req"/></h4>
      </div>
      <div class="modal-body">

		<div class="form-panel detail-panel panel panel-default ">

			<div class="panel-body" style="height: 400px; overflow: auto;">
				<jsp:include page="input.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<div class="modal-footer">
        <button type="button" v-if="'${action}' == ''" title="<spring:message code="btn_request" text="바로요청"/>" onclick="vra_deploy()" class="btn"><spring:message code="btn_request" text="작업"/></button>
        <button type="button" v-if="'${action}' == ''" title="<spring:message code="btn_work" text="작업함에 담기"/>" onclick="vra_to_work()" class="btn"><spring:message code="btn_work" text="작업"/></button>
        <button type="button" v-if="'${action}' == 'update'" title="<spring:message code="btn_work" text="저장"/>" onclick="vra_save()" class="btn"><spring:message code="btn_save" text="저장"/></button>
   	</div>
	</div>
</div>
</div>
<script>

function openVraPopup(req, catalogid, reqid, dt)
{
	
	openVra(req, catalogid, reqid, dt);
	$("#newVraModal").modal();
}

</script>

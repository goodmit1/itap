<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
 
<div class="vraReq_common" style="width:100%;float:left;padding:0px 10px;">
	<div class="section">
		<div class="section-title"><spring:message code="msg_desc" text="기본정보" /></div>
		<input type="hidden" name="CATALOGREQ_ID" id="CATALOGREQ_ID" />
		<input type="hidden" name="CATALOG_NM" id="CATALOG_NM" />
		<input type="hidden" name="INS_DT" id="INS_DT" />
		<div class="section-row">
			<fm-output id="CATALOG_NM" name="CATALOG_NM"   title="<spring:message code="NC_VRA_CATALOG_CATALOG_ID" text="카탈로그" />"  ></fm-output>
		</div>
		<div class="section-row">
			<fm-output id="FEE"   name="FEE" :value="formatNumber(form_data.FEE)"  title="<spring:message code="label_VRA_FEE" text="비용"/>"  tooltip="<spring:message code="vra_fee_guide" text="초기 구성에 필요한 비용으로, 생성 시 한번만 부과됨."/>"></fm-output>
		</div>
		<div class="section-row">
			<fm-input v-if="'${action}' == '' || '${action}' == 'update' " id="PURPOSE"   name="PURPOSE"   title="<spring:message code="NC_VRA_CATALOG_PURPOSE" text="목적" />"  ></fm-input>
			<fm-output v-if="'${action}' == 'view'" id="PURPOSE"   name="PURPOSE"   title="<spring:message code="NC_VRA_CATALOG_PURPOSE" text="목적" />"  ></fm-output>
		</div>
		<div class="section-row">
			<fm-textarea id="CMT" v-if="'${action}' == '' || '${action}' == 'update' " name="CMT"   title="<spring:message code="NC_VRA_CATALOG_CMT" text="설명" />"  ></fm-textarea>
			<fm-output id="CMT" v-if="'${action}' == 'view'" name="CMT"   title="<spring:message code="NC_VRA_CATALOG_CMT" text="설명" />"  ></fm-output>
		</div>
	</div>
</div>
<div class="gap" style="width:100%; float:left; height:1px;"></div>
<div id="vraReq_general" class="vraReq_general" style="width:100%;float:left;padding:0px 10px;"></div>
<div class="gap" style="width:100%; float:left; height:1px;"></div>
<div id="extra_html" class="extra_html" style="width:100%;float: left;padding:0px 10px;"></div>
<script>
	function setVraField(selector, fieldObj){
		 
			$(selector).each(function(idx, obj){
				var id = $(obj).attr("name");
				var val = fieldObj[id];
				if(val){
					if(val instanceof Array){
						$(obj).val( val.pop());
					} else {
						$(obj).val(val);
					}
				}
			});
		 	
		
		
		 
	}

	var totalFee = 0;
	function recurAjax(list, i, callback, returnVal){
		if(i < list.length){
			var data = list[i];
			if(returnVal.DC_ID && (data.DC_ID == undefined || data.DC_ID == null || data.DC_ID == '')) data.DC_ID = returnVal.DC_ID.split(',')[i];
			if(returnVal.DATA_JSON){
				var specJSON = JSON.parse(returnVal.DATA_JSON)[data.id];
				if(specJSON != undefined && specJSON != null){
					for(prop in specJSON){
						data[prop] = specJSON[prop];
					}
					data.CPU_CNT_MIN=data.cpu.minValue;
					data.CPU_CNT_MAX=data.cpu.maxValue;
					if(data.memory.minValue) data.RAM_SIZE_MIN=data.memory.minValue/1024;
					if(data.memory.maxValue) data.RAM_SIZE_MAX=data.memory.maxValue/1024;
					
					if(data.SPEC_ID){
						
						getDayFee(data.DC_ID, data.CPU_CNT, data.RAM_SIZE, '',data.DISK_SIZE, data.DISK_UNIT, data.SPEC_ID, null, function(_data){
							totalFee += _data.FEE;
							$.ajax(data.url, { data: data, dataType: 'html', success: function(_data){
								returnVal.html = returnVal.html.replace('@template:' + data.id + '@', _data);
								recurAjax(list, i+1, callback, returnVal);
							}});
						});
					} else {
						$.ajax(data.url, { data: data, dataType: 'html', success: function(_data){
							returnVal.html = returnVal.html.replace('@template:' + data.id + '@', _data);
							recurAjax(list, i+1, callback, returnVal);
						}});
					}
				} else {
					if(callback) callback(returnVal, false);
				}
			} else {
				if(data.cpu)
				{
					data.CPU_CNT=data.cpu.defaultValue;
					data.CPU_CNT_MIN=data.cpu.minValue;
					data.CPU_CNT_MAX=data.cpu.maxValue;
					data.RAM_SIZE=data.memory.defaultValue/1024;
					data.DISK_SIZE=data.disk.defaultValue ;
					if(data.memory.minValue) data.RAM_SIZE_MIN=data.memory.minValue/1024;
					if(data.memory.maxValue) data.RAM_SIZE_MAX=data.memory.maxValue/1024;
				}	
				data.UID = 'vra_' + Math.floor(Math.random() * 1000000000000);
				$.ajax(data.url, { data: data, dataType: 'html', success: function(_data){
					returnVal.html = returnVal.html.replace('@template:' + data.id + '@', _data);
					recurAjax(list, i+1, callback, returnVal);
				}});
			}
		} else {
			if(callback) callback(returnVal);
		}
	}

	function openVra(req, catalogid, reqid, dt){
		 
		var param = {};
		param.CATALOG_ID = catalogid;
		if(reqid){
			param.CATALOGREQ_ID=reqid;
			param.INS_DT = dt;
			$("#CATALOGREQ_ID").val(reqid);
			$("#INS_DT").val(dt);
		}
		$('#vraReq_general').children().remove();
		$('#extra_html').children().remove();
		post('/api/vra_catalog/form_info', param, function(data){
			req.putData(data);
			$("#CATALOG_NM").val(data.CATALOG_NM);

			if(data.html){
				if(data.html.indexOf('@template:') > -1){
					if(data.template){
						totalFee = 0;
						recurAjax(data.template, 0, function(_data, isSuccess){
							if(isSuccess == false){
								alert('Error');
								return;
							}
							$('#vraReq_general').html(_data.html);
							var isSpecExists = false;
							var specDataList = [];
							for(var i = 0; i < data.template.length; i++){
								var template = data.template[i];
								if(template.type == 'spec'){
									specDataList.push(template);
								}
							}
							if(specDataList.length > 0) {
								$('<div class="section" style="width:100%;"><div class="margin10"><div class="fm-output inline" style="width:100%;"><label for="totalFee" class="control-label grid-title value-title"><spring:message code="label_DD_TOTAL_FEE" text="총 일간요금(원)" /></label><div id="totalFee" name="totalFee" class="output" style="text-align:right;">'+formatNumber(totalFee)+'</div></div></div>').appendTo($('#vraReq_general'));
								$('#totalFee').prop('var-data', data);
							}
							tuningCSSAndValue(data);
						}, data);
					}
				} else {
					$('#vraReq_general').html(data.html);
					tuningCSSAndValue(data);
				}
			} else {
				tuningCSSAndValue(data);
			}
		});
	}

	function tuningCSSAndValue(data){
		setTimeout(function(){
			if(data.extra_html){
				$('#extra_html').html(data.extra_html);
				$('<div class="section-title"></div>').text('<spring:message code="NC_VRA_EXTRA_INFO" text="추가정보" />').prependTo($('#extra_html').children().get(0));
			}
			var dataJSON = null;
			if(data.DATA_JSON){
				dataJSON = JSON.parse(data.DATA_JSON);
				setVraField(".extra_html input", dataJSON.fields);
				delete dataJSON.fields
				 
				if('${action}' == 'view'){
					$('#newVraModal input').prop('disabled', true);
					$('#newVraModal input').prop('readonly', true);
				}
			}
			$(".section").css('width', '100%');
			$(".section input").addClass("form-control");
			$(".section input").addClass("input");
			$(".section input").addClass("hastitle");
			$(".section label").addClass("value-title");

			$(".section").each(function(i){
				var $this = $(this);
				var id = $this.attr('id');
				var title = $this.attr('title');
				var rowspan = $this.children().length;
				if(title != undefined && rowspan > 0){
					var $titleDiv = $('<div class="section-title"></div>').text(title);
					$('<div class="section-title"></div>').text(title).prependTo($this);
				}
				if(dataJSON != null){
					var data1 = dataJSON[id];
					if(data1)
					{	
						var keys = Object.keys(data1);
						keys.forEach(function(key, i)
						{
							$this.find("input[name='" + key + "']").val(data1[key])
						}); // 값 셋팅
					}
				}
			});

			//$(".section").css('padding-top', '5px').css('padding-bottom', '5px').css('border', '1px solid #ececec').css('border-radius', '5px').css('margin-bottom', '10px');
			//$('.section-title').css('padding-left', '5px').css('padding-bottom', '5px').css('border-bottom', '1px solid #cecece');
		}, 100);
	}
	function chkMinMax()
	{
		var isOK=true;
		$(".chkMinMax").each(function(){
			var val = $(this).find("input").val();
			var min  = $(this).attr("min")
			if(min && 1*min>0)
			{
				if(val<min)
				{
					isOK=false;
					$(this).find("input").focus();
					alert(msg_min_err.format($(this).find("label").text(), min));
					return;
				}	
			}	
			var max  = $(this).attr("max")
			if(max && 1*max>0)
			{
				if(val>max)
				{
					isOK=false;
					$(this).find("input").focus();
					alert(msg_max_err.format($(this).find("label").text(), max));
					return;
				}	
			}	
		})
		return isOK;
	}
	function onBeforeSaveVra(){
		
		if(!chkMinMax()) return;
		var result = getObjFromInput(".vraReq_common input,.vraReq_common textarea");
		var datajson = getObjFromInput(".vraReq_general :input:not([type='button'])");
		var fields = getObjFromInput(".extra_html :input");
		if(fields && Object.keys(fields).length > 0){
			datajson.fields = fields;
		}
		result.DATA_JSON = JSON.stringify(datajson)
		result.CATALOG_ID=form_data.CATALOG_ID;
		result.PREDICT_DD_FEE = $('#totalFee').val();
		return result;
	}

	function vra_to_work(){
		vra_request("N", "/clovirsm/workflow/work/index.jsp")
	}

	function vra_deploy(){
		vra_request("Y", "/clovirsm/resources/vra/index.jsp")
	}

	function vra_save(){
		vra_request("N");
	}

	function vra_request(deploy, next_url){
		var param = onBeforeSaveVra();
		var url = '/api/vra_catalog/insertReq';
		if(param.CATALOGREQ_ID != null && param.CATALOGREQ_ID != ''){
			url = '/api/vra_catalog/updateReq';
		}
		param.DEPLOY_REQ_YN=deploy
		post(url, param, function(data){
			if(deploy=='Y'){
				if(!confirm(msg_complete_work)){
					if(next_url) parent.location.href=next_url;
					return;
				}
			} else {
				if(next_url && confirm(msg_complete_work_move)){
					parent.location.href=next_url;
					return;
				} else if(!next_url) {
					alert(msg_complete);
					
				}
			}
			<c:if test="${callback != null}">
				${callback}();
			</c:if>
		});
	}

	function calcTotalFee(form_data, fee){
		var vraData = $('#totalFee').prop('var-data');
		if(vraData){
			var totalFee = 0;
			for(var i = 0; i < vraData.template.length; i++){
				var template = vraData.template[i];
				if(template.UID == form_data.UID){
					template.DD_FEE = form_data.DD_FEE;
				}
				if(isNaN(template.DD_FEE)) continue;
				totalFee += template.DD_FEE;
			}
			$('#totalFee').val(totalFee);
			$('#totalFee').text(formatNumber(totalFee));
		}
	}
</script>
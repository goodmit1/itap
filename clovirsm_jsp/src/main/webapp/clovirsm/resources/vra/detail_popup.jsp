<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	 
%>
<style>

#${popupid} .modal-content{
	width:1200px;
	height:600px
}

</style>

<fm-modal id="${popupid}" title="<spring:message code="new_vra_detail" text="신규배포 상세"/>" cmd="header-title"  >
	 
		
 
	<div class="form-panel detail-panel" >
		  <ul class="nav nav-tabs">
			<li class="active">
				<a href="#tab1" data-toggle="tab" onclick="setTabIdx( 1)"><spring:message code="title_subTable" text="상세정보"/></a>
			</li>
			<li>
				<a href="#tab2" data-toggle="tab" onclick="setTabIdx( 2)"><spring:message code="tab_server" text="서버"/></a>
			</li>
			<li v-if="ADMIN_YN == 'Y'">
				<!-- <a href="#tab3" data-toggle="tab" onclick="setTabIdx( 3)">N/W Traffic</a> -->
			</li>
		</ul>
		<div class="tab-content " style="height:420px">
			<div class="tab-buttons">
			</div>
			<div class="form-panel detail-panel tab-pane active" id="tab1">
				<jsp:include page="detail.jsp"></jsp:include>
			</div>
			<div class="form-panel detail-panel tab-pane" id="tab2">
				<jsp:include page="vm_list.jsp"></jsp:include>
			</div>
			<!-- <div class="form-panel detail-panel tab-pane" id="tab3" >
				<jsp:include page="nw_traffic.jsp"></jsp:include> 
			</div>
			 -->
		</div>
	</div>
	<script>
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		  

		 function ${popupid}_click(vue, param)
		 {
			 	post('/api/vra_catalog/info/selectByPrimaryKey_NC_VRA_CATALOGREQ_REQ/', param, function(data){
			 		${popupid}_vue.form_data=data;
			 	});
				post('/api/vra_catalog/list/list_NC_VM/', param, function(data){
					
					vmTable.setData(data);
				});
			  
			 return true;
		 }
	</script>
	<style>

	</style>
</fm-modal>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
    <%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
    <%
	 
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	 

%>
<style>
#${popupid} .modal-dialog{
	width:990px;
	height:600px;

}
</style>
<fm-modal id="${popupid}" title="<spring:message code="btn_modify" text="정보 수정"/>" cmd="header-title" >
	<span slot="footer" id="${popupid}_footer">
		 <fm-popup-button popupid="vra_owner_change_form_popup" popup="/popup/owner_change_form_popup.jsp?svc=vra_catalog&key=CATALOGREQ_ID" cmd="update" class="saveBtn contentsBtn tabBtnImg change top5" param="${popupid}_param"><spring:message code="btn_change_owner" text="담당자변경"/></fm-popup-button>

		<input   type="button" class="btn vra_saveBtn contentsBtn tabBtnImg save top5"  value="<spring:message code="btn_save" text="저장" />" onclick="${popupid}_save()" />
	</span>
	<div class="form-panel" style="height: 80px;">
		<form id="${popupid}_form" action="none">
			<div class="col col-sm-9" >
					<fm-input id="${popupid}_REQ_TITLE" name="REQ_TITLE" title="<spring:message code="title" text="제목"/>"   ></fm-input>
			</div>
			<div class="col col-sm-3" >
					<fm-input id="${popupid}_USE_MM" name="USE_MM" title="<spring:message code="NC_FW_USE_MM_M" text="사용기간(개월)" />" min="0"  ></fm-input>
			</div>
			<div class="col col-sm-12">
				<fm-textarea id="${popupid}_CMT" name="CMT"   title="<spring:message code="NC_VRA_CATALOG_CMT" text="설명" />"  ></fm-textarea>
				
			</div>
		</form>
	</div>
</fm-modal>
<script>
var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
var ${popupid}_param = {};
var ${popupid}_vue = new Vue({
	el: '#${popupid}',
	data: {
		form_data: ${popupid}_param
	},
	});
	function ${popupid}_click(vue, param){
		${popupid}_vue.form_data =  param ;
		${popupid}_param = param;
		if(param.CATALOGREQ_ID){
			return true
		}else{
			alert(msg_select_first);
			return false;
		}
	}
	function ${popupid}_save(){
		var param = $("#${popupid}_form").serializeObject();
		param.CATALOGREQ_ID= ${popupid}_vue.form_data.CATALOGREQ_ID;
		
		 post('/api/vra_catalog/save', param, function(){
			 alert('ok');
			 $('#${popupid}').modal('hide');
		 })
	}
 
</script>
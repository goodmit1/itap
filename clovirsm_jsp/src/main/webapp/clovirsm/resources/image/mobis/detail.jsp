<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 입력 Form -->
<div class="table_title">
	<div class="btn_group">
		<fm-popup-button popupid="img_save_popup" popup="../../popup/mobis/img_detail_form_popup.jsp?action=save&callback=onAfterModify" cmd="update" class="saveBtn" param="imgReq.getData()"><spring:message code="btn_modify" text="정보 수정"/></fm-popup-button>
	</div>
</div>
<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm-6">
			<fm-output id="DC_NM"  name="DC_NM" title="<spring:message code="NC_DC_DC_NM" text="데이터 센터" />"  ></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="VM_NM"  name="VM_NM" :value="form_data.VM_NM && form_data.VM_NM != null ? form_data.VM_NM + '('+form_data.FROM_ID+')':''" title="<spring:message code="NC_IMG_FROM_ID_NM" text="원본  서버명" />"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="IMG_NM" name="IMG_NM"  required="true" title="<spring:message code="NC_IMG_IMG_NM" text="템플릿명" />" disabled="disabled"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="DISK_SIZE"   name="DISK_SIZE"   title="<spring:message code="NC_IMG_DISK_SIZE" text="디스크" />" :value="getTemplateDiskSize(form_data)"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="GUEST_NM"  name="GUEST_NM" title="<spring:message code="NC_VM_GUEST_NM" text="OS명" />"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="TASK_STATUS_CD_NM"  name="TASK_STATUS_CD_NM" title="<spring:message code="NC_IMG_TASK_STATUS_CD_NM" text="작업상태" />" :value="getTaskStatusCdNm(form_data)"  :ostyle="getCUDCDStyle(form_data)"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="TEAM_NM" name="TEAM_NM" title="<spring:message code="FM_TEAM_TEAM_CD" text="팀" />"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="USER_NAME" name="USER_NAME" title="<spring:message code="FM_TEAM_USER_NAME" text="담당자" />"></fm-output>
		</div>
		<c:if test="${sessionScope.STATE }">
		<div class="col col-sm-6">
			<fm-output id="CATEGORY" name="CATEGORY_NM" :value="(form_data.ORG_CATEGORY ? form_data.ORG_CATEGORY : '' )" title="<spring:message code="PART" text="부품"/>" ></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="P_KUBUN" name="P_KUBUN_NM" :value="(form_data.P_KUBUN_NM ? form_data.P_KUBUN_NM : '')" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>" ></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="PURPOSE" name="PURPOSE_NM" title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>" ></fm-output>
		</div>
		</c:if>
		<div class="col col-sm-6">
			<fm-output id="USE_MM" name="USE_MM" title="<spring:message code="NC_FW_USE_MM_M" text="사용기간(개월)" />" :value="getUseMMAndExpireDT(form_data, '<spring:message code="dt_month"/>','<spring:message code="dt_expiredate"/>')":ostyle="getUseMMStyle(form_data)"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="FEE" name="DD_FEE" title="<spring:message code="NC_IMG_SPEC_FEE" text="요금(일일/총합)" />" :value="getFee(form_data)"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="INS_DT" name="INS_DT" title="<spring:message code="NC_VM_INS_TMS" text="등록일시" />" :value="formatDate(form_data.INS_DT, 'datetime')"></fm-output>
		</div>
		<div class="col col-sm-12">
			<div style="height: 200px;">
				<label for="CMT" class="control-label grid-title value-title"><spring:message code="NC_IMG_CMT" text="설명" /></label>
				<div v-html="form_data.CMT" class="output hastitle" style="min-height: 200px;overflow-y: auto;word-break: break-word;"></div>
			</div>
		</div>
		<div class="col col-sm-12" v-if="form_data.FAIL_MSG != undefined">
			<fm-output id="FAIL_MSG" name="FAIL_MSG" title="<spring:message code="NC_IMG_FAIL_MSG" text="실패메시지" />" disabled="disabled"></fm-output>
		</div>
	</div>
</div>
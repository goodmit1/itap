<span slot="footer">
		<input type="button" class="btn" value="<spring:message code="btn_work" text=""/>" onclick="server_create_to_work()" />
		<input type="button" class="btn" value="<spring:message code="btn_request" text="바로 요청"/>" onclick="server_create_request()" />
	</span>
	<style>
	.row-fluid.Editor-container {
		width:calc(100% - 150px);
		float: right;
	}
	</style>
	<div class="form-panel detail-panel">
		<form id="vm_create_popup_form" action="none">
			<div class="panel-body">
				<input type="hidden" name="VM_ID" id="F_VM_ID"  />
				<div class="col col-sm-12">
					<fm-select id="SCP_DC_NM" name="DC_ID"
						url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC"
						keyfield="DC_ID" titlefield="DC_NM"
						title="<spring:message code="NC_DC_DC_NM" text="데이터 센터"/>">
					</fm-select>
				</div>
				<div class="col col-sm-12">
					<fm-output id="SCP_IMG_NM" name="IMG_NM" title="<spring:message code="NC_IMG_IMG_NM" text="템플릿명"/>"></fm-output>
				</div>
				<div class="col col-sm-12">
					<fm-input id="SCP_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="VM명"/>" v-if="${IS_AUTO_VM_NAME}" disabled="disabled"></fm-input>
					<fm-input id="SCP_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="VM명"/>" v-if="${!IS_AUTO_VM_NAME}"></fm-input>
				</div>
				<div class="col col-sm-6">
					<fm-select url="/api/code_list?grp=PURPOSE" id="SCP_PURPOSE" emptystr=" " name="PURPOSE" title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>"></fm-select>
				</div>
				<div class="col col-sm-6">
					<fm-select url="/api/code_list?grp=P_KUBUN" id="SCP_P_KUBUN" emptystr=" " name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>"></fm-select>
				</div>
				<div class="col col-sm-6">
					<fm-ibutton id="SCP_SPEC_NM" name="SPEC_NM" title="<spring:message code="NC_VM_SPEC_NM" text="사양"/>">
						<fm-popup-button v-show="false" popupid="template_spec_search" popup="../../popup/spec_search_form_popup.jsp" cmd="update" param="${popupid}_getSpecSearchParam()" callback="select_spec"><spring:message code="btn_spec_search" text="사양검색"/></fm-popup-button>
					</fm-ibutton>
				</div>
				<div class="col col-sm-6">
					<fm-output id="SCP_CPU_CNT" name="CPU_CNT" title="<spring:message code="NC_IMG_CPU_CNT" text="CPU"/>"></fm-output>
				</div>
				<div class="col col-sm-6">
					<fm-output id="SCP_DISK_SIZE" name="DISK_SIZE" title="<spring:message code="NC_VM_DISK_SIZE" text="디스크"/>"></fm-output>
				</div>
				<div class="col col-sm-6">
					<fm-output id="SCP_RAM_SIZE" name="RAM_SIZE" title="<spring:message code="NC_VM_RAM_SIZE" text="메모리"/>"></fm-output>
				</div>
				<div class="col col-sm-12">
					<div>
						<label for="SCP_CMT" class="control-label grid-title value-title"><spring:message code="NC_VM_CMT" text="설명" /></label>
						<textarea id="SCP_CMT" name="CMT" style="width:calc(100% - 150px);"></textarea>
					</div>
				</div>
			</div>
		</form>
	</div>
	<script>
		$(function(){
			setTimeout(function(){
				$("#SCP_CMT").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false});
			},1000)
		});
		var popup_server_create_form = newPopup("vm_create_popup_button", "vm_create_popup");
		var vm_create_popup_param = {};
		var vm_create_popup_vue = new Vue({
			el: '#vm_create_popup',
			data: {
				form_data: vm_create_popup_param
			}
		});
		function vm_create_popup_click(vue, param){
			 
			if(param.IMG_ID){
				vm_create_popup_param = $.extend({
					VM_NM: '',
					CMT: '',
					DC_ID: -1,
					DC_NM: '',
					IMG_ID: -1,
					IMG_NM: '',
					SPEC_ID: -1,
					SPEC_NM: '',
					CPU_CNT: 0,
					DISK_SIZE: 0,
					DISK_UNIT: 'G',
					RAM_SIZE: 0
				}, param);
				vm_create_popup_vue.form_data = vm_create_popup_param;
				setTimeout(function(){$("#SCP_CMT").Editor("setText", vm_create_popup_param.CMT ? vm_create_popup_param.CMT:"")},1000);
				return true;
			} else {
				vm_create_popup_param = null;
				alert(msg_select_first);
				return false;
			}
		}

		function vm_create_popup_onAfterOpen(){

		}

		function ${popupid}_getSpecSearchParam(){
			return {
				DC_ID: ${popupid}_vue.form_data.DC_ID,
				SPEC_NM: $('#SCP_SPEC_NM').val()
			}
		}

		function server_create_to_work(){
			if(vm_create_popup_param.SPEC_ID && vm_create_popup_param.SPEC_ID > 0){
				vm_create_popup_param.CMT = $("#SCP_CMT").Editor("getText" );
				post('/api/vm/insertReq', vm_create_popup_param,  function(data){
					if(confirm(msg_complete_work_move)){
						location.href="/clovirsm/workflow/work/index.jsp";
						return;
					}
					vmImageSearch();
					$('#vm_create_popup').modal('hide');
				});
			} else {
				alert(msg_select_first);
			}
		}

		function server_create_request(){
			if(vm_create_popup_param.SPEC_ID && vm_create_popup_param.SPEC_ID > 0){
				vm_create_popup_param.CMT = $("#SCP_CMT").Editor("getText" );
				vm_create_popup_param.DEPLOY_REQ_YN = 'Y';
				post('/api/vm/insertReq', vm_create_popup_param,  function(data){
					alert(msg_complete_work);
					vmImageSearch();
					$('#vm_create_popup').modal('hide');
				});
			} else {
				alert(msg_select_first);
			}
		}

		function select_spec(args, callback){
 			vm_create_popup_param = $.extend(vm_create_popup_param, args);
 			vm_create_popup_vue.form_data = vm_create_popup_param;
 			if(callback) callback();
		}

	</script>
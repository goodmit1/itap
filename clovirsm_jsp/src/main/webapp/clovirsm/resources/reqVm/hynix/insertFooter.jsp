<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>

		<input v-if="${isAppr == 'N'}" type="button" class="btn exeBtn" id="prevBtn" value="<spring:message code="btn_prev" text="이전" />" @click="prev()" />
		<input v-if="${isAppr == 'N'}" type="button" class="btn exeBtn" id="nextBtn" value="<spring:message code="btn_next" text="다음" />" @click="next()" />
		<input v-if="${isAppr == 'N'} && ${direct == null && action == 'insert'}  " type="button" class="btn saveBtn" value="<spring:message code="btn_work" text="작업함에 담기"/>" onclick="${popupid}_to_work()" />
		<input v-if="${isAppr == 'N'} && (${action == 'insert'} && form_data.CUD_CD == 'C') || ${action == 'save'} || ${direct == 'Y'}" type="button" class="btn saveBtn"  value="<spring:message code="btn_save" text="저장" />" onclick="${popupid}_save()" />
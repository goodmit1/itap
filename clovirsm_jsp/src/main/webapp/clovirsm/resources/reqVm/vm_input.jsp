<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("action", action);

	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);
%>
<div class="panel-body">
	<fm-popup-button style="display:none;" popupid="${ popupid }_search_form_popup" popup="/clovirsm/popup/nas_search_form_popup.jsp" cmd="" param="" callback="${ popupid }_select_nas"></fm-popup-button>
	<div class="col col-sm-6">
		<fm-input id="${popupid}_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="VM명"/>(<spring:message code="label_auto_gen"/>)" v-if="${IS_AUTO_VM_NAME}" :disabled=${sessionScope.ADMIN_YN eq "Y" ? false : true}></fm-input>
		<fm-input id="${popupid}_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="VM명"/>" required="true" v-if="${!IS_AUTO_VM_NAME}" :disabled="${sessionScope.ADMIN_YN eq 'Y' ? false : true }"></fm-input>
	</div>
	<div class="col col-sm-6">
		<fm-spin id="${popupid}_USE_MM" name="USE_MM" class="inline compact" style="" title="<spring:message code="NC_VM_USE_MM" text="사용기간(개월)" />" min="6" :disabled="${isAppr != 'N'}"></fm-spin>
	</div>
	<fmtags:include page="/clovirsm/popup/_spec_form.jsp" />
	<c:if test="${sessionScope.STATE }">
		<div class="col col-sm-6">
			<fm-select url="/api/site/querykey/selectPartCategory/" id="${popupid}_CATEGORY"
				keyfield="CATEGORY_ID" titlefield="CATEGORY_NM"
				:disabled="${isAppr != 'N'}"
				name="CATEGORY" title="<spring:message code="PART" text="부품"/>">
			</fm-select>
		</div>
	</c:if>
	<div class="col col-sm-6">
		<fm-output id="OS_NM" name="OS_NM" title="<spring:message code="NC_VM_FROM_NM" text="원본 서버명"/>"></fm-output>
	</div>
	<c:if test="${sessionScope.STATE }">
		<div class="col col-sm-6">
			<fm-select url="/api/code_list?grp=P_KUBUN" id="${popupid}_P_KUBUN"
				emptystr=" " keyfield="P_KUBUN" titlefield="P_KUBUN_NM"
				:disabled="${isAppr != 'N'}"
				name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>">
			</fm-select>
		</div>
		<div class="col col-sm-6">
			<fm-select url="/api/code_list?grp=PURPOSE" id="${popupid}_PURPOSE" name="PURPOSE"
				title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>" :disabled="${isAppr != 'N'}"
				emptystr=" " keyfield="PURPOSE" titlefield="PURPOSE_NM">
			</fm-select>
		</div>
	</c:if>
	 
	<c:if test="${sessionScope.NAS_SHOW}">
	<div class="col col-sm-12" style="max-height: 278px;">
		<div class="hastitle">
			<label for="${popupid}_NAS"
				class="control-label grid-title value-title"
				style="vertical-align: top;">
				<spring:message code="NC_NAS_NAS" text="NAS" />
				<button type="button" class="btn btn-primary btn" style="float: right; margin: 6px 17px;" onclick="${popupid}_newNas()" title="btn_new">
					<i class="fa fa-plus"></i>
				</button>
			</label>
			<div class="hastitle output" id="${popupid}_NAS_TABLE" style="height: 100%;overflow: auto;">
			</div>
		</div>
	</div>
	</c:if>
	<div class="col col-sm-12" style="height: 248px;">
		<div>

			<c:if test="${isAppr != 'N'}">
				<label for="CMT" class="control-label grid-title value-title"><spring:message code="NC_VM_CMT" text="설명" /></label>
				<div v-html="form_data.CMT" class="output hastitle" style="min-height: 200px;overflow-y: auto;word-break: break-word;"></div>
			
			</c:if>
			<c:if test="${isAppr == 'N'}">
				<fm-textarea id="${popupid}_CMT" name="CMT" style=""></fm-textarea>
			</c:if>
		</div>
	</div>
</div>
<script>

	var ${popupid}_CPUOptions = ${popupid}_makeOptions(32, '', 'core');
	var ${popupid}_RAMOptions = ${popupid}_makeOptions(64, '', 'GB');
	var diskUnitOptions = {
		G: 'GB',
		T: 'TB'
	}
	var fw_click_check = false;
	var fw_click_count = 0;

	function ${popupid}_makeOptions(maxSize, title_prefix, title_postfix){
		title_prefix = title_prefix?title_prefix: '';
		title_postfix = title_postfix?title_postfix: '';
		var ramList = {};
		for(var i = 1; i <= maxSize; i++){
			eval('ramList['+i+']= title_prefix+i+title_postfix;');
		}
		return ramList;
	}

	function ${popupid}_makeOptions2(maxSize, title_prefix, title_postfix){
		title_prefix = title_prefix?title_prefix: '';
		title_postfix = title_postfix?title_postfix: '';
		var ramList = {};
		for(var i = 1; i <= maxSize; i++){
			eval('ramList['+i+']= title_prefix+i+title_postfix;');
		}
		return ramList;
	}
	function ${popupid}_makeInsertReqParam(addParam){
		//TODO ${popupid}_vue.form_data.CATEGORY=$(".img_category_select:last").val()

		var param = $.extend(${popupid}_vue.form_data, {
			NC_VM_FW_LIST: JSON.stringify(${popupid}_vmFwListGrid.getData())
		});
		<c:if test="${isAppr == 'N'}">
		param.CMT = $("#${popupid}_CMT").Editor("getText" );
		</c:if>
		if(addParam) param = $.extend(param, addParam);
		var list = $("#${popupid}_NAS_TABLE .nas_text");
		var nasStr = [];
		
		for(var i=0; i<list.length; i++){
			if(isNaN(i) || i == null) {
				continue;
			}
			var val = $(list[i]).text();
			if(val && val.trim() != "") {
				nasStr.push(val);
			}
			
		}
		
		/* for(var i in list) {
			if(isNaN(i) || i == null) {
				continue;
			}
			var val = $(list[i]).text();
			if(val && val.trim() != "") {
				nasStr.push(val);
			}
		} */
		if(nasStr[0] != null) {
			param.NAS = nasStr.join(",");
		}
		try {
			${popupid}_makeDataDiskParam(param); //Data Disk param
		} catch(e) {
			console.log(e);
		}
		if(param.DISK_UNIT == 'T') {
			param.DISK_UNIT = 'G';
			param.DISK_SIZE = Number(param.DISK_SIZE) * 1024;
		}
		delete param.TEAM_CD;
		param.DISK_SIZE = Number(param.DISK_SIZE) + Number(param.OS_DISK_SIZE);
		return param;
	}
	
	function fw_grid_validate(){
		if(!${popupid}_fw_input_validate()) return;
		var value = ${popupid}_vmFwListGrid.getRow(0);
		if(value === undefined || value === 'undefined')
		{
			alert('<spring:message code="NC_FW_VALIDATE" text="서버 생성요청자 이외에 서버접속이 필요한 사용자는 접근통제 탭에서 등록해주시기 바랍니다" />')
			return false;
		}
		else return true;
	}
	function ${popupid}_fw_check(){
		if(fw_click_count<=0 ){
			alert('<spring:message code="NC_FW_CLICK_CHECK" text="서버 접근이 필요한 대상자가 추가로 있으시면 접근통제 탭에서 등록해주시기 바랍니다." />')
			fw_click_count++;
			return false;
		}
		return true;
	}
 
	
	 
	
	/* function ${popupid}_validateCheck(){
		var result = true;
		if(!${popupid}_fw_check()){result = false} ;
		if(!${popupid}_validation_vm_req()) result = false ;
		if(!fw_grid_validate()) result = false ;
		if($('#${popupid}_FW_CIDR').val() == '0:0:0:0:0:0:0:1'){
			alert('<spring:message code="tab_firewall" text="접근제어" /> <spring:message code="msg_ip_invalid" text="IP입력값이 잘못되었습니다." />')
			result = false ;
		}
		if(!${popupid}_validate_port()){
			alert('<spring:message code="MSG_ALERT_FW_PORT" text="PORT번호 입력값이 잘못되었습니다." />');
			result = false ;
		}
		return result
	} */

	function ${popupid}_to_work(popupid){
		//${popupid}_vue.form_data.DISK_TYPE_ID = '1';
		<c:if test="${sessionScope.FW_SHOW }">
		${popupid}_fw_check();
		if(!${popupid}_validation_vm_req()) return;
		if(!fw_grid_validate()) return;
		 
		</c:if>
		 
		var param = ${popupid}_makeInsertReqParam();
		post('/api/vmReq/insertReq', param, function(data){
			if(confirm(msg_complete_work_move)){
				location.href="/clovirsm/workflow/work/index.jsp";
				return;
			}else {
				location.href="/clovirsm/resources/vm/index.jsp";
				return;
			}
		});
	}

	function ${popupid}_request(popupid){
		//${popupid}_vue.form_data.DISK_TYPE_ID = '1';
		<c:if test="${sessionScope.FW_SHOW }">
	
		if(!${popupid}_validation_vm_req()) return;
		if(!fw_grid_validate()) return;
		if(!${popupid}_fw_input_validate()){
			 
			return;
		}
		${popupid}_fw_check();
		</c:if>
		if($('#${popupid}_FW_CIDR').val() == '0:0:0:0:0:0:0:1'){
			alert('<spring:message code="tab_firewall" text="접근제어" /> <spring:message code="msg_ip_invalid" text="IP입력값이 잘못되었습니다." />')
			return;
		}
		var param = ${popupid}_makeInsertReqParam({DEPLOY_REQ_YN: 'Y'});
		post('/api/vmReq/insertReq', param, function(data){
			alert(msg_complete_work);
			if(${popupid}_callback != ''){
				eval(${popupid}_callback + '(${popupid}_vue.form_data);');
			} else {
				location.href="/clovirsm/resources/vm/index.jsp";
				return;
			}
			if(popupid) {
				parent.$(popupid).modal('hide');
			}
		});
	}

	function ${popupid}_save(popupid){

		//${popupid}_vue.form_data.DISK_TYPE_ID = '1';
		if(!${popupid}_validation_vm_req()) return;
		var param = ${popupid}_makeInsertReqParam();
		post('/api/vmReq/insertReq', param, function(data){
			alert(msg_complete);
			try{
				if(${popupid}_callback != ''){
					try {
						eval(${popupid}_callback + '(${popupid}_vue.form_data);');
					} catch(e){
						eval('parent.' + ${popupid}_callback + '(${popupid}_vue.form_data);');
					}
				}
			}catch (e) { }
			if(popupid) {
				parent.$(popupid).modal('hide');
			}else {
				$("#${popupid}").modal("hide");
			}
		});
	}
	function ${popupid}_newNas(){
		$("#${ popupid }_search_form_popup_button").click();
	}

	function ${popupid}_getSpecSearchParam(){
		return {
			DC_ID: ${popupid}_vue.form_data.DC_ID,
			SPEC_NM: $('#${popupid}_SPEC_NM').val()
		}
	}

	function ${popupid}_validation_vm_req(){
		/* if(${popupid}_vue.form_data.VM_NM == undefined || ${popupid}_vue.form_data.VM_NM == ''){
			alert('<spring:message code="msg_input_vm_name"/>');
			$('#${popupid}_VM_NM').select();
			return false;
		} */
		try{
			if($("#${popupid}_CMT").Editor("getText" )==''){
				alert(msg_empty_value + "(<spring:message code="NC_VM_CMT" text="설명" />)"  )
				return false;
			}
		}catch(e){}
		if($('#${popupid}_DISK_SIZE').hasClass('invalid-size')){
			alert('<spring:message code="msg_disk_size_re_setting"/>');
			$('#${popupid}_DISK_SIZE').select();
			return false;
		}

		var rows = ${popupid}_vmFwListGrid.getData();
		for(var i = 0 ; i < rows.length; i++){
			var row = rows[i];
			if(row.CIDR == undefined || row.CIDR == ''  ){
				alert('<spring:message code="msg_invalid_fw_info"/>');
				return false;
			} else {

			}
		}

		return true;
	}

	function select_${popupid}_spec(spec, callback){
		${popupid}_vue.form_data.SPEC_ID = spec.SPEC_ID;
		${popupid}_vue.form_data.SPEC_NM = spec.SPEC_NM;
		${popupid}_vue.form_data.CPU_CNT = spec.CPU_CNT;
		${popupid}_vue.form_data.DISK_SIZE = spec.DISK_SIZE;
		${popupid}_vue.form_data.DISK_UNIT = 'G';
		${popupid}_vue.form_data.RAM_SIZE = spec.RAM_SIZE;
		${popupid}_vue.form_data.DISK_TYPE_ID = spec.DISK_TYPE_ID;
		//${popupid}_vue.form_data.SPEC_INFO = getSimpleSpecInfo(spec);
		$("#${popupid}_SPEC_NM").val(spec.SPEC_NM);
		$("#${popupid}_SPEC_INFO").text(getSpecInfo(null, spec.CPU_CNT,spec.RAM_SIZE,${popupid}_vue.form_data.OS_DISK_SIZE, 'G'))
		getDayFee(${popupid}_vue.form_data.DC_ID, ${popupid}_vue.form_data.CPU_CNT,${popupid}_vue.form_data.RAM_SIZE,'',${popupid}_vue.form_data.DISK_SIZE, ${popupid}_vue.form_data.DISK_UNIT, ${popupid}_vue.form_data.SPEC_ID, '${popupid}_DD_FEE')
		if(callback) callback();
	}

	function ${popupid}_select_nas(data, callback){
		if(callback) {
			callback.call();
		}

		for(var i=0; i<data.length; i++){
			var row = data[i];
			if(!${popupid}_chk_dupl_nas(row.PATH)){
				continue;
			}
			${popupid}_makeNas(row.PATH, true);
		}
		
		/* for(var i in data) {
			var row = data[i];
			if(!${popupid}_chk_dupl_nas(row.PATH)){
				continue;
			}
			${popupid}_makeNas(row.PATH, true);
		} */
	}

	function ${popupid}_clearNas(){
		$("#${popupid}_NAS_TABLE").html("");
	}

	function ${popupid}_makeNas(path, delAble) {
		var table = $("#${popupid}_NAS_TABLE");
		var html = '<div class="nas_item choice" style="width: fit-content;float:left;">';
		if(delAble == true){
			html += '<span class="nas_del" style="color: red;cursor: pointer;" onclick="delete_nas(this);">×</span>';
		}
		html += '<span class="nas_text">'
		html += path;
		html += "</span></div>";
		table.append(html);
	}

	function ${popupid}_chk_dupl_nas(path){
		var list = $("#${popupid}_NAS_TABLE .nas_text");
		
		for(var i=0; i<list.length; i++){
			if(isNaN(i) || i == null) {
				continue;
			}
			var val = $(list[i]).text();
			if(path == val) {
				return false;
			}
			
		}
		return true;
		/* for(var i in list) {
			if(isNaN(i) || i == null) {
				continue;
			}
			var val = $(list[i]).text();
			if(path == val) {
				return false;
			}
		} */
	}

	function delete_nas(that){
		var _this = $(that);
		_this.parent().remove();
	}

	function ${popupid}_checkServerInfo(){
		return validate("${popupid}_form");
	}
</script>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
   String popupid = request.getParameter("popupid");
   request.setAttribute("popupid", popupid);

   String action = request.getParameter("action");
   request.setAttribute("action", action);

   String callback = request.getParameter("callback");
   request.setAttribute("callback", callback);

   String isAppr = request.getParameter("isAppr");
   request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);
%>
<div class="section" style="width:100%;" id="img_category_row" v-if="${action != 'insert'}"></div>
   <div class="section" style="width:100%;display:none;" id="img_row">
      <div class="col col-sm-6" id="${popupid}_DC_ID_area">
         <fm-select id="${popupid}_DC_ID" name="DC_ID"
            title="<spring:message code="NC_VM_DC_ID" text="데이터 센터"/>"
            required="true"
            emptystr=" " keyfield="DC_ID" titlefield="DC_NM"
            onchange="${popupid}_onchangeDC(this)"
            disabled v-if="${action != 'insert'}">
         </fm-select>
         <fm-output name="DC_NM" title="<spring:message code="NC_VM_DC_ID" text="데이터 센터"/>" v-if="${action == 'insert'}"></fm-output>
      </div>
      <div class="col col-sm-6">
<%--          <fm-select id="${popupid}_OS_ID" name="OS_ID"
            title="<spring:message code="NC_IMG_IMG_NM" text="템플릿"/>"
            required="true"
            emptystr=" " keyfield="IMG_ID" titlefield="IMG_NM"
            disabled v-if="Number(form_data.root_category_id) &lt; Number(Object.keys(mappingParam)[0]) && ${action != 'insert'}">
         </fm-select>
         <fm-select id="${popupid}_OS_ID" name="FROM_ID"
            title="<spring:message code="NC_IMG_IMG_NM" text="템플릿"/>"
            required="true"
            emptystr=" " keyfield="IMG_ID" titlefield="IMG_NM"
            disabled v-if="Number(form_data.root_category_id) &gt;= Number(Object.keys(mappingParam)[0]) && ${action != 'insert'}">
         </fm-select>--%>
          <fm-output name="TEMPLATE_NM" title="<spring:message code="NC_VM_FROM_NM" text="원본 서버명"/>" v-if="form_data.FROM_ID && form_data.FROM_ID.substring(0,1) == 'S' && ${action == 'insert'}"></fm-output>
         <fm-output name="TEMPLATE_NM" title="<spring:message code="NC_IMG_IMG_NM" text="템플릿"/>" v-if="((form_data.FROM_ID && form_data.FROM_ID.substring(0,1) == 'G') || form_data.OS_ID) && ${action == 'insert'}"></fm-output>
      </div>
   </div>
   <script>

   function ${popupid}_onChangeFromDC(target){
      var $this = $(target);
      ${popupid}_addImgCategorySelect($this.val());

   }

var mappingParam={};
mappingParam[-3]={text:"<spring:message code="NC_VM_VM_IMG_NAME" text="개인탬플릿"/>", url:"/api/vmReq/list/list_IMG_SELECT/"};
mappingParam[-4]={text:"<spring:message code="NC_VM_VM_CNT_NAME" text="자주사용서버"/>",url:"/api/vmReq/list/list_IMG_SELECT_4_CNT/"};
//mappingParam[-5]={text:"<spring:message code="NC_VM_VM_TMS_NAME" text="최근사용서버"/>",url:"/api/vmReq/list/list_IMG_SELECT_4_TMS/"};

   function ${popupid}_addRootImgCategorySelect(){
      post('/api/vmReq/list/list_CATEGORY_SELECT/?PAR_CATEGORY_ID=0', null, function(data){
         for(var _key in mappingParam) {
            if(_key == -3) {
               continue;
            }
            var obj = {};
            obj.CATEGORY_ID = "" + _key;
            obj.PAR_CATEGORY_ID = "0";
            obj.CATEGORY_NM = mappingParam[_key].text;
            data.push(obj);
         }
         var $row = $('<div class="col col-sm-12">');
         $('#img_category_row').prepend($row);
         var $selectWrapper = $('<div></div>');
         $selectWrapper.appendTo($row);
         var $label = $('<label for="root_img_category" class="control-label input-group-title grid-title value-title"><spring:message code="NC_VM_VM_IMG_CATEGORY" text="카테고리"/></label>');
         $label.appendTo($selectWrapper);
         var $select = $('<select id="root_img_category" name="root_category" style="width:150px;margin-right:15px;margin-left:3px" class="root_category img_category_select form-control input" level="0">');
         $select.appendTo($selectWrapper);
         fillOptionByData('root_img_category', data, ' ', 'CATEGORY_ID', 'CATEGORY_NM');
         $('#${popupid}_SPEC_NM').prop('disabled', true);
         $('#${popupid}_SPEC_NM').parent().find('button').prop('disabled', true);
         $('#${popupid}_DISK_SIZE').prop('disabled', true);
         $('#${popupid}_DISK_SIZE').parent().find('button').prop('disabled', true);
         $('#${popupid}_DISK_UNIT').prop('disabled', true);
         setTimeout(function(){setRootCategorySelect();$('#root_img_category').val('1').trigger('change');}, 100);
      }, true)
   }

   function setRootCategorySelect() {
      var s = $("#img_category_row #root_img_category");
      if(s[0] == null) {
         setTimeout(function() {setRootCategorySelect();}, 100);
         return;
      }
      $("#root_img_category").on('change.root_img_category', function(e){
         var $this = $(this);
         var img_category_id = $this.val();

         $('#${popupid}_DC_ID').prop('disabled', true);
         $('#${popupid}_DC_ID option').remove();

         $('#${popupid}_SPEC_NM').prop('disabled', true);
         $('#${popupid}_SPEC_NM').parent().find('button').prop('disabled', true);
         $('#${popupid}_DISK_SIZE').prop('disabled', true);
         $('#${popupid}_DISK_SIZE').parent().find('button').prop('disabled', true);
         $('#${popupid}_DISK_UNIT').prop('disabled', true);
         $("#${popupid}_tmplListGrid").hide();
         ${popupid}_tmplGrid.setData([])
         $("#${popupid}_partListGrid").hide();
         $("#imageMapArea").hide();
         var $img_category_rows = $('#img_category_row .img_category_select');
         for(var i = $img_category_rows.length-1; i >= 1 ; i--){
            $($img_category_rows.get(i)).remove();
         }
         if(isStep2) {
            //parent.setFrameHeight("1000");
         }
         if(img_category_id == '') return;
         $("#${popupid}_tmplListGrid").show();

         ${popupid}_vue.form_data.root_category_id = img_category_id;

         ${popupid}_vue.form_data.DC_ID = null;
         ${popupid}_vue.form_data.OS_ID = null;
         ${popupid}_vue.form_data.FROM_ID = null;
         //${popupid}_vue.form_data.SPEC_ID = null;
         //${popupid}_vue.form_data.SPEC_NM = null;
         ${popupid}_vue.form_data.DISK_SIZE = 0;
         ${popupid}_vue.form_data.DISK_UNIT= 'G';
         if(img_category_id <= Number(Object.keys(mappingParam)[0])){

            ${popupid}_addDCSelect(img_category_id);
         } else {
            if(img_category_id != '2'){
               ${popupid}_addImgCategorySelect(img_category_id, 1);
            }else {
               if(isStep2) {
                  //parent.setFrameHeight("1110");
               }
               var $img_category_rows = $('#img_category_row .img_category_select');
               for(var i = $img_category_rows.length-1; i >= 1 ; i--){
                  $($img_category_rows.get(i)).remove();
               }
               $("#imageMapArea").show();
               $("#${popupid}_tmplListGrid").hide();
               ${popupid}_addDCSelect();
            }
         }
      });
   }

   function ${popupid}_addImgCategorySelect(parent_img_category_id, level){
      var $img_category_rows = $('#img_category_row .img_category_select');
      for(var i = $img_category_rows.length-1; i >= level ; i--){
         $($img_category_rows.get(i)).remove();
      }
      $('#${popupid}_DC_ID').prop('disabled', true);
      $('#${popupid}_DC_ID option').remove();

      $('#${popupid}_SPEC_NM').prop('disabled', true);
      $('#${popupid}_SPEC_NM').parent().find('button').prop('disabled', true);
      $('#${popupid}_DISK_SIZE').prop('disabled', true);
      $('#${popupid}_DISK_SIZE').parent().find('button').prop('disabled', true);
      $('#${popupid}_DISK_UNIT').prop('disabled', true);
      ${popupid}_tmplGrid.setData([])

      ${popupid}_vue.form_data.DC_ID = null;
      ${popupid}_vue.form_data.OS_ID = null;
      ${popupid}_vue.form_data.FROM_ID = null;
      //${popupid}_vue.form_data.SPEC_ID = null;
      //${popupid}_vue.form_data.SPEC_NM = null;
      ${popupid}_vue.form_data.DISK_SIZE = 0;
      ${popupid}_vue.form_data.DISK_UNIT= 'G';

      if(parent_img_category_id == '') {
         var $img_category_rows = $('#img_category_row .img_category_select');
         for(var i = $img_category_rows.length-1; i >= 1 ; i--){
            $($img_category_rows.get(i)).remove();
         }
         ${popupid}_addImgCategorySelect(${popupid}_vue.form_data.root_category_id);
         return;
      }

      post('/api/vmReq/list/list_CATEGORY_SELECT/?PAR_CATEGORY_ID=' + parent_img_category_id, null, function(data){
          
         ${popupid}_addDCSelect(parent_img_category_id);
         if(data.length == 0){
            return;
         }
         var $select = $('<select id="img_category_'+parent_img_category_id+'" name="img_category_'+parent_img_category_id+'" class="img_category_select form-control input" level="'+level+'"style="width:auto; min-width:auto;margin-right: 15px;"></select>');
         $("#root_img_category").parent().append($select);
         $("#img_category_" + parent_img_category_id).on('change.img_category_'+parent_img_category_id, function(e){
            var $this = $(this);
            var img_category_id = $this.val();
            ${popupid}_addImgCategorySelect(img_category_id, Number($this.attr('level'))+1);
         });
         fillOptionByData('img_category_' + parent_img_category_id, data, ' ', 'CATEGORY_ID', 'CATEGORY_NM');
      }, true)
   }
   //${popupid}_DC_ID
   function ${popupid}_addDCSelect(img_category_id){

      if($('#${popupid}_DC_ID')[0].tagName == "SELECT") {
         $('#${popupid}_DC_ID option').remove();
         var param = '';
         if(Number(img_category_id) > Number(Object.keys(mappingParam)[0])){
            param = '?CATEGORY=' + img_category_id;
         }
         post('/api/vmReq/list/list_IMG_DC_SELECT/'+param, null, function(data){
            $('#${popupid}_DC_ID').prop('img_category_id', img_category_id);
            if(data.length > 1){
               $('#img_row').show();
            }else {
               $('#img_row').hide();
            }
            if(data.length > 0){
               $('#${popupid}_DC_ID').prop('disabled', false);
               fillOptionByData('${popupid}_DC_ID', data, null, 'DC_ID', 'DC_NM');
               ${popupid}_onchangeDC($("#${popupid}_DC_ID")[0], img_category_id);
            }
         }, true);
      } else {
         ${popupid}_onchangeDC($("#${popupid}_DC_ID")[0], img_category_id)
      }
   }
   function ${popupid}_addImgSelect(dc_id, img_category_id){
      ${popupid}_vue.form_data.DC_ID = dc_id;
      ${popupid}_vue.form_data.OS_ID = null;
      ${popupid}_vue.form_data.FROM_ID = null;
      //${popupid}_vue.form_data.SPEC_ID = null;
      //${popupid}_vue.form_data.SPEC_NM = null;
      ${popupid}_vue.form_data.DISK_SIZE = 0;
      ${popupid}_vue.form_data.DISK_UNIT= 'G';

      $('#${popupid}_SPEC_NM').prop('disabled', true);
      $('#${popupid}_SPEC_NM').parent().find('button').prop('disabled', true);
      $('#${popupid}_DISK_SIZE').prop('disabled', true);
      $('#${popupid}_DISK_SIZE').parent().find('button').prop('disabled', true);
      $('#${popupid}_DISK_UNIT').prop('disabled', true);

      if(dc_id == '') return;

      var param = '?DC_ID=' + dc_id;
      var url = '/api/vmReq/list/list_IMG_SELECT/';
      if(mappingParam[img_category_id]){
         url = mappingParam[img_category_id].url;
      } else {
         if(img_category_id != null && img_category_id != '') {
            param += '&CATEGORY=' + img_category_id;
         }
      }
      post(url + param, null, function(data){
          
         if(data.length == 0) return;
         $('#${popupid}_SPEC_NM').prop('disabled', false);
         $('#${popupid}_SPEC_NM').parent().find('button').prop('disabled', false);
         $('#${popupid}_DISK_SIZE').prop('disabled', false);
         $('#${popupid}_DISK_SIZE').parent().find('button').prop('disabled', false);
         $('#${popupid}_DISK_UNIT').prop('disabled', false);
         ${popupid}_tmplGrid.setData(data);
         if(mappingParam[img_category_id] == null) {
            //fillOptionByData('${popupid}_OS_ID', data, null, 'OS_ID', ['OS_NM', 'P_KUBUN_NM', 'PURPOSE_NM']);
         } else {
            //fillOptionByData('${popupid}_OS_ID', data, null, 'IMG_ID', ['IMG_NM', 'P_KUBUN_NM', 'PURPOSE_NM']);
         }
      }, true)
   }

   function ${popupid}_onchangeDC(select){
      var $this = $(select);
      var dc_id = $this.val();
      var img_category_id = $this.prop('img_category_id');
      if(img_category_id == '2'){
         $("#imageMapArea").show();
         $("#${popupid}_tmplListGrid").hide();
         return;
      }
      ${popupid}_addImgSelect(dc_id, img_category_id);
   }

   function onImageMapClick() {
      var title = this.getAttribute("org_title");
      post("/api/vmReq/list/list_PART_SELECT/", {title: title, DC_ID: ${popupid}_param.DC_ID}, function(data){
         $("#${popupid}_partListGrid").show();
          
         ${popupid}_partGrid.setData(data);
      })
   }
   </script>
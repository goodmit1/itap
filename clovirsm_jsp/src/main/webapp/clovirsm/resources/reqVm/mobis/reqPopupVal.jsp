<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<script>
_popupTitle = ['<spring:message code="label_select_templete" text="템플릿 선택"/>','<spring:message code="btn_server_create_step_vm" text="VM정보 입력"/>', '<spring:message code="btn_server_create_step_fw" text="접근제어 입력"/>'];
</script>
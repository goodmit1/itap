<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="imageMapArea" style="background: #FFF;text-align:center;display:none">
	<script>
		var mouse = {x: 0, y: 0};
		$.fn.setPosition = function(top, left, unit) {
			if(unit == null){
				unit = "px";
			}
			this.css("top", top + unit).css("left", left + unit);
		}
		$(function(){
			tooltip = $("#tooltip");
			$("#imageMapArea map[name='map'] area").click(function(){
				try{
					 
					onImageMapClick.call(this);
				}catch(ignore){}
			});
			$("map[name='map'] area").mouseover(function(){
				this.setAttribute("org_title", this.title);
				tooltip.find("#title").html(this.title);
				this.title = "";
			});
			$("map[name='map'] area").mouseout(function(){
				tooltip.hide();
				this.title = this.getAttribute("org_title");
				tooltip.find("#title").html("");
			});
			$("map[name='map'] area").mousemove(function(event) {
				mouse.x = event.pageX;
				mouse.y = event.pageY;
				var y = mouse.y - 55
				var x = mouse.x - 12
				x += (85 - Number(tooltip.css("width").replace(/[a-z]/gi ,""))) / 5;
				tooltip.show().setPosition(y, x);
			});
		});
	</script>
	<style>

	</style>
	<img src="/res/img/car.png" id="map-image" style="border:none; width: 565px; max-width: 100%; height: auto;" alt="" usemap="#map" />
	<map name="map">
		<area shape="circle" href="#" coords="174, 207, 4" title="MDPS" />
		<area shape="circle" href="#" coords="287, 67 , 4" title="Radar" />
		<area shape="circle" href="#" coords="466, 250, 4" title="Build Automation" />
		<area shape="circle" href="#" coords="271, 63 , 4" title="DAS" />
		<area shape="circle" href="#" coords="243, 95 , 4" title="AVN" />
		<area shape="circle" href="#" coords="258, 98 , 4" title="AUDIO" />
		<area shape="circle" href="#" coords="299, 96 , 4" title="MDPS" />
		<area shape="circle" href="#" coords="343, 117, 4" title="AVM" />
		<area shape="circle" href="#" coords="283, 108, 4" title="Cluster" />
		<area shape="circle" href="#" coords="264, 115, 4" title="HUD" />
		<area shape="circle" href="#" coords="265, 128, 4" title="ACU" />
		<area shape="circle" href="#" coords="228, 180, 4" title="Air Suspension" />
		<area shape="circle" href="#" coords="151, 180, 4" title="Motor" />
		<area shape="circle" href="#" coords="73 , 180, 4" title="Camera" />
		<area shape="circle" href="#" coords="417, 250, 4" title="Public" />
		<area shape="circle" href="#" coords="441, 250, 4" title="Verification" />
	</map>
	<div id="tooltip">
		<svg width="67" height="48" viewbox="0 0 67 48" preserveAspectRatio="none">
			<path fill="#F36760" fill-rule="evenodd" d="M59.986-.003H7.011C3.145-.003 0 3.129 0 6.98V32c0 3.849 3.135 7 7 7h8v9l12-9h33c3.865 0 6.997-2.653 6.997-6.503V6.98c0-3.851-3.146-6.983-7.011-6.983z"/>
		</svg>
		<div>
			<span id="title"></span>
		</div>
	</div>
</div>
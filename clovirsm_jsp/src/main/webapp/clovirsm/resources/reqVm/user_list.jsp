<%@page contentType="text/html; charset=UTF-8"%><%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
			<div class="col col-sm-12">
				<fm-sButton id="${popupid}_add_fw_popup" cmd="update" onclick="${popupid}_add_fw(${popupid}_vmFwListGrid)" :disabled="${isAppr != 'N'}"><spring:message code="btn_add" text="추가"/></fm-sButton>
				<fm-sButton id="${popupid}_delete_check_fw_popup" cmd="update" onclick="${popupid}_delete_check_fw(${popupid}_vmFwListGrid)" :disabled="${isAppr != 'N'}"><spring:message code="btn_delete" text="삭제"/></fm-sButton>
				<fm-sButton id="${popupid}_vmFwSearch_popup" cmd="search" onclick="${popupid}_vmFwSearch()" style="float: right;" :disabled="${isAppr != 'N'}"><spring:message code="btn_reload" text="새로고침"/></fm-sButton>
			</div>
			<div class="col col-sm-12">
				<form id="${popupid}_fw_form" action="none">
					<div class="panel-body">
						<div class="col col-sm-6">
							<fm-ibutton id="${popupid}_FW_USER_NAME" name="FW_USER_NAME"  required="true" title="<spring:message code="FM_USER_USER_NAME" text="이름" />" :disabled="${isAppr != 'N'}">
								<fm-popup-button v-show="false" popupid="${popupid}_user_search_popup" popup="/popup/user_search_form_popup.jsp" cmd="update" param="$('#${popupid}_USER_NAME').val()" callback="${popupid}_select_user"  ><spring:message code="title_chg_owner" text="담당자변경"/></fm-popup-button>
							</fm-ibutton>
						</div>
						<div class="col col-sm-6">
							<fm-input id="${popupid}_FW_CIDR" name="FW_CIDR" required="true" placeholder="<spring:message code="mulit_sortation_placeholder" text="여러 개인 경우 ,로 구분해서 입력하세요." />" title="<spring:message code="NC_FW_PORT_USER" text="사용자 IP" />" onchange="${popupid}_onchangeFWForm();"   ></fm-input>
						</div>
						<div class="col col-sm-6">
							<fm-input id="${popupid}_FW_PORT" name="FW_PORT" required="true" placeholder="<spring:message code="mulit_sortation_placeholder" text="여러 개인 경우 ,로 구분해서 입력하세요." />" title="<spring:message code="NC_FW_PORT_ALLOW" text="허용 PORT" />" onchange="${popupid}_onchangeFWForm();"  ></fm-input>
						</div>
						<div class="col col-sm-6">
							<fm-spin id="${popupid}_FW_USE_MM" name="FW_USE_MM" style="width: 300px" title="<spring:message code="NC_FW_USE_MM_M" text="사용기간(개월)" />" min="6" onchange="${popupid}_onchangeFWForm();" ></fm-spin>
						</div>
						<c:if test="${'Y'.equals(sessionScope.VM_USER_YN)}">
						<div class="col col-sm-6">
							<fm-select url="/api/code_list?grp=sys.yn" id="${popupid}_FW_VM_USER_YN" name="FW_VM_USER_YN" title="<spring:message code="NC_FW_VM_USER_YN_OS" text="OS 계정생성여부"/>" onchange="${popupid}_onchangeFWForm();"  ></fm-select>
						</div>
						</c:if>
						<div class="col col-sm-6">
							<fm-output id="${popupid}_FW_TEAM_NM" name="FW_TEAM_NM" title="<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />"></fm-output>
						</div>

					</div>
					<div class="col col-sm-12">
						<div id="${popupid}_vmFwListGrid" class="ag-theme-fresh" style="height: 200px" ></div>
					</div>
				</form>
			</div>
			
			<script>

			var ${popupid}_vmFwReq = new Req();
			${popupid}_vmFwListGrid;

			function ${popupid}_fw_init() {
				var
				${popupid}_vmFwListGridColumnDefs = [ {
					headerName : '<spring:message code="NC_FW_CUD_CD" text="구분" />',
					field : 'IU_NM',
					width: 120,
					cellStyle: function(params){
						var style = {
							color: 'black'
						}
						if(params.data.CUD_CD == 'D') style.color = 'red';
						return style;
					},
					valueGetter: function(params){
						if( params.data.IU_NM ) return params.data.IU_NM;
						else return nvl(params.data.CUD_CD_NM,'') + ' ' + nvl(params.data.APPR_STATUS_CD_NM,'');
					}
				},{
					headerName : '<spring:message code="NC_FW_USER_IP" text="사용자 IP" />',
					field : 'CIDR',
					width: 130
				},{
					headerName : '<spring:message code="allowedPort" text="허용 Port" />',
					field : 'PORT',
					width: 130
				},{
					headerName : '<spring:message code="NC_FW_FW_USER_ID_NM" text="이름" />',
					field : 'USER_NAME',
					width: 120
				},{
					headerName : '<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />',
					field : 'TEAM_NM',
					width: 150
				},
				
				<c:if test="${'Y'.equals(sessionScope.VM_USER_YN)}">{
					headerName : 'OS <spring:message code="NC_FW_VM_USER_YN" text="계정생성여부" />',
					field : 'VM_USER_YN',
					width: 180,
					valueGetter: function(params){
						 
						if(params.data.VM_USER_YN == undefined || params.data.VM_USER_YN == '') return '';
						return params.data.VM_USER_YN  == 'Y' ? '<spring:message code="label_yes"/>': '<spring:message code="label_no"/>';
					}
				},</c:if>
				
				{
					headerName : '<spring:message code="NC_FW_USE_MM" text="Port" />',
					field : 'USE_MM',
					width: 100
				},  ];
				var
				${popupid}_vmFwListGridOptions = {
					hasNo : true,
					columnDefs : ${popupid}_vmFwListGridColumnDefs,
					//rowModelType: 'infinite',
					rowSelection : 'single',
					sizeColumnsToFit: true,
					cacheBlockSize: 100,
					rowData : [],
					enableSorting : true,
					enableColResize : true,
					enableServerSideSorting: false,
					onRowClicked: function(){
						var arr = ${popupid}_vmFwListGrid.getSelectedRows();
						var data = arr[0];

						setVueData(${popupid}_vue, 'FW_IU', data.IU);
						setVueData(${popupid}_vue, 'FW_IU_NM', data.IU_NM);
						setVueData(${popupid}_vue, 'FW_CUD_CD', data.CUD_CD);
						setVueData(${popupid}_vue, 'FW_CIDR', data.CIDR);
						setVueData(${popupid}_vue, 'FW_PORT', data.PORT);
						setVueData(${popupid}_vue, 'FW_USE_MM', data.USE_MM);
						setVueData(${popupid}_vue, 'FW_TEAM_CD', data.TEAM_CD);
						setVueData(${popupid}_vue, 'FW_TEAM_NM', data.TEAM_NM);
						setVueData(${popupid}_vue, 'FW_USER_ID', data.FW_USER_ID);
						setVueData(${popupid}_vue, 'FW_USER_NAME', data.USER_NAME);
						setVueData(${popupid}_vue, 'FW_VM_USER_YN', data.VM_USER_YN);
						${popupid}_setEnabledFWForm(true);
					}
				}
				${popupid}_vmFwListGrid = newGrid("${popupid}_vmFwListGrid", ${popupid}_vmFwListGridOptions);
				$('#${popupid}_DISK_SIZE').css('width', '100px');
			}
			
			function ${popupid}_setEnabledFWForm(isEnabled){

					$('#${popupid}_FW_PORT').prop('disabled', !isEnabled);
					$('#${popupid}_FW_USE_MM').parent().find(':input').prop('disabled', !isEnabled);
					$('#${popupid}_FW_USER_NAME').parent().find(':input').prop('disabled', !isEnabled);
					$('#${popupid}_FW_VM_USER_YN').prop('disabled', !isEnabled);

			}
			function ${popupid}_validate_port(port){
				//var reg = new RegExp('^[0-9]+[,]{0,1}$','g') // 숫자 , ',' 들어가 있는지  체크 
				var reg = new RegExp('^[0-9]+$') // 숫자 , ',' 들어가 있는지  체크 
				 
				var pattern = ',';
				var standard = port.indexOf(',');
				
				if(standard === -1)
				{
					if(reg.test(port)){return true;	}
					else{return false;}
				}else{
					return validateRange(port, reg);					
				}
				
			}
			
			function ${popupid}_validate_ip(ip){
				var reg = new RegExp('^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$') //ipv4 255.255.255.255
				var pattern = ',';
				var standard = ip.indexOf(',');
				
				if(standard === -1)
				{
					if(reg.test(ip)){return true;	}
					else{return false;}
				}else{
					return validateRange(ip, reg);					
				}
				
			}
			
			function validateRange(el, reg){
			    var arr = new Array();
			    var trueFalse = new Array();
			    var pattern = ',';

			    arr = el.replace(/[\t\s]/g,'').split(','); // 공백 문자 제거 후  ',' 기준으로 문자 잘라서 배열 반환 
			     

			    arr.forEach(function(el) {
			        var val = discriminant(el,pattern,reg)
			        trueFalse.push(val)
			    })

			     
			    var result = trueFalse.every(function(x) {return x === true;}) //배열안의 값 비교
			     
			    
			    return result;
			}

			
			function discriminant(port,pattern,reg)
			{
				var start = port.indexOf(pattern);
				var end = port.length;				
				var cidr = port.substring(start,end)
				var result = reg.test(cidr)
				
				return result; 
			}
			
			function ${popupid}_fw_input_validate(){
				var fwData = ${popupid}_vmFwListGrid.getData();
				for(var i=0; i < fwData.length; i++){
					if(fwData[i].PORT != ''){// 포트 입력 있는지 확인 
						if( !${popupid}_validate_port(fwData[i].PORT) ){
							alert('<spring:message code="MSG_ALERT_FW_PORT" text="PORT번호 입력값이 잘못되었습니다." />: ' + fwData[i].PORT);
							return false;
						}						
					}
					if(!${popupid}_validate_ip(fwData[i].CIDR)){
						alert('<spring:message code="tab_firewall" text="접근제어" /> <spring:message code="msg_ip_invalid" text="IP입력값이 잘못되었습니다." />')
						return false;
					}
				}
				return true;
			}
			function ${popupid}_vmFwSearch(){
				${popupid}_setEnabledFWForm(false);
				if(${popupid}_vue.form_data.VM_ID) {
					${popupid}_vmFwReq.searchSub('/api/fw/list', {VM_ID: ${popupid}_vue.form_data.VM_ID,INS_DT: ${popupid}_vue.form_data.INS_DT}, function(data) {
						${popupid}_vmFwListGrid.setData(data);
					});
				}
			}
			function ${popupid}_add_fw(grid, notclick){
				var data = {
					IU: 'I',
					IU_NM: '<spring:message code="btn_new" text="신규" />',
					CUD_CD: 'C',
					CIDR: '',
					PORT: '',
					USE_MM: 6,
					TEAM_CD: '',
					TEAM_NM: '',
					FW_USER_ID: '',
					USER_NAME: '',
					VM_USER_YN: ''
				};
				grid.insertRow(data);
				${popupid}_initFWForm();
				setVueData(${popupid}_vue, 'FW_CUD_CD', 'C');
				setVueData(${popupid}_vue, 'FW_VM_USER_YN', 'Y');
				setVueData(${popupid}_vue, 'FW_USE_MM', 6);
				${popupid}_setEnabledFWForm(true);

				if(!notclick) $("#${popupid}_user_search_popup_button").click();
			}

			function ${popupid}_initFWForm(){
				setVueData(${popupid}_vue, 'FW_IU', '');
				setVueData(${popupid}_vue, 'FW_IU_NM', '');
				setVueData(${popupid}_vue, 'FW_CUD_CD', '');
				setVueData(${popupid}_vue, 'FW_CIDR', '');
				setVueData(${popupid}_vue, 'FW_PORT', '');
				setVueData(${popupid}_vue, 'FW_USE_MM', '');
				setVueData(${popupid}_vue, 'FW_TEAM_CD', '');
				setVueData(${popupid}_vue, 'FW_TEAM_NM', '');
				setVueData(${popupid}_vue, 'FW_USER_ID', '');
				setVueData(${popupid}_vue, 'FW_USER_NAME', '');
				setVueData(${popupid}_vue, 'FW_VM_USER_YN', '');
			}

			function ${popupid}_delete_check_fw(grid){
				var rows = ${popupid}_vmFwListGrid.getSelectedRows();
				var data = rows[0];
				if(data == undefined) return;
				if(data.IU != undefined){
					if(data.IU == 'I'){
						grid.deleteRow();
						${popupid}_initFWForm();
						${popupid}_setEnabledFWForm(false);
					} else {
						data.IU = 'U';
						data.IU_NM = '<spring:message code="btn_delete" text="삭제"/>';
						data.CUD_CD = 'D';
						grid.updateRow(data);
					}
				} else {
					//if(data.CUD_CD && data.APPR_STATUS_CD)
					{
						data.IU = 'U';
						data.IU_NM = '<spring:message code="btn_delete" text="삭제"/>';
						data.CUD_CD = 'D';
						grid.updateRow(data);
					}
				}
			}

			function ${popupid}_select_user(user, callback){
				var rows = ${popupid}_vmFwListGrid.getSelectedRows();
				if(rows.length==0){
					 ${popupid}_add_fw(${popupid}_vmFwListGrid, true);
				}
				setVueData(${popupid}_vue, 'FW_TEAM_CD', user.TEAM_CD);
				setVueData(${popupid}_vue, 'FW_TEAM_NM', user.TEAM_NM);
				setVueData(${popupid}_vue, 'FW_USER_ID', user.USER_ID);
				setVueData(${popupid}_vue, 'FW_USER_NAME', user.USER_NAME);
				setVueData(${popupid}_vue, 'FW_CIDR', user.LAST_LOGIN_IP);
				${popupid}_onchangeFWForm();
				if(callback) callback();
			}

			function ${popupid}_onchangeFWForm(){
				setTimeout(function(){
					var rows = ${popupid}_vmFwListGrid.getSelectedRows();
					var data = rows[0];
					if(data && data != null){
						if(data.CUD_CD == 'D'){
							if(!confirm('<spring:message code="msg_delete_user_edit"/>')) {
								$(obj).val(data['prev_' + obj.name]);
								return;
							}
						}
						if(data.IU != 'I') {
							data.IU = 'U';
							data.IU_NM = '<spring:message code="btn_edit" text=""/>';
							data.CUD_CD = 'U';
						}
						data.CIDR = ${popupid}_vue.form_data.FW_CIDR;
						data.PORT = ${popupid}_vue.form_data.FW_PORT;
						data.USE_MM = ${popupid}_vue.form_data.FW_USE_MM;
						data.TEAM_CD = ${popupid}_vue.form_data.FW_TEAM_CD;
						data.TEAM_NM = ${popupid}_vue.form_data.FW_TEAM_NM;
						data.FW_USER_ID = ${popupid}_vue.form_data.FW_USER_ID;
						data.USER_NAME = ${popupid}_vue.form_data.FW_USER_NAME;
						data.VM_USER_YN = ${popupid}_vue.form_data.FW_VM_USER_YN;
						${popupid}_vmFwListGrid.updateRow(data);
					}
				}, 10);
			}
			</script>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/index">
    <layout:put block="content">
    	<form id="mainForm" onsubmit="return false">
    	<jsp:include page="list.jsp"></jsp:include>
    	<fm-popup-button popupid="vm_req_popup" popup="../../popup/vm_detail_tab_form_popup.jsp?action=insert" cmd="update" param="vmReq.getData()"><spring:message code="label_vm_req" text="생성요청"/></fm-popup-button>
    	</form>
    </layout:put>
    <layout:put block="popup_area">
	</layout:put>
</layout:extends>
var Category = function(id, name)
{
	this.id = id;
	this.name = name;
	this.child = [];
	this.template=[];
	this.addTemplate = function(obj)
	{
		this.template.push(obj);
	}
	this.addChild = function(category){
		if(!this.child.indexOf(category) >= 0)
		{	
			this.child.push(category);
		}
	}
	
}
var CategoryAll = function()
{
	var categoryAll = [];
	var categoryMap = {};
	this.rootCategory = new Category('0', 'root');
	this.maxLevel = 0;
	categoryMap['0'] = this.rootCategory;
	this.addCategory=function(ids, names, data)
	{
		 
		var that = this;
		if(this.maxLevel<ids.length) this.maxLevel = ids.length;
		ids.forEach(function(id, idx) {
			var obj = categoryMap[id];
			if(obj == null){
				 
			
				obj = new Category(id, names[idx]);
				if(idx==0)
				{
					that.rootCategory.addChild(obj);
				}
				else
				{
					var parent = ids[idx-1];
					categoryMap[parent].addChild(obj);
				}
				categoryMap[id] = obj;
			}
			if(idx == ids.length-1)
			{
				obj.addTemplate(data);
			}
		})
	}
	 
	this.getChildrenList = function(id){
		var category = categoryMap[id];
		var children = category.child;
		var arr = [];
		children.forEach(function(child, idx) {
			var arr1 = {'CATEGORY_ID':child.id, 'CATEGORY_NM':child.name};
			arr.push(arr1);
		});
		return arr;
	}
	this.getTemplateList = function(id, p_kubun){
		var category = categoryMap[id];
		var template = category.template;
		var arr = [];
		 
		template.forEach(function(child, idx) {
			if(child.P_KUBUN==p_kubun)
			{
			 
				arr.push(child);
				 
			}
		});
		return arr;
	}
	this.getPKubunList = function(id, labelList){
		var category = categoryMap[id];
		var template = category.template;
		var arr = [];
		var unique = {};
		template.forEach(function(child, idx) {
			if(unique[child.P_KUBUN] == null)
			{
				var arr1 = {'ID':child.P_KUBUN, 'TITLE':labelList[child.P_KUBUN]};
				arr.push(arr1);
				unique[child.P_KUBUN]=labelList[child.P_KUBUN];
			}
		});
		return arr;
	}
}
 
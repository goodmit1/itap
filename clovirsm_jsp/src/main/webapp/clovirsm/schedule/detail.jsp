<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!-- 입력 Form -->
<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm-6">
			<fm-select url="/api/code_list?grp=SCHEDULE_SYNC" id="SCHEDULE_SYNC" onchange="inputvue.$forceUpdate();"
				name="SCHEDULE_SYNC" title="<spring:message code="NC_SCHEDULE_SCHEDULE_SYNC" text="주기/비주기"/>">
			</fm-select>
		</div>
		<template v-if="form_data.SCHEDULE_SYNC == 'C'">
			<div class="col col-sm-6">
				<fm-input id="CRON" name="CRON" title="<spring:message code="NC_SCHEDULE_CRON" text="CRON" />"></fm-input>
			</div>
		</template>
		<template v-if="form_data.SCHEDULE_SYNC == 'A'">
			<div class="col col-sm-3">
				<fm-spin id="YEAR" name="YEAR" class="inline compact" min="0" max="9999" title="<spring:message code="label_year" text="년" />"></fm-spin>
			</div>
			<div class="col col-sm-3">
				<fm-spin id="MONTH" name="MONTH" class="inline compact" min="1" max="12" title="<spring:message code="label_month" text="월" />"></fm-spin>
			</div>
			<div class="col col-sm-3">
				<fm-spin id="DAY" name="DAY" class="inline compact" min="1" max="31" title="<spring:message code="label_date" text="일" />"></fm-spin>
			</div>
			<div class="col col-sm-3">
				<fm-spin id="HOUR" name="HOUR" class="inline compact" min="1" max="24" title="<spring:message code="hour" text="시" />"></fm-spin>
			</div>
			<div class="col col-sm-3">
				<fm-spin id="MIN" name="MIN" class="inline compact" min="1" max="59" title="<spring:message code="minute" text="분" />"></fm-spin>
			</div>
		</template>
		<template v-if="form_data.SCHEDULE_SYNC == 'S'">
			<div class="col col-sm-6">
				<fm-select url="/api/code_list?grp=SCHEDULE_CYCLE" id="SCHEDULE_CYCLE" onchange="inputvue.$forceUpdate();"
					name="SCHEDULE_CYCLE" title="<spring:message code="NC_SCHEDULE_SCHEDULE_CYCLE" text="주기"/>">
				</fm-select>
			</div>
			<template v-if="form_data.SCHEDULE_CYCLE == 'D'">
				<div class="col col-sm-3">
					<fm-spin id="HOUR" name="HOUR" class="inline compact" min="1" max="24" title="<spring:message code="hour" text="시" />"></fm-spin>
				</div>
				<div class="col col-sm-3">
					<fm-spin id="MIN" name="MIN" class="inline compact" min="1" max="59" title="<spring:message code="minute" text="분" />"></fm-spin>
				</div>
			</template>
			<template v-if="form_data.SCHEDULE_CYCLE == 'W'">
				<div class="col col-sm-3">
					<div class="fm-select">
						<label for="WEEK" class="control-label grid-title value-title"><spring:message code="label_day" text="요일" /></label>
						<select name="WEEK" id="WEEK" class="form-control input hastitle" v-model="form_data.WEEK">
							<option value="1"><spring:message code="label_monday"/></option>
							<option value="2"><spring:message code="label_tuesday"/></option>
							<option value="3"><spring:message code="label_wednesday"/></option>
							<option value="4"><spring:message code="label_thursday"/></option>
							<option value="5"><spring:message code="label_friday"/></option>
							<option value="6"><spring:message code="label_saturday"/></option>
							<option value="0"><spring:message code="label_sunday"/></option>
						</select>
					</div>
				</div>
				<div class="col col-sm-3">
					<fm-spin id="HOUR" name="HOUR" class="inline compact" min="1" max="24" title="<spring:message code="hour" text="시" />"></fm-spin>
				</div>
				<div class="col col-sm-3">
					<fm-spin id="MIN" name="MIN" class="inline compact" min="1" max="59" title="<spring:message code="minute" text="분" />"></fm-spin>
				</div>
			</template>
			<template v-if="form_data.SCHEDULE_CYCLE == 'M'">
				<div class="col col-sm-3">
					<fm-spin id="DAY" name="DAY" class="inline compact" min="1" max="31" title="<spring:message code="label_date" text="일" />"></fm-spin>
				</div>
				<div class="col col-sm-3">
					<fm-spin id="HOUR" name="HOUR" class="inline compact" min="1" max="24" title="<spring:message code="hour" text="시" />"></fm-spin>
				</div>
				<div class="col col-sm-3">
					<fm-spin id="MIN" name="MIN" class="inline compact" min="1" max="59" title="<spring:message code="minute" text="분" />"></fm-spin>
				</div>
			</template>
		</template>
		<div class="col col-sm-3">
			<fm-ibutton id="VM_NM" name="VM_NM" required="required" title="<spring:message code="NC_VM_VM_NM" text="서버명"/>">
				<fm-popup-button popupid="vm_search" popup="/clovirsm/popup/vm_search_form_popup.jsp?isActiveOnly=Y" cmd="update" param="" callback="select_vm"><spring:message code="btn_vm_search" text="서버검색"/></fm-popup-button>
			</fm-ibutton>
		</div>
		<div class="col col-sm-3">
			<fm-select url="/api/code_list?grp=SCHEDULE_ACTION" id="ACTION"
				name="ACTION" title="<spring:message code="NC_SCHEDULE_ACTION" text="Action"/>">
			</fm-select>
		</div>
		<div class="col col-sm-12" v-if="form_data.FAIL_MSG">
			<fm-output id="FAIL_MSG" name="FAIL_MSG" title="<spring:message code="NC_VM_FAIL_MSG" text="실패 사유" />" ></fm-output>
		</div>
	</div>
	<script>
	function makeCron() {
		var result = "0 ";
		if(form_data.SCHEDULE_SYNC == "A") {
			result += form_data.MIN + " ";
			result += form_data.HOUR + " ";
			result += form_data.DAY + " ";
			result += form_data.MONTH + " ? ";
			result += form_data.YEAR;
		} else {
			result += form_data.MIN + " ";
			result += form_data.HOUR + " ";
			if(form_data.SCHEDULE_CYCLE == "D") {
				result += "* * ?";
			} else if(form_data.SCHEDULE_CYCLE == "W") {
				result += "? * ";
				result += form_data.WEEK;
			} else if(form_data.SCHEDULE_CYCLE == "M") {
				result += form_data.DAY + " ";
				result +=  "* ?";
			}
		}
		form_data.CRON = result;
	}
	
	function fromCron() {
		var arr = form_data.CRON.split(" ");
		form_data.MIN   = arr[1];
		form_data.HOUR  = arr[2];
		if(arr.length == 7) {
			form_data.SCHEDULE_SYNC = "A";
			form_data.DAY   = arr[3];
			form_data.MONTH = arr[4];
			form_data.YEAR  = arr[6];
		} else if(arr.length == 6){
			form_data.SCHEDULE_SYNC = "S";
			if(arr[5] != "*" && arr[5] != "?") {
				form_data.SCHEDULE_CYCLE = "W";
				form_data.WEEK = arr[5];
			} else if(arr[3] != "*" && arr[3] != "?") {
				form_data.SCHEDULE_CYCLE = "M";
				form_data.DAY   = arr[3];
			} else {
				form_data.SCHEDULE_CYCLE = "D";
			}
		} else {
			form_data.SCHEDULE_SYNC = "C";
		}
	}
	
	function select_vm(vm, callback){
		 
		form_data.VM_NM = vm.VM_NM;
		$("#VM_NM").val(vm.VM_NM);
		form_data.ID = vm.VM_ID;
		if(callback) callback();
	}
	</script>
</div>

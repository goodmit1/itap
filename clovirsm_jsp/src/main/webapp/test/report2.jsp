<%@page import="java.io.FileOutputStream"%>
<%@page import="net.sf.dynamicreports.report.builder.chart.PieChartBuilder"%>
<%@page import="net.sf.dynamicreports.report.builder.chart.BarChartBuilder"%>
<%@page import="net.sf.dynamicreports.report.builder.chart.Bar3DChartBuilder"%>
<%@page import="net.sf.dynamicreports.report.builder.datatype.BigDecimalType"%>
<%@page import="net.sf.dynamicreports.report.base.expression.AbstractValueFormatter"%>
<%@page import="net.sf.dynamicreports.report.builder.style.FontBuilder"%>
<%@page import="java.io.OutputStream"%>
<%@page import="net.sf.dynamicreports.report.builder.column.TextColumnBuilder"%>
<%@page import="net.sf.dynamicreports.report.constant.HorizontalTextAlignment"%>
<%@page import="java.awt.Color"%>
<%@page import="net.sf.dynamicreports.report.builder.style.StyleBuilder"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="net.sf.dynamicreports.report.datasource.DRDataSource"%>
<%@page import="net.sf.dynamicreports.report.constant.HorizontalAlignment"%>
<%@page import="net.sf.dynamicreports.report.builder.component.Components"%>
<%@page import="net.sf.dynamicreports.report.builder.datatype.DataTypes"%>
<%@page import="net.sf.dynamicreports.report.builder.column.Columns"%>
<%@page import="net.sf.dynamicreports.report.builder.DynamicReports"%>
<%@page import="net.sf.dynamicreports.jasper.builder.JasperReportBuilder"%>
<%@ page language="java" contentType="application/pdf; charset=utf-8"
    pageEncoding="utf-8"%>
    <%

	StyleBuilder baseStyle = DynamicReports.stl.style().setFontName("맑은 고딕");

	StyleBuilder boldStyle = DynamicReports.stl.style(baseStyle).bold();


    StyleBuilder boldCenteredStyle = DynamicReports.stl.style(boldStyle).setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
    StyleBuilder titleStyle = DynamicReports.stl.style(boldCenteredStyle).setFontSize(20) ;
    StyleBuilder columnTitleStyle = DynamicReports.stl.style(boldCenteredStyle)
            .setBorder(DynamicReports.stl.pen1Point())
 			.setBackgroundColor( Color.LIGHT_GRAY);
    StyleBuilder subTotalStyle = DynamicReports.stl.style(boldStyle).setBackgroundColor(Color.LIGHT_GRAY);
    TextColumnBuilder<Integer> quantityColumn = Columns.column("<spring:message code="cnt" />", "quantity", DataTypes.integerType());

	DRDataSource dataSource = new DRDataSource("item", "quantity", "unitprice");
	TextColumnBuilder<String> itemColumn = DynamicReports.col.column("Item", "item", DataTypes.stringType());
	TextColumnBuilder  priceColumn =  DynamicReports.col.column("unitprice", "unitprice", new BigDecimalType() {
		@Override
		public String getPattern() {
		    return "#,###원";

		  }

	});
	 BarChartBuilder itemChart = DynamicReports.cht.barChart().setTitle("Sales by item")
			.setCategory(itemColumn)
			.addSerie(DynamicReports.cht.serie(quantityColumn), DynamicReports.cht.serie(priceColumn));
	 PieChartBuilder itemChart2 = DynamicReports.cht.pieChart().setTitle("Sales by item")
			 .setKey(itemColumn)
			 .addSerie(
					DynamicReports.cht.serie(quantityColumn) );

dataSource.add("테스트", 2, new BigDecimal(10000));
dataSource.add("Notebook", 1, new BigDecimal(500));
JasperReportBuilder report = DynamicReports.report();//a new report
report
.highlightDetailEvenRows()
.setColumnStyle(baseStyle)
.setColumnTitleStyle(columnTitleStyle)
.setSubtotalStyle(subTotalStyle)
.subtotalsAtSummary(DynamicReports.sbt.text("Total", itemColumn) , DynamicReports.sbt.sum(quantityColumn), DynamicReports.sbt.sum(priceColumn))
.columns(
		itemColumn,
    quantityColumn,priceColumn)

 .title(//title of the report
		 DynamicReports.cmp.horizontalList().add(
			DynamicReports.cmp.text("제목")
	  		.setStyle(titleStyle)
	  	).newRow(10)
	  )
	  .pageFooter(Components.pageXofY().setStyle(boldCenteredStyle))//show page number on the page footer
	  .setDataSource(dataSource).summary(
			  DynamicReports.cmp.horizontalList(itemChart, itemChart2))
;
response.resetBuffer();
//report.showJrXml().toJrXml(new FileOutputStream("/temp/report2.jrxml"));
	report.toPdf(response.getOutputStream());
	%>
<%@page contentType="text/html; charset=UTF-8" %>
<style>
.custom_rate_text{ 
font-weight:bold; 
font-size:30px ;
}
</style>
<script src="/res/js/jquery.min.js"></script>
<div class="custom_rate" id="test" src="/res/img/cpu.png" height="170" width="170" color="red"></div>
<div class="custom_rate" id="test2" src="/res/img/memory.png" height="170" width="170" color="green"></div>
<div class="custom_rate" id="test3" src="/res/img/disk.png" height="170" width="170" color="orange"></div>
<div class="custom_rate" id="test4" src="/res/img/angry_person.png" height="170" width="170" color="red"></div>
<script>
function run(id, rate, textPos)
{
	if(!textPos)
	{
		textPos = 'in';	
	}
	var container = $("#" + id);
	var h = container.attr("height");
	var w = container.attr("width");
	var src = container.attr("src");
	if(src.startsWith("http"))
	{
		var href = location.href;
		var pos = href.indexOf('/',10);
		src = href.substring(0,pos) + src;	
	}
	var html = "";
	if(textPos == 'up')
	{	
		html += "<div class='custom_rate_text' style='text-align:center; color: " +  container.attr("color") + ";width:" + w + "'>" + rate + "%</div>";
	}
	html += "<svg height='" +  h + "' width='" + w + "'><g>";
	html += '<rect height="' + (h*rate/100) + '" width="' + w + '" y="' + (h - (h*rate/100)) + '" x="0" fill="' + container.attr("color") + '"/>';
	html += "<image xlink:href='" + src + "' height='" +  h + "' width='" + w + "' y='0' x='0'/>";
	if(textPos == 'in')
	{
		var ty = (h - (h*rate/100)*0.7);
		var tcolor = "#ffffff";
		if(rate<50)
		{
			ty = h/2;
			tcolor = container.attr("color"); 
		}
		html += '<text fill="' + tcolor + '" class="custom_rate_text" y="' + ty + '">';
		html += '<tspan x="50%" text-anchor="middle">' +  rate + '%</tspan>';
		html += '</text>'
	}
	html += "</g></svg>";
	if(textPos == 'down')
	{	
		html += "<div class='custom_rate_text' style='text-align:center; color: " +  container.attr("color") + ";width:" + w + "'>" + rate + "%</div>";
	}
	container.html(html);
}
run("test", 80, 'down');
run("test2", 40, 'down');
run("test3", 50, 'down');
run("test4", 40, 'down');
</script>
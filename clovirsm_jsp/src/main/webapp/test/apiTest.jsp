<%--
  Created by IntelliJ IDEA.
  User: kimhk
  Date: 2021-06-01
  Time: 오전 10:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
         pageEncoding="EUC-KR" %>
<script src="/res/js/jquery.min.js"></script>
<script src="/res/js/common.js"></script>
<form>
<pre>
<div id="loginArea">
<%--  dwportal/fBuTZC69R4xv077e1cNS9w --%>
로그인 테스트
userId: <input name="id" value="apiuser">
token: <input name="token" value="nWAUgxQBzeI=">
<input type="button" onclick="login()" value="getAccessKey">
</div>

<div id="getVm">
VM LIST 불러오기
AccessKey : <input name="key" id="key" readonly="readonly">
<input type="button" onclick="getVmList('getVm')" value="getVmList(GET)">
output:<div class="output"></div>
</div>
<div id="getVmId">
하나의 VM 불러오기
VM_NM : <select name="VM_ID" id="VM_ID"></select>
<input type="button" onclick="getVmListId('getVmId')" value="getVmListId(POST)">
output:<div class="output"></div>
</div>
<div id="getDc">
DC LIST 불러오기
<input type="button" onclick="getDcList('getDc')" value="getDcList(GET)">
output:<div class="output"></div>
</div>
<div id="getDcId">
하나의 DC 불러오기
VM_NM : <select name="DC_ID" id="DC_ID"></select>
<input type="button" onclick="getDcListId('getDcId')" value="getDcId(POST)">
output:<div class="output"></div>
</div>

<%--<div id="makeTempApp">--%>
<%--APP_CD: <input name="APP_CD" value="">--%>
<%--TMPL_ID: <input name="TMPL_ID" value="L272296917320000">--%>
<%--<input type="button" onclick="makeTempApp('makeTempApp')" value="makeTempApp(POST)">--%>
<%--output:<div class="output"></div>--%>
<%--</div>--%>
<div id="makeDc">
DC 생성하기
DC_NM(데이터 센터): <input name="DC_NM" value="" required>
CONN_URL(접속URL): <input name="CONN_URL" value="" required>
CONN_USERID(접속URLID): <input name="CONN_USERID" value="" required>
NW_AUTO_YN(네트워크 가상화 여부): <select id="NW" name="NW_AUTO_YN"><option>예</option><option>아니오</option></select>
DEL_YN(삭제 여부): <select id="DEL" name="DEL_YN"><option>예</option><option>아니오</option></select>
INIT_POOL_SIZE(초기연결갯수): <input name="INIT_POOL_SIZE" value="">
REAL_DC_NM(실 데이터센터명): <input name="REAL_DC_NM" value="" required>
IMG_DISK_TYPE_ID(템플릿 디스크종류): <select id="DISK" name="IMG_DISK_TYPE_ID" required><option>보통</option><option>SSD</option></select>
GUEST_CONN_PWD(Default PRK): <input name="GUEST_CONN_PWD" value="">
<input type="button" onclick="makeDc('makeDc')" value="makeDc(POST)">
output:<div class="output"></div>
</div>

</pre>
    <script>
        function login() {
            var param = toObj('#loginArea')
            $.post('/api/guest/login', param, function (data) {
                $("#key").val(data.key)
            })
        }

        function makeVm(id) {
            var param = JSON.stringify(toObj('#' + id))
            if ($("#key").val() != '') {
                $.ajax({
                        data: param,
                        url: '/api/cm/site/project',
                        method: 'POST',
                        contentType: "application/json",
                        headers: {'Authorization': "Bearer " + $("#key").val()}
                    }
                ).done(
                    function (data) {
                        console.log("success!");
                        $('#' + id + " .output").text(JSON.stringify(data));
                    }
                )
                    .fail(
                        function (jqXHR, textStatus, data) {
                            console.log("fail MakeApp")
                            $('#' + id + " .output").text(jqXHR.responseText)
                        }
                    )

            }

        }

        function makeTempApp(id) {
            var param = JSON.stringify(toObj('#' + id))
            if ($("#key").val() != '') {
                $.ajax({
                        url: '/api/cm/site/temp-container',
                        data: param,
                        method: 'POST',
                        contentType: "application/json",
                        headers: {'Authorization': "Bearer " + $("#key").val()}
                    }
                ).done(
                    function (data) {
                        window.open(data.result.URL, '_blank');
                        $('#' + id + " .output").text(JSON.stringify(data))
                    }
                )
                    .fail(
                        function (jqXHR, textStatus, data) {
                            $('#' + id + " .output").text(jqXHR.responseText)
                        }
                    )

            }

        }

        function getVmList(id) {
            if ($("#key").val() != '') {
                $.ajax({
                    url: '/api/vm/list',
                    method: 'GET',
                    contentType: "application/json",
                    headers: {'Authorization': "Bearer " + $("#key").val()}
                })
                    .done(function (data) {
                        var list = data.list;
                        $("#VM_ID").html("");
                        $('#' + id + " .output").html("");
                        $('#' + id + " .output").append("VM 개수: " + list.length);
                        list.forEach((vm, i) => $('#' + id + " .output").append("<div>" + (i+1) +". " + vm.VM_NM + ", "+ "ID: " +vm.VM_ID) + "</div>");
                        list.forEach((vm, i) => $("#VM_ID").append("<option value='"+vm.VM_ID+"'>" + vm.VM_NM + "</option>"))
                    })
                    .fail((data, status) => {
                        console.log("fail getVmList");
                        console.log(data, status);
                    });
            }
        }

        function getVmListId(id) {
            var param = $('#VM_ID option:selected').val();
            if ($("#key").val() != '') {
                $.ajax({
                    url: '/api/vm/info?VM_ID=' + param,
                    method: 'POST',
                    headers: {'Authorization': "Bearer " + $("#key").val()}
                })
                    .done(function (data) {
                        console.log(data);
                        if(data)
                            $('#' + id + " .output").text(data.VM_ID)
                    })
                    .fail((data, status) => {
                        console.log("fail getVmListId");
                        console.log(data, status);
                    });
            }
        }
        function getDcList(id) {
            if ($("#key").val() != '') {
                $.ajax({
                    url: '/api/dc_mng/list',
                    method: 'GET',
                    contentType: "application/json",
                    headers: {'Authorization': "Bearer " + $("#key").val()}
                })
                    .done(function (data) {
                        var list = data.list;
                        $('#' + id + " .output").html("");
                        $('#' + id + " .output").append("DC 개수: " + list.length);
                        list.forEach((dc, i) => $('#' + id + " .output").append("<div>" + (i+1) +". " + dc.DC_NM + ", "+ "ID: " +dc.DC_ID) + "</div>");
                        list.forEach((dc, i) => $("#DC_ID").append("<option value='"+dc.DC_ID+"'>" + dc.DC_NM + "</option>"))
                    })
                    .fail((data, status) => {
                        console.log("fail getVmList");
                        console.log(data, status);
                    });
            }
        }
        function getDcListId(id) {
            var param = $('#DC_ID option:selected').val();
            if ($("#key").val() != '') {
                $.ajax({
                    url: '/api/dc_mng/info?DC_ID=' + param,
                    method: 'POST',
                    headers: {'Authorization': "Bearer " + $("#key").val()}
                })
                    .done(function (data) {
                        console.log(data);
                        if(data)
                            $('#' + id + " .output").text(data.DC_ID)
                    })
                    .fail((data, status) => {
                        console.log("fail getVmListId");
                        console.log(data, status);
                    });
            }
        }
        function makeDc(id) {
            var param = JSON.stringify(toObj('#' + id));
            console.log(param);
            if ($("#key").val() != '') {
                $.ajax({
                        url: '/api/dc_mng/save',
                        data: param,
                        method: 'POST',
                        contentType: "application/json",
                        headers: {'Authorization': "Bearer " + $("#key").val()}
                    }
                ).done(
                    function (data) {
                        $('#' + id + " .output").text(JSON.stringify(data))
                    }
                )
                    .fail(
                        function (jqXHR, textStatus, data) {
                            if(jqXHR.status === 500) {
                                $('#' + id + " .output").text("input 내용을 입력해주세요.");
                            }
                        }
                    )

            }

        }
    </script>
</form>
package fliconz_admin_jsp_sample;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.clovirsm.hv.RestClient;

public class APICall {

	@Test
	public void uuid() {
		System.out.println( UUID.randomUUID().toString());

		 
	}
	@Test
	public void cmapicall() throws   Exception {
		   RestTemplate restTemplate = new RestTemplate();
		   JSONObject param = new JSONObject();
		   param.put("id", "dwportal");
		   param.put("token", "fBuTZC69R4xv077e1cNS9w");
		   String baseUrl = "http://localhost:8080";
		   String authRes = restTemplate.postForObject(baseUrl + "/api/guest/login",param, String.class);
		   JSONObject authJson = new JSONObject(authRes);
		   if(authJson.has("key")) {
			   HttpHeaders headers = new HttpHeaders();
			   headers.add("Authorization", "Bearer " + authJson.getString("key"));
			   JSONObject paramJson = new JSONObject();
			   HttpEntity<String> entity = new HttpEntity<String>(paramJson.toString(),headers);
		       ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/api/cm/site/templates", HttpMethod.GET, entity, String.class);
		       JSONObject json = new JSONObject(response.getBody());
		       if(json.has("error")) {
		    	   throw new Exception(authJson.getString("error"));
		       }
		       else {
		    	   JSONArray templateList = json.getJSONArray("result");
		    	   System.out.println(templateList);
		       }
		   }
		   else {
			   throw new Exception(authJson.getString("error"));
		   }
	}
	 
	@Test
	public void apicall() {
		//RestClient client = new RestClient("http://10.150.0.4:7080");
		RestClient client = new RestClient("http://localhost:8080");
		client.setMimeType("application/x-www-form-urlencoded");
		try {
			JSONObject res = (JSONObject)client.post("/api/guest/login", "id=dwportal&token=fBuTZC69R4xv077e1cNS9w");
			System.out.println(res);
			client.setToken(res.getString("key"));
			JSONObject json = new JSONObject();
			client.setMimeType("application/json");
			json.put("LOGIN_ID", "lkw3").put("USER_NAME", "이광우3").put("EMAIL", "syk11@goodmit.co.kr").put("PRJ_ID", "apitest2").put("PRJ_NM", "apitest2").put("TMPL_ID", "L2157449953164100");
			
			//String param = "LOGIN_ID=syk&USER_NAME=서윤경&EMAIL=syk@goodmit.co.kr&PRJ_ID=apitest&PRJ_NM=apitest&TMPL_ID=L241839705027801";
					
			System.out.println(client.post("/api/cm/site/project", json.toString()));
			//System.out.println(client.get("/api/cm/site/templates" ));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void createR() {
		//RestClient client = new RestClient("http://10.150.0.4:7080");
		RestClient client = new RestClient("http://localhost:8080");
		client.setMimeType("application/x-www-form-urlencoded");
		try {
			JSONObject res = (JSONObject)client.post("/api/guest/login", "id=dwportal&token=fBuTZC69R4xv077e1cNS9w");
			System.out.println(res);
			client.setToken(res.getString("key"));
			JSONObject json = new JSONObject();
			client.setMimeType("application/json");
			json.put("APP_CD", "lkw3").put("TMPL_ID", "L272296917320000");
			
			//String param = "LOGIN_ID=syk&USER_NAME=서윤경&EMAIL=syk@goodmit.co.kr&PRJ_ID=apitest&PRJ_NM=apitest&TMPL_ID=L241839705027801";
			for( int i=0; i < 2; i++) {
				System.out.println(client.post("/api/cm/site/temp-container", json.toString()));
			}
			//System.out.println(client.get("/api/cm/site/templates" ));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void deleteR() {
		//RestClient client = new RestClient("http://10.150.0.4:7080");
		RestClient client = new RestClient("http://localhost:8080");
		client.setMimeType("application/x-www-form-urlencoded");
		try {
			JSONObject res = (JSONObject)client.post("/api/guest/login", "id=dwportal&token=fBuTZC69R4xv077e1cNS9w");
			System.out.println(res);
			client.setToken(res.getString("key"));
			JSONObject json = new JSONObject();
			client.setMimeType("application/json");
			  
					
			System.out.println(client.delete("/api/cm/site/temp-container" ));
			//System.out.println(client.get("/api/cm/site/templates" ));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

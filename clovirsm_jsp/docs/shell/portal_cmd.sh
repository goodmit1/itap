#!/bin/sh


if [ "$1" = "adduser" ] ; then
	linecnt=$(id $2 | grep uid | wc -l)
	if [ "$linecnt" = "1" ]; then
		exit 0
	fi
	sudo useradd -d /home/$2 -m -s /bin/bash -k /etc/skel $2
	echo "$2:P@ssw0rd" | sudo chpasswd
	sudo chage -d 0 $2
else 
	if [ "$1" = "deluser" ] ; then
		if [ "$linecnt" = "0" ]; then
			exit 0
		fi
		sudo userdel -r  $2
	fi
	if [ "$1" = "df" ] ; then
		df > df.txt
	fi
    if [ "$1" = "mount" ] ; then
        echo "$2 $3 nfs defaults 0 0" >> /etc/fstab
        mount $2
    fi
    if [ "$1" = "umount" ] ; then
        a=$( (echo $2 | sed 's/\//\\\//g'))
        sed "/$a/d" -i /etc/fstab
        umount $2
    fi

fi

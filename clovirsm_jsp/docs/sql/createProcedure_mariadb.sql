
CREATE FUNCTION datediff( date1 TIMESTAMP,  dateNum2 NUMERIC) RETURNS decimal(10,0)
BEGIN
	if dateNum2 IS NULL OR dateNum2<=0 OR date1 IS null  THEN
		RETURN -1;
	END IF;
	RETURN CAST(date1 AS DATE) - (STR_TO_DATE('1970-01-01 00','%Y-%m-%d %H') + (dateNum2)/1000/60/60/24) ;
end;
 
 
CREATE or replace FUNCTION  `dept_connect_by_parent`() RETURNS int(11)
    READS SQL DATA
BEGIN

    DECLARE _id INT;

    DECLARE _parent INT;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET @id = NULL;

 

    SET _parent = @id;

    SET _id = -1;



    IF @id IS NULL THEN

        RETURN NULL;

    END IF;

 

    LOOP

        SELECT  MIN(TEAM_CD)

        INTO    @id

        FROM    FM_TEAM

        WHERE   PARENT_CD = _parent

            AND TEAM_CD > _id;

        

 IF @id IS NOT NULL OR _parent = @start_with THEN

            SET @level = @level + 1;

            RETURN @id;

        END IF;

  

        SET @level := @level - 1;

        

 SELECT  TEAM_CD, PARENT_CD

        INTO    _id, _parent

        FROM    FM_TEAM

        WHERE   TEAM_CD = _parent;

    END LOOP;

END;


CREATE  FUNCTION  `fn_parent_dept_id_by_lvl`(a_id varchar(30), a_lvl int) RETURNS varchar(30) CHARSET utf8
    READS SQL DATA
BEGIN

DECLARE _parent varchar(30);

select ID into _parent from (
SELECT   func._id as ID,   @rownum:=@rownum+1 as lvl FROM
(
SELECT B._id 
FROM (
      SELECT @r AS _id, 
                ( SELECT @r := PARENT_CD  FROM FM_TEAM  WHERE TEAM_CD = _id ) AS parent,
                   @l := @l +1 AS lv
     FROM 
                 (
                  SELECT @r := a_id,
                  @l := 0
                 ) vars,
                FM_TEAM d
      WHERE @r <> 0
      ORDER BY lv DESC
) B 
) func Join FM_TEAM d 
ON func._id = d.TEAM_CD,  (select @rownum:=0) t 
) t where lvl=a_lvl;

return _parent;


END

CREATE  FUNCTION  `get_currentTimeMillis`() RETURNS bigint(20) unsigned
begin
	return UNIX_TIMESTAMP(CURRENT_TIMESTAMP )*1000;
end

CREATE  FUNCTION  `get_img_category_ids`(  node varchar(30) ) RETURNS varchar(300) CHARSET utf8
    READS SQL DATA
BEGIN
   	DECLARE _path varchar(200);
	DECLARE _cpath varchar(200);
    DECLARE _id varchar(30);
	DECLARE _nm varchar(100);
    DECLARE EXIT HANDLER FOR NOT FOUND RETURN substring(_path, 1, length(_path)-1);
    SET _id =   COALESCE(node, @id);
    SET _path = '';
    loop
    	
         SELECT   CATEGORY_ID, PAR_CATEGORY_ID
              INTO   _nm, _id
         FROM    NC_IMG_CATEGORY
         WHERE   CATEGORY_ID = _id
                AND COALESCE(CATEGORY_ID <> @start_with, TRUE);
         SET _path = CONCAT(_nm, ',',  _path);
		 
    		 
		 
  	END LOOP;
END


CREATE or replace FUNCTION  `get_img_category_nm`(  node varchar(30) ) RETURNS varchar(300) CHARSET utf8
    READS SQL DATA
BEGIN
    DECLARE _path varchar(800);
	select img_category_by_path('>', node, 'NM') into _path from dual;
	if _path <> '' then
		set _path = substring(_path, 1,   CHAR_LENGTH(_path)-1);
	end if;
	return _path;
END

CREATE or replace FUNCTION  `get_img_category_ids`(  node varchar(30) ) RETURNS varchar(300) CHARSET utf8
    READS SQL DATA
BEGIN
    DECLARE _path varchar(800);
	select img_category_by_path(',', node, 'ID') into _path from dual;
	 
	return _path;
END
 

CREATE   FUNCTION  `get_next_approver`(`_Id` BIGINT) RETURNS varchar(200) CHARSET utf8
BEGIN
DECLARE done INT DEFAULT FALSE;
DECLARE result VARCHAR(200) DEFAULT "";
DECLARE nm VARCHAR(50);
DECLARE c1 CURSOR FOR
     SELECT USER_NAME
     FROM FM_USER a, NC_REQ_NEXT_APPROVER b
     WHERE a.USER_ID=b.APPROVER and b.REQ_ID=_Id ;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;



   OPEN c1;
   get_nm: LOOP
   FETCH c1 INTO nm;


	IF done = TRUE THEN 
		 LEAVE get_nm;
	 END IF;
 -- build email list
   if result != '' then
   	set result  = concat (result , ",");
   end if;	
 	SET result = CONCAT(result,nm);
	END LOOP get_nm;

   CLOSE c1;

   RETURN result;
END


CREATE  FUNCTION  `get_org_nm`( _team_cd  varchar(20)) RETURNS varchar(200) CHARSET utf8
BEGIN
DECLARE result1 VARCHAR(200) DEFAULT ''; 
select comp_nm into result1 from FM_COMPANY a, FM_TEAM b where a.COMP_ID=b.COMP_ID and b.TEAM_CD=_team_cd; 
RETURN result1;
END

CREATE   FUNCTION  `get_spec_nm`( specId VARCHAR(20) , cpuCnt INT, ramSize INT, diskSize INT ,  unit VARCHAR(1) ) RETURNS varchar(600) CHARSET utf8
BEGIN
     declare result1 VARCHAR(200) DEFAULT ''; 
    IF specId = '0' then
       return concat( cpuCnt,  'vCore ', ramSize , 'GB RAM, ', CASE WHEN MOD(diskSize,1024)=0 THEN  concat(format(diskSize/1024,0), 'TB')  ELSE  concat(format(diskSize,0),'GB') end, 'Disk');
   else
       SELECT concat(SPEC_NM, '(', ifnull(cpuCnt, CPU_CNT), 'vCore, ', ifnull(ramSize,RAM_SIZE), 'GB Mem, ', CASE WHEN MOD(diskSize,1024)=0 THEN  concat(format(diskSize/1024,0), 'TB ')  ELSE  concat(format(diskSize,0),'GB ') end,
         DISK_TYPE_NM ,'Disk)')
      INTO result1 FROM NC_VM_SPEC,  NC_DISK_TYPE d  
      WHERE d.DISK_TYPE_ID=NC_VM_SPEC.DISK_TYPE_ID  AND SPEC_ID=specId;
   end if;   
   RETURN result1;

 
END;

CREATE   FUNCTION  `get_template_nm`(os_id1 varchar(30) , from_id1 VARCHAR(30)) RETURNS varchar(200) CHARSET utf8
BEGIN
 declare result1 VARCHAR(200) DEFAULT ''; 	
if from_id1 IS NULL THEN
	IF os_id1 is NULL THEN 
		set result1  = '';
	ELSE 
		SELECT NC_OS_TYPE.OS_NM  into result1 FROM NC_OS_TYPE WHERE NC_OS_TYPE.OS_ID = os_id1;
	END IF;
ELSEif from_id1 IS not NULL THEN
	IF SUBSTR(from_id1, 1, 1) = 'S' THEN SELECT NC_VM.VM_NM into result1 FROM NC_VM WHERE NC_VM.VM_ID = from_id1;
	ELSEIF SUBSTR(from_id1, 1, 1) = 'G' THEN SELECT NC_IMG.IMG_NM into result1 FROM NC_IMG WHERE NC_IMG.IMG_ID = from_id1;
	END IF;
end if;
return result1;
END


CREATE   FUNCTION get_alarm(DC_ID1 varchar(20), OBJ_ID varchar(30)) RETURNS varchar(200)
begin
	
		declare result1 varchar(200) DEFAULT '';	
	SELECT group_concat(ALARM_NM SEPARATOR ',') INTO result1 FROM NC_HV_ALARM  WHERE DC_ID=DC_ID1 AND TARET_ID=OBJ_ID;
    RETURN result1;
END ;
CREATE  FUNCTION  `get_vm_dd_fee`( ID1 VARCHAR(30)  , DC_ID1 VARCHAR(30)  , CPU_CNT1 integer, RAM_SIZE1 integer, DISK_TYPE_ID1 varchar(20), DISK_SIZE1 integer   , SPEC_ID1 VARCHAR(30)  ) RETURNS int(11)
BEGIN 
declare DISK_FEE int default 0;
declare SPEC_FEE int default 0;
declare EXTRA_DISK_SIZE int default DISK_SIZE1;

  IF SPEC_ID1 = '0' THEN
  	SELECT (SELECT DD_FEE FROM NC_ETC_FEE WHERE SVC_CD='C') * CPU_CNT1 + (SELECT DD_FEE FROM NC_ETC_FEE WHERE SVC_CD='M') *RAM_SIZE1+ (select DD_FEE from NC_DISK_TYPE b  where  b.DISK_TYPE_ID=IFNULL( DISK_TYPE_ID1,'1')) * DISK_SIZE1 INTO  SPEC_FEE FROM DUAL;
	RETURN SPEC_FEE;
  elseif SPEC_ID1  is not null and SPEC_ID1 != '' then 
	select b.DD_FEE  , a.DD_FEE  , DISK_SIZE1 -  a.DISK_SIZE  
		into DISK_FEE, SPEC_FEE,EXTRA_DISK_SIZE from NC_VM_SPEC a, NC_DISK_TYPE b  where SPEC_ID=SPEC_ID1 and a.DISK_TYPE_ID=b.DISK_TYPE_ID	;
  else
  	select DD_FEE into DISK_FEE from NC_DISK_TYPE b, NC_DC a where a.IMG_DISK_TYPE_ID=b.DISK_TYPE_ID and a.DC_ID = DC_ID1;
  end if;	
  return SPEC_FEE + (EXTRA_DISK_SIZE*DISK_FEE);


END


CREATE or replace FUNCTION  `get_vm_ip`( dc_id1  varchar(20), vm_nm1 varchar(200),  vm_id1 varchar(20)) RETURNS varchar(200) CHARSET utf8
BEGIN
DECLARE result1 VARCHAR(200) DEFAULT ''; 


if vm_nm1 is   null then 
	select VM_NM , DC_ID into vm_nm1, dc_id1 from NC_VM where VM_ID= vm_id1;
end if;

select  GROUP_CONCAT( IP SEPARATOR ',')   into result1 from NC_IP where DC_ID=dc_id1 and VM_NM = vm_nm1  ;
  
RETURN result1;
END


CREATE or replace  FUNCTION  `img_category_by_path`(delimiter varchar(10), node varchar(30), kubun varchar(5) ) RETURNS varchar(400) CHARSET utf8
    READS SQL DATA
BEGIN
     DECLARE _path varchar(400);
	DECLARE _cpath varchar(400);
    DECLARE _id varchar(30);
	DECLARE _nm varchar(200);
    DECLARE EXIT HANDLER FOR NOT FOUND RETURN _path;
    SET _id =   COALESCE(node, @id);
    SET _path = '';
 	if kubun <> 'NM' then
 		set _path = _id;
	end if;
    LOOP
         SELECT   CATEGORY_NM, PAR_CATEGORY_ID
              INTO   _nm, _id
         FROM    NC_IMG_CATEGORY
         WHERE   CATEGORY_ID = _id
                AND COALESCE(CATEGORY_ID <> @start_with, TRUE);
		 if kubun = 'NM' then 
         	SET _path = CONCAT(_nm, delimiter, _path);
         elseif _id <> '0' then
         	set _path = CONCAT(_id, delimiter, _path);
		 end if;
  	END LOOP;
END



CREATE FUNCTION get_ddic_nm(DD_ID1 varchar(20), DD_VALUE1 varchar(30), LANG1 VARCHAR(2), KO_NM varchar(500)) 
RETURNS  varchar(500)
  

BEGIN
DECLARE	result1 varchar(500) DEFAULT KO_NM;
if LANG1 IS NULL OR LANG1='ko' THEN
	if KO_NM is null then
	 	
		SELECT KO_DD_DESC INTO result1 FROM FM_DDIC WHERE DD_ID=DD_ID1 AND DD_VALUE = DD_VALUE1;
	end if;	
ELSE
	SELECT NM INTO result1 FROM FM_NL WHERE TABLE_NM='FM_DDIC' AND FIELD_NM = 'DD_VALUE' AND ID=concat(concat(DD_ID1, '.'), DD_VALUE1) AND LANG_TYPE=LANG1;
end if;
return result1;
END;


CREATE  FUNCTION  get_old_spec_nm ( vmId VARCHAR(20), intDt varchar(14)) RETURNS varchar(200)  
begin
	
declare 	result1 VARCHAR(200) DEFAULT ''; 

	
SELECT  get_spec_nm(SPEC_ID, CPU_CNT, RAM_SIZE,  DISK_SIZE, DISK_UNIT) SPEC_INFO into result1
	FROM NC_VM_REQ WHERE VM_ID=vmId AND INS_DT = ( SELECT max(INS_DT) FROM NC_VM_REQ WHERE  VM_ID=vmId AND INS_DT < intDT ) and APPR_STATUS_CD='A';
	
if result1 is null or result1 = '' then 
	SELECT  get_spec_nm(SPEC_ID, CPU_CNT, RAM_SIZE,  DISK_SIZE, DISK_UNIT) into result1  from NC_VM WHERE VM_ID=vmId;
end if;
 
	RETURN result1;

 
end;

CREATE  FUNCTION nvl(s decimal(22), d decimal(22)) RETURNS decimal(22,0)
begin
	return ifnull(s,d);
end;


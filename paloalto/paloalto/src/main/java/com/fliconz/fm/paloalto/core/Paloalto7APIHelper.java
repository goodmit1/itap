package com.fliconz.fm.paloalto.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.entity.ContentType;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.paloalto.log.LogFileManager;
import com.fliconz.fm.util.RequestHelper;

public class Paloalto7APIHelper {

	protected static final String RESPONSE_STATUS_SUCCESS = "success";
	protected static final String RESPONSE_STATUS_FAIL = "fail";

	protected static final String KEY_RESULT = "result";
	protected static final String KEY_MEMBER = "member";
	protected static final String KEY_MEMBERS = "members";
	protected static final String KEY_SOURCE = "source";
	protected static final String KEY_SOURCE_USER = "source-user";
	protected static final String KEY_SERVICE = "service";
	protected static final String KEY_DESTINATION = "destination";
	protected static final String KEY_CONTENT = "content";
	protected static final String KEY_ENTRY = "entry";
	protected static final String KEY_NAME = "name";
	protected static final String KEY_IP_NETMASK = "ip-netmask";
	protected static final String KEY_STATIC = "static";
	protected static final String KEY_PROTOCOL = "protocol";
	protected static final String KEY_TCP = "tcp";
	protected static final String KEY_PORT = "port";
	protected static final String KEY_ATTR_STATUS = "status";
	protected static final String KEY_ATTR_TOTAL_COUNT = "total-count";

	/**
	 * Request types of all
	 */
	protected static final String TYPE_KEYGEN = "keygen";
	protected static final String TYPE_CONFIG = "config";
	protected static final String TYPE_COMMIT = "commit";
	protected static final String TYPE_OP = "op";
	protected static final String TYPE_REPORT = "report";
	protected static final String TYPE_LOG = "log";
	protected static final String TYPE_IMPORT = "import";
	protected static final String TYPE_EXPORT = "export";
	protected static final String TYPE_USER_ID = "user-id";
	protected static final String TYPE_VERSION = "version";

	/**
	 * Actions of all
	 */
	protected static final String ACTION_SHOW = "show";
	protected static final String ACTION_GET = "get";
	protected static final String ACTION_SET = "set";
	protected static final String ACTION_EDIT = "edit";
	protected static final String ACTION_DELETE = "delete";
	protected static final String ACTION_RENAME = "rename";
	protected static final String ACTION_CLONE = "clone";
	protected static final String ACTION_MOVE = "move";
	protected static final String ACTION_OVERRIDE = "override";
	protected static final String ACTION_MULTI_MOVE = "multi-move";
	protected static final String ACTION_MULTI_CLONE = "multi-clone";
	protected static final String ACTION_COMPLETE = "complete";


	protected static final String API_KEY_QRY = "type="+TYPE_KEYGEN+"&user=%s&password=%s";
	protected static final String API_READ_QRY = "type=%s&action=%s&xpath=%s&key=%s";
	protected static final String API_MODIFY_QRY = API_READ_QRY + "&element=%s";

	/**
	 * xPath of all
	 */

	protected static final String XPATH_DEVICE_NAME = "localhost.localdomain";
	protected static final String XPATH_VSYS_NAME = "vsys1";

	protected static final String XPATH_NAME = "[@name='%s']";
	protected static final String XPATH_ENTRY = "/entry";
	protected static final String XPATH_ENTRY_NAME = XPATH_ENTRY + XPATH_NAME;
	protected static final String XPATH_MEMBER = "/member[text()='%s']";

	protected static final String XPATH_PREFIX = "/config/devices"+String.format(XPATH_ENTRY_NAME, XPATH_DEVICE_NAME)+"/vsys" + String.format(XPATH_ENTRY_NAME, XPATH_VSYS_NAME);

	protected static final String XPATH_SECURITY_RULE = XPATH_PREFIX + "/rulebase/security/rules" + XPATH_ENTRY;
	protected static final String XPATH_SECURITY_RULE_ENTRY_NAME = XPATH_SECURITY_RULE + XPATH_NAME;
	protected static final String XPATH_SECURITY_DESTINATION = XPATH_SECURITY_RULE_ENTRY_NAME + "/destination";
	protected static final String XPATH_SECURITY_DESTINATION_MEMBER = XPATH_SECURITY_DESTINATION + XPATH_MEMBER;
	protected static final String XPATH_SECURITY_SOURCE = XPATH_SECURITY_RULE_ENTRY_NAME + "/source";
	protected static final String XPATH_SECURITY_SOURCE_MEMBER = XPATH_SECURITY_SOURCE + XPATH_MEMBER;
	protected static final String XPATH_SECURITY_SOURCE_USER = XPATH_SECURITY_RULE_ENTRY_NAME + "/source-user";
	protected static final String XPATH_SECURITY_SOURCE_USER_MEMBER = XPATH_SECURITY_SOURCE_USER + XPATH_MEMBER;

	protected static final String XPATH_SECURITY_SERVICE = XPATH_SECURITY_RULE_ENTRY_NAME + "/service";
	protected static final String XPATH_SECURITY_SERVICE_GROUP_ENTRY = XPATH_SECURITY_RULE_ENTRY_NAME + "/service-group" + XPATH_ENTRY_NAME;


	protected static final String XPATH_ADDRESS_ENTRY = XPATH_PREFIX + "/address" + XPATH_ENTRY_NAME;
	protected static final String XPATH_ADDRESS_IP_NETMASK_ENTRY = XPATH_ADDRESS_ENTRY + "/ip-netmask" + XPATH_ENTRY_NAME;

	protected static final String XPATH_ADDRESS_GROUP_ENTRY = XPATH_PREFIX + "/address-group" + XPATH_ENTRY_NAME;
	protected static final String XPATH_ADDRESS_GROUP_DESCRIPTION = XPATH_ADDRESS_GROUP_ENTRY + "/description";
	protected static final String XPATH_ADDRESS_GROUP_DYNAMIC = XPATH_ADDRESS_GROUP_ENTRY + "/dynamic";
	protected static final String XPATH_ADDRESS_GROUP_DYNAMIC_FILTER = XPATH_ADDRESS_GROUP_DYNAMIC + "/filter";
	protected static final String XPATH_ADDRESS_GROUP_DYNAMIC_FILTER_MEMBER = XPATH_ADDRESS_GROUP_DYNAMIC_FILTER + XPATH_MEMBER;
	protected static final String XPATH_ADDRESS_GROUP_STATIC = XPATH_ADDRESS_GROUP_ENTRY + "/static";
	protected static final String XPATH_ADDRESS_GROUP_STATIC_MEMBER = XPATH_ADDRESS_GROUP_STATIC + XPATH_MEMBER;

	protected static final String XPATH_SERVICE_ENTRY = XPATH_PREFIX + "/service" + XPATH_ENTRY_NAME;
	protected static final String XPATH_SERVICE_DESCRIPTION = XPATH_SERVICE_ENTRY + "/description";

	protected static final String XPATH_SERVICE_GROUP_ENTRY = XPATH_PREFIX + "/service-group" + XPATH_ENTRY_NAME;
	protected static final String XPATH_SERVICE_GROUP_MEMBERS = XPATH_SERVICE_GROUP_ENTRY + "/members";
	protected static final String XPATH_SERVICE_GROUP_MEMBERS_MEMBER = XPATH_SERVICE_GROUP_MEMBERS + XPATH_MEMBER;

	protected static final String ELEMENT_VALUE_ANY = "any";

	protected static final String ELEMENT_MEMBER = "<member>%s</member>";
	protected static final String ELEMENT_MEMBERS = "<members>%s</members>";
	protected static final String ELEMENT_SECURITY_RULE_FROM = "<from><member>any</member></from>";
	protected static final String ELEMENT_SECURITY_RULE_TO = "<to><member>any</member></to>";
	protected static final String ELEMENT_SECURITY_RULE_SOURCE = "<source>%s</source>";
	protected static final String ELEMENT_SECURITY_RULE_DESTINATION = "<destination>%s</destination>";
	protected static final String ELEMENT_SECURITY_RULE = ELEMENT_SECURITY_RULE_FROM + ELEMENT_SECURITY_RULE_TO;

	protected static final String ELEMENT_SECURITY_RULE_SERVICE = "<service>%s</service>";

	protected static final String ELEMENT_SOURCE = "<source>%s</source>";
	protected static final String ELEMENT_SOURCE_USER = "<source-user>%s</source-user>";
	protected static final String ELEMENT_DESTINATION = "<destination>%s</destination>";
	protected static final String ELEMENT_ADDRESS_IP_NETMASK = "<ip-netmask>%s</ip-netmask>";
	protected static final String ELEMENT_ADDRESS_GROUP_STATIC = "<static>%s</static>";

	protected static final String ELEMENT_SERVICE_PROTOCOL = "<protocol>%s</protocol>";
	protected static final String ELEMENT_SERVICE_PROTOCOL_TCP = String.format(ELEMENT_SERVICE_PROTOCOL, "<tcp>%s</tcp>");
	protected static final String ELEMENT_SERVICE_PROTOCOL_TCP_PORT = String.format(ELEMENT_SERVICE_PROTOCOL_TCP, "<port>%s</port>");

	protected static final String ELEMENT_SERVICE_GROUP_MEMBERS = "<members>%s</members>";

	public static boolean isSaveLog = false;
	public static boolean isPrintLog = true;


	/**
	 * 팔로알토 API 키를 가지고 오는 래퍼 함수
	 * @param url
	 * @param username
	 * @param password
	 * @return
	 */
	public static final String getAPIKey(String url, String username, String password){
		String apiKey = assignAPIKey(url, username, password);
		return apiKey;
	}

	/**
	 * 실제로 팔로알토와 통신해서 API키를 발급 받는 함수
	 * 에러가 나거나 결과가 없을 때는 null을 리턴한다.
	 * @param url
	 * @param username
	 * @param password
	 * @return
	 */
	private static final String assignAPIKey(String url, String username, String password){
		JSONObject root = RequestHelper.sendIgnoreSSLRequest(url, String.format(API_KEY_QRY, username, password), RequestMethod.GET, "UTF-8", ContentType.APPLICATION_XML);
		JSONObject  response = root.getJSONObject("response");
		if(RESPONSE_STATUS_SUCCESS.equals(response.getString(KEY_ATTR_STATUS))){
			JSONObject result = response.getJSONObject(KEY_RESULT);
			return result.getString("key");
		}
		return null;
	}

	/**
	 * 팔로알토 API 요청을 수행하는 함수.
	 * 주로 ACTION_GET 요청일 때 쓰인다.
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param type 현재 요청이 어떤 type인지 명시해주는 변수. ex) TYPE_CONFIG
	 * @param action 현재 요청이 어떤 action을 취하는지 명시해주는 변수.
	 * @param xpath 현재 요청하는 데이터에 대한 xpath
	 * @param element 현재 요청하는 action이 SET이나 EDIT일 경우 해당 xpath 밑에 들어갈 xml 내용
	 * @return
	 * @throws Exception
	 */
	protected static final JSONObject executeAPI(String url, String apiKey, String type, String action, String xpath, String element) throws Exception {
		try {
			if(apiKey == null) new Exception("API KEY 인증에 문제가 있습니다.");
			String qryStr = String.format(API_READ_QRY, type, action, xpath, apiKey);
			if(element != null) qryStr = String.format(API_MODIFY_QRY, type, action, xpath, apiKey, element);
			JSONObject root = RequestHelper.sendIgnoreSSLRequest(url, qryStr, RequestMethod.GET, "UTF-8", ContentType.APPLICATION_XML);
			JSONObject  response = root.getJSONObject("response");
			if(isSaveLog) LogFileManager.makeLogFile(url, apiKey, type, action, xpath, element, root);
			if(isPrintLog) LogFileManager.printLog(url, apiKey, type, action, xpath, element, root);
			return response;
		} catch(Exception e){
			throw new PaloaltoAPIException(url, apiKey, type, action, xpath, element, e.getMessage());
		}
	}


	/**
	 * 팔로알토 API 요청을 수행하는 함수.
	 * 주로 ACTION_GET 요청일 때 쓰인다.
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param type 현재 요청이 어떤 type인지 명시해주는 변수. ex) TYPE_CONFIG
	 * @param action 현재 요청이 어떤 action을 취하는지 명시해주는 변수.
	 * @param xpath 현재 요청하는 데이터에 대한 xpath
	 * @return
	 * @throws Exception
	 */
	protected static final JSONObject executeAPI(String url, String apiKey, String type, String action, String xpath) throws Exception {
		return executeAPI(url, apiKey, type, action, xpath, null);
	}

	/**
	 * 해당 xpath의 element를 JSONObject 형태로 가져온다.
	 * 보통 수정 전 조회로 쓴다.
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param xpath 현재 요청하는 데이터에 대한 xpath
	 * @return
	 * @throws Exception
	 */
	private static final JSONObject getElementJSONFromXPath(String url, String apiKey, String xpath) throws Exception{
		JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_GET, xpath);
		if(!RESPONSE_STATUS_SUCCESS.equals(response.getString(KEY_ATTR_STATUS))) throw new Exception("결과 값을 가져오는데 실패하였습니다.");
		return response;
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param ruleName security rule의 이름. vm이름을 주로 쓴다.
	 * @return
	 * @throws Exception
	 */
	public static final boolean existsSecurityRule(String url, String apiKey, String ruleName) throws Exception {
		JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_GET, String.format(XPATH_SECURITY_RULE_ENTRY_NAME, ruleName));
		if(RESPONSE_STATUS_SUCCESS.equals(response.getString(KEY_ATTR_STATUS))){
			try
			{
				JSONObject result = response.getJSONObject(KEY_RESULT);
				return result.getInt(KEY_ATTR_TOTAL_COUNT) > 0;
			}
			catch(Exception e)
			{
				return false;
			}
		} else {
			throw new Exception("시큐리티 룰 정보를 확인하는 중 에러가 발생했습니다.");
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param ruleName security rule의 이름. vm이름을 주로 쓴다.
	 * @throws Exception
	 *
	 * 해당 이름[ruleName]을 가진 시큐리티 룰이 있는지 확인한 후, 없으면 새로 생성한다.
	 */
	public static final void createSecurityRule(String url, String apiKey, String ruleName) throws Exception {
		if(!existsSecurityRule(url, apiKey, ruleName)){
			JSONObject result = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_SET, String.format(XPATH_SECURITY_RULE_ENTRY_NAME, ruleName), ELEMENT_SECURITY_RULE);
			if(!RESPONSE_STATUS_SUCCESS.equals(result.getString(KEY_ATTR_STATUS))) throw new Exception("시큐리티 룰 생성에 실패했습니다.");
		}
	}

	/**
	 * 파라미터로 받은 addressName을 가진 Address객체가 ruleName을 가진 시큐리티 룰의 destination으로 등록되어 있는지 확인한다.
	 * @param url
	 * @param ruleName
	 * @param addressName
	 * @param apiKey
	 * @return
	 * @throws Exception
	 */

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param ruleName security rule의 이름. vm이름을 주로 쓴다.
	 * @param addressGroupName Source에 매핑되어 있는 어드레스 그룹 이름
	 * @return
	 * @throws Exception
	 *
	 * 시큐리티 룰[ruleName]에 [addressGroupName]이 source로 매핑되어 있는지 확인한다.
	 */
	private static final boolean existsSource(String url, String apiKey, String ruleName, String addressGroupName) throws Exception {
		JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_GET, String.format(XPATH_SECURITY_SOURCE_MEMBER, ruleName, addressGroupName));
		if(RESPONSE_STATUS_SUCCESS.equals(response.getString(KEY_ATTR_STATUS))){
			Object resultObj = response.get(KEY_RESULT);
			if(resultObj instanceof String) {
				if("".equals((String)resultObj)) return false;
			}
			JSONObject result = response.getJSONObject(KEY_RESULT);
			return result.getInt(KEY_ATTR_TOTAL_COUNT) > 0;
		} else {
			throw new Exception("소스 정보를 확인하는 중 에러가 발생했습니다.");
		}
	}


	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param addressGroupName 어드레스를 추가할 어드레스 그룹명
	 * @param addressNames 어드레스 그룹에 추가할 어드레스 객체 이름들
	 * @throws Exception
	 */
	private static final void addAddressToGroup(String url, String apiKey, String addressGroupName, String[] addressNames) throws Exception {
		JSONObject checkResponse = getElementJSONFromXPath(url, apiKey, String.format(XPATH_ADDRESS_GROUP_STATIC, addressGroupName));
		if(checkResponse != null){
			Object resultObj = checkResponse.get(KEY_RESULT);
			if(resultObj instanceof String) {
				if("".equals((String)resultObj)) throw new Exception("어드레스 그룹 정보를 가져올 수 없습니다.");
			}
			JSONObject result = checkResponse.getJSONObject(KEY_RESULT);
			JSONObject statics = result.getJSONObject("static");
			String membersTag = "";
			for(String serviceName: addressNames){
				boolean exists = false;
				if(statics.has(KEY_MEMBER)){
					Object memberObj = statics.get(KEY_MEMBER);
					if(memberObj instanceof JSONArray){
						JSONArray membersArr = statics.getJSONArray(KEY_MEMBER);
						for(int i = 0 ; i < membersArr.length(); i++){
							String content = membersArr.getJSONObject(i).getString("content");
							if(content.equals(serviceName)) exists = true;
						}
					} else if(memberObj instanceof JSONObject){
						JSONObject member = (JSONObject)memberObj;
						String content = member.getString("content");
						if(content.equals(serviceName)) exists = true;
					}
				}
				if(!exists) membersTag += String.format(ELEMENT_MEMBER, serviceName);
			}
			if(statics.has(KEY_MEMBER)){
				Object memberObj = statics.get(KEY_MEMBER);
				if(memberObj instanceof JSONArray){
					JSONArray membersArr = statics.getJSONArray(KEY_MEMBER);
					for(int i = 0 ; i < membersArr.length(); i++){
						String content = membersArr.getJSONObject(i).getString("content");
						membersTag += String.format(ELEMENT_MEMBER, content);
					}
				} else if(memberObj instanceof JSONObject){
					JSONObject member = (JSONObject)memberObj;
					String content = member.getString("content");
					membersTag += String.format(ELEMENT_MEMBER, content);
				}
			}

			String element = String.format(ELEMENT_ADDRESS_GROUP_STATIC, membersTag);
			JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_EDIT, String.format(XPATH_ADDRESS_GROUP_STATIC, addressGroupName), element);
			if(!RESPONSE_STATUS_SUCCESS.equals(response.getString(KEY_ATTR_STATUS))) throw new Exception("어드레스 그룹 매핑에 실패했습니다.");
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param ruleName 어드레스 그룹을 소스에 매핑할 시큐리티 룰 이름
	 * @param addressGroupName 소스에 매핑할 어드레스 그룹 명
	 * @throws Exception
	 */
	private static final void setSourceAddressGroup(String url, String apiKey, String ruleName, String addressGroupName) throws Exception {
		if(!existsSource(url, apiKey, ruleName, addressGroupName)){
			String element = String.format(ELEMENT_SOURCE, String.format(ELEMENT_MEMBER, addressGroupName));
			JSONObject result = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_EDIT, String.format(XPATH_SECURITY_SOURCE, ruleName), element);
			if(!RESPONSE_STATUS_SUCCESS.equals(result.getString(KEY_ATTR_STATUS))) throw new Exception("Source 매핑에 실패했습니다.");
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param ruleName 소스 유저에 사용자가 존재하는지 검색할 시큐리티 룰 이름
	 * @param userName 소스 유저에 사용자가 존재하는지 검색할 사용자 이름
	 * @return 사용자가 있으면 true|없으면 false
	 * @throws Exception
	 */
	private static final boolean existsSourceUser(String url, String apiKey, String ruleName, String userName) throws Exception {
		JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_GET, String.format(XPATH_SECURITY_SOURCE_USER, ruleName));
		if(RESPONSE_STATUS_SUCCESS.equals(response.getString(KEY_ATTR_STATUS))){
			Object resultObj = response.get(KEY_RESULT);
			if(resultObj instanceof String) {
				if("".equals((String)resultObj)) return false;
			}
			JSONObject result = response.getJSONObject(KEY_RESULT);
			return result.getInt(KEY_ATTR_TOTAL_COUNT) > 0;
		} else {
			throw new Exception("소스 유저 정보를 확인하는 중 에러가 발생했습니다.");
		}
	}


	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param ruleName 사용자를 소스 유저에 매핑할 시큐리티 룰 이름
	 * @param userName 소스 유저에 매핑할 사용자 이름
	 * @throws Exception
	 */
	public static final void setSourceUser(String url, String apiKey, String ruleName, String userName) throws Exception {
		if(!existsSourceUser(url, apiKey, ruleName, userName)){
			JSONObject checkResponse = getElementJSONFromXPath(url, apiKey, String.format(XPATH_SECURITY_SOURCE_USER, ruleName));
			if(checkResponse != null){
				Object resultObj = checkResponse.get(KEY_RESULT);

				String membersTag = "";

				//없을 때(초기 설정 할 때)
				if(resultObj instanceof String && "".equals((String)resultObj)) {
					membersTag += String.format(ELEMENT_MEMBER, userName);
				} else {
					JSONObject result = checkResponse.getJSONObject(KEY_RESULT);
					JSONObject source_user = result.getJSONObject("source-user");
					Object memberObj = source_user.get(KEY_MEMBER);
					if(memberObj instanceof String[]){
						String[] members = (String[])memberObj;
						for(int i = 0 ; i < members.length; i++){
							membersTag += String.format(ELEMENT_MEMBER, members[i]);
						}
						membersTag += String.format(ELEMENT_MEMBER, userName);
					} else {
						String member = source_user.getString(KEY_MEMBER);
						if(!ELEMENT_VALUE_ANY.equals(member)){
							membersTag += String.format(ELEMENT_MEMBER, member);
						}
						membersTag += String.format(ELEMENT_MEMBER, userName);
					}
				}

				String element = String.format(ELEMENT_SOURCE_USER, membersTag);
				JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_EDIT, String.format(XPATH_SECURITY_SOURCE_USER, ruleName), element);
				if(!RESPONSE_STATUS_SUCCESS.equals(response.getString(KEY_ATTR_STATUS))) throw new Exception("Source user 매핑에 실패했습니다.");
			}
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param ruleName 사용자를 소스 유저에서 제거할 시큐리티 룰 이름
	 * @param userName 제거할 사용자 이름
	 * @param userNameFormat 제거할 사용자 이름 포멧. userName과 조합하여 source user name을 생성한다.
	 * @throws Exception
	 */
	public static final void removeSourceUser(String url, String apiKey, String ruleName, String sourceUserName) throws Exception {
		executeAPI(url, apiKey, TYPE_CONFIG, ACTION_DELETE, String.format(XPATH_SECURITY_SOURCE_USER_MEMBER, ruleName, sourceUserName));
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param ruleName 어드레스가 매핑되어 있는지 확인할 데스티네이션의 시큐리티 룰 이름
	 * @param addressName 데스티네이션에서 매핑되어 있는지 확인할 어드레스 이름
	 * @return
	 * @throws Exception
	 */
	private static final boolean existsDestination(String url, String apiKey, String ruleName, String addressName) throws Exception {
		JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_GET, String.format(XPATH_SECURITY_DESTINATION_MEMBER, ruleName, addressName));
		if(RESPONSE_STATUS_SUCCESS.equals(response.getString(KEY_ATTR_STATUS))){
			Object resultObj = response.get(KEY_RESULT);
			if(resultObj instanceof String) {
				if("".equals((String)resultObj)) return false;
			}
			JSONObject result = response.getJSONObject(KEY_RESULT);
			return result.getInt(KEY_ATTR_TOTAL_COUNT) > 0;
		} else {
			throw new Exception("데스티네이션 정보를 확인하는 중 에러가 발생했습니다.");
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param ruleName 어드레스를 매핑할 데스티네이션의 시큐리티 룰 이름
	 * @param addressName 데스티네이션에 매핑할 어드레스 이름
	 * @param addressIp 어드레스에 세팅할 IP 주소
	 * @throws Exception
	 */
	public static final void setDestinationAddress(String url, String apiKey, String ruleName, String addressName, String addressIp) throws Exception {
		if(!existsDestination(url, apiKey, ruleName, addressName)){
			//svrName을 이름으로 하는 address 객체를 생성한다.
			createAddress(url, apiKey, addressName, addressIp);
			JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_EDIT, String.format(XPATH_SECURITY_DESTINATION, ruleName), String.format(ELEMENT_DESTINATION, String.format(ELEMENT_MEMBER, ruleName)));
			if(!RESPONSE_STATUS_SUCCESS.equals(response.getString(KEY_ATTR_STATUS))) throw new Exception("Destination 매핑에 실패했습니다.");
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param addressName 팔로알토 address 리스트에서 존재하는 지 검색할 어드레스 이름
	 * @return
	 * @throws Exception
	 */
	private static final boolean existsAddressObject(String url, String apiKey, String addressName) throws Exception {
		JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_GET, String.format(XPATH_ADDRESS_ENTRY, addressName));
		if(RESPONSE_STATUS_SUCCESS.equals(response.getString(KEY_ATTR_STATUS))){
			Object resultObj = response.get(KEY_RESULT);
			if(resultObj instanceof String) {
				if("".equals((String)resultObj)) return false;
			}
			JSONObject result = response.getJSONObject(KEY_RESULT);
			return result.getInt(KEY_ATTR_TOTAL_COUNT) > 0;
		} else {
			throw new Exception("어드레스 정보를 확인하는 중 에러가 발생했습니다.");
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param addressName 추가할 어드레스 이름
	 * @param addressIp 추가할 어드레스에 세팅할 ip 주소
	 * @throws Exception
	 */
	private static final void createAddress(String url, String apiKey, String addressName, String addressIp) throws Exception {
		if(!existsAddressObject(url, apiKey, addressName)){
			JSONObject result = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_SET, String.format(XPATH_ADDRESS_ENTRY, addressName), String.format(ELEMENT_ADDRESS_IP_NETMASK, addressIp));
			if(!RESPONSE_STATUS_SUCCESS.equals(result.getString(KEY_ATTR_STATUS))) throw new Exception("Address 생성에 실패했습니다.");
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param addressGroupName 팔로알토 어드레스 그룹 리스트에서 존재하는 지 검색할 어드레스 그룹 이름
	 * @return
	 * @throws Exception
	 */
	private static final boolean existsAddressGroup(String url, String apiKey, String addressGroupName) throws Exception {
		JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_GET, String.format(XPATH_ADDRESS_GROUP_ENTRY, addressGroupName));
		if(RESPONSE_STATUS_SUCCESS.equals(response.getString(KEY_ATTR_STATUS))){
			Object resultObj = response.get(KEY_RESULT);
			if(resultObj instanceof String) {
				if("".equals((String)resultObj)) return false;
			}
			JSONObject result = response.getJSONObject(KEY_RESULT);
			return result.getInt(KEY_ATTR_TOTAL_COUNT) > 0;
		} else {
			throw new Exception("어드레스 그룹 정보를 확인하는 중 에러가 발생했습니다.");
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param addressGroupName 어드레스들을 매핑에서 제거할 어드레스 그룹 이름
	 * @param addressNames 어드레스 그룹에서 제거할 어드레스들의 이름
	 * @throws Exception
	 */
	public static final void removeAddressFromGroup(String url, String apiKey, String addressGroupName, String[] addressNames) throws Exception {
		for(String addressName: addressNames){
			executeAPI(url, apiKey, TYPE_CONFIG, ACTION_DELETE, String.format(XPATH_ADDRESS_GROUP_STATIC_MEMBER, addressGroupName, addressName));
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param addressGroupName 생성할 어드레스 그룹 이름
	 * @param addressNames 어드레스 그룹에 추가할 어드레스들의 이름
	 * @throws Exception
	 */
	private static final void createAddressGroup(String url, String apiKey, String addressGroupName, String[] addressNames) throws Exception {
		if(!existsAddressGroup(url, apiKey, addressGroupName)){
			String addressMembers = "";
			for(String addressName: addressNames){
				addressMembers += String.format(ELEMENT_MEMBER, addressName);
			}
			String element = String.format(ELEMENT_ADDRESS_GROUP_STATIC, addressMembers);
			JSONObject result = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_SET, String.format(XPATH_ADDRESS_GROUP_ENTRY, addressGroupName), element);
			if(!RESPONSE_STATUS_SUCCESS.equals(result.getString(KEY_ATTR_STATUS))) throw new Exception("Address Group["+addressGroupName+"] 생성에 실패했습니다.");
		} else {
			addAddressToGroup(url, apiKey, addressGroupName, addressNames);
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param serviceName 팔로알토 서비스 리스트에서 존재하는지 확인할 서비스 이름
	 * @return
	 * @throws Exception
	 */
	private static final boolean existsServiceObject(String url, String apiKey, String serviceName) throws Exception {
		JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_GET, String.format(XPATH_SERVICE_ENTRY, serviceName));
		if(RESPONSE_STATUS_SUCCESS.equals(response.getString(KEY_ATTR_STATUS))){
			Object resultObj = response.get(KEY_RESULT);
			if(resultObj instanceof String) {
				if("".equals((String)resultObj)) return false;
			}
			JSONObject result = response.getJSONObject(KEY_RESULT);
			return result.getInt(KEY_ATTR_TOTAL_COUNT) > 0;
		} else {
			throw new Exception("서비스 정보를 확인하는 중 에러가 발생했습니다.");
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param serviceName 생성할 서비스 이름
	 * @param port 생성할 서비스에 할당할 포트
	 * @throws Exception
	 */
	private static final void createService(String url, String apiKey, String serviceName, String port) throws Exception {
		if(!existsServiceObject(url, apiKey, serviceName)){
			String element = String.format(ELEMENT_SERVICE_PROTOCOL_TCP_PORT, port);
			JSONObject result = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_SET, String.format(XPATH_SERVICE_ENTRY, serviceName), element);
			if(!RESPONSE_STATUS_SUCCESS.equals(result.getString(KEY_ATTR_STATUS))) throw new Exception("Service["+serviceName+"] 생성에 실패했습니다.");
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param serviceGroupName 팔로알토 서비스 그룹 리스트에서 존재하는지 확인할 서비스 그룹 명
	 * @return
	 * @throws Exception
	 */
	private static final boolean existsServiceGroup(String url, String apiKey, String serviceGroupName) throws Exception {
		JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_GET, String.format(XPATH_SERVICE_GROUP_ENTRY, serviceGroupName));
		if(RESPONSE_STATUS_SUCCESS.equals(response.getString(KEY_ATTR_STATUS))){
			Object resultObj = response.get(KEY_RESULT);
			if(resultObj instanceof String) {
				if("".equals((String)resultObj)) return false;
			}
			JSONObject result = response.getJSONObject(KEY_RESULT);
			return result.getInt(KEY_ATTR_TOTAL_COUNT) > 0;
		} else {
			throw new Exception("서비스 그룹 정보를 확인하는 중 에러가 발생했습니다.");
		}

	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param serviceGroupName 서비스를 추가할 서비스 그룹 이름
	 * @param serviceNames 서비스 그룹에 추가할 서비스들의 이름
	 * @throws Exception
	 */
	private static final void addServiceToGroup(String url, String apiKey, String serviceGroupName, String[] serviceNames) throws Exception {
		JSONObject checkResponse = getElementJSONFromXPath(url, apiKey, String.format(XPATH_SERVICE_GROUP_MEMBERS, serviceGroupName));
		if(checkResponse != null){
			Object resultObj = checkResponse.get(KEY_RESULT);
			if(resultObj instanceof String) {
				if("".equals((String)resultObj)) throw new Exception("서비스 그룹 정보를 가져올 수 없습니다.");
			}
			JSONObject result = checkResponse.getJSONObject(KEY_RESULT);
			if(result.has(KEY_MEMBERS) && result.get(KEY_MEMBERS) instanceof JSONObject) {
				JSONObject members = result.getJSONObject(KEY_MEMBERS);
				String membersTag = "";
				for(String serviceName: serviceNames){
					boolean exists = false;
					if(members.has(KEY_MEMBER)){
						Object memberObj = members.get(KEY_MEMBER);
						if(memberObj instanceof JSONArray){
							JSONArray membersArr = members.getJSONArray(KEY_MEMBER);
							for(int i = 0 ; i < membersArr.length(); i++){
								String content = membersArr.getJSONObject(i).getString("content");
								if(content.equals(serviceName)) exists = true;
							}
						} else if(memberObj instanceof JSONObject){
							JSONObject member = (JSONObject)memberObj;
							String content = member.getString("content");
							if(content.equals(serviceName)) exists = true;
						}
					}
					if(!exists) membersTag += String.format(ELEMENT_MEMBER, serviceName);
				}
				if(members.has(KEY_MEMBER)){
					Object memberObj = members.get(KEY_MEMBER);
					if(memberObj instanceof JSONArray){
						JSONArray membersArr = members.getJSONArray(KEY_MEMBER);
						for(int i = 0 ; i < membersArr.length(); i++){
							String content = membersArr.getJSONObject(i).getString("content");
							membersTag += String.format(ELEMENT_MEMBER, content);
						}
					} else if(memberObj instanceof JSONObject){
						JSONObject member = (JSONObject)memberObj;
						String content = member.getString("content");
						membersTag += String.format(ELEMENT_MEMBER, content);
					}
				}

				String element = String.format(ELEMENT_SERVICE_GROUP_MEMBERS, membersTag);
				JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_EDIT, String.format(XPATH_SERVICE_GROUP_MEMBERS, serviceGroupName), element);
				if(!RESPONSE_STATUS_SUCCESS.equals(response.getString(KEY_ATTR_STATUS))) throw new Exception("서비스 그룹 매핑에 실패했습니다.");
			}
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param serviceGroupName 생성할 서비스 그룹 명
	 * @param DEFAUlT_SERVICES 서비스 그룹 생성시 기본적으로 매핑할 서비스 이름들(이미 생성되어 있어야 한다)
	 * @throws Exception
	 */
	private static final void createServiceGroup(String url, String apiKey, String serviceGroupName, String[] DEFAUlT_SERVICES) throws Exception {
		if(!existsServiceGroup(url, apiKey, serviceGroupName)){
			String serviceMember = "";
			for(String member: DEFAUlT_SERVICES){
				serviceMember += String.format(ELEMENT_MEMBER, member);
			}
			String element = String.format(ELEMENT_MEMBERS, serviceMember);
			JSONObject result = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_SET, String.format(XPATH_SERVICE_GROUP_ENTRY, serviceGroupName), element);
			if(!RESPONSE_STATUS_SUCCESS.equals(result.getString(KEY_ATTR_STATUS))) throw new Exception("SErvice Group["+serviceGroupName+"] 생성에 실패했습니다.");
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param ruleName 서비스 그룹을 세팅할 시큐리티 룰 이름
	 * @param serviceGroupName 세팅할 서비스 그룹 명
	 * @param DEFAUlT_SERVICES 서비스 그룹이 세팅될 때 기본적으로 그룹에 매핑될 서비스들의 이름
	 * @throws Exception
	 */
	public static final void setRuleServiceGroup(String url, String apiKey, String ruleName, String serviceGroupName, String[] DEFAUlT_SERVICES) throws Exception {
		//기본 서비스 그룹 생성 및 서비스 그룹에 기본 서비스 추가
		createServiceGroup(url, apiKey, serviceGroupName, DEFAUlT_SERVICES);

		//추가된 서비스 그룹을 시큐리티 룰에 매핑
		String element = String.format(ELEMENT_SECURITY_RULE_SERVICE, String.format(ELEMENT_MEMBER, serviceGroupName));
		JSONObject result = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_EDIT, String.format(XPATH_SECURITY_SERVICE, ruleName), element);
		if(!RESPONSE_STATUS_SUCCESS.equals(result.getString(KEY_ATTR_STATUS))) throw new Exception("SErvice Group["+serviceGroupName+"] 생성에 실패했습니다.");
	}

	/**
	 * 1. address group  유무 체크 후, 없으면 생성<br>
	 * 2. address 유무 체크 후, 없으면 생성<br>
	 * 3. address group에 address 매핑<br>
	 * 4. rule에 address group를 맴버로 한 source로 매핑<br>
	 * @param url: api url
	 * @param apiKey: 인증받은 apiKey
	 * @param ruleName: VM_NM과 일치하는 security rule 이름
	 * @param addressGroupNameFormat: source에 추가할 address group 이름 패턴
	 * @param clientIp: address group에 추가할 접근 제어 요청한 ip
	 * @param sabun: source-user에 추가할 접근 제어 요청한 user_id
	 * @param userNameFormat: source-user에 추가할 접근 제어 요청한 user 이름 패턴
	 * @throws Exception
	 */

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param ruleName 소스정보를 세팅할 시큐리티 명
	 * @param addressGroupNameFormat 어드레스 그룹명의 스트링 포멧
	 * @param addressIps 추가할 어드레스 IP들
	 * @param sabun userNameFormat과 조합하여 사용자 이름을 만든다.
	 * @param userNameFormat sabun과 조합할 사용자 이름 포멧
	 * @throws Exception
	 */
	public static void setSourceData(String url, String apiKey, String ruleName, String addressGroupNameFormat, String[] addressIps, String sabun, String userNameFormat) throws Exception {
		//IP 추가 및 어드레스 그룹에 추가
		if(addressIps != null){
			addSourceData(url, apiKey, ruleName, addressGroupNameFormat, addressIps);
		}

		//사번 등록
		if(sabun != null){
			String userName = String.format(userNameFormat, sabun);
			Paloalto7APIHelper.setSourceUser(url, apiKey, ruleName, userName);
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param addressGroupName 소스의 어드레스 그룹명
	 * @param addressIps 어드레스 그룹에 추가할 어드레스 IP들
	 * @throws Exception
	 */
	public static void addSourceData(String url, String apiKey, String ruleName, String addressGroupNameFormat, String[] addressIps) throws Exception {
		//어드레스 추가
		if(addressIps != null){
			for(String addressIp: addressIps){
				Paloalto7APIHelper.createAddress(url, apiKey, addressIp, addressIp);
			}
		}

		String addressGroupName = String.format(addressGroupNameFormat, ruleName);

		//어드레스 그룹 추가 및 그룹에 어드레스 매핑
		if(addressIps != null){
			Paloalto7APIHelper.createAddressGroup(url, apiKey, addressGroupName, addressIps);
		}

		//룰 소스에 어드레스 그룹 매핑
		Paloalto7APIHelper.setSourceAddressGroup(url, apiKey, ruleName, addressGroupName);
	}


	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param serviceGroupName 서비스 생성 후, 매핑할 서비스 그룹 명
	 * @param ports 서비스로 등록할 포트들
	 * @param serviceNameFormat 서비스로 등록할 포트와 조합할 서비스이름 포멧
	 * @throws Exception
	 */
	public static void addServices(String url, String apiKey, String serviceGroupName, String[] ports, String serviceNameFormat) throws Exception {
		if(ports != null){
			String[] serviceNames = new String[ports.length];
			for(int i = 0; i < ports.length; i++){
				String port = ports[i];
				String serviceName = String.format(serviceNameFormat, port);
				Paloalto7APIHelper.createService(url, apiKey, serviceName, port);
				serviceNames[i] = serviceName;
			}
			Paloalto7APIHelper.addServiceToGroup(url, apiKey, serviceGroupName, serviceNames);
		}
	}

	/**
	 *
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param serviceGroupName 서비스를 매핑에서 제거할 서비스 그룹 명
	 * @param ports 서비스에서 제거 할 포트들
	 * @param serviceNameFormat 서비스에서 제거할 포트와 조합할 서비스이름 포멧
	 * @throws Exception
	 */
	public static void removeServices(String url, String apiKey, String serviceGroupName, String[] ports, String serviceFormat) throws Exception {
		if(ports != null){
			for(String port: ports){
				executeAPI(url, apiKey, TYPE_CONFIG, ACTION_DELETE, String.format(XPATH_SERVICE_GROUP_MEMBERS_MEMBER, serviceGroupName, String.format(serviceFormat, port)));
			}
		}
	}

	/**
	 * security rule 삭제
	 * @param url 팔로알토 url, ex)https://firewall/api/
	 * @param apiKey getAPIKey함수를 이용해 받아온 apiKey
	 * @param ruleName security rule의 이름. vm이름을 주로 쓴다.
	 * @throws Exception
	 */
	public static void deleteSecurityRule(String url, String apiKey, String ruleName) throws Exception {
		executeAPI(url, apiKey, TYPE_CONFIG, ACTION_DELETE, String.format(XPATH_SECURITY_RULE_ENTRY_NAME, ruleName));
	}


	public static void deleteAddress(String url, String apiKey, String addressName) throws Exception {
		executeAPI(url, apiKey, TYPE_CONFIG, ACTION_DELETE, String.format(XPATH_ADDRESS_ENTRY, addressName));
	}

	public static void deleteAddressGroup(String url, String apiKey, String addressGroupName) throws Exception {
		executeAPI(url, apiKey, TYPE_CONFIG, ACTION_DELETE, String.format(XPATH_ADDRESS_GROUP_ENTRY, addressGroupName));
	}

	public static void deleteServiceGroup(String url, String apiKey, String serviceGroupName) throws Exception {
		executeAPI(url, apiKey, TYPE_CONFIG, ACTION_DELETE, String.format(XPATH_SERVICE_GROUP_ENTRY, serviceGroupName));
	}


	public static List<DataMap<String, Object>> getSecurityRuleList(String url, String apiKey, String rulePrefix) throws Exception {
		List<DataMap<String, Object>> ruleMapList = new ArrayList<DataMap<String, Object>>();
		JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_GET, XPATH_SECURITY_RULE);
		JSONObject rules = response.getJSONObject(KEY_RESULT);
		if(rules.has(KEY_ENTRY)){
			Object entryObj = rules.get(KEY_ENTRY);
			if(entryObj instanceof JSONArray){
				JSONArray entryArray = (JSONArray)entryObj;
				List<DataMap<String, Object>> tempList = new ArrayList<DataMap<String, Object>>();
				int tempCnt = 10;
				int loadCnt = entryArray.length();
				loadCnt = 100;
				for(int i = 0; i < loadCnt; i++){
					if(i >= tempCnt) {
						LogFileManager.makeTextFile(tempList, "temp_result["+(i-10)+"~"+i+"]");
						tempList.clear();
						tempCnt += i;
					}
					
					JSONObject entry = entryArray.getJSONObject(i);
					String ruleName = entry.getString(KEY_NAME);
					if(rulePrefix != null && !ruleName.startsWith(rulePrefix)) continue;
					
					DataMap<String, Object> ruleMap = new DataMap<String, Object>();
					Iterator<String> it = entry.keys();
					while(it.hasNext()){
						String key = it.next();
						ruleMap.put(key, entry.get(key));
					}
					
					ruleMap.put("VM_NM", ruleName);

					ArrayList<DataMap<String, Object>> cidrList = new ArrayList<DataMap<String, Object>>();
					ruleMap.put("CIDR_LIST", cidrList);
					getSourceAddress(url, apiKey, entry, cidrList);

					ArrayList<DataMap<String, Object>> userList = new ArrayList<DataMap<String, Object>>();
					ruleMap.put("USER_LIST", userList);
					getSourceUsers(url, apiKey, entry, userList);

					ArrayList<DataMap<String, Object>> portList = new ArrayList<DataMap<String, Object>>();
					ruleMap.put("PORT_LIST", portList);
					getRuleServices(url, apiKey, entry, portList);

					ArrayList<DataMap<String, Object>> destList = new ArrayList<DataMap<String, Object>>();
					ruleMap.put("SERVER_IPS", destList);
					getDestinationAddress(url, apiKey, entry, destList);
					
					tempList.add(ruleMap);
					ruleMapList.add(ruleMap);
				}
			} else if(entryObj instanceof JSONObject){
				DataMap<String, Object> ruleMap = new DataMap<String, Object>();
				ruleMapList.add(ruleMap);
				JSONObject entry = (JSONObject)entryObj;
				String ruleName = entry.getString(KEY_NAME);
				if(rulePrefix == null || ruleName.startsWith(rulePrefix)){
					ruleMap.put("VM_NM", ruleName);
	
					ArrayList<DataMap<String, Object>> cidrList = new ArrayList<DataMap<String, Object>>();
					ruleMap.put("CIDR_LIST", cidrList);
					getSourceAddress(url, apiKey, entry, cidrList);
	
					ArrayList<DataMap<String, Object>> userList = new ArrayList<DataMap<String, Object>>();
					ruleMap.put("USER_LIST", userList);
					getSourceUsers(url, apiKey, entry, userList);
	
					ArrayList<DataMap<String, Object>> portList = new ArrayList<DataMap<String, Object>>();
					ruleMap.put("PORT_LIST", portList);
					getRuleServices(url, apiKey, entry, portList);
	
					ArrayList<DataMap<String, Object>> destList = new ArrayList<DataMap<String, Object>>();
					ruleMap.put("SERVER_IPS", destList);
					getDestinationAddress(url, apiKey, entry, destList);
				}
			}
			LogFileManager.makeTextFile(ruleMapList, "result");
		}
		return ruleMapList;
	}

	public static void getSourceAddress(String url, String apiKey, JSONObject entry, ArrayList<DataMap<String, Object>> addressList) throws Exception {
		if(!entry.has(KEY_SOURCE)) return;
		JSONObject source = entry.getJSONObject(KEY_SOURCE);
		try {
			if(source.has(KEY_MEMBER)){
				Object sourceMemberObj = source.get(KEY_MEMBER);
				if(sourceMemberObj instanceof JSONArray){
					JSONArray sourceMemberArray = (JSONArray)sourceMemberObj;
					for(int i = 0; i < sourceMemberArray.length(); i++){
						if(sourceMemberArray.get(i) instanceof JSONObject){
							JSONObject sourceMember = sourceMemberArray.getJSONObject(i);
							String sourceMemberName = sourceMember.getString(KEY_CONTENT);
							getAddresses(url, apiKey, sourceMemberName, addressList);
						} else if(sourceMemberArray.get(i) instanceof String && !"".equals(sourceMemberArray.get(i))){
							String sourceMemberName = sourceMemberArray.getString(i);
							getAddresses(url, apiKey, sourceMemberName, addressList);
						}
					}
				} else if(sourceMemberObj instanceof JSONObject){
					JSONObject sourceMember = (JSONObject)sourceMemberObj;
					String sourceMemberName = sourceMember.getString(KEY_CONTENT);
					getAddresses(url, apiKey, sourceMemberName, addressList);
				}
			}
		} catch(PaloaltoAPIException pe){
			throw pe;
		} catch(Exception e){
			throw new PaloaltoAPIException(url, apiKey, source.toString());
		}
	}

	private static void getAddresses(String url, String apiKey, String addressName, ArrayList<DataMap<String, Object>> addressList) throws Exception {
		JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_GET, String.format(XPATH_ADDRESS_GROUP_ENTRY, addressName));
		Object resultObj = response.get(KEY_RESULT);
		try {
			if(resultObj instanceof String && "".equals(resultObj)){
				response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_GET, String.format(XPATH_ADDRESS_ENTRY, addressName));
				if(response.has(KEY_RESULT) && response.get(KEY_RESULT) instanceof JSONObject){
					JSONObject result = response.getJSONObject(KEY_RESULT);
					if(result.has(KEY_ENTRY) && result.get(KEY_ENTRY) instanceof JSONObject){
						JSONObject entry = result.getJSONObject(KEY_ENTRY);
						if(entry.has(KEY_IP_NETMASK) && entry.get(KEY_IP_NETMASK) instanceof JSONObject){
							JSONObject ipNetMask = entry.getJSONObject(KEY_IP_NETMASK);
							String ip = ipNetMask.getString(KEY_CONTENT);
							DataMap<String, Object> cidr = new DataMap<String, Object>();
							cidr.put("IP", ip);
							addressList.add(cidr);
						}
					}
				}
	
			} else {
				if(response.has(KEY_RESULT) && response.get(KEY_RESULT) instanceof JSONObject){
					JSONObject result = response.getJSONObject(KEY_RESULT);
					if(result.has(KEY_ENTRY) && result.get(KEY_ENTRY) instanceof JSONObject){
						JSONObject entry = result.getJSONObject(KEY_ENTRY);
						if(entry.has(KEY_STATIC) && entry.get(KEY_STATIC) instanceof JSONObject){
							JSONObject statics = entry.getJSONObject(KEY_STATIC);
							if(statics.has(KEY_MEMBER)){
								Object memberObj = statics.get(KEY_MEMBER);
								if(memberObj instanceof JSONArray){
									JSONArray memberArray = (JSONArray)memberObj;
									for(int i = 0; i < memberArray.length(); i++){
										if(memberArray.get(i) instanceof JSONObject){
											JSONObject member = memberArray.getJSONObject(i);
											String _addressName = member.getString(KEY_CONTENT);
											getAddresses(url, apiKey, _addressName, addressList);
										} else if(memberArray.get(i) instanceof String && !"".equals(memberArray.get(i))){
											String _addressName = memberArray.getString(i);
											getAddresses(url, apiKey, _addressName, addressList);
										}
									}
								} else if(memberObj instanceof String[]){
									String[] memberArray = (String[])memberObj;
									for(String member: memberArray){
										DataMap<String, Object> cidr = new DataMap<String, Object>();
										cidr.put("IP", member);
										addressList.add(cidr);
									}
								} else if(memberObj instanceof JSONObject){
									JSONObject member = (JSONObject)memberObj;
									String _addressName = member.getString(KEY_CONTENT);
									getAddresses(url, apiKey, _addressName, addressList);
								} 
							}
						}
					}
				}
			}
		} catch(PaloaltoAPIException pe){
			throw pe;
		} catch(Exception e){
			throw new PaloaltoAPIException(url, apiKey, resultObj.toString());
		}
	}

	public static void getSourceUsers(String url, String apiKey, JSONObject entry, ArrayList<DataMap<String, Object>> userMapList) throws Exception {
		if(!entry.has(KEY_SOURCE_USER)) return;
		JSONObject sourceUser = entry.getJSONObject(KEY_SOURCE_USER);
		try {
			if(sourceUser.has(KEY_MEMBER)){
				Object sourceUserMemberObj = sourceUser.get(KEY_MEMBER);
				if(sourceUserMemberObj instanceof JSONArray){
					JSONArray sourceUserMemberArray = (JSONArray)sourceUserMemberObj;
					for(int i = 0; i < sourceUserMemberArray.length(); i++){
						if(sourceUserMemberArray.get(i) instanceof JSONObject){
							JSONObject sourceUserMember = sourceUserMemberArray.getJSONObject(i);
							String sourceUserName = sourceUserMember.getString(KEY_CONTENT);
							DataMap<String, Object> userMap = new DataMap<String, Object>();
							userMap.put("USER_NM", sourceUserName.replace("mobis\\", ""));
							userMapList.add(userMap);
						} else if(sourceUserMemberArray.get(i) instanceof String && !"".equals(sourceUserMemberArray.get(i))){
							String sourceUserName = sourceUserMemberArray.getString(i);
							DataMap<String, Object> userMap = new DataMap<String, Object>();
							userMap.put("USER_NM", sourceUserName.replace("mobis\\", ""));
							userMapList.add(userMap);
						}
					}
				} else if(sourceUserMemberObj instanceof JSONObject){
					JSONObject sourceUserMember = (JSONObject)sourceUserMemberObj;
					String sourceUserName = sourceUserMember.getString(KEY_CONTENT);
					DataMap<String, Object> userMap = new DataMap<String, Object>();
					userMap.put("USER_NM", sourceUserName.replace("mobis\\", ""));
					userMapList.add(userMap);
				}
			}
		} catch(Exception e){
			throw new PaloaltoAPIException(url, apiKey, sourceUser.toString());
		}
	}

	public static void getRuleServices(String url, String apiKey, JSONObject entry, ArrayList<DataMap<String, Object>> portList) throws Exception {
		JSONObject service = entry.getJSONObject(KEY_SERVICE);
		try {
			if(service.has(KEY_MEMBER)){
				Object serviceMemberObj = service.get(KEY_MEMBER);
				if(serviceMemberObj instanceof JSONArray){
					JSONArray serviceMemberArray = (JSONArray)serviceMemberObj;
					for(int i = 0; i < serviceMemberArray.length(); i++){
						if(serviceMemberArray.get(i) instanceof JSONObject){
							JSONObject serviceMember = serviceMemberArray.getJSONObject(i);
							String serviceMemberName = serviceMember.getString(KEY_CONTENT);
							getServices(url, apiKey, serviceMemberName, portList);
						} else if(serviceMemberArray.get(i) instanceof String && !"".equals(serviceMemberArray.get(i))){
							String serviceMemberName = serviceMemberArray.getString(i);
							getServices(url, apiKey, serviceMemberName, portList);
						}
					}
				} else if(serviceMemberObj instanceof JSONObject){
					JSONObject serviceMember = (JSONObject)serviceMemberObj;
					String serviceMemberName = serviceMember.getString(KEY_CONTENT);
					getServices(url, apiKey, serviceMemberName, portList);
				}
			}
		} catch(PaloaltoAPIException pe){
			throw pe;
		} catch(Exception e){
			throw new PaloaltoAPIException(url, apiKey, service.toString());
		}
	}

	private static void getServices(String url, String apiKey, String serviceName, ArrayList<DataMap<String, Object>> portList) throws Exception {
		JSONObject response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_GET, String.format(XPATH_SERVICE_GROUP_ENTRY, serviceName));
		Object resultObj = response.get(KEY_RESULT);
		try {
			if(resultObj instanceof String && "".equals(resultObj)){
				response = executeAPI(url, apiKey, TYPE_CONFIG, ACTION_GET, String.format(XPATH_SERVICE_ENTRY, serviceName));
				if(response.has(KEY_RESULT) && response.get(KEY_RESULT) instanceof JSONObject){
					JSONObject result = response.getJSONObject(KEY_RESULT);
					if(result.has(KEY_ENTRY) && result.get(KEY_ENTRY) instanceof JSONObject){
						JSONObject entry = result.getJSONObject(KEY_ENTRY);
						getPort(entry, portList);
					}
				}
			} else {
				if(response.has(KEY_RESULT) && response.get(KEY_RESULT) instanceof JSONObject){
					JSONObject result = response.getJSONObject(KEY_RESULT);
					if(result.has(KEY_ENTRY) && result.get(KEY_ENTRY) instanceof JSONObject){
						JSONObject entry = result.getJSONObject(KEY_ENTRY);
						if(entry.has(KEY_MEMBERS) && entry.get(KEY_MEMBERS) instanceof JSONObject){
							JSONObject members = entry.getJSONObject(KEY_MEMBERS);
							if(members.has(KEY_MEMBER)){
								Object memberObj = members.get(KEY_MEMBER);
								if(memberObj instanceof JSONArray){
									JSONArray memberArray = (JSONArray)memberObj;
									for(int i = 0; i < memberArray.length(); i++){
										if(memberArray.get(i) instanceof JSONObject){
											JSONObject member = memberArray.getJSONObject(i);
											String _serviceName = member.getString(KEY_CONTENT);
											getServices(url, apiKey, _serviceName, portList);
										} else if(memberArray.get(i) instanceof String && !"".equals(memberArray.get(i))){
											String _serviceName = memberArray.getString(i);
											getServices(url, apiKey, _serviceName, portList);
										}
									}
								} else if(memberObj instanceof JSONObject){
									JSONObject member = (JSONObject)memberObj;
									String _serviceName = member.getString(KEY_CONTENT);
									getServices(url, apiKey, _serviceName, portList);
								}
							}
						}
					}
				}
			}
		} catch(PaloaltoAPIException pe){
			throw pe;
		} catch(Exception e){
			throw new PaloaltoAPIException(url, apiKey, resultObj.toString());
		}
	}

	private static void getPort(JSONObject entry, ArrayList<DataMap<String, Object>> portList) throws Exception {
		try {
			if(entry.has(KEY_PROTOCOL) &&  entry.get(KEY_PROTOCOL) instanceof JSONObject){
				JSONObject protocol =  entry.getJSONObject(KEY_PROTOCOL);
				if(protocol.has(KEY_TCP) && protocol.get(KEY_TCP) instanceof JSONObject){
					JSONObject tcp = protocol.getJSONObject(KEY_TCP);
					if(tcp.has(KEY_PORT) && tcp.get(KEY_PORT) instanceof JSONObject){
						JSONObject port = tcp.getJSONObject(KEY_PORT);
						DataMap<String, Object> portMap = new DataMap<String, Object>();
						portMap.put("PORT_NM", entry.get(KEY_NAME));
						portMap.put("PORT", port.get(KEY_CONTENT));
						portList.add(portMap);
					}
				}
			}
		} catch(Exception e){
			throw new PaloaltoAPIException("getPort: " + entry);
		}
	}

	private static void getDestinationAddress(String url, String apiKey, JSONObject entry, ArrayList<DataMap<String, Object>> destList) throws Exception {
		if(!entry.has(KEY_DESTINATION)) return;
		JSONObject destination = entry.getJSONObject(KEY_DESTINATION);
		try {
			if(destination.has(KEY_MEMBER)){
				Object destMemberObj = destination.get(KEY_MEMBER);
				if(destMemberObj instanceof JSONArray){
					JSONArray destMemberArray = (JSONArray)destMemberObj;
					for(int i = 0; i < destMemberArray.length(); i++){
						if(destMemberArray.get(i) instanceof JSONObject){
							JSONObject destMember = destMemberArray.getJSONObject(i);
							String destMemberName = destMember.getString(KEY_CONTENT);
							getAddresses(url, apiKey, destMemberName, destList);
						} else if(destMemberArray.get(i) instanceof String && !"".equals(destMemberArray.get(i))){
							String destMemberName = destMemberArray.getString(i);
							getAddresses(url, apiKey, destMemberName, destList);
						}
					}
				} else if(destMemberObj instanceof JSONObject){
					JSONObject destMember = (JSONObject)destMemberObj;
					String destMemberName = destMember.getString(KEY_CONTENT);
					getAddresses(url, apiKey, destMemberName, destList);
				}
			}
		} catch(PaloaltoAPIException pe){
			throw pe;
		} catch(Exception e){
			throw new PaloaltoAPIException(url, apiKey, destination.toString());
		}
	}
}

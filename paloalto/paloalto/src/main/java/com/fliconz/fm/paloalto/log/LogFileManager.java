package com.fliconz.fm.paloalto.log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.fliconz.fm.common.util.DataMap;

public class LogFileManager {


	public static final void makeLogFile(String url, String apiKey, String type, String action, String xpath, JSONObject response){
		makeLogFile(url, apiKey, type, action, xpath, null, response);
	}

	public static final void makeLogFile(String url, String apiKey, String type, String action, String xpath, String element, JSONObject response){
		FileOutputStream fos = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			String fileName = xpath + "/" + type + "/" + action;
			File logFile = new File("paloalto-log", fileName + "/" + sdf.format(new Date())+".txt");
			logFile.getParentFile().mkdirs();
			logFile.createNewFile();
			fos = new FileOutputStream(logFile, true);


			StringBuffer sb = new StringBuffer();
			sb.append("==================== REQUEST ================").append("\n");
			sb.append("param[type]: " + type).append("\n");
			sb.append("param[action]: " + action).append("\n");
			sb.append("param[xpath]: " + xpath).append("\n");
			sb.append("param[element]: " + element).append("\n");
			sb.append("=============================================").append("\n");
			sb.append("==================== RESPONSE ===============").append("\n");
			sb.append(response.toString()).append("\n");
			sb.append("=============================================").append("\n");
			fos.write(sb.toString().getBytes("UTF-8"));
		} catch(Exception e){
			e.printStackTrace();
		} finally {
			try {
				if(fos != null){
					fos.flush();
					fos.close();
				}
			} catch(IOException e){
				e.printStackTrace();
			}
		}
	}

	public static final void makeQueryLogFile(String query, Map<String, Object> param, String[] paramKeys, int rs){
		FileOutputStream fos = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			String fileName = (String)param.get(paramKeys[0]);
			File logFile = new File("paloalto-log/query", fileName + "/" + sdf.format(new Date())+".txt");
			logFile.getParentFile().mkdirs();
			logFile.createNewFile();
			fos = new FileOutputStream(logFile, true);


			StringBuffer sb = printQueryLogFile(query, param, paramKeys, rs);
			fos.write(sb.toString().getBytes("UTF-8"));
		} catch(Exception e){
			e.printStackTrace();
		} finally {
			try {
				if(fos != null){
					fos.flush();
					fos.close();
				}
			} catch(IOException e){
				e.printStackTrace();
			}
		}
	}
	
	public static final StringBuffer printQueryLogFile(String query, Map<String, Object> param, String[] paramKeys, int rs){
		StringBuffer sb = new StringBuffer();
		sb.append("==================== QUERY ================").append("\n");
		sb.append("query: " + query).append("\n");
		sb.append("param: " + param).append("\n");
		sb.append("paramKeys: " + paramKeys).append("\n");
		sb.append("===================== RS ==================").append("\n");
		sb.append(rs).append("\n");
		sb.append("=============================================").append("\n");
		System.out.println(sb);
		return sb;
	}

	public static final void makeQueryLogFile(String query, Map<String, Object> param, String[] paramKeys, DataMap<String, Object> resultMap){
		FileOutputStream fos = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			String fileName = (String)param.get(paramKeys[0]);
			File logFile = new File("paloalto-log/query", fileName + "/" + sdf.format(new Date())+".txt");
			logFile.getParentFile().mkdirs();
			logFile.createNewFile();
			fos = new FileOutputStream(logFile, true);


			StringBuffer sb = new StringBuffer();
			sb.append("==================== QUERY ================").append("\n");
			sb.append("query: " + query).append("\n");
			sb.append("param: " + param).append("\n");
			sb.append("paramKeys: " + paramKeys).append("\n");
			sb.append("===================== RS ==================").append("\n");
			sb.append(resultMap).append("\n");
			sb.append("=============================================").append("\n");
			fos.write(sb.toString().getBytes("UTF-8"));
		} catch(Exception e){
			e.printStackTrace();
		} finally {
			try {
				if(fos != null){
					fos.flush();
					fos.close();
				}
			} catch(IOException e){
				e.printStackTrace();
			}
		}
	}

	public static final void printLog(String url, String apiKey, String type, String action, String xpath, String element, JSONObject response){
		StringBuffer sb = new StringBuffer();
		sb.append("==================== REQUEST ================").append("\n");
		sb.append("param[type]: " + type).append("\n");
		sb.append("param[action]: " + action).append("\n");
		sb.append("param[xpath]: " + xpath).append("\n");
		sb.append("param[element]: " + element).append("\n");
		sb.append("=============================================").append("\n");
		sb.append("==================== RESPONSE ===============").append("\n");
		sb.append(response.toString()).append("\n");
		sb.append("=============================================").append("\n");
		System.out.println(sb);
	}

	public static final void makeErrorLogFile(Exception pe){
		FileOutputStream fos = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			String fileName = "error";
			File logFile = new File("paloalto-log", fileName + "/" + sdf.format(new Date())+".txt");
			logFile.getParentFile().mkdirs();
			logFile.createNewFile();
			fos = new FileOutputStream(logFile, true);

			StringBuffer sb = new StringBuffer();
			sb.append(pe.getMessage()).append(pe.getCause()).append("\n");
			StackTraceElement[] trace = pe.getStackTrace();
            for (StackTraceElement traceElement : trace){
            	sb.append("\tat " + traceElement).append("\n");
            }

			fos.write(sb.toString().getBytes("UTF-8"));
		} catch(Exception e){
			e.printStackTrace();
		} finally {
			try {
				if(fos != null){
					fos.flush();
					fos.close();
				}
			} catch(IOException e){
				e.printStackTrace();
			}
		}
	}

	public static final void printErrorLog(Exception pe){
		StringBuffer sb = new StringBuffer();
		sb.append("==================== API ERROR ================").append("\n");
		sb.append(pe.getMessage()).append("\n");

		StackTraceElement[] trace = pe.getStackTrace();
        for (StackTraceElement traceElement : trace){
        	sb.append("\tat " + traceElement).append("\n");
        }
        sb.append("=============================================").append("\n");
        System.out.println(sb);
	}

	public static final void makeTextFile(List<DataMap<String, Object>> ruleMapList){
		makeTextFile(ruleMapList, null);
	}

	public static final void makeTextFile(List<DataMap<String, Object>> ruleMapList, String flieName){
		FileOutputStream fos = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			String logFlieName = flieName + "_" + sdf.format(new Date());
			if(flieName == null) logFlieName = sdf.format(new Date());
			File logFile = new File("paloalto-log", logFlieName +".txt");
			logFile.getParentFile().mkdirs();
			logFile.createNewFile();
			fos = new FileOutputStream(logFile, true);


			StringBuffer sb = new StringBuffer();
			sb.append("==================== RULES ===============").append("\n").append("\n");
			for(DataMap<String, Object> ruleMap: ruleMapList){
				sb.append("==== RULE NAME: [" + ruleMap.getString("VM_NM") +"]").append("\n");
				sb.append("==== SERVER_IP(s) ====").append("\n");
				sb.append(ruleMap.getList("SERVER_IPS")).append("\n");
				sb.append("==== CIDR_LIST ====").append("\n");
				sb.append(ruleMap.getList("CIDR_LIST")).append("\n");
				sb.append("==== USER_LIST ====").append("\n");
				sb.append(ruleMap.getList("USER_LIST")).append("\n");
				sb.append("==== PORT_LIST ====").append("\n");
				sb.append(ruleMap.getList("PORT_LIST")).append("\n");
				sb.append("=============================================").append("\n").append("\n");
			}
			sb.append("=============================================").append("\n");
			fos.write(sb.toString().getBytes("UTF-8"));
		} catch(Exception e){
			e.printStackTrace();
		} finally {
			try {
				if(fos != null){
					fos.flush();
					fos.close();
				}
			} catch(IOException e){
				e.printStackTrace();
			}
		}
	}
}

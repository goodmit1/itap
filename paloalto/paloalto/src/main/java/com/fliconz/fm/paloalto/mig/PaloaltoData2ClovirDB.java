package com.fliconz.fm.paloalto.mig;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.paloalto.client.PaloaltoDataClient;
import com.fliconz.fm.paloalto.client.PaloaltoFWClient;
import com.fliconz.fm.paloalto.core.PaloaltoAPIHelper;
import com.fliconz.fm.paloalto.log.LogFileManager;
import com.fliconz.fm.paloalto.mig.core.DBClientFactory;
import com.fliconz.fm.paloalto.mig.core.QueryHandler;

public class PaloaltoData2ClovirDB {

	private List<DataMap<String, Object>> paloaltoDataList;

	public PaloaltoData2ClovirDB() { }

	public void loadPaloaltoData() {
		try {
			File paloaltoDataListFile = new File("paloalto-log", "data/paloaltoDataList.plt");
			if(!paloaltoDataListFile.exists()){
				PaloaltoAPIHelper.isSaveLog = true;
				String url = "https://10.230.253.28";
				String userId = "mclovir";

	//			String url = "https://10.230.136.26";
	//			String userId = "potest";

				String pwd = "VMware1!";
				PaloaltoDataClient client = new PaloaltoDataClient(url, userId, pwd);

				this.paloaltoDataList = client.getSecurityRuleList("KRDM");
				if(this.paloaltoDataList.size() == 0){
					throw new Exception("기존 팔로알토 데이터를 불러오는데 실패하였습니다.");
				}

				paloaltoDataListFile.getParentFile().mkdirs();
				paloaltoDataListFile.createNewFile();

				FileOutputStream fos = new FileOutputStream(paloaltoDataListFile, true);
				ObjectOutputStream objectOut = new ObjectOutputStream(fos);
				objectOut.writeObject(new PaloaltoDataObject(this.paloaltoDataList));
				objectOut.close();
			} else {
				FileInputStream fis = new FileInputStream(paloaltoDataListFile);
				ObjectInputStream objectInput = new ObjectInputStream(fis);
				PaloaltoDataObject paloaltoDataObject = (PaloaltoDataObject)objectInput.readObject();
				this.paloaltoDataList = paloaltoDataObject.getData();
				objectInput.close();
			}
		} catch(Exception e){
			e.printStackTrace();
			LogFileManager.makeErrorLogFile(e);
		}
	}

	public void migrationPaloaltoData2ClovirDB() throws Exception {
		//clovirSM version 1 db
		DataMap<String, Object> connParam = new DataMap<String, Object>();
		connParam.put(DBClientFactory.KEY_DRIVER, DBClientFactory.DRIVER_MARIADB);
		connParam.put(DBClientFactory.KEY_URL, "10.230.9.252");
		connParam.put(DBClientFactory.KEY_PORT, "3306");
		connParam.put(DBClientFactory.KEY_DB_NAME, "gitms");
		connParam.put(DBClientFactory.KEY_USERNAME, "goodmit");
		connParam.put(DBClientFactory.KEY_PASSWORD, "git3775*");
		QueryHandler qh = new QueryHandler(connParam);

		String query = "select a.VM_ID, a.VM_NM, a.VM_HV_ID, a.HOST_HV_ID, b.PRIVATE_IP, b.PRIVATE_IP_NUM from nc_vm a left outer join nc_vm_nic b on b.VM_ID = a.VM_ID ";
		DataMap<String, Object> param = new DataMap<>();
		String[] resultKeys = { "VM_ID", "VM_NM", "VM_HV_ID", "HOST_HV_ID", "PRIVATE_IP", "PRIVATE_IP_NUM"};
		int[] pks = {1};
		DataMap<String, Object> vmMap = qh.excute(query, param, resultKeys, pks);


		//clovirSM version 2 db
		DataMap<String, Object> connParam2 = new DataMap<String, Object>();
		connParam2.put(DBClientFactory.KEY_DRIVER, DBClientFactory.DRIVER_MARIADB);
		connParam2.put(DBClientFactory.KEY_URL, "10.230.9.253");
		connParam2.put(DBClientFactory.KEY_PORT, "3306");
		connParam2.put(DBClientFactory.KEY_DB_NAME, "clovirsm");
		connParam2.put(DBClientFactory.KEY_USERNAME, "goodmit");
		connParam2.put(DBClientFactory.KEY_PASSWORD, "git3775*");
		QueryHandler qh2 = new QueryHandler(connParam2);
		qh2.commit();
		try {
			for(DataMap<String, Object> paloMap: this.paloaltoDataList){
				String p_vmNm = paloMap.getString("VM_NM");
				String vmKey = p_vmNm;
				ArrayList<DataMap<String, Object>> destList = (ArrayList<DataMap<String, Object>>)paloMap.getList("SERVER_IPS");
				String vmIp = "";
				DataMap<String, Object> vmInfo = vmMap.getDataMap(vmKey);
				for(DataMap<String, Object> dest: destList){
					if(destList.indexOf(dest) > 0) vmIp += ",";
					String ip = dest.getString("IP");
					vmIp += ip;
				}
				if(vmInfo == null) vmInfo = new DataMap<String, Object>();
				vmInfo.put("RULE_NAME", p_vmNm);
				vmInfo.put("SERVER_IPS", vmIp);
				ArrayList<DataMap<String, Object>> cidrList = (ArrayList<DataMap<String, Object>>)paloMap.getList("CIDR_LIST");
				ArrayList<DataMap<String, Object>> portList = (ArrayList<DataMap<String, Object>>)paloMap.getList("PORT_LIST");
				ArrayList<DataMap<String, Object>> userList = (ArrayList<DataMap<String, Object>>)paloMap.getList("USER_LIST");

				String insertQuery = "insert into nc_vm_paloalto_client_ips(VM_ID, VM_NM, VM_HV_ID, HOST_HV_ID, PRIVATE_IP, PRIVATE_IP_NUM, RULE_NAME, SERVER_IPS, CLIENT_IP) values(?,?,?,?,?,?,?,?,?)";
				String[] paramKeys = { "VM_ID", "VM_NM", "VM_HV_ID", "HOST_HV_ID", "PRIVATE_IP", "PRIVATE_IP_NUM", "RULE_NAME", "SERVER_IPS", "CLIENT_IP" };
				for(DataMap<String, Object> cidr: cidrList){
					vmInfo.put("CLIENT_IP", cidr.getString("IP"));
					qh2.update(insertQuery, vmInfo, paramKeys);
				}
				vmInfo.remove("CLIENT_IP");
				String insertQuery2 = "insert into nc_vm_paloalto_source(VM_ID, VM_NM, VM_HV_ID, HOST_HV_ID, PRIVATE_IP, PRIVATE_IP_NUM, RULE_NAME, PORTS) values(?,?,?,?,?,?,?,?)";
				String[] paramKeys2 = { "VM_ID", "VM_NM", "VM_HV_ID", "HOST_HV_ID", "PRIVATE_IP", "PRIVATE_IP_NUM", "RULE_NAME", "PORTS" };
				String ports = "";
				for(DataMap<String, Object> port: portList){
					if(portList.indexOf(port) > 0) ports += ",";
					ports += port.getString("PORT");
				}
				vmInfo.put("PORTS", ports);
				qh2.update(insertQuery2, vmInfo, paramKeys2);
				vmInfo.remove("PORTS");


				String insertQuery3 = "insert into nc_vm_paloalto_source_user(VM_ID, VM_NM, VM_HV_ID, HOST_HV_ID, PRIVATE_IP, PRIVATE_IP_NUM, RULE_NAME, USER_ID) values(?,?,?,?,?,?,?,?)";
				String[] paramKeys3 = { "VM_ID", "VM_NM", "VM_HV_ID", "HOST_HV_ID", "PRIVATE_IP", "PRIVATE_IP_NUM", "RULE_NAME", "USER_ID" };
				for(DataMap<String, Object> user: userList){
					vmInfo.put("USER_ID", user.getString("USER_NM"));
					qh2.update(insertQuery3, vmInfo, paramKeys3);
				}
				vmInfo.remove("USER_ID");
				vmInfo.remove("SERVER_IPS");
				vmInfo.remove("RULE_NAME");
			}
			qh2.commit();
		} catch(Exception e){
			qh2.rollback();
			throw e;
		}
	}

	private static void migrationTest(){
		try {
			PaloaltoAPIHelper.isSaveLog = true;
			PaloaltoData2ClovirDB pm = new PaloaltoData2ClovirDB();
			pm.loadPaloaltoData();
			pm.migrationPaloaltoData2ClovirDB();
		} catch(Exception e){
			e.printStackTrace();
			if(PaloaltoAPIHelper.isSaveLog) LogFileManager.makeErrorLogFile(e);
			if(PaloaltoAPIHelper.isPrintLog) LogFileManager.printErrorLog(e);
		}
	}


	private static void insertTest() {
		try {
			PaloaltoAPIHelper.isSaveLog = true;
			String url = "https://10.230.136.26";
			String userId = "potest";
			String pwd = "VMware1!";
			String svrName = "API_TEST_SERVER_002";
			String[] serverIps = {"111.111.111.222"};
			Map<String, Object> prop = new HashMap();
			prop.put("beforeRule", "rule1");
			PaloaltoFWClient client = new PaloaltoFWClient(url, userId, pwd, svrName, serverIps, prop);

			String[] clientIps = {"111.222.222.221", "111.222.222.222" };
			String[] ports = {"1020", "8020"};
			String sabun = "DT123555";

			client.insert(clientIps, ports, sabun);
		} catch(Exception e){
			e.printStackTrace();
			if(PaloaltoAPIHelper.isSaveLog) LogFileManager.makeErrorLogFile(e);
			if(PaloaltoAPIHelper.isPrintLog) LogFileManager.printErrorLog(e);
		}
	}

	private static void insertTest2() {
		try {
			PaloaltoAPIHelper.isSaveLog = true;
			String url = "https://10.230.136.26";
			String userId = "potest";
			String pwd = "VMware1!";
			String svrName = "API_TEST_SERVER_002";
			String[] serverIps = {"111.222.111.222"};
			Map<String, Object> prop = new HashMap();
			prop.put("beforeRule", "rule1");
			PaloaltoFWClient client = new PaloaltoFWClient(url, userId, pwd, svrName, serverIps, prop);

			String[] clientIps = {"111.111.222.221", "111.222.222.221" };
			String[] ports = {"4030", "2030"};
			String sabun = "DT123455";
			String sabun2 = "DT135754";
			client.insertServerIp(serverIps);
			client.insertClientIp(clientIps);
			client.insertPort(ports);
			client.insertSabun(sabun);
			client.insertSabun(sabun2);

		} catch(Exception e){
			e.printStackTrace();
			if(PaloaltoAPIHelper.isSaveLog) LogFileManager.makeErrorLogFile(e);
			if(PaloaltoAPIHelper.isPrintLog) LogFileManager.printErrorLog(e);
		}
	}

	private static void deleteTest() {
		try {
			PaloaltoAPIHelper.isSaveLog = true;
			String url = "https://10.230.136.26";
			String userId = "potest";
			String pwd = "VMware1!";
			String svrName = "API_TEST_SERVER_002";
			String[] serverIps = {"111.111.111.111"};
			PaloaltoFWClient client = new PaloaltoFWClient(url, userId, pwd, svrName, serverIps, null);
			client.deleteServer();
		} catch(Exception e){
			e.printStackTrace();
			if(PaloaltoAPIHelper.isSaveLog) LogFileManager.makeErrorLogFile(e);
			if(PaloaltoAPIHelper.isPrintLog) LogFileManager.printErrorLog(e);
		}
	}

	private static void deleteTest2() {
		try {
			PaloaltoAPIHelper.isSaveLog = true;
			String url = "https://10.230.136.26";
			String userId = "potest";
			String pwd = "VMware1!";
			String svrName = "API_TEST_SERVER_002";
			String[] serverIps = {"111.111.111.222"};
			PaloaltoFWClient client = new PaloaltoFWClient(url, userId, pwd, svrName, serverIps, null);
			client.deleteServerIp(serverIps);

			String[] clientIps = {"111.222.222.221"};
			client.deleteClientIp(clientIps);

			String[] ports = {"1020", "4030"};
			client.deletePort(ports);

			String sabun = "DT123455";
			String sabun2 = "DT135754";
			client.deleteSabun(sabun);
			client.deleteSabun(sabun2);
		} catch(Exception e){
			e.printStackTrace();
			if(PaloaltoAPIHelper.isSaveLog) LogFileManager.makeErrorLogFile(e);
			if(PaloaltoAPIHelper.isPrintLog) LogFileManager.printErrorLog(e);
		}
	}

	private static void migrationTest2(boolean isNew){
		try {
			PaloaltoAPIHelper.isSaveLog = true;
			PaloaltoData2ClovirDB pm = new PaloaltoData2ClovirDB();
			if(isNew) new File("paloalto-log", "data/paloaltoDataList.plt").delete();
			pm.loadPaloaltoData();
			pm.migrationPaloaltoData2ClovirDB();
		} catch(Exception e){
			e.printStackTrace();
			if(PaloaltoAPIHelper.isSaveLog) LogFileManager.makeErrorLogFile(e);
			if(PaloaltoAPIHelper.isPrintLog) LogFileManager.printErrorLog(e);
		}
	}

	public static void main(String[] args) throws Exception{
		//migrationTest2(false);
		insertTest();
	}

}

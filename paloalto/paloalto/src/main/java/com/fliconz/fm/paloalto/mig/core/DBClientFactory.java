package com.fliconz.fm.paloalto.mig.core;

import java.sql.Connection;
import java.sql.DriverManager;

import com.fliconz.fm.common.util.DataMap;

public class DBClientFactory {

	public static final String DRIVER_MARIADB = "org.mariadb.jdbc.Driver";
	public static final String DRIVER_MYSQL = "com.mysql.jdbc.Driver";

	public static final String FORMAT_MARIADB_JDBC_URL = "jdbc:mysql://%s:%s/%s";

	public static final String KEY_DRIVER = "driver";
	public static final String KEY_URL = "url";
	public static final String KEY_PORT = "port";
	public static final String KEY_DB_NAME = "name";
	public static final String KEY_USERNAME = "user";
	public static final String KEY_PASSWORD = "pwd";

	public static Connection getConnection(DataMap<String, Object> param) throws  Exception {
		String driver = param.getString(KEY_DRIVER);
		Connection conn = null;
		if(DRIVER_MARIADB.equals(driver)){
			Class.forName(driver);
			String url = param.getString(KEY_URL);
			String port = param.getString(KEY_PORT);
			String name = param.getString(KEY_DB_NAME);
			String user = param.getString(KEY_USERNAME);
			String pwd = param.getString(KEY_PASSWORD);

			String jdbcUrl = String.format(FORMAT_MARIADB_JDBC_URL, url, port, name);
			conn = DriverManager.getConnection(jdbcUrl, user, pwd);
		}
		return conn;
	}
}

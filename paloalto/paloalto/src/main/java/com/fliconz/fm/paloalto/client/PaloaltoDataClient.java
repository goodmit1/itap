package com.fliconz.fm.paloalto.client;

import java.util.List;

import org.apache.log4j.LogManager;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.paloalto.core.PaloaltoAPIHelper;
import com.fliconz.fw.runtime.util.PropertyManager;

public class PaloaltoDataClient {

	protected static final String FIREWALL_IP = PropertyManager.getString("paloalto.firewall.ip", "10.230.253.28");
	// protected static final String API_URL = "http://localhost:8080/test/test.xml";
	protected static final String API_URL = "https://" + FIREWALL_IP + "/api/";

	private String url;
	private String apiKey;

	private org.apache.log4j.Logger log = LogManager.getLogger(this.getClass());

	public PaloaltoDataClient(String url, String userId, String pwd) throws Exception {
		log.info("------------------- PaloaltoFWClient.Constructor ------------------- ");
		log.info("url=" + url);
		log.info("userId=" + userId);
		log.info("pwd=" + pwd);
		log.info("------------------------------------------------ ");
		this.url = url;

		if(this.url != null && !"".equals(this.url) && !this.url.endsWith("/api/")){
			this.url += "/api/";
		}

		this.init(userId, pwd);
	}

	/**
	 * 초기화: 지정된 url과 userId, pwd를 이용하여 해당 서버에서 apiKey를 발급받은 후, 현재 clorvirSM에서
	 * 요청된 VM에 대하여 룰이 없으면 생성해준다.
	 *
	 * @throws Exception
	 */
	private void init(String userId, String pwd) throws Exception {
		this.apiKey = PaloaltoAPIHelper.getAPIKey(url, userId, pwd);

		log.info("------------------- PaloaltoDataClient.init ------------------- ");
		log.info("apiKey=" + apiKey);
		log.info("------------------------------------------------ ");
	}

	public List<DataMap<String, Object>> getSecurityRuleList() throws Exception {
		return getSecurityRuleList(null);
	}
	
	public List<DataMap<String, Object>> getSecurityRuleList(String rulePrefix) throws Exception {
		return getSecurityRuleList(rulePrefix, 0);
	}

	public List<DataMap<String, Object>> getSecurityRuleList(String rulePrefix, int loadCnt) throws Exception {
		return PaloaltoAPIHelper.getSecurityRuleList(url, apiKey, rulePrefix, loadCnt);
	}
}
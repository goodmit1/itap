package com.fliconz.fm.paloalto.core;

public class PaloaltoAPIException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	String url;
	String apiKey;
	String type;
	String action;
	String xpath;
	String element;
	String msg;
	
	public PaloaltoAPIException(String msg){
		super(msg);
	}
	
	public PaloaltoAPIException(String url, String apiKey, String type, String action, String xpath, String element, String msg){
		super(msg);
		this.url = url;
		this.apiKey = apiKey;
		this.type = type;
		this.action = action;
		this.xpath = xpath;
		this.element = element;
		this.msg = msg;
	}

	public PaloaltoAPIException(String url, String apiKey, String msg){
		super(msg);
		this.url = url;
		this.apiKey = apiKey;
		this.msg = msg;
	}
	
	@Override
	public String getMessage() {
		StringBuffer sb = new StringBuffer();
		if(type != null || action != null || xpath != null || element != null) sb.append("==================== REQUEST ================").append("\n");
		if(type != null) sb.append("param[type]: " + type).append("\n");
		if(action != null) sb.append("param[action]: " + action).append("\n");
		if(xpath != null) sb.append("param[xpath]: " + xpath).append("\n");
		if(element != null) sb.append("param[element]: " + element).append("\n");
		sb.append("=============================================").append("\n");
		sb.append("==================== MSG ===============").append("\n");
		sb.append(msg).append("\n");
		sb.append("=============================================").append("\n");
		return sb.toString();
	}
}
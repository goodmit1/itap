package com.clovirsm.cm.sys.git;

import org.json.JSONException;
import org.json.JSONObject;

public interface   IGitAPI {
	public void setUrl(String url);
	public void setToken(String token);
	public boolean movePrjToGroup(String group, String prjId) throws Exception;
	public JSONObject getPrj(String group, String prjId) throws Exception;
	public String createUser(String id , String email, String pwd) throws Exception;
	public void createRepository(String group, String prjId) throws Exception;
	public String getGitURL(String group, String prjId, String usertoken);
	 
	public void createDirectory(String group, String prjId, String dir) throws Exception;
	public   void newFile(String group,  String id, String branch, String fileName,  String content, String commitMsg) throws Exception;
	public   void saveFile(String group,  String id, String branch, String fileName,  String content, String commitMsg) throws Exception;
	public void setUserId(String string);
	public void setRepo(String string);
	public String getUserId() ;
	public String getRepo();
	public String getToken();
	public void addPrjMember(String group, String prjId, String userId, String level) throws Exception;
	public void deletePipeline(String group, String name, String pipeline) throws Exception;
	public void deleteFile(String group,  String repoName, String branch, String fileName, String commitMsg) throws Exception;
	public void deletePrj(String group, String name) throws Exception;
	public boolean existFile(String gitGroup, String repo,   String fileName);
	public String getJobLog(String group,  String repoName,  String jobId) throws Exception;
	public void deleteFiles(String src_group, String src_repoName, String path, String commitMsg) throws JSONException, Exception;
	public String makeUserToken(String userId) throws Exception;
}

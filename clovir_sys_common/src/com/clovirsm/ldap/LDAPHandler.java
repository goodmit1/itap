package com.clovirsm.ldap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class LDAPHandler {


	String[] returnAttributes = {"sAMAccountName", "ou", "cn", "mail"};
	//String baseFilter = "(&((&(objectCategory=Person)(objectClass=User)))";
	String baseFilter = "(&((&(ou=People)))";
	javax.naming.directory.DirContext dirContext;

	SearchControls searchCtls;
	String domainBase;
	String domainController;

	public LDAPHandler() {
	}

	public LDAPHandler(String url, String username, String password) throws Exception {
		init(url, username, password);
	}

	public LDAPHandler(String url, String domainBase, String username, String password) throws Exception {

		init(url, username, password);
		this.domainBase = domainBase;
	}

	public void close() {
		if (dirContext != null) {
			try {
				dirContext.close();
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean addUser(Map<String, Object> param) throws Exception {
		String id = (String) param.get("sAMAccountName");
		Attributes entry = new BasicAttributes();

		if (searchById("username", id) == null) {
			Attribute objClasses = new BasicAttribute("objectClass");
			objClasses.add("top");
			objClasses.add("person");
			objClasses.add("organizationalPerson");
			objClasses.add("user");
			entry.put(objClasses);
			for (String key : param.keySet()) {
				if (param.get(key) instanceof String[]) {
					Attribute a = new BasicAttribute(key);
					for (String av : (String[]) param.get(key)) {
						a.add(av);
					}
					entry.put(a);
				} else {
					Attribute a = new BasicAttribute(key, param.get(key));
					entry.put(a);
				}

			}
			Attribute userAccountControl = new BasicAttribute("userAccountControl", "544");
			entry.put(userAccountControl);
			String entryDN = "CN=" + param.get("cn") + ",OU=" + param.get("ou") + "," + domainBase;
			System.out.println("entryDN :" + entryDN);
			dirContext.createSubcontext(entryDN, entry);
			return true;
		} else {
			return false;
		}
	}

	public boolean addFMUser(Map param) throws Exception {
		Map newParam = new HashMap();
		newParam.put("cn", param.get("USER_NAME"));
		newParam.put("sAMAccountName", param.get("LOGIN_ID"));
		newParam.put("mail", param.get("EMAIL"));
		newParam.put("userPrincipalName", param.get("LOGIN_ID") + "@" + this.domainController);
		newParam.put("ou", param.get("ou"));
		return addUser(newParam);
	}

	public Map searchById(String field, String val) throws Exception {
		//String filter = getFilter(baseFilter, id, "username");
		String filter = "(" + field + "=" + val + ")";
		//CN=Tom Healy,OU=Helpdesk: null:null:{mail=mail: tom.healy@gmobis.com, givenname=givenName: Tom, samaccountname=sAMAccountName: 7401188, cn=cn: Tom Healy}
		NamingEnumeration<SearchResult> searchResult = dirContext.search(domainBase, filter, searchCtls);

		while (searchResult.hasMore()) {
			SearchResult o = searchResult.next();
			Map result = new HashMap();
			result.put("USER_NAME", o.getAttributes().get("cn"));
			result.put("TEAM_NM", o.getAttributes().get("OU"));
			result.put("EMAIL", o.getAttributes().get("mail"));
			return result;
		}
		return null;
	}

	public NamingEnumeration<SearchResult> searchResultNamingEnumeration(String domainBase, String field, String val) throws Exception {
		this.domainBase = domainBase;
		String filter = "(" + field + "=" + val + ")";
		return dirContext.search(this.domainBase, filter, searchCtls);
	}

	protected LDAPHandler init(String uri, String username, String password) throws Exception {

		setProperties(uri, username, password);
		searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		searchCtls.setReturningAttributes(returnAttributes);

		return this;

	}


	public LDAPHandler init(String uri, String username, String password, String[] attrs) throws Exception {

		setProperties(uri, username, password);
		searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		searchCtls.setReturningAttributes(attrs);

		return this;

	}

	private void setProperties(String uri, String username, String password) throws NamingException {
		int pos = uri.indexOf("//");
		domainController = uri.substring(pos + 2);
		java.util.Properties properties = new java.util.Properties();

		properties.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		properties.put(Context.PROVIDER_URL, "LDAP://" + domainController);
		properties.put(Context.SECURITY_PRINCIPAL, username);
		properties.put(Context.SECURITY_CREDENTIALS, password);
		properties.put(Context.REFERRAL, "follow");


		properties.put(Context.SECURITY_AUTHENTICATION, "simple");

		properties.put("com.sun.jndi.ldap.connect.pool", "false");
		properties.put("java.naming.ldap.attributes.binary", "objectSID");

		properties.put(Context.REFERRAL, "follow");
		properties.put("com.sun.jndi.ldap.read.timeout", "-1");
		//initializing active directory LDAP connection

		dirContext = new javax.naming.directory.InitialDirContext(properties);
	}


	public SearchControls setSearchCtls (String[] attrs) {
		SearchControls controls = new SearchControls();
		controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		controls.setReturningAttributes(attrs);
		return controls;
	}




	String getFilter(String baseFilter, String searchValue, String searchBy) {
		String filter = baseFilter;
		if (searchBy.equals("email")) {
			filter += "(mail=" + searchValue + "))";
		} else if (searchBy.equals("username")) {
			filter += "(samaccountname=" + searchValue + "))";
		} else {
			filter += "(" + searchBy + "=" + searchValue + "))";
		}
		return filter;
	}

	String getDomainBase(String base) {
		base = base.substring(base.indexOf("/") + 1);
		return base;
	}

}

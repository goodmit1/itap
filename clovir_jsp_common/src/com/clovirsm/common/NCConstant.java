package com.clovirsm.common;



public class NCConstant {

	/**
	 * @author Seongwook
	 * CUD 코드
	 * C: 생성
	 * U: 수정
	 * D: 삭제
	 */
	public enum CUD_CD {C,U,D}

	/**
	 * @author Seongwook
	 * 서버 상태 코드
	 * W: 생성중
	 * F: 생성 실패
	 * R: 시작
	 * S: 중지
	 * C: 요청중
	 */
	public enum RUN_CD {W,R,F,S,C}


	/**
	 * @author Seongwook
	 * 결재상태 코드
	 * A: 승인
	 * C: 취소
	 * D: 거부
	 * R: 결재의뢰
	 * W: 작업중
	 */
	public enum APP_KIND {A, C, D, R, W}

	/**
	 * @author Seongwook
	 * 사업자 구분 코드
	 * C: 개인
	 * I: 법인
	 * N: 비영리/공공기관
	 */
	public enum BIZ_KIND {C, I, N}

	/**
	 * @author Seongwook
	 * 요금제 코드
	 * D: 일간
	 * H: 시간
	 * M: 월간
	 */
	public enum FEE {D, H, M}

	/**
	 * @author Seongwook
	 * hypervisor 코드
	 * V: VMWare
	 */
	public enum HYPERVISOR {V}

	/**
	 * @author Seongwook
	 * 서비스 종류 코드
	 * D: 디스크
	 * F: 방화벽
	 * G: 이미지
	 * I: IP
	 * S: 서버
	 * B: 외부망
	 * N: NAS
	 * A: Container Application
	 * O: Storage 
	 * C: VRA Catalog
	 * E: 기간 연장
	 * T: snapshot
	 * R: snapshot revert
	 * V: 서버 복구
	 * J: cronJob
	 * P: cm project
	 */
	public enum SVC {D, F, G, I, S, B, N, A, C, E, T, R, V, O, J, P}

	/**
	 * @author Seongwook
	 * 서비스 종류 코드
	 * D: 디스크
	 * R: 복구
	 * G: 이미지
	 * N: 네트웍 연경
	 * I: 공인 아이피
	 * F: 방화벽
	 * T: 스냅샷
	 * S: 서버
	 * A: NAT
	 * C: VRA Catalong
	 */
	public enum TASK_CD {D, R, G, N, S, I, F, T , A, C }


	/**
	 * @author Seongwook
	 * 결제 구분 코드
	 * A: 계좌이체
	 * C: 신용카드
	 */
	public enum PAY_KIND {A, C}

	/**
	 * @author Seongwook
	 * 프로토콜 코드
	 */
	public enum PROTOCOL {ICAMP, TCP, UDP}


	/**
	 *
	 * @author 윤경
	 * W:반영전(진행중)
	 * S:완료
	 * F:실패
	 * D:거부
	 */
	public enum TASK_STATUS_CD  {W, S, F, D}

}

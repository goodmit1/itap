package com.clovirsm.common;

import com.fliconz.fm.mvc.util.MessageException;

public class NCException extends MessageException{
	public static final String DUPLICATE = "err.DUPLICATE";
	public static final String DUPLICATE_NAME = "err.DUPLICATE_NAME";
	public static final String INVALID_OWNER_STATUS = "err.INVALID_OWNER_STATUS";
	public static String NOT_FOUND = "err.NOT_FOUND";
	public static String INVALID_OWNER = "err.INVALID_OWNER";
	public static String NOT_SELECT = "err.NOT_SELECT";
	public static String LACK_OF_SIZE = "err.LACK_OF_SIZE";
	public static String SNAPSHOT_NOT_DEL = "err.SNAPSHOT_NOT_DEL";
	public NCException(String... msgCode) {
		super(msgCode);

	}
 

}

package com.clovirsm.service.popup;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.mvc.DefaultService;

@Service
public class CategorySearchPopupService extends DefaultService {


	protected String getNameSpace() {
		return "com.clovirsm.popup.Category";
	}

	@Override
	public String[] getPks() {
		return new String[]{"CATEGORY_ID"};
	}

	@Override
	protected String getTableName() {
		return "NC_IMG_CATEGORY";
	}

	@Override
	public int delete(Map param) throws Exception {
		if(param.get("DELETE_LIST") == null)
		{
			List<Map> list = this.selectListByQueryKey("list_NC_IMG_CATEGORY_byParent", param);
			String[] delList = new String[list.size()];
			int idx = 0;
			for(Map m:list)	{
				delList[idx++] = (String)m.get("CATEGORY_ID");			
			}
			
			 
			param.put("DELETE_LIST", delList);
		}
		try {
			updateByQueryKey("update_NC_OS_TYPE", param);
		}catch(Exception ignore) {
			
		}
		return super.delete(param);
	}

	@Override
	public int insert(Map param) throws Exception {
		 
		int row =  super.insert(param);
		onAfterInsertUpdate(param);
		return row;
	}
	protected void onAfterInsertUpdate(Map param) throws Exception
	{
		
	}
	 
	@Override
	public int update(Map param) throws Exception {
		int row =  super.update(param);
		onAfterInsertUpdate(param);
		return row;
	}

	public Map chkParentCategory(Object id) throws Exception {
		Map param = new HashMap();
		param.put("CATEGORY_ID", id);
		
		Map info = this.getInfo(param);
		if(info != null && !TextHelper.isEmpty((String)info.get("CATEGORY_CODE")))
		{
			return info;
		}
		return null;
	}
	
	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("PURPOSE", "PURPOSE");
		config.put("KUBUN", "KUBUN");
		return config;
	}

	
}
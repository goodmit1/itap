package com.clovirsm.service.popup;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fliconz.fm.mvc.DefaultService;

@Service
public class UserSearchPopupService extends DefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.popup.UserSearch";
	}

	@Override
	public String[] getPks() {
		return new String[]{"USER_ID"};
	}

	@Override
	protected String getTableName() {
		return "FM_USER";
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		return config;
	}

}
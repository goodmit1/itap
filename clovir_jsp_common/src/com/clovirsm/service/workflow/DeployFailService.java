package com.clovirsm.service.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;
import com.clovirsm.common.NCReqService;
import com.clovirsm.common.NCScheduleFactoryBean;
import com.clovirsm.service.ScheduleJspService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Service
public class DeployFailService extends NCDefaultService {
		 
	@Autowired NextApproverService nextApproverService;
	@Autowired ScheduleJspService scheduleJspService;
	/**
	 * 
	 * @param svcCd
	 * @param cudCd
	 * @param svcId
	 * @param insDt
	 * @param msg
	 * @return true: first fail, false: not first
	 * @throws Exception
	 */
		public boolean insert(String svcCd, String cudCd, String svcId, String insDt, String msg) throws Exception{
			Map param = new HashMap();
			param.put("TASK_STATUS_CD", "F");
			
			param.put("FAIL_MSG", msg);
			
			param.put("SVC_ID",svcId);
			param.put("SVC_CD",svcCd);
			param.put("CUD_CD",cudCd);
			param.put("INS_DT",insDt);
			
			int row =  this.update(param);
			if(row==0) {
				this.insert(param);
				return true;
			}
			else {
				return false;
			}
		}
		protected String getNameSpace() {
			return "com.clovirsm.workflow.deployFail";
		}
		protected Map putMailContent(Map param, NCReqService service,String svcCd, String svcId,   String insDt) throws Exception {
		 
			Map<String, Map> info =  nextApproverService.getNextApproverAPI().getDetailList(svcCd, svcId, insDt);
		 
			param.put("CMT", nextApproverService.getNextApproverAPI().getDetailMailContent(null, info));
			return (Map)((List)info.get(svcCd + "_list")).get(0);
		}
		
		/**
		 * 占쏙옙占쏙옙占�
		 * @param svcCd
		 * @param svcId
		 * @param insDt
		 * @param cudCd
		 * @throws Exception
		 */
		public void update_redeploy(String svcCd, String svcId, String insDt, String cudCd) throws Exception{
			
			NCReqService service = NCReqService.getService(svcCd);
			Map param = new HashMap();
			param.put("SVC_ID",svcId);
			param.put("SVC_CD",svcCd);
			param.put("INS_DT",insDt);
			Map info = putMailContent(param, service, svcCd, svcId,   insDt);
			 
			boolean isOK = service.updateReDeploy(svcId, insDt, cudCd);
			if(isOK ) {
				
				updateSuccess(svcCd,svcId,insDt);
				param.remove("FAIL_MSG");
				sendMail(info.get("INS_ID"), "mail_title_deploy", "deploy", param);
			}

		}
		public void updateSuccess(String svcCd, String svcId, String insDt) throws Exception{
			Map param = new HashMap();
			param.put("SVC_ID",svcId);
			param.put("SVC_CD",svcCd);
			param.put("INS_DT",insDt);
			param.put("TASK_STATUS_CD", "S");
			 
			param.put("FAIL_MSG", "");
		 
		
			this.update(param);
		}
		@Override
		protected Map<String, String> getCodeConfig() {
			Map config = new HashMap();
			config.put("TASK_STATUS_CD", "TASK_STATUS_CD");
			//config.put("APPR_STATUS_CD", "APP_KIND");
			//config.put("RUN_CD", "RUN_CD");
			config.put("SVCCUD", "SVCCUD");
			//config.put("CUD_CD", "CUD");
			config.put("P_KUBUN", "P_KUBUN");
			//config.put("PURPOSE", "PURPOSE");
			return config;
		}
		@Override
		protected String getTableName() {
			 
			return "NC_DEPLOY_FAIL";
		}

		@Override
		public String[] getPks() {
			 
			return new String[] {"SVC_CD", "SVC_ID", "INS_DT"};
		}
		
		public void updateReserve(Map param) throws Exception{
			int row = this.update(param);
			scheduleJspService.updateReloadChk();
			if(row >0  && "Y".equals(param.get("MAIL_SEND_YN"))) {
				NCReqService service = NCReqService.getService((String)param.get("SVC_CD"));
				putMailContent(param, service, (String)param.get("SVC_CD"),  (String)param.get("SVC_ID"),    (String)param.get("INS_DT"));
				super.sendMail((String)param.get("INS_ID"), "title_reserve_deploy", "deploy_guide", param);
			}
		}
		
		@Override
		public int delete(Map param) throws Exception {
			
			NCReqService service = NCReqService.getService((String)param.get("SVC_CD"));
			service.delete4DeployFail(param);
			
			param.put("TASK_STATUS_CD","D"); //占쏙옙占쏙옙
			int row = this.update(param);
			
			if(row >0 ) {
				putMailContent(param, service, (String)param.get("SVC_CD"),  (String)param.get("SVC_ID"),    (String)param.get("INS_DT"));
				super.sendMail((String)param.get("INS_ID"), "title_delete_deploy", "deploy_guide", param);
			}
			 
			return row;
		}
}

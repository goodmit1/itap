package com.clovirsm.service.workflow;

import java.util.ArrayList;
import java.util.List;

public class StepInfo {
	public static final String PARAM_STEP = "STEP";
	public StepInfo(int stepIdx)
	{
		this.stepIdx = stepIdx;
	}
	public boolean isEnd()
	{
		return approverList == null;
	}
	public void addApprover(Object id)
	{
		if(approverList == null)
		{
			approverList = new ArrayList();
		}
		approverList.add(id);
	}
	public void addApprover(Object[] ids)
	{
		if(approverList == null)
		{
			approverList = new ArrayList();
		}
		for(Object id:ids)
		{	
			approverList.add(id);
		}
	}
	int stepIdx;
	String stepName;
	
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	public int getStepIdx() {
		return stepIdx;
	}
	public void setStepIdx(int stepIdx) {
		this.stepIdx = stepIdx;
	}
	public List getApproverList() {
		return approverList;
	}
	public void setApproverList(List approverList) {
		this.approverList = approverList;
	}
	List approverList;
}

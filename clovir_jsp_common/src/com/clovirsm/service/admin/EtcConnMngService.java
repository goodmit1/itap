package com.clovirsm.service.admin;


import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.cm.sys.git.IGitAPI;
import com.clovirsm.common.NCDefaultService;
import com.clovirsm.hv.DBPoolAPI;
 
import com.clovirsm.hv.DBConnection;
import com.clovirsm.service.ComponentService;
import com.fliconz.fm.common.CodeHandler;
import com.fliconz.fw.runtime.util.PropertyManager;

import freemarker.template.utility.StringUtil;

@Service
public class EtcConnMngService extends NCDefaultService {

	@Override
	public int update(Map param) throws Exception {
		 
		int row  = super.update(param);
		
		return row;
	}

	protected String getNameSpace() {
		return "com.clovirsm.admin.etcConnMng";
	}

	@Override
	protected String getTableName() {
		return "NC_ETC_CONN";
	}

	@Override
	public String[] getPks() {
		return new String[]{"CONN_ID"};
	}
	
	
	public Map getConnInfo(String connType) throws Exception{
		Map param = new HashMap();
		param.put("CONN_TYPE", connType);
		return this.selectOneByQueryKey("get_NC_ETC_CONN_includePWD", param);
	}
	 
	public DBConnection getDBConn(String connId) throws Exception{
		Map param = new HashMap();
		param.put("CONN_ID", connId);
		Map info =  this.selectOneByQueryKey("get_NC_ETC_CONN_includePWD", param);
		return getDBConn(info);
		 
	}
	@Autowired ComponentService componentService;
	public DBConnection getDBConn(Map info) throws Exception{
		DBPoolAPI api = new DBPoolAPI();
		String type = DBConnection.getDBType((String)info.get("CONN_URL"));
		if(info.get("CONN_PROP") != null){
			info.put("CONN_URL", info.get("CONN_URL") + "?" + info.get("CONN_PROP")) ;
		}
		info.put("driverClassName", componentService.getDDExt("DB_TYPE", type));
		
		return (DBConnection)api.connect(info) ;
	}
	public IGitAPI getGitApi() throws Exception {
		String gitApiClass = PropertyManager.getString("clovircm.git.class");
		Map info = getConnInfo("GIT");
		if(info == null) return null;
		IGitAPI git = (IGitAPI)Class.forName(gitApiClass).newInstance();
		git.setUrl((String)info.get("CONN_URL"));
        git.setToken((String)info.get("CONN_PWD"));
        git.setUserId( (String)info.get("CONN_USERID"));
        git.setRepo((String)info.get("CONN_PROP"));
        			
		return git;
	}
	@Override
	public int insertOrUpdate(Map param) throws Exception{
		if(param.get("CONN_PROP") != null) {
			param.put("CONN_PROP", StringUtil.replace((String)param.get("CONN_PROP"),"&amp;", "&"));
				
		}
		if(param.get("CONN_ID") == null){
			 param.put("CONN_ID", Long.valueOf(System.nanoTime()));
		}
		else {
	    	  clearDBPool(param);
	      }
	    return super.insertOrUpdate(param);
	}
	
	public void clearDBPool(Map param1) throws Exception{
		 Map param = new HashMap();
		 param.putAll(param1);
		 String type = (String)param.get("CONN_TYPE");
		 if(type.indexOf("DB")>=0) {
			 DBPoolAPI api =   new DBPoolAPI();
			 if(param.get("CONN_PROP") != null){
				 param.put("CONN_URL", param.get("CONN_URL") + "?" + param.get("CONN_PROP")) ;
				}
			 api.clearPool(param);
		 }
	}
	@Override
	public int insertOrUpdateMulti(List<Map> params) throws Exception{
		 for (Map param : params) {
		      if ("I".equals(param.get("IU"))) {
		    	  param.put("CONN_ID", Long.valueOf(System.nanoTime()));
		      }
		      else {
		    	  clearDBPool(param);
		      }
		 }
		 return super.insertOrUpdateMulti(params);
	}
}
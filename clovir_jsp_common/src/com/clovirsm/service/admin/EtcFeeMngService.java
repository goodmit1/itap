package com.clovirsm.service.admin;
 

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.clovirsm.common.ClassHelper;
import com.clovirsm.common.NCDefaultService;
import com.fliconz.fw.runtime.util.ListUtil;

@Service
public class EtcFeeMngService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.admin.etcfee.EtcFeeMng";
	}

	@Override
	protected String getTableName() {
		 
		return "NC_ETC_FEE";
	}

	@Override
	public String[] getPks() {
		return new String[]{"SVC_CD"};
	}
	
	public List<Map> getEtcFeeDetail(Map param) throws Exception
	{
		Map cntInfra = this.selectOneByQueryKey("list_INFRA_COUNT", param);
		List<Map> detail = this.selectListByQueryKey("list_ETC_FEE_DETAIL", param);
		//List<Map> detailTemp = this.selectListByQueryKey("list_ETC_FEE_DETAIL", null);
		List<Map> disk = this.selectListByQueryKey("list_ETC_FEE_DISK", param);
		int cnt = 0;
		if(detail.size() > 0) {
			for(int i = 0 ; i < detail.size() ; i++) {
				if(detail.get(i).get("SVC_CD").equals("C")) {
					detail.get(i).put("HOST_CNT", cntInfra.get("TOTAL_CPU"));
					detail.get(i).put("VM_CNT", cntInfra.get("CPU"));
				} else if(detail.get(i).get("SVC_CD").equals("M")) {
					detail.get(i).put("HOST_CNT", cntInfra.get("TOTAL_MEM"));
					detail.get(i).put("VM_CNT", cntInfra.get("MEM"));
				} else if(detail.get(i).get("SVC_CD").equals("P")) {
					detail.get(i).put("HOST_CNT", cntInfra.get("TOTAL_GPU"));
					detail.get(i).put("VM_CNT", cntInfra.get("GPU"));
				}
				
				for(int z = 0 ; z < disk.size() ; z++) {
					if(detail.get(i).get("SVC_CD").equals(disk.get(z).get("SVC_CD"))) {
						//detail.get(i).put("SVC_NM", disk.get(z).get("SVC_NM"));
						detail.get(i).put("HOST_CNT", disk.get(z).get("HOST_CNT"));
						detail.get(i).put("VM_CNT", disk.get(z).get("VM_CNT"));
						detail.get(i).put("DC_ID" , disk.get(z).get("DC_ID"));
						cnt++;
					} 
				}
				
				 
			}
		}  
		return detail;
	}
	
	public void etcDetailSaveMulti(Map param) throws Exception {
		 String jsonArrStr = (String)param.get("selected_json");
	        if ((jsonArrStr == null) || ("".equals(jsonArrStr))) {
	          throw new Exception("NO DATA");
	        }
	        this.insertOrUpdateMulti(ListUtil.makeJson2List(jsonArrStr));
		
	}
	
	@Override
	public int insertOrUpdateMulti(List<Map> params)
		    throws Exception
		  {
		    int row = 0;
		    for (Map param : params) {
		    	int update = 0;
		    	update = update(param);
		        row += update;
		        if(update == 0) {
		        	row += insert(param);
		        }
	      }
		    ClassHelper.getSite().onAfterCollect(this.getTableName(), null);
		    return row;
		  }
	
}
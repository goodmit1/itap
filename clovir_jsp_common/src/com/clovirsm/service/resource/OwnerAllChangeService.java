package com.clovirsm.service.resource;

import java.util.Iterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCConstant;
import com.fliconz.fm.common.CodeHandler;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.mvc.DefaultService;


@Service
public class OwnerAllChangeService extends DefaultService {

	@Autowired CodeHandler codeHandler;

	protected String getNameSpace() {
		return "com.clovirsm.workflow.ownerallchange.OwnerAllChange";
	}


	public void updateOwnerAllChange(Map param) throws Exception {
		String[] tableNms = null;
		if(TextHelper.isEmpty((String)param.get("SVC_CD"))){
			tableNms = new String[]{ "NC_VRA_CATALOGREQ", "NC_DISK", "NC_FW", "NC_IMG", "NC_VM"};
		} else {
			Map<String, Object> codMap = codeHandler.getCodeList("SVC", getLang());
			Iterator<String> it = codMap.keySet().iterator();
			while(it.hasNext()){
				String key = it.next();
				if(key.equals(param.get("SVC_CD"))){
					//NAT
					if(NCConstant.SVC.A.toString().equals(key)){
						tableNms = new String[]{"NC_NAT"};
					}
					//상품구매
					else if(NCConstant.SVC.B.toString().equals(key)){
						tableNms = new String[]{"NC_BUY"};
					}
					//VRA 카탈로그
					else if(NCConstant.SVC.C.toString().equals(key)){
						tableNms = new String[]{"NC_VRA_CATALOGREQ"};
					}
					//디스크
					else if(NCConstant.SVC.D.toString().equals(key)){
						tableNms = new String[]{"NC_DISK"};
					}
					//방화벽
					else if(NCConstant.SVC.F.toString().toString().equals(key)){
						tableNms = new String[]{"NC_FW"};
					}
					//템플릿
					else if(NCConstant.SVC.G.toString().equals(key)){
						tableNms = new String[]{"NC_IMG"};
					}
					//IP
					else if(NCConstant.SVC.I.toString().equals(key)){
						tableNms = new String[]{"NC_PUBLIC_IP"};
					}
					//네트워크 연결
					else if(NCConstant.SVC.N.toString().equals(key)){
						tableNms = new String[]{"NC_COMP_NW_CON"};
					}
					//서버(VM)
					else if(NCConstant.SVC.S.toString().equals(key)){
						tableNms = new String[]{"NC_VM"};
					}
					break;
				}
			}
		}
		for (String tableNm : tableNms) {
			param.put("TABLE_NM", tableNm);
			this.updateByQueryKey("updateOwnerChange", param);

			param.put("TABLE_NM", tableNm + "_REQ");
			this.updateByQueryKey("updateOwnerChangeReq", param);
		}
	}


	@Override
	public String[] getPks() {
		return null;
	}


	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return null;
	}
}
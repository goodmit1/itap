package com.clovirsm.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.context.ContextLoaderListener;

import com.clovirsm.common.NCDefaultService;
import com.clovirsm.common.NCScheduleFactoryBean;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Service
public class ScheduleJspService extends NCDefaultService {

	@Autowired ComponentService compService;
	boolean isFirst = true;
	public void updateReloadChk() throws Exception {
		Map param = new HashMap();
		param.put("RELOAD_YN","Y");
		updateByQueryKey("updateReloadCheck", param);
	}
	
	 
 
	public void chkReload( ) throws Exception {
		if(compService.isScheduleService()) {
			
			NCScheduleFactoryBean bean = getSchedulerBean();
			if(bean == null) return;
			Map param = new HashMap();
			if(isFirst || "Y".equals(selectOneObjectByQueryKey("getReloadCheck", param))){
				bean.setTriggers(selectListByQueryKey("list_RUN_NC_SCHEDULE", new HashMap()));
				param.put("RELOAD_YN","N");
				updateByQueryKey("updateReloadCheck", param);
				 
				isFirst = false;
				 
			}
		}
	
	}
	 public static NCScheduleFactoryBean getSchedulerBean() {
		 ApplicationContext appContext = ContextLoaderListener.getCurrentWebApplicationContext();
		 return appContext.getBean(NCScheduleFactoryBean.class);
	 }
	
	 
	public void chkCron(String cron) throws Exception{
		CronTriggerImpl cti = new CronTriggerImpl();
		cti.setCronExpression(cron);
	}
	
	@Override
	public int insert(Map param) throws Exception {
		chkCron((String)param.get("CRON"));
		int row = super.insert(param);
		deploy();
		return row;
	}

	private void deploy() throws Exception{
		//NCScheduleFactoryBean bean = (NCScheduleFactoryBean)SpringBeanUtil.getBean("NCScheduleFactoryBean");
		updateReloadChk();
	}
	@Override
	public int update(Map param) throws Exception {
		chkCron((String)param.get("CRON"));
		int row =  super.update(param);
		deploy();
		return row;
	}

	@Override
	public int delete(Map param) throws Exception {
		int row =  super.delete(param);
		deploy();
		return row;
	}

	protected String getNameSpace() {
		return "com.clovirsm.Schedule";
	}

	@Override
	public String[] getPks() {
		return new String[]{"SCHEDULE_ID"};
	}

	@Override
	protected String getTableName() {
		return "NC_SCHEDULE";
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map<String, String> conf = new HashMap<String, String>();
		conf.put("LAST_RUN_RESULT", "TASK_STATUS_CD");
		conf.put("ACTION", "SCHEDULE_ACTION");
		return conf;
	}
	
}
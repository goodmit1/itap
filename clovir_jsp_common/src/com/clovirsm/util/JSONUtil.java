package com.clovirsm.util;

import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONUtil {

	public static org.json.simple.JSONObject createJSON() {
		org.json.simple.JSONObject json = new JSONObject();
		return json;
	}

	public static org.json.simple.JSONObject createJSON(String stringify) {
		org.json.simple.JSONObject json = null;
		JSONParser parser = new JSONParser();
		try {
			Object parsedObject = parser.parse(stringify);
			json = (JSONObject)parsedObject;
		} catch(ParseException e){
			try {
				json = (JSONObject)parser.parse(stringify.replaceAll("&quot;", "\""));
			}catch(ParseException e2){
				e2.printStackTrace();
			}
		}
		return json;
	}

	public static org.json.simple.JSONArray createJSONArray(String stringify) {
		org.json.simple.JSONArray jsonArray = null;
		JSONParser parser = new JSONParser();
		try {
			Object parsedObject = parser.parse(stringify);
			jsonArray = (JSONArray)parsedObject;
		} catch(ParseException e){
			try {
				jsonArray = (JSONArray)parser.parse(stringify.replaceAll("&quot;", "\""));
			}catch(ParseException e2){
				e2.printStackTrace();
			}
		}
		return jsonArray;
	}


	public static org.json.simple.JSONObject createJSON(Map data) {
		org.json.simple.JSONObject json = new JSONObject();
		if(data != null && !data.isEmpty()){
			json.put("RESULT", "ok");
			json.putAll(data);
		} else {
			json.put("RESULT", "no_result");
			json.put("MSG", "no result");
		}
		return json;
	}

	public static void main(String[] args) throws Exception {
		System.out.println(JSONUtil.createJSON("{ &quot;test&quot;: &quot;aaa&quot; }").toJSONString());
	}
}

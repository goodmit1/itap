package com.clovirsm.sys.hv.after;

import java.util.Map;

import com.clovirsm.service.TaskService;
 

public abstract class OnAfterCreateThread extends OnAfterCreate implements Runnable{

	int term = 60000;
	String taskId;
	boolean alreadyStarted = false; 
	public OnAfterCreateThread(  Map param ) {
		super( param);
		this.taskId = this.getSvcId();
	}
	
	
	public void setAlreadyStarted(boolean isStarted)
	{
		this.alreadyStarted = isStarted;
	}
	public void setTerm(int term)
	{
		this.term = term;
	}
	protected abstract boolean getState( ) throws Exception;
	public void run()
	{
		
		try {
			if(!alreadyStarted) this.startTask(taskId);
			while(true)
			{
				
					Thread.sleep(this.term);
					boolean result = getState();
					if(result )
					{
						this.onAfterSuccess(this.taskId);
						break;
					}
					 
				
			}
		} catch ( Exception e) {
			try {
				this.onAfterFail(taskId , e);
			} catch (Exception e1) {
			 
				e1.printStackTrace();
			}
		}
		
	}

}

package com.clovirsm.sys.hv.after;

import java.util.HashMap;
import java.util.Map;

import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.service.TaskService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
 

public abstract class OnAfterCreate  implements IAfterProcess
{
	protected TaskService service;
	protected Map param ;
	protected String taskId;
	protected String svcCd;
	public void setSvcCd(String svcCd) {
		this.svcCd = svcCd;
	}
	public Map getParam()
	{
		return param;
	}
	public void setParam(Map param)
	{
		this.param = param;
	}
	public OnAfterCreate(  Map param, String svcCd) {
		this(param);
		this.setSvcCd(svcCd);
	}
	public OnAfterCreate(  Map param)
	{
		this.service =(TaskService) SpringBeanUtil.getBean("taskService");
		this.param = param;
	}
	public void setTask(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public void startTask(String taskId) {
		this.taskId = taskId;
		service.updateStartTask(taskId, getSvcCd(), getSvcId(), getTableNm(), param);
		onAfterStart(taskId, param);
	}
	
	protected abstract void onAfterStart(String taskId, Map param)  ;
	public  void chkState( )  
	{
		 
		try {
			if(getState())
			{
				try {
					
					this.onAfterSuccess(taskId);
				
				} catch (Exception e) {
					 
					try {
						e.printStackTrace();
						this.onAfterFail(taskId, e);
					} catch (Exception e1) {
						 
						e1.printStackTrace();
					}
				}
				service.finishTask(taskId);
			}
		} catch (Exception e) {
			try {
				e.printStackTrace();
				this.onAfterFail(taskId, e);
				service.finishTask(taskId);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		
			
		 
	}
	protected abstract boolean getState()  throws Exception;
	protected   String getSvcCd() {
		return svcCd;
	}
 
	protected String getSvcId() {
	 
		return taskId;
	}
	

	protected   String getTableNm() {
		return "";
	}
}

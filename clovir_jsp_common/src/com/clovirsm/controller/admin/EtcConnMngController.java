package com.clovirsm.controller.admin;

import com.clovirsm.service.admin.EtcConnMngService;
import com.fliconz.fm.mvc.DefaultController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value =  "/etcConnMng" )
public class EtcConnMngController extends DefaultController {

	@Autowired EtcConnMngService service;
	@Override
	protected EtcConnMngService getService() {
		 
		return service;
	}

	
}

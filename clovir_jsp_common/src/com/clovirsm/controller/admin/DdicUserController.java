package com.clovirsm.controller.admin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.DdicUserService;
import com.fliconz.fm.admin.controller.MultilangPopupController;
import com.fliconz.fm.admin.service.MultilangPopupService;
import com.fliconz.fm.admin.service.UserService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.ListUtil;
import com.fliconz.fw.runtime.util.MapUtil;
import com.mysql.jdbc.log.Log;

@RestController
@RequestMapping(value =  "/ddicUser" )
public class DdicUserController extends DefaultController{


	@Autowired
	DdicUserService service;
	@Autowired
	MultilangPopupService mservice;
	
	@Override
	protected DefaultService getService() {
		// TODO Auto-generated method stub
		return service;
	}
	
  @RequestMapping({"/save_multi"})
  public Object saveMulti(HttpServletRequest request, HttpServletResponse response) {
    return  (new ActionRunner()
      {
        protected Object run(Map param) throws Exception
        {
        	String jsonArrStr = (String)param.get("selected_json");
        	if (jsonArrStr == null || "".equals(jsonArrStr)) throw new Exception("NO DATA");
        	System.out.println("json arr: "+jsonArrStr);
        	        	
        	service.insertOrUpdateMulti(ListUtil.makeJson2List(jsonArrStr));
        	if(!UserVO.getUser().getLocale().equals("ko")) {
        		mservice.insertOrUpdateMulti(ListUtil.makeJson2List(jsonArrStr));
        		return ControllerUtil.getSuccessMap(param, mservice.getPks());
        	}
        	return ControllerUtil.getSuccessMap(param, service.getPks());
        }
      }).run(request);
  }
  
  @RequestMapping({"/delete"})
  public Object delete(HttpServletRequest request, HttpServletResponse response) {
    return  (new ActionRunner()
      {
        protected Object run(Map param) throws Exception
        {
          service.delete(param);
          if(!UserVO.getUser().getLocale().equals("ko")) {
        	  	param.put("TABLE_NM", "FM_DDIC");
        	  	param.put("FIELD_NM", "DD_VALUE");
        	  	param.put("LANG_TYPE", UserVO.getUser().getLocale());
	        	mservice.delete(param);
	      		return ControllerUtil.getSuccessMap(param, mservice.getPks());
		}
          return ControllerUtil.getSuccessMap(param, service.getPks());
        }
      }).run(request);
  }
	
		
		
}









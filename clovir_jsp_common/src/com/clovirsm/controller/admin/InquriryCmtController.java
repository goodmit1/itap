package com.clovirsm.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.EtcFeeMngService;
import com.clovirsm.service.admin.InquiryCmtService;
import com.fliconz.fm.mvc.DefaultController;

@RestController
@RequestMapping(value =  "/inquiryCmt" )
public class InquriryCmtController extends DefaultController {

	@Autowired InquiryCmtService service;
	@Override
	protected InquiryCmtService getService() {
		 
		return service;
	}

}

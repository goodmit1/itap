package com.clovirsm.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.QnaAnswerService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.util.ControllerUtil;

@RestController
@RequestMapping(value =  "/qnaAnswer" )
public class QnaAnswerController extends DefaultController{

	@Autowired QnaAnswerService service;
	@Override
	protected QnaAnswerService getService() {
		return service;
	}

	
	@RequestMapping({"/saveAnswer"})
	  public Object saveAnswer(HttpServletRequest request, HttpServletResponse response)
	  {
		return  (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				int result = service.updateByQueryKey("update_FM_INQUIRY_ANSWER", param);
				if(result > 0)
					service.sendMail(param.get("INS_ID"), "mail_qna_title", "qna_answer", param);
				
		        return ControllerUtil.getSuccessMap(param, service.getPks());
			}
		}
		).run(request);

	  }
}

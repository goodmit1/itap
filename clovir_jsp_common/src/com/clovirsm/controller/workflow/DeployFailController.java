package com.clovirsm.controller.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.EtcFeeMngService;
import com.clovirsm.service.workflow.DeployFailService;
import com.clovirsm.service.workflow.NextApproverService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.util.ControllerUtil;

@RestController
@RequestMapping(value =  "/deploy_fail" )
public class DeployFailController extends DefaultController {

	@Autowired DeployFailService service;
	@Autowired NextApproverService nextApprover;
	
	@Override
	protected DeployFailService getService() {
		 
		return service;
	}
	@RequestMapping(value =  "/update_redeploy" )
	public Object redeploy(final HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			service.update_redeploy(request.getParameter("SVC_CD"), request.getParameter("SVC_ID"), request.getParameter("INS_DT"), request.getParameter("CUD_CD"));
			return ControllerUtil.getSuccessMap(new HashMap());
		}
		catch(Exception e) {
			return ControllerUtil.getErrMap(e);
		}
	}
	@RequestMapping(value =  "/chkBeforeDeploy" )
	public Object chkBeforeDeploy(final HttpServletRequest request, HttpServletResponse response) {
		return  (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				List<Map> details = new ArrayList();
				details.add(param);
				String msg = nextApprover.getNextApproverAPI().chkBeforeDeploy(details );;
				param.put("last_chk_msg",msg);
				return ControllerUtil.getSuccessMap(param, "last_chk_msg");
			}
		}).run(request);

	}

	@RequestMapping(value =  "/updateReserve" )
	public Map updateReserve(final HttpServletRequest request, HttpServletResponse response) throws Exception {
				int result = 0;
				Map param = ControllerUtil.getParam(request);
				
				service.updateReserve(param);
				return ControllerUtil.getSuccessMap(param);
	}
	
	 
	 

}

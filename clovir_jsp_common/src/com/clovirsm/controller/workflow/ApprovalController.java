package com.clovirsm.controller.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.clovirsm.service.admin.DdicUserService;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCReqService;
 
import com.clovirsm.service.workflow.ApprovalDetailService;
import com.clovirsm.service.workflow.ApprovalService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.MapUtil;

@RestController
@RequestMapping(value =  "/approval" )
public class ApprovalController extends DefaultController{

	@Autowired ApprovalService service;
	@Autowired DdicUserService ddUserService;
	@Override
	protected DefaultService getService() {
		return service;
	}

	//반려
	@RequestMapping(value =  "/deny" )
	public Object deny(final HttpServletRequest request, HttpServletResponse response) {
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				System.out.println("deny param : "+param);
				param.put("APPR_STATUS_CD", NCConstant.APP_KIND.D.toString());
				Map <String,Object> info = service.getInfo(param);
				info.putAll(param);
				service.updateDeny(info);
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}
		}).run(request);

	}
	@RequestMapping(value =  "/chkBeforeDeploy" )
	public Object chkBeforeDeploy(final HttpServletRequest request, HttpServletResponse response) {
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				param.put("last_chk_msg",service.chkBeforeDeploy(param));
				return ControllerUtil.getSuccessMap(param, "last_chk_msg");
			}
		}).run(request);

	}
	private Set getSelectedDatas(Map param) throws  Exception {
		String jsonArrStr = (String)param.remove("selected_json");
		if(jsonArrStr == null || "".equals(jsonArrStr)) throw new Exception("NO DATA");
		JSONArray arr = new JSONArray(jsonArrStr);
		Set<Map> selRows = new HashSet();
		for(int i=0; i < arr.length(); i++)
		{
			selRows.add( MapUtil.jsonToMap( arr.getJSONObject(i).toString()) );
		}
		return selRows;
	}
	private List getMulitiWork(Map param, boolean isDeny) throws Exception {
		Set<Map> list = getSelectedDatas(param);
		List errorMap = new ArrayList();
		 
		for(Map m : list) {
			try
			{
				m.putAll(param);
				m.putAll(service.getInfo(m));
				if(isDeny) {
					service.updateDeny(m);
				}
				else {
					service.updateApprove(m);
				}
				
			}
			catch(Exception e) {
				e.printStackTrace();
				m.put("ERR_MSG", e.getMessage());
				errorMap.add(m);
			}
		}
		return errorMap;
	}
	
	//반려
	@RequestMapping(value =  "/deny_multi" )
	public Object denyMulti(final HttpServletRequest request, HttpServletResponse response) {
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
			
				param.put("APPR_STATUS_CD", "D");	 
				param.put("err_list", getMulitiWork(param, true));
				return ControllerUtil.getSuccessMap(param);
			}
		}).run(request);

	}
	@RequestMapping(value =  "/approve_multi" )
	public Object approveMulti(final HttpServletRequest request, HttpServletResponse response) {
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
			
				param.put("APPR_STATUS_CD", "A");
				param.put("err_list", getMulitiWork(param, false));
				return ControllerUtil.getSuccessMap(param, "err_list");
			}
		}).run(request);

	}
	//승인
	@RequestMapping(value =  "/approve" )
	public Object approve(final HttpServletRequest request, HttpServletResponse response) {
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				param.putAll(service.getInfo(param));
				service.updateApprove(param);
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}
		}).run(request);

	}

	@Autowired ApprovalDetailService detailService;

	@RequestMapping(value =  "/detail_list" )
	public Object detail_list(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				Map result = new HashMap();
				List list = detailService.list(param);
				result.put("list", list);
				return result;
			}
		}).run(request);

	}
	
	
	
	@RequestMapping(value =  "/appr_list" )
	public Object appr_list(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				Map result = new HashMap();
				List list = detailService.list("list_NC_APPR",param);
				result.put("list", list);
				return result;
			}
		}).run(request);

	}
 

	@RequestMapping(value =  "/detail" )
	public Object detail(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				/**
				 * @author Seongwook
				 * 서비스 종류 코드
				 * F: 방화벽
				 * G: 이미지
				 * S: 서버
				 * C: VRA Catalong
				 */
				NCReqService service = null;
				
				String svc_cd = (String)param.get("SVC_CD");
				service = NCReqService.getService(svc_cd);
				
				param.put(service.getPks()[0], param.get("SVC_ID"));
			
				 
				if(service != null){
					return service.getInfo(param);
				}
				return null;
			}
		}).run(request);

	}

	@RequestMapping(value = "/appr_comment_list")
	public Object appr_comment(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				Map result = new HashMap();
				List list = ddUserService.list("list_FM_DDIC",param);
				result.put("list", list);
				return result;
			}
		}).run(request);
	}

}

package com.clovirsm.controller.workflow;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
 
import com.clovirsm.service.workflow.ExpireService;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@RestController
@RequestMapping(value =  "/expire" )
public class ExpireController extends DefaultController {

	@Autowired ExpireService service;
	@Override
	protected ExpireService getService() {
		return service;
	}

	@RequestMapping(value =  "/vm_delete" )
	public Object vm_delete(final HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			service.delete(request.getParameter("SVC_CD"), request.getParameter("SVC_ID"));
			
			return ControllerUtil.getSuccessMap(new HashMap());
		}
		catch(Exception e) {
			return ControllerUtil.getErrMap(e);
		}
		
	}
	@RequestMapping(value =  "/update_reuse" )
	public Object update(final HttpServletRequest request, HttpServletResponse response) throws Exception {
	 
		try {
			
			Date expireDt = null;
			service.updateReUse(request.getParameter("SVC_CD"), request.getParameter("SVC_ID"), Integer.parseInt(request.getParameter("ADD_USE_MM")), expireDt);
			
			return ControllerUtil.getSuccessMap(new HashMap());
		}
		catch(Exception e) {
			return ControllerUtil.getErrMap(e);
		}
	}
}
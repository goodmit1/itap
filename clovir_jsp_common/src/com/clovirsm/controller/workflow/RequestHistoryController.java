package com.clovirsm.controller.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.common.NCConstant;
import com.clovirsm.service.workflow.RequestHistoryService;
import com.clovirsm.service.workflow.WorkService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;

@RestController
@RequestMapping(value =  "/request_history" )
public class RequestHistoryController extends DefaultController{

	@Autowired RequestHistoryService service;
	@Override
	protected DefaultService getService() {
		return service;
	}



	//결재취소
	@RequestMapping(value =  "/cancel" )
	public Object cancel(final HttpServletRequest request, HttpServletResponse response) {
		return  (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				
				param.put("APPR_COMMENT", null);
				service.updateCancel(param);
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}
		}).run(request);

	}

	//작업함으로 이동
	@RequestMapping(value =  "/rework" )
	public Object rework(final HttpServletRequest request, HttpServletResponse response) {
		return  (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				param.put("APPR_STATUS_CD", NCConstant.APP_KIND.W.toString());
				param.put("APPR_COMMENT", null);
				service.updateRework(param);
				return ControllerUtil.getSuccessMap(param, getService().getPks());
			}
		}).run(request);

	}

}

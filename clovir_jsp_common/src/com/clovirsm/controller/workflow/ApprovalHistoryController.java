package com.clovirsm.controller.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.workflow.ApprovalHistoryService;
import com.clovirsm.service.workflow.RequestHistoryService;
import com.clovirsm.service.workflow.WorkService;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;

@RestController
@RequestMapping(value =  "/approval_history" )
public class ApprovalHistoryController extends DefaultController{

	@Autowired ApprovalHistoryService service;
	@Override
	protected DefaultService getService() {
		return service;
	}

}

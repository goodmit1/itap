package com.clovirsm.controller.resource;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.controller.ReqController;
import com.clovirsm.service.resource.OwnerAllChangeService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.util.ControllerUtil;

@RestController
@RequestMapping(value =  "/all_change_owner" )
public class OwnerAllChangeController extends ReqController{

	@Autowired OwnerAllChangeService service;
	@Override
	protected OwnerAllChangeService getService() {
		return service;
	}

	@RequestMapping(value =  "/chg_owner" )
	public Object changeOwner(HttpServletRequest request) throws Exception
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				service.updateOwnerAllChange(param);
				return ControllerUtil.getSuccessMap(param, getService().getPks());

			}

		}).run(request);
	}
}

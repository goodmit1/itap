package com.clovirsm.service.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCReqService;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.vmware.BeforeAfterVMWare;
import com.fliconz.fw.runtime.util.ArrUtil;


@Service("fwService")
public class FWService extends NCReqService {

	//protected boolean chkDuplicate = false;

	@Autowired DCService dcService;

	/*@Override
	public void chkDuplicate(String tableNm, Map selRow, String titleField) throws Exception
	{
		if(!chkDuplicate){
			return ;
		}
		super.chkDuplicate(tableNm, selRow, titleField);
	}
*/
	protected String getNameSpace() {
		return "com.clovirsm.resources.FW";
	}



	@Override
	protected String getDefaultTitle(Map param) {
		return param.get("CIDR") + "->" + param.get("VM_NM") + ":" + param.get("PORT");
	}

	@Override
	public String getSVC_CD() {
		return NCConstant.SVC.F.toString();
	}

	@Override
	protected boolean onDeploy(String pkVal, String cudCode, Map info) throws Exception {

		Map vmInfo = dcService.getVMInfo( (String)info.get("VM_ID"));
		BeforeAfterVMWare beforeAfter = (BeforeAfterVMWare) dcService.getDC((String)vmInfo.get("DC_ID")).getBefore(dcService);
		beforeAfter.setDcInfo(dcService.getDC((String)vmInfo.get("DC_ID")));
		beforeAfter.onAfterFWDeploy(info);
		return true;
	}


	public String[] getDiffPort(Map newInfo,String[] newPorts , String[] oldPorts ) throws Exception {
		// select * from NC_FW where VM_ID=#{VM_ID}

		String[] diffPort = getDiff(newPorts, oldPorts);


		ArrayList list = new ArrayList();
		for(int i=0; diffPort != null && i < diffPort.length; i++)
		{
			if(!isExistPort(newInfo.get("VM_ID"),newInfo.get("FW_ID"), diffPort[i]))
			{
				list.add(diffPort[i]);
			}
		}
		String[] result = new String[list.size()];
		list.toArray(result);
		return result;
	}

	public static String[] getDiff(String[] oldPorts, String[] newPorts) {
		if(oldPorts==null || oldPorts.length==0) return null;
		if(newPorts==null || newPorts.length==0) return oldPorts;
		List<String> list = new ArrayList();
		for(String oldPort:oldPorts)
		{
			if(ArrUtil.indexOf(newPorts, oldPort)<0)
			{
				list.add(oldPort);
			}
		}
		String[] result = new String[list.size()];
		list.toArray(result);
		return result;
	}
	
	
	public boolean existClientIp(Object vmId, Object excludeFWId, String ip) throws Exception {
		Map param = new HashMap();
		param.put("VM_ID", vmId);
		param.put("FW_ID", excludeFWId);
		param.put("CIDR", ip);
		Object o = this.selectOneObjectByQueryKey("countClientIp", param);
		if(o == null || NumberUtil.getInt(o)==0)
		{
			return false;
		}
		return true;
	}

	public void updateFWDeployResult(String fwId, String result)
	{
		Map param = new HashMap();
		param.put("FW_DEPLOY_RESULT", result);
		param.put("FW_ID", fwId);
		try {
			this.updateByQueryKey("updateFWAutoResult", param);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public boolean existFWUser(Object vmId, Object excludeFWId, String id) throws Exception {
		Map param = new HashMap();
		param.put("VM_ID", vmId);
		param.put("FW_ID", excludeFWId);
		param.put("FW_USER_ID", id);
		Object o = this.selectOneObjectByQueryKey("countFWUser", param);
		if(o == null || NumberUtil.getInt(o)==0)
		{
			return false;
		}
		return true;
	}
	public boolean isExistPort(Object vmId, Object excludeFWId, String port) throws Exception {
		Map param = new HashMap();
		param.put("VM_ID", vmId);
		param.put("FW_ID", excludeFWId);
		param.put("PORT", port);
		Object o = this.selectOneObjectByQueryKey("countPort", param);
		if(o == null || NumberUtil.getInt(o)==0)
		{
			return false;
		}
		// select count(*) from NC_FW where VM_ID=#{VM_ID} and FW_ID != #{FW_ID} and concat(',',concat(PORT,',')) like concat('%,',concat(#{PORT},',%'))
		return true;
	}

	@Override
	protected int updateDBTable(String tableNm, Map param) throws Exception {
		if(tableNm.equals("NC_FW_REQ") && "D".equals(param.get("CUD_CD")) && "F".equals(param.get("TASK_STATUS_CD"))) //실패인데 삭제 요청한 경우 NC_FW set DEL_YN='Y'
		{

			param.put("DEL_YN", "Y");
			return super.updateDBTable("NC_FW", param);
		}
		else
		{
			return super.updateDBTable(tableNm, param);
		}
	}
	@Override
	protected String getTableName() {
		return "NC_FW";
	}
	@Override
	public String[] getPks() {
		return new String[]{"FW_ID"};
	}

	public Map getOldInfo(String fwId, String instDt) throws Exception
	{
		Map param = new HashMap();
		param.put("FW_ID", fwId);
		param.put("INS_DT", instDt);
		return this.selectOneByQueryKey("selectByPrimaryKey_NC_FW_old", param);
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("TASK_STATUS_CD", "TASK_STATUS_CD");
		config.put("APPR_STATUS_CD", "APP_KIND");
		config.put("RUN_CD", "RUN_CD");
		config.put("CUD_CD", "CUD");
		config.put("P_KUBUN", "P_KUBUN");
		config.put("PURPOSE", "PURPOSE");
		config.put("VM_USER_YN", "sys.yn");
		return config;
	}

}
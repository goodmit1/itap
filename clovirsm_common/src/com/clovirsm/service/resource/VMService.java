package com.clovirsm.service.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.IAsyncJob;
import com.clovirsm.common.IDGenHelper;
import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCReqService;
import com.clovirsm.hv.AlreadyExistException;
import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.NotFoundException;
import com.clovirsm.service.admin.FwMngService;
import com.clovirsm.service.admin.UserSearchService;
import com.clovirsm.service.workflow.RequestHistoryService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.executor.CreateNic;
import com.clovirsm.sys.hv.executor.CreateSnapshot;
import com.clovirsm.sys.hv.executor.CreateVM;
import com.clovirsm.sys.hv.executor.DeleteVM;
import com.clovirsm.sys.hv.executor.GetVMInfos;
import com.clovirsm.sys.hv.executor.ReconfigVM;
import com.clovirsm.sys.hv.executor.RenameSnapshot;
import com.clovirsm.sys.hv.executor.RenameVM;
import com.fliconz.fm.mvc.util.MessageException;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.fliconz.fw.runtime.util.NumberUtil;

@Service("VMService")
public class VMService extends NCReqService implements IAsyncJob {

	@Autowired
	transient CreateVM createVM;

	@Autowired
	transient DeleteVM deleteVM;

	@Autowired
	transient ReconfigVM reconfigVM;

	@Autowired
	transient RenameSnapshot renameSnapshot;

	@Autowired
	transient CreateSnapshot createSnapshot;

	@Autowired
	transient FWService fwService;
	@Autowired
	transient FwMngService fwMngService;

	@Autowired
	transient RequestHistoryService reqHistoryService;


	protected String getNameSpace() {
		return "com.clovirsm.resources.vm.VM";
	}



	@Override
	protected String getDefaultTitle(Map param) {
		return (String)param.get("VM_NM");
	}

	public void syncAll() throws Exception{
		Map param = new HashMap();
		List<Map> list = this.selectList("NC_VM_org", param);
		for(Map m:list)
		{
			try
			{
				vmStatus.run(true,(String)m.get("DC_ID"), (String)m.get("COMP_ID"),(String)m.get("TEAM_CD"),  m );
			}
			catch(NotFoundException ignore)
			{
				
			}
		}
	}
	public void insertVMfromDC(Map param) throws Exception {



		Map userInfo;

		userInfo = userService.selectInfo("FM_USER", param);
		if(userInfo == null || userInfo.size() ==0)
		{
			throw new Exception("Not exist user " + param.get("USER_ID") + ".\n");
		}
		param.put("INS_ID",  userInfo.get("USER_ID").toString());
		param.put("COMP_ID", userInfo.get("COMP_ID"));
		param.put("TEAM_CD", userInfo.get("TEAM_CD"));

		Map oldInfo = this.selectOneByQueryKey("selectByName_NC_VM", param);
		if(oldInfo != null)
		{
			param.put("VM_ID", oldInfo.get("VM_ID"));
			throw new AlreadyExistException("Already  exists VM : " + param.get("VM_NM") + ".\n");
		}
		param.put("VM_ID",IDGenHelper.getId(this.getSVC_CD()));
		param.put("BY_PASS_COMMON_PARAM", "Y");

		param.put("INS_DT", param.get("_SYSDATE_"));
		

		this.insertDBTable("NC_VM", param);

		Map result = vmStatus.run(true, (String)param.get("DC_ID"), (String)userInfo.get("COMP_ID"), userInfo.get("TEAM_CD"), param);
		boolean isSave = dcService.getDC((String)param.get("DC_ID")).getBefore(dcService).onAfterGetVM(param); // 사이트 마다 추가 다른 값 넣어서..
		
		if(isSave) {
			param.putAll(result);			
			this.updateDBTable("NC_VM", param);
		}
		
		//this.insertDBTable("NC_VM", param);


		//param.put("CUD_CD", "C");
		param.put("APPR_STATUS_CD", "A");
		param.put("_SYSDATE_", param.get("INS_DT"));
		this.insertDBTable("NC_VM_REQ", param);
	}

	@Override
	protected String[] getChildTable() {
		return new String[] {"NC_DISK" };
	}

	@Override
	protected void onBeforeDeploy(Map info) throws Exception{
		super.onBeforeDeploy(info);
		info.put("RUN_CD", "W");
	}
	@Override
	public boolean updateReDeploy( String pkVal, String date, String cudCd) throws Exception{
		vmStatus.run(true, pkVal);
		this.updateStatus(pkVal, date, "R"); 
		return super.updateDeploy(pkVal, date);
	}
	@Override
	protected boolean onDeploy(String pkVal, String cudCode, Map info) throws Exception {
		boolean result = true;
		if(cudCode.equals(NCConstant.CUD_CD.C.toString())) {
			deployVMUser(pkVal, (String)info.get("INS_DT"));
			deployVMDisk(pkVal, (String)info.get("INS_DT"));
			Map param = new HashMap();
			param.put("VM_ID", pkVal);
			param.put("INS_DT", info.get("INS_DT"));
			this.updateByQueryKey("onAfterCreate", param);
			createVM.run(pkVal);


		} else if(cudCode.equals(NCConstant.CUD_CD.U.toString())) {
			diskService.deployByVMId(pkVal, (String)info.get("INS_DT"));
			if(isChangeSpec(info)) {
				chkGPU(info );
				DC dc = dcService.getDC((String)info.get("DC_ID"));
				result = dc.getBefore(dcService).reconfigVM( pkVal, (String)info.get("INS_DT"), info );
			}

		} else if(cudCode.equals(NCConstant.CUD_CD.D.toString())) {
			List<Map> fwList = this.selectListByQueryKey("list_del_vm_fw", info);
			//접근제어가 결재 요청 중 -> 결재 취소, 사유 입력
			//접근제어가 작업 완료 -> 삭제
			//접근제어가 작업함에 있는 경우 -> 작업 취소
			//접근제어가 어드민 작업전 -> 거부
			Map tmp = new HashMap();
			for(Map loop : fwList){
				tmp.clear();
				tmp.putAll(loop);
				if("W".equals(loop.get("APPR_STATUS_CD"))){//작업함
					fwService.updateStatusCancel((String)tmp.get("FW_ID"), (String)tmp.get("INS_DT"));
					continue;
				}else if("R".equals(loop.get("APPR_STATUS_CD"))){//승인대기
					Map map = this.selectOneByQueryKey("select_del_vm_fw_req_detail", tmp);
					if(map != null)
					{
						tmp.putAll(map);
					}
					tmp.put("APPR_STATUS_CD", "C");
					tmp.put("APPR_COMMENT", "VM Deleted");
					reqHistoryService.updateCancel(tmp);
					continue;
				}else if("A".equals(loop.get("APPR_STATUS_CD")) || "".equals(loop.get("APPR_STATUS_CD"))){//승인
					if("W".equals(loop.get("TASK_STATUS_CD"))){//작업중
						tmp.put("TASK_STATUS_CD", "D");
						tmp.put("FAIL_MSG", "VM Deleted");
						fwMngService.updateApproval(tmp);
						continue;
					}else if("S".equals(loop.get("TASK_STATUS_CD"))){//작업완료
						tmp.put("CMT", "VM Deleted");
						this.updateByQueryKey("update_del_vm_fw", tmp);
						continue;
					}
				}
			}
			DC dc = dcService.getDC((String)info.get("DC_ID"));
			result = dc.getBefore(dcService).deleteVM( pkVal, (String)info.get("INS_DT"), info);
			 
			this.updateByQueryKey("delete_NC_SNAPSHOT", info);
		}
		return result;
	}

	/**
	 * 디스크 정보 deploy
	 * @param pkVal
	 * @param instDt
	 * @throws Exception
	 */
	protected void deployVMDisk(String pkVal, String instDt) throws Exception {
		Map param  = new HashMap();
		param.put("VM_ID", pkVal);
		param.put("INS_DT", instDt);
		diskService.updateByQueryKey("delete_NC_DISK_byVM", param);
		diskService.updateByQueryKey("deploy_NC_DISK", param);

	}

	@Override
	protected int updateStatus(String pkVal, String date, String status ) throws Exception
	{
		int row = super.updateStatus(pkVal, date, status);
		if(row == 1)
		{
			Map param  = new HashMap();
			param.put("VM_ID", pkVal);
			param.put("INS_DT", date);
			param.put("APPR_STATUS_CD",status);
			this.updateByQueryKey("update_NC_FW_REQ_status", param);
			this.updateByQueryKey("update_NC_DISK_REQ_status", param);
		}
		return row;
	}
	public void deployVMUser(String pkVal, String instDt) throws Exception {
		Map param  = new HashMap();
		param.put("VM_ID", pkVal);
		param.put("INS_DT", instDt);
		this.updateByQueryKey("delete_NC_FW", param);
		this.updateByQueryKey("deploy_NC_FW", param);
	}

	@Override
	public String getSVC_CD(){
		return NCConstant.SVC.S.toString();
	}

	@Autowired
	transient CreateNic createNic;



	@Autowired
	transient RenameVM renameVM;

	public Map insertNic(String vmId) throws Exception {
		return createNic.run(vmId);
	}
	@Override
	public Map getInfo(Map  param) throws Exception
	{
		Map info = super.getInfo(param);
		if(info == null) {
			return null;
		}

		info.put("NC_VM_DATA_DISK", diskService.selectListByQueryKey("list_NC_DISK_byVM", param));

		return info;
	}

	// data disk 변경시 변경될 내용과 삭제될 내용
	public void chkDiskSize(String vmId, List<Map> vmDiskReqList ) throws Exception {
		Map param = new HashMap();
		param.put("VM_ID", vmId);
		List<Map> oldDiskInfo = diskService.selectListByQueryKey("list_NC_DISK_byVM", param);
		Map<Object,Map> oldDiskMap = new HashMap();
		for(Map m : oldDiskInfo)
		{
			oldDiskMap.put(m.get("DISK_ID"), m);
		}

		for(Map m : vmDiskReqList)
		{
			Object diskId = m.get("DISK_ID");
			Map oldInfo = oldDiskMap.get(diskId);
			if(oldInfo != null)
			{
				if(!m.get("DISK_TYPE_ID").equals(oldInfo.get("DISK_TYPE_ID")))
				{
					throw new Exception("not same disk type");
				}
				if(NumberUtil.getInt(m.get("DISK_SIZE"))<NumberUtil.getInt(oldInfo.get("DISK_SIZE")))
				{
					throw new Exception("disk size error " + oldInfo.get("DISK_SIZE") + ">" + m.get("DISK_SIZE") );
				}
				else if(NumberUtil.getInt(m.get("DISK_SIZE")) == NumberUtil.getInt(oldInfo.get("DISK_SIZE")))
				{
					m.put("CUD_CD", "");
				}
				else
				{
					m.put("CUD_CD", "U");
				}
				m.put("OLD_DISK_SIZE",  oldInfo.get("DISK_SIZE"));
				m.put("DISK_PATH",  oldInfo.get("DISK_PATH"));

				oldDiskMap.remove(diskId);

			}
			else
			{
				m.put("CUD_CD", "C");
			}
		}
		for(Map m : oldDiskMap.values())
		{
			m.put("OLD_DISK_SIZE",  m.get("DISK_SIZE"));
			m.put("DISK_SIZE", 0);
			m.put("CUD_CD", "D");
			vmDiskReqList.add(m);
		}
	}
	@Override
	protected void onAfterInsertReq(String tableNm, Map param) throws Exception {
		if("Y".equals(param.get("ONLY_COPY")) && tableNm.equals("NC_VM_REQ") && param.get("FROM_ID") != null) {
			//insertByQueryKey("insert_NC_FW_REQ_by_fromId", param);
		}
	}

	@Override
	protected int updateDBTable(String tableNm, Map param) throws Exception {
		Map oldInfo = null;
		if("NC_VM".equals(tableNm)){
			chkDuplicate(tableNm, param,"VM_NM");
			oldInfo = this.selectInfo("NC_VM_org", param);
			if(oldInfo == null || oldInfo.size()==0) return 0;
			param.put("OLD_VM_NM", oldInfo.get("VM_NM"));
			param.put("OLD_DISK_SIZE", oldInfo.get("DISK_SIZE"));
			param.put("OLD_RAM_SIZE", oldInfo.get("RAM_SIZE"));
			param.put("OLD_CPU_CNT", oldInfo.get("CPU_CNT"));
			param.put("OLD_GPU_SIZE", oldInfo.get("GPU_SIZE"));
			
			if(isChangeValue(param, "VM_NM")){
				renameVM.run((String)param.get("VM_ID"), (String)param.get("VM_NM"));
			}
			
		}
		int row =  super.updateDBTable(tableNm, param);
		 
		return row;
	}

	private boolean isChangeSpec(Map param) {
		return isChangeValue(param, "RAM_SIZE")|| isChangeValue(param, "CPU_CNT") || isChangeValue(param,"DISK_SIZE")|| isChangeValue(param,"GPU_SIZE");
	}
	@Autowired CreateVM createVm;
	public void updateRecreate(Map param) throws Exception {
		if("N".equals(param.get("reKubun")))
		{		
			String osId = (String)param.get("OS_ID");
			if(osId.startsWith("S") ||osId.startsWith("G"))
			{
				param.put("FROM_ID", osId);
				
			}
			else
			{
				param.put("OS_ID", osId);
			}
			updateByQueryKey("update_NC_VM_OS_FROM", param);
		}
		fwService.updateByQueryKey("updateTaskWByVMId", param);
		diskService.deployByVMId((String)param.get("VM_ID"), (String)param.get("INS_DT"));
		createVm.run((String)param.get("VM_ID"));
	}

	@Autowired UserSearchService userService;
	@Autowired DCService dcService;
	@Autowired
	transient GetVMInfos vmStatus;
	@Autowired PublicipService ipService;
	public void updateExcelImport(List<Map> list, String dcId, Map osMap) throws Exception
	{
		StringBuffer err = new StringBuffer();
		int idx=2;
		for(Map m : list)
		{
			try
			{
				m.put("DC_ID", dcId);
				insertVMfromDC(m);
			} catch (Exception e) {
				if(e instanceof NotFoundException)
				{
					err.append(MsgUtil.getMsg("msg_cant_find_vm_info", new String[] {String.valueOf(idx), String.valueOf(m.get("VM_NM"))}));
				}
				else
				{
					e.printStackTrace();
					err.append(MsgUtil.getMsg("msg_cant_find_vm_info", new String[] {String.valueOf(idx), e.getMessage()}));
				}
			}

		}
		if(err.length()>0)
		{
			throw new Exception(err.toString());
		}
	}

	public Map getVMByFromId(Map vm) throws Exception {
		return this.selectOneByQueryKey("selectByFromId", vm);
	}
	public boolean doInsertVM(Map vm) throws Exception {
		vm.put("VM_ID",IDGenHelper.getId(getSVC_CD()));
		insertDBTable("NC_VM", vm);
		return true;
	}

	protected void genPK(String tableNm, Map param) throws Exception {
		this.chkDuplicate(tableNm.substring(0, tableNm.length()-"_REQ".length()), param, "VM_NM");
		if("NC_VM_REQ".equals(tableNm)) param.put(this.getPks()[0], IDGenHelper.getId(this.getSVC_CD()));
	}
	protected int insertReqDBTable(String tableNm, Map param) throws Exception
	{
		if(tableNm.equals("NC_VM_REQ") && "C".equals(param.get("CUD_CD"))) {
			String vmNm = (String) param.get("VM_NM");
			if(vmNm == null || "".equals(vmNm)) {
				vmNm = this.dcService.getDC((String)param.get("DC_ID")).getBefore(dcService).getVMName(param);
				param.put("VM_NM", vmNm);
			}
			if(vmNm == null || "".equals(vmNm)) {
				throw new Exception("VM name is empty");
			}
		}
		return super.insertReqDBTable(tableNm, param);
	}
	public void updateVMReq(Map selRow, List fwList,  List diskList, boolean isDeploy) throws Exception {
		int updateRow = this.insertReqDBTable("NC_VM_REQ", selRow);
		if(diskList != null){
			diskService.deleteByQueryKey("delete_NC_DISK_REQ_byVM", selRow);
			for(Object rowObj : diskList){
				Map row = (Map)rowObj;
				row.put("VM_ID", selRow.get("VM_ID"));
				row.put("DC_ID", selRow.get("DC_ID"));
				row.put("TEAM_CD", selRow.get("TEAM_CD"));
				row.put("INS_ID", selRow.get("INS_ID"));

				row.put("INS_DT", selRow.get("INS_DT"));

				diskService.saveReq(diskService.getTableName()+"_REQ", row);

			}
		}
		if(updateRow>0 && isDeploy){
			this.update_deployReq(selRow);
		}
	}

	@Autowired DiskService diskService;
	public void insertVMReq(Map selRow, List fwList, List diskList, boolean isDeploy) throws Exception {
		int insertRow = this.insertReqDBTable("NC_VM_REQ", selRow);
		if(insertRow == 0 ) return;
		if(fwList != null){
			for(Object rowObj : fwList){
				Map row = (Map)rowObj;
				row.put("VM_ID", selRow.get("VM_ID"));
				row.put("INS_DT", selRow.get("INS_DT"));
				insertRow = fwService.saveReq(fwService.getTableName()+"_REQ", row);

			}
		}
		if(diskList != null){
			diskService.deleteByQueryKey("delete_NC_DISK_REQ_byVM", selRow);
			for(Object rowObj : diskList){
				Map row = (Map)rowObj;
				row.put("VM_ID", selRow.get("VM_ID"));
				row.put("DC_ID", selRow.get("DC_ID"));
				row.put("TEAM_CD", selRow.get("TEAM_CD"));
				row.put("INS_ID", selRow.get("INS_ID"));
				row.put("CUD_CD", "C");
				row.put("INS_DT", selRow.get("INS_DT"));
				row.put("DISK_NM", selRow.get("DISK_NM"));
				insertRow = diskService.saveReq(diskService.getTableName()+"_REQ", row);

			}
		}
		if(insertRow > 0 && isDeploy){
			this.update_deployReq(selRow);
		}
	}

	public List vmUserList(Map searchParam) throws Exception {
		List<Map> list= this.selectListByQueryKey("list_NC_FW", searchParam);
		addCodeNames(list);

		return list;
	}

	@Override
	public String[] getPks() {
		return new String[] {"VM_ID"};
	}

	@Override
	protected String getTableName() {
		return "NC_VM";
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("TASK_STATUS_CD", "TASK_STATUS_CD");
		config.put("APPR_STATUS_CD", "APP_KIND");
		config.put("RUN_CD", "RUN_CD");
		config.put("CUD_CD", "CUD");
		config.put("P_KUBUN", "P_KUBUN");
		config.put("PURPOSE", "PURPOSE");
		return config;
	}

	public Map getInfoWithOld(Map param) throws Exception{
		Map info = super.selectInfo(this.getTableName() + "_OLD", param);
		this.addCodeNames(info, this.getCodeConfig());

		info.put("isSavable", this.isSavable(param));
		info.put("isOwner", this.isOwner(param));
		return info;
	}



	@Override
	public IAfterProcess getAfter( String id) throws Exception {
		Map param = dcService.getVMCreateInfo(id, true);
		if(param == null) return null;
	 
		DC dcInfo = dcService.getDC((String)param.get("DC_ID"));
		param.putAll(dcInfo.getProp());
		return dcInfo.getBefore(dcService  ).onAfterProcess("S", "C", param);
	}
	
	@Override
	public void delete4DeployFail(Map param) throws Exception{
		super.delete4DeployFail(param);
		if("C".equals(param.get("CUD_CD"))) {
			deleteVM.run((String)param.get("SVC_ID"));
			param.put("DEL_YN", "Y");
			param.put("FAIL_MSG", param.get("ADMIN_CMT"));
			this.updateByQueryKey("NC_VM", param);
		}
		else if("U".equals(param.get("CUD_CD"))) {
			vmStatus.run(true, (String)param.get("SVC_ID"));
		}
	}
	public boolean needRestart(Map m ) throws Exception {
		if(NumberUtil.getInt(m.get("GPU_SIZE"),0) != NumberUtil.getInt(m.get("OLD_GPU_SIZE"),0)) {
			return true;
		}
		boolean isHotDeploy = HVProperty.getInstance().getProperty("reconfig.poweron_yn", "N").equals("Y");
		if(!isHotDeploy) {
			if(NumberUtil.getInt(m.get("RAM_SIZE"),0) != NumberUtil.getInt(m.get("OLD_RAM_SIZE"),0)) {
				return true;
			}
			if(NumberUtil.getInt(m.get("CPU_CNT"),0) != NumberUtil.getInt(m.get("OLD_CPU_CNT"),0)) {
				return true;
			}
		}
		else {
			if(NumberUtil.getInt(m.get("RAM_SIZE"),0) < NumberUtil.getInt(m.get("OLD_RAM_SIZE"),0)) {
				return true;
			}
			if(NumberUtil.getInt(m.get("CPU_CNT"),0) < NumberUtil.getInt(m.get("OLD_CPU_CNT"),0)) {
				return true;
			}
		}
		return false;
	}
	protected void chkGPU(Map m ) throws Exception {
		if(NumberUtil.getInt(m.get("GPU_SIZE"),0) > NumberUtil.getInt(m.get("OLD_GPU_SIZE"),0)) {
			Map param = new HashMap();
			param.put("DC_ID", m.get("DC_ID"));
			param.put("VM_NM", m.get("VM_NM"));
			param.put("GPU_SIZE", 0);
			Map hostInfo =  selectOneByQueryKey("com.clovirsm.monitor","select_target_gpu_server", param);
			if(hostInfo == null) {
				throw new MessageException("msg_no_gpu"); //"GPU가 없습니다.");
			}
			if(NumberUtil.getInt(hostInfo.get("REMAIN_GPU_SIZE")) < NumberUtil.getInt(m.get("GPU_SIZE"),0) - NumberUtil.getInt(m.get("OLD_GPU_SIZE"),0)) {
				throw new MessageException("msg_lack_gpu",   (String)hostInfo.get("NAME"),hostInfo.get("REMAIN_GPU_SIZE").toString() ,"" +  (NumberUtil.getInt(m.get("GPU_SIZE"),0) - NumberUtil.getInt(m.get("OLD_GPU_SIZE"),0))); //GPU가 부족합니다, (Host:{0}, 남아있는 GPU:{1}, 추가 필요 GPU:{2})
				
			}
		}
		 
	}
	@Override
	public String chkOnBeforeDeploy(String id, String instDt) throws Exception {
		Map reqInfo = setSelectedReq(id, instDt);
		if(reqInfo != null && "U".equals(reqInfo.get("CUD_CD"))) {
			if(needRestart(reqInfo )) {
				return MsgUtil.getMsg("msg_server_restart", new String[] {});
			}
		}
		return null;
	}
	@Override
	public List getReqDetailList( Map param ) throws Exception {
		List<Map> list = super.getReqDetailList(param);
		
		 
		for(int i=0; i < list.size(); i++){
			Map sInfo =  list.get(i);
			sInfo.put("DISK_LIST", diskService.list("list_NC_DISK_byVM", sInfo));
			if(needRestart(sInfo)){
				sInfo.put("WARN_MSG", MsgUtil.getMsg("msg_server_restart", new String[]{}) );
			}
		}
		return list;
	}
}
package com.clovirsm.service.resource;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.clovirsm.common.IAsyncJob;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Service
public class RevertService implements IAsyncJob{

	@Override
	public IAfterProcess getAfter(  String id) throws Exception {
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		Map param = new HashMap();
		param.put("VM_ID", id);
		
		param = dcService.selectOneByQueryKey("selectRevertInfo", param);
		if(param == null) return null;
		 
		DC dcInfo = dcService.getDC((String)param.get("DC_ID"));
		param.putAll(dcInfo.getProp());
		return dcInfo.getBefore(dcService  ).onAfterProcess("R", "C",  param);
	}

}

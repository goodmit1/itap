package com.clovirsm.service.resource;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.IAsyncJob;
import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCReqService;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.service.ComponentService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.executor.CreateImg;
import com.clovirsm.sys.hv.executor.DeleteImg;
import com.clovirsm.sys.hv.executor.RenameImg;

@Service
public class ImageService extends NCReqService implements IAsyncJob{

	@Autowired
	transient RenameImg renameImg;

	@Autowired
	transient CreateImg createImg;

	@Autowired
	transient DeleteImg deleteImg;
	@Autowired
	transient DCService dcService;

	protected String getNameSpace() {
		return "com.clovirsm.resources.image.Image";
	}


	@Override
	protected String getDefaultTitle(Map param) {
		return (String)param.get("IMG_NM");
	}

	@Override
	public String getSVC_CD(){
		return NCConstant.SVC.G.toString();
	}

	@Override
	protected boolean onDeploy(String pkVal, String cudCode, Map info)
			throws Exception {
		if(cudCode.equals(NCConstant.CUD_CD.C.toString()))
		{
			createImg.run(pkVal);
		}
		else if(cudCode.equals(NCConstant.CUD_CD.D.toString()))
		{
			deleteImg.run(pkVal);
		}
		return true;
	}

	@Override
	protected int insertDBTable(String tableNm, Map param) throws Exception {
		if(!tableNm.equals("NC_IMG")) param.put("FROM_ID", param.get("VM_ID"));
		return super.insertDBTable(tableNm, param);
	}

	@Override
	protected int updateDBTable(String tableNm, Map param) throws Exception {
		if("NC_IMG".equals(tableNm)){
			chkDuplicate(tableNm, param,"IMG_NM");
			if(isChangeValue(param, "IMG_NM")){
				renameImg.run((String)param.get("IMG_ID"), (String)param.get("IMG_NM"));
			}
		}
		return super.updateDBTable(tableNm, param);
	}
	public void chkDuplicate(String tableNm, Map selRow, String chkField) throws Exception
	{
		super.chkDuplicate(tableNm, selRow, chkField);
		int max = Integer.parseInt((String)ComponentService.getEnv("image.max","0"));
		if(max>0)
		{
			int num = super.selectCountByQueryKey("list_count_img_by_vm", selRow);
			if(num>=max)
			{
				throw new Exception("The maximum number(" + max + ") has been exceeded.");
			}
		}
		
	}
	@Override
	protected int deleteDBTable(String tableNm, Map param) throws Exception {
		return super.deleteDBTable(tableNm, param);
	}


	@Override
	protected String getTableName() {
		return "NC_IMG";
	}


	@Override
	public String[] getPks() {
		return new String[]{"IMG_ID"};
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("TASK_STATUS_CD", "TASK_STATUS_CD");
		config.put("APPR_STATUS_CD", "APP_KIND");
		config.put("CUD_CD", "CUD");
		config.put("P_KUBUN", "P_KUBUN");
		config.put("PURPOSE", "PURPOSE");
		return config;
	}
	@Override
	public int saveReq(String tableNm,Map param) throws Exception
	{
		if(tableNm.equals("NC_IMG_REQ") && "C".equals(param.get("CUD_CD")))
		{
			String imgNm = (String) param.get("IMG_NM");
			if(imgNm == null || "".equals(imgNm))
			{
				imgNm = this.dcService.getDC((String)param.get("DC_ID")).getBefore(dcService).getImgName(param);
				param.put("IMG_NM", imgNm);
			}
			if(imgNm == null || "".equals(imgNm))
			{
				throw new Exception("name is empty");
			}
		}
		return super.saveReq(tableNm, param);
	}


	@Override
	public IAfterProcess getAfter(  String id) throws Exception {
		Map param = dcService.getImgInfo(id);
		if(param == null) return null;
	 
		DC dcInfo = dcService.getDC((String)param.get("DC_ID"));
		param.putAll(dcInfo.getProp());
		return dcInfo.getBefore(dcService  ).onAfterProcess("G","C", param);
		 
	}
}
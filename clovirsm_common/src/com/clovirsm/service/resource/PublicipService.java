package com.clovirsm.service.resource;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCReqService;

@Service
public class PublicipService extends NCReqService {

	protected int updateDBTable(String tableNm, Map param) throws Exception {
		if("NC_PUBLIC_IP".equals(tableNm)){
			if(isChangeValue(param, "PUBLIC_IP_NM")){
				chkDuplicate(tableNm, param, "PUBLIC_IP");
			}
		}
		return super.updateDBTable(tableNm, param);
	}
	protected String getNameSpace() {
		return "com.clovirsm.resources.publicip.Publicip";
	}

 
	@Override
	protected String getDefaultTitle(Map param) {
		return (String)param.get("PUBLIC_IP_NM");
	}

	/*@Override
	protected String[] getChildTable()
	{
		return new String[] {"NC_FW"};
	}*/
	@Override
	public String getSVC_CD() {
		return NCConstant.SVC.I.toString();
	}

	@Override
	protected boolean onDeploy(String pkVal, String cudCode, Map info) throws Exception {
		return true;
	}
	@Override
	public String[] getPks() {
		 
		return new String[]{"PUBLIC_IP_ID"};
	}
	@Override
	protected String getTableName() {
		return "NC_PUBLIC_IP";
	}

}
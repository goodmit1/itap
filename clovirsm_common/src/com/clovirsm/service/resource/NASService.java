package com.clovirsm.service.resource;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCReqService;

@Service("NASService")
public class NASService extends NCReqService {

	@Override
	protected String getDefaultTitle(Map param) {
	 
		return null;
	}

	@Override
	protected boolean onDeploy(String pkVal, String cudCode, Map info) throws Exception {
	 
		return true;
	}

	@Override
	public String getSVC_CD() {
		 
		return NCConstant.SVC.N.toString();
	}

	@Override
	protected String getTableName() {

		return "NC_NAS";
	}

	@Override
	public String[] getPks() {
	 
		return new String[] {"NAS_ID"};
	}

	@Override
	protected String getNameSpace() {
		// TODO Auto-generated method stub
		return null;
	}

}

package com.clovirsm.service.batch;

import java.rmi.dgc.VMID;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.log4j.LogManager;
import org.apache.xmlbeans.impl.jam.mutable.MParameter;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import com.fliconz.fm.common.CodeHandler;
import com.fliconz.fm.security.FMSecurityContextHelper;
import com.fliconz.fm.util.IMailSender;
import com.fliconz.fw.runtime.util.DateUtil;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.ibm.icu.util.Calendar;
import com.clovirsm.common.NCScheduleFactoryBean;
import com.clovirsm.service.AlarmService;
import com.clovirsm.service.ComponentService;
import com.clovirsm.service.admin.LicenseELAHelper;
import com.clovirsm.service.admin.NotUseVMService;
import com.clovirsm.service.admin.OsTypeMngService;
import com.clovirsm.service.admin.VmSpecMngService;
import com.clovirsm.service.admin.VraCatalogMngService;
import com.clovirsm.service.monitor.OverUnderVMService;
import com.clovirsm.service.resource.SnapshotService;
import com.clovirsm.service.resource.VMService;
import com.clovirsm.service.workflow.ExpireService;
import com.clovirsm.service.workflow.WorkService;
import com.clovirsm.sys.hv.CommonScheduleService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.executor.ChkServiceLive;
import com.clovirsm.sys.hv.executor.InsertAlarm;
import com.clovirsm.sys.hv.executor.InsertAllIp;
import com.clovirsm.sys.hv.executor.InsertAllVMPerf;
import com.clovirsm.sys.hv.executor.InsertDSPerf;
import com.clovirsm.sys.hv.executor.InsertHostPerf;
import com.clovirsm.sys.hv.executor.InsertLicense;
import com.clovirsm.sys.hv.executor.InsertMMStat;
import com.clovirsm.sys.hv.executor.InsertVMPerf;
import com.clovirsm.sys.hv.executor.InsertVMTag;
import com.clovirsm.sys.hv.executor.ListHVObjects;
import com.clovirsm.sys.hv.vmware.VROpsAction;

public class ScheduleService  extends CommonScheduleService{
	
	public static String NS="com.clovirsm.common.log.";

	@Resource InsertAlarm insertAlarm;

	@Resource InsertAllVMPerf insertAllVMPerf;
	
	@Resource InsertVMPerf insertVMPerf;

	@Resource InsertHostPerf insertHostPerf;
	@Resource InsertDSPerf insertDSPerf;
	 
	@Autowired CodeHandler codeHandler;
	@Autowired UsageLogService usageLogService;
	@Autowired InsertAllIp insertAllIp;
	@Autowired WorkService workService;
	@Autowired DCService dcService;
	 
	@Autowired transient SnapshotService snapshotService;
	@Autowired NotUseVMService notUseVmservice;
	@Autowired AlarmService alaramService = new AlarmService();

	
	
	
	public void removeOldSnapshot()
    {
		try
		{
			FMSecurityContextHelper.configureAuthentication("ROLE_SUPER_ADMIN");
			Map param = new HashMap();
		 
			
			List<Map> list = dcService.selectRemoveSnapshotTarget( );
			for(Map m : list)
			{
				snapshotService.removeSnapshot(m);
				alaramService.insertAlarm("remove snapshot" + m.get("VM_NM"), "S", m.get("VM_ID"), m.get("INS_ID"));
				
			}
				
		 
	    } catch (Exception e) {
			 
			e.printStackTrace();
			try {
				alaramService.logAlarm("remove snapshot Exception : "+ e, "SCHEDULE", null);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
		}
		finally
		{
			FMSecurityContextHelper.clearAuthentication();
		}
    }
	
	private float getTerm(String startTime, String endTime)
	{
		return Integer.parseInt(endTime.substring(0,2)) + 1.0f * Integer.parseInt(endTime.substring(2))/60  - (Integer.parseInt(startTime.substring(0,2)) + 1.0f * Integer.parseInt(startTime.substring(2))/60 );
	}

	private String format(Date date, String pattern)
	{
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		return format.format(date);
	}
	private long getDateDiff(String date1 ) throws Exception
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

		  Date d1 = null;
		  Date d2 = null;


		   d1 = format.parse(date1);
		   d2 = new Date();

		   //in milliseconds
		   long diff = d2.getTime() - d1.getTime();
		   return diff / (24 * 60 * 60 * 1000);
	}
	private long getMonthDiff(String date1 ) throws Exception
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyyMM");

		  Date d1 = null;
		  Date d2 = null;


		   d1 = format.parse(date1);
		   d2 = new Date();


		 Calendar startCalendar = Calendar.getInstance();
		 startCalendar.setTime(d1);
		 Calendar endCalendar = Calendar.getInstance();
		 endCalendar.setTime(d2);

		 int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
		 int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
		 return diffMonth;
	}
	public void runCollectVRA()
	{
		if( isRunServer())
		{	
			
			try {
				VraCatalogMngService catalog = (VraCatalogMngService)SpringBeanUtil.getBean("vraCatalogMngService");
				catalog.insertCollect();
				
				 
				
				
				
			
				
				
			} catch (Exception e) {
				log.error("VRA", e);
			}
		}
	}
	/**
	 * IP 정보 수집
	 */
	public void runGatherAllIp()
	{
		if( isRunServer())
		{	
			try
			{
			 
					insertAllIp.runAll(false);
			
			}catch (Exception e) {
				e.printStackTrace();
				log.error("IP", e);
			}
			
		}
	}
  
	/**
	 * IP 정보 수집
	 */
	
	@Autowired OverUnderVMService overUnderVMService;
	 
	
	@Autowired VROpsAction opsAction;
	/**
	 * VROPS 정보 수집
	 */
	public void runVROps()
	{
		if( isRunServer())
		{
			try {
				opsAction.insertDCHealth();
			} catch (Exception e) {
				e.printStackTrace();
				log.error("VROps-DCHealth", e);
			}
			try {
				overUnderVMService.insertFromVROps();
			}
			catch (Exception e) {
				e.printStackTrace();
				log.error("VROps-OverUnderSize", e);
			}
		}
	}
	/**
	 * 모든 VM 정보 모니터링 정보 수집
	 */
	public void runGatherAllVM()
	{
		if( isRunServer())
		{
			try {
				insertAllVMPerf.runAll(false);
	
			} catch (Exception e) {
	
				e.printStackTrace();
				log.error("VM MONITOR", e);
			}
		}
	}
	/**
	 * 모니터링 정보 수집
	 */
	public void runGatherAlarmExcludeVM()
	{
		if( isRunServer())
		{
			 
			
			try {
				insertAlarm.runAll( true );
				insertAlarm.alaramSend();
			} catch (Exception e) {

				e.printStackTrace();
				log.error("ALARM", e);
			}

			try
			{
				insertHostPerf.runAll(true);
			}
			catch(Exception e)
			{
				log.error("HOST MONITOR", e);
			}
			try
			{
				insertDSPerf.runAll(true);
			}
			catch(Exception e)
			{
				log.error("DS MONITOR", e);
			}
		}
	}
	
	 
	public void chkSvcLive() {
		if( isRunServer())
		{
			try {
				ChkServiceLive   chkServiceLive = (ChkServiceLive)SpringBeanUtil.getBean("chkServiceLive");
				chkServiceLive.runAll();
			}
			catch (Exception e) {

				e.printStackTrace();
				log.error("SVC_LIVE", e);
			}
		}
		
	}
	/**
	 * 모니터링 정보 수집
	 */
	public void runGatherAlarm()
	{
		if( isRunServer())
		{
			 
			
			try {
				insertAlarm.runAll( true );
				insertAlarm.alaramSend();
			} catch (Exception e) {

				e.printStackTrace();
				log.error("ALARM", e);
			}

			try
			{
				insertHostPerf.runAll(true);
			}
			catch(Exception e)
			{
				log.error("HOST MONITOR", e);
			}
			try
			{
				insertDSPerf.runAll(true);
			}
			catch(Exception e)
			{
				log.error("DS MONITOR", e);
			}
			try {
				insertVMPerf.runAll(false);

			} catch (Exception e) {

				e.printStackTrace();
				log.error("VM MONITOR", e);
			}
		}

	}
	@Autowired InsertLicense insertLicense;
	/**
	 * 라이센스 정보 수집
	 */
	public void runInsertLicense()
	{
		 
		try
		{
			if( isRunServer())
			{
				insertLicense.runAll(true);
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace( );
			log.error("GET LICENSE INFO", e);
		}
	}
	public void runCollectHVObj()
	{
		if( isRunServer())
		{
			ListHVObjects service = (ListHVObjects)SpringBeanUtil.getBean("listHVObjects");
			try {
				service.runAll(true);
				
			} catch (Exception e) {
				log.error("runHVObj", e);
			}
		}
	}
	/**
	 * 라이센스 ELA 정보 수집
	 */
	public void runInsertLicenseELA()
	{
		 
		try
		{
			if( isRunServer())
			{
				runInsertLicense();
				runCollectHVObj();
				
				LicenseELAHelper helper = new LicenseELAHelper();
				String yyyyMM = DateUtil.format(new Date(), "yyyyMM");
				helper.insertProductCnt(yyyyMM);
				helper.insertCPUCnt(yyyyMM);
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace( );
			log.error("GET LICENSE INFO", e);
		}
	}


	@Autowired InsertMMStat insertMMStat;

	/**
	 * 월별 통계(모비스)
	 * @param yyyyMM
	 */
	public void runMonthStat()
	{

		Date date = new Date();
		//date.setMonth(date.getMonth()-1);
		String yyyyMM = DateUtil.format(date, "yyyyMM");
		this.runMonthStat(yyyyMM);
	}

	/**
	 * 월별 통계(모비스)
	 * @param yyyyMM
	 */
	public void runMonthStat(String yyyyMM)
	{
		  
		try
		{
			insertMMStat.run( yyyyMM);
			insertMMStat.runMonitorInfo(yyyyMM);
			
		}
		catch(Exception e)
		{
			e.printStackTrace( );
			log.error("MM AVG", e);
		}
	}
	/**
	 * 월별 통계(모비스)
	 * @param yyyyMM
	 */
	public void runMonthStatHasDetail()
	{
		if( isRunServer())
		{ 
			try	{

				Date date = new Date();
				//date.setMonth(date.getMonth()-1);
				String yyyyMM = DateUtil.format(date, "yyyyMM");
				insertMMStat.runHasDetail( yyyyMM);
				insertMMStat.runMonitorInfo(yyyyMM);
				
			}
			catch(Exception e)
			{
				e.printStackTrace( );
				log.error("MM AVG", e);
			}
		}
	}

 
	/**
	 * 일일 사용량
	 */
	public void runDayUseTime()
	{
		try {
			String lastDate =  sqlSession.selectOne(NS+ "selectMaxDayUse", new HashMap());

			long diff = 10;
			if(lastDate != null) diff = getDateDiff(lastDate );
			for(int i=0; i<diff; i++)
			{
				String prevDate = format( DateUtil.addDate(new Date(), Calendar.DAY_OF_YEAR, -1 * i), "yyyyMMdd");
				usageLogService.insert_NC_DD_USE(prevDate);
			}
		} catch (Exception e) {

			e.printStackTrace();
			String prevDate = format( DateUtil.addDate(new Date(), Calendar.DAY_OF_YEAR, -1), "yyyyMMdd");
			usageLogService.insert_NC_DD_USE(prevDate);
		}

	}

	/**
	 * 일일 사용량과 월별 사용 통계
	 */
	public void runDayMMStat()
	{
		if( isRunServer())
		{
			runDayUseTime();
			this.runMonthStat();
		}
	}
	/**
	 * VM Tag정보 수집
	 */
	public void runVMTag() throws Exception 
	{
		if( isRunServer())
		{
			 InsertVMTag vmTag  = (InsertVMTag)SpringBeanUtil.getBean("insertVMTag");
			 vmTag.runAll(true);
		}
	}
	
	public void runReclaimWork() throws Exception 
	{
		if( isRunServer())
		{	
			Map param = new HashMap();
			param.put("limit", (String)ComponentService.getEnv("use_work_reclaim","6"));
			List<Map<String, Object>> list = sqlSession.selectList("list_NC_WORK_RECLAIM", param);
			Set<Map> set = new HashSet<Map>();
			Iterator<Map<String, Object>> iter = list.iterator();
			if(!list.isEmpty())
			{
				while(iter.hasNext())
				{
					System.out.print("LIST ITER : ");
					System.out.println(iter.next());
					set.addAll(list);
					workService.updateWorkCancel(set);
	
				}
			}
		}
	}
	
	public void runNotUseVmMailSend() throws Exception{
			Map<String, Object> param = new HashMap();
			List<Map<String,Object>> dcList = sqlSession.selectList("list_NotUse_DC");
			List<Map<String,Object>> vmList = new ArrayList<>();
			Set<String> set = new HashSet<>();
			IMailSender mailSender = (IMailSender)SpringBeanUtil.getBean("mailSender");
			
			param.put("CPU", (String)ComponentService.getEnv("use_vm.cpu_min", "5"));
			param.put("MEM", (String)ComponentService.getEnv("use_vm_mem_min", "5"));
			param.put("NET", (String)ComponentService.getEnv("use_vm.net_min", "5"));
			param.put("DAY", (String)ComponentService.getEnv("use_vm.max_not_use_day", "30"));
			param.put("ANDOR","OR");
			param.put("Team", PropertyManager.getString("send.mail.team"));
			param.put("User_Name", PropertyManager.getString("send.mail.admin").substring(0, 3));
			
			param.put("Date",calParam());
			param.put("mail_title", "장기간미사용서버");

			String[] admin= PropertyManager.getString("send.mail.admin.email").split(",");
			Stream<String> adminEmail = Stream.of(admin);
			
			dcList.forEach(dc -> {
				param.put("DC_ID",(String) dc.get("DC_ID"));
				param.put("DC_NM",(String) dc.get("DC_NM"));
				try {
					List<Map<String,Object>> list = notUseVmservice.listMinUse(param); // 미사용서버 데이터 list
					list.stream().map(s -> s.get("VM_ID")).forEach(s ->{ //장기간 미사용 목록
						param.put("VM_ID", (String) s);
						vmList.addAll(sqlSession.selectList("selectNotUseVmUser",param));
					});
				} catch (Exception e) {
					e.printStackTrace();
				} // 미사용서버 데이터 list
			});
			
			vmList.stream().map(s ->  s.get("EMAIL")).forEach(s ->{ set.add((String) s); }); // 중복 발송 방지용 set 

		List<Map<String,Object>> userList = vmList.stream().flatMap(s -> {
				List<Map<String,Object>> list = new ArrayList<>();
				Map map = new HashMap<>();
				map.put("EMAIL",(String) s.get("EMAIL"));
				map.put("VM_ID",(String) s.get("VM_ID"));
				map.put("VM_NM", (String) s.get("VM_NM"));
				map.put("USER_NAME", (String) s.get("USER_NAME"));
				map.put("LOGIN_ID", (String) s.get("LOGIN_ID"));
				map.put("TEAM_NM", (String) s.get("TEAM_NM"));
				list.add(map);
				return list.stream();
			}).collect(Collectors.toList());
			
			userList.stream().forEach(x->System.out.println("x: "+x));
			
			System.out.println("발송 Email list :"+set);
			StringBuilder adminSendContent = new StringBuilder();
			if(!set.isEmpty()) {
				for (String email : set) { //사용자 발송
					try {
						List<String> sendList = new ArrayList<>();
						StringBuilder content = new StringBuilder();
						for (Map<String, Object> map : userList) {
							if(email.equals(map.get("EMAIL"))) {
								sendList.add(map.get("VM_NM") + " | " + map.get("USER_NAME") + " | "  + map.get("LOGIN_ID") + " | "  + map.get("TEAM_NM") +"<br>");		
								content.append("<tr><td>"+map.get("VM_NM").toString()+"</td><td>"+map.get("USER_NAME").toString()+"</td><td>"+map.get("LOGIN_ID")+"</td></tr>");
								adminSendContent.append("<tr><td>"+map.get("VM_NM").toString()+"</td><td>"+map.get("USER_NAME").toString()+"</td><td>"+map.get("LOGIN_ID")+"</td></tr>");
							}
						}//map for
						param.put("NotUseNm",content.toString());
						mailSender.send(email,"미사용 서버 test", "notuseVM", "ko", param);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				adminEmail.forEach(x->{ //운영자 발송
					param.put("NotUseNm", adminSendContent.toString());
					try {
						mailSender.send(x,"미사용 서버 test admin", "notuseVM", "ko", param);
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
			}
			
		/*	if(!set.isEmpty()) {
				set.stream().forEach(email ->{
					try {
						List<String> sendList = new ArrayList<>();
						userList.stream().filter(x -> email.equals(x.get("EMAIL"))).forEach(x->{
							sendList.add(x.get("VM_NM") + " | " + x.get("USER_NAME") + " | "  + x.get("LOGIN_ID") + " | "  + x.get("TEAM_NM") +"<br>");								
						});
						String content = sendList.stream().collect(Collectors.joining());
						System.out.println("email: "+email+" 발송내용: "+content);
						param.put("NotUseNm",content);
						mailSender.send(email,"미사용 서버 test", "notuseVM", "ko", param);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
			}*/
	}
	
	public String calParam() {
		String date;
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH)+1;
		String sMonth="";
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int week = cal.get(Calendar.DAY_OF_WEEK);
		String dayOfWeek = "";
		
		if(month<10) {
			sMonth=("0".concat(String.valueOf(month)));
		}
		
		switch (week) {
			case 1:
				dayOfWeek = "일";
				break ;
			case 2: 
				dayOfWeek = "월";
				break ;
			case 3:
				dayOfWeek = "화";
				break ;
			case 4:
				dayOfWeek = "수";
				break ;
			case 5:
				dayOfWeek = "목";
				break ;
			case 6:
				dayOfWeek = "금";
				break ;
			case 7:
				dayOfWeek = "토";
				break ;
		}
		
		date = sMonth+"/"+day+"("+dayOfWeek+")";
		return date;
	}

	public void previousIpUpdate(){

	}
	
}

package com.clovirsm.service.batch;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.clovirsm.common.ClassHelper;
import com.clovirsm.common.NCDefaultService;

@Service
public class BatchService extends NCDefaultService{

	@Override
	protected String getNameSpace() {
		 
		return "com.clovirsm.sys.batch";
	}
	public void deleteMonitorInfo(String tableNm, Map param) throws Exception {
		this.deleteByQueryKey(  "delete_" + tableNm, param);
		 
		
	}
	public void insertMonitorInfo(String tableNm, Map m) throws Exception {
		if(tableNm.equals("NC_HV_ALARM") || tableNm.equals("NC_MONITOR_HOST"))
		{
			 
			int row = this.updateByQueryKey("update_" + tableNm, m);
			if(row==0)
			{
				this.insertByQueryKey( "insert_" + tableNm, m);
			}
		}
		else
		{
			this.insertByQueryKey( "insert_" + tableNm, m);
		}
	}
	public void updateOnAfter(String tableNm, Map m) throws Exception {
		if(tableNm.equals("NC_HV_ALARM") || tableNm.equals("NC_MONITOR_HOST"))
		{
			this.updateByQueryKey("delete_real_" + tableNm, m);
		}
		ClassHelper.getSite().onAfterCollect(tableNm, m);
	}
	public int updateMonitorInfo(String tableNm, Map m) throws Exception {
		return this.insertByQueryKey(  "update_" + tableNm, m);
	}
	
	public void insertGuestJobLog(Map m) throws Exception
	{
		this.insertDBTable("NC_VM_GUEST_JOB_LOG", m);
	}
	public void updateGuestJobLog(Map m) throws Exception
	{
		String failMsg = (String)m.get("FAIL_MSG");
		if(failMsg != null && failMsg.length()>500)
		{
			m.put("FAIL_MSG", failMsg.substring(0, 500));
		}
		this.updateDBTable("NC_VM_GUEST_JOB_LOG", m);
	}
	@Override
	public String[] getPks() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return null;
	}
}

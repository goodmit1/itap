package com.clovirsm.service.popup;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fliconz.fm.mvc.DefaultService;

@Service
public class NASSearchPopupService extends DefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.popup.NASSearch";
	}

	@Override
	public String[] getPks() {
		return new String[]{"NAS_IP","NAS_PATH"};
	}

	@Override
	protected String getTableName() {
		return "NC_NAS";
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		return config;
	}

}
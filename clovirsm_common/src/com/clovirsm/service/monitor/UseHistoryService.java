package com.clovirsm.service.monitor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
import com.clovirsm.common.NCDefaultService;

@Service
public class UseHistoryService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.myinfo.usehistory.UseHistory";
	}

	@Override
	protected String getTableName() {
		 
		return "NC_MM_STAT";
	}

	@Override
	public String[] getPks() {
		 
		return null;
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map map = new HashMap();
		map.put("OBJ_TYPE", "SVC");
		return map;
	}

}
package com.clovirsm.service.monitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;
import com.clovirsm.sys.hv.executor.ListAlarm;
import com.clovirsm.sys.hv.executor.ListEvent;
import com.clovirsm.sys.hv.executor.ListPerfHistory;
import com.fliconz.fm.security.UserVO;

@Service
public class MonitorService extends NCDefaultService {

	@Autowired ListPerfHistory listPerfHistory;
	@Autowired ListEvent listEvent;

	@Autowired ListAlarm listAlarm;
	@Autowired SqlSessionTemplate sqlSession;
	protected String getNameSpace() {
		return "com.clovirsm.monitor";
	}

	public String[] getPerfHistoryTypes(String hvCd, String objType) throws Exception {
		return listPerfHistory.getPerfHistoryTypes(hvCd, objType);
	}
	public Map getEventList(String dcId, String objType, String name) throws Exception {
		return listEvent.run(dcId, objType,  name);
	}
	public Map getAlarmList(String dcId, String objType, String name) throws Exception {
		return listAlarm.run(dcId, objType,  name);
	}
	public Map getPerfHistoryList(String period, String dcId, String objType, String name, Map searchParam) throws Exception {
		return listPerfHistory.run(period, dcId, objType,  name, searchParam);
	}
	public List getNoticeList() throws Exception{
		
		List noticeIDs = new ArrayList();
		Map param = new HashMap();
		param.put("_IS_ADMIN_", isAdmin() ? "Y" : "N");
		if(!isAdmin()) {
			param.put("_TEAM_CD_", UserVO.getUser().getTeam().getTeamCd());
			param.put("_COMP_ID_", UserVO.getUser().getTeam().getCompId());
		}
		noticeIDs = sqlSession.selectList(getNameSpace()+".list_FM_NOTICE",param);
		System.out.println("noticeIDs"+noticeIDs);
		return noticeIDs;
	}
	
	public List getNoticeList(Map searchParam) throws Exception{
		
		List noticeIDs = new ArrayList();
		searchParam.put("_IS_ADMIN_", isAdmin() ? "Y" : "N");
		if(!isAdmin()) {
			searchParam.put("_TEAM_CD_", UserVO.getUser().getTeam().getTeamCd());
			searchParam.put("_COMP_ID_", UserVO.getUser().getTeam().getCompId());
		}
		noticeIDs = sqlSession.selectList(getNameSpace()+".list_FM_NOTICE",searchParam);
		System.out.println("noticeIDs"+noticeIDs);
		return noticeIDs;
	}
	
	@Override
	public String[] getPks() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map c = new HashMap();
		c.put("TASK_CD", "TASK_CD");
		c.put("READ_YN", "sys.yn");
		return c;
	}

	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return null;
	}



}
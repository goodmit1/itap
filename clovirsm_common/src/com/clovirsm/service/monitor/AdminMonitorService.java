package com.clovirsm.service.monitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.IPAddressUtil;
import com.clovirsm.sys.hv.executor.ListDSAttribute;
import com.clovirsm.sys.hv.executor.ListHostAttribute;
import com.clovirsm.sys.hv.executor.ListVMPerfInDC;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.mvc.DefaultService;

@Service
public class AdminMonitorService extends DefaultService {


	@Autowired transient ListHostAttribute listHostAttribute;

	@Autowired transient ListDSAttribute listDSAttribute;

	@Autowired transient ListVMPerfInDC listVMAttribute;
	protected String getNameSpace() {
		return "com.clovirsm.admin.monitor";
	}

	@Override
	public List selectList(String tableNm, Map param) throws Exception {
		if(tableNm.equals("DS"))
		{
			if((String)param.get("DC_ID") == null)
			{
				return new ArrayList();
			}
			return selectListByQueryKey("list_NC_MONITOR_DS", param);
			//return listDSAttribute.list((String)param.get("DC_ID"), param);
		}
		else if(tableNm.equals("HOST"))
		{
			if((String)param.get("DC_ID") == null)
			{
				return new ArrayList();
			}
			return selectListByQueryKey("com.clovirsm.monitor", "list_NC_MONITOR_HOST", param);
			//return listHostAttribute.list((String)param.get("DC_ID"), param);
		}
		else if(tableNm.equals("VM"))
		{
			System.out.println(param);
			if((String)param.get("DC_ID") == null)
			{
				return new ArrayList();
			}
			return listVMAttribute.list((String)param.get("DC_ID"), param);
		}
		else if(tableNm.equals("IP"))
		{
			return  getIpList(param);
		}
		return super.selectList(tableNm, param);
	}
	private void addIPMap(Map<Object, Map> ipMap, Map m, Map codeConfig) throws Exception
	{
		super.addCodeNames(codeConfig, m);
		Map m1 = ipMap.get(m.get("PRIVATE_IP"));
		if(m1==null)
		{
			ipMap.put(m.get("PRIVATE_IP"), m);
		}
		else
		{
			String[] fieldArr = {"VM_NM", "TEAM_NM", "P_KUBUN_NM", "PURPOSE_NM", "INS_ID_NM", "GUEST_NM"};
			for(int i=0; i < fieldArr.length; i++)
			{
				String f = fieldArr[i];
				m1.put(f, TextHelper.nvl((String) m1.get(f),"") + "," + TextHelper.nvl((String)m.get(f),""));
			}
			ipMap.put(m.get("PRIVATE_IP"), m1);
		}
	}
	private List getIpList(Map param) throws Exception
	{
		long start_ipNum = 0;
		long end_ipNum = 0;
		try
		{
			start_ipNum = IPAddressUtil.ipToLong((String)param.get("START_IP"));
			param.put("START_IP_NUM",start_ipNum);
		}
		catch(Exception ignore){}
		try
		{
			end_ipNum = IPAddressUtil.ipToLong((String)param.get("END_IP"));
			param.put("END_IP_NUM", end_ipNum);
		}
		catch(Exception ignore){}
		List<Map> list = super.selectList("IP", param);
		if(start_ipNum== 0)
		{
			start_ipNum = IPAddressUtil.ipToLong((String)list.get(0).get("PRIVATE_IP"));
		}
		if(end_ipNum== 0)
		{
			end_ipNum = IPAddressUtil.ipToLong((String)list.get(list.size()-1).get("PRIVATE_IP"));
		}
		Map<Object, Map> ipMap = new HashMap();
		Map codeConfig = new HashMap();
		codeConfig.put("P_KUBUN", "P_KUBUN");
		codeConfig.put("PURPOSE", "PURPOSE");
		for(Map m : list)
		{
			addIPMap(ipMap, m, codeConfig);
		}
		List result = new ArrayList();
		for(long i=start_ipNum; i <= end_ipNum ; i++)
		{
			if(!IPAddressUtil.isAvailableIp(i)) continue;
			String ip = IPAddressUtil.longToIp(i);
			Map m = new HashMap();
			m.put("IP", ip);
			if(ipMap.get(ip)!=null)
			{
				m.putAll(ipMap.get(ip));
			}
			result.add(m);

		}
		return result;
	}
	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getPks() {
		// TODO Auto-generated method stub
		return null;
	}

}
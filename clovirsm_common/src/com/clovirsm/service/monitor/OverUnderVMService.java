package com.clovirsm.service.monitor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;
import com.clovirsm.sys.hv.vmware.VROpsAction;

@Service
public class OverUnderVMService  extends NCDefaultService {

	@Autowired VROpsAction action;
	
	@Override
	protected String getTableName() {
		 
		return "NC_OVERUNDER_VM";
	}

	@Override
	public String[] getPks() {
		 
		return new String[] {"VM_HV_ID", "VM_NM"};
	}

	@Override
	protected String getNameSpace() {
		return "com.clovirsm.overunderVM";
	}

	public void insertFromVROps() throws Exception{
		Map param = new HashMap();
		this.delete(param);
		List<Map> list = action.getOverUnderSize();
		for(Map m:list) {
			this.insert(m);
		}
		this.updateByQueryKey("update_VM_ID", param);
	}
}

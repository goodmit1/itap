package com.clovirsm.service.admin;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.clovirsm.common.NCDefaultService;
import com.clovirsm.service.resource.IPService;

@Service
public class PortgroupService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.admin.portgroup";
	}

	@Override
	protected String getTableName() {
		return "NC_DC_KUBUN_CONF";
	}

	@Override
	public String[] getPks() {
		return new String[]{"DC_ID", "ID", "KUBUN"};
	}

	@Autowired IPService ipService;
	private void fillIpInfo(Map param) throws Exception
	{
		if(param.get("ALL_YN") == null)
		{
			ipService.insertAllIp((String)param.get("ID"), (String)param.get("VAL1"));
		}
	}
	public int insert(Map param) throws Exception
	{
		
		if(param.get("VAL1") == null || "".equals(param.get("VAL1"))) return 0;
		if(param.get("KUBUN") == null)
		{
			param.put("KUBUN", "IP");
		}
		int row = super.insert(param);
		 
		fillIpInfo(param);
		return row;
	}
	public int update(Map param) throws Exception
	{
		if(param.get("KUBUN") == null)
		{
			param.put("KUBUN", "IP");
		}
		if(param.get("VAL1") == null || "".equals(param.get("VAL1"))) 
		{
			return this.delete(param);
		}
		int row = super.update(param);
		fillIpInfo(param);
		return row;
	}
	 

}
package com.clovirsm.service.admin;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fliconz.fw.runtime.util.DateUtil;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.clovirsm.common.NCDefaultService;
import com.clovirsm.common.NCReqService;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.service.ComponentService;
import com.clovirsm.service.batch.BatchService;
import com.clovirsm.service.resource.VMService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;

/**
 * 미사용 VM 목록
 * @author user01
 *
 */
@Service
public class NotUseVMService extends NCDefaultService {

	 
	@Autowired DCService dcService;
	protected String getNameSpace() {
		return "com.clovirsm.admin.NotUseVM";
	}

	@Override
	public List selectList(String tableNm, Map param) throws Exception {
		param.put("max_not_use_day", ComponentService.getEnv("use_vm.max_not_use_day", 15));
		return super.selectList(tableNm, param);
	}
	public List listMinUse(Map param) throws Exception {
		 Map param1 = new HashMap();
		 param1.put("DC_ID", param.get("DC_ID"));
		 int day = NumberUtil.getInt(param.get("DAY"));
		 param1.put("DAY", -1*(day));
		 List<Map> list = selectList("NC_VM", param1);
		 List result = new ArrayList();
		 DC dcInfo = dcService.getDC((String)param.get("DC_ID"));
		
		 float cpu = NumberUtil.getFloat(param.get("CPU"));
		 float memory = NumberUtil.getFloat(param.get("MEM"));
		 float net = NumberUtil.getFloat(param.get("NET"));
		 boolean isOr = "OR".equals(param.get("ANDOR"));
		 Date fromDate = DateUtil.addDate(new Date(), Calendar.DATE, -1*(day+1));
		 
		 Date toDate =  new Date();
		 Map<String, Map> vmMap = new HashMap();
		 for(Map m:list) {
			 vmMap.put((String)m.get("VM_HV_ID"), m);
		 }
		 
		 param1.put("START_DT", fromDate);
		 param1.put("FINISH_DT", toDate);
		 param1.putAll(dcInfo.getProp());
		 Map<String, Map> perfMap = dcInfo.getAPI().listAllVMMaxPerf(param1);
		 for(String k : perfMap.keySet()) {
			 	Map m = perfMap.get(k);
			 	if(isTarget(m , cpu, memory, net, isOr )) {
			 		Map vmInfo = vmMap.get(k);
			 		if(vmInfo != null) {
			 			vmInfo.putAll(m);
			 			result.add(vmInfo);
			 		}
			 	}
		 }
			 
		 
		 return result;
	}
	public List listMinUse1(Map param) throws Exception {
		 Map param1 = new HashMap();
		 param1.put("DC_ID", param.get("DC_ID"));
		 VMService vmService = (VMService) NCReqService.getService("S");
		 
		 List<Map> list = vmService.list(param1);
		 List result = new ArrayList();
		 DC dcInfo = dcService.getDC((String)param.get("DC_ID"));
		 int day = NumberUtil.getInt(param.get("DAY"));
		 float cpu = NumberUtil.getFloat(param.get("CPU"));
		 float memory = NumberUtil.getFloat(param.get("MEM"));
		 float net = NumberUtil.getFloat(param.get("NET"));
		 boolean isOr = "OR".equals(param.get("ANDOR"));
		 Date fromDate = DateUtil.addDate(new Date(), Calendar.DATE, -1*day);
		 Date toDate =  new Date();
		 for(Map m:list) {
			 param1.put("START_DT", fromDate);
			 param1.put("FINISH_DT", toDate);
			 param1.putAll(dcInfo.getProp());
			 param1.put(com.clovirsm.hv.HypervisorAPI.PARAM_OBJECT_TYPE, "VirtualMachine");
			 param1.put(com.clovirsm.hv.HypervisorAPI.PARAM_OBJECT_NM, m.get("VM_NM")); //"GIT-Mobis-Portal");
			 param1.put(com.clovirsm.hv.HypervisorAPI.PARAM_VM_NM, m.get("VM_NM")); //"GIT-Mobis-Portal");
			 try
			 {
				 Map info = dcInfo.getAPI().getPerf("", param1);
				 if(isTarget((Map)info.get("LIST"), cpu, memory, net, isOr )) {
					 m.putAll((Map)info.get("LIST"));
					 result.add(m);
				 }
			 }
			 catch(Exception e) {
				 System.out.println(e.getMessage());
			 }
		 }
		 return result;
	}
	private boolean isTarget(Map info, float cpu, float memory, float net, boolean isOr) {
		float cpu1 = (float) (info.get("cpu_usage_max")==null ? 0.0 : NumberUtil.getFloat(info.get("cpu_usage_max") ));
		float mem1 = (float) (info.get("mem_usage_max")==null ? 0.0 : NumberUtil.getFloat(info.get("mem_usage_max") ));
		float net1 = (float) (info.get("net_usage_max")==null ? 0.0 : NumberUtil.getFloat(info.get("net_usage_max") ));
		if(isOr) {
			return cpu1<=cpu || mem1<=memory || net1<=net;
		} else {
			return cpu1<=cpu && mem1<=memory && net1<=net;
		}
	}
	@Override
	protected String getTableName() {

		return "NOT_USE_VM";
	}

	@Override
	public String[] getPks() {
		 return new String[]{"VM_ID"};
	}


}
package com.clovirsm.service.admin;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.ClassHelper;
import com.clovirsm.common.NCDefaultService;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.vmware.BeforeAfterVMWare;
import com.clovirsm.sys.hv.vra.VRAListBaseImage;
import com.clovirsm.sys.hv.vra.VRAListCatalog;
import com.clovirsm.sys.hv.vra.VRAListClusterTag;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Service
public class VraCatalogMngService  extends NCDefaultService {

	@Autowired VRAListCatalog listCatalog;
	 
	@Override
	protected String getNameSpace() {
		
		return "com.clovirsm.admin.vra.CatalogMng";
		
	}

	private void putDCIds(DCService service, Map m) throws Exception {
		List<String[]> list = (List)m.get("DC_IPS");
		if(list==null) return;
		StringBuffer dcIds = new StringBuffer();
		int i=0;
		for(String[] ip:list)	{
			m.put("DC_IP", ip[0]);
			m.put("REAL_DC_NM", ip[1]);
			DC dc = service.getDCByIp(m);
			if(i>0 ) dcIds.append(",");
			if(dc != null)	{
				dcIds.append(dc.getProp("DC_ID"));
			}
			else {
				System.out.println("############################ VRA vCenter NOT FOUND ");
				System.out.println(ip[0] + "/" + ip[1]);
				System.out.println("######################################################## ");
			}
			
			i++;
			 
			
		}
		m.put("DC_ID", dcIds.toString());
	
	}
	protected void mappingCategory(Map m  ) {
		try {
			
			JSONObject json = new JSONObject((String)m.get("FORM_INFO"));
			String svcDefault = json.getJSONObject("inputs").getJSONObject("_svc").getString("default");
			String[] svcs = svcDefault.split(",");
			categoryMapService.insertByCode((String)m.get("CATALOG_ID"), "C", svcs );
				 
			
		}
		catch(Exception ignore) {
			System.out.println(ignore.getMessage());
		}
	}
	
	@Autowired com.clovirsm.sys.hv.vra.VRAListIPRange collectVraIp;
	@Autowired
	VRAListBaseImage listBaseImage;
	
	@Autowired
	VRAListClusterTag clusterTag;
	
	
	@Autowired OsTypeMngService osService;
	public void insertVraEtcCollect() throws Exception {
		Map param = new HashMap();
		 
		 
		//Map result = listBaseImage.runAll(param);
		//osService.insertAll(param, (List)result.get(HypervisorAPI.PARAM_LIST));
		Map result = clusterTag.runAll(param);
		clusterTag.insertAll(result);
		
		//collectVraIp.runAll(param);
		
		//VmSpecMngService spec = (VmSpecMngService)SpringBeanUtil.getBean("vmSpecMngService");
		//spec.insertCollect();
	}
	public void insertCollect() throws Exception{
		Map param = new HashMap();
		 
		Map result = listCatalog.runAll(param);
		this.updateByQueryKey("update_NC_VRA_CATALOG_DEL", param);
		List<Map> list = (List)result.get(HypervisorAPI.PARAM_LIST);
		for(Map m : list){
			m.put("DEL_YN", "N");
			putDCIds(listCatalog.getDCService(), m);
			
			int row = this.updateByQueryKey("update_NC_VRA_CATALOG", m);
			if(row==0){
				row = this.updateByQueryKey("insert_NC_VRA_CATALOG", m);
			}
			else {
				this.updateByQueryKey("delete_NC_VRA_CATALOG_STEP", m);
			}
			Collection<Map> steps = (Collection) m.get("STEP_INFO");
			if(steps != null) {
				int idx=1;
				for(Map step:steps) {
					for(Object k : step.keySet()) {
						step.put(k, step.get(k).toString());
					}
					step.put("STEP_SEQ", idx++);
					step.put("CATALOG_ID", m.get("CATALOG_ID"));
					
					this.updateByQueryKey("insert_NC_VRA_CATALOG_STEP", step);
				}
			}
			mappingCategory(m);
		}
		ClassHelper.getSite().onAfterCollect("NC_VRA_CATALOG", result);
		insertVraEtcCollect();
	}

	@Override
	protected String getTableName() {
		 
		return "NC_VRA_CATALOG";
	}

	@Override
	public String[] getPks() {
		return new String[]{"CATALOG_ID"};
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("ALL_USE_YN", "sys.yn");
		return config;
	}

	
	@Autowired CategoryMapService categoryMapService;
	
	@Override
	public int update(Map param) throws Exception {
		 
		int row =  super.update(param);
		String iconYN = (String) param.get("ICON_YN");
		if(iconYN == null)
			iconYN = "N";
		if(iconYN == "N") {
		this.deleteByQueryKey("delete_NC_VRA_CATALOG_TEAM", param);
			if(param.get("TEAM_CD") != null && param.get("TEAM_CD") instanceof String[]) {	
				String[] teamList = (String[])param.get("TEAM_CD");
				if(teamList != null){
					for(String team:teamList) {
						param.put("TEAM_CD", team);
						this.insertByQueryKey("insert_NC_VRA_CATALOG_TEAM", param);
					}
				}
			
		}
		}
		
		categoryMapService.insert((String)param.get("CATALOG_ID"), param.get("CATEGORY_IDS"), "C", iconYN);
		
		return row; 
	}

	@Override
	public int delete(Map param) throws Exception {
		int row = super.delete(param);
		this.deleteByQueryKey("delete_NC_VRA_CATALOG_TEAM", param);
		return row;
	}

	@Override
	public Map getInfo(Map param) throws Exception {
		Map info = super.getInfo(param);
		info.put("TEAM_CD", this.selectList("NC_VRA_CATALOG_TEAM", param));
		return info;
	}
}

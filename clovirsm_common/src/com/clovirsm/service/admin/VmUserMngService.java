package com.clovirsm.service.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCDefaultService;
import com.fliconz.fw.runtime.util.ListUtil;

@Service
public class VmUserMngService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.admin.vmuser";
	}
	/**
	 * 1.작업완료 여부 변경
	 * 2.변경된 정보 사용자에게 메일 발송
	 *
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public int updateApproval( Map param) throws Exception {
		if (NCConstant.TASK_STATUS_CD.S.toString().equals(param.get("TASK_STATUS_CD"))) {
			param.put("FAIL_MSG", "");
		}
		int row =  this.updateByQueryKey("updateApprovalByAdmin", param);

		return row;
	}

	public int updateApprovalList( Map param) throws Exception {
		int row = 0;
		List<Map> list = ListUtil.makeJson2List((String)param.get("selected_json"));
		for(Map tmp : list){
			row += updateApproval(tmp);
		}
		return row;
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map conf = new HashMap();
		conf.put("TASK_STATUS_CD", "TASK_STATUS_CD");
		return conf;
	}

	@Override
	protected String getTableName() {

		return "NC_VM_USER";
	}
	@Override
	public String[] getPks() {
		 return new String[]{"VM_ID","USER_ID","USER_IP"};
	}
}
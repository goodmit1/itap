package com.clovirsm.service.admin;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.vra.VRAListClusterTag;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class LicenseELAHelper {

	public void insertProductCnt(String yyyymm) throws Exception {
		LicenseService service = (LicenseService)SpringBeanUtil.getBean("licenseService");
		 
		Map param = new HashMap();
		List<Map> dcList = service.selectListByQueryKey("list_NC_DC", param);
		Map result = new HashMap();
		result.put("YYYYMM", yyyymm);
		result.put("VCENTER", dcList.size());
		result.put("VSAN", 0);
		result.put("NSX", 0);
		result.put("VRA", 0);
		result.put("SDDC", 0);
		result.put("VRLI", 0);
		result.put("VRNI", 0);
		result.put("VROPS", 0);
		List<Map> list = service.selectListByQueryKey("com.clovirsm.admin.etcConnMng", "select_count_by_type", param);
		for(Map m: list) {
			result.put(m.get("CONN_TYPE"), m.get("CNT"));
			
		}
		
		service.deleteByQueryKey("delete_NC_LICENSE_ELA_PRD_CNT", result);
		service.insertByQueryKey("insert_NC_LICENSE_ELA_PRD_CNT", result);
		// insert into NC_LICENSE_ELA_PRD_CNT
	}
	public void insertCPUCnt(String yyyymm) throws Exception {
		LicenseService service = (LicenseService)SpringBeanUtil.getBean("licenseService");
		DCService dcService  = (DCService)SpringBeanUtil.getBean("DCService");
	
		Map param = new HashMap();
		param.put("YYYYMM", yyyymm);
		List<Map> list = service.selectListByQueryKey("list_NC_DC", param);
		Map<Object, Map> result = new HashMap();
		for(Map m : list) {
			m.put("YYYYMM", yyyymm);
			m.put("VSAN", 0);
			m.put("NSX", 0);
			m.put("VRA", 0);
			m.put("SDDC", 0);
			m.put("VRLI", 0);
			m.put("VRNI", 0);
			m.put("VROPS", 0);
			result.put(m.get("DC_ID"), m);
		}
		
		param.put("LICENSE_NAME", " VSAN ");
		List<Map> vsanList = service.selectListByQueryKey("select_used_by_name", param);
		for(Map m : vsanList) {
			Map dc = result.get(m.get("DC_ID"));
			if(dc != null) {
				dc.put("VSAN", m.get("CPU"));
			}
		}
		addVraCPU(service, dcService, result);
		addNsxCPU(service, dcService, result);
		addSDDCCPU(service, dcService, result);
		addVROpsCPU(service, dcService, result);
		addVRLICPU(service, dcService, result);
		addVRNICPU(service, dcService, result);
		
		service.deleteByQueryKey("delete_NC_LICENSE_ELA_CPU_CNT", param);
		Collection<Map> values = result.values();
		for(Map param1 : values ) {
			service.insertByQueryKey("insert_NC_LICENSE_ELA_CPU_CNT", param1);
		}
	}
	private void getHostCPU(LicenseService service, Map result, Object dcId, String prod, Object name, Object connUrl) throws Exception {
		Map param = new HashMap();
		param.put("DC_ID", dcId);
		param.put("NAME", name);
		Map info =  service.selectOneByQueryKey("select_cpu_by_cluster_host", param);
		if(info != null && !info.isEmpty()) {
			putHostCPU(result, info.get("DC_ID"), prod, NumberUtil.getInt(info.get("CPU")) );
		}
		else {
			System.out.println("Not Found : " + name + " in " + prod  + "." + connUrl);
		}
	}
	private void putHostCPU(Map result, Object dcId, String prod,  int cnt) throws Exception {
		Map info = (Map) result.get(dcId);
		if(info != null) {
			Object o = info.get(prod);
			if(o == null) {
				info.put(prod, cnt);
			}
			else {
				info.put(prod, NumberUtil.getInt(o) + cnt);
			}
		}
	}
	private void addVraCPU(LicenseService licenseService, DCService dcService, Map result)  {
		try {
			VRAListClusterTag clusterTag = (VRAListClusterTag)SpringBeanUtil.getBean("VRAListClusterTag");
			Map param = new HashMap();
			Map info = clusterTag.runAll(param);
			List<Map> vraList = (List)info.get("LIST");
			for(Map m : vraList) {
				DC dc = dcService.getDCByIp(m);
				getHostCPU(licenseService, result, dc.getProp("DC_ID"),  "VRA",  (String)m.get("NAME"), param.get("CONN_URL"));
				 
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
			
	}
	private void addNsxCPU(LicenseService licenseService, DCService dcService, Map result)   {
		VMWareAPI vmwareApi = new VMWareAPI();	 
		try {
			List<Map> connList = dcService.getEtcConnInfo("NSX");
			for(Map param : connList) {
				Map data = vmwareApi.getNSX().listHostNode(param);
				List<String> data1 = (List)data.get("LIST");
				for(String s:data1) {
					getHostCPU(licenseService, result, null,  "NSX",  s, param.get("CONN_URL"));
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	private void addSDDCCPU(LicenseService licenseService, DCService dcService, Map result)  {
		VMWareAPI vmwareApi = new VMWareAPI();	 
		try {
			List<Map> connList =  dcService.getEtcConnInfo("SDDC");
			for(Map param : connList) {
				Map data = vmwareApi.getSDDC().listHostNode(param);
				List<String> data1 = (List)data.get("LIST");
				for(String s:data1) {
					getHostCPU(licenseService, result, null,  "SDDC",  s, param.get("CONN_URL"));
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	private void addVRNICPU(LicenseService licenseService, DCService dcService, Map result)  {
		VMWareAPI vmwareApi = new VMWareAPI();	 
		try {
			List<Map> connList =  dcService.getEtcConnInfo("VRNI");
			for(Map param : connList) {
				Map data = vmwareApi.getVNI().listHostNode(param);
				List<String> data1 = (List)data.get("LIST");
				for(String s:data1) {
					getHostCPU(licenseService, result, null,  "VRNI",  s, param.get("CONN_URL"));
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	private void addVROpsCPU(LicenseService licenseService, DCService dcService, Map result)  {
		VMWareAPI vmwareApi = new VMWareAPI();	 
		try {
			List<Map> connList =  dcService.getEtcConnInfo("VROPS");
			for(Map param : connList) {
				Map data = vmwareApi.getVROps().listHostNode(param);
				List<String> data1 = (List)data.get("LIST");
				for(String s:data1) {
					getHostCPU(licenseService, result, null,  "VROPS",  s, param.get("CONN_URL"));
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	private void addVRLICPU(LicenseService licenseService, DCService dcService, Map result)  {
		VMWareAPI vmwareApi = new VMWareAPI();	 
		try {
			List<Map> connList =  dcService.getEtcConnInfo("VRLI");
			for(Map param : connList) {
				Map data = vmwareApi.getVRLI().listHostNode(param);
				List<String> data1 = (List)data.get("LIST");
				for(String s:data1) {
					getHostCPU(licenseService, result, null,  "VRLI",  s, param.get("CONN_URL"));
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}

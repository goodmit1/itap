package com.clovirsm.service.admin;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.clovirsm.common.IDGenHelper;
import com.clovirsm.common.NCDefaultService;

@Service
public class NasMngService extends NCDefaultService {
	
	
	@Override
	protected String getNameSpace() {
		// TODO Auto-generated method stub
		return "com.clovirsm.admin.NasMng";
	}

	@Override
	public String[] getPks() {
		// TODO Auto-generated method stub
		return new String[]{"NAS_IP","NAS_PATH"};
	}
	

	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return "NC_NAS";
	}
	
	/*@Override
	protected int insertDBTable(String tableNm, Map param) throws Exception {
		param.put("NAS_IP", IDGenHelper.getId(""));
		return super.insertDBTable(tableNm, param);
	}*/


}

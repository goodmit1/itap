package com.clovirsm.service.admin;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;

@Service
public class MmPayService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.admin.mmpay.NcMmPay";
	}

	@Override
	protected String getTableName() {

		return "NC_MM_STAT";
	}

	@Override
	public String[] getPks() {
		return null;
	}

}
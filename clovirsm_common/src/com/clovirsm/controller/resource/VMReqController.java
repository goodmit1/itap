package com.clovirsm.controller.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.common.IDGenHelper;
import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCException;
import com.clovirsm.common.NCReqService;
import com.clovirsm.common.RequiredCheckUtil;
import com.clovirsm.controller.ReqController;
import com.clovirsm.service.resource.VMService;
import com.clovirsm.util.JSONUtil;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@RestController
@RequestMapping(value =  "/vmReq" )
public class VMReqController extends ReqController{

	VMService service;
	@Override
	protected VMService getService() {
		if(service == null) {
			try {
				service = (VMService) NCReqService.getService("S");
			} catch (Exception e) {
				service = (VMService)SpringBeanUtil.getBean("VMService");
			}
		}
		return service;
	}

	@RequestMapping(value =  "/querykey/{key}" )
	public Object getListByQueryKey(final HttpServletRequest request, HttpServletResponse response, @PathVariable("key") final String key) {
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				List list = getService().selectListByQueryKey(key, param);
				Map tmp = new HashMap();
				tmp.put("list", list);
				return tmp;
			}
		}).run(request);

	}

	@RequestMapping(value =  "/oslist" )
	public Object getOsList(final HttpServletRequest request, HttpServletResponse response) {
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				if(param.get("CATEGORY_ID") != null && !"".equals(param.get("CATEGORY_ID"))){
					List<String> categoryList = service.selectListByQueryKey("selectChildCategory", param);
					if(categoryList != null && categoryList.size() > 0){
						param.put("CATEGORY_LIST", categoryList);
					}
				}
				List list = service.selectListByQueryKey("list_OS_TYPE", param);
				service.addCodeNames(list);
				Map tmp = new HashMap();
				tmp.put("list", list);
				return tmp;
			}
		}).run(request);

	}

	@RequestMapping(value =  "/initFormValue" )
	public Object initFormValue(final HttpServletRequest request, HttpServletResponse response){
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				Map tmp = getService().selectOneByQueryKey("getVmSpecByCpu", param);
				tmp.put("ADMIN_YN", UserVO.getUser().isSuperAdmin() ? 'Y' : 'N');
				param.putAll(tmp);
				return param;
			}
		}).run(request);
	}

	/**
	 * 요청, DEPLOY_REQ_YN=Y이면 바로 요청
	 * @throws Exception
	 */
	@RequestMapping(value =  "/insertReq" )
	public Object insertReq_request(HttpServletRequest request) throws Exception {
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				boolean isDeploy = "Y".equals(param.get("DEPLOY_REQ_YN"));
				param.put("CUD_CD", NCConstant.CUD_CD.C.toString());
				param.put("APPR_STATUS_CD", NCConstant.APP_KIND.W.toString());
				String[] result = RequiredCheckUtil.requiredCheck(param, new String[]{"VM_NM", "CPU_CNT", "DISK_SIZE", "RAM_SIZE"});
				if(result != null){
					System.out.println(result[0]);
					throw new NCException(NCException.NOT_FOUND, MsgUtil.getMsg("NC_VM_" + result[0], null));
				}
				if(service.selectCountByQueryKey("list_count_NC_VM_NAME", param) > 0){
					throw new NCException(NCException.DUPLICATE_NAME, (String)param.get("VM_NM"));
				}
				List vmFwReqList = null;
				List vmDiskReqList = null;
				if(param.get("NC_VM_FW_LIST") != null){
					vmFwReqList = new ArrayList<Map<String, Object>>(JSONUtil.createJSONArray((String)param.get("NC_VM_FW_LIST")));
				}
				if(param.get("NC_VM_DATA_DISK") != null){
					vmDiskReqList = new ArrayList<Map<String, Object>>(JSONUtil.createJSONArray((String)param.get("NC_VM_DATA_DISK")));
				}
				service.insertVMReq(param, vmFwReqList,vmDiskReqList, isDeploy);
				return ControllerUtil.getSuccessMap(param,  "INS_DT");

			}

		}).run(request);
	}


}

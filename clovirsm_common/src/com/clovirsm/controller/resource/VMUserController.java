package com.clovirsm.controller.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.common.NCReqService;
import com.clovirsm.controller.ReqController;
import com.clovirsm.service.resource.VMService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@RestController
@RequestMapping(value =  "/vmuser" )
public class VMUserController extends ReqController{

	VMService service;
	@Override
	protected VMService getService() {
		if(service == null) {
			try {
				service = (VMService) NCReqService.getService("S");
			} catch (Exception e) {
				service = (VMService)SpringBeanUtil.getBean("VMService");
			}
		}
		return service;
	}

	@RequestMapping(value =  "/list" )
	public Object list(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				Map result = new HashMap();
				List list = getService().vmUserList(param);
				result.put("list", list);
				return result;
			}

		}).run(request);

	}

}

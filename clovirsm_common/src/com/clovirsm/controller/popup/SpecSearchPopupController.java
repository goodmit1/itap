package com.clovirsm.controller.popup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.popup.SpecSearchPopupService;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;

@RestController
@RequestMapping(value =  "/popup/specSearch" )
public class SpecSearchPopupController extends DefaultController{

	@Autowired SpecSearchPopupService service;
	@Override
	protected DefaultService getService() {
		return service;
	}

}

package com.clovirsm.controller.admin;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.ListUtil;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.NasMngService;
import com.fliconz.fm.admin.service.UserService;
import com.fliconz.fm.mvc.DefaultController;

@RestController
@RequestMapping(value =  "/nas_mng" )
public class NasMngController extends DefaultController{

	@Autowired NasMngService service;
	@Override
	protected NasMngService getService() {
		 
		return service;
	}
	
	@RequestMapping(value =  "/test/pwdreset" )
	public String pwdReset() throws Exception {
		Map<String, Object> param = new HashMap<String, Object>();
		UserService userService = new UserService();
		List<Map<String, Object>> list= service.selectList("pwd_null", param);
		System.out.println(list);
		JSONArray jsonArray = new JSONArray();
		for(Map<String,Object> map :list) {
			JSONObject jsonObject = new JSONObject();
			System.out.println(map);
			for(Map.Entry<String, Object> entry : map.entrySet()) {
				try {
					jsonObject.put(entry.getKey(), entry.getValue());
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
			jsonArray.add(jsonObject);
		}
		return jsonArray.toString();
	}

	@RequestMapping(value = "/save")
	public Object save(HttpServletRequest request, HttpServletResponse response) {
		return  (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				String nasId = param.get("NAS_ID").toString();
				if(nasId.equals("")){
					nasId = "N" +  System.currentTimeMillis();
					param.put("NAS_ID", nasId);
					service.insert(param);
				}
				service.update(param);
				return ControllerUtil.getSuccessMap(param, service.getPks());
			}
		}).run(request);
	}
}



package com.clovirsm.controller.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.OsTypeMngService;
import com.clovirsm.service.popup.CategorySearchPopupService;
import com.clovirsm.sys.hv.executor.CheckTemplatePath;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.ListUtil;
import com.fliconz.fw.runtime.util.NumberUtil;

@RestController
@RequestMapping(value =  "/ostype" )
public class OsTypeMngController extends DefaultController {

	@Autowired OsTypeMngService service;
	
	@Autowired CategorySearchPopupService categoryService;
	
	 
	@Override
	protected OsTypeMngService getService() {
		return service;
	}

	@RequestMapping(value =  "/delete_category" )
	public Object deleteCategory(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				categoryService.delete(param);			 
				return ControllerUtil.getSuccessMap(param);
		}}).run(request);

	}
				
	@RequestMapping(value =  "/save_category" )
	public Object insertCategory(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				
				String name = (String) param.get("CATEGORY_NM");
				String[] names = name.split(">");
				Map orgParentInfo = null;
				for(int i=0; i < names.length; i++)
				{
					if(names[i].trim().equals("")) continue;
					param.put("CATEGORY_NM", names[i].trim());
					if(param.get("CATEGORY_ID") == null)
					{
						if(i==0)
						{
							orgParentInfo = categoryService.chkParentCategory(param.get("PAR_CATEGORY_ID"));
						}
						if(i==names.length-1 && orgParentInfo != null)
						{
							param.put("CATEGORY_CODE", orgParentInfo.get("CATEGORY_CODE"));
							param.put("OLD_CATEGORY_ID", orgParentInfo.get("CATEGORY_ID")); // leaf에 자식이 생긴 경우 
						}
						categoryService.insert(param);
						if(param.get("OLD_CATEGORY_ID") != null)
						{
							service.chgCategory(param.get("OLD_CATEGORY_ID"), param.get("CATEGORY_ID")); // leaf에 자식이 생긴 경우 자식에게 템플릿 넘김.
						}
					}
					else
					{
						categoryService.update( param);
					}
					
					param.put("PAR_CATEGORY_ID", param.get("CATEGORY_ID"));
					param.remove("CATEGORY_ID");
					
				}
				return ControllerUtil.getSuccessMap(param);
			}}).run(request);

	}
			
	 

	@RequestMapping(value =  "/list_include_category" )
	public Object list_include_category(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				Map result = new HashMap();
				int pageSize = NumberUtil.getInt(param.get("pageSize"),0) ;
				if(pageSize != 0)
				{
					param.put("pageSize", pageSize);
					param.put("first", pageSize * (NumberUtil.getInt(param.get("pageNo"),0)-1) );
				}
				List<Map> list = getService().selectListByQueryKey("list_IMG_NC_DC_OS_TYPE", param);
				int maxLevel = 0;
				for(Map m : list)
				{
					String names = (String)m.get("NAMES");
					String[] nameArr = names.split(">");
					String[] idArr = ((String)m.get("IDS")).split(",");
					for(int i=0; i< nameArr.length; i++)
					{
						m.put("NAME_" + i, nameArr[i]);
						m.put("ID_" + i, idArr[i]);
					}
					if(nameArr.length>maxLevel)
					{
						maxLevel = nameArr.length;
					}
				}
				result.put("list", list);
				result.put("maxLevel", maxLevel);
				if(pageSize  != 0)
				{
					result.put("total", getService().list_count(param));
				}
				else
				{
					result.put("total", list.size());
				}
				return result;
			}

		}).run(request);

	}
	@RequestMapping(value =  "/save_mapping" )
	public Object saveMapping(final HttpServletRequest request, HttpServletResponse response) {
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				List<Map<String, Object>> list = ListUtil.makeJson2List((String)param.get("LIST"));
				List<String> delList = new ArrayList<String>();
				for(Map<String, Object> loop : list){
					loop.put("OS_ID", param.get("OS_ID"));
					String tmplPath = loop.containsKey("TMPL_PATH") ? String.valueOf(loop.get("TMPL_PATH")) : "";
					if(tmplPath != null && !"".equals(tmplPath.trim())){
						
						service.insertOrUpdate4DC(loop);
					}else{
						delList.add(String.valueOf(loop.get("DC_ID")));
					}
				}
				param.put("DEL_LIST", delList);
				if(delList.size() != 0) {
					service.deleteByQueryKey("delete_NC_DC_OS_TYPE", param);
				}
				return new HashMap();
			}
		}).run(request);
	}
	
	@RequestMapping(value =  "/querykey/{key}" )
	public Object getListByQueryKey(final HttpServletRequest request, HttpServletResponse response, @PathVariable("key") final String key) {
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				List list = getService().selectListByQueryKey(key, param);
				Map tmp = new HashMap();
				service.addCodeNames(list);
				tmp.put("LIST", list);
				return tmp;
			}
		}).run(request);

	}
	
	@RequestMapping(value =  "/collect" )
	public Object collect(final HttpServletRequest request, HttpServletResponse response )
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				 
				getService().insertCollect();
				return ControllerUtil.getSuccessMap(param );
				 
			}

		}).run(request);
			 
	}
}

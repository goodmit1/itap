package com.clovirsm.controller.admin;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.VraCatalogMngService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;

@RestController
@RequestMapping(value =  "/vra_catalog_mng" )
public class VraCatalogMngController extends DefaultController {

	@Autowired VraCatalogMngService service;
	@Override
	protected VraCatalogMngService getService() {
		 
		return service;
	}
	@RequestMapping(value =  "/collect" )
	public Object collect(final HttpServletRequest request, HttpServletResponse response )
	{
		return  (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				 
				getService().insertCollect();
				return ControllerUtil.getSuccessMap(param );
				 
			}

		}).run(request);
			 
	}
	
}

package com.clovirsm.controller.admin;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.TeamConfService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.util.ControllerUtil;

@RestController
@RequestMapping(value =  "/team_conf" )
public class TeamConfController extends DefaultController {

	@Autowired TeamConfService service;
	@Override
	protected TeamConfService getService() {
		return service;
	}

	@RequestMapping({"/save"})
	@Override
	public Map save(HttpServletRequest request, HttpServletResponse response) {
		return ((Map) new ActionRunner() {
			@SuppressWarnings("unchecked")
			protected Object run(Map param) throws Exception {
				String[] keyArr = new String[] {"IP", "TEAM_ETC_CD", "CLUSTER"};

				for(String key : keyArr) {
					if(param.get(key) != null){
						param.put("KUBUN", key);
						param.put("VAL1", param.get(key));
						param.put("IU", param.get(key + "_IU"));
						if("".equals(param.get(key))){
							if("I".equals(param.get("IU"))) {
								continue;
							}
							param.put("IU", "D");
						}
						service.insertOrUpdateOrDelete(param);
					}
				}
				return ControllerUtil.getSuccessMap(param, service.getPks());
			}

		}.run(request));
	}
}
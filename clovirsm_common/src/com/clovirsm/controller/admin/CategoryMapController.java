package com.clovirsm.controller.admin;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.common.NCException;
import com.clovirsm.hv.HVProperty;
import com.clovirsm.service.admin.CategoryMapService;
import com.clovirsm.service.admin.DdicUserService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.mvc.util.MsgUtil;

@RestController
@RequestMapping(value =  "/category/map" )
public class CategoryMapController extends DefaultController {

	@Autowired CategoryMapService service;
	@Autowired DdicUserService ddicService;
	@Override
	protected CategoryMapService getService() {
		 
		return service;
	}
	
	
	@RequestMapping(value =  "/savePurpose" )
	  public Object savePurpose(HttpServletRequest request, HttpServletResponse response)
	  {
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				Map tempParam = new HashMap();
				
				tempParam.put("DD_ID", "PURPOSE"); 
				tempParam.put("DD_NM", param.get("ID")); 
				tempParam.put("DD_VALUE", String.valueOf(param.get("ID")).toLowerCase()); 
				tempParam.put("KO_DD_DESC", param.get("ID")); 
				tempParam.put("EN_DD_DESC", param.get("ID")); 
				
				ddicService.insertOrUpdate(tempParam);
				param.put("ID",tempParam.get("DD_VALUE"));
				int result = service.insertOrUpdate(param);
				
		        return ControllerUtil.getSuccessMap(param, service.getPks());
			}
		}
		).run(request);

	  }
}

package com.clovirsm.controller.admin;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.VraCatalogDrvService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;

@RestController
@RequestMapping(value =  "/vra_drv" )
public class VraCatalogDrvController extends DefaultController {

	@Autowired VraCatalogDrvService service;
	@Override
	protected VraCatalogDrvService getService() {
		 
		return service;
	}

	@RequestMapping(value =  "/list_fields" )
	public Object listFields(final HttpServletRequest request, HttpServletResponse response )
	{
		Map param;
		try {
			param = ControllerUtil.getParam(request);
			return service.getFieldList(param);
		} catch (Exception e) {
			return ControllerUtil.getErrMap(e);
		}
		
	}
}

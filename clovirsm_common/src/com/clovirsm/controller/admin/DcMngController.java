package com.clovirsm.controller.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.common.NCException;
import com.clovirsm.hv.HVProperty;
import com.clovirsm.service.admin.DcMngService;
import com.fliconz.fm.common.CodeHandler;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.fliconz.fw.runtime.util.ListUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@RestController
@RequestMapping(value =  "/dc_mng" )
public class DcMngController extends DefaultController {

	@Autowired DcMngService service;
	
 

	@Override
	protected DcMngService getService() {
		 
		return service;
	}
	 
	 
	@RequestMapping(value =  "/list/prop" )
	public Object listProp(final Locale locale,HttpServletRequest request) throws Exception
	{
		return  (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				List<Map> valList =  getService().selectListByQueryKey("list_NC_DC_PROP", param);
				Map valMap = new HashMap();
				if(valList != null && valList.size()>0)
				{
					for(Map m : valList)
					{
						valMap.put(m.get("PROP_NM"), m.get("PROP_VAL"));
					}
				}
				String t= HVProperty.getInstance().getProperty("hv.prop." + param.get("HV_CD"));
				List list = new ArrayList();
				if(t == null)
				{
					return list;
				}
				
				if(!"".equals(t)) {
					String[] prop = t.split(",");
				
					for(String prop1 : prop)
					{
						String[] propArr = prop1.split("[|]");
						Map m = new HashMap();
						 
						m.put("PROP_NM", propArr[0]);			 
						m.put("PROP_TYPE_NM", propArr[1]);
						m.put("PROP_NM_TITLE", MsgUtil.getMsg("NC_DC_PROP_" + propArr[0] , null, locale.getLanguage()));
						m.put("PROP_VAL", valMap.get(propArr[0]));
						list.add(m);
					}
				}
				return list;
				 
			}

		}).run(request);
		
	}
	/*@RequestMapping(value =  "/list/etc_conn" )
	public List listEtcConn(final Locale locale,HttpServletRequest request) throws Exception
	{
		return (List)(new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				List<Map> list =  getService().selectListByQueryKey("list_NC_DC_ETC_CONN", param);
				CodeHandler hdr = (CodeHandler)SpringBeanUtil.getBean("codeHandler");
				Map<String,String> etcProducts = hdr.getCodeList("HV_ETC_PRODUCT_" + param.get("HV_CD"), "ko");
				if(etcProducts != null)
				{
					Map<String,String> newProducts = new HashMap();
					newProducts.putAll(etcProducts);
					for(Map m : list)
					{
						newProducts.remove(m.get("CONN_TYPE"));
					}
					for(String key:newProducts.keySet())
					{
						Map m = new HashMap();
						m.put("CONN_TYPE_NM", newProducts.get(key));
						m.put("CONN_TYPE",key );
						list.add(m);
					}
				}
				return list;
				 
			}

		}).run(request);
		
	}*/
	@RequestMapping(value =  "/collect/{dc_id}" )
	public Object collect(final Locale locale,HttpServletRequest request, @PathVariable("dc_id") final String dcId) throws Exception
	{
		return   (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				service.collectHVObj(dcId);
				return ControllerUtil.getSuccessMap(param);
			}}).run(request);
	}
	@RequestMapping(value =  "/delete/disk_type" )
	public Object deleteDisk(final Locale locale,HttpServletRequest request  ) throws Exception
	{
		return   (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				service.deleteDiskType(param);
				return ControllerUtil.getSuccessMap(param);
			}}).run(request);
	}
}
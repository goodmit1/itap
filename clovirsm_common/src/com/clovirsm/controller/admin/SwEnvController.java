package com.clovirsm.controller.admin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.cm.sys.git.GitLabAPI;
import com.clovirsm.service.admin.CategoryMapService;
import com.clovirsm.service.admin.SwEnvService;
import com.clovirsm.service.admin.SwService;
import com.clovirsm.service.admin.VraCatalogMngService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.PropertyManager;

@RestController
@RequestMapping(value =  "/sw/env" )
public class SwEnvController extends DefaultController {

	@Autowired SwEnvService service;
	@Override
	protected SwEnvService getService() {
		 
		return service;
	}
}

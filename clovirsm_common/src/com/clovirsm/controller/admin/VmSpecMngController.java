package com.clovirsm.controller.admin;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.FwMngService;
import com.clovirsm.service.admin.UserSearchService;
import com.clovirsm.service.admin.VmSpecMngService;
import com.clovirsm.service.admin.VmUserMngService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.util.ControllerUtil;


/**
 * 서버 사양 관리
 * @author user01
 *
 */
@RestController
@RequestMapping(value =  "/vm_spec_mng" )
public class VmSpecMngController extends DefaultController {

	@Autowired VmSpecMngService service;
	@Override
	protected VmSpecMngService getService() {
		 
		return service;
	}
	@RequestMapping(value =  "/collect" )
	public Object collect(final HttpServletRequest request, HttpServletResponse response )
	{
		return  (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				 
				getService().insertCollect();
				return ControllerUtil.getSuccessMap(param );
				 
			}

		}).run(request);
			 
	}
	
	@RequestMapping(value =  "/update/{key}/{pay}/" )
	public int codeGitImport(final HttpServletRequest request, HttpServletResponse response, @PathVariable("key") final String key, @PathVariable("pay") final String pay) throws Exception{
				
		Map param = new HashMap();
		param.put("SPEC_ID", key);
		param.put("DD_FEE", pay);
		
		return service.update(param);
				
	}
}
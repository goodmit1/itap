package com.clovirsm.sys.hv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.clovirsm.common.CIDR;
import com.clovirsm.common.IPAddressUtil;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.IPInfo;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.service.resource.IPService;
import com.clovirsm.sys.hv.executor.CheckTemplatePath;
import com.fliconz.fm.common.util.NumberHelper;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class NextIPHelper {
	protected IPService ipService;
	 
	public NextIPHelper(  ) {
		ipService = (IPService)SpringBeanUtil.getBean("IPService");
	}

	/**
	 * 마지막 IP가 Gateway인지 여부, hv.properties의 gatewayIp.firstYn의 값이 Y가 아닐때
	 * @return
	 * @throws Exception
	 */

	
	 
	private List<Map> getCIDRInfoByVMNm(Map param, String vmNm) throws Exception
	{
		CheckTemplatePath chkTmpl = (CheckTemplatePath)SpringBeanUtil.getBean("checkTemplatePath");
		Map map = chkTmpl.run((String)param.get("DC_ID"),  vmNm );
		if(map != null)
		{
			param.put("NETWORK", map.get("NETWORK"));
			List<String> ntlist =  (List<String>)map.get("NETWORK");
			if(ntlist != null && ntlist.size()==0)
			{
				return null;
			}
			
			List list = new ArrayList();
			for(String id :  ntlist)
			{
				
				param.put("NW_HV_ID", id);
				Map m = ipService.selectOneByQueryKey("selectNWList", param);
				if(m != null)
				{
					list.add(m);
				}
				else
				{
					Map m1 = new HashMap();
					m1.put("NW_HV_ID", id);
					list.add(m1);
				}
			}
			return list;
		}
		return null;
	}
	public List<IPInfo> getNextIp(int firstNicId , Map param) throws Exception {
		List<Map> list = null;
		if(!TextHelper.isEmpty((String)param.get("FROM_ID")))
		{
			list = getCIDRInfoByVMNm(param, (String)param.get("TMPL_PATH"));
			
		}
		else
		{
			list = ipService.selectListByQueryKey("selectNWList", param);
		}
		 
		List<IPInfo> result = new ArrayList();
		for(Map m : list)
		{
			IPInfo info = new IPInfo();
			info.nicId = firstNicId++;
			
			String cidrs = (String)m.get("CIDRS");
			if(TextHelper.isEmpty(cidrs))
			{
				result.add(info);
				continue;
			}
			getNextIp(cidrs, info, param);
			if(info.ip  == null)
			{
				throw new Exception("no more IP");
			}
			result.add(info);
		}
		return result;
	}
	public void getNextIp(String cidrs,IPInfo info, Map vmInfo) throws Exception
	{
		String[] arr = cidrs.split(",");
		for(int i=0; i < arr.length; i++)
		{
			 
			CIDR cidr = new CIDR(arr[i]);
			String ip = ipService.getReserveNextIp( vmInfo, cidr.getNthIpNum(1), cidr.lastHostIpNum());
			if(ip != null)
			{
				info.ip = ip;
				info.gateway = cidr.getGateWay();
				info.subnetMask = cidr.getSubnetMask();
				info.nw_ip = cidr.getNWIp();
				return  ;
			}
		}
		return ;
	}
	public void setNextIp(Map param, String ip) throws Exception {
		param.put("IP", ip);
		param.put("IP_NUM", IPAddressUtil.ipToLong(ip));
		param.put("STATUS", "U");
		
		ipService.update(param);
		
	}
	 
}

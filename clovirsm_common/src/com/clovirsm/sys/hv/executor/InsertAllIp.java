package com.clovirsm.sys.hv.executor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.service.resource.IPService;
import com.clovirsm.sys.hv.DC;


/**
 * VM, Host IP정보 수집해서 NC_IP에 insert
 * @author user01
 *
 */
@Component("insertAllIp")
public class InsertAllIp extends InsertAlarm{

	@Autowired IPService ipService;
 	  
	@Override
	protected Map run1(DC dcInfo, boolean isNew, Map param) throws Exception {
		 Map result = dcInfo.getAPI().getVMIpListInDC(param);
		 List<Map> list = (List)result.get(HypervisorAPI.PARAM_LIST);
		 param.remove("VM_NM");
		 ipService.updateByQueryKey("update_STATUS_T", param);
		 for(Map m : list) {
			 List<Map> ipList = (List)m.get("IP");
			 if(ipList != null && ipList.size()>0) {	 
				 for(Map ipm:ipList)
				 {
					 param.putAll(ipm);
					 param.put("VM_HV_ID", m.get("OBJ_ID"));
					 param.put("VM_NM", m.get("NAME"));
					 param.put("HOST_HV_ID", m.get("HOST_HV_ID"));
					 param.put("STATUS", "U");
					 int row = ipService.updateByQueryKey("update_NC_IP", param);
					 if(row == 0)
					 {
						 //row = ipService.updateByQueryKey("insert_NC_IP", param);
					 }
					 
				 }
			 }
			 else if(VMWareAPI.RUN_CD_STOP.equals(m.get(VMWareAPI.PARAM_RUN_CD)))
			 {
				 param.put("STATUS", "U");
				 param.put("VM_HV_ID", m.get("OBJ_ID"));
				 ipService.updateByQueryKey("update_STATUS", param);
			 }
		 }
		 param.put("OBJ_TYPE_NM", "HostSystem");
		 List<Map> hostList = dcService.selectListByQueryKey("list_NC_HV_OBJ", param);	//Host의 ip
		 for(Map m : hostList)
		 {
			 param.put("IP", m.get("OBJ_NM"));
			 param.put("VM_HV_ID", m.get("OBJ_ID"));
			 param.put("VM_NM", m.get("OBJ_NM"));
			 param.put("STATUS", "U");
			 int row = ipService.updateByQueryKey("update_NC_IP", param);
			 if(row == 0)
			 {
				 //row = ipService.updateByQueryKey("insert_NC_IP", param);
			 }
		 }
		 param.put("STATUS", "N");
		 param.remove("VM_HV_ID");
		 param.remove("VM_NM");
		 ipService.updateByQueryKey("update_STATUS", param);
		 
		 dcInfo.getBefore(dcService).onAfterCollect("NC_IP", null);
		 return null;
	}

	public void pingAll()
	{
		try
		{
			Map param = new HashMap();
			param.put("STATUS","N");
			List<Map> list = ipService.selectList("NC_IP", param);
			for(Map m : list)
			{
				boolean isPing = ipService.ping((String)m.get("IP"));
				if(isPing)
				{
					 
					ipService.setPing((String)m.get("IP"));
				}
			}
		}
		catch(Exception ignore)
		{
			ignore.printStackTrace();
		}
	}
}

	
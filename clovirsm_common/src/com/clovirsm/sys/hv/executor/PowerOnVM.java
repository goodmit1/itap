package com.clovirsm.sys.hv.executor;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.fliconz.fm.common.util.NumberHelper;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.clovirsm.common.NCConstant;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.service.batch.UsageLogService;
import com.clovirsm.sys.hv.DC;

/**
 * VM 시작
 * @author 윤경
 *
 */
@Component
public class PowerOnVM extends Common{

	protected String getOp()
	{
		return VMWareAPI.POWER_ON;
	}
	public String run(String vmId) throws Exception
	{
		Map param = dcService.getVMInfo(vmId);
	 
		Map result = this.run(param);
		return (String)result.get(HypervisorAPI.PARAM_PRIVATE_IP);
	}
	@Override
	protected Map run1(DC dcInfo,boolean isNew, Map param) throws Exception {
		
		VMInfo info = dcService.toVMInfo(param);
		 
		return doProcess(dcInfo, param, info);
	}
	protected Map doProcess(DC dcInfo, Map param,VMInfo info ) throws Exception
	{
		Map result = dcInfo.getAPI().powerOpVM(param, info, getOp());
		onAfterProcess(param );
		return result;
	}
	protected void onAfterProcess(Map param ) throws Exception
	{
		dcService.updateVMStart(param);
		 
	}
 
	public void runInThread(long delay, String vmId) 
	{
		Thread thread = new Thread()
				{
					public void run()
					{
						try {
							Thread.sleep(delay);
						
							PowerOnVM infos = (PowerOnVM)SpringBeanUtil.getBean("powerOnVM");
							infos.run(   vmId );
						} catch ( Exception e) {
							 
							e.printStackTrace();
						}
					}
				};
		thread.start();		
	}
		
	 
}

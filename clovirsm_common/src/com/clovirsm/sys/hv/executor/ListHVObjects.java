package com.clovirsm.sys.hv.executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.fliconz.fm.common.util.TextHelper;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.sys.hv.DC;

/**
 * 어드민 > 데이터 센터 관리에서 사용하기 위해 필요한 Object(데이터 센터, 클러스터, 데이터 스토어, 호스트 등)를 가지고 와서  NC_HV_OBJ테이블에 insert
 * 
 * @author 윤경
 *
 */
@Component("listHVObjects")
public class ListHVObjects extends InsertAlarm {

	public List run(boolean reload, String dcId, String type, String parent) throws Exception
	{
		Map param = new HashMap();
		param.put("DC_ID", dcId);
		param.put("OBJ_TYPE_NM", type);
		param.put("PARENT_OBJ_NM", parent);
		param.put("PARENT_OBJ_NM", parent);
		param.put("RELOAD_YN", reload?"Y":"N");
		Map result = super.run(param);
		if(result == null) return null;
		return (List)result.get(HypervisorAPI.PARAM_LIST);
		
	}
	public List run(Map m, String type, String parent) throws Exception
	{
		if(m.get("CONN_URL") == null || m.get("CONN_USERID")==null || m.get("CONN_PWD") == null || m.get("HV_CD") == null)
		{
			return new ArrayList();
		}
		DC dcInfo = new DC(  (String)m.get("HV_CD")   );
		dcInfo.addProp(m);
		m.put("OBJ_TYPE_NM", dcInfo.getAPI().getHVObjType(type));
		m.put("PARENT_OBJ_NM", parent);
		List objList = null;
		if(!"Y".equals(m.get("RELOAD_YN")))
		{
			objList = dcService.selectList("NC_HV_OBJ",m);
			if(objList.size()==0 && !TextHelper.isEmpty(parent))
			{
				m.put("PARENT_OBJ_NM", "");
				objList = dcService.selectList("NC_HV_OBJ",m);
			}
		}
		
		if(objList == null || objList.size()==0 || "Y".equals(m.get("RELOAD_YN")))
		{
			Map result = this.run1(dcInfo, false, m);
			if(result == null) return new ArrayList();
			return (List)result.get(HypervisorAPI.PARAM_LIST);
		}
		else
		{
			return objList ;
		}
		
	}
	@Override
	protected Map run1(DC dcInfo, boolean isNew, Map param) throws Exception {
		 
		Map result = dcInfo.getAPI().listObject(param);
		if(result == null) return null;
		List<Map> list = (List)result.get(HypervisorAPI.PARAM_LIST);
		dcService.deleteAllHVObject(param);
		List objList = new ArrayList();
		 
		for(Map m : list)
		{
			if(m.get("OBJ_TYPE_NM").equals(param.get("OBJ_TYPE_NM")) && m.get("PARENT_OBJ_NM").equals(param.get("PARENT_OBJ_NM")))
			{
				objList.add(m);
			}
			m.put("DC_ID" , param.get("DC_ID"));
			m.put("CONN_URL", param.get("CONN_URL"));
			dcService.insertHVObject(m);
		}
		 
		Map result1 = new HashMap();
		result1.put(HypervisorAPI.PARAM_LIST, objList);
		return result1; 
		
	}

}

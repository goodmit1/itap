package com.clovirsm.sys.hv.executor;

import java.util.HashMap;
import java.util.Map; 

import org.springframework.stereotype.Component;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCException;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;

/**
 * 이미지 명 변경
 * @author 윤경
 *
 */
@Component
public class RenameImg extends Common{
	public void run(String imgId, String newName) throws Exception
	{
		Map param = new HashMap();
		param.put(PARAM_IMG_ID, imgId);
		Map imgInfo = dcService.getImgInfo(imgId);
		imgInfo.put(HypervisorAPI.PARAM_OBJECT_NEW_NM, newName);
		this.run(imgInfo);
	}
	 
	 
		
		 
	@Override
	protected Map run1(DC dcInfo, boolean isNew, Map param) throws Exception {
		return dcInfo.getAPI().renameImg(param);
		
	}

 
	 

}

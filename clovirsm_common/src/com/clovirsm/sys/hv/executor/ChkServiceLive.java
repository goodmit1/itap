package com.clovirsm.sys.hv.executor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.clovirsm.hv.CommonAPI;
import com.clovirsm.hv.IAPI;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.service.batch.BatchService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;

@Component
public class ChkServiceLive {
	@Autowired DCService dcService;
	@Autowired BatchService batchService;
	public void runAll( ) throws Exception
	{
		Map param = new HashMap();
		batchService.updateByQueryKey("reset_NC_SVC_LIVE_CHK", param);
		List<Map> list = dcService.selectList("NC_DC", param);
		VMWareAPI vmwareApi = new VMWareAPI();	 
		for(Map m : list){

			String dc = (String)m.get("DC_ID"); 
			DC dcInfo = dcService.getDC(dc);
			try {
				dcInfo.getAPI().connectTest(m);
 
				setLive("DC",(String)m.get("DC_ID"), (String)m.get("CONN_URL"), true);
 
			}
			catch(Exception e) {
 
				setLive("DC", (String)m.get("DC_ID"), (String)m.get("CONN_URL"), false);
 
 			}
 
		}
		List< Map> etcConn = dcService.getEtcConnInfo(null);
		for(Map m : etcConn) {
			String kubun = (String)m.get("CONN_TYPE");
			IAPI api = vmwareApi.getAPI(kubun);
			IConnection conn = null;
			if(api != null) {
				try {
					conn = api.connect(m);
					setLive(kubun, (String)m.get("CONN_ID"), (String)m.get("CONN_URL"), true);
 
				}
				catch(Exception e) {
					System.out.println("Connect error:" + m.get("CONN_URL") + ":" + e.getMessage());
					setLive(kubun, (String)m.get("CONN_ID"), (String)m.get("CONN_URL"), false);
				}
				finally {
					if(conn != null) CommonAPI.disconnect(conn);
				}

			}
		}
			
		
		batchService.updateByQueryKey("delete_NC_SVC_LIVE_CHK", param);
	}

	protected void setLive(String kubun, String id, String url, boolean isLive) throws Exception {
		int pos = url.indexOf("/",10);
		if(pos<10) {
			pos = url.length();
		}
		Map param = new HashMap();
		param.put("KUBUN", kubun);
		param.put("CONN_ID", id);
		param.put("URL", url.substring(0, pos));
		param.put("LIVE_YN", isLive?"Y":"N");
		 
		int row = batchService.updateByQueryKey("update_NC_SVC_LIVE_CHK", param);
		if(row==0) {
			batchService.updateByQueryKey("insert_NC_SVC_LIVE_CHK", param);
		}
	}
}

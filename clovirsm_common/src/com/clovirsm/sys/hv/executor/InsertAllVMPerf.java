package com.clovirsm.sys.hv.executor;

 
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fliconz.fm.common.CodeHandler;
import com.fliconz.fw.runtime.util.DateUtil;
import com.fliconz.fw.runtime.util.NumberUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.vmware.connection.SsoConnection.SSOLoginException;
import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.service.ComponentService;
import com.clovirsm.service.batch.BatchService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;

/**
 * VM 모니터링 정보 수집
 * @author user01
 *
 */
@Component("insertAllVMPerf")
public class InsertAllVMPerf  extends InsertAlarm {

 
	@Autowired BatchService batchService;
	 
	@Autowired transient ListVMPerfInDC listVMAttribute;
	public InsertAllVMPerf()
	{
		
		
	}
	@Override  
	protected Map getMonitorInfo( DC dcInfo,Map param) throws Exception
	{
		 
		BatchService batchService = (BatchService)SpringBeanUtil.getBean("batchService");
		 
		List<Map> list = listVMAttribute.list((String)param.get("DC_ID"), param);
		Date date1 = new Date();
		param.put("REG_DT", DateUtil.format(date1, "yyyyMMdd"));
		param.put("REG_TM", DateUtil.format(date1, "HHmmss"));
		batchService.updateByQueryKey("reset_" + this.getTableNm(), param);
		for(Map m : list )
		{
			try
			{
				if(!(boolean)m.get("TEMPLATE") && m.get("HOST_NM") != null){ 
					m.putAll(param);
					int row= batchService.insertByQueryKey( "update_" + getTableNm(), m);
					if(row==0) {
						batchService.insertByQueryKey( "insert_" + getTableNm(), m);
					}
				}
				 
			}
			catch(Exception ignore)
			{
				if(ignore instanceof com.clovirsm.hv.NotFoundException)	{
					
				}
				else if(ignore instanceof SSOLoginException){
					throw ignore;
				}
				else {
					ignore.printStackTrace();
				}
			}
		}
		batchService.updateByQueryKey("delete_" + this.getTableNm(), param);
		return null;
	}
	 
	@Override
	protected String getTableNm()
	{
		return "NC_MONITOR_VM_ALL";
	}
	
	 
	 

}


package com.clovirsm.sys.hv.executor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.sys.hv.DC;

/**
 * 데이터 스토어 속성 (어드민 > 대쉬보드 > 클러스터의 데이터 스토어 탭에서 사용)
 * @author 윤경
 *
 */
@Component
public class ListVMPerfInDC extends ListHostAttribute {

 
	
	@Override
	protected Map run1(DC dcInfo, boolean isNew, Map param) throws Exception {
		ListPerfHistory.makeDateType(param, "START_DT");
		ListPerfHistory.makeDateType(param, "FINISH_DT");
		return dcInfo.getAPI().getVMPerfListInDC(param);
	}

}

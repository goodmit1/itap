package com.clovirsm.sys.hv.executor;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.service.batch.BatchService;
import com.clovirsm.sys.hv.DC;
import com.fliconz.fw.runtime.util.DateUtil;
import com.fliconz.fw.runtime.util.NumberUtil;


/**
 * 라이센스 목록
 * @author user01
 *
 */
@Component("insertVMTag")
public class InsertVMTag  extends InsertAlarm {

	@Override
	protected Map getMonitorInfo( DC dcInfo,Map param) throws Exception
	{
		 return  dcInfo.getAPI().getVMTags(param);
	}
	 
	@Override
	protected String getTableNm()
	{
		return "NC_VM_TAG";
	}
	 
	@Override
	protected void setExtraInfo(Map m, Map param, DC dcInfo) throws Exception
	{
		 
		 
	}

	 
}

package com.clovirsm.sys.hv.executor;

 
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fliconz.fw.runtime.util.DateUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.service.batch.BatchService;
import com.clovirsm.sys.hv.DC;
@Component("insertHostPerf")
public class InsertHostPerf  extends InsertAlarm {

	@Override
	protected Map getMonitorInfo( DC dcInfo,Map param) throws Exception
	{
		 return  dcInfo.getAPI().getHostAttribute(param);
	}
	protected void onBeforeRun(Map param) throws Exception{
		 
		Date date1 = new Date();
		param.put("REG_DT", DateUtil.format(date1, "yyyyMMdd"));
		param.put("REG_TM", DateUtil.format(date1, "HHmmss"));
	}
	@Override
	protected String getTableNm()
	{
		return "NC_MONITOR_HOST";
	}
	
	@Override
	protected void setExtraInfo(Map m, Map param, DC dcInfo) throws Exception
	{
		
		 m.put("REG_DT", param.get("REG_DT"));
		 m.put("REG_TM", param.get("REG_TM"));
		 
	}
 
	@Override
	protected void onAfter(BatchService batchService, Map param, Map result) throws Exception{
		super.onAfter(batchService, param, result);
		batchService.updateByQueryKey("delete_real_NC_MONITOR_HOST", param);
	}


}

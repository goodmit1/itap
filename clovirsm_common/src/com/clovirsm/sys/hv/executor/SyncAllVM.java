package com.clovirsm.sys.hv.executor;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.clovirsm.service.batch.BatchService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

/**
 * 모든 VM Sync
 * @author user01
 *
 */
@Component
public class SyncAllVM extends Common{

	@Autowired GetVMInfos vmInfos;
	@Override
	protected Map run1(DC dcInfo, boolean isNew, Map param) throws Exception {
		BatchService batchService = (BatchService)SpringBeanUtil.getBean("batchService");
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		List<Map> list = batchService.selectList("NC_VM", param);
		 
		for(Map m : list )
		{
			try
			{
				 
				 
				vmInfos.run(false, (String) m.get("DC_ID"),  (String)m.get("COMP_ID"), m.get("TEAM_CD"), m);
			}
			catch(Exception ignore)
			{
				
			}
		}
		return null;
	}

}

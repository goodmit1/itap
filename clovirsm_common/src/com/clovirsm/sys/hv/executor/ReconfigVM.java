package com.clovirsm.sys.hv.executor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.clovirsm.common.NCException;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

/**
 * VM 스펙 변경
 * @author 윤경
 *
 */
@Component
public class ReconfigVM extends Common{
	public Map runByVmInfo(Map param) throws Exception{
		dcService.putVMCreateInfo(param);
		this.run(param);
		return param;
	}
	public Map run(String vmId) throws Exception
	{
		Map param = dcService.getVMCreateInfo(vmId, false);
		return run(param);
		 
	}
	 
	@Override
	protected Map run1(DC dcInfo,boolean isNew, Map param) throws Exception {
		
		VMInfo info = dcService.toVMInfo(param);
		 
		return doProcess(dcInfo, param, info);
	}
	protected Map doProcess(DC dcInfo, Map param,VMInfo info ) throws Exception
	{
		IAfterProcess after = dcService.getDC(dcInfo.getProp("DC_ID")).getBefore(dcService).onAfterProcess("S", "U",param);
		Map result =  dcInfo.getAPI().reconfigVM( param, info );
		after.onAfterSuccess(null);
		
		
		 
		return result; 
	}

}

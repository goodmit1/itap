package com.clovirsm.sys.hv.executor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.clovirsm.common.NCException;
import com.clovirsm.hv.IBeforeAfter;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.sys.hv.DC;
/**
 * 이미지 생성
 * @author 윤경
 *
 */
@Component
public class CreateImg extends Common
{

	public void run(String imgId) throws Exception
	{
		Map param = new HashMap();
		param.put(PARAM_IMG_ID, imgId);
		Map diskInfo = dcService.getImgInfo(imgId);
		this.run(diskInfo);
	}
	@Override
	protected Map run1(DC dcInfo,boolean isNew, Map param) throws Exception {

		Map info = dcService.getVMInfo((String)param.get( PARAM_FROM_ID));
		if(info == null)
		{
			throw new NCException(NCException.NOT_FOUND,"VM", (String)param.get(PARAM_VM_ID)  );
		}
		param.put("DISK_SIZE", info.get("DISK_SIZE"));
		String[] dsNames = dcService.getDsName(param);
		param.put(VMWareAPI.PARAM_IMG_DATASTORE_NM, dsNames);
		return doProcess(dcInfo, param, dcService.toVMInfo(info));
	}
	protected Map doProcess(DC dcInfo, Map param,VMInfo info ) throws Exception
	{
		IBeforeAfter before = dcInfo.getBefore( dcService  );
		Map result = dcInfo.getAPI().createImage(before.onAfterProcess("G","C", param), param, info);
		return result;
	}
}

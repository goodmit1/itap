package com.clovirsm.sys.hv.executor;
 

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.clovirsm.common.NCException;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.HypervisorException;
import com.clovirsm.service.batch.UsageLogService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService; 

public abstract class Common {
	public static final String PARAM_DC_ID = "DC_ID";
	public static final String PARAM_VM_ID = "VM_ID";
	public static final String PARAM_FROM_ID = "FROM_ID";
	public static final String PARAM_TEAM_CD = "TEAM_CD";
	public static final String PARAM_COMP_ID = "COMP_ID";
	protected static final String PARAM_IMG_ID = "IMG_ID";
	public static final String PARAM_DISK_ID = "DISK_ID";
	public static final String PARAM_INS_ID = "INS_ID";
	@Autowired
	DCService dcService;
	
	@Autowired
	transient UsageLogService usageLogService;
	
	Logger log = LoggerFactory.getLogger(Common.class);
	protected Map run(Map param) throws Exception
	{
		try
		{
			boolean isNew = false;
			String dc = (String)param.get( PARAM_DC_ID);
			DC dcInfo = dcService.getDC(dc);
			if(dcInfo == null)
			{
				throw new NCException(NCException.NOT_FOUND,"Datacenter",dc);
			}
			param.putAll(dcInfo.getProp());
			/*
			ICreateInfo4Org createInfo = (ICreateInfo4Org)SpringBeanUtil.getBean("createInfo4Org");
			if(createInfo != null)
			{
				isNew = createInfo.getOrgDCInfo(dcService, dc, param);
			}*/
			 
			log.info("hv api start :" + this.getClass().getName() + ":" + param);
			return run1(dcInfo, isNew, param);
		}
		catch(Exception e)
		{
			log.error("hv api error :" + this.getClass().getName() + ":" + param + ":" + e.getMessage());
			e.printStackTrace();
			if(e instanceof HypervisorException)
			{
				throw new NCException(((HypervisorException)e).getMsgParams());
			}
			throw e;
			//logging
		}
	}

	protected abstract Map run1(DC dcInfo, boolean isNew, Map param) throws Exception;
}

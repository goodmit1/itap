package com.clovirsm.sys.hv.executor;

 
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fliconz.fw.runtime.util.DateUtil;
import com.fliconz.fw.runtime.util.NumberUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.vmware.connection.SsoConnection.SSOLoginException;
import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.clovirsm.service.batch.BatchService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
@Component("insertMMStat")
public class InsertMMStat  extends Common {

	@Autowired BatchService batchService;

	public InsertMMStat()
	{
		  super();
		
	}
	
	public void run(String yyyyMM) throws Exception
	{
		Map param = new HashMap();
		 
		param.put("YYYYMM", yyyyMM) ;
		batchService.deleteMonitorInfo("NC_MM_STAT", param);
		batchService.deleteMonitorInfo("NC_MM_STAT_USER", param);
		
		batchService.insertMonitorInfo("NC_MM_STAT", param);
		
		
		batchService.insertMonitorInfo("NC_MM_STAT_USER", param);

		/**/
	}
	public void runHasDetail(String yyyyMM) throws Exception
	{
		Map param = new HashMap();
		 
		param.put("YYYYMM", yyyyMM) ;
		batchService.deleteMonitorInfo("NC_MM_STAT", param);
		batchService.deleteMonitorInfo("NC_MM_STAT_DETAIL", param);
		batchService.deleteMonitorInfo("NC_MM_STAT_USER", param);
		
	
		batchService.insertMonitorInfo("NC_MM_STAT_DETAIL", param);
		batchService.insertMonitorInfo("NC_MM_STAT2", param);
		batchService.insertMonitorInfo("NC_MM_STAT_USER", param);
		batchService.updateByQueryKey("update_NC_VM_MM_FEE", param);

		/**/
	}
	public void runMonitorInfo(String yyyyMM) throws Exception{
		List<Map> list = dcService.selectList("NC_DC", new HashMap());
		for(Map m : list)
		{
			run((String)m.get("DC_ID"), yyyyMM);
		}
	}
	public List run(String dcId , String yyyyMM) throws Exception
	{
		Map param = new HashMap();
		param.put("DC_ID", dcId);
		param.put("YYYYMM", yyyyMM) ;
		super.run(param);
		return null;
		
		
	}
	
	 
	@Override
	protected Map run1(DC dcInfo, boolean isNew, Map param) throws Exception {
	
		BatchService batchService = (BatchService)SpringBeanUtil.getBean("batchService");
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		List<Map> list = batchService.selectList("NC_VM", param);
		 
		for(Map m : list )
		{
			try
			{
				 
				m.putAll(param);
				String yymm = (String)param.get("YYYYMM");
				Date startDate = DateUtil.toDate(yymm + "01", "yyyyMMdd");
				int dd = DateUtil.getLastDay(startDate.getYear()+1900, startDate.getMonth() );
				Date finishDate = new Date(startDate.getYear(), startDate.getMonth(), dd);
				finishDate.setHours(23);
				finishDate.setMinutes(59);
				finishDate.setSeconds(59);
				m.put(VMWareCommon.PARAM_START_DT, startDate);
				m.put(VMWareCommon.PARAM_FINISH_DT, finishDate);
				Map result = dcInfo.getAPI().getVMPerf(m);
				Map m1 = (Map)result.get("LIST");
				if( m1== null || m1.get("VM_HV_ID") == null) continue;
				m.putAll(m1);
				m.put("DC_ID", param.get("DC_ID"));
				batchService.updateMonitorInfo("NC_MM_STAT", m);
				 
			}
			catch(Exception ignore)
			{
				if(ignore instanceof SSOLoginException)
				{
					throw ignore;
				}
			}
		}
		return null;
	}
	 
	 

}


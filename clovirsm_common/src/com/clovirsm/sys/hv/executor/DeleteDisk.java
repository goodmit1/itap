package com.clovirsm.sys.hv.executor;

import java.util.Map;

import org.springframework.stereotype.Component;
 

import com.clovirsm.common.NCConstant;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;

/**
 * 디스크 삭제
 * @author 윤경
 *
 */
@Component
public class DeleteDisk extends CreateDisk{

		 
		protected Map doProcess(DC dcInfo, Map param,VMInfo info ) throws Exception
		{
			if(info.hostName==null)
			{
				info = null;
			}
			Map result = dcInfo.getAPI().deleteDisk(param, info);
			usageLogService.useEnd(NCConstant.SVC.S.toString(), (String)param.get(PARAM_DISK_ID));
			return result;
		}
}

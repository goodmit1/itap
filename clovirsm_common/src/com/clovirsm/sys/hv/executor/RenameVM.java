package com.clovirsm.sys.hv.executor;

import java.util.Map;

import org.springframework.stereotype.Component;
 


import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;

/**
 * VM명 변경
 * @author 윤경
 *
 */
@Component
public class RenameVM extends Common{

	public Map run(String vmId, String newName) throws Exception
	{
		Map param = dcService.getVMInfo(vmId);
		param.put(HypervisorAPI.PARAM_OBJECT_NEW_NM, newName);
		this.run(param);
		return param;
	}
	@Override
	protected Map run1(DC dcInfo,boolean isNew, Map param) throws Exception {
		
		VMInfo info = dcService.toVMInfo(param);
		 
		return doProcess(dcInfo, param, info);
	}
	protected Map doProcess(DC dcInfo, Map param,VMInfo info ) throws Exception
	{
		return dcInfo.getAPI().renameVM(param, info);
	}

}
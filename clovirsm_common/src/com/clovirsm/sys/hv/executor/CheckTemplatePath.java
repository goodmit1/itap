package com.clovirsm.sys.hv.executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.clovirsm.common.NCException;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.sys.hv.DC;

/**
 * 데이터 센터 정보 입력 시 입력한 OS템플릿이 존재하는지 체크
 * @author 윤경
 *
 */
@Component
public class CheckTemplatePath extends Common{
	public Map run(Map m, String[] templateNames) throws Exception
	{
		if(m.get("CONN_URL") == null || m.get("CONN_USERID")==null || m.get("CONN_PWD") == null || m.get("HV_CD") == null)
		{
			throw new NCException("err.INVALID_CONN");
		}
		DC dcInfo = new DC(  (String)m.get("HV_CD") );  
		dcInfo.addProp(m);
		m.put(HypervisorAPI.PARAM_TMPL_NM_LIST, templateNames) ;
		return this.run1(dcInfo, false, m);
	}
	public Map run(String dcId, String templateName) throws Exception
	{
		DC dcInfo = dcService.getDC(dcId);
		if(dcInfo==null) throw new Exception("not found dc");
		Map m = new HashMap();
		m.put(HypervisorAPI.PARAM_TMPL_NM_LIST, new String[]{templateName}) ;
		m.putAll(dcInfo.getProp());
		Map result =  this.run1(dcInfo, false, m);
		return (Map)result.get(templateName);
	}
	@Override
	protected Map run1(DC dcInfo, boolean isNew, Map param) throws Exception {
		return dcInfo.getAPI().checkTemplatePath(param);
		 
	}

}

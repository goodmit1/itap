package com.clovirsm.sys.hv.executor;

import java.util.Map;

import org.springframework.stereotype.Component;
 
import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;
/**
 * 디스크 언마운트
 * @author 윤경
 *
 */
@Component
public class UnmountDisk extends CreateDisk{

 
	protected Map doProcess(DC dcInfo, Map param,VMInfo info ) throws Exception
	{
		return dcInfo.getAPI().unmount(param, info);
	}

}
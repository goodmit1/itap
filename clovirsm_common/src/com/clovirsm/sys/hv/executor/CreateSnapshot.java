package com.clovirsm.sys.hv.executor;

import java.util.Map;

import org.springframework.stereotype.Component;
 




import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IBeforeAfter;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;

/**
 * 스냅샷 추가
 * @author 윤경
 *
 */
@Component
public class CreateSnapshot extends  Common{


	private static final String PARAM_SNAPSHOP_ID = "SNAPSHOP_ID";
	public void run(String vmId, String snapshotId) throws Exception
	{
	 
		Map snapshotInfo = dcService.getSnapshotInfo(vmId, snapshotId);
		this.run(snapshotInfo);
	}
 
	@Override
	protected Map run1(DC dcInfo,boolean isNew, Map param) throws Exception {
		
		VMInfo info = dcService.toVMInfo(param);
		 
		return doProcess(dcInfo, param, info);
	}
	 
	protected Map doProcess(DC dcInfo, Map param,VMInfo info ) throws Exception
	{
		IBeforeAfter before = dcInfo.getBefore( dcService  );
		Map result =  dcInfo.getAPI().createSnapshot(before.onAfterProcess("T", "C",  param), param, info);
		 
		return result;
	}

}
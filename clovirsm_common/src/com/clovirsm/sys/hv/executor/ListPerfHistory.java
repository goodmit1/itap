package com.clovirsm.sys.hv.executor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;
import com.fliconz.fw.runtime.util.DateUtil;

/**
 * 성능 이력 
 * @author 윤경
 *
 */
@Component
public class ListPerfHistory extends Common{

	 
	/**
	 * hv.properties에 
	 * @param hvCd
	 * @param stype
	 * @return
	 * @throws Exception
	 */
	public String[] getPerfHistoryTypes(String hvCd, String stype) throws Exception
	{
		HypervisorAPI api = DC.getAPI(hvCd);
		
		
		return api.getPerfHistoryTypes(stype);
	}
	
	public static void makeDateType(Map param, String key) throws Exception
	{
		Object o = param.get(key);
		if(o==null   ) return;
		if("".equals(o))
		{
			param.remove(key);
			return;
		}
		if(o instanceof String)
		{
			String dateStr = (String)o;
			String format =  "yyyy-MM-dd";
			if(dateStr.length()==8)
			{
				format =  "yyyyMMdd";
			}
			else if(dateStr.length()==16 && dateStr.indexOf(":")>0)
			{
				format =  "yyyy-MM-dd HH:mm";
			}
			param.put(key, DateUtil.toDate(dateStr,format));
		}
	}
	/**
	 * 
	 * @param period
	 * @param dc
	 * @param type HypervisorAPI.OBJ_TYPE_xx에 정의 되어 있음.
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public Map run(String period,String dc, String type, String name, Map param) throws Exception{
		 
		param.put(PARAM_DC_ID, dc);
		if(type.equals(HypervisorAPI.OBJ_TYPE_VM)){
			Map vmInfo = null;
			try	{
				vmInfo = dcService.getVMInfo(name);
			}catch(Exception ignore){
				param.put("VM_NM", name);
			}
			if(vmInfo != null) param.putAll(vmInfo);
		}
		else{
			
			param.put(HypervisorAPI.PARAM_OBJECT_NM, name);
		}
	 
		makeDateType(param, "START_DT");
		makeDateType(param, "FINISH_DT");
		param.put(HypervisorAPI.PARAM_OBJECT_TYPE, type);
		param.put(HypervisorAPI.PARAM_PERIOD, period);
		return this.run(param);
		 
		 
		
	}
 
	@Override
	protected Map run1(DC dcInfo, boolean isNew, Map param) throws Exception {
		 
		return dcInfo.getAPI().listPerfHistory(param );
	}
	 


}

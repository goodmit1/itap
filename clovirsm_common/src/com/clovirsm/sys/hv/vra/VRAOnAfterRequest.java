package com.clovirsm.sys.hv.vra;

import java.util.Map;

import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.clovirsm.service.TaskService;
import com.clovirsm.service.resource.VraCatalogService;
import com.clovirsm.service.workflow.DeployFailService;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.after.OnAfterCreate;
import com.clovirsm.sys.hv.after.OnAfterCreateSM;
import com.clovirsm.sys.hv.after.OnAfterCreateThread;

public class VRAOnAfterRequest extends OnAfterCreateSM {

	public VRAOnAfterRequest(  Map param ) {
		super(  param );
		 
	}
	
	protected void onAfterCreateVM(VraCatalogService vraCatalogService, Map param) {
		try
		{
			DeployFailService failService = (DeployFailService)SpringBeanUtil.getBean("deployFailService");
			failService.updateSuccess("C", (String)  param.get("CATALOGREQ_ID"),    (String)  param.get("INS_DT"));
			param.remove("FAIL_MSG");
			this.sendSuccessMail(vraCatalogService, param);
		}
		catch(Exception ignoe) {
			
		}
	}
	protected void sendSuccessMail(VraCatalogService vraCatalogService, Map param) throws Exception {
		vraCatalogService.sendMail(param.get("INS_ID"), "mail_title_deploy", "vraCreate", param);
		vraCatalogService.sendMailToAdmin( "mail_title_deploy", "vraCreate", param); //어드민에게 메일
	}
	protected void sendFailMail(boolean isFirst, VraCatalogService vraCatalogService, Map param) throws Exception {
		if(isFirst) {
			//vraCatalogService.sendMail(param.get("INS_ID"), "mail_title_deploy_fail", "vraCreate", param);
		}
		vraCatalogService.sendMailToAdmin( "mail_title_deploy_fail", "vraCreate", param); //어드민에게 메일
	}
	protected void onAfterFail(VraCatalogService vraCatalogService, Map param) {
		try
		{
			
			DeployFailService failService = (DeployFailService)SpringBeanUtil.getBean("deployFailService");
			boolean isFirst = failService.insert(  "C",   "C", (String)  param.get("CATALOGREQ_ID"),    (String)  param.get("INS_DT"), (String) param.get("FAIL_MSG"));
			sendFailMail(isFirst, vraCatalogService, param);
		}
		catch(Exception ignoe) {
			
		}
	}
	@Override
	protected boolean getState( ) throws Exception
	{
		VRAGetRequestState state = (VRAGetRequestState)SpringBeanUtil.getBean("VRAGetRequestState");
		Map result = state.run(param);
		String status = (String)result.get("TASK_STATUS_CD");
		if(status == null || "".equals(status)) return false;
		else {
			VraCatalogService vraCatalogService =(VraCatalogService) SpringBeanUtil.getBean("vraCatalogService");
			param.putAll( vraCatalogService.getAllDetail((String)param.get("CATALOGREQ_ID")) );
			if("S".equals(status)) {
				
				onAfterCreateVM(vraCatalogService, param);
				
				return true;
			}
			else {
				try
				{
					param.put("FAIL_MSG", result.get("FAIL_MSG"));
					
					onAfterFail(vraCatalogService, param);
				}
				catch(Exception ignoe) {
					
				}	
				throw new Exception((String)result.get("FAIL_MSG"));
			}
		}
		
	}
	@Override
	protected String getSvcCd() {
		 
		return "C";
	}

	@Override
	protected String getSvcId() {
		 
		return (String)param.get("CATALOGREQ_ID");
	}

	@Override
	protected String getTableNm() {
		 
		return "NC_VRA_CATALOGREQ";
	}
	 

}

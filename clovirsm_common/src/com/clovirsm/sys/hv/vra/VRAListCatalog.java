package com.clovirsm.sys.hv.vra;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.vra.VRAAPI;
import com.clovirsm.sys.hv.DC;

@Component
public class VRAListCatalog extends VRACommonAction {

 

	@Override
	protected Map run1(VRAAPI api, Map param) throws Exception {
		Map result =  api.listCatalog(param);
		
		return result;
	}

}

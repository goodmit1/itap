package com.clovirsm.sys.hv.vra;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.clovirsm.common.IDGenHelper;
import com.clovirsm.common.NCReqService;
import com.clovirsm.hv.AlreadyExistException;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.vmware.vra.VRAAPI;
import com.clovirsm.service.admin.SwService;
import com.clovirsm.service.resource.DiskService;
import com.clovirsm.service.resource.IPService;
import com.clovirsm.service.resource.VMService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.executor.GetVMInfos;

@Component
public class VRAGetRequestState extends VRACommonAction  {

	 
	@Autowired IPService ipService;
	@Autowired DiskService diskService;
	@Autowired GetVMInfos getVMInfos;
	@Override
	protected Map run1(VRAAPI api, Map param) throws Exception {
		Map result = api.getRequestState(param);
		List<Map> vmList = (List)result.get("VM_LIST");

		if(vmList != null)
		{
			DC dc = null;
			for(Map vm:vmList)
			{
				if(vm.get("DC_IP") != null)
				{
					dc = dcService.getDCByIp(vm);
					 
				}
				if(dc != null)
				{
					vm.put("DC_ID", dc.getProp("DC_ID"));
					vm.put("FROM_ID", param.get("CATALOGREQ_ID"));
					vm.put("VRA_ID", param.get("RESOURCE_ID"));
					vm.put("TEAM_CD", param.get("TEAM_CD"));
					 
					vm.put("INS_ID", param.get("INS_ID") );
					vm.put("USER_ID", param.get("INS_ID") );
					vm.put("_USER_ID_",  param.get("INS_ID") );
					vm.put("_TEAM_CD_",  param.get("TEAM_CD") );
					vm.put("BY_PASS_COMMON_PARAM","Y");
					Map tags = (Map)vm.remove("tags");
					List sws = (List)vm.remove("sws");
					vm.put("PURPOSE", tags.get("purpose"));
					vm.put("CATEGORY",param.get("CATEGORY"));
					vm.put("CMT",param.get("CMT"));
					vm.put("P_KUBUN",param.get("P_KUBUN"));
					vm.put("USE_MM",param.get("USE_MM"));
					vm.put("FAB",param.get("FAB"));
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
					vm.put("_SYSDATE_", sdf.format(new Date()));
					vm.remove("VM_ID");
					VMService vmService = (VMService) NCReqService.getService("S");
					Map vmOldInfo = vmService.getVMByFromId(vm);
					if(vmOldInfo == null)
					{
						try
						{
							vmService.insertVMfromDC(vm);
							saveSWInfO(vm.get("VM_ID"), sws);
						}
						catch(AlreadyExistException e) {
							// TODO udpate vm info
						}
						if(vm.get("PRIVATE_IP") != null && !"".equals(vm.get("PRIVATE_IP"))) {
							vmOldInfo = vmService.getVMByFromId(vm);
							ipService.ipUsed(vmOldInfo, (String)vm.get("PRIVATE_IP"));
						}
					}
					
					// vra때문에 ...
					/*List<Map> diskList = (List<Map>) vm.get("disks");
					if(diskList != null) {
						diskService.updateByQueryKey("update_delYN", vm);
						for(Map m: diskList) {
							m.put("VM_ID", vm.get("VM_ID"));
							diskService.updateByQueryKey("update_DISK_PATH_bySize", m);
						}
					}*/
					
					
					
					
				}
			}

		}
		return result;

	}
	@Autowired SwService swService;
	private void saveSWInfO(Object vmId, List<String> sws) throws Exception {
		Map param =new HashMap();
		param.put("VM_ID", vmId);
		VMService vmService = (VMService) NCReqService.getService("S");
		vmService.deleteByQueryKey("delete_NC_VM_SW", param);
		if(sws == null) return;
		for(String swNameVer: sws) { 
			param.put("SW_NM_VER", swNameVer);
			Object swId = swService.selectOneObjectByQueryKey("getIdByName", param);
			if(swId != null) {
				param.put("SW_ID", swId);
				vmService.insertByQueryKey("insert_NC_VM_SW", param);
			}
		}
	}
	 

}

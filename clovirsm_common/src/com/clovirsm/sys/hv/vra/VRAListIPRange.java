package com.clovirsm.sys.hv.vra;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.vra.VRAAPI;
import com.clovirsm.service.resource.IPService;
import com.clovirsm.sys.hv.DC;

@Component
public class VRAListIPRange extends VRACommonAction {

 
	@Autowired IPService ipService;

	@Override
	protected Map run1(VRAAPI api, Map param) throws Exception {
		Map result =  api.listIpRange(param);
		List<Map> list = (List)result.get("LIST");
		StringBuffer sb = new StringBuffer();
		for(Map m:list) {
			String startIp = (String) m.get("startIPAddress");
			int pos = startIp.lastIndexOf(".");
			sb.append(startIp.substring(0, pos + 1)  + "0/24," );
		}
		
		ipService.insertAllIp(null, sb.toString());
		return result;
	}

}

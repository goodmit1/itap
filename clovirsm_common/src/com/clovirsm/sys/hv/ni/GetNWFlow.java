package com.clovirsm.sys.hv.ni;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.clovirsm.common.NCException;
import com.clovirsm.hv.HypervisorException;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.vni.VNIAPI;
import com.clovirsm.hv.vmware.vra.VRAAPI;
import com.clovirsm.sys.hv.DCService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Component
public class GetNWFlow {
	@Autowired
	DCService dcService;
	Logger log = LoggerFactory.getLogger(GetNWFlow.class);
	public   VNIAPI getAPI(Map param) throws Exception
	{
		VMWareAPI vmwareApi = new VMWareAPI();
		 
		param.putAll( dcService.getEtcConnInfoByDC("VRNI", (String)param.get("DC_ID")));
		 
		return vmwareApi.getVNI();
	}
	
	public Map run(String[] vmNames, Map param) throws Exception
	{
		try
		{
			VNIAPI api = getAPI(param);
			List flowList = new ArrayList();
			List portList = new ArrayList();
			List byteList = new ArrayList();
			for(int i=0; i < vmNames.length; i++) {
				param.put("VM_NM", vmNames[i]);
				Map result1 = api.getFlow(param);
				if(!result1.isEmpty()) {
					Map flow = (Map)result1.get("FLOW");
					flowList.addAll(flow.values());
					portList.addAll((List)result1.get("PORT_TRAFFIC"));
					byteList.addAll((List)result1.get("FLOW_BYTES"));
				}
			}
			
			Map result = new HashMap();
			result.put("FLOW", flowList);
			result.put("PORT_TRAFFIC", portList);
			result.put("FLOW_BYTES", byteList);
			return result;
		}
		catch(Exception e)
		{
			log.error("hv api error :" + this.getClass().getName() + ":" + param + ":" + e.getMessage());
			e.printStackTrace();
			if(e instanceof HypervisorException)
			{
				throw new NCException(((HypervisorException)e).getMsgParams());
			}
			throw e;
			//logging
		}
	}

}

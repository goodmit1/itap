package com.clovirsm.sys.hv.after;

import java.util.Map;

import com.clovirsm.common.NCConstant;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.service.TaskService;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.executor.Common;

public class OnAfterCreateDisk extends OnAfterCreateSM{
	@Override
	protected void onAfterSuccessExtra() throws Exception {
		 
		super.onAfterSuccessExtra();
		//service.useStart(getSvcCd(),getSvcId(),(String)param.get(Common.PARAM_TEAM_CD) ,(String) param.get(Common.PARAM_COMP_ID),(String) param.get(Common.PARAM_INS_ID));
	}
	public OnAfterCreateDisk(  Map param)
	{
		super( param);
	}
	protected String getTableNm() {
		 
		return "NC_DISK";
	}

	 
	protected String getSvcCd()
	{
		return NCConstant.SVC.D.toString();
	}
	protected String getSvcId()
	{
		return (String)param.get("DISK_ID");
	}
}

package com.clovirsm.sys.hv.after;

import java.util.Map;

import com.clovirsm.common.NCConstant;
import com.clovirsm.service.TaskService;
import com.clovirsm.sys.hv.DCService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public abstract class OnAfterCreateSM extends OnAfterCreate{
	public OnAfterCreateSM(  Map param) {
		super(  param);
		 
	}
	@Override
	protected  void onAfterStart(String taskId,Map oldParam)  
	{
		oldParam.put("RUN_CD", NCConstant.RUN_CD.W.toString());
		oldParam.put("TASK_STATUS_CD", NCConstant.RUN_CD.W.toString());
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		try {
			dcService.updateByQueryKey("update_" + this.getTableNm(), oldParam) ;
		} catch (Exception e) {
		 
			e.printStackTrace();
		}
	}
	@Override
	protected   boolean getState()  throws Exception
	{
	 
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		param.put("TASK_ID", taskId);
		Map result = dcService.getDC((String)param.get("DC_ID")).getAPI().chkTask(param);
		return (boolean)result.get("RESULT");
	}
	
	@Override
	public void onAfterSuccess(String taskId) throws Exception {
		 DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		 dcService.updateCreateSuccess(taskId, getTableNm(), param);
		 onAfterSuccessExtra();
		
		
	}
	@Override
	public void onAfterFail(String taskId, Throwable e) throws Exception {
		 
		 DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		 dcService.updateCreateFail(taskId, getTableNm(), param, e.getMessage());
		onAfterFailExtra();
	}
	protected void onAfterFailExtra()  throws Exception{
	 
		
	}

	protected void onAfterSuccessExtra() throws Exception {

		
	}
}

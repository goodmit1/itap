package com.clovirsm.sys.hv;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fliconz.fm.common.BaseService;
import com.fliconz.fm.common.util.HttpHelper;
import com.fliconz.fm.common.util.NumberHelper;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.security.util.ICrypt;
import com.fliconz.fw.runtime.util.LicenseReader;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.clovirsm.common.IDGenHelper;
import com.clovirsm.common.IPAddressUtil;
import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCDefaultService;
import com.clovirsm.common.NCException;
import com.clovirsm.hv.CommonAPI;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.IPInfo;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.hv.vmware.VMWareConnection;
import com.clovirsm.service.batch.UsageLogService;
import com.clovirsm.service.resource.IPService;
import com.clovirsm.sys.hv.executor.Common;

/**
 * Hypervisor 관련 서비스
 * @author 윤경
 *
 */
@Service
public class DCService extends NCDefaultService{

	 
	static String NS = "com.clovirsm.sys.dc";
	@Autowired
	transient UsageLogService usageLogService;
	
	@Autowired
	transient ICrypt crypt;
	
	@Autowired
	transient LicenseReader licenseReader;
	private Map<String, DC> dcMap;
	
	boolean isLoaded = false;
	
	
	@PostConstruct
	public void load() throws Exception
	{
		//licenseReader.check();
		loadDCInfo(true, new HashMap());
		isLoaded = true;
		
	}
	
	public boolean isLoaded() {
		return isLoaded;
	}
	public void putTempDCInfo(Map m) throws Exception
	{
		DC dc = new DC(  (String)m.get("HV_CD") );
		dc.addProp(m);
		dcMap.put((String)m.get("DC_ID"), dc);
	}
	 
	public String getConnIdByDCId(String kubun, String dcId) throws Exception{
		Map param = new HashMap();
		param.put("CONN_TYPE", kubun);
		param.put("DC_ID", dcId);
		return (String)this.selectOneObjectByQueryKey("getConnIdByDCId", param);
	}
	/**
	 * NC_DC, NC_DC_PROP 테이블  정보를 메모리에 올린다. Hypervisor 초기 연결를 hv.properties의  connection.initialSize에 정의된 개수 만큼 한다.
	 * @param isPoolMake 커넥션풀 초기 연결을 할지 여부
	 * @throws Exception
	 */
	public void loadDCInfo(boolean isPoolMake, Map param) throws Exception
	{
		dcMap = new HashMap();
		List<Map> list = super.selectList("NC_DC" , param);
		 
		for(Map m : list)
		{
			DC dc = new DC(  (String)m.get("HV_CD") );
			dc.addProp(m);
			dcMap.put((String)m.get("DC_ID"), dc);
		}
		if(isPoolMake)
		{
			 
			 
				for(Map m : list)
				{
					int initialSize = NumberUtil.getInt(m.get("INIT_POOL_SIZE"),0);
					if(initialSize<=0) continue;
					try
					{
						IConnection conn = ConnectionPool.getInstance().getConnection(VMWareConnection.class.getName(), (String)m.get("CONN_URL"), (String)m.get("CONN_USERID"), (String)m.get("CONN_PWD"), m);
						CommonAPI.disconnect(conn);
					}
					catch(Exception ignore)
					{
						System.out.println(m.get("DC_NM") + " connect fail");
					}
						
				}
			 
		}
		list = super.selectList("NC_DC_ETC_CONN" , param);
		for(Map m : list)
		{
			DC dc = dcMap.get((String)m.get("DC_ID"));
			if(dc == null) continue;
			String connProp = (String)m.get("CONN_PROP");
			if(connProp != null && !"".equals(connProp))
			{
				m.putAll(CommonUtil.getQueryMap(connProp));
			}
			dc.putProp((String)m.get("CONN_TYPE"), m);
			 
			
		}
		
		
		
		list = selectListByQueryKey("selectAllCenterProp", param);
		for(Map m : list)
		{
			DC dc = (DC)dcMap.get(m.get("DC_ID"));
			if(dc != null)
			{
				dc.putProp((String)m.get("PROP_NM"), (String)m.get("PROP_VAL"));
			}
		}
	}
	
	/**
	 * NC_COMP_DC_PROP테이블에서 회사의 데이터 센터 정보를 가지고 온다.
	 * @param orgId 회사 ID
	 * @param dc DC ID
	 * @return
	 * @throws Exception
	 */
	/*public Map getOrgDCInfo(Object compId, Object teamCd, String dc) throws Exception
	{
		Map param = new HashMap();
		param.put("COMP_ID", compId);
		param.put("TEAM_CD", teamCd);
		param.put("DC_ID", dc);
		List<Map>list =  super.selectListByQueryKey("selectOrgDcInfo", param);
		Map result = new HashMap();
		for(Map m : list)
		{
			result.put(m.get("PROP_NM"),  m.get("PROP_VAL"));
		}
		return result;
	}*/
	
	/**
	 * VM정보 
	 * @param vmId
	 * @return NC_VM 테이블값
	 * @throws Exception
	 */
	public Map getVMInfo(String vmId) throws Exception
	{
		Map param = new HashMap();
		param.put("VM_ID", vmId);
		Map info = super.selectOneByQueryKey("selectVMInfo", param);
		if(info == null)
		{
			throw new NCException(NCException.NOT_FOUND, "VM_ID=" + vmId);
		}
		return info;
	}
	
	public Map getUserInfo(Object Id) throws Exception
	{
		Map param = new HashMap();
		param.put("USER_ID", Id);
		Map info = super.selectOneByQueryKey("selectUser", param);
		if(info == null)
		{
			throw new NCException(NCException.NOT_FOUND, "USER_ID=" + Id);
		}
		return info;
	}
	
	/**
	 * Map을 VMInfo로 변환
	 * @param info VM정보
	 * @return
	 */
	public VMInfo toVMInfo(Map info)
	{
		VMInfo vminfo = new VMInfo();
		 
		vminfo.cpuCnt = NumberHelper.getInt(info.get("CPU_CNT"));
		vminfo.ramSize = NumberHelper.getInt(info.get("RAM_SIZE")) ;
		vminfo.diskSize =NumberHelper.getInt(info.get("DISK_SIZE")) ;
		vminfo.hostName = (String) info.get(HypervisorAPI.PARAM_VM_NM);
		vminfo.hvId = (String) info.get("VM_HV_ID");
		return vminfo;
	}
	
	/**
	 * NC_HV_OBJ 테이블에 데이터 추가
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public int insertHVObject(Map param) throws Exception
	{
		int row = updateDBTable("NC_HV_OBJ", param);
		if(row ==0) {
			return  insertDBTable("NC_HV_OBJ", param);
		}
		return row;
	}
	
	/**
	 *  NC_HV_OBJ 테이블에 데이터 삭제
	 * @param param CONN_URL(연결정보 URL)
	 * @return
	 * @throws Exception
	 */
	public int deleteAllHVObject(Map param) throws Exception
	{
		return deleteDBTable("NC_HV_OBJ", param);
	}
	/**
	 * VM생성하기 위한 정보
	 * @param vmId
	 * @return NC_VM 테이블 값, TMPL_PATH(템플릿 경로),  LINUX_YN(리눅스 여부),  EMAIL(작성자 이메일),  USER_NAME(작성자 이름)
	 * @throws Exception
	 */
	public Map getVMCreateInfo(String vmId, boolean isCreate ) throws Exception
	{
		Map param = new HashMap();
	 
		param.put("VM_ID", vmId);
		Map info =  super.selectOneByQueryKey("getVMCreateInfo", param);
		/*List<Map> list = super.selectListByQueryKey("selectDiskInfoByVM", param); 
		for(Map m : list)
		{
			if(isCreate || m.get("DISK_PATH") == null)
			{
				String[] dsNames = getDsName(m);
				m.put(HypervisorAPI.PARAM_DATASTORE_NM_LIST, dsNames);
			}
		}
		info.put(HypervisorAPI.PARAM_DATA_DISK_LIST, list);*/
		putVMCreateInfo( info );
		return info;
		 
	}
	public void putVMCreateInfo(Map info) throws Exception{
		List<Map> list = super.selectListByQueryKey("selectDiskInfoByVM", info); 
		for(Map m : list)
		{
			if(  m.get("DISK_PATH") == null)
			{
				String[] dsNames = getDsName(m);
				m.put(HypervisorAPI.PARAM_DATASTORE_NM_LIST, dsNames);
			}
		}
		info.put(HypervisorAPI.PARAM_DATA_DISK_LIST, list);
	}
	/**
	 * 회사에게 기본적으로 나눠주는 IP개수 ,hv.properties에  privateIp.cnt에 정의된 값, 기본 값은 32
	 * @return
	 * @throws Exception
	 */
	public static int getIpCnt() throws Exception
	{
		return HVProperty.getInstance().getInt("privateIp.cnt", 32);
	}
	
	  
	
	 
	
	/**
	 * VM 복사 원천 정보
	 * @param vmId 복사 원천 VM_ID 혹은 IMG_ID
	 * @return IMG_NM
	 * @throws Exception
	 */
	public Map getVMCopyInfo(String vmId) throws Exception
	{
		Map param = new HashMap();
		param.put("VM_ID", vmId);
	 
		return super.selectOneByQueryKey("getVMCopyInfo", param);
		 
	}
	
	/**
	 * 메모리에 있는 데이터 센터 정보
	 * @param dcId
	 * @return
	 */
	public DC getDC(String dcId)
	{
		return dcMap.get(dcId);
	}
	public DC getDCByIp(Map param) throws Exception{
		List<DC> list = getDCByIps(param);
		if(list.size()>0) {
			return list.get(0);
		}
		return null;
		
	}
	public List<DC> getDCByIps(Map param) throws Exception
	{
		String ip = null;
		String domain = null;
		String addr = (String)param.get("DC_IP");
		try
		{
			Integer.parseInt(addr.substring(0,addr.indexOf(".")));
			ip = addr;
			InetAddress ipAddress = java.net.InetAddress.getByName( addr ); // 
			domain = ipAddress.getHostName();
		}
		catch(Exception e)
		{
			InetAddress ipAddress = java.net.InetAddress.getByName( addr ); // 
			ip = ipAddress.getHostAddress();
			domain = addr;
		}
		
		param.put("DC_IP", ip);
		param.put("DC_DOMAIN", domain);
		List<String> list = this.selectListByQueryKey("selectDCIdByIpName", param);
		List<DC> result = new ArrayList();
		for(String s : list) {
			DC  dc = getDC(s);
			if(dc != null) {
				result.add(dc);
			}
		}
		return result;
		
	}
	@Override
	protected String getNameSpace() {  
		return NS;
	}
	
	/**
	 * NC_COMP_DC_PROP테이블에 회사별 데이터 센터 속성 값 추가
	 * @param param COMP_ID, DC_ID
	 * @param result 회사별 데이터 센터 속성 값  
	 * @throws Exception
	 */
	/*public void insertOrgDCInfo(Map param, Map result) throws Exception {
		 
		 for(Object key : result.keySet())
		 {
			 param.put("PROP_NM", key);
			 param.put("PROP_VAL", result.get(key));
			 super.insertDBTable("NC_COMP_DC_PROP", param);
		 }
		
	}
	public void deleteAllOrgDCInfo() throws Exception
	{
		 super.deleteDBTable("NC_COMP_DC_PROP", new HashMap());
	}*/
	/*public void updateDiskVMID(String vmId, String diskId) throws Exception 
	{
		Map param = new HashMap();
		param.put("VM_ID", vmId);
		param.put("DISK_ID", diskId);
		int row = this.updateByQueryKey("update_VM_ID", param);
	}*/
	
	/**
	 * 디스크 정보
	 * @param diskId
	 * @return
	 * @throws Exception
	 */
	public Map getDiskInfo(String diskId) throws Exception {
		Map param = new HashMap();
		param.put("DISK_ID", diskId);
	 
		return super.selectOneByQueryKey("selectDiskInfo", param);
	}
	
	/**
	 * 이미지 정보
	 * @param imgId
	 * @return
	 * @throws Exception
	 */
	public Map getImgInfo(String imgId) throws Exception {
		Map param = new HashMap();
		param.put("IMG_ID", imgId);
	 
		return super.selectOneByQueryKey("selectImgInfo", param);
	}
	
	/**
	 * 스냅샷 정보
	 * @param vmId
	 * @param snapshotId
	 * @return
	 * @throws Exception
	 */
	public Map getSnapshotInfo(String vmId, String snapshotId)  throws Exception {
		Map param = new HashMap();
		param.put("VM_ID", vmId);
		if(snapshotId != null) param.put("SNAPSHOT_ID", snapshotId);
		return super.selectOneByQueryKey("selectSnapshotInfo", param);
	}
 
	/**
	 * NC_DISK에 디스크 정보 추가, VM생성 후 추가 디스크가 있다면
	 * @param param VM_ID,TEAM_CD,DC_ID,DISK_NM,CMT,DISK_SIZE,FEE_TYPE_CD,DISK_PATH,DS_NM
	 * @return
	 * @throws Exception
	 */
	public int insertDisk(Map param) throws Exception
	{
		param.put(Common.PARAM_DISK_ID, IDGenHelper.getId(NCConstant.SVC.D.toString()));
		int row = super.insertDBTable("NC_DISK", param);
		if(row == 1)
		{
			//usageLogService.useStart(NCConstant.SVC.D.toString(),(String) param.get(Common.PARAM_DISK_ID), (String)param.get(Common.PARAM_TEAM_CD) ,(String) param.get(Common.PARAM_COMP_ID),(String) param.get(Common.PARAM_INS_ID));
		}
		return row;
	}
	
	/**
	 * 디스크 종류에 해당하는 데이터 스토어 목록,  VM이 적은 순
	 * @param param DC_ID, DISK_TYPE_ID 혹은 SPEC_ID 
	 * @return  
	 * @throws Exception
	 */
	public String[] getDsName(Map param) throws Exception
	{
		List<Map> list = this.selectListByQueryKey("getDSInfo", param);
		String[] result = new String[list.size()];
		int idx = 0;
		for(Map m : list)
		{
			result[idx++] = (String)m.get("OBJ_ID");
		}
		return result;
	}
	 
	/**
	 * VM정지
	 * @param m VM_ID, TEAM_CD, COMP_ID
	 * @throws Exception
	 */
	public void updateVMStop(Map m) throws Exception {
		 
		usageLogService.useStop( NCConstant.SVC.S.toString(), 
				(String) m.get(Common.PARAM_VM_ID),
				(String)m.get(Common.PARAM_TEAM_CD),
				(String) m.get(Common.PARAM_COMP_ID),
				(String) m.get(Common.PARAM_INS_ID));
		try	{
			if(!"F".equals(m.get("RUN_CD"))){
				m.put("RUN_CD", NCConstant.RUN_CD.S.toString());
			}
			super.updateDBTable("NC_VM", m);
		}
		catch(Exception ignore)	{}
		
	}
	 
	 
	
	
	
	
	/**
	 * 다음 NIC_ID 가져오기
	 * @param param  VM_ID
	 * @return
	 * @throws Exception
	 */
	public int getNextNicId(Map param) throws Exception {
		Map m =  selectOneByQueryKey("selectNextNicId", param);
		return NumberHelper.getInt(m.get("NIC_ID"));
	}
	
	/**
	 * VM시작과 관련 DB update
	 * @param param VM_ID, TEAM_CD(팀 코드), COMP_ID(회사 ID), INS_ID(시작한 사람)
	 * @throws Exception
	 */
	public void updateVMStart(Map param) throws Exception {
		param.put("RUN_CD", NCConstant.RUN_CD.R.toString());
		super.updateDBTable("NC_VM", param);
		try	{
			usageLogService.useStart(NCConstant.SVC.S.toString(), 
					(String) param.get(Common.PARAM_VM_ID),
					(String)param.get(Common.PARAM_TEAM_CD),
					(String) param.get(Common.PARAM_COMP_ID),(String) param.get(Common.PARAM_INS_ID));
		}
		catch(Exception ignore){}
	}
	
	 
	
	
	 
	/**
	 * 이미지 디스크 목록
	 * @param fromId 이미지 ID
	 * @return
	 * @throws Exception
	 */
	public List<Map> getImgDisk(String fromId) throws Exception {
		Map param = new HashMap();
		param.put(Common.PARAM_FROM_ID, fromId);
		return this.selectListByQueryKey("selectImgDisk", param );
		
	}
	/**
	 * Datastore이름으로 디스크 종류 알아내기
	 * @param dcId
	 * @param dsNm
	 * @return
	 * @throws Exception
	 */
	public Object getDiskType(String dcId, String dsNm) throws Exception
	{
		Map param = new HashMap();
		param.put("DC_ID", dcId);
		param.put("DS_NM", dsNm);
		return this.selectOneObjectByQueryKey( "select_disk_type_by_ds_nm", param);
	}
	
	
	/**
	 * 생성 실패 된 겅우 해당 테이블에 데이터 update
	 * @param taskId
	 * @param tableNm
	 * @param param
	 * @param message
	 * @throws Exception
	 */
	public void updateCreateFail(String taskId,String tableNm, Map param, String message) throws Exception {
		 
		param.put("RUN_CD", NCConstant.RUN_CD.F.toString());
		param.put("TASK_STATUS_CD", NCConstant.RUN_CD.F.toString());
		param.put("FAIL_MSG", message);
		super.updateDBTable( tableNm, param);
		
	}
	
	/**
	 * 생성 성공한 겅우 해당 테이블에 데이터 update
	 * @param taskId
	 * @param tableNm
	 * @param param
	 * @throws Exception
	 */
	public void updateCreateSuccess(String taskId,String tableNm, Map param ) throws Exception {
		 
		param.put("RUN_CD", NCConstant.RUN_CD.S.toString());
		param.put("TASK_STATUS_CD", NCConstant.RUN_CD.S.toString());
		param.put("FAIL_MSG", "");
		super.updateDBTable( tableNm, param);
		
	}
	
	/**
	 * 사용 시작되었음을 NC_DD_USE테이블에 insert
	 * @param svcCd
	 * @param svcId
	 * @param teamCd
	 * @param compId
	 * @param userId
	 * @throws Exception
	 */
	public void useStart(String svcCd, String svcId, String teamCd, String compId, String userId) throws Exception {
		usageLogService.useStart(svcCd, svcId, teamCd, compId, userId);
		
	}
	
	
	/**
	 * 정기적으로 제거할 스냅샷 목록, hv.properties의 snapshot_remove_day_term에 보관일 지정하여 그 이후 삭제됨.
	 * @return
	 * @throws Exception
	 */
	public List<Map> selectRemoveSnapshotTarget() throws Exception {
		Map param = new HashMap();
		param.put("DD", HVProperty.getInstance().getInt("snapshot_remove_day_term", 1));
		return this.selectListByQueryKey("selectRemoveSnapshotTarget", param);
	}
	public void deleteDiskByVM(Map param) throws Exception {
	 
		this.updateByQueryKey("deleteDiskByVM", param);
		
	}
	 
	
	
	public List<Map> getEtcConnInfo(String connType) throws Exception{
		Map param = new HashMap();
		param.put("CONN_TYPE",  connType);
		 
		List<Map> 	list = super.selectList("NC_ETC_CONN" , param);
			
			for(Map m:list) {
				if(!TextHelper.isEmpty((String)m.get("CONN_PROP"))) {
					try {
						m.putAll(CommonUtil.getQueryMap((String)m.get("CONN_PROP")));
					}
					catch(Exception ignore) {}
				}
				   
			}
			return list;
		 
	}
	public Map getEtcConnInfoByDC(String connType, Object dcId) throws Exception{
		Map param = new HashMap();
		param.put("CONN_TYPE",  connType);
		param.put("DC_ID",  dcId);
		List<Map> list;
		 
			list = super.selectList("NC_ETC_CONN" , param);
			
			if(list.size()>0) {
				Map m =  list.get(0);
				if(!TextHelper.isEmpty((String)m.get("CONN_PROP"))) {
					try {
						m.putAll(CommonUtil.getQueryMap((String)m.get("CONN_PROP")));
					}
					catch(Exception ignore) {}
				}
				return m;
			}
			else {
				throw new Exception("No Connection Info" + connType);
			}
		 
	}
	public Map getEtcConnInfo(String connType, Object connId) {
		Map param = new HashMap();
		param.put("CONN_TYPE",  connType);
		if(connId != null) {
			param.put("CONN_ID",  connId);
		}
		List<Map> list;
		try {
			list = super.selectList("NC_ETC_CONN" , param);
			
			if(list.size()>0) {
				Map m =  list.get(0);
				if(!TextHelper.isEmpty((String)m.get("CONN_PROP"))) {
					m.putAll(CommonUtil.getQueryMap((String)m.get("CONN_PROP")));
				}
				return m;
			}
			else {
				return null;
			}
		} catch (Exception e) {
			 
			e.printStackTrace();
			return null;
		}
	}
 

	public Map getCatalogReqInfo(String id) throws Exception {
		Map param = new HashMap();
		param.put("CATALOGREQ_ID", id);
		return this.selectOneByQueryKey("getCatalogReqInfo", param);
	}

	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getPks() {
		// TODO Auto-generated method stub
		return null;
	}


	 
}

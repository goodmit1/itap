package com.clovirsm.sys.hv.nsx;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.nsx.NSXAPI;
import com.clovirsm.hv.vmware.vra.VRAAPI;
import com.clovirsm.hv.vmware.vrops.VROpsAPI;
import com.clovirsm.service.admin.DcKubunConfService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;

@Component
public class NSXAction {

	@Autowired DCService dcService;
	@Autowired DcKubunConfService mapService;
	public void insertDCMapping() throws Exception {
		List list = new ArrayList();
		List<Map> connList = dcService.getEtcConnInfo("NSX");
		for(Map m:connList) {
			NSXAPI api = getAPI();
			Map result = api.listDC(m);
			List<String> dclist = (List)result.get("LIST");
			String conn = (String)m.get("CONN_ID");
			mapService.delete(null,conn,"DC_ETC_CONN", null);
			for(String s : dclist) {
				m.put("DC_IP", s);
				List<DC> dcList = dcService.getDCByIps(m);
				for(DC dc:dcList) {
					String dcId = dc.getProp("DC_ID");
					mapService.insert(dcId, conn, "DC_ETC_CONN", null, null);
				}
			}
			
		}
	}
	private NSXAPI getAPI() throws Exception {
			VMWareAPI vmwareApi = new VMWareAPI();
			NSXAPI api =  vmwareApi.getNSX();
			return api;
	}
}
	
